﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class AdvanceSalDeductionBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal=new ClsCommonOperationDAL();
        AdvanceSalDeductionDAL aAdvanceSalDeductionDAL = new AdvanceSalDeductionDAL();
        public bool SaveDataForAdvanceSalDeduction(AdvanceSalDeduction aAdvanceSalDeduction)
        {
            try
            {
                if (!aAdvanceSalDeductionDAL.HasAdvanceSalDeduction(aAdvanceSalDeduction))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aAdvanceSalDeduction.ASDId = aClsPrimaryKeyFind.PrimaryKeyMax("ASDId", "tblAdvanceSalaryDeduction", "HRDB");
                    aAdvanceSalDeductionDAL.SaveAdvanceSalDeduction(aAdvanceSalDeduction);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView,aDataTable);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aAdvanceSalDeductionDAL.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aAdvanceSalDeductionDAL.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aAdvanceSalDeductionDAL.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aAdvanceSalDeductionDAL.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aAdvanceSalDeductionDAL.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aAdvanceSalDeductionDAL.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aAdvanceSalDeductionDAL.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aAdvanceSalDeductionDAL.LoadSalGradeName(aDropDownList);
        }
        public void DeleteAdvanceSalDeductionBLL(string ASDId)
        {
           aAdvanceSalDeductionDAL.DeleteAdvanceSalDeduction(ASDId);

        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aAdvanceSalDeductionDAL.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(AdvanceSalDeduction aAdvanceSalDeduction)
        {
            return aAdvanceSalDeductionDAL.UpdateAdvanceSalDeduction(aAdvanceSalDeduction);
        }
        public DataTable LoadAdvanceSalDeduction()
        {
            return aAdvanceSalDeductionDAL.LoadAdvanceSalDeductionView();
        }
        public AdvanceSalDeduction AdvanceSalDeductionEditLoad(string ASDId)
        {
            return aAdvanceSalDeductionDAL.AdvanceSalDeductionEditLoad(ASDId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aAdvanceSalDeductionDAL.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aAdvanceSalDeductionDAL.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aAdvanceSalDeductionDAL.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aAdvanceSalDeductionDAL.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aAdvanceSalDeductionDAL.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aAdvanceSalDeductionDAL.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aAdvanceSalDeductionDAL.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aAdvanceSalDeductionDAL.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aAdvanceSalDeductionDAL.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aAdvanceSalDeductionDAL.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(AdvanceSalDeduction aAdvanceSalDeduction)
        {
            return aAdvanceSalDeductionDAL.ApprovalUpdateDAL(aAdvanceSalDeduction);
        }
        public DataTable LoadAdvanceSalDeductionViewForApproval(string ActionStatus)
        {
            return aAdvanceSalDeductionDAL.LoadAdvanceSalDeductionViewForApproval(ActionStatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aAdvanceSalDeductionDAL.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aAdvanceSalDeductionDAL.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aAdvanceSalDeductionDAL.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
