﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ArrearBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        ArrearDAL aArrearDal = new ArrearDAL();
        public bool SaveDataForArrear(Arrear aArrear)
        {
            try
            {
                if (!aArrearDal.HasArrear(aArrear))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aArrear.ArrearId = aClsPrimaryKeyFind.PrimaryKeyMax("ArrearId", "tblArrear", "HRDB");
                    aArrearDal.SaveArrear(aArrear);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aArrearDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aArrearDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aArrearDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aArrearDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aArrearDal.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aArrearDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aArrearDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aArrearDal.LoadSalGradeName(aDropDownList);
        }
        public void DeleteArrearBLL(string ArrearId)
        {
           aArrearDal.DeleteArrear(ArrearId);

        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aArrearDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(Arrear aArrear)
        {
            return aArrearDal.UpdateArrear(aArrear);
        }
        public DataTable LoadArrear()
        {
            return aArrearDal.LoadArrearView();
        }
        public Arrear ArrearEditLoad(string ArrearId)
        {
            return aArrearDal.ArrearEditLoad(ArrearId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aArrearDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aArrearDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aArrearDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aArrearDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aArrearDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aArrearDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aArrearDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aArrearDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aArrearDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aArrearDal.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(Arrear aArrear)
        {
            return aArrearDal.ApprovalUpdateDAL(aArrear);
        }
        public DataTable LoadArrearViewForApproval(string ActionStatus)
        {
            return aArrearDal.LoadArrearViewForApproval(ActionStatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aArrearDal.PlaceEmpStatus(aEmpGeneralInfo);
        }

        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aArrearDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aArrearDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
