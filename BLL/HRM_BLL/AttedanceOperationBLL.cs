﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
   public class AttedanceOperationBLL
    {
       AttedanceOperationDAL attedanceOperationDal = new AttedanceOperationDAL();
       public void UpdateOperationBLL(DateTime attDate,int empId,string AttStatus,string Remarks)
       {

           DataTable dtCheckWHReplace = attedanceOperationDal.CheckWeeklyHolidayReplace(empId, attDate);

           DataTable dtCheckWH = attedanceOperationDal.CheckWeeklyHoliday(empId, attDate);

           if (dtCheckWHReplace.Rows.Count > 0)
           {
               Remarks = Remarks + ",Weekly Holiday";
           }
           else
           {
               if (dtCheckWH.Rows.Count > 0)
               {
                   Remarks = Remarks + ",Weekly Holiday";
               }
           }
           
           if (attedanceOperationDal.CheckAttData(attDate,empId).Rows.Count>0)
           {
               attedanceOperationDal.AttOperationUpdateDAL(attDate,empId,AttStatus,Remarks);
           }
           else
           {
               attedanceOperationDal.AttOperationInsertDAL(attDate, empId, AttStatus, Remarks);
           }
       }


    }
}
