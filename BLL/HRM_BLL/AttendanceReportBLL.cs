﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
    public class AttendanceReportBLL
    {
        AttendanceReportDAL attendanceReportDal=new AttendanceReportDAL();
        public DataTable RptHeader()
        {
            return attendanceReportDal.RptHeader();
        }
        public DataTable MonthlyAttendance(string Date, string EmpCode)
        {
            return attendanceReportDal.MonthlyAttendance(Date, EmpCode);
        }
        public DataTable TotalStats(string fromdate, string todate)
        {
            return attendanceReportDal.TotalStats(fromdate, todate);

        }
        public DataTable EmployeeAttendanceRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.EmployeeAttendanceRecordDAL(fromDate, toDate, parameter);
        }
        public DataTable DeptWiseAttendanceSummaryBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.DeptWiseAttendanceSummaryDAL(fromDate, toDate, parameter);
        }
        public DataTable AllRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.PresentAllRecordDAL(fromDate, toDate, parameter);
        }
        public DataTable ManualRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.ManualRecordDAL(fromDate, toDate, parameter);
        }
        public DataTable PresentRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.PresentRecordDAL(fromDate, toDate, parameter);
        }
        public DataTable AbsentRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.AbsentRecordDAL(fromDate, toDate, parameter);
        }
        public DataTable LateRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.LateRecordDAL(fromDate, toDate, parameter);
        }
        public DataTable LeaveRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.LeaveRecordDAL(fromDate, toDate, parameter);
        }

        public DataTable OnDutyRecordBLL(string fromDate, string toDate, string parameter)
        {
            return attendanceReportDal.OnDutyRecordDAL(fromDate, toDate, parameter);
        }

        public DataTable MonthlyAttendanceSummery(DateTime fromDate, DateTime toDate, string parameter)
        {
            DataTable dtallemp = attendanceReportDal.EmpInfo(parameter);
            DataTable dtMain = new DataTable();
            dtMain.Columns.Add("EmpMasterCode");
            dtMain.Columns.Add("EmpName");
            dtMain.Columns.Add("JoiningDate");
            dtMain.Columns.Add("Designation");
            dtMain.Columns.Add("Department");
            dtMain.Columns.Add("TotalMonthDay");
            dtMain.Columns.Add("TotalPresent");
            dtMain.Columns.Add("TotalAbsent");
            dtMain.Columns.Add("TotalOTHour");
            DataRow dr;

            foreach (DataRow dataRow in dtallemp.Rows)
            {
                DataTable dtpresent = attendanceReportDal.TotalPresent(dataRow["EmpInfoId"].ToString(),fromDate,toDate);
                DataTable dtabsent = attendanceReportDal.TotalAbsent(dataRow["EmpInfoId"].ToString(),fromDate,toDate);
                int totalmonthday = Convert.ToInt32(attendanceReportDal.GetMonthDays(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate)));
                dr = dtMain.NewRow();
                dr["EmpMasterCode"] = dataRow["EmpMasterCode"].ToString();
                dr["EmpName"] = dataRow["EmpName"].ToString();
                dr["JoiningDate"] = Convert.ToDateTime((dataRow["JoiningDate"].ToString())).ToString("dd-MMM-yyyy");
                dr["Designation"] = dataRow["DesigName"].ToString();
                dr["Department"] = dataRow["DeptName"].ToString();
                dr["TotalMonthDay"] = totalmonthday;
                dr["TotalPresent"] = dtpresent.Rows[0][0].ToString();
                dr["TotalAbsent"] = dtabsent.Rows[0][0].ToString();
                dr["TotalOTHour"] = attendanceReportDal.OverTimeHour(dataRow["EmpInfoId"].ToString(), Convert.ToDateTime(fromDate),
                                                                           Convert.ToDateTime(toDate));
                dtMain.Rows.Add(dr);
            }

            return dtMain;
        }
        public DataTable EmpImage(string EmpInfoId)
        {

            return attendanceReportDal.EmpImage(EmpInfoId);

        }
        public DataTable Total(DateTime fromDate, DateTime toDate, string parameter)
        {
            DataTable dtallemp = attendanceReportDal.EmpInfo(parameter);
            DataTable dtMain = new DataTable();

            dtMain.Columns.Add("EmpInfoId");
            dtMain.Columns.Add("TotalLate");
            dtMain.Columns.Add("TotalLateTime");
            dtMain.Columns.Add("TotalPresent");
            dtMain.Columns.Add("TotalAbsent");
            dtMain.Columns.Add("TotalLeave");
            dtMain.Columns.Add("TotalOT");
            DataRow dr;

            foreach (DataRow dataRow in dtallemp.Rows)
            {
                DataTable dttotal = attendanceReportDal.Total(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtlate = attendanceReportDal.TotalLate(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dttotalot = attendanceReportDal.TotalOT(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                dr = dtMain.NewRow();
                dr["EmpInfoId"] = dataRow["EmpInfoId"].ToString();
                if (dttotal.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dttotalot.Rows[0]["OTFirstMin"].ToString()) > 60)
                    {
                        dr["TotalOT"] = dttotalot.Rows[0]["OTSecond"].ToString() + ":" +
                                         dttotalot.Rows[0]["OTSecondMin"].ToString();
                    }
                    if (Convert.ToInt32(dttotalot.Rows[0]["OTFirstMin"].ToString()) < 60)
                    {
                        dr["TotalOT"] = dttotalot.Rows[0]["OTFirst"].ToString() + ":" +
                                         dttotalot.Rows[0]["OTFirstMin"].ToString();
                    }
                    if (Convert.ToInt32(dttotalot.Rows[0]["OTFirstMin"].ToString()) == 60)
                    {
                        dr["TotalOT"] = (Convert.ToInt32(dttotalot.Rows[0]["OTFirst"].ToString()) + 1) + ":00";

                    }

                    dr["TotalLate"] = dttotal.Rows[0][3].ToString();
                    dr["TotalPresent"] = dttotal.Rows[0][2].ToString();
                    dr["TotalAbsent"] = dttotal.Rows[0][5].ToString();
                    dr["TotalLeave"] = dttotal.Rows[0][4].ToString();
                    dr["TotalLateTime"] = dtlate.Rows[0][0].ToString();
                    dtMain.Rows.Add(dr);
                }
            }

            return dtMain;
        }


        public DataTable SingleWEmpAttendance(DateTime fromDate, DateTime toDate, string parameter)
        {
            DataTable dtallemp = attendanceReportDal.EmployeeAttendanceRecordDAL(fromDate.ToString(), toDate.ToString(), parameter);
            DataTable dtMain = new DataTable();
            dtMain.Columns.Add("EmpMasterCode");
            dtMain.Columns.Add("EmpInfoId");
            dtMain.Columns.Add("EmpName");
            dtMain.Columns.Add("DayName");
            dtMain.Columns.Add("ATTDate");
            dtMain.Columns.Add("JoiningDate");
            dtMain.Columns.Add("DesigName");
            dtMain.Columns.Add("DeptName");
            dtMain.Columns.Add("InTime");
            dtMain.Columns.Add("OutTime");
            dtMain.Columns.Add("Late");
            dtMain.Columns.Add("ATTStatus");
            dtMain.Columns.Add("OverTimeDuration");
            dtMain.Columns.Add("Remarks");
            dtMain.Columns.Add("FromDate");
            dtMain.Columns.Add("ToDate");
            dtMain.Columns.Add("DutyDuration");
            dtMain.Columns.Add("CardNo");
            dtMain.Columns.Add("WorkingDay");

            DataRow dr;

            foreach (DataRow dataRow in dtallemp.Rows)
            {

                dr = dtMain.NewRow();
                dr["EmpMasterCode"] = dataRow["EmpMasterCode"].ToString();
                dr["EmpInfoId"] = dataRow["EmpInfoId"].ToString();
                dr["FromDate"] = Convert.ToDateTime(dataRow["FromDate"].ToString()).ToString("dd-MMM-yyyy");
                dr["ToDate"] = Convert.ToDateTime(dataRow["ToDate"].ToString()).ToString("dd-MMM-yyyy");
                dr["EmpName"] = dataRow["EmpName"].ToString();
                dr["JoiningDate"] = Convert.ToDateTime((dataRow["JoiningDate"].ToString())).ToString("dd-MMM-yyyy");
                dr["DesigName"] = dataRow["DesigName"].ToString();
                dr["DeptName"] = dataRow["DeptName"].ToString();
                dr["InTime"] = dataRow["InTime"].ToString();
                dr["ATTDate"] = Convert.ToDateTime(dataRow["ATTDate"].ToString()).ToString("dd-MM-yyyy");
                dr["DayName"] = dataRow["DayName"].ToString();
                dr["OutTime"] = dataRow["OutTime"].ToString();
                dr["DutyDuration"] = dataRow["DutyDuration"].ToString();

                DateTime fromDt = new DateTime();
                DateTime toDt = new DateTime();
                fromDt = Convert.ToDateTime(fromDate);
                toDt = Convert.ToDateTime(toDate);
                TimeSpan span = toDt.Subtract(fromDt);
                dr["WorkingDay"] = Convert.ToString(((int)span.TotalDays) + 1);

                TimeSpan duration;
                if (dataRow["ATTStatus"].ToString() == "L")
                {
                    try
                    {
                        duration = DateTime.Parse(dataRow["InTime"].ToString()).Subtract(DateTime.Parse(dataRow["ShiftStart"].ToString()));
                    }
                    catch (Exception)
                    {

                        duration = Convert.ToDateTime("00:00:00").TimeOfDay;
                    }

                }
                else
                {
                    duration = Convert.ToDateTime("00:00:00").TimeOfDay;
                }

                dr["Late"] = duration;
                dr["OverTimeDuration"] = dataRow["OverTimeDuration"].ToString();
                dr["ATTStatus"] = dataRow["ATTStatus"].ToString();
                dr["Remarks"] = dataRow["Remarks"].ToString();
                

                dtMain.Rows.Add(dr);
            }

            return dtMain;
        }

        public string ShortDayName(string dayname)
        {
            if (dayname == "Saturday")
            {
                dayname = "Sat";
            }
            if (dayname == "Sunday")
            {
                dayname = "Sun";
            }
            if (dayname == "Monday")
            {
                dayname = "Mon";
            }
            if (dayname == "Tuesday")
            {
                dayname = "Tue";
            }
            if (dayname == "Wednesday")
            {
                dayname = "Wed";
            }
            if (dayname == "Thursday")
            {
                dayname = "Thu";
            }
            if (dayname == "Friday")
            {
                dayname = "Fri";
            }
            return dayname;
        }
        public DataTable MonthlyDeptWiseSumAttendance(DateTime fromDate, DateTime toDate, string parameter)
        {
            DataTable dtallemp = attendanceReportDal.EmpInfo(parameter);
            DataTable dtMain = new DataTable();
            
            dtMain.Columns.Add("EmpMasterCode");
            dtMain.Columns.Add("EmpName");
            dtMain.Columns.Add("FromDate");
            dtMain.Columns.Add("ToDate");
            dtMain.Columns.Add("DesigName");
            dtMain.Columns.Add("DeptName");
            dtMain.Columns.Add("Present");
            dtMain.Columns.Add("LatePresent");
            dtMain.Columns.Add("Absent");
            dtMain.Columns.Add("EarnLeave");
            dtMain.Columns.Add("CasualLeave");
            dtMain.Columns.Add("SickLeave");
            dtMain.Columns.Add("MaternityLeave");
            dtMain.Columns.Add("OtherLeave");
            dtMain.Columns.Add("Tour");
            dtMain.Columns.Add("Dayes");
            dtMain.Columns.Add("Overtime");
            dtMain.Columns.Add("WHoliday");
            dtMain.Columns.Add("WorkingDay");
            
            DataRow dr;

            foreach (DataRow dataRow in dtallemp.Rows)
            {

                DataTable dtpresent =attendanceReportDal.Present(dataRow["EmpInfoId"].ToString(),fromDate,toDate);
                DataTable dtlatepresent = attendanceReportDal.LatePresent(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtabsent =attendanceReportDal.Absent(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtearn = attendanceReportDal.Earn(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtcasual = attendanceReportDal.Casual(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtmedical = attendanceReportDal.Medical(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtmaternity = attendanceReportDal.Maternity(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtother = attendanceReportDal.Other(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                DataTable dtholiday = attendanceReportDal.TotalHoliday(fromDate, toDate);

                dr = dtMain.NewRow();
                dr["EmpMasterCode"] = dataRow["EmpMasterCode"].ToString();
                dr["EmpName"] = dataRow["EmpName"].ToString();
                dr["FromDate"] = Convert.ToDateTime(fromDate).ToString("dd-MMM-yyyy");
                dr["ToDate"] = Convert.ToDateTime(toDate).ToString("dd-MMM-yyyy");
                dr["DesigName"] = dataRow["DesigName"].ToString();
                dr["DeptName"] = dataRow["DeptName"].ToString();
                if (dtpresent.Rows.Count>0)
                {
                    dr["Present"] = dtpresent.Rows[0][0].ToString();    
                }
                else
                {
                    dr["Present"] = "0";
                }
                if (dtlatepresent.Rows.Count>0)
                {
                    dr["LatePresent"] = dtlatepresent.Rows[0][0].ToString();    
                }
                else
                {
                    dr["LatePresent"] = "0";
                }
                if (dtabsent.Rows.Count>0)
                {
                    dr["Absent"] = dtabsent.Rows[0][0].ToString();    
                }
                else
                {
                    dr["Absent"] = "0";
                }
                int day = Convert.ToInt32(attendanceReportDal.GetMonthDays(fromDate, toDate));
                int hday = Convert.ToInt32(dtholiday.Rows[0][0].ToString());
                DataTable dtot = attendanceReportDal.Total(dataRow["EmpInfoId"].ToString(), fromDate, toDate);
                dr["EarnLeave"] = dtearn.Rows[0][0].ToString();
                dr["CasualLeave"] = dtcasual.Rows[0][0].ToString();
                dr["SickLeave"] = dtmedical.Rows[0][0].ToString();
                dr["MaternityLeave"] = dtmaternity.Rows[0][0].ToString();
                dr["OtherLeave"] = dtother.Rows[0][0].ToString();
                dr["Tour"] = "0";
                dr["Dayes"] = attendanceReportDal.GetMonthDays(fromDate, toDate);
                if (dtot.Rows.Count>0)
                {
                    dr["Overtime"] = dtot.Rows[0]["TotalOT"].ToString();    
                }
                else
                {
                    dr["Overtime"] = "00:00";
                }
                dr["WHoliday"] = dtholiday.Rows[0][0].ToString();
                dr["WorkingDay"] = day - hday;

                dtMain.Rows.Add(dr);
            }

            return dtMain;
        }

        public DataTable MonthDayName(string fromdate, string todate)
        {
            
            DataTable aDataTable=new DataTable();

            aDataTable.Columns.Add("1");
            aDataTable.Columns.Add("2");
            aDataTable.Columns.Add("3");
            aDataTable.Columns.Add("4");
            aDataTable.Columns.Add("5");
            aDataTable.Columns.Add("6");
            aDataTable.Columns.Add("7");
            aDataTable.Columns.Add("8");
            aDataTable.Columns.Add("9");
            aDataTable.Columns.Add("10");
            aDataTable.Columns.Add("11");
            aDataTable.Columns.Add("12");
            aDataTable.Columns.Add("13");
            aDataTable.Columns.Add("14");
            aDataTable.Columns.Add("15"); 
            aDataTable.Columns.Add("16");
            aDataTable.Columns.Add("17");
            aDataTable.Columns.Add("18");
            aDataTable.Columns.Add("19");
            aDataTable.Columns.Add("20");
            aDataTable.Columns.Add("21");
            aDataTable.Columns.Add("22");
            aDataTable.Columns.Add("23");
            aDataTable.Columns.Add("24");
            aDataTable.Columns.Add("25");
            aDataTable.Columns.Add("26");
            aDataTable.Columns.Add("27");
            aDataTable.Columns.Add("28");
            aDataTable.Columns.Add("29");
            aDataTable.Columns.Add("30");
            aDataTable.Columns.Add("31");

            DataRow dataRow = null;
            try
            {

            DataTable dt = attendanceReportDal.DayName(fromdate, todate);

            
                dataRow = aDataTable.NewRow();
                
                    dataRow["1"] = ShortDayName(dt.Rows[0][1].ToString());    
                
                    dataRow["2"] = ShortDayName(dt.Rows[1][1].ToString());
                
                    dataRow["3"] = ShortDayName(dt.Rows[2][1].ToString());
                
                    dataRow["4"] = ShortDayName(dt.Rows[3][1].ToString());
                
                    dataRow["5"] = ShortDayName(dt.Rows[4][1].ToString());
                
                    dataRow["6"] = ShortDayName(dt.Rows[5][1].ToString());
                
                    dataRow["7"] = ShortDayName(dt.Rows[6][1].ToString());
                
                    dataRow["8"] = ShortDayName(dt.Rows[7][1].ToString());
                
                    dataRow["9"] = ShortDayName(dt.Rows[8][1].ToString());
                
                    dataRow["10"] = ShortDayName(dt.Rows[9][1].ToString());
                
                    dataRow["11"] = ShortDayName(dt.Rows[10][1].ToString());

                   dataRow["12"] = ShortDayName(dt.Rows[11][1].ToString());
                
                    dataRow["13"] = ShortDayName(dt.Rows[12][1].ToString());
                
                    dataRow["14"] = ShortDayName(dt.Rows[13][1].ToString());
                
                    dataRow["15"] = ShortDayName(dt.Rows[14][1].ToString());
                
                    dataRow["16"] = ShortDayName(dt.Rows[15][1].ToString());
                
                    dataRow["17"] = ShortDayName(dt.Rows[16][1].ToString());
                
                    dataRow["18"] = ShortDayName(dt.Rows[17][1].ToString());
                
                    dataRow["19"] = ShortDayName(dt.Rows[18][1].ToString());
                
                    dataRow["20"] = ShortDayName(dt.Rows[19][1].ToString());
                
                    dataRow["21"] = ShortDayName(dt.Rows[20][1].ToString());
                
                    dataRow["22"] = ShortDayName(dt.Rows[21][1].ToString());
                
                    dataRow["23"] = ShortDayName(dt.Rows[22][1].ToString());
                
                    dataRow["24"] = ShortDayName(dt.Rows[23][1].ToString());
                
                    dataRow["25"] = ShortDayName(dt.Rows[24][1].ToString());
                
                    dataRow["26"] = ShortDayName(dt.Rows[25][1].ToString());
                
                    dataRow["27"] = ShortDayName(dt.Rows[26][1].ToString());
                
                    dataRow["28"] = ShortDayName(dt.Rows[27][1].ToString());


                    if (dt.Rows.Count > 28)
                    {
                        dataRow["29"] = ShortDayName(dt.Rows[28][1].ToString());
                    }
                    if (dt.Rows.Count > 29)
                    {
                        dataRow["30"] = ShortDayName(dt.Rows[29][1].ToString());
                    }

                    if (dt.Rows.Count > 30)
                    {

                        dataRow["31"] = ShortDayName(dt.Rows[30][1].ToString());

                    }
                
                

                aDataTable.Rows.Add(dataRow);
            }
            catch (Exception)
            {

            }

            return aDataTable;
        }
    }
}
