﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class AttendenceBLL
    {
        AttendenceDAL attendenceDal=new AttendenceDAL();

        public bool SaveAttendence(List<Attendence> attendenceList)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                foreach (var attendence in attendenceList)
                {
                    attendence.AttendenceId = aClsPrimaryKeyFind.PrimaryKeyMax("AttendenceId", "tblAttendanceRecord", "HRDB");
                    attendenceDal.SaveAttendence(attendence);
                    
                }
                return true;                    
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }


        public DataTable LoadEmployee(string shiftId)
        {
            string toDay = System.DateTime.Today.ToString();
            return attendenceDal.LoadEmployee(shiftId,toDay);
        }
        public void LoadDeaprtment(DropDownList ddl)
        {
            attendenceDal.LoadDeaprtment(ddl);
        }

        public DataTable LoadShift(string shiftId)
        {
            return attendenceDal.LoadShift(shiftId);
        }

        public void LoadShift(DropDownList ddl)
        {
            attendenceDal.LoadShift(ddl);
        }
    }
}
