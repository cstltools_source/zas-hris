﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class CompanyInfoBLL
    {
        CompanyInfoDAL aCompanyInfoDal = new CompanyInfoDAL();
        public bool SaveCompanyInfoData(CompanyInfo aCompanyInfo)
        {
            try
            {
                if (!aCompanyInfoDal.HasCompanyName(aCompanyInfo))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aCompanyInfo.CompanyInfoId = aClsPrimaryKeyFind.PrimaryKeyMax("CompanyInfoId", "tblCompanyInfo","HRDB" );
                    aCompanyInfo.CompanyCode = CompanyCodeGenerator(aCompanyInfo.CompanyInfoId);
                    aCompanyInfoDal.SaveDataForCompanyInfo(aCompanyInfo);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public string CompanyCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "COMP-" + Id;
            return code;
        }
        public DataTable LoadCompanyInfoReport()
        {
            return aCompanyInfoDal.LoadCompanyInfoReport();
        }

        public bool UpdateDataForCompanyInfo(CompanyInfo aCompanyInfo)
        {
            return aCompanyInfoDal.UpdateCompanyInfo(aCompanyInfo);
        }

        public DataTable LoadCompanyInfo()
        {
            return aCompanyInfoDal.LoadCompanyInfo();
        }

        public CompanyInfo CompanyInfoEditLoad(string companyInfoId)
        {
            return aCompanyInfoDal.CompanyInfoEditLoad(companyInfoId);
        }
        public void AutoSalaryBLL()
        {

            aCompanyInfoDal.AutoSalaryDAL();

        }
    }
}
