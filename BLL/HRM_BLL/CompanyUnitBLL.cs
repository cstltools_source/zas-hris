﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class CompanyUnitBLL
    {
        CompanyUnitDAL aCompanyUnitDal = new CompanyUnitDAL();
        public bool SaveCompanyCompanyUnit(CompanyUnit aCompanyUnit)
        {
            try
            {
                if (!aCompanyUnitDal.HasCompanyUnitName(aCompanyUnit))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aCompanyUnit.UnitId = aClsPrimaryKeyFind.PrimaryKeyMax("UnitId", "tblCompanyUnit","HRDB");
                    aCompanyUnit.UnitCode = CompanyUnitCodeGenerator(aCompanyUnit.UnitId);
                    aCompanyUnitDal.SaveCompanyUnit(aCompanyUnit);
                    return true; 
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }


        public string CompanyUnitCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "UNIT-" + Id;
            return code;
        }

        public void LoadCompayName(DropDownList ddl)
        {
            CompanyUnitDAL aCompanyUnitDal = new CompanyUnitDAL();
            aCompanyUnitDal.LoadCompanyName(ddl);
        }

        public bool UpdateDataForCompanyUnit(CompanyUnit aCompanyUnit)
        {
            return aCompanyUnitDal.UpdateCompaniUnit(aCompanyUnit);
        }

        public DataTable LoadCompanyUnit()
        {
            return aCompanyUnitDal.LoadCompanyUnitView();
        }

        public CompanyUnit CompanyUnitEditLoad(string companyUnitId)
        {
            return aCompanyUnitDal.CompaniUnitEditLoad(companyUnitId);
        }

    }
}
