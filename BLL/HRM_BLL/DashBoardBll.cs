﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.HRM_DAL;

namespace BLL.HRM_BLL
{
    public class DashBoardBll
    {
        DashBoardDal aDashBoardDal = new DashBoardDal();
        public DataTable LoadCompanyUnitInfo()
        {
            return aDashBoardDal.GetCompanyUnitInfo();
        }

        public DataTable LoadEmployeeInfo()
        {
            return aDashBoardDal.GetEmployeeInfo();
        }

        public DataTable GetEmpAtt(string attdate)
        {
            return aDashBoardDal.GetEmpAtt(attdate);
        }
    }
}
