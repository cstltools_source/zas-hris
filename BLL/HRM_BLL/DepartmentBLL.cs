﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class DepartmentBLL
    {
        DepartmentDAL aDepartmentDal = new DepartmentDAL();
        public bool SaveDataForDepartment(Department aDepartment)
        {
            try
            {
                if (!aDepartmentDal.HasDeptName(aDepartment))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aDepartment.DepartmentId = aClsPrimaryKeyFind.PrimaryKeyMax("DeptId", "tblDepartment","HRDB");
                    aDepartment.DeaprtmentCode = DepartmentCodeGenerator(aDepartment.DepartmentId);
                    aDepartmentDal.SaveDepartmentInfo(aDepartment);
                    return true;
                }
                else
                {
                    return false;
                }  
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        
        public string DepartmentCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length==1)
            {
                Id = "00"+Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "DEPT-" + Id;
            return code;
        }

        public bool UpdateDataForDepartment(Department aDepartment)
        {
            return aDepartmentDal.UpdateDepartmentInfo(aDepartment);
        }
        public DataTable LoadDepartmentView()
        {
            return aDepartmentDal.LoadDepartmentView();
        }
        public Department DepartmentEditLoad(string departmentId)
        {
            return aDepartmentDal.DepartmentEditLoad(departmentId);
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            aDepartmentDal.LoadDivisionName(ddl);
        }
    }
}
