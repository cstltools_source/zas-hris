﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class DeptHeadSetupBLL
    {
        ClsCommonOperationDAL aCommonOperationDal=new ClsCommonOperationDAL();
        DeptHeadSetupDAL aDeptHeadSetupDal = new DeptHeadSetupDAL();
        public bool SaveDataForDeptHeadSetup(DeptHeadSetup aDeptHeadSetup)
        {
            try
            {
                if (!aDeptHeadSetupDal.HasDeptHeadSetup(aDeptHeadSetup))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aDeptHeadSetup.DHSId = aClsPrimaryKeyFind.PrimaryKeyMax("DHSId", "tblDeptHeadSetup", "HRDB");
                    aDeptHeadSetupDal.SaveDeptHeadSetup(aDeptHeadSetup);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aCommonOperationDal.CancelDataMark(aGridView,aDataTable);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aDeptHeadSetupDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aDeptHeadSetupDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeptHeadSetupDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeptHeadSetupDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aDeptHeadSetupDal.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeptHeadSetupDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aDeptHeadSetupDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeptHeadSetupDal.LoadSalGradeName(aDropDownList);
        }
        public void DeleteDeptHeadSetupBLL(string DHSId)
        {
           aDeptHeadSetupDal.DeleteDeptHeadSetup(DHSId);

        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeptHeadSetupDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(DeptHeadSetup aDeptHeadSetup)
        {
            return aDeptHeadSetupDal.UpdateDeptHeadSetup(aDeptHeadSetup);
        }
        public DataTable LoadDeptHeadSetup()
        {
            return aDeptHeadSetupDal.LoadDeptHeadSetupView();
        }
        public DeptHeadSetup DeptHeadSetupEditLoad(string DHSId)
        {
            return aDeptHeadSetupDal.DeptHeadSetupEditLoad(DHSId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aDeptHeadSetupDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aDeptHeadSetupDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aDeptHeadSetupDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aDeptHeadSetupDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aDeptHeadSetupDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aDeptHeadSetupDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aDeptHeadSetupDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aDeptHeadSetupDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aDeptHeadSetupDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aDeptHeadSetupDal.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(DeptHeadSetup aDeptHeadSetup)
        {
            return aDeptHeadSetupDal.ApprovalUpdateDAL(aDeptHeadSetup);
        }
        public DataTable LoadDeptHeadSetupViewForApproval()
        {
            return aDeptHeadSetupDal.LoadDeptHeadSetupViewForApproval();
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aDeptHeadSetupDal.PlaceEmpStatus(aEmpGeneralInfo);
        }
    }
}
