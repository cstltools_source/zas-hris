﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class DesignationBLL
    {
        DesignationDAL aDesignationDal = new DesignationDAL();
        public bool SaveDataForDesignation(Designation aDesignation)
        {
            try
            {
                if (!aDesignationDal.HasDesigName(aDesignation))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aDesignation.DesignationId = aClsPrimaryKeyFind.PrimaryKeyMax("DesigId", "tblDesignation","HRDB");
                    aDesignation.DesignaitonCode = DesignationCodeGenerator(aDesignation.DesignationId);
                    aDesignationDal.SaveDesignationInfo(aDesignation);
                    return true;

                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }


        public string DesignationCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "DESG-" + Id;
            return code;
        }
        public DataTable LoadDesignationView()
        {
            return aDesignationDal.LoadDesignationView();
        }
        public Designation DesignationEditLoad(string designationId)
        {
            return aDesignationDal.DesignationEditLoad(designationId);
        }
        public bool UpdateDataForDesignation(Designation aDesignation)
        {
            return aDesignationDal.UpdateDesignationInfo(aDesignation);
        }
        public bool DeleteDataForDesignation(string desigId)
        {
            return aDesignationDal.DeleteDesignationInfo(desigId);
        }
        public DataTable RptHeader()
        {
            return aDesignationDal.RptHeader();
        }
    }
}
