﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class DivisionBLL
    {
        DivisionDAL aDivisionDal = new DivisionDAL();
        public bool SaveDataForDivision(Division aDivision)
        {
            try
            {
                if (!aDivisionDal.HasDivisionName(aDivision))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aDivision.DivisionId = aClsPrimaryKeyFind.PrimaryKeyMax("DivisionId", "tblDivision", "HRDB");
                    aDivision.DivCode = DivisionCodeGenerator(aDivision.DivisionId);
                    aDivisionDal.SaveDevision(aDivision);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public string DivisionCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "000" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "00" + Id;
            }
            code = "DIV-" + Id;
            return code;
        }
        public bool UpdateaDataforDivision(Division aDivision)
        {
            return aDivisionDal.UpdateDivision(aDivision);
        }
        public DataTable LoadDataforDivisionView()
        {
            return aDivisionDal.LoadDivisionView();
        }
        public Division DivisionEditLoad(string DivisionId)
        {
            return aDivisionDal.DivisionEditLoad(DivisionId);
        }
    }
}
