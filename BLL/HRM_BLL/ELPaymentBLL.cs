﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
    public class ELPaymentBLL
    {
        ELPaymentDAL  aElPaymentDal=new ELPaymentDAL();
        public int ELPaymentProcess(string Year, int empcategoryId, string executeby, string dataBaseName)
        {
            return aElPaymentDal.ELPaymentProcess(Year, empcategoryId, executeby, dataBaseName);
        }
        public DataTable ELPaymentReport(string year, string parameter)
        {
            return aElPaymentDal.ELPaymentReport(year, parameter);
        }
    }
}
