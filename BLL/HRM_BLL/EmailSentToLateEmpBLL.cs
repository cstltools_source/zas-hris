﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;

namespace Library.BLL.HRM_BLL
{
    public class EmailSentToLateEmpBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        EmailSentToLateEmpDAL aEmailSentToLateEmpDal = new EmailSentToLateEmpDAL();

        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aEmailSentToLateEmpDal.LoadDivisionName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList)
        {
            aEmailSentToLateEmpDal.LoadUnitName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aEmailSentToLateEmpDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aEmailSentToLateEmpDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public DataTable LoadEmployeeBLL(string division, string department, string section)
        {
           return aEmailSentToLateEmpDal.LoadEmployee(division, department, section);
        }
        public DataTable LoadEmployeeATTBLL(DateTime date)
        {
            return aEmailSentToLateEmpDal.LoadEmployeeATT(date);
        }
        public DataTable LoadEmployeeATTAllBLL(DateTime date)
        {
            return aEmailSentToLateEmpDal.LoadEmployeeATTAllDAL(date);
        }
    }
}
