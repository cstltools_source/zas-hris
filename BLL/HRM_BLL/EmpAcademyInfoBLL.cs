﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class EmpAcademyInfoBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        EmpAcademyInfoDAL aAcademyInfoDal = new EmpAcademyInfoDAL();
        public bool SaveDataForAcademicInfo(AreaOfStaudy aAreaOfStaudy)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aAreaOfStaudy.StudyId = aClsPrimaryKeyFind.PrimaryKeyMax("StudyId", "tblAreaofStudy", "HRDB");
                aAcademyInfoDal.SaveDataForAcademicInfo(aAreaOfStaudy);
                return true;
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool SaveEmpEduInstitute(EmpEduInstitute aEmpEduInstitute)
        {
            try
            {
               ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
               aEmpEduInstitute.EduInstituteId = aClsPrimaryKeyFind.PrimaryKeyMax("EduInstituteId", "tblEduInstitute", "HRDB");
               aAcademyInfoDal.SaveEmpEduInstitute(aEmpEduInstitute);

               return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool SaveDataForEmpExamp(EmpExam aEmpExam)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aEmpExam.ExamId = aClsPrimaryKeyFind.PrimaryKeyMax("ExamId", "tblExam", "HRDB");
                aAcademyInfoDal.SaveDataForEmpExamp(aEmpExam);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool SaveDataForEmpQualification(EmpQualification aQualification)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aQualification.QualificationId = aClsPrimaryKeyFind.PrimaryKeyMax("QualificationId", "tblQualification", "HRDB");
                aAcademyInfoDal.SaveDataForEmpQualification(aQualification);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        
        //public void EmpApprovalBLL(string EmpInfoId, string joiningDate, string AppUser, DateTime appDate)
        //{
        //    aGeneralInfoDal.EmpApprovalDAL(EmpInfoId, joiningDate, AppUser, appDate);
        //}

        //public void LoadDesignationToDropDownBLL(DropDownList aDropDownList,string gradeId)
        //{
        //    aGeneralInfoDal.LoadDesignationName(aDropDownList,gradeId);
        //}
        //public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        //{
        //    aGeneralInfoDal.LoadDepartmentName(aDropDownList, divisionId);
        //}
        //public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        //{
        //    aGeneralInfoDal.LoadEmployeeName(aDropDownList);
        //}
        //public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        //{
        //    aGeneralInfoDal.LoadCompanyName(aDropDownList);
        //}
        
        //public void LoadBoardName(DropDownList ddl)
        //{
        //    aGeneralInfoDal.LoadBoardName(ddl);
        //}

        //public void LoadQualificationName(DropDownList ddl)
        //{
        //    aGeneralInfoDal.LoadQualificationName(ddl);
        //}

        //public void LoadExam(DropDownList ddl)
        //{
        //    aGeneralInfoDal.LoadExam(ddl);
        //}

        //public void LoadAreaStudy(DropDownList ddl)
        //{
        //    aGeneralInfoDal.LoadAreaStudy(ddl);
        //}
    }
}
