﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class EmpGeneralInfoBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        EmpGeneralInfoDAL aGeneralInfoDal = new EmpGeneralInfoDAL();
        public bool SaveDataFoEmployeeInfo(EmpGeneralInfo aGeneralInfo, out int empid)
        {
            try
            {
                if (aGeneralInfo.IsSameData==true)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aGeneralInfo.EmpInfoId = aClsPrimaryKeyFind.PrimaryKeyMax("EmpInfoId", "tblEmpGeneralInfo", "HRDB");
                    //aGeneralInfo.EmpMasterCode = EmpMasterCodeGenerator(aGeneralInfo.EmpInfoId);
                    //aGeneralInfo.EmpMasterCode = aGeneralInfoDal.EmpMasterCodeGeneratorDAL();
                    aGeneralInfo.EmpMasterCode = aGeneralInfo.CardNo;
                    empid = (int) aGeneralInfo.EmpInfoId;
                    aGeneralInfoDal.SaveEmployeeInfo(aGeneralInfo);
                    return true;
                }
                else
                {
                    if (!aGeneralInfoDal.HasEmpGeneralInfo(aGeneralInfo))
                    {
                        ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                        aGeneralInfo.EmpInfoId = aClsPrimaryKeyFind.PrimaryKeyMax("EmpInfoId", "tblEmpGeneralInfo", "HRDB");
                        //aGeneralInfo.EmpMasterCode = EmpMasterCodeGenerator(aGeneralInfo.EmpInfoId);
                        //aGeneralInfo.EmpMasterCode = aGeneralInfoDal.EmpMasterCodeGeneratorDAL();
                        aGeneralInfo.EmpMasterCode = aGeneralInfo.CardNo;
                        empid = (int)aGeneralInfo.EmpInfoId;
                        aGeneralInfoDal.SaveEmployeeInfo(aGeneralInfo);
                        return true;
                    }
                    else
                    {
                        empid = 0;
                        return false;
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool SaveEmpEducationInfo(List<EmpEducationInfo> aEducationInfoList)
        {
            try
            {
                foreach (var aUniversityInfo in aEducationInfoList)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aUniversityInfo.EmpEduId = aClsPrimaryKeyFind.PrimaryKeyMax("EmpEduId", "tblEmpEducation", "HRDB");
                    aGeneralInfoDal.SaveEmpEducationInfo(aUniversityInfo);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool SaveEmpJobExperianceInfo(List<JobExperiancInfo> aExperiancInfoList)
        {
            try
            {
                foreach (var aExperiancInfo in aExperiancInfoList)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aExperiancInfo.JobExpId = aClsPrimaryKeyFind.PrimaryKeyMax("JobExpId", "tblJobExperianc", "HRDB");
                    aGeneralInfoDal.SaveEmpJobExperianceInfo(aExperiancInfo);    
                }

                
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool SaveEmpTreningInfo(List<EmpTrainingInfo> aTrainingInfoList)
        {
            try
            {
                foreach (var aTrainingInfo in aTrainingInfoList)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aTrainingInfo.TrainingId = aClsPrimaryKeyFind.PrimaryKeyMax("TraningId", "tblTraining", "HRDB");
                    aGeneralInfoDal.SaveEmpTreningInfo(aTrainingInfo);
                }

                
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public string EmpMasterCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "0000" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "000" + Id;
            }
            if (Id.Length == 3)
            {
                Id = "00" + Id;
            }
            code = "" + Id;
            return code;
        }

        public DataTable CheckCardNo(string cardno)
        {
            return aGeneralInfoDal.CheckCardNo(cardno);
        }

        public void EmpApprovalBLL(string EmpInfoId, string joiningDate, string AppUser, DateTime appDate,string actionstatus)
        {
            aGeneralInfoDal.EmpApprovalDAL(EmpInfoId, joiningDate, AppUser, appDate,actionstatus);
        }
        public bool DeleteEmpJobExperianceInfo(JobExperiancInfo aExperiancInfo)
        {
            return aGeneralInfoDal.DeleteEmpJobExperianceInfo(aExperiancInfo);
        }
        public bool DeleteEmpEducationInfo(EmpEducationInfo aEmpEducationInfo)
        {
            return aGeneralInfoDal.DeleteEmpEducationInfo(aEmpEducationInfo);
        }
        public bool DeleteEmpTrainingeInfo(EmpTrainingInfo aTrainingInfo)
        {
            return aGeneralInfoDal.DeleteEmpTrainingeInfo(aTrainingInfo);
        }

        public void LoadCountry(DropDownList ddl)
        {
            aGeneralInfoDal.LoadCountry(ddl);
        }

        public DataTable LoadEucation(string empid)
        {
            return aGeneralInfoDal.LoadEucation(empid);
        }
        public DataTable EmployeeTinReport(string parameter)
        {
            return aGeneralInfoDal.EmployeeTinReport(parameter);
        }
        public DataTable LoadJobExp(string empid)
        {
            return aGeneralInfoDal.LoadJobExp(empid);
        }

        public DataTable LoadTraining(string empid)
        {
            return aGeneralInfoDal.LoadTraining(empid);
        }

        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList,string gradeId)
        {
            aGeneralInfoDal.LoadDesignationName(aDropDownList,gradeId);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aGeneralInfoDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aGeneralInfoDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aGeneralInfoDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aGeneralInfoDal.LoadUnitName(aDropDownList, companyId);
        }

        public DataTable LoadCompany(string id)
        {
            return aGeneralInfoDal.LoadCompany(id);
        }

        public void LoadUnitNameAll(DropDownList ddl)
        {
            aGeneralInfoDal.LoadUnitNameAll(ddl);
        }
        public void LoadUnitNameAllByUser(DropDownList ddl)
        {
            aGeneralInfoDal.LoadUnitNameAllByUser(ddl);
        }
        public void LoadEmpCategoryBLL(DropDownList aDropDownList)
        {
            aGeneralInfoDal.LoadEmpCategory(aDropDownList);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aGeneralInfoDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aGeneralInfoDal.LoadSectionName(aDropDownList, DeptId);
        }

        public void LoadSalaryScaleNameToDropDownBLL(DropDownList aDropDownList)
        {
            aGeneralInfoDal.LoadSalaryScaleName(aDropDownList);
        }
        public void LoadGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aGeneralInfoDal.LoadGradeName(aDropDownList);
        }
        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aGeneralInfoDal.LoadEmpTypeName(aDropDownList);
        }
        public void LoadBankName(DropDownList ddl)
        {
            aGeneralInfoDal.LoadBankName(ddl);
        }
        public void LoadShift(DropDownList ddl)
        {
            aGeneralInfoDal.LoadShift(ddl);
        }
        public bool UpdateDataForEmpGeneralInfo(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aGeneralInfoDal.UpdateEmployeeInfo(aEmpGeneralInfo);
        }
        public DataTable RptHeader()
        {
            return aGeneralInfoDal.RptHeader();
        }
        public DataTable LoadEmpGeneralInfo()
        {
            return aGeneralInfoDal.LoadEmployeeView();
        }
        public DataTable LoadEmpGeneralInfoDeptWise(string deptId, string companyId)
        {
            return aGeneralInfoDal.LoadEmployeeViewDeptWise(deptId,companyId);
        }
        public DataTable LoadEmpGeneralInfoDeptWise()
        {
            return aGeneralInfoDal.LoadEmployeeViewDeptWise();
        }
        public DataTable LoadEmpGeneralInfoDeptWiseForApproval(string param)
        {
            return aGeneralInfoDal.LoadEmployeeViewDeptWiseForApproval(param);
        }
        public DataTable EmployeeReport(string parameter)
        {
            return aGeneralInfoDal.EmployeeReport(parameter);
        }

        public DataTable EmployeeReportJobleft(string parameter)
        {
            return aGeneralInfoDal.EmployeeReportJobleft(parameter);
        }

        public DataTable ProbationaryEmployeeList(string parameter)
        {
            return aGeneralInfoDal.ProbationaryEmployeeList(parameter);
        }
        public DataTable EmployeeDynamicReport(string whereparameter, string columnparameter)
        {
            return aGeneralInfoDal.EmployeeDynamicReport(whereparameter, columnparameter);
        }

        public DataTable EmpDynamicReport()
        {
            DataTable aDataTable=new DataTable();
            aDataTable.Columns.Add("Column1");
            aDataTable.Columns.Add("Column2");
            aDataTable.Columns.Add("Column3");
            aDataTable.Columns.Add("Column4");
            aDataTable.Columns.Add("Column5");
            aDataTable.Columns.Add("Column6");
            aDataTable.Columns.Add("Column7");
            aDataTable.Columns.Add("Column8");
            aDataTable.Columns.Add("Column9");
            aDataTable.Columns.Add("Column10");
            aDataTable.Columns.Add("Column11");
            aDataTable.Columns.Add("Column12");
            aDataTable.Columns.Add("Column13");
            aDataTable.Columns.Add("Column14");


            DataRow dataRow = null;

            return aDataTable;
        }

        public DataTable EmployeeReportactive(string parameter)
        {
            return aGeneralInfoDal.EmployeeReportactive(parameter);
        }
        public DataTable EmployeeReportInactive(string parameter)
        {
            return aGeneralInfoDal.EmployeeReportInactive(parameter);
        }
        public DataTable EmployeeReportPosted(string parameter)
        {
            return aGeneralInfoDal.EmployeeReportPosted(parameter);
        }
        public DataTable EmpSalaryInfo(string parameter)
        {
            return aGeneralInfoDal.EmpSalaryInfo(parameter);
        }
        public DataTable EmpEduInfo(string parameter)
        {
            return aGeneralInfoDal.EmpEduInfo(parameter);
        }
        public DataTable EmpJobInfo(string parameter)
        {
            return aGeneralInfoDal.EmpJobInfo(parameter);
        }
        public DataTable EmpTrainInfo(string parameter)
        {
            return aGeneralInfoDal.EmpTrainInfo(parameter);
        }
        public EmpGeneralInfo EmpGeneralInfoEditLoad(string EmpInfoId)
        {
            return aGeneralInfoDal.EmpInfoEditLoad(EmpInfoId);
        }
        public bool UpdateEmpImage(EmpGeneralInfo aGeneralInfo)
        {
            return aGeneralInfoDal.UpdateEmpImage(aGeneralInfo);
        }
        public List<EmpGeneralInfo> GetEmployeeName(string EmpInfoId)
        {
            return aGeneralInfoDal.ViewEmpName(EmpInfoId);
        }
        public DataTable LoadEmployeeReportViewAll(string deptId)
        {
            return aGeneralInfoDal.LoadEmployeeReportViewAll(deptId);
        }
        public DataTable LoadEmployeeReportView(string EmpMasterCode, string EmpInfoId)
        {
            return aGeneralInfoDal.LoadEmployeeReportView(EmpMasterCode, EmpInfoId);
        }
        public DataTable LoadEmployee(string EmpMasterCode)
        {
            return aGeneralInfoDal.LoadEmployee(EmpMasterCode);
        }
        public DataTable searchID(string EmpMasterCode)
        {
            return aGeneralInfoDal.searchID(EmpMasterCode);
        }

        public DataTable searchWithEmpID(string EmpMasterCode)
        {
            return aGeneralInfoDal.searchWithEmpID(EmpMasterCode);
        }

        public bool UpdateWeeklyHoliday(WeeklyHoliday aWeeklyHoliday)
        {
            return aGeneralInfoDal.UpdateWeeklyHoliday(aWeeklyHoliday);
        }
        public DataTable LoadWeeklyHoliday(string empid)
        {
            return aGeneralInfoDal.LoadWeeklyHoliday(empid);
        }

        public void LoadBoardName(DropDownList ddl)
        {
            aGeneralInfoDal.LoadBoardName(ddl);
        }

        public void LoadQualificationName(DropDownList ddl)
        {
            aGeneralInfoDal.LoadQualificationName(ddl);
        }

        public void LoadExam(DropDownList ddl)
        {
            aGeneralInfoDal.LoadExam(ddl);
        }

        public void LoadAreaStudy(DropDownList ddl)
        {
            aGeneralInfoDal.LoadAreaStudy(ddl);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aGeneralInfoDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aGeneralInfoDal.LoadForApprovalConditionDAL(pageName, userName);
        }

        public void LoadRegion(DropDownList ddl, string unitId)
        {
            aGeneralInfoDal.LoadRegion(ddl,unitId);
        }


        public void LoadRegionWithoutComapny(DropDownList ddl)
        {
            aGeneralInfoDal.GetddlregionWithoutCompany(ddl);
        }

        public void LoadAreaDropDownListWithoutCompany(DropDownList ddl, string regionId)
        {
            aGeneralInfoDal.GetAreaDropdownListWithoutCompany(ddl, regionId);
        }

        public void LoadAreaDropDownList(DropDownList ddl, string regionId, string companyId)
        {
            aGeneralInfoDal.GetAreaDropdownList(ddl, regionId,companyId);
        }

        public void LoadTerritoryDropDownList(DropDownList ddl, string areaId, string companyId)
        {
            aGeneralInfoDal.GetTerritoryDropdownList(ddl, areaId, companyId);
        }

        public void GetTerritoryDropdownListWithoutCompany(DropDownList ddl, string areaId)
        {
            aGeneralInfoDal.GetTerritoryDropdownList(ddl, areaId);
        }

        public void LoadJobLocation(DropDownList ddl)
        {
            aGeneralInfoDal.LoadJobLocation(ddl);
        }
    }
}
