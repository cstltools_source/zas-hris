﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class EmpGradeBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        EmployeeGradeDAL _aEmployeeGradeDal = new EmployeeGradeDAL();
        public bool SaveDataForDepartment(EmployeeGrade aEmployeeGrade)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aEmployeeGrade.GradeId = aClsPrimaryKeyFind.PrimaryKeyMax("GradeId", "dbo.tblEmployeeGrade", "HRDB");
                aEmployeeGrade.GradeCode = GradeCodeGenerator(aEmployeeGrade.GradeId);
                _aEmployeeGradeDal.SaveGradeInfo(aEmployeeGrade);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        
        public string GradeCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "GRADE-" + Id;
            return code;
        }

        public DataTable LoadGradeView()
        {
            return _aEmployeeGradeDal.LoadGeadeView();
        }

        public EmployeeGrade GradeEditLoad(string gradeId)
        {
            return _aEmployeeGradeDal.GradeEditLoad(gradeId);
        }

        public bool UpdateDataForGrade(EmployeeGrade aEmployeeGrade)
        {
            return _aEmployeeGradeDal.UpdateGradeInfo(aEmployeeGrade);
        }

    }
}
