﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
   public class EmpImageBLL
   {
       EmpImageDAL aEmpImageDal=new EmpImageDAL();
       public bool SaveEmpImage(byte[] imagebyt, byte[] sigimagebyt, int empid)
       {
           aEmpImageDal.DeleteData(empid);
           return aEmpImageDal.SaveEmpImage(imagebyt, sigimagebyt, empid);

       }
       
   }
}
