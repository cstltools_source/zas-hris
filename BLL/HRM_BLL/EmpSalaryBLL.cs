﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class EmpSalaryBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        EmpSalaryDAL aEmpSalaryDal = new EmpSalaryDAL();
        ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

        public bool CheckNumaric(string value)
        {
            try
            {
                double nuValue = Convert.ToDouble(value.Trim());
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
        public bool CheckValidation(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            return true;
        }
        public DataTable Calculate(string gradetype, decimal grossSalary)
        {
            DataTable dtsalarycalculate = new DataTable();
            dtsalarycalculate.Columns.Add("SalaryHeadId");
            dtsalarycalculate.Columns.Add("SalaryHead");
            dtsalarycalculate.Columns.Add("Amount");

            DataRow dataRow;
            
            DataTable dtSalaryRules = aEmpSalaryDal.SalaryRule(gradetype);
            
            decimal gross = Convert.ToDecimal(grossSalary);
            decimal basic = 0;
            decimal medical = 0;
            decimal houserent = 0;
            decimal conveyance = 0;

            string basicRule = "";
            string medicalRule = "";
            string houserentRule = "";
            string conveyanceRule = "";
            string lunchAllowanceRule = "";


            string basicValue = "0";
            string medicalValue = "0";
            string houserentValue = "0";
            string conveyanceValue = "0";
            string lunchAllowanceValue = "0";
            
            DataTable basicRuleRow = (from b in dtSalaryRules.AsEnumerable()
                                      where b.Field<string>("SHName") == "Basic"
                                      select b).CopyToDataTable();
            DataTable medicalRuleRow = (from b in dtSalaryRules.AsEnumerable()
                                        where b.Field<string>("SHName") == "Medical"
                                        select b).CopyToDataTable();
            DataTable houserentRuleRow = (from b in dtSalaryRules.AsEnumerable()
                                          where b.Field<string>("SHName") == "House Rent"
                                          select b).CopyToDataTable();
            
            basicRule = basicRuleRow.Rows[0]["RuleDefinition"].ToString();
            medicalRule = medicalRuleRow.Rows[0]["RuleDefinition"].ToString();
            houserentRule = houserentRuleRow.Rows[0]["RuleDefinition"].ToString();


            if (gradetype == "WorkerGrade" || gradetype == "Trainee")
            {
                DataTable conveyanceRuleRow = (from b in dtSalaryRules.AsEnumerable()
                                               where b.Field<string>("SHName") == "Conveyance Allowance"
                                               select b).CopyToDataTable();
                conveyanceRule = conveyanceRuleRow.Rows[0]["RuleDefinition"].ToString();
                DataTable lunchAllowanceRow = (from b in dtSalaryRules.AsEnumerable()
                                               where b.Field<string>("SHName") == "Lunch Allowance"
                                               select b).CopyToDataTable();
                lunchAllowanceRule = lunchAllowanceRow.Rows[0]["RuleDefinition"].ToString();
                if (!string.IsNullOrEmpty(basicRule))
                {
                    if (basicRule.Contains("[Gross]"))
                    {
                        basicRule = basicRule.Replace("[Gross]", gross.ToString());
                        string basicRuleCalc = basicRule;

                        basicRule = basicRule.Substring(1);
                        int indexOfPertition = basicRule.IndexOf(")");

                        basicRule = basicRule.Substring(0, indexOfPertition);

                        string[] basicRuleArray = basicRule.Split('-');

                        string rst = basicRuleCalc.Substring(basicRuleCalc.IndexOf("/") + 1);
                        basic = (Convert.ToDecimal(basicRuleArray[0]) - Convert.ToDecimal(basicRuleArray[1])) / Convert.ToDecimal(rst);
                        
                        basicValue = decimal.Round(basic, 0).ToString();
                        dataRow = dtsalarycalculate.NewRow();

                        dataRow["SalaryHeadId"] = "1";
                        dataRow["SalaryHead"] = "Basic";
                        dataRow["Amount"] = basicValue.ToString();
                        
                        dtsalarycalculate.Rows.Add(dataRow);
                    }
                }
                if (!string.IsNullOrEmpty(medicalRule))
                {
                    medicalValue = medicalRuleRow.Rows[0]["RuleDefinition"].ToString();
                    dataRow = dtsalarycalculate.NewRow();

                    dataRow["SalaryHeadId"] = "4";
                    dataRow["SalaryHead"] = "Medical";
                    dataRow["Amount"] = medicalValue.ToString();
                    
                    dtsalarycalculate.Rows.Add(dataRow);
                }
                if (!string.IsNullOrEmpty(houserentRule))
                {
                    if (houserentRule.Contains("[Basic]"))
                    {
                        houserentRule = houserentRule.Replace("[Basic]", basicValue);
                        int indexofhouserent = houserentRule.IndexOf(")");
                        houserentRule = houserentRule.Substring(1, indexofhouserent - 1);

                        string[] houseArray = houserentRule.Split('*');
                        houserent = (Convert.ToDecimal(houseArray[0]) * Convert.ToDecimal(houseArray[1]));
                        
                        houserentValue = decimal.Round(houserent, 0).ToString();
                        dataRow = dtsalarycalculate.NewRow();
                        dataRow["SalaryHeadId"] = "2";
                        dataRow["SalaryHead"] = "House Rent";
                        dataRow["Amount"] = houserentValue.ToString();
                        
                        dtsalarycalculate.Rows.Add(dataRow);
                    }

                }
                if (!string.IsNullOrEmpty(conveyanceRule))
                {

                    conveyanceValue = conveyanceRuleRow.Rows[0]["RuleDefinition"].ToString();
                    dataRow = dtsalarycalculate.NewRow();
                    dataRow["SalaryHeadId"] = "3";
                    dataRow["SalaryHead"] = "Conveyance";
                    dataRow["Amount"] = conveyanceRule.ToString();
                    
                    dtsalarycalculate.Rows.Add(dataRow);
                }

                if (!string.IsNullOrEmpty(lunchAllowanceRule))
                {

                    lunchAllowanceValue = lunchAllowanceRow.Rows[0]["RuleDefinition"].ToString();
                    dataRow = dtsalarycalculate.NewRow();
                    dataRow["SalaryHeadId"] = "5";
                    dataRow["SalaryHead"] = "Lunch Allowance";
                    dataRow["Amount"] = lunchAllowanceValue.ToString();
                    
                    dtsalarycalculate.Rows.Add(dataRow);
                }
            }
            //if (gradetype == "DirectorGrade" || gradetype == "GMGrade" || gradetype == "ManagerGrade" || gradetype == "OfficerGrade" || gradetype == "SupervisorGrade"
            //    || gradetype == "SalaryGrade")
            //{
            //    DataTable conveyanceRuleRow = (from b in dtSalaryRules.AsEnumerable()
            //                                   where b.Field<string>("SHName") == "Conveyance Allowance"
            //                                   select b).CopyToDataTable();
            //    conveyanceRule = conveyanceRuleRow.Rows[0]["RuleDefinition"].ToString();
            //    if (!string.IsNullOrEmpty(basicRule))
            //    {
            //        if (basicRule.Contains("[Gross]"))
            //        {
            //            basicRule = basicRule.Replace("[Gross]", gross.ToString());
            //            int indexofbasic = basicRule.IndexOf(")");
            //            basicRule = basicRule.Substring(1, indexofbasic - 1);
            //            string[] basicarray = basicRule.Split('*');
            //            basic = (Convert.ToDecimal(basicarray[0]) * Convert.ToDecimal(basicarray[1]));
            //            basicValue = decimal.Round(basic, 0).ToString();

            //            dataRow = dtsalarycalculate.NewRow();
            //            dataRow["SalaryHeadId"] = "1";
            //            dataRow["SalaryHead"] = "Basic";
            //            dataRow["Amount"] = basicValue.ToString();
                        
            //            dtsalarycalculate.Rows.Add(dataRow);
            //        }
            //    }
            //    if (!string.IsNullOrEmpty(conveyanceRule))
            //    {

            //        if (conveyanceRule.Contains("[Gross]"))
            //        {
            //            conveyanceRule = conveyanceRule.Replace("[Gross]", gross.ToString());
            //            int indexofconveyance = conveyanceRule.IndexOf(")");
            //            conveyanceRule = conveyanceRule.Substring(1, indexofconveyance - 1);
            //            string[] conveyancearray = conveyanceRule.Split('*');
            //            conveyance = (Convert.ToDecimal(conveyancearray[0]) * Convert.ToDecimal(conveyancearray[1]));
            //            conveyanceValue = decimal.Round(conveyance, 0).ToString();
            //            dataRow = dtsalarycalculate.NewRow();
            //            dataRow["SalaryHeadId"] = "3";
            //            dataRow["SalaryHead"] = "Conveyance";
            //            dataRow["Amount"] = conveyanceValue.ToString();
                        
            //            dtsalarycalculate.Rows.Add(dataRow);
            //        }

            //    }
            //    if (!string.IsNullOrEmpty(houserentRule))
            //    {
            //        if (houserentRule.Contains("[Gross]"))
            //        {
            //            houserentRule = houserentRule.Replace("[Gross]", gross.ToString());
            //            int indexofhouserent = houserentRule.IndexOf(")");
            //            houserentRule = houserentRule.Substring(1, indexofhouserent - 1);
            //            string[] houseArray = houserentRule.Split('*');

            //            houserent = (Convert.ToDecimal(houseArray[0]) * Convert.ToDecimal(houseArray[1]));
            //            houserentValue = decimal.Round(houserent, 0).ToString();
            //            dataRow = dtsalarycalculate.NewRow();
            //            dataRow["SalaryHeadId"] = "2";
            //            dataRow["SalaryHead"] = "House Rent";
            //            dataRow["Amount"] = houserentValue.ToString();

            //            dtsalarycalculate.Rows.Add(dataRow);
            //        }

            //    }
            //    if (!string.IsNullOrEmpty(medicalRule))
            //    {
            //        if (medicalRule.Contains("[Gross]"))
            //        {
            //            medicalRule = medicalRule.Replace("[Gross]", gross.ToString());
            //            int indexofmedical = medicalRule.IndexOf(")");
            //            medicalRule = medicalRule.Substring(1, indexofmedical - 1);
            //            string[] medicalArray = medicalRule.Split('*');

            //            medical = (Convert.ToDecimal(medicalArray[0]) * Convert.ToDecimal(medicalArray[1]));
            //            medicalValue = decimal.Round(medical, 0).ToString();
            //            dataRow = dtsalarycalculate.NewRow();
            //            dataRow["SalaryHeadId"] = "4";
            //            dataRow["SalaryHead"] = "Medical";
            //            dataRow["Amount"] = medicalValue.ToString();
                        
            //            dtsalarycalculate.Rows.Add(dataRow);
            //        }
            //    }
            //}
            if (gradetype == "DirectorGrade" || gradetype == "GMGrade" || gradetype == "ManagerGrade" || gradetype == "OfficerGrade" || gradetype == "SupervisorGrade"
                || gradetype == "SalaryGrade")
            {
                DataTable conveyanceRuleRow = (from b in dtSalaryRules.AsEnumerable()
                                               where b.Field<string>("SHName") == "Conveyance Allowance"
                                               select b).CopyToDataTable();
                conveyanceRule = conveyanceRuleRow.Rows[0]["RuleDefinition"].ToString();
                if (!string.IsNullOrEmpty(basicRule))
                {
                    if (basicRule.Contains("[Gross]"))
                    {
                        basicRule = basicRule.Replace("[Gross]", gross.ToString());
                        int indexofbasic = basicRule.IndexOf(")");
                        basicRule = basicRule.Substring(1, indexofbasic - 1);
                        string[] basicarray = basicRule.Split('*');
                        basic = (Convert.ToDecimal(basicarray[0]) * Convert.ToDecimal(basicarray[1]));
                        basicValue = decimal.Round(basic, 0).ToString();

                        dataRow = dtsalarycalculate.NewRow();
                        dataRow["SalaryHeadId"] = "1";
                        dataRow["SalaryHead"] = "Basic";
                        dataRow["Amount"] = basicValue.ToString();

                        dtsalarycalculate.Rows.Add(dataRow);
                    }
                }
                if (!string.IsNullOrEmpty(conveyanceRule))
                {

                    if (conveyanceRule.Contains("[Basic]"))
                    {
                        conveyanceRule = conveyanceRule.Replace("[Basic]", basic.ToString());
                        int indexofconveyance = conveyanceRule.IndexOf(")");
                        conveyanceRule = conveyanceRule.Substring(1, indexofconveyance - 1);
                        string[] conveyancearray = conveyanceRule.Split('*');
                        conveyance = (Convert.ToDecimal(conveyancearray[0]) * Convert.ToDecimal(conveyancearray[1]));
                        conveyanceValue = decimal.Round(conveyance, 0).ToString();
                        dataRow = dtsalarycalculate.NewRow();
                        dataRow["SalaryHeadId"] = "3";
                        dataRow["SalaryHead"] = "Conveyance";
                        dataRow["Amount"] = conveyanceValue.ToString();

                        dtsalarycalculate.Rows.Add(dataRow);
                    }

                }
                if (!string.IsNullOrEmpty(houserentRule))
                {
                    if (houserentRule.Contains("[Basic]"))
                    {
                        houserentRule = houserentRule.Replace("[Basic]", basic.ToString());
                        int indexofhouserent = houserentRule.IndexOf(")");
                        houserentRule = houserentRule.Substring(1, indexofhouserent - 1);
                        string[] houseArray = houserentRule.Split('*');

                        houserent = (Convert.ToDecimal(houseArray[0]) * Convert.ToDecimal(houseArray[1]));
                        houserentValue = decimal.Round(houserent, 0).ToString();
                        dataRow = dtsalarycalculate.NewRow();
                        dataRow["SalaryHeadId"] = "2";
                        dataRow["SalaryHead"] = "House Rent";
                        dataRow["Amount"] = houserentValue.ToString();

                        dtsalarycalculate.Rows.Add(dataRow);
                    }

                }
                if (!string.IsNullOrEmpty(medicalRule))
                {
                    if (medicalRule.Contains("[Basic]"))
                    {
                        medicalRule = medicalRule.Replace("[Basic]", basic.ToString());
                        int indexofmedical = medicalRule.IndexOf(")");
                        medicalRule = medicalRule.Substring(1, indexofmedical - 1);
                        string[] medicalArray = medicalRule.Split('*');

                        medical = (Convert.ToDecimal(medicalArray[0]) * Convert.ToDecimal(medicalArray[1]));
                        medicalValue = decimal.Round(medical, 0).ToString();
                        dataRow = dtsalarycalculate.NewRow();
                        dataRow["SalaryHeadId"] = "4";
                        dataRow["SalaryHead"] = "Medical";
                        dataRow["Amount"] = medicalValue.ToString();

                        dtsalarycalculate.Rows.Add(dataRow);
                    }
                }
            }
            if (gradetype == "DriverGrade")
            {
                if (!string.IsNullOrEmpty(basicRule))
                {
                    if (basicRule.Contains("[Gross]") && basicRule.Contains("[Medical]"))
                    {
                        basicRule = basicRule.Replace("[Gross]", gross.ToString());
                        basicRule = basicRule.Replace("[Medical]", medicalRule);
                        string basicRuleCalc = basicRule;
                        basicRule = basicRule.Substring(1);

                        int indexofbasic = basicRule.IndexOf(")");
                        basicRule = basicRule.Substring(0, indexofbasic);
                        string rst = basicRuleCalc.Substring(basicRuleCalc.IndexOf("/") + 1);
                        string[] basicarray = basicRule.Split('-');
                        basic = (Convert.ToDecimal(basicarray[0]) - Convert.ToDecimal(basicarray[1])) / Convert.ToDecimal(rst);
                        basicValue = decimal.Round(basic, 0).ToString();
                        dataRow = dtsalarycalculate.NewRow();
                        dataRow["SalaryHeadId"] = "1";
                        dataRow["SalaryHead"] = "Basic";
                        dataRow["Amount"] = basicValue.ToString();
                        
                        dtsalarycalculate.Rows.Add(dataRow);
                    }

                }
                if (!string.IsNullOrEmpty(houserentRule))
                {
                    if (houserentRule.Contains("[Gross]") && houserentRule.Contains("[Basic]") && houserentRule.Contains("[Medical]"))
                    {
                        houserentRule = houserentRule.Replace("[Gross]", gross.ToString());
                        houserentRule = houserentRule.Replace("[Basic]", basicValue);
                        houserentRule = houserentRule.Replace("[Medical]", medicalRule);
                        string houserentRuleCalc = houserentRule;
                        int indexofhouserent = houserentRule.IndexOf(")");
                        houserentRule = houserentRule.Substring(1, indexofhouserent - 1);
                        string[] houserentarray = houserentRule.Split('-');
                        houserent = (Convert.ToDecimal(houserentarray[0]) - Convert.ToDecimal(houserentarray[1]) - Convert.ToDecimal(houserentarray[2]));
                        houserentValue = decimal.Round(houserent, 0).ToString();
                        dataRow = dtsalarycalculate.NewRow();
                        dataRow["SalaryHeadId"] = "2";
                        dataRow["SalaryHead"] = "House Rent";
                        dataRow["Amount"] = houserentValue.ToString();
                        
                        dtsalarycalculate.Rows.Add(dataRow);

                    }

                }
                if (!string.IsNullOrEmpty(medicalRule))
                {
                    medicalValue = medicalRuleRow.Rows[0]["RuleDefinition"].ToString();
                    dataRow = dtsalarycalculate.NewRow();
                    dataRow["SalaryHeadId"] = "4";
                    dataRow["SalaryHead"] = "Medical";
                    dataRow["Amount"] = medicalValue.ToString();
                    
                    dtsalarycalculate.Rows.Add(dataRow);
                }

            }
            decimal others = 0;
            others = gross - Convert.ToDecimal(basicValue) - Convert.ToDecimal(houserentValue) - Convert.ToDecimal(medicalValue) - Convert.ToDecimal(conveyanceValue);
            dataRow = dtsalarycalculate.NewRow();

            dataRow["SalaryHeadId"] = "6";
            dataRow["SalaryHead"] = "Other";
            dataRow["Amount"] = others.ToString();

            dtsalarycalculate.Rows.Add(dataRow);



            dataRow = dtsalarycalculate.NewRow();

            dataRow["SalaryHeadId"] = "7";
            dataRow["SalaryHead"] = "Gross";
            dataRow["Amount"] = gross.ToString();

            dtsalarycalculate.Rows.Add(dataRow);

            return dtsalarycalculate;
        }

        //public bool UpDateEmpInformationForPromotion(EmployeeSalaryHistory aSalaryHistory)
        //{
        //    return aEmpSalaryDal.UpdateEmpInfoForPromotion(aSalaryHistory);
        //}
        public DataTable SalaryRule(string ruleName)
        {
            return aEmpSalaryDal.SalaryRule(ruleName);
        }
        public void LoadSalaryRuleToDropDownBLL(DropDownList aDropDownList, string strSalScaleId)
        {
            aEmpSalaryDal.LoadSalaryRuleToDropDown(aDropDownList, strSalScaleId);
        }

        public DataTable EmpSalaryDetailBll(string empId)
        {
            return aEmpSalaryDal.EmpSalaryDetailDal(empId);
        }
        public DataTable EmpInformationForPromotionBll(string empId)
        {
            return aEmpSalaryDal.EmpInformationForPromotionDal(empId);
        }
        public DataTable EmpInformationBll(string empId)
        {
            return aEmpSalaryDal.EmpInformationDal(empId);
        }
        public string TotalSalaryAmount(List<EmployeeSalary> aEmployeeSalaryList)
        {
            string amount = string.Empty;

            if (aEmployeeSalaryList.Count > 0)
            {
                decimal totalSalary = Convert.ToDecimal((from s in aEmployeeSalaryList select s.Amount).Sum());
                amount = totalSalary.ToString();
            }
            else
            {
                amount = "0";
            }
            return amount;
        }

        public bool StartingSalaryCheck(string empId)
        {
            if (aEmpSalaryDal.StartingSalaryCheck(empId.Trim()).Rows.Count > 0)
            {
                return false;
            }
            return true;
        }
        public bool SalarySave(List<EmployeeSalary> aEmployeeSalaryList)
        {
            foreach (var aEmployeeSalary in aEmployeeSalaryList)
            {
                string year = aEmployeeSalary.ActiveDate.Year.ToString();
                aEmployeeSalary.SalaryInfoId = aClsPrimaryKeyFind.PrimaryKeyMax("SalaryInfoId", "dbo.tblSalaryInformation", "HRDB");
                if (!aEmpSalaryDal.HasEmployeeSalary(aEmployeeSalary))
                {
                    if (!aEmpSalaryDal.SalaryDetailSave(aEmployeeSalary))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public bool DeleteData(string EmpInfoId)
        {
            return aEmpSalaryDal.DeleteData(EmpInfoId);

        }
        public DataTable LoadEmpGeneralInfoDeptWise()
        {
            return aEmpSalaryDal.LoadEmployeeViewDeptWise();
        }
        public DataTable LoadSalaryView(string empinfoId)
        {
            return aEmpSalaryDal.LoadSalaryView(empinfoId);
        }

        public DataTable LoadEmpView(string empinfoId)
        {
            return aEmpSalaryDal.LoadEmpView(empinfoId);
        }
    }
}
