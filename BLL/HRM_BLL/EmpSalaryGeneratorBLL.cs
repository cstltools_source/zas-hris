﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class EmpSalaryGeneratorBLL
    {

        EmpSalaryGeneratorDAL aEmpSalaryGeneratorDal = new EmpSalaryGeneratorDAL();
        public string SaveEmpSalaryGenerator(EmpSalaryGenerator aEmpSalaryGenerator)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                //aEmpSalaryGenerator.SalaryId = aClsPrimaryKeyFind.PrimaryKeyMax("SalaryId", "tblSalaryRecordPerMonth","HRDB");
                aEmpSalaryGeneratorDal.SaveDataForEmpSalaryGeneartor(aEmpSalaryGenerator);
                return "Data Save Successfully ";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        
        public bool SalaryProcess(DateTime SalaryStartDate,DateTime SalaryEndDate,string salaryId)
        {
            DataTable allEmpData = new DataTable();
            allEmpData = aEmpSalaryGeneratorDal.ActiveEmpGenInfo();

            foreach (DataRow row in allEmpData.Rows)
            {
                SalaryProcessAllEmp(row[0].ToString(), SalaryStartDate, SalaryEndDate, salaryId,row[1].ToString());
            }

            return true;
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aEmpSalaryGeneratorDal.LoadEmpInfo(EmpMasterCode);
        }
       
        public bool SalaryProcessSingleEmp(string empid,DateTime SalaryStartDate,DateTime SalaryEndDate,string salaryId,string EmpMasterCode)
        {
            DataTable dtempcheck = aEmpSalaryGeneratorDal.EmpCheck(empid);
            string otflag = aEmpSalaryGeneratorDal.OTFlag(empid);
            string emptype = aEmpSalaryGeneratorDal.CheckEmpType(empid);
            int totalPresent = 0;
            int totalAbsent = 0;
            int totalDayofMonth = 0;
            decimal Gross = 0;
            decimal Basic = 0;
            decimal lunchallowance = 0;
            decimal hr = 0;
            decimal medical = 0;
            decimal ConveyanceAllowance = 0;
            decimal perDayConveyanceAllowance = 0;
            decimal totalConveyanceAllowance = 0;
            decimal netPayable = 0;
            decimal totalOvertime = 0;
            decimal totalOTAmount = 0;
            decimal OTAmount = 0;
            decimal perDaySalary = 0;
            decimal netPayable2 = 0;
            decimal perDayBasic = 0;
            decimal perDaylunchallowance = 0;
            decimal perDayhr = 0;
            decimal perDaymedical = 0;
            decimal totalBasic = 0;
            decimal totalhr = 0;
            decimal totallunchallowance = 0;
            decimal totalmedical = 0;
            if (dtempcheck.Rows.Count > 0 && emptype == "Permanante" || dtempcheck.Rows.Count > 0 && emptype == "Provisional")
            {

                totalPresent = aEmpSalaryGeneratorDal.AttendencePresent(empid, SalaryStartDate, SalaryEndDate);
                totalAbsent = aEmpSalaryGeneratorDal.AttendenceAbsent(empid, SalaryStartDate, SalaryEndDate);
                totalDayofMonth = aEmpSalaryGeneratorDal.DayQtyofMonth(SalaryStartDate, SalaryEndDate);
                //totalOvertime = aEmpSalaryGeneratorDal.OverTime(empid, SalaryStartDate, SalaryEndDate);
                Gross = aEmpSalaryGeneratorDal.Gross(empid);
                Basic = aEmpSalaryGeneratorDal.Basic(empid);
                lunchallowance = aEmpSalaryGeneratorDal.LunchAllowance(empid);
                ConveyanceAllowance = aEmpSalaryGeneratorDal.ConveyanceAllowance(empid);
                perDayConveyanceAllowance = ConveyanceAllowance / totalDayofMonth;
                medical = aEmpSalaryGeneratorDal.Medical(empid);
                hr = aEmpSalaryGeneratorDal.HouseRent(empid);
                if (otflag == "Yes")
                {
                    OTAmount = (Basic / 206) * 2;
                }
                else
                {
                    OTAmount = 0;
                }
                totalOTAmount = OTAmountCalculation(totalOvertime, OTAmount);
                perDaySalary = Gross / totalDayofMonth;
                perDaylunchallowance = lunchallowance / totalDayofMonth;
                perDayBasic = Basic / totalDayofMonth;
                perDayhr = hr / totalDayofMonth;
                perDaymedical = medical / totalDayofMonth;
                totalBasic = Basic - (totalAbsent * perDayBasic);
                totalhr = hr - (totalAbsent * perDayhr);
                totalConveyanceAllowance = ConveyanceAllowance - (totalConveyanceAllowance * perDayConveyanceAllowance);
                totallunchallowance = lunchallowance - (totalAbsent * perDaylunchallowance);
                totalmedical = medical - (totalAbsent * perDaymedical);
                netPayable = (Gross - (totalAbsent * perDaySalary)) + totalOTAmount;
                netPayable2 = perDaySalary * totalPresent;
                DataTable dtEmpInfo = EmpGeneralId(empid);

                EmpSalaryGenerator aEmpSalaryGenerator = new EmpSalaryGenerator()
                {
                    SalaryStartDate = SalaryStartDate,
                    SalaryEndDate = SalaryEndDate,
                    EmpMasterCode = EmpMasterCode,
                    DeptId = Convert.ToInt32(dtEmpInfo.Rows[0]["DepId"].ToString()),
                    SectionId = Convert.ToInt32(dtEmpInfo.Rows[0]["SectionId"].ToString()),
                    Basic = totalBasic,
                    HouseRent = totalhr,
                    Medical = totalmedical,
                    //OTHours = totalOvertime / 60,
                    OTRate=
                    OTAmount = totalOTAmount,
                    Gross = Gross,
                    NetPayable = netPayable,
                    LunchAllowance = totallunchallowance,
                    DesigId = Convert.ToInt32(dtEmpInfo.Rows[0]["DesigId"].ToString()),
                    ConveyanceAllowance = totalConveyanceAllowance,
                    SalScaleId = Convert.ToInt32(dtEmpInfo.Rows[0]["SalScaleId"].ToString()),
                    EmpInfoId = Convert.ToInt32(empid),
                    SalaryId = Convert.ToInt32(salaryId),
                    //Period = DateTime.Today.Month.ToString()
                };

                SaveEmpSalaryGenerator(aEmpSalaryGenerator);
                return true;
            }
            else
            {
                return false;
            }


        }

        public bool SalaryProcessAllEmp(string empid, DateTime SalaryStartDate, DateTime SalaryEndDate, string salaryId,string EmpMasterCode)
        {
            
            DataTable dtsalheadconvchk = aEmpSalaryGeneratorDal.SalHeadNameconvChk(empid);
            DataTable dtsalheadlunchchk = aEmpSalaryGeneratorDal.SalHeadNameLunchChk(empid);
            DataTable dtempcheck = aEmpSalaryGeneratorDal.EmpCheck(empid);
            DataTable dtbasicchk = aEmpSalaryGeneratorDal.BasicChk(empid);
            DataTable dthouserentchk = aEmpSalaryGeneratorDal.HouseRentChk(empid);
            DataTable dtconveyancechk = aEmpSalaryGeneratorDal.ConveyanceAllowanceChk(empid);
            DataTable dtmedicalchk = aEmpSalaryGeneratorDal.MedicalChk(empid);
            DataTable dtlunchallowance = aEmpSalaryGeneratorDal.LunchAllowanceChk(empid);
            DataTable dtgross = aEmpSalaryGeneratorDal.GrossChk(empid);
            string otflag = aEmpSalaryGeneratorDal.OTFlag(empid);
            string emptype = aEmpSalaryGeneratorDal.CheckEmpType(empid);
            int totalPresent = 0;
            int totalAbsent = 0;
            int totalDayofMonth = 0;
            decimal tiffin = 0;
            decimal Gross = 0;
            decimal Basic = 0;
            decimal lunchallowance = 0;
            decimal hr = 0;
            decimal medical = 0;
            decimal ConveyanceAllowance = 0;
            decimal perDayConveyanceAllowance = 0;
            decimal totalConveyanceAllowance = 0;
            decimal netPayable = 0;
            decimal totalOvertime = 0;
            decimal totalOTAmount = 0;
            decimal OTAmount = 0;
            decimal perDaySalary = 0;
            decimal netPayable2 = 0;
            decimal perDayBasic = 0;
            decimal perDaylunchallowance = 0;
            decimal perDayhr = 0;
            decimal perDaymedical = 0;
            decimal totalBasic = 0;
            decimal totalhr = 0;
            decimal totallunchallowance = 0;
            decimal totalmedical = 0;
            decimal totaltiffin = 0;
            decimal totalleave = 0;
            if (dtempcheck.Rows.Count > 0 && emptype == "Permanante" || dtempcheck.Rows.Count > 0 && emptype == "Provisional")
            {

                totalPresent = aEmpSalaryGeneratorDal.AttendencePresent(empid, SalaryStartDate, SalaryEndDate);
                totalAbsent = aEmpSalaryGeneratorDal.AttendenceAbsent(empid, SalaryStartDate, SalaryEndDate);
                totalDayofMonth = (int) (SalaryEndDate - SalaryStartDate).TotalDays;
                totalleave = aEmpSalaryGeneratorDal.Leave(empid,SalaryStartDate, SalaryEndDate);
                //totalOvertime = aEmpSalaryGeneratorDal.OverTime(empid, SalaryStartDate, SalaryEndDate);

                if (dtgross.Rows.Count > 0)
                {
                    Gross = aEmpSalaryGeneratorDal.Gross(empid);    
                }
                else
                {

                    Gross = 0;
                }
                if (dtlunchallowance.Rows.Count>0)
                {
                    lunchallowance = aEmpSalaryGeneratorDal.LunchAllowance(empid);
                }
                else
                {
                    lunchallowance = 0;
                }
                if (dtmedicalchk.Rows.Count>0)
                {
                    medical = aEmpSalaryGeneratorDal.Medical(empid);    
                }
                else
                {
                    medical = 0;
                }
                if (dtconveyancechk.Rows.Count>0)
                {
                    ConveyanceAllowance = aEmpSalaryGeneratorDal.ConveyanceAllowance(empid);    
                }
                else
                {
                    ConveyanceAllowance = 0;
                }
                if (dthouserentchk.Rows.Count>0)
                {
                    hr = aEmpSalaryGeneratorDal.HouseRent(empid);    
                }
                else
                {
                    hr = 0;
                }
                if (dtbasicchk.Rows.Count>0)
                {
                    Basic = aEmpSalaryGeneratorDal.Basic(empid);    
                }
                else
                {
                    Basic = 0;
                }
                
                if (otflag=="Yes")
                {
                    OTAmount = (Basic / 206) * 2;
                }
                else
                {
                    OTAmount = 0;
                }
                DataTable dtarrear = aEmpSalaryGeneratorDal.Arrear(empid, SalaryStartDate, SalaryEndDate);
                DataTable dtotherallowance = aEmpSalaryGeneratorDal.OtherAllowance(empid, SalaryStartDate, SalaryEndDate);
                DataTable dtfoodcharge = aEmpSalaryGeneratorDal.FoodCharge(empid, SalaryStartDate, SalaryEndDate);
                //totaltiffin = totalPresent*tiffin;
                //totalOTAmount = OTAmountCalculation(totalOvertime, OTAmount);
                //perDaySalary = Gross / totalDayofMonth;
                //perDaylunchallowance = lunchallowance / totalDayofMonth;
                //perDayBasic = Basic / totalDayofMonth;
                //perDayhr = hr / totalDayofMonth;
                //perDaymedical = medical / totalDayofMonth;
                //totalBasic = Basic - (totalAbsent * perDayBasic);
                //totalhr = hr - (totalAbsent * perDayhr);
                //totalConveyanceAllowance = ConveyanceAllowance - (totalConveyanceAllowance * perDayConveyanceAllowance);
                //totallunchallowance = lunchallowance - (totalAbsent * perDaylunchallowance);
                //totalmedical = medical - (totalAbsent * perDaymedical);
                //netPayable = (Gross - (totalAbsent * perDaySalary)) + totalOTAmount;
                //netPayable2 = perDaySalary * totalPresent;
                DataTable dtEmpInfo = EmpGeneralId(empid);

                EmpSalaryGenerator aEmpSalaryGenerator = new EmpSalaryGenerator();
                aEmpSalaryGenerator.SalaryStartDate = SalaryStartDate;
                aEmpSalaryGenerator.SalaryEndDate = SalaryEndDate;
                aEmpSalaryGenerator.EmpMasterCode = EmpMasterCode;
                aEmpSalaryGenerator.JoiningDate = Convert.ToDateTime((dtEmpInfo.Rows[0]["JoiningDate"].ToString()));
                aEmpSalaryGenerator.CompanyInfoId = Convert.ToInt32(dtEmpInfo.Rows[0][0].ToString());
                aEmpSalaryGenerator.UnitId = Convert.ToInt32(dtEmpInfo.Rows[0][1].ToString());
                aEmpSalaryGenerator.DivisionId = Convert.ToInt32(dtEmpInfo.Rows[0][2].ToString());
                aEmpSalaryGenerator.DeptId = Convert.ToInt32(dtEmpInfo.Rows[0][3].ToString());
                aEmpSalaryGenerator.SectionId = Convert.ToInt32(dtEmpInfo.Rows[0][4].ToString());
                aEmpSalaryGenerator.DesigId = Convert.ToInt32(dtEmpInfo.Rows[0][5].ToString());
                aEmpSalaryGenerator.EmpTypeId = Convert.ToInt32(dtEmpInfo.Rows[0][6].ToString());
                aEmpSalaryGenerator.EmpGradeId = Convert.ToInt32(dtEmpInfo.Rows[0][7].ToString());
                aEmpSalaryGenerator.SalScaleId = Convert.ToInt32(dtEmpInfo.Rows[0][8].ToString());
                aEmpSalaryGenerator.MonthDays = totalDayofMonth;
                aEmpSalaryGenerator.PunchDay = totalPresent;
                aEmpSalaryGenerator.AbsentDays = totalAbsent;
                aEmpSalaryGenerator.Basic = Basic;
                aEmpSalaryGenerator.HouseRent = hr;
                aEmpSalaryGenerator.Medical = medical;
                aEmpSalaryGenerator.Gross = Gross;
                aEmpSalaryGenerator.LunchAllowance = lunchallowance;
                aEmpSalaryGenerator.ConveyanceAllowance = ConveyanceAllowance;
                if (dtfoodcharge.Rows.Count>0)
                {
                    aEmpSalaryGenerator.FoodCharge = Convert.ToDecimal(dtfoodcharge.Rows[0][0].ToString());    
                }
                else
                {
                    aEmpSalaryGenerator.FoodCharge = 0;
                }
                
                aEmpSalaryGenerator.EmpInfoId = Convert.ToInt32(empid);
                aEmpSalaryGenerator.SalaryId = Convert.ToInt32(salaryId);
                aEmpSalaryGenerator.TotalHoliday = Convert.ToInt32("1");
                aEmpSalaryGenerator.WorkingDays = Convert.ToInt32("1");
                aEmpSalaryGenerator.MobileAllowance = Convert.ToDecimal("1");
                aEmpSalaryGenerator.SpecialAllowance = Convert.ToDecimal("1");
                aEmpSalaryGenerator.HolidayBillAmt = Convert.ToDecimal("1");
                aEmpSalaryGenerator.NightBillAmt = Convert.ToDecimal("1");
                aEmpSalaryGenerator.OTHours = "1";
                aEmpSalaryGenerator.OTRate = Convert.ToDecimal("1");
                aEmpSalaryGenerator.OTAmount = Convert.ToDecimal("1");
                aEmpSalaryGenerator.TiffinCharge = Convert.ToDecimal("1");
                aEmpSalaryGenerator.AttnBonusRate = Convert.ToDecimal("1");
                aEmpSalaryGenerator.SpecialBonus = Convert.ToDecimal("1");
                aEmpSalaryGenerator.SalaryAdvance = Convert.ToDecimal("1");
                aEmpSalaryGenerator.Tax = Convert.ToDecimal("1");
                aEmpSalaryGenerator.Absenteeism = Convert.ToDecimal("1");
                aEmpSalaryGenerator.OtherDeduction = Convert.ToDecimal("1");
                aEmpSalaryGenerator.SalAdvDeduct = Convert.ToDecimal("1");
                aEmpSalaryGenerator.NetPayable = Convert.ToDecimal("1");
                aEmpSalaryGenerator.PayBankorCash = "A";
                aEmpSalaryGenerator.BankAccNo = dtEmpInfo.Rows[0]["BankAccNo"].ToString();
                aEmpSalaryGenerator.BankId = Convert.ToInt32(dtEmpInfo.Rows[0]["BankId"].ToString());
                aEmpSalaryGenerator.PrFund = Convert.ToDecimal("1");
                    
                    //Period = DateTime.Today.Month.ToString()
                
                if (dtarrear.Rows.Count>0)
                {
                    aEmpSalaryGenerator.Arrear = Convert.ToDecimal((dtarrear.Rows[0][0].ToString()));  
                }
                else
                {
                    aEmpSalaryGenerator.Arrear = 0;
                }
                if (dtotherallowance.Rows.Count>0)
                {
                    aEmpSalaryGenerator.OtherAllowances = Convert.ToDecimal((dtotherallowance.Rows[0][0].ToString()));
                }
                else
                {
                    aEmpSalaryGenerator.OtherAllowances = 0;
                }
                SaveEmpSalaryGenerator(aEmpSalaryGenerator);
                return true;
            }
            else
            {
                return false;
            }
        }
        
        private decimal OTAmountCalculation(decimal totalOvertime, decimal OTAmount)
        {
            decimal amount = 0;
            amount = (totalOvertime/60)*OTAmount;
            return amount;
        }
        
        public DataTable EmpGeneralId(string empid)
        {
            return aEmpSalaryGeneratorDal.EmpGeneralId(empid);
        }
        public DataTable EmpCheck(string empid)
        {
            return aEmpSalaryGeneratorDal.EmpCheck(empid);
        } 
    }
}
