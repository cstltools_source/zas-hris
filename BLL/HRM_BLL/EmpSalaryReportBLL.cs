﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
    public class EmpSalaryReportBLL
    {
        EmpSalaryReportDAL aEmpSalaryReportDal=new EmpSalaryReportDAL();
        public DataTable SalaryDeduction()
        {
            return aEmpSalaryReportDal.SalaryDeduction();
        }
        public DataTable ArrearRpt()
        {
            return aEmpSalaryReportDal.ArrearRpt();
        }
        public DataTable AdvanceSalDeduc()
        {
            return aEmpSalaryReportDal.AdvanceSalDeducRpt();
        }
        public DataTable EmpSalaryReportMainData(string parameter)
        {
            return aEmpSalaryReportDal.EmpSalaryReportMainData(parameter);
        }
        public DataTable EmpSalaryReportMainDataCom(string parameter)
        {
            return aEmpSalaryReportDal.EmpSalaryReportMainDataCom(parameter);
        }
        
        public DataTable RptHeader()
        {
            return aEmpSalaryReportDal.RptHeader();
        }
        public DataTable SalaryReportDetailsBLL(string parameter, string BankCash)
        {
            return aEmpSalaryReportDal.SalaryReportDetailsDAL(parameter, BankCash);
        }
        public DataTable LoanAmountInfo(string frommonth, string tomonth, string year)
        {
            return aEmpSalaryReportDal.LoanAmountInfo(frommonth, tomonth, year);
        }
        public DataTable EmpSalaryInfo(string empcode)
        {
            return aEmpSalaryReportDal.EmpSalaryInfo(empcode);
        }
        public DataTable SalaryReportDetailsBLLCom(string parameter, string BankCash)
        {
            return aEmpSalaryReportDal.SalaryReportDetailsComDAL(parameter, BankCash);
        }
        public DataTable FinalSattlementSalaryReportDetailsBLL(string parameter, string BankCash)
        {
            return aEmpSalaryReportDal.FinalSattlementSalaryReportDetailsDAL(parameter, BankCash);
        }
        public DataTable FinalSattlementSalaryReportDetailsBLLCom(string parameter, string BankCash)
        {
            return aEmpSalaryReportDal.FinalSattlementSalaryReportDetailsComDAL(parameter, BankCash);
        }
        public DataTable EmpOTInfo(string empid, string month)
        {
            return aEmpSalaryReportDal.EmpOTInfo(empid,month);
        }
        public DataTable EmpComOTInfo(string empid, string month)
        {
            return aEmpSalaryReportDal.EmpComOTInfo(empid,month);
        }
        public DataTable ExtraOverTime(string month,string monthname)
        {
            DataTable dtallemp = aEmpSalaryReportDal.EmpGenInfo();
            DataTable dtMain = new DataTable();
            dtMain.Columns.Add("EmpMasterCode");
            dtMain.Columns.Add("EmpName");
            dtMain.Columns.Add("MonthName");
            dtMain.Columns.Add("Designation");
            dtMain.Columns.Add("Department");
            dtMain.Columns.Add("UnitName");
            dtMain.Columns.Add("SectionName");
            dtMain.Columns.Add("GradeName");
            dtMain.Columns.Add("TotalOTHour");
            dtMain.Columns.Add("TotalAmount");
            
            DataRow dr;

            foreach (DataRow dataRow in dtallemp.Rows)
            {
                DataTable dtEmpOt = EmpOTInfo(dataRow["EmpInfoId"].ToString(),month);
                DataTable dtEmpOtCom = EmpComOTInfo(dataRow["EmpInfoId"].ToString(),month);

                
                int othour = Convert.ToInt32(dtEmpOt.Rows[0][0].ToString());
                decimal otAmount = Convert.ToDecimal(dtEmpOt.Rows[0][1].ToString());
                int othourCom = Convert.ToInt32(dtEmpOtCom.Rows[0][0].ToString());
                decimal otAmountCom = Convert.ToDecimal(dtEmpOtCom.Rows[0][1].ToString());

                int totalhour = othour-othourCom;
                decimal totalamount = otAmount - otAmountCom;

                if (totalhour != 0)
                {
                dr = dtMain.NewRow();
                dr["EmpMasterCode"] = dataRow["EmpMasterCode"].ToString();
                dr["EmpName"] = dataRow["EmpName"].ToString();
                dr["Designation"] = dataRow["DesigName"].ToString();
                dr["Department"] = dataRow["DeptName"].ToString();
                dr["GradeName"] = dataRow["SalScaleName"].ToString();
                dr["SectionName"] = dataRow["SectionName"].ToString();
                dr["UnitName"] = dataRow["UnitName"].ToString();
                dr["MonthName"] = monthname;
                dr["TotalAmount"] = totalamount;
                dr["TotalOTHour"] = totalhour;
                dtMain.Rows.Add(dr);    
                
                    
                }
                
            }

            return dtMain;
        }

        public DataTable EmpSalary(string empcode)
        {
            DataTable dtMain = new DataTable();
            dtMain.Columns.Add("Basic");
            dtMain.Columns.Add("HouseRent");
            dtMain.Columns.Add("Medical");
            dtMain.Columns.Add("Conveyance");
            dtMain.Columns.Add("LunchAllow");
            
            DataRow dr;



            DataTable dtbasic = aEmpSalaryReportDal.Basic(empcode);
            DataTable dthouse = aEmpSalaryReportDal.HouseRent(empcode);
            DataTable dtmedical = aEmpSalaryReportDal.Medical(empcode);
            DataTable dtconv = aEmpSalaryReportDal.Conveyance(empcode);
            DataTable dtlunch = aEmpSalaryReportDal.LunchAllow(empcode);

            dr = dtMain.NewRow();
            dr["Basic"] = dtbasic.Rows[0][1].ToString();
            dr["HouseRent"] = dthouse.Rows[0][1].ToString();
            dr["Medical"] = dtmedical.Rows[0][1].ToString();
            dr["Conveyance"] = dtconv.Rows[0][1].ToString();
            if (dtlunch.Rows.Count>0)
            {
                dr["LunchAllow"] = dtlunch.Rows[0][1].ToString();    
            }
            else
            {
                dr["LunchAllow"] = "0";
            }
            
            
            dtMain.Rows.Add(dr);

            return dtMain;
        }

    }
}
