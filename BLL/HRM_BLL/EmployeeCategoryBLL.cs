﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class EmployeeCategoryBLL
    {
        EmployeeCategoryDAL aEmployeeCategoryDal = new EmployeeCategoryDAL();
        public string SaveDataForEmpCategory(EmployeeCategory aEmployeeCategory)
        {
            try
            {
                if (!aEmployeeCategoryDal.HasEmpCategoryName(aEmployeeCategory))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aEmployeeCategory.EmpCategoryId = aClsPrimaryKeyFind.PrimaryKeyMax("EmpCategoryId", "tblEmpCategory","HRDB");
                    aEmployeeCategory.EmpCategoryCode = EmpCategoryCodeGenerator(aEmployeeCategory.EmpCategoryId);
                    aEmployeeCategoryDal.SaveDataForEmployeeCategory(aEmployeeCategory);
                    return "Data Save Successfully Designaiton Code is :  " + aEmployeeCategory.EmpCategoryCode + " And Designation Name is : " + aEmployeeCategory.EmpCategoryName;  
                }
                else
                {
                    return "Employee Cetegoty Name already exist";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }


        public string EmpCategoryCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "EMPC-" + Id;
            return code;
        }

        public bool UpdateDataForEmployeeCetegory(EmployeeCategory aEmployeeCategory)
        {
            return aEmployeeCategoryDal.UpdateEmployeeCetegoryInfo(aEmployeeCategory);
        }

        public DataTable LoadEmployeeCetegoryView()
        {
            return aEmployeeCategoryDal.LoadEmployeeCetegoryView();
        }

        public EmployeeCategory EmployeeCategoryEditLoad(string employeeCategoryId)
        {
            return aEmployeeCategoryDal.EmployeeCetegoryEditLoad(employeeCategoryId);
        }
    }
}
