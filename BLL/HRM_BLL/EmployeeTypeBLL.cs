﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class EmployeeTypeBLL
    {
        EmployeeTypeDAL aTypeDal= new EmployeeTypeDAL();
        public bool SaveDataForEmployeeType(EmployeeType aType)
        {
            try
            {
                if (!aTypeDal.HasEmpTypeName(aType))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aType.EmpTypeId = aClsPrimaryKeyFind.PrimaryKeyMax("EmpTypeId", "tblEmployeeType", "HRDB");
                    aTypeDal.SaveDataForEmployeeType(aType);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateEmployeeType(EmployeeType aType)
        {
            return aTypeDal.UpdateEmployeeType(aType);
        }

        public DataTable LoadEmployeeTypeView()
        {
            return aTypeDal.LoadEmployeeTypeView();
        }

        public EmployeeType EmployeeTypeEditLoad(string EmpTypeId)
        {
            return aTypeDal.EmployeeTypeEditLoad(EmpTypeId);
        }
    }
}
