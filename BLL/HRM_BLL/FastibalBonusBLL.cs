﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class FastibalBonusBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        FastibalBonusDAL aFastibalBonusDal=new FastibalBonusDAL();
        public bool SaveDataForFastibalBonus(FastibalBonusMastar aFastibalBonusMastar,List<FastibalBonusDetail> aFastibalBonusDetailList )
        {
            try
            {
                if (!aFastibalBonusDal.HasFastibalBonus(aFastibalBonusMastar))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aFastibalBonusMastar.FastivalBonusId = aClsPrimaryKeyFind.PrimaryKeyMax("FastivalBonusId",
                        "tblFastivalBonusMastar", "HRDB");
                    aFastibalBonusDal.SaveFastibalBonusMastar(aFastibalBonusMastar);
                    foreach (var fastibalBonusDetail in aFastibalBonusDetailList)
                    {
                        fastibalBonusDetail.FastivalBonusId = aFastibalBonusMastar.FastivalBonusId;
                        fastibalBonusDetail.FastivalBonusDetailsId =
                            aClsPrimaryKeyFind.PrimaryKeyMax("FastivalBonusDetailsId", "tblFastivalBonusDetails", "HRDB");
                        aFastibalBonusDal.SaveFastibalBonusDetail(fastibalBonusDetail);
                    }
                    return true;
                }
                else
                {
                    return false;
                }


                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void LoadFestivalName(DropDownList ddl)
        {
            aFastibalBonusDal.LoadFestivalName(ddl);
        }
        public void LoadEmpCateGory(DropDownList ddl)
        {
            aFastibalBonusDal.LoadEmpCateGory(ddl);
        }
        public DataTable LoadFastibalBonusView()
        {
            return aFastibalBonusDal.LoadFastibalBonusView();
        }
        public DataTable FestivalBonusReport(string year, string festivalId,string parameter)
        {
            return aFastibalBonusDal.FestivalBonusReport(year, festivalId,parameter);
        }
        public DataTable LoadFastivalBonusEditData(string fastivalbonusId)
        {
            return aFastibalBonusDal.LoadFastivalBonusEditData(fastivalbonusId);
        }
        public bool UpdateFastibalBonusMastar(FastibalBonusMastar aFastibalBonusMastar)
        {
            return aFastibalBonusDal.UpdateFastibalBonusMastar(aFastibalBonusMastar);
        }
        public bool UpdateFastibalBonusDetail(FastibalBonusDetail aFastibalBonusDetail)
        {
            return aFastibalBonusDal.UpdateFastibalBonusDetail(aFastibalBonusDetail);
        }
        public bool DeleteFastibalBonusDetail(string festivalId)
        {
            return aFastibalBonusDal.DeleteFastibalBonusDetail(festivalId);
        }
        public bool SaveFastibalBonusDetail(FastibalBonusDetail aFastibalBonusDetail)
        {
            ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
            aFastibalBonusDetail.FastivalBonusDetailsId = aClsPrimaryKeyFind.PrimaryKeyMax("FastivalBonusDetailsId", "tblFastivalBonusDetails", "HRDB");
            return aFastibalBonusDal.SaveFastibalBonusDetail(aFastibalBonusDetail);
        }
        public int FestivalProcess(int fastivalId, string festivalYear, int empcategoryId, string executeby, string dataBaseName)
        {
            return aFastibalBonusDal.FestivalProcess(fastivalId, festivalYear, empcategoryId, executeby,dataBaseName);
        }
        public DataTable GetFestivalBonusData(string festivalId, string festivalyear, string emcategoryId)
        {
            return aFastibalBonusDal.GetFestivalBonusData(festivalId, festivalyear, emcategoryId);
        }
    }
}
