﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class FastibalNameBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        FastibalNameDAL aFastibalNameDal = new FastibalNameDAL();
        public bool SaveFastibalName(FastibalName aFastibalName)
        {
            try
            {
                if (!aFastibalNameDal.HasFastibalName(aFastibalName))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aFastibalName.FestivalId = aClsPrimaryKeyFind.PrimaryKeyMax("FestivalId", "tblFastivalName", "HRDB");
                    aFastibalNameDal.SaveFastibalName(aFastibalName);
                    return true;

                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        

        public DataTable LoadFastibalNameView()
        {
            return aFastibalNameDal.LoadFastibalNameView();
        }

        //public void LoadDivisionName(DropDownList ddl)
        //{
        //    ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
        //    string queryStr = "select * from tblDivision";
        //    aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        //}
        public FastibalName FastibalNameEditLoad(string FestivalId)
        {
            return aFastibalNameDal.FastibalNameEditLoad(FestivalId);
        }

        public bool UpdateFastibalName(FastibalName aFastibalName)
        {
            return aFastibalNameDal.UpdateFastibalName(aFastibalName);
        }
        public bool DeleteFastibalName(string FestivalId)
        {
            return aFastibalNameDal.DeleteFastibalName(FestivalId);
        }
    }
}
