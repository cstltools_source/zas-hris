﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class FinalSattlementBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        FinalSattlementDAL aFinalSattlementDal = new FinalSattlementDAL();
        public bool SalaryProcessMain(DataRow dataRow, DateTime SalaryStartDate, DateTime SalaryEndDate, decimal prfund)
        {
            string empid = dataRow["EmpInfoId"].ToString();
            FinalSattlement aFinalSattlement = new FinalSattlement();

            aFinalSattlement.EmpInfoId = Convert.ToInt32(empid);
            aFinalSattlement.SalaryStartDate = SalaryStartDate;
            aFinalSattlement.SalaryEndDate = SalaryEndDate;
            aFinalSattlement.EmpMasterCode = dataRow["EmpMasterCode"].ToString();
            aFinalSattlement.JoiningDate = Convert.ToDateTime((dataRow["JoiningDate"].ToString()));
            aFinalSattlement.CompanyInfoId = Convert.ToInt32(dataRow["CompanyInfoId"].ToString());
            aFinalSattlement.UnitId = Convert.ToInt32(dataRow["UnitId"].ToString());
            aFinalSattlement.DivisionId = Convert.ToInt32(dataRow["DivisionId"].ToString());
            aFinalSattlement.DeptId = Convert.ToInt32(dataRow["DepId"].ToString());
            aFinalSattlement.SectionId = Convert.ToInt32(dataRow["SectionId"].ToString());
            aFinalSattlement.DesigId = Convert.ToInt32(dataRow["DesigId"].ToString());
            aFinalSattlement.EmpTypeId = Convert.ToInt32(dataRow["EmpTypeId"].ToString());
            aFinalSattlement.EmpGradeId = Convert.ToInt32(dataRow["EmpGradeId"].ToString());
            aFinalSattlement.SalScaleId = Convert.ToInt32(dataRow["SalScaleId"].ToString());
            aFinalSattlement.PrFund = prfund;
            aFinalSattlement.MonthDays = Convert.ToInt32(aFinalSattlementDal.GetMonthDays(SalaryStartDate, SalaryEndDate));
            aFinalSattlement.AbsentDays = aFinalSattlementDal.AttendenceAbsent(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.PunchDay = aFinalSattlementDal.GetAttendencePunchDays(empid, SalaryStartDate, SalaryEndDate);
            string otflag = dataRow["OTAllow"].ToString().Trim();

            aFinalSattlement.Basic = decimal.Round(((aFinalSattlementDal.Basic(empid) / 30) * aFinalSattlement.MonthDays), 0);
            aFinalSattlement.HouseRent = decimal.Round(((aFinalSattlementDal.HouseRent(empid) / 30) * aFinalSattlement.MonthDays), 0);
            aFinalSattlement.Medical = decimal.Round(((aFinalSattlementDal.Medical(empid) / 30) * aFinalSattlement.MonthDays), 0);

            aFinalSattlement.LunchAllowance = decimal.Round(((aFinalSattlementDal.LunchAllowance(empid) / 30) * aFinalSattlement.MonthDays), 0);
            aFinalSattlement.ConveyanceAllowance = decimal.Round(((aFinalSattlementDal.ConveyanceAllowance(empid) / 30) * aFinalSattlement.MonthDays), 0);
            aFinalSattlement.ActualGross = decimal.Round(((aFinalSattlementDal.Gross(empid) / 30) * aFinalSattlement.MonthDays), 0);

            DataTable dtarrear = aFinalSattlementDal.Arrear(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.Arrear = Convert.ToDecimal(dtarrear.Rows[0][0].ToString());
            DataTable dtfoodcharge = aFinalSattlementDal.FoodCharge(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.FoodCharge =
                decimal.Round((Convert.ToDecimal(dtfoodcharge.Rows[0][0].ToString()) / 30) * aFinalSattlement.MonthDays);
            DataTable dtotherallowance = aFinalSattlementDal.OtherAllowance(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.OtherAllowances = decimal.Round((Convert.ToDecimal(dtotherallowance.Rows[0][0].ToString()) / 30) * aFinalSattlement.MonthDays);
            DataTable dtmobAllow = aFinalSattlementDal.MobileAllowance(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.MobileAllowance = decimal.Round((Convert.ToDecimal(dtmobAllow.Rows[0][0].ToString()) / 30) * aFinalSattlement.MonthDays);
            aFinalSattlement.TotalHoliday = aFinalSattlementDal.GetHoliDays(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.WorkingDays = aFinalSattlementDal.GetAttendencePunchDays(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.SpecialAllowance = Convert.ToDecimal("0");
            aFinalSattlement.HolidayBillAmt = Convert.ToDecimal("0");
            aFinalSattlement.NightBillAmt = Convert.ToDecimal("0");


            if (otflag == "Yes")
            {
                aFinalSattlement.OTHours = "0";
                aFinalSattlement.OTRate = Convert.ToDecimal("0");
                aFinalSattlement.OTAmount = Convert.ToDecimal("0");

                //aFinalSattlement.OTHours = aFinalSattlementDal.OverTimeHour(empid, SalaryStartDate, SalaryEndDate);
                //aFinalSattlement.OTRate = decimal.Round(((Convert.ToDecimal((aFinalSattlement.Basic)) / 208) * 2), 0);
                //aFinalSattlement.OTAmount = decimal.Round((aFinalSattlement.OTRate * Convert.ToDecimal(aFinalSattlement.OTHours)), 0);
            }
            else
            {
                aFinalSattlement.OTHours = "0";
                aFinalSattlement.OTRate = Convert.ToDecimal("0");
                aFinalSattlement.OTAmount = Convert.ToDecimal("0");
            }

            aFinalSattlement.TiffinCharge = decimal.Round(aFinalSattlementDal.GetTiffinCharge(empid, SalaryStartDate, SalaryEndDate), 0);
            aFinalSattlement.AttnBonusRate = Convert.ToDecimal("0");
            aFinalSattlement.SpecialBonus = Convert.ToDecimal("0");
            aFinalSattlement.SalaryAdvance = 0;
            aFinalSattlement.SalaryAdvance = aFinalSattlementDal.SalaryAdvance(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.SalaryAdvance = aFinalSattlement.SalaryAdvance +
                                             aFinalSattlementDal.LoanAmount(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.Tax = Convert.ToDecimal("0");
            //aFinalSattlement.Absenteeism =
            //    decimal.Round(
            //        ((aFinalSattlement.Basic / aFinalSattlement.MonthDays) * aFinalSattlement.AbsentDays), 0);
            aFinalSattlement.Absenteeism = decimal.Round(((aFinalSattlementDal.Gross(empid) / 30) * aFinalSattlement.AbsentDays), 0);

            aFinalSattlement.OtherDeduction = aFinalSattlementDal.OtherDeduction(empid, SalaryStartDate, SalaryEndDate);

            aFinalSattlement.PayBankorCash = dataRow["PayType"].ToString();
            aFinalSattlement.BankAccNo = dataRow["BankAccNo"].ToString();
            aFinalSattlement.BankId = Convert.ToInt32(dataRow["BankId"].ToString());
            aFinalSattlement.Gross = aFinalSattlement.ActualGross + aFinalSattlement.Arrear + aFinalSattlement.MobileAllowance +
                                        aFinalSattlement.SpecialAllowance + aFinalSattlement.HolidayBillAmt +
                                        aFinalSattlement.NightBillAmt + aFinalSattlement.OtherAllowances +
                                        aFinalSattlement.TiffinCharge + aFinalSattlement.OTAmount + prfund;

            decimal totalPay = 0;
            decimal totalDeduction = 0;

            totalPay = aFinalSattlement.Gross;
            totalDeduction = aFinalSattlement.FoodCharge + aFinalSattlement.SalaryAdvance + aFinalSattlement.Absenteeism +
                             aFinalSattlement.OtherDeduction;


            aFinalSattlement.NetPayable = decimal.Round((totalPay - totalDeduction), 0);

            aFinalSattlementDal.DeleteDataForEmpSalaryGeneartor(aFinalSattlement);
            aFinalSattlementDal.SaveDataForEmpSalaryGeneartor(aFinalSattlement);


            return true;
        }

        public bool SalaryProcessCom(DataRow dataRow, DateTime SalaryStartDate, DateTime SalaryEndDate, decimal prfund)
        {
            string empid = dataRow["EmpInfoId"].ToString();
            FinalSattlement aFinalSattlement = new FinalSattlement();

            aFinalSattlement.EmpInfoId = Convert.ToInt32(empid);
            aFinalSattlement.SalaryStartDate = SalaryStartDate;
            aFinalSattlement.SalaryEndDate = SalaryEndDate;
            aFinalSattlement.EmpMasterCode = dataRow["EmpMasterCode"].ToString();
            aFinalSattlement.JoiningDate = Convert.ToDateTime((dataRow["JoiningDate"].ToString()));
            aFinalSattlement.CompanyInfoId = Convert.ToInt32(dataRow["CompanyInfoId"].ToString());
            aFinalSattlement.UnitId = Convert.ToInt32(dataRow["UnitId"].ToString());
            aFinalSattlement.DivisionId = Convert.ToInt32(dataRow["DivisionId"].ToString());
            aFinalSattlement.DeptId = Convert.ToInt32(dataRow["DepId"].ToString());
            aFinalSattlement.SectionId = Convert.ToInt32(dataRow["SectionId"].ToString());
            aFinalSattlement.DesigId = Convert.ToInt32(dataRow["DesigId"].ToString());
            aFinalSattlement.EmpTypeId = Convert.ToInt32(dataRow["EmpTypeId"].ToString());
            aFinalSattlement.EmpGradeId = Convert.ToInt32(dataRow["EmpGradeId"].ToString());
            aFinalSattlement.SalScaleId = Convert.ToInt32(dataRow["SalScaleId"].ToString());
            aFinalSattlement.PrFund = prfund;
            aFinalSattlement.MonthDays = Convert.ToInt32(aFinalSattlementDal.GetMonthDays(SalaryStartDate, SalaryEndDate));
            aFinalSattlement.AbsentDays = aFinalSattlementDal.AttendenceAbsent(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.PunchDay = aFinalSattlementDal.GetAttendencePunchDays(empid, SalaryStartDate, SalaryEndDate);
            string otflag = dataRow["OTAllow"].ToString().Trim();

            aFinalSattlement.Basic = decimal.Round(((aFinalSattlementDal.Basic(empid) / 30) * aFinalSattlement.PunchDay), 0);
            aFinalSattlement.HouseRent = decimal.Round(((aFinalSattlementDal.HouseRent(empid) / 30) * aFinalSattlement.PunchDay), 0);
            aFinalSattlement.Medical = decimal.Round(((aFinalSattlementDal.Medical(empid) / 30) * aFinalSattlement.PunchDay), 0);

            aFinalSattlement.LunchAllowance = decimal.Round(((aFinalSattlementDal.LunchAllowance(empid) / 30) * aFinalSattlement.PunchDay), 0);
            aFinalSattlement.ConveyanceAllowance = decimal.Round(((aFinalSattlementDal.ConveyanceAllowance(empid) / 30) * aFinalSattlement.PunchDay), 0);
            aFinalSattlement.ActualGross = decimal.Round(((aFinalSattlementDal.Gross(empid) / 30) * aFinalSattlement.PunchDay), 0);

            DataTable dtarrear = aFinalSattlementDal.Arrear(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.Arrear = Convert.ToDecimal(dtarrear.Rows[0][0].ToString());
            DataTable dtfoodcharge = aFinalSattlementDal.FoodCharge(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.FoodCharge =
                decimal.Round((Convert.ToDecimal(dtfoodcharge.Rows[0][0].ToString()) / 30) * aFinalSattlement.PunchDay);
            DataTable dtotherallowance = aFinalSattlementDal.OtherAllowance(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.OtherAllowances = decimal.Round((Convert.ToDecimal(dtotherallowance.Rows[0][0].ToString()) / 30) * aFinalSattlement.PunchDay);
            DataTable dtmobAllow = aFinalSattlementDal.MobileAllowance(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.MobileAllowance = decimal.Round((Convert.ToDecimal(dtmobAllow.Rows[0][0].ToString()) / 30) * aFinalSattlement.PunchDay);
            aFinalSattlement.TotalHoliday = aFinalSattlementDal.GetHoliDays(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.WorkingDays = aFinalSattlementDal.GetAttendencePunchDays(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.SpecialAllowance = Convert.ToDecimal("0");
            aFinalSattlement.HolidayBillAmt = Convert.ToDecimal("0");
            aFinalSattlement.NightBillAmt = Convert.ToDecimal("0");


            if (otflag == "Yes")
            {
                aFinalSattlement.OTHours = aFinalSattlementDal.ComOverTimeHour(empid, SalaryStartDate, SalaryEndDate);
                aFinalSattlement.OTRate = decimal.Round(((Convert.ToDecimal((aFinalSattlement.Basic)) / 208) * 2), 0);
                aFinalSattlement.OTAmount = decimal.Round((aFinalSattlement.OTRate * Convert.ToDecimal(aFinalSattlement.OTHours)), 0);
            }
            else
            {
                aFinalSattlement.OTHours = "0";
                aFinalSattlement.OTRate = Convert.ToDecimal("0");
                aFinalSattlement.OTAmount = Convert.ToDecimal("0");
            }

            aFinalSattlement.TiffinCharge = decimal.Round(aFinalSattlementDal.GetTiffinCharge(empid, SalaryStartDate, SalaryEndDate), 0);
            aFinalSattlement.AttnBonusRate = Convert.ToDecimal("0");
            aFinalSattlement.SpecialBonus = Convert.ToDecimal("0");
            aFinalSattlement.SalaryAdvance = aFinalSattlementDal.SalaryAdvance(empid, SalaryStartDate, SalaryEndDate);
            aFinalSattlement.Tax = Convert.ToDecimal("0");
            aFinalSattlement.Absenteeism =
                decimal.Round(
                    ((aFinalSattlement.Basic / aFinalSattlement.MonthDays) * aFinalSattlement.AbsentDays), 0);

            aFinalSattlement.OtherDeduction = aFinalSattlementDal.OtherDeduction(empid, SalaryStartDate, SalaryEndDate);

            aFinalSattlement.PayBankorCash = dataRow["PayType"].ToString();
            aFinalSattlement.BankAccNo = dataRow["BankAccNo"].ToString();
            aFinalSattlement.BankId = Convert.ToInt32(dataRow["BankId"].ToString());
            aFinalSattlement.Gross = aFinalSattlement.ActualGross + aFinalSattlement.Arrear + aFinalSattlement.MobileAllowance +
                                        aFinalSattlement.SpecialAllowance + aFinalSattlement.HolidayBillAmt +
                                        aFinalSattlement.NightBillAmt + aFinalSattlement.OtherAllowances +
                                        aFinalSattlement.TiffinCharge + aFinalSattlement.OTAmount + prfund;

            decimal totalPay = 0;
            decimal totalDeduction = 0;

            totalPay = aFinalSattlement.Gross;
            totalDeduction = aFinalSattlement.FoodCharge + aFinalSattlement.SalaryAdvance +
                             aFinalSattlement.OtherDeduction;


            aFinalSattlement.NetPayable = decimal.Round((totalPay - totalDeduction), 0);

            aFinalSattlementDal.DeleteDataForEmpSalaryGeneartorCom(aFinalSattlement);
            aFinalSattlementDal.SaveDataForEmpSalaryGeneartorCom(aFinalSattlement);


            return true;
        }
        public DataTable GetWorkDays(string empinfoId)
        {
            return aFinalSattlementDal.GetWorkDays(empinfoId);
        }
        public DataTable GetPrFundAmount(string empinfoId)
        {
            return aFinalSattlementDal.GetPrFundAmount(empinfoId);
        }
        public DataTable GetAllActiveEmployee(string EmpMasterCode)
        {
            return aFinalSattlementDal.GetAllActiveEmployee(EmpMasterCode);
        }
        public DataTable GetEmployee(string EmpMasterCode)
        {
            return aFinalSattlementDal.GetEmployee(EmpMasterCode);
        }

    }
}
