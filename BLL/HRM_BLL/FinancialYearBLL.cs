﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class FinancialYearBLL
    {
        FinancialYearDAL aFinancialYearDal = new FinancialYearDAL();
        public bool SaveFinancialYear(FinancialYearEntry financialYearEntry)
        {
            try
            {
                if (!aFinancialYearDal.HasFinancialYear(financialYearEntry))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    int code = aClsPrimaryKeyFind.PrimaryKeyMax("FyrCode", "tblFinancialYear", "HRDB");
                    aFinancialYearDal.SaveFinancialYear(financialYearEntry);
                    return true;
                }
                else
                {
                    return false;
                } 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public string CostHeadCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();

            if (Id.Length == 1)
            {
                Id = "100" + Id;
            }
            code = "FY-" + Id;
            return code;
        }

        public bool UpdateDataForFinancialYear(FinancialYearEntry aFinancialYearEntry)
        {
            return aFinancialYearDal.UpdateFinancialYear(aFinancialYearEntry);
        }

        public DataTable LoadFinancialYear()
        {
            return aFinancialYearDal.LoadFinancialYear();
        }
        public FinancialYearEntry FinancialYearEditLoad(string FiscalYearId)
        {
            return aFinancialYearDal.FinancialYearEditLoad(FiscalYearId);
        }

        public void CompanyNameLoad(DropDownList dropDownList)
        {
            aFinancialYearDal.LoadCompanyInfo(dropDownList);
        }
    }
}
