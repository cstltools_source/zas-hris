﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class FoodChargeBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        FoodChargeDAL aFoodChargeDal = new FoodChargeDAL();
        public bool SaveDataForDivision(FoodCharge aFoodCharge)
        {
            try
            {
                if (!aFoodChargeDal.HasFoodCAmount(aFoodCharge))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aFoodCharge.FoodId = aClsPrimaryKeyFind.PrimaryKeyMax("FoodId", "tblFoodCharge","HRDB");
                    aFoodChargeDal.SaveFoodCharges(aFoodCharge);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
         
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public bool UpdateaDataforaFoodCharge(FoodCharge aFoodCharge)
        {
            return aFoodChargeDal.UpdateFoodCharge(aFoodCharge);
        }

        public DataTable LoadDataforaFoodChargeView(string pram)
        {
            return aFoodChargeDal.LoadFoodChargeView(pram);
        }

        public FoodCharge FoodChargeEditLoad(string FoodId)
        {
            return aFoodChargeDal.FoodChargeEditLoad(FoodId);
        }
        public DataTable LoadEmpInfo(string empcode)
        {
            return aFoodChargeDal.LoadEmpInfo(empcode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aFoodChargeDal.LoadEmpInfoCode(EmpInfoId);
        }
        public bool ApprovalUpdateBLL(FoodCharge aFoodCharge)
        {
            return aFoodChargeDal.ApprovalUpdateDALL(aFoodCharge);
        }
        public DataTable LoadFoodChargeViewForApproval(string ActionStatus)
        {
            return aFoodChargeDal.LoadFoodChargeViewForApproval(ActionStatus);
        }
        public void DeleteDataBLL(string fId)
        {
            aFoodChargeDal.DeleteData(fId);

        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {

            aFoodChargeDal.LoadApprovalControlDAL(rdl, pageName, userName);

        }

        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aFoodChargeDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
