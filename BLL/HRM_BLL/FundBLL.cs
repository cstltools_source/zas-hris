﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class FundBll
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        FundDAL aFundDal = new FundDAL();
        public bool SaveDataForFund(List<Fund> aFundList)
        {
            try
            {
                foreach (var aFund in aFundList)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aFund.FundId = aClsPrimaryKeyFind.PrimaryKeyMax("FundId", "tblFund", "HRDB");
                    aFundDal.SaveDataForFund(aFund);
                    
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public DataTable CheckFund(string empId, string fund)
        {
            return aFundDal.CheckFund(empId, fund);
        }
        public DataTable EmpInformationBll(string empId)
        {
            return aFundDal.EmpInformationDal(empId);
        }
        public DataTable LoadFund()
        {
            return aFundDal.LoadFundView();
        }
        public void DeleteDataBLL(string PromotionId)
        {
            aFundDal.DeleteData(PromotionId);

        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aFundDal.LoadEmpInfo(EmpMasterCode);
        }
        public Fund fundEditLoad(string fundId)
        {
            return aFundDal.FundEditLoad(fundId);
        }
        public bool UpdateDataForfund(Fund aFund)
        {
            return aFundDal.Updatefund(aFund);
        }
    }
}
