﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class GeneralDutyBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        GeneralDutyDAL aGeneralDutyDal = new GeneralDutyDAL();
        public bool SaveDataForGeneralDuty(GeneralDuty aGeneralDuty)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aGeneralDuty.GDutyId = aClsPrimaryKeyFind.PrimaryKeyMax("GDutyId", "tblGeneralDuty", "HRDB");
                aGeneralDutyDal.SaveDataForGeneralDuty(aGeneralDuty);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateDataForGeneralDuty(GeneralDuty aGeneralDuty)
        {
            return aGeneralDutyDal.UpdateGeneralDuty(aGeneralDuty);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadGeneralDutyView()
        {
            return aGeneralDutyDal.LoadGeneralDutyView();
        }
        public DataTable LoadGeneralDutyViewForApproval(string actionstatus)
        {
            return aGeneralDutyDal.LoadGeneralDutyViewForApproval(actionstatus);
        }
        public GeneralDuty GeneralDutyEditLoad(string GDutyId)
        {
            return aGeneralDutyDal.GeneralDutyEditLoad(GDutyId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aGeneralDutyDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aGeneralDutyDal.LoadEmpInfoCode(EmpInfoId);
        }

        public bool ApprovalUpdateBLL(GeneralDuty aGeneralDuty)
        {
            return aGeneralDutyDal.ApprovalUpdateDAL(aGeneralDuty);
        }

        public void DeleteDataBLL(string GDutyId)
        {
            aGeneralDutyDal.DeleteData(GDutyId);

        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aGeneralDutyDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aGeneralDutyDal.LoadForApprovalConditionDAL(pageName, userName);
        }

    }
}
