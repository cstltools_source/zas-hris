﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class GroupBLL
    {
        GroupDAL aGroupDAL = new GroupDAL();
        public bool SaveDataForGroup(List<Group> aGroupList)
        {
            try
            {
                foreach (var aGroup in aGroupList)
                {
                    if (!aGroupDAL.HasEmp(aGroup))
                    {
                        aGroupDAL.SaveGroupInfo(aGroup);
                    }
                    else
                    {

                        aGroupDAL.UpdateGroupInfo(aGroup);
                    } 
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool DelGroupEmpInfo(string empId)
        {
            return aGroupDAL.DelGroupEmpInfo(empId);
        }

        public DataTable LoadDepartmentWiseEmp(string deptid)
        {
            return aGroupDAL.LoadDepartmentWiseEmp(deptid);
        }

        public DataTable LoadSectionWiseEmp(string sectionId)
        {
            return aGroupDAL.LoadSectionWiseEmp(sectionId);
        }

        public DataTable LoadEmpInfo(string empcode)
        {
            return aGroupDAL.LoadEmpInfo(empcode);
        }
        public bool UpdateDataForGroup(Group aGroup)
        {
            return aGroupDAL.UpdateGroupInfo(aGroup);
        }
        public DataTable LoadGroupView()
        {
            return aGroupDAL.LoadGroupView();
        }
        public DataTable LoadGroupRpt(string groupId,string fromdt,string todt)
        {
            return aGroupDAL.LoadGroupReport(groupId,fromdt,todt);
        }
        public DataTable LoadGroupRpt(string fromdt, string todt)
        {
            return aGroupDAL.LoadGroupReport(fromdt,todt);
        }
        public void LoadGroup(DropDownList ddl)
        {
            aGroupDAL.LoadGroupName(ddl);
        }
        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            aGroupDAL.LoadDepartmentName(ddl,divisionId);
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            aGroupDAL.LoadDivisionName(ddl);
        }

        public void LoadGroupByUnit(DropDownList ddl, string unitId)
        {
            aGroupDAL.LoadGroupByUnit(ddl, unitId);
        }
    }
}
