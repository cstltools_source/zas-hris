﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class HolidayInfoBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        HolidayInfoDAL aHolidayInfoDal = new HolidayInfoDAL();
        public bool SaveHolidayInfoData(HolidayInfo aHolidayInfo)
        {
            try
            {
                DateTime dtStart = aHolidayInfo.HolidayfromDate;
                DateTime dtEnd = aHolidayInfo.HolidaytoDate;
                if (!aHolidayInfoDal.HasHolidayDate(aHolidayInfo))
                {
                    while (dtStart <= dtEnd)
                    {
                        ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                        aHolidayInfo.HolidayId = aClsPrimaryKeyFind.PrimaryKeyMax("HolidayId", "tblHolidayInformation",
                            "HRDB");
                        aHolidayInfo.HolidayfromDate = dtStart;
                        aHolidayInfoDal.SaveDataForHolidayInfo(aHolidayInfo);
                        dtStart = dtStart.AddDays(1);
                    }
                    return true;    
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadUnitName(DropDownList ddl)
        {
            HolidayInfoDAL aHolidayInfoDal = new HolidayInfoDAL();
            aHolidayInfoDal.LoadUnitName(ddl);
        }

        public void LoadCompanyName(DropDownList ddl)
        {
            HolidayInfoDAL aHolidayInfoDal = new HolidayInfoDAL();
            aHolidayInfoDal.LoadCompanyName(ddl);
        }

        public DataTable LoadHolidaView()
        {
            return aHolidayInfoDal.LoadHolidayView();
        }

        public HolidayInfo HolidayEditLoad(string holidaId)
        {
            return aHolidayInfoDal.HolidayInfoEditLoad(holidaId);
        }

        public bool UpdateDataForHoliday(HolidayInfo aHolidayInfo)
        {
            return aHolidayInfoDal.UpdateHolidayInfo(aHolidayInfo);
        }


    }
}
