﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class HolidayReplaceBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal=new ClsCommonOperationDAL();
        HolidayReplaceDAL aHolidayReplaceDal = new HolidayReplaceDAL();
        public bool SaveHolidayReplaceData(HolidayReplace aHolidayReplace)
        {
            try
            {
                //if (!aHolidayReplaceDal.HasHolidayDate(aHolidayReplace))
                //{
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aHolidayReplace.WHRId = aClsPrimaryKeyFind.PrimaryKeyMax("WHRId", "dbo.tblWeeklyHolidayReplacement", "HRDB");

                    aHolidayReplaceDal.SaveDataForHolidayReplace(aHolidayReplace);
                    return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool HasWeeklyHoliday(string dayname, string empid)
        {
            return aHolidayReplaceDal.HasWeeklyHoliday(dayname, empid);
        }
        public string DayName(string dayName)
        {
            return aHolidayReplaceDal.DayName(dayName);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aHolidayReplaceDal.LoadEmpInfo(EmpMasterCode);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public bool ApprovalUpdateDAL(HolidayReplace aHolidayReplace)
        {
            return aHolidayReplaceDal.ApprovalUpdateDAL(aHolidayReplace);
        }
        public DataTable LoadHolidayReplaceViewApproval(string ActionStatus)
        {
            return aHolidayReplaceDal.LoadHolidayReplaceViewApproval(ActionStatus);
        }

        public DataTable LoadHolidaView()
        {
            return aHolidayReplaceDal.LoadHolidayView();
        }

        public HolidayReplace HolidayEditLoad(string holidaId)
        {
            return aHolidayReplaceDal.HolidayReplaceEditLoad(holidaId);
        }

        public bool UpdateDataForHoliday(HolidayReplace aHolidayReplace)
        {
            return aHolidayReplaceDal.UpdateHolidayReplace(aHolidayReplace);
        }
        public void DeleteDataBLL(string WHRId)
        {
            aHolidayReplaceDal.DeleteData(WHRId);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aHolidayReplaceDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aHolidayReplaceDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
