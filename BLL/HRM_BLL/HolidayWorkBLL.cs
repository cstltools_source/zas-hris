﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class HolidayWorkBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        HolidayWorkDAL aHolidayWorkDal = new HolidayWorkDAL();
        public bool SaveDataForHolidayWork(HolidayWork aHolidayWork,HolidayInfo aHolidayInfo)
        {
            try
            {
                
                    if (!aHolidayWorkDal.HasHolidayDate(aHolidayInfo))
                    {

                        if (!aHolidayWorkDal.HasWeeklyHoliday(aHolidayWorkDal.DayName(aHolidayWork.HworkDate.ToString()), aHolidayWork.EmpInfoId.ToString()))
                        {
                            if (aHolidayWorkDal.HasAlterNativedate(aHolidayWorkDal.DayName(aHolidayWork.HworkDate.ToString())))
                            {
                                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                                aHolidayWork.HolidayWorkId = aClsPrimaryKeyFind.PrimaryKeyMax("HolidayWorkId", "tblHolidayWork", "HRDB");
                                aHolidayWorkDal.SaveDataForHolidayWork(aHolidayWork);
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else
                        {
                            ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                            aHolidayWork.HolidayWorkId = aClsPrimaryKeyFind.PrimaryKeyMax("HolidayWorkId", "tblHolidayWork", "HRDB");
                            aHolidayWorkDal.SaveDataForHolidayWork(aHolidayWork);
                            return true;
                        }

                    }
                    else
                    {
                        ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                        aHolidayWork.HolidayWorkId = aClsPrimaryKeyFind.PrimaryKeyMax("HolidayWorkId", "tblHolidayWork", "HRDB");
                        aHolidayWorkDal.SaveDataForHolidayWork(aHolidayWork);
                        return true;
                    }    
                
                
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool HasHolidayWork(HolidayWork aHolidayWork)
        {
            return aHolidayWorkDal.HasHolidayWork(aHolidayWork);
        }
        public bool UpdateDataForHolidayWork(HolidayWork aHolidayWork)
        {
            return aHolidayWorkDal.UpdateHolidayWork(aHolidayWork);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadHolidayWorkView()
        {
            return aHolidayWorkDal.LoadHolidayWorkView();
        }
        public DataTable LoadHolidayWorkViewForApproval(string actionstatus)
        {
            return aHolidayWorkDal.LoadHolidayWorkViewForApproval(actionstatus);
        }
        public HolidayWork HolidayWorkEditLoad(string HolidayWorkId)
        {
            return aHolidayWorkDal.HolidayWorkEditLoad(HolidayWorkId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aHolidayWorkDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aHolidayWorkDal.LoadEmpInfoCode(EmpInfoId);
        }

        public bool ApprovalUpdateBLL(HolidayWork aHolidayWork)
        {
            return aHolidayWorkDal.ApprovalUpdateDAL(aHolidayWork);
        }
        public void DeleteDataBLL(string HolidayWorkId)
        {
            aHolidayWorkDal.DeleteData(HolidayWorkId);

        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aHolidayWorkDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aHolidayWorkDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
