﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class JobDescriptionBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        JobDescriptionDAL aDescriptionDal = new JobDescriptionDAL();
        public bool SaveDataForJobDescription(JobDescription aJobDescription)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aJobDescription.JobDscId = aClsPrimaryKeyFind.PrimaryKeyMax("JobDscId", "tblJobDescription", "HRDB");
                aDescriptionDal.SaveJobDescreption(aJobDescription);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aDescriptionDal.LoadDesignationName(aDropDownList);
        }

        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList)
        {
            aDescriptionDal.LoadDepartmentName(aDropDownList);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public bool UpdateDataForJobDescription(JobDescription aJobDescription)
        {
            return aDescriptionDal.UpdateJobDescription(aJobDescription);
        }

        public DataTable LoadJobDescriptionView()
        {
            return aDescriptionDal.LoadJobDescriptionView();
        }

        public JobDescription JobDescriptionEditLoad(string JobDscId)
        {
            return aDescriptionDal.JobDescriptionEditLoad(JobDscId);
        }
        public DataTable LoadEmpInfoCode(string empcode)
        {
            return aDescriptionDal.LoadEmpInfoCode(empcode);
        }
        public void DeleteDataBLL(string HolidayWorkId)
        {
            aDescriptionDal.DeleteData(HolidayWorkId);

        }
    }
}
