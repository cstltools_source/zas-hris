﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class JobLeftBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        JobLeftDAL aJobLeftDal = new JobLeftDAL();
        public bool SaveDataForEmpSalBenefit(JobLeft aJobLeft)
        {
            try
            {
                if (!aJobLeftDal.HasJobleft(aJobLeft))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aJobLeft.JobLeftId = aClsPrimaryKeyFind.PrimaryKeyMax("JobLeftId", "tblJobLeft", "HRDB");
                    aJobLeftDal.SaveJobleft(aJobLeft);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aJobLeftDal.LoadDesignationName(aDropDownList);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aJobLeftDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aJobLeftDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aJobLeftDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aJobLeftDal.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aJobLeftDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aJobLeftDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadEmpGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aJobLeftDal.LoadEmpGradeName(aDropDownList);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aJobLeftDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(JobLeft aJobLeft)
        {
            return aJobLeftDal.UpdateJobleft(aJobLeft);
        }
        public DataTable LoadEmpJobLeftView()
        {
            return aJobLeftDal.LoadJobleftView();
        }
        public JobLeft EmpSalBenefitEditLoad(string JobLeftId)
        {
            return aJobLeftDal.EmpJobleftEditLoad(JobLeftId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aJobLeftDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aJobLeftDal.LoadEmpInfoCode(EmpInfoId);
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aJobLeftDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aJobLeftDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aJobLeftDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aJobLeftDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aJobLeftDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aJobLeftDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aJobLeftDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aJobLeftDal.LoadEmpType(EmpTypeId);
        }

        public bool ApprovalUpdateBLL(JobLeft aJobLeft)
        {
            return aJobLeftDal.ApprovalUpdateDAL(aJobLeft);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aJobLeftDal.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public DataTable LoadJobLeftViewForApproval(string ActionStatus)
        {
            return aJobLeftDal.LoadJobLeftViewForApproval(ActionStatus);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aJobLeftDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aJobLeftDal.LoadForApprovalConditionDAL(pageName, userName);
        }

        public void DeleteDataBLL(string HolidayWorkId)
        {
            aJobLeftDal.DeleteData(HolidayWorkId);
        }
    }
}
