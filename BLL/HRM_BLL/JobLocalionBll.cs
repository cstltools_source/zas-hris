﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.HRM_DAL;
using DAO.HRM_Entities;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace BLL.HRM_BLL
{
    public class JobLocalionBll
    {
        JobLocationDal2 aDivisionDal = new JobLocationDal2();
        public bool SaveDataForDivision(JobLocationDao2 aDivision)
        {
            try
            {
                if (!aDivisionDal.HasDivisionName(aDivision))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aDivision.JobLocationisionId = aClsPrimaryKeyFind.PrimaryKeyMax("DivisionId", "tblDivision", "HRDB");
                    aDivision.JobLocationCode = DivisionCodeGenerator(aDivision.JobLocationisionId);
                    aDivisionDal.SaveDevision(aDivision);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public string DivisionCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "000" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "00" + Id;
            }
            code = "DIV-" + Id;
            return code;
        }
        public bool UpdateaDataforDivision(JobLocationDao2 aDivision)
        {
            return aDivisionDal.UpdateDivision(aDivision);
        }
        public DataTable LoadDataforDivisionView()
        {
            return aDivisionDal.LoadDivisionView();
        }
        public Division DivisionEditLoad(string DivisionId)
        {
            return aDivisionDal.DivisionEditLoad(DivisionId);
        }
    }
}
