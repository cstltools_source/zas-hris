﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class LateApproveBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        LateApproveDAL aLateApproveDal = new LateApproveDAL();
        public bool SaveDataForLateApprove(LateApprove aLateApprove)
        {
            try
            {
                if (!aLateApproveDal.HasLateApprove(aLateApprove))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aLateApprove.LateApproveId = aClsPrimaryKeyFind.PrimaryKeyMax("LateApproveId", "tblLateApprove", "HRDB");
                    aLateApproveDal.SaveLateApprove(aLateApprove);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aLateApproveDal.LoadDesignationName(aDropDownList);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aLateApproveDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aLateApproveDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aLateApproveDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aLateApproveDal.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aLateApproveDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aLateApproveDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aLateApproveDal.LoadSalGradeName(aDropDownList);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aLateApproveDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(LateApprove aLateApprove)
        {
            return aLateApproveDal.UpdateLateApprove(aLateApprove);
        }
        public DataTable LoadLateApprove()
        {
            return aLateApproveDal.LoadLateApproveView();
        }
        public DataTable LoadLateTime(DateTime date, string empid)
        {
            return aLateApproveDal.LoadLateTime(date, empid);
        }
        public LateApprove LateApproveEditLoad(string LateApproveId)
        {
            return aLateApproveDal.LateApproveEditLoad(LateApproveId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aLateApproveDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aLateApproveDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aLateApproveDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aLateApproveDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aLateApproveDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aLateApproveDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aLateApproveDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aLateApproveDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aLateApproveDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aLateApproveDal.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(LateApprove aLateApprove)
        {
            return aLateApproveDal.ApprovalUpdateDAL(aLateApprove);
        }
        public DataTable LoadLateApproveViewForApproval(string ActionStatus)
        {
            return aLateApproveDal.LoadLateApproveViewForApproval(ActionStatus);
        }
        public bool PlaceLate(string empid, DateTime date, string status)
        {
            return aLateApproveDal.PlaceLate(empid,date,status);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aLateApproveDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aLateApproveDal.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void DeleteDataBLL(string LateAppId)
        {
            aLateApproveDal.DeleteData(LateAppId);

        }
        public bool VarifyLate(string attdate,string empid)
        {
            DataTable dt = new DataTable();
            dt = aLateApproveDal.VarifyLate(attdate, empid);
            if (dt.Rows.Count > 0)
                return true;
            else
            {
                return false;
            }
        }
    }
}
