﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class LeaveAvailBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        LeaveAvailDAL availDal = new LeaveAvailDAL();
        public bool SaveDateForLeaveAvail(LeaveAvail avail)
        {
            try
            {
                if (LoadLeave(avail.FromDate.ToString("dd-MMM-yyyy"),avail.EmpInfoId.ToString()).Rows.Count < 1)
                {


                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    avail.LeaveAvailId = aClsPrimaryKeyFind.PrimaryKeyMax("LeaveAvailId", "tblLeaveAvail", "HRDB");
                    avail.Status = "Posted";
                    availDal.SaveLeaveAvail(avail);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public bool UpdateDataForLeaveAvail(LeaveAvail avail)
        {
            return availDal.UpdateLeaveAvailInfo(avail);
        }
        public DataTable LoadLeave(string fromdate,string empInfoId)
        {
            return availDal.LoadLeave(fromdate,empInfoId);
        }
        public bool UpdateLeaveInventory(LeaveInventory aLeaveInventory)
        {
            return availDal.UpdateLeaveInventory(aLeaveInventory);
        }
        public void LoadLeaveName(DropDownList ddl)
        {
            availDal.LoadLeaveName(ddl);
        }
        public void LoadLeaveName(DropDownList ddl,string empId, string year)
        {
            availDal.LoadLeaveName(ddl, empId, year);
        }

        public DataTable LoadLeaveAvailView()
        {
            return availDal.LoadLeaveAvailView();
        }
        public DataTable LoadLeaveAvailView(string fromdate, string todate)
        {
            return availDal.LoadLeaveAvailView(fromdate, todate);
        }
        public DataTable LoadLeaveEmployeeLeaveQuantity(string empcode)
        {
            return availDal.LoadLeaveEmployeeLeaveQuantity(empcode);
        }
        public LeaveAvail LeaveAvailEditLoad(string leaveId)
        {
            return availDal.LeaveAvailEditLoad(leaveId);
        }
        public DataTable LeaveInfo(string empinfoId)
        {
            return availDal.LeaveInfo(empinfoId);
        }
        public DataTable LeaveQtyCheck(string leaveId, string empId, string year)
        {
            return availDal.LeaveQtyCheck(leaveId, empId, year);
        }
        public DataTable GetInventory(string leaveInventoryId)
        {
            return availDal.GetInventory(leaveInventoryId);
        }
        public void LeaveUpdateIntoAttRecord(List<LeaveAvail> leaveAvailList)
        {
            AttedanceOperationBLL attedanceOperationBll = new AttedanceOperationBLL();

            foreach (var leaveAvail in leaveAvailList)
            {
                DateTime fromDate = leaveAvail.FromDate;
                DateTime toDate = leaveAvail.ToDate;
                while (fromDate <= toDate)
                {
                    DataTable dtcheckgovhol = availDal.CheckGovtHoliday(fromDate.ToString(), fromDate.ToString(),
                        leaveAvail.EmpInfoId.ToString());
                    DataTable dtweeklyholiday = availDal.CheckWeekHoliday(leaveAvail.EmpInfoId.ToString(),
                        fromDate.DayOfWeek.ToString());

                    if (dtcheckgovhol.Rows.Count<1 && dtweeklyholiday.Rows.Count<1)
                    {
                        
                        attedanceOperationBll.UpdateOperationBLL(fromDate, leaveAvail.EmpInfoId, "LV", leaveAvail.LeaveName);
                    }
                    
                    
                    fromDate=fromDate.AddDays(1.0);
                }
            }
        }
        public DataTable DeptHead(string empinfoId)
        {
            return availDal.DeptHead(empinfoId);
        }
        public DataTable LoadLeaveApproveViewForApproval(string ActionStatus)
        {
            return availDal.LoadLeaveAvailViewApproval(ActionStatus);
        }
        public DataTable DeptHeadLoadLeaveApproveViewForApproval(string ActionStatus,string deptId)
        {
            return availDal.DeptHeadLoadLeaveAvailViewApproval(ActionStatus,deptId);
        }
        public bool ApprovalUpdateBLL(LeaveAvail avail)
        {
            return availDal.ApprovalUpdateDAL(avail);
        }
        
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            availDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return availDal.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void DeleteDataBLL(string LMID)
        {
            availDal.DeleteData(LMID);
        }
    }
}
