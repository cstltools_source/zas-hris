﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class LeaveBLL
    {
        LeaveDAL aLeaveDal = new LeaveDAL();
        public bool SaveDataForLeave(Leave aLeave)
        {
            try
            {
                if (!aLeaveDal.HasLeaveName(aLeave))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aLeave.LeaveId = aClsPrimaryKeyFind.PrimaryKeyMax("LeaveId", "tblLeave", "HRDB");
                    aLeave.LeaveCode = DesignationCodeGenerator(aLeave.LeaveId);
                    aLeaveDal.SaveLeaveInfo(aLeave);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }


        public string DesignationCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "LEV-" + Id;
            return code;
        }
        public DataTable LoadLeaveView()
        {
            return aLeaveDal.LoadLeaveView();
        }
        public Leave LeaveEditLoad(string leaveId)
        {
            return aLeaveDal.LeaveEditLoad(leaveId);
        }
        public bool UpdateDataForLeave(Leave aLeave)
        {
            return aLeaveDal.UpdateLeaveInfo(aLeave);
        }
    }
}
