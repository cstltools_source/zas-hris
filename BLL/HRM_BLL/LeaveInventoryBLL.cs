﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class LeaveInventoryBLL
    {
        LeaveInventoryDAL aLeaveInventoryDal = new LeaveInventoryDAL();
        public bool SaveDataForLeave(LeaveInventory aLeaveInventory)
        {
            try
            {
                if(!aLeaveInventoryDal.HasLeaveName(aLeaveInventory))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aLeaveInventory.LeaveInventoryId = aClsPrimaryKeyFind.PrimaryKeyMax("LeaveInventoryId","tblLeaveInventory", "HRDB");
                    aLeaveInventoryDal.SaveLeaveInventory(aLeaveInventory);
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool SaveDataForLeaveList(List<LeaveInventory> aLeaveInventoryList)
        {
            try
            {
                foreach (var aLeaveInventory in aLeaveInventoryList)
                {
                    if (!aLeaveInventoryDal.HasLeaveName(aLeaveInventory))
                    {
                        ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                        aLeaveInventory.LeaveInventoryId = aClsPrimaryKeyFind.PrimaryKeyMax("LeaveInventoryId", "tblLeaveInventory", "HRDB");
                        aLeaveInventoryDal.SaveLeaveInventory(aLeaveInventory);
                        
                    }
                    else
                    {
                        return false;
                    }    
                }

                return true;


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public void LeaveGenerator(string leaveid, string leavename, string leaveyear, string dayqty, DataRow dataRow)
        {
            LeaveInventory aLeaveInventory = new LeaveInventory()
                                                 {
                                                     LeaveId = Convert.ToInt32(leaveid),
                                                     LeaveName = leavename,
                                                     LeaveYear = leaveyear,
                                                     DayQty = dayqty,
                                                     YearDayQty = Convert.ToDecimal(dayqty),
                                                     EmpMasterCode = dataRow["EmpMasterCode"].ToString(),
                                                     EmpName = dataRow["EmpName"].ToString(),

                                                     EmpInfoId =
                                                         Convert.ToInt32(dataRow["EmpInfoId"].ToString()),
                                                 };
            SaveDataForLeave(aLeaveInventory);
        }

        public DataTable LoadEmpInfo(string parameter)
        {
            return aLeaveInventoryDal.LoadEmpInfo(parameter);
        }
        public DataTable LoadEmpInfoCode(string empcode)
        {
            return aLeaveInventoryDal.LoadEmpInfoCode(empcode);
        }

        public bool UpdateDataForLeaveInventory(LeaveInventory aLeaveInventory)
        {
            return aLeaveInventoryDal.UpdateLeaveInfo(aLeaveInventory);
        }
        public DataTable RptHeader()
        {
            return aLeaveInventoryDal.RptHeader();
        }
        public DataTable LeaveInventoryRpt(string parameter)
        {
            return aLeaveInventoryDal.LeaveInventoryRpt(parameter);
        }
        public bool DeleteLeave(Attendence attendence)
        {
            return aLeaveInventoryDal.DeleteLeave(attendence);
        }
        public bool UpdateLeave(string LeaveInventoryId, string DayQty)
        {
            return aLeaveInventoryDal.UpdateLeave(LeaveInventoryId, DayQty);
        }
        public DataTable LeaveInv(string empinfoId, string year, string leaveName)
        {
            return aLeaveInventoryDal.LeaveInv(empinfoId, year, leaveName);
        }
        public DataTable LoadLeave(string year, string empinfoId, string leaveName)
        {
            return aLeaveInventoryDal.LoadLeave(year, empinfoId, leaveName);
        }
        public bool DeleteLeaveAvail(string availId)
        {
            return aLeaveInventoryDal.DeleteLeaveAvail(availId);
        }
        public DataTable LoadLeaveInv(string empinfoId, string parameter)
        {
            return aLeaveInventoryDal.LoadLeaveInv(empinfoId, parameter);
        }
        public bool DeleteLeaveAvail(LeaveAvail avail)
        {
            return aLeaveInventoryDal.DeleteLeaveAvail(avail);
        }
        public bool UpdateLeaveAvail(LeaveAvail avail, DateTime fromdt, DateTime todt)
        {
            return aLeaveInventoryDal.UpdateLeaveAvail(avail, fromdt, todt);
        }
        public bool UpdateLeave(LeaveInventory aLeaveInventory)
        {
            return aLeaveInventoryDal.UpdateLeave(aLeaveInventory);
        }
        public DataTable LeaveAvailRpt(string parameter)
        {
            return aLeaveInventoryDal.LeaveAvailRpt(parameter);
        }
        public void LoadLeaveName(DropDownList ddl)
        {
            aLeaveInventoryDal.LoadLeaveName(ddl);
        }
        public DataTable LoadLeaveView()
        {
            return aLeaveInventoryDal.LoadLeaveInventoryView();
        }
        public LeaveInventory LeaveInventoryEditLoad(string leaveId)
        {
            return aLeaveInventoryDal.LeaveInventoryEditLoad(leaveId);
        }
        public DataTable CheckGovtHoliday(string fromdate, string todate, string empinfoId)
        {
            return aLeaveInventoryDal.CheckGovtHoliday(fromdate, todate, empinfoId);
        }
        public DataTable CheckWeeklyHolidayDays(string fromdt, string todt, string empinfoId)
        {
            return aLeaveInventoryDal.CheckWeeklyHolidayDays(fromdt, todt, empinfoId);
        }
        public List<LeaveInventory> GetEmployeeNameAndDayQty(string EmpInfoId)
        {
            return aLeaveInventoryDal.ViewEmpNameAndDayQty(EmpInfoId);
        }
        public DataTable GetEmpInfo(string id)
        {
            return aLeaveInventoryDal.GetEmpInfo(id);
        }


        public DataTable AllLeaveReport(string year)
        {
            return aLeaveInventoryDal.AllLeaveReport(year);
        }
        public DataTable LeaveReport(string parameter,string year)
        {
            return aLeaveInventoryDal.LeaveReport(parameter,year);
        }
        public DataTable LeaveAvailReport(string empcode,string  year)
        {
            return aLeaveInventoryDal.LeaveAvailReport(empcode, year);
        }
        public DataTable LeaveAvailReport(string leaveavailId)
        {
            return aLeaveInventoryDal.LeaveAvailReport(leaveavailId);
        }
        public DataTable LeaveInventoryReport2(string empcode)
        {
            return aLeaveInventoryDal.LeaveInventoryReport2(empcode);
        }
    }
}
