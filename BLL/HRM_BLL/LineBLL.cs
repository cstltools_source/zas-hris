﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class LineBLL
    {
        LineDAL aLineDAL = new LineDAL();
        public bool SaveDataForLine(Line aLine)
        {
            try
            {
                if (!aLineDAL.HasLineName(aLine))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aLine.LineId = aClsPrimaryKeyFind.PrimaryKeyMax("LineId", "tblLine", "HRDB");
                    aLineDAL.SaveLineInfo(aLine);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool UpdateDataForLine(Line aLine)
        {
            return aLineDAL.UpdateLineInfo(aLine);
        }
        public DataTable LoadLineView()
        {
            return aLineDAL.LoadLineView();
        }

        public Line LineEditLoad(string LineId)
        {
            return aLineDAL.LineEditLoad(LineId);
        }
    }
}
