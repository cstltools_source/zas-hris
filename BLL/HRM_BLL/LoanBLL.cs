﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class LoanBll
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        LoanMasterDAL aLoanMasterDal = new LoanMasterDAL();
        public bool SaveDataForLoan(LoanMaster aLoanMaster,List<LoanDetail> aLoanDetailList )
        {
            try
            {
                if (!aLoanMasterDal.HasLoanMaster(aLoanMaster))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aLoanMaster.LoanId = aClsPrimaryKeyFind.PrimaryKeyMax("LoanId", "tblLoanMaster", "HRDB");
                    aLoanMasterDal.SaveDataForLoanMaster(aLoanMaster);
                    foreach (var aloanDetail in aLoanDetailList)
                    {
                        aloanDetail.LoanId = aLoanMaster.LoanId;
                        aloanDetail.LoanDetailId = aClsPrimaryKeyFind.PrimaryKeyMax("LoanDetailId", "tblLoanDetail", "HRDB");
                        aLoanMasterDal.SaveDataForLoanDetail(aloanDetail);
                    }

                    return true;    
                }
                else
                {
                    return false;
                }
                
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool SaveDataForLoanUpdate(LoanMaster aLoanMaster, List<LoanDetail> aLoanDetailList)
        {
            try
            {
                //if (!aLoanMasterDal.HasLoanMaster(aLoanMaster))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aLoanMaster.LoanId = aClsPrimaryKeyFind.PrimaryKeyMax("LoanId", "tblLoanMaster", "HRDB");
                    aLoanMasterDal.SaveDataForLoanMaster(aLoanMaster);
                    foreach (var aloanDetail in aLoanDetailList)
                    {
                        aloanDetail.LoanId = aLoanMaster.LoanId;
                        aloanDetail.LoanDetailId = aClsPrimaryKeyFind.PrimaryKeyMax("LoanDetailId", "tblLoanDetail", "HRDB");
                        aLoanMasterDal.SaveDataForLoanDetail(aloanDetail);
                    }

                    return true;
                }
               

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool SaveDataForLoanDetail( List<LoanDetail> aLoanDetailList)
        {
            try
            {
                
                    foreach (var aloanDetail in aLoanDetailList)
                    {
                        ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                        aloanDetail.LoanDetailId = aClsPrimaryKeyFind.PrimaryKeyMax("LoanDetailId", "tblLoanDetail", "HRDB");
                        aLoanMasterDal.SaveDataForLoanDetail(aloanDetail);
                    }

                    return true;
               


            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool DeleteNewData(string loanId, string startdate, string empInfoId)
        {
            return aLoanMasterDal.DeleteNewData(loanId, startdate, empInfoId);
        }

        public bool ApprovalUpdateBLL(LoanMaster aLoan)
        {
            return aLoanMasterDal.ApprovalUpdateDAL(aLoan);
        }
        public DataTable LoadLoanApproveViewForApproval(string ActionStatus)
        {
            return aLoanMasterDal.LoadLoanApproveViewForApproval(ActionStatus);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aLoanMasterDal.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aLoanMasterDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public DataTable EmpInformationBll(string empId)
        {
            return aLoanMasterDal.EmpInformationDal(empId);
        }
        public void DeleteDataBLL(string LoanId)
        {
            aLoanMasterDal.DeleteData(LoanId);

        }

        public DataTable PaidLoanInform(string epminfoId, string startdate)
        {
            return aLoanMasterDal.PaidLoanInform(epminfoId, startdate);
        }

        public DataTable LoanInform(string epminfoId)
        {
            return aLoanMasterDal.LoanInform(epminfoId);
        }
        public bool DeleteLoanMaster(string loanId)
        {
            return aLoanMasterDal.DeleteLoanMaster(loanId);
        }
        public bool DeleteLoanDetail(string loanId)
        {
            return aLoanMasterDal.DeleteLoanDetail(loanId);
        }
        public DataTable LoadLoanByIdNew(string id)
        {
            return aLoanMasterDal.LoadLoanById(id);
        }

        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadLoan()
        {
            return aLoanMasterDal.LoadLoan();
        }
        public DataTable LoadLoanById(string id)
        {
            return aLoanMasterDal.LoadLoan();
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aLoanMasterDal.LoadEmpInfo(EmpMasterCode);
        }
    }
}
