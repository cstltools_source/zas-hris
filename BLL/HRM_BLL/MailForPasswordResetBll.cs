﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.HRM_DAL;

namespace BLL.HRM_BLL
{
    public class MailForPasswordResetBll
    {
        PasswordResetMailDal aResetMailDal = new PasswordResetMailDal();
        public DataTable LoadUserIsValidOrNotInfo(string email)
        {
            return aResetMailDal.GetUserIsValidOrNotInfo(email);
        }

        public bool UpdateUserPassword(string password, string id)
        {
            return aResetMailDal.UpdateUserPasswordById(password, id);
        }
    }
}
