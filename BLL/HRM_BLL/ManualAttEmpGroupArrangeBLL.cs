﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ManualAttEmpGroupArrangeBLL
    {
        ManualAttEmpGroupArrangeDAL aGroupDAL = new ManualAttEmpGroupArrangeDAL();
        public bool SaveDataForGroup(List<Group> aGroupList)
        {
            try
            {
                foreach (var aGroup in aGroupList)
                {
                    if (!aGroupDAL.HasEmp(aGroup))
                    {
                        aGroupDAL.SaveGroupInfo(aGroup);
                    }
                    else
                    {

                        aGroupDAL.UpdateGroupInfo(aGroup);
                    } 
                }
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public DataTable LoadDepartmentWiseEmp(string deptid)
        {
            return aGroupDAL.LoadDepartmentWiseEmp(deptid);
        }
        public DataTable LoadEmpInfo(string empcode)
        {
            return aGroupDAL.LoadEmpInfo(empcode);
        }
        public bool UpdateDataForGroup(Group aGroup)
        {
            return aGroupDAL.UpdateGroupInfo(aGroup);
        }
        public DataTable LoadGroupEmp(string deptid,string groupId)
        {

            return aGroupDAL.LoadGroupEmp(deptid,groupId);
        }
        public DataTable LoadGroupView()
        {
            return aGroupDAL.LoadGroupView();
        }
        public DataTable LoadGroupRpt(string groupId)
        {
            return aGroupDAL.LoadGroupReport(groupId);
        }
        public void LoadGroup(DropDownList ddl)
        {
            aGroupDAL.LoadGroupName(ddl);
        }
        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            aGroupDAL.LoadDepartmentName(ddl,divisionId);
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            aGroupDAL.LoadDivisionName(ddl);
        }
        public bool DeleteGroup(Group aGroup)
        {
            return aGroupDAL.DeleteGroup(aGroup);
        }
    }
}
