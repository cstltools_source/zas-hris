﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ManualAttGroupWiseEntryBLL
    {
        ManualAttGroupWiseEntryDAL aShiftWiseGroupDal = new ManualAttGroupWiseEntryDAL();
        ManualAttendenceDAL aManualAttendenceDal = new ManualAttendenceDAL();

        public bool SaveShiftWiseGroupData(ShiftWiseGroup aShiftWiseGroup)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aShiftWiseGroup.GSAId = aClsPrimaryKeyFind.PrimaryKeyMax("GSAId", "dbo.tblManualAttEmpGroupWiseDetail", "HRDB");
                aShiftWiseGroupDal.SaveDataForShiftWiseGroup(aShiftWiseGroup);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool SaveDataForManualAttendence(ManualAttendence aManualAttendence)
        {
            try
            {
                DateTime dtStart = aManualAttendence.FromDate;
                DateTime dtEnd = aManualAttendence.ToDate;
                while (dtStart <= dtEnd)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                   
                    aManualAttendence.MAttendenceId = aClsPrimaryKeyFind.PrimaryKeyMax("MAttendenceId", "tblManualAttendence", "HRDB");
                    aManualAttendence.ATTDate = dtStart;
                    aManualAttendence.DayName = Convert.ToDateTime(dtStart).DayOfWeek.ToString();
                    aManualAttendenceDal.SaveManualAttendenceInfo(aManualAttendence);
                    dtStart = dtStart.AddDays(1);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public DataTable LoadEmpID(int id)
        {
           return aShiftWiseGroupDal.LoadEmpID(id);
        }
        public void LoadGroup(DropDownList ddl)
        {
            aShiftWiseGroupDal.LoadGroupName(ddl);
        }
        public DataTable LoadDate(string groupId, string fromdt, string todt)
        {
            return aShiftWiseGroupDal.LoadDate(groupId, fromdt, todt);
        }
        public void LoadShift(DropDownList ddl)
        {
            aShiftWiseGroupDal.LoadShift(ddl);
        }
        public bool HasWeeklyHoliday(string dayname, string empid)
        {
            return aShiftWiseGroupDal.HasWeeklyHoliday(dayname, empid);
        }
        public string DayName(string dayName)
        {
            return aShiftWiseGroupDal.DayName(dayName);
        }
        public DataTable LoadShift(string shiftId)
        {
            return aShiftWiseGroupDal.LoadShift(shiftId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aShiftWiseGroupDal.LoadEmpInfo(EmpMasterCode);
        }

        public bool ApprovalUpdateDAL(ShiftWiseGroup aShiftWiseGroup)
        {
            return aShiftWiseGroupDal.ApprovalUpdateDAL(aShiftWiseGroup);
        }
        public DataTable LoadShiftWiseGroupViewApproval(string actionstatus)
        {
            return aShiftWiseGroupDal.LoadShiftWiseGroupViewApproval(actionstatus);
        }

        public DataTable LoadHolidaView()
        {
            return aShiftWiseGroupDal.LoadHolidayView();
        }

        //public ShiftWiseGroup HolidayEditLoad(string holidaId)
        //{
        //    return aShiftWiseGroupDal.ShiftWiseGroupEditLoad(holidaId);
        //}

        //public bool UpdateDataForHoliday(ShiftWiseGroup aShiftWiseGroup)
        //{
        //    return aShiftWiseGroupDal.UpdateShiftWiseGroup(aShiftWiseGroup);
        //}

        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aShiftWiseGroupDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aShiftWiseGroupDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
