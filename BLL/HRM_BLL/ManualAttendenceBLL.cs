﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ManualAttendenceBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        ManualAttendenceDAL aManualAttendenceDal = new ManualAttendenceDAL();
        public bool SaveDataForManualAttendence(ManualAttendence aManualAttendence)
        {
            try
            {
                DateTime dtStart = aManualAttendence.ATTDate;
                DateTime dtEnd = aManualAttendence.ToDate;
                while (dtStart <= dtEnd)
                {

                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aManualAttendence.MAttendenceId = aClsPrimaryKeyFind.PrimaryKeyMax("MAttendenceId",
                        "tblManualAttendence", "HRDB");
                    aManualAttendence.ATTDate = dtStart;
                    aManualAttendence.DayName = dtStart.DayOfWeek.ToString();
                    aManualAttendenceDal.SaveManualAttendenceInfo(aManualAttendence);
                    dtStart = dtStart.AddDays(1);
                    
                }
                return true;
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public bool DeleteData(string MAttendenceId)
        {
            return aManualAttendenceDal.DeleteData(MAttendenceId);

        }
        public DataTable LoadView()
        {
            return aManualAttendenceDal.LoadView();
        }
        public bool HasEmpManualAttendence(ManualAttendence aManualAttendence)
        {
            return aManualAttendenceDal.HasEmpManualAttendence(aManualAttendence);
        }
        public DataTable GetEmpInfoForGroupManualAttBLL(string date)
        {

            return aManualAttendenceDal.GetEmpInfoForGroupManualAtt(date);
        }

        public DataTable LoadManualAttendenceView()
        {
            return aManualAttendenceDal.LoadManualAttendenceView();
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable Empcode(string empcode)
        {
            return aManualAttendenceDal.EmpCode(empcode);
        }
        public DataTable Shift(string shiftId)
        {
            return aManualAttendenceDal.Shift(shiftId);
        }

        public bool UpdateDataForManualAttendence(ManualAttendence aManualAttendence)
        {
            return aManualAttendenceDal.UpdateManualAttendenceInfo(aManualAttendence);
        }
        public void LoadShiftName(DropDownList ddl,string empinfoId)
        {
            aManualAttendenceDal.LoadShiftName(ddl,empinfoId);
        }
        public void LoadShiftName(DropDownList ddl)
        {
            aManualAttendenceDal.LoadShiftName(ddl);
        }
        public void LoadShiftNameGroupWise(DropDownList ddl, string empinfoId, string date)
        {
            aManualAttendenceDal.LoadShiftNameGroupWise(ddl,empinfoId,date);
        }
        public DataTable ChkShiftData(string empId, string date)
        {
            return aManualAttendenceDal.ChkShiftData(empId, date);

        }
        public DataTable LoadAttendanceRecordData(int empId, string date)
        {
            return aManualAttendenceDal.LoadAttendanceRecordData(empId, date);

        }
        public bool ApprovalUpdateBLL(Attendence attendence)
        {
           
            AttedanceOperationDAL attedanceOperationDal = new AttedanceOperationDAL();
            DataTable dtCheckWHReplace = attedanceOperationDal.CheckWeeklyHolidayReplace(attendence.EmpId, attendence.ATTDate);

            DataTable dtCheckWH = attedanceOperationDal.CheckWeeklyHoliday(attendence.EmpId, attendence.ATTDate);

            if (dtCheckWHReplace.Rows.Count > 0)
            {
                if (attendence.ATTStatus == "A")
                {
                    attendence.ATTStatus = "WH";
                    attendence.Remarks = "Weekly Holiday";
                }
                else
                {
                    attendence.Remarks = attendence.Remarks + ",Weekly Holiday";
                }
                
            }
            else
            {
                if (dtCheckWH.Rows.Count > 0)
                {
                    if (attendence.ATTStatus == "A")
                    {
                        attendence.ATTStatus = "WH";
                        attendence.Remarks = "Weekly Holiday";
                    }
                    else
                    {
                        attendence.Remarks = attendence.Remarks + ",Weekly Holiday";
                    }
                }
            }
            return aManualAttendenceDal.ApprovalUpdateDAL(attendence);
        }

        public DataTable ManualAttendence()
        {
            return aManualAttendenceDal.LoadManualAttendence();
        }
        public bool ApprovalUpdateBLL(ManualAttendence attendence)
        {
            return aManualAttendenceDal.ApprovalUpdateDAL(attendence);
        }
        public DataTable LoadManualAttendanceViewForApproval(string ActionStatus)
        {
            return aManualAttendenceDal.LoadManualAttendanceViewForApproval(ActionStatus);
        }
        public DataTable LoadManualAttData(int AttendenceId)
        {
            return aManualAttendenceDal.LoadManualAttData(AttendenceId);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aManualAttendenceDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aManualAttendenceDal.LoadForApprovalConditionDAL(pageName, userName);
        }
        public bool InsertUpdate(Attendence attendence)
        {
            return aManualAttendenceDal.ApprovalUpdateDAL(attendence);
        }
        public bool DeleteData(Attendence attendence)
        {
            return aManualAttendenceDal.DeleteData(attendence);
        }
    }
}
