﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ManualAttendenceGroupBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        ManualAttendenceGroupDAL aManualAttendenceDal = new ManualAttendenceGroupDAL();
        public bool SaveDataForManualAttendenceGroup(ManualAttendenceGroup aManualAttendence)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aManualAttendence.MAGId = aClsPrimaryKeyFind.PrimaryKeyMax("MAGId", "dbo.tblManualAttendanceGroup", "HRDB");
                aManualAttendenceDal.SaveManualAttendenceGroupInfo(aManualAttendence);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool DeleteData(string mgid)
        {
            return aManualAttendenceDal.DeleteData(mgid);
        }
        public DataTable LoadManualAttendenceView()
        {
            return aManualAttendenceDal.LoadManualAttendenceView();
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable Empcode(string empcode)
        {
            return aManualAttendenceDal.EmpCode(empcode);
        }
        
    }
}
