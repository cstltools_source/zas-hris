﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ManualGroupBLL
    {
        ManualGroupDAL aEmpGroupDal = new ManualGroupDAL();
        public bool SaveDataForEmpGroup(EmpGroup aEmpGroup)
        {
            try
            {
                if (!aEmpGroupDal.HasEmpGroupName(aEmpGroup))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aEmpGroup.GroupId = aClsPrimaryKeyFind.PrimaryKeyMax("GroupId", "tblManualAttGroup", "HRDB");
                    aEmpGroupDal.SaveDevision(aEmpGroup);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateaDataforEmpGroup(EmpGroup aEmpGroup)
        {
            return aEmpGroupDal.UpdateEmpGroup(aEmpGroup);
        }
        public DataTable LoadDataforEmpGroupView()
        {
            return aEmpGroupDal.LoadEmpGroupView();
        }
        public EmpGroup EmpGroupEditLoad(string GroupId)
        {
            return aEmpGroupDal.EmpGroupEditLoad(GroupId);
        }
    }
}
