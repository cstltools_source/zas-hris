﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
    public  class MisReportBLL
    {
        MisReportDAL aMisReportDal=new MisReportDAL();
        public DataTable DeptSalReport()
        {
            return aMisReportDal.DeptSalReport();
        }
        public DataTable YearlySalary(string from, string to)
        {
            return aMisReportDal.YearlySalary(from, to);
        }
        public DataTable Temp()
        {
            return aMisReportDal.Temp();
        }
        public DataTable YearlyAttendanceStatement(string fromdate, string todate)
        {
            return aMisReportDal.YearlyAttendanceStatement(fromdate, todate);
        }
        public DataTable YearWiseJoinLeftReport(string year)
        {
            DataTable aDataTable=new DataTable();

            aDataTable.Columns.Add("DesigName");
            aDataTable.Columns.Add("JanJoin");
            aDataTable.Columns.Add("JanLeft");
            aDataTable.Columns.Add("FebJoin");
            aDataTable.Columns.Add("FebLeft");
            aDataTable.Columns.Add("MarJoin");
            aDataTable.Columns.Add("MarLeft");
            aDataTable.Columns.Add("AprJoin");
            aDataTable.Columns.Add("AprLeft");
            aDataTable.Columns.Add("MayJoin");
            aDataTable.Columns.Add("MayLeft");
            aDataTable.Columns.Add("JunJoin");
            aDataTable.Columns.Add("JunLeft");
            aDataTable.Columns.Add("JulJoin");
            aDataTable.Columns.Add("JulLeft");
            aDataTable.Columns.Add("AugJoin");
            aDataTable.Columns.Add("AugLeft");
            aDataTable.Columns.Add("SepJoin");
            aDataTable.Columns.Add("SepLeft");
            aDataTable.Columns.Add("OctJoin");
            aDataTable.Columns.Add("OctLeft");
            aDataTable.Columns.Add("NovJoin");
            aDataTable.Columns.Add("NovLeft");
            aDataTable.Columns.Add("DecJoin");
            aDataTable.Columns.Add("DecLeft");
            aDataTable.Columns.Add("TotalJoin");
            aDataTable.Columns.Add("TotalLeft");
            aDataTable.Columns.Add("AllTotalJoin");
            aDataTable.Columns.Add("AllTotalLeft"); 
            aDataTable.Columns.Add("Year");

            DataRow dataRow = null;

            DataTable dtdesig = aMisReportDal.Designation();
            for (int i = 0; i < dtdesig.Rows.Count; i++)
            {
                dataRow = aDataTable.NewRow();

                dataRow["DesigName"] = dtdesig.Rows[i]["DesigName"].ToString();
                dataRow["JanJoin"] = aMisReportDal.JanJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["JanLeft"] = aMisReportDal.JanLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["FebJoin"] = aMisReportDal.FebJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["FebLeft"] = aMisReportDal.FebLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["MarJoin"] = aMisReportDal.MarJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["MarLeft"] = aMisReportDal.MarLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["AprJoin"] = aMisReportDal.AprJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["AprLeft"] = aMisReportDal.AprLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["MayJoin"] = aMisReportDal.MayJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["MayLeft"] = aMisReportDal.MayLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["JunJoin"] = aMisReportDal.JunJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["JunLeft"] = aMisReportDal.JunLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["JulJoin"] = aMisReportDal.JulJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["JulLeft"] = aMisReportDal.JulLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["AugJoin"] = aMisReportDal.AugJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["AugLeft"] = aMisReportDal.AugLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["SepJoin"] = aMisReportDal.SepJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["SepLeft"] = aMisReportDal.SepLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["OctJoin"] = aMisReportDal.OctJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["OctLeft"] = aMisReportDal.OctLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["NovJoin"] = aMisReportDal.NovJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["NovLeft"] = aMisReportDal.NovLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["DecJoin"] = aMisReportDal.DecJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["DecLeft"] = aMisReportDal.DecLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["TotalJoin"] = aMisReportDal.TotalJoin(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["TotalLeft"] = aMisReportDal.TotalLeft(dtdesig.Rows[i]["DesigId"].ToString(), year).Rows[0][0].ToString();
                dataRow["AllTotalJoin"] = aMisReportDal.AllTotalJoin( year).Rows[0][0].ToString();
                dataRow["AllTotalLeft"] = aMisReportDal.AllTotalLeft(year).Rows[0][0].ToString();
                dataRow["Year"] = year;
                
                aDataTable.Rows.Add(dataRow);
            }

            return aDataTable;
        }
    }
}
