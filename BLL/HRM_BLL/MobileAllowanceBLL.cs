﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class MobileAllowanceBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        MobileAllowanceDAL aMobileAllowanceDAL = new MobileAllowanceDAL();
        public bool SaveDataForMobAllowance(MobileAllowance aMobileAllowance)
        {
            try
            {
                if (!aMobileAllowanceDAL.HasMobAllowance(aMobileAllowance))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aMobileAllowance.MobAllowanceId = aClsPrimaryKeyFind.PrimaryKeyMax("MobAllowanceId", "tblMobileAllowance", "HRDB");
                    aMobileAllowanceDAL.SaveMobAllowance(aMobileAllowance);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aMobileAllowanceDAL.LoadDesignationName(aDropDownList);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aMobileAllowanceDAL.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aMobileAllowanceDAL.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aMobileAllowanceDAL.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aMobileAllowanceDAL.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aMobileAllowanceDAL.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aMobileAllowanceDAL.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aMobileAllowanceDAL.LoadSalGradeName(aDropDownList);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aMobileAllowanceDAL.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(MobileAllowance aMobileAllowance)
        {
            return aMobileAllowanceDAL.UpdateMobAllowance(aMobileAllowance);
        }
        public DataTable LoadMobAllowance()
        {
            return aMobileAllowanceDAL.LoadMobAllowanceView();
        }
        public MobileAllowance MobAllowanceEditLoad(string MobAllowanceId)
        {
            return aMobileAllowanceDAL.MobAllowanceEditLoad(MobAllowanceId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aMobileAllowanceDAL.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpMobAllowanceInfo(string empInfoId)
        {
            return aMobileAllowanceDAL.LoadEmpMobAllowanceInfo(empInfoId);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aMobileAllowanceDAL.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aMobileAllowanceDAL.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aMobileAllowanceDAL.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aMobileAllowanceDAL.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aMobileAllowanceDAL.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aMobileAllowanceDAL.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aMobileAllowanceDAL.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aMobileAllowanceDAL.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aMobileAllowanceDAL.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(MobileAllowance aMobileAllowance)
        {
            return aMobileAllowanceDAL.ApprovalUpdateDAL(aMobileAllowance);
        }
        public bool UpdatePreMobAllowanceDAL(string empInfoId,string effectiveDate)
        {
            return aMobileAllowanceDAL.UpdatePreMobAllowanceDAL(empInfoId,effectiveDate);
        }
        public DataTable LoadMobileAllowanceViewForApproval(string ActionStatus)
        {
            return aMobileAllowanceDAL.LoadMobileAllowanceViewForApproval(ActionStatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aMobileAllowanceDAL.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aMobileAllowanceDAL.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aMobileAllowanceDAL.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void DeleteDataBLL(string LID)
        {
            aMobileAllowanceDAL.DeleteData(LID);

        }
    }
}
