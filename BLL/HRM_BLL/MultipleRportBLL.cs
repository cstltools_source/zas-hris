﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
    public class MultipleRportBLL
    {

        MultipleReportDAL aMultipleReportDal=new MultipleReportDAL();
        public DataTable RptHeader()
        {
            return aMultipleReportDal.RptHeader();
        }
        public DataTable SalaryGrade()
        {
            return aMultipleReportDal.SalaryGrade();
        }
        public DataTable CompanyRpt()
        {
            return aMultipleReportDal.CompanyRpt();
        }
        public DataTable UnitRpt()
        {
            return aMultipleReportDal.UnitRpt();
        }
        public DataTable DivisionRpt()
        {
            return aMultipleReportDal.DivisionRpt();
        }
        public DataTable DepartmentRpt()
        {
            return aMultipleReportDal.DepartmentRpt();
        }
        public DataTable EmpGradeRpt()
        {
            return aMultipleReportDal.EmpGradeRpt();
        }
        public DataTable LineRpt()
        {
            return aMultipleReportDal.LineRpt();
        }
        public DataTable DesignationRpt()
        {
            return aMultipleReportDal.DesignationRpt();
        }
        public DataTable SectionRpt()
        {
            return aMultipleReportDal.SectionRpt();
        }
        public DataTable EmpTypeRpt()
        {
            return aMultipleReportDal.EmpTypeRpt();
        }
        public DataTable ShiftRpt()
        {
            return aMultipleReportDal.ShiftRpt();
        }
        public DataTable UserRpt()
        {
            return aMultipleReportDal.UserRpt();
        }
        public DataTable EmpAttGroupRpt()
        {
            return aMultipleReportDal.EmpAttGroupRpt();
        }
    }
}
