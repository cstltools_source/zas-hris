﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class OnDutyBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        OnDutyDAL aOnDutyDal = new OnDutyDAL();
        public bool SaveDataForOnDuty(OnDuty aOnDuty)
        {
            try
            {
                DateTime dtStart = aOnDuty.OnDDate;
                DateTime dtEnd = aOnDuty.OnTDate;
                while (dtStart <= dtEnd)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aOnDuty.OnDutyId = aClsPrimaryKeyFind.PrimaryKeyMax("OnDutyId", "tblOnDuty", "HRDB");
                    aOnDuty.OnDDate = dtStart;
                    aOnDutyDal.SaveDataForOnDuty(aOnDuty);
                    dtStart = dtStart.AddDays(1);
                }
                
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateDataForOnDuty(OnDuty aOnDuty)
        {
            return aOnDutyDal.UpdateOnDuty(aOnDuty);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadOnDutyView()
        {
            return aOnDutyDal.LoadOnDutyView();
        }
        
        public OnDuty OnDutyEditLoad(string OnDutyId)
        {
            return aOnDutyDal.OnDutyEditLoad(OnDutyId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aOnDutyDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aOnDutyDal.LoadEmpInfoCode(EmpInfoId);
        }

        public bool ApprovalUpdateBLL(OnDuty aOnDuty)
        {
            return aOnDutyDal.ApprovalUpdateDAL(aOnDuty);
        }
        public DataTable LoadOnDutyViewForApproval(string ActionStatus)
        {
            return aOnDutyDal.LoadOnDutyViewForApproval(ActionStatus);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aOnDutyDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aOnDutyDal.LoadForApprovalConditionDAL(pageName, userName);
        }

        public void DeleteDataBLL(string OnDutyId)
        {
            aOnDutyDal.DeleteData(OnDutyId);

        }
    }
}
