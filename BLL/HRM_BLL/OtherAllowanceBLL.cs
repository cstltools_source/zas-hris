﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class OtherAllowanceBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        OtherAllowanceDAL aOtherAllowanceDal = new OtherAllowanceDAL();
        public bool SaveDataForOtherAllowance(List<OtherAllowance> aOtherAllowanceList)
        {
            try
            {
                foreach (var aOtherAllowance in aOtherAllowanceList)
                {
                    if (!aOtherAllowanceDal.HasAmount(aOtherAllowance))
                    {
                        ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                        aOtherAllowance.OtherAllowanceId = aClsPrimaryKeyFind.PrimaryKeyMax("OtherAllowanceId", "tblOtherAllowance", "HRDB");
                        aOtherAllowanceDal.SaveOtherAllowances(aOtherAllowance);
                        
                    }
                    
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateaDataforaOtherAllowance(OtherAllowance aOtherAllowance)
        {
            return aOtherAllowanceDal.UpdateOtherAllowance(aOtherAllowance);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadDataforaOtherAllowanceView()
        {
            return aOtherAllowanceDal.LoadOtherAllowanceView();
        }

        public OtherAllowance OtherAllowanceEditLoad(string OtherAllowanceId)
        {
            return aOtherAllowanceDal.OtherAllowanceEditLoad(OtherAllowanceId);
        }
        public DataTable LoadEmpInfo(string empcode)
        {
            return aOtherAllowanceDal.LoadEmpInfo(empcode);
        }
        public DataTable LoadEmpDept(string sectionId)
        {
            return aOtherAllowanceDal.LoadEmpDept(sectionId);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aOtherAllowanceDal.LoadEmpInfoCode(EmpInfoId);
        }
        public bool ApprovalUpdateBLL(OtherAllowance aOtherAllowance)
        {
            return aOtherAllowanceDal.ApprovalUpdateDALL(aOtherAllowance);
        }

        public DataTable LoadOtherAllowanceViewForApproval(string ActionStatus)
        {
            return aOtherAllowanceDal.LoadOtherAllowanceViewForApproval(ActionStatus);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aOtherAllowanceDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aOtherAllowanceDal.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void DeleteDataBLL(string OtherAllowanceId)
        {
            aOtherAllowanceDal.DeleteData(OtherAllowanceId);

        }
    }
}
