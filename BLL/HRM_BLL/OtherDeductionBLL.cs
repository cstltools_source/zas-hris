﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class OtherDeductionBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        OtherDeductionDAL aDeductionDal = new OtherDeductionDAL();
        public bool SaveDataForOtherDeduction(OtherDeduction aOtherDeduction)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aOtherDeduction.ODId = aClsPrimaryKeyFind.PrimaryKeyMax("ODId", "tblOtherDeduction", "HRDB");
                aDeductionDal.SaveOtherDeduction(aOtherDeduction);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateaDataforOtherDeduction(OtherDeduction aOtherDeduction)
        {
            return aDeductionDal.UpdateOtherDeduction(aOtherDeduction);
        }
        public DataTable LoadDataforOtherDeductionView()
        {
            return aDeductionDal.LoadOtherDeductionView();
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public OtherDeduction OtherDeductionEditLoad(string ODId)
        {
            return aDeductionDal.OtherDeductionEditLoad(ODId);
        }
        public void LoadSalHeadName(DropDownList ddl)
        {
            aDeductionDal.LoadSalHeadName(ddl);
        }
        
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aDeductionDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aDeductionDal.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aDeductionDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadSalGradeName(aDropDownList);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadEmpTypeName(aDropDownList);
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aDeductionDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aDeductionDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aDeductionDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aDeductionDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aDeductionDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aDeductionDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aDeductionDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aDeductionDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aDeductionDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aDeductionDal.LoadEmpType(EmpTypeId);
        }
        public void DeleteOtherDeductionBLL(string ODId)
        {
            aDeductionDal.DeleteOtherDeduction(ODId);
        }
        public bool ApprovalUpdateBLL(OtherDeduction aOtherDeduction)
        {
            return aDeductionDal.ApprovalUpdateDAL(aOtherDeduction);
        }
        public DataTable LoadOtherDeductionViewForApproval(string ActionStatus)
        {
            return aDeductionDal.LoadOtherDeductionViewForApproval(ActionStatus);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aDeductionDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aDeductionDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
