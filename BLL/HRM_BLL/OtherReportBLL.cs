﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
    public class OtherReportBLL
    {
        OtherReportDAL aOtherReportDal=new OtherReportDAL();
        public DataTable RptHeader()
        {
            return aOtherReportDal.RptHeader();
        }
        public DataTable SalaryGrade(string parameter)
        {
            return aOtherReportDal.SalaryGrade(parameter);
        }
        public DataTable LateApproveRpt(string parameter)
        {
            return aOtherReportDal.LateApproveRpt(parameter);
        }
        public DataTable MobileAllowance(string parameter)
        {
            return aOtherReportDal.MobileAllowance(parameter);
        }
        public DataTable SalaryDeduction(string parameter)
        {
            return aOtherReportDal.SalaryDeduction(parameter);
        }
        public DataTable LoanRptDAL(string parameter)
        {
            return aOtherReportDal.LoanRptDAL(parameter);
        }
        public DataTable LoanRptDALNew(string ReportParameter)
        {
            return aOtherReportDal.LoanRptDALNew(ReportParameter);
        }
        public DataTable TaxRpt(string fromdate, string todate)
        {
            return aOtherReportDal.TaxRpt(fromdate, todate);
        }
        public DataTable LoanRptDALSingle(string ReportParameter, string empmastercode, string fromdate, string todate)
        {
            return aOtherReportDal.LoanRptDALSingle(ReportParameter, empmastercode, fromdate, todate);
        }
        public DataTable ArrearRpt(string parameter)
        {
            return aOtherReportDal.ArrearRpt(parameter);
        }
        public DataTable IncrementRpt(string parameter)
        {
            return aOtherReportDal.IncrementRpt(parameter);
        }
        public DataTable ReappontmentRpt(string parameter)
        {
            return aOtherReportDal.ReappontmentRpt(parameter);
        }
        public DataTable SuspendRpt(string parameter)
        {
            return aOtherReportDal.SuspendRpt(parameter);
        }
        public DataTable TransferRpt(string parameter)
        {
            return aOtherReportDal.TransferRpt(parameter);
        }
        public DataTable TaxRpt(string parameter)
        {
            return aOtherReportDal.TaxRpt(parameter);
        }

        public DataTable  PromotionRpt(string ReportParameter)
        {
            return aOtherReportDal.PromotionRptDAL(ReportParameter);
        }
        public DataTable JobleftRpt(string ReportParameter)
        {
            return aOtherReportDal.JobleftRpt(ReportParameter);
        }
        public DataTable ManualAttRpt(string ReportParameter)
        {
            return aOtherReportDal.ManualAttRpt(ReportParameter);
        }
        public DataTable UserRpt(string ReportParameter)
        {
            return aOtherReportDal.UserRpt(ReportParameter);
        }
        public DataTable HolidayRpt(string ReportParameter)
        {
            return aOtherReportDal.HolidayRpt(ReportParameter);
        }
        public DataTable ReappointmentRpt(string ReportParameter)
        {
            return aOtherReportDal.ReappointmentRpt(ReportParameter);
        }


        public DataTable AllIncrement(string fromdt, string todt, string actionstatus)
        {
            DataTable dtempinfo = aOtherReportDal.AllEmpinfo();

            DataTable aDataTable=new DataTable();
            aDataTable.Columns.Add("SalHeadName");
            aDataTable.Columns.Add("PreAmount");
            aDataTable.Columns.Add("NewAmount");
            aDataTable.Columns.Add("IncreaseAmount");
            aDataTable.Columns.Add("DeptName");
            aDataTable.Columns.Add("DesigName");
            aDataTable.Columns.Add("EmpName");
            aDataTable.Columns.Add("EmpMasterCode");
            aDataTable.Columns.Add("ActiveDate");
            aDataTable.Columns.Add("PreDesigName");
            aDataTable.Columns.Add("NewDesigName");
            aDataTable.Columns.Add("PreGradeName");
            aDataTable.Columns.Add("NewGradeName");

            DataRow aDataRow = null;

            for (int i = 0; i < dtempinfo.Rows.Count; i++)
            {
                DataTable dtincinfo = SingleIncrement(dtempinfo.Rows[i]["EmpMasterCode"].ToString(), fromdt, todt,
                    actionstatus);

                foreach (DataRow row in dtincinfo.Rows)
                {
                    aDataRow = aDataTable.NewRow();
                    aDataRow["SalHeadName"] = row["SalHeadName"].ToString();
                    aDataRow["PreAmount"] = row["PreAmount"].ToString();
                    aDataRow["NewAmount"] = row["NewAmount"].ToString();
                    aDataRow["IncreaseAmount"] = row["IncreaseAmount"].ToString();
                    aDataRow["DeptName"] = row["DeptName"].ToString();
                    aDataRow["DesigName"] = row["DesigName"].ToString();
                    aDataRow["EmpName"] = row["EmpName"].ToString();
                    aDataRow["EmpMasterCode"] = row["EmpMasterCode"].ToString();
                    aDataRow["ActiveDate"] = row["ActiveDate"].ToString();

                    aDataRow["PreDesigName"] = row["PreDesigName"].ToString();
                    aDataRow["NewDesigName"] = row["NewDesigName"].ToString();
                    aDataRow["PreGradeName"] = row["PreGradeName"].ToString();
                    aDataRow["NewGradeName"] = row["NewGradeName"].ToString();

                    aDataTable.Rows.Add(aDataRow);


                }
            }

            return aDataTable;

        }


        public DataTable SingleIncrement(string empcode,string fromdt,string todt,string actionstatus)
        {
            DataTable aDataTable=new DataTable();

            aDataTable.Columns.Add("SalHeadName");
            aDataTable.Columns.Add("PreAmount");
            aDataTable.Columns.Add("NewAmount");
            aDataTable.Columns.Add("IncreaseAmount");
            aDataTable.Columns.Add("DeptName");
            aDataTable.Columns.Add("DesigName");
            aDataTable.Columns.Add("EmpName");
            aDataTable.Columns.Add("EmpMasterCode");
            aDataTable.Columns.Add("ActiveDate");
            aDataTable.Columns.Add("PreDesigName");
            aDataTable.Columns.Add("NewDesigName");
            aDataTable.Columns.Add("PreGradeName");
            aDataTable.Columns.Add("NewGradeName");


            DataRow aDataRow = null;

            DataTable dtsalinfo = aOtherReportDal.SalaryInfo(empcode);
            
            DataTable dtpromotionInfo = aOtherReportDal.Promotion(empcode,fromdt,todt,actionstatus);
            

            for (int i = 0; i < dtsalinfo.Rows.Count; i++)
            {

                DataTable dtincrement = aOtherReportDal.IncrementAmount(empcode, dtsalinfo.Rows[i]["SalaryHeadId"].ToString(),fromdt,todt,actionstatus);
                DataTable dtpresalinfo = aOtherReportDal.PreSalaryInfo(dtsalinfo.Rows[i]["EmpInfoId"].ToString(), (dtsalinfo.Rows.Count + 1).ToString(), dtsalinfo.Rows[i]["SalaryHeadId"].ToString());

                if (dtincrement.Rows.Count > 0)
                {
                    aDataRow = aDataTable.NewRow();

                    aDataRow["SalHeadName"] = dtsalinfo.Rows[i]["SalHeadName"].ToString();
                    if (dtpresalinfo.Rows.Count>0)
                    {
                        aDataRow["PreAmount"] = dtpresalinfo.Rows[0]["Amount"].ToString();    
                    }
                    else
                    {
                        aDataRow["PreAmount"] = "0.00";
                    }
                    
                    aDataRow["NewAmount"] = dtincrement.Rows[0]["Amount"].ToString();
                    aDataRow["IncreaseAmount"] = Convert.ToDecimal(aDataRow["NewAmount"].ToString()) -
                                                 Convert.ToDecimal(aDataRow["PreAmount"].ToString());
                    aDataRow["DeptName"] = dtsalinfo.Rows[i]["DeptName"].ToString();
                    aDataRow["DesigName"] = dtsalinfo.Rows[i]["DesigName"].ToString();
                    aDataRow["EmpName"] = dtsalinfo.Rows[i]["EmpName"].ToString();
                    aDataRow["EmpMasterCode"] = dtsalinfo.Rows[i]["EmpMasterCode"].ToString();
                    aDataRow["ActiveDate"] = Convert.ToDateTime(dtsalinfo.Rows[i]["ActiveDate"].ToString()).ToString("dd-MMM-yyyy");
                    if (dtpromotionInfo.Rows.Count >0)
                    {
                        aDataRow["PreDesigName"] = dtpromotionInfo.Rows[0]["PreDesigName"].ToString();
                        aDataRow["NewDesigName"] = dtpromotionInfo.Rows[0]["NewDesigName"].ToString();
                        aDataRow["PreGradeName"] = dtpromotionInfo.Rows[0]["PreGradeName"].ToString();
                        aDataRow["NewGradeName"] = dtpromotionInfo.Rows[0]["NewGradeName"].ToString();
                    }
                    

                    aDataTable.Rows.Add(aDataRow);
                }

                

            }


            return aDataTable;
        }
        public DataTable SingleIncrementWithPromotion(string empcode, string fromdt, string todt, string actionstatus)
        {
            DataTable aDataTable = new DataTable();

            aDataTable.Columns.Add("SalHeadName");
            aDataTable.Columns.Add("PreAmount");
            aDataTable.Columns.Add("NewAmount");
            aDataTable.Columns.Add("IncreaseAmount");
            aDataTable.Columns.Add("DeptName");
            aDataTable.Columns.Add("DesigName");
            aDataTable.Columns.Add("EmpName");
            aDataTable.Columns.Add("ShortName");
            aDataTable.Columns.Add("EmpMasterCode");
            aDataTable.Columns.Add("ActiveDate");
            aDataTable.Columns.Add("PreDesigName");
            aDataTable.Columns.Add("NewDesigName");
            aDataTable.Columns.Add("PreGradeName");
            aDataTable.Columns.Add("NewGradeName");


            DataRow aDataRow = null;

            DataTable dtsalinfo = aOtherReportDal.SalaryInfo(empcode);
            DataTable dtpromotionInfo = aOtherReportDal.Promotion(empcode, fromdt, todt, actionstatus);


            for (int i = 0; i < dtsalinfo.Rows.Count; i++)
            {

                DataTable dtincrement = aOtherReportDal.IncrementWithPromotion(empcode, dtsalinfo.Rows[i]["SalaryHeadId"].ToString(), fromdt, todt, actionstatus);

                if (dtincrement.Rows.Count > 0)
                {
                    aDataRow = aDataTable.NewRow();

                    aDataRow["SalHeadName"] = dtsalinfo.Rows[i]["SalHeadName"].ToString();
                    aDataRow["PreAmount"] = dtsalinfo.Rows[i]["Amount"].ToString();
                    aDataRow["NewAmount"] = dtincrement.Rows[0]["Amount"].ToString();
                    aDataRow["IncreaseAmount"] = Convert.ToDecimal(aDataRow["NewAmount"].ToString()) -
                                                 Convert.ToDecimal(aDataRow["PreAmount"].ToString());
                    aDataRow["DeptName"] = dtsalinfo.Rows[i]["DeptName"].ToString();
                    aDataRow["DesigName"] = dtsalinfo.Rows[i]["DesigName"].ToString();
                    aDataRow["ShortName"] = dtsalinfo.Rows[i]["ShortName"].ToString();
                    aDataRow["EmpName"] = dtsalinfo.Rows[i]["EmpName"].ToString();
                    aDataRow["EmpMasterCode"] = dtsalinfo.Rows[i]["EmpMasterCode"].ToString();
                    aDataRow["ActiveDate"] = Convert.ToDateTime(dtsalinfo.Rows[i]["ActiveDate"].ToString()).ToString("dd-MMM-yyyy");

                    aDataRow["PreDesigName"] = dtincrement.Rows[0]["PreDesigName"].ToString();
                    aDataRow["NewDesigName"] = dtincrement.Rows[0]["NewDesigName"].ToString();
                    aDataRow["PreGradeName"] = dtincrement.Rows[0]["PreGradeName"].ToString();
                    aDataRow["NewGradeName"] = dtincrement.Rows[0]["NewGradeName"].ToString();
                    


                    aDataTable.Rows.Add(aDataRow);
                }

            }


            return aDataTable;
        }

        public void LoadLoan(DropDownList ddl,string empcode)
        {
            aOtherReportDal.LoadLoan(ddl,empcode);
        }
        public string Month(string number)
        {
            string monthname = "";

            if (number == "1")
            {
                monthname = "January";
            }
            if (number == "2")
            {
                monthname = "February";
            }
            if (number == "3")
            {
                monthname = "March";
            }
            if (number == "4")
            {
                monthname = "April";
            }
            if (number == "5")
            {
                monthname = "May";
            }
            if (number == "6")
            {
                monthname = "June";
            }
            if (number == "7")
            {
                monthname = "July";
            }
            if (number == "8")
            {
                monthname = "August";
            }
            if (number == "9")
            {
                monthname = "September";
            }
            if (number == "10")
            {
                monthname = "October";
            }
            if (number == "11")
            {
                monthname = "November";
            }
            if (number == "12")
            {
                monthname = "December";
            }
            return monthname;
        }

        public DataTable LoanDetailYearly(string year)
        {

            DataTable aDataTable=new DataTable();
            aDataTable.Columns.Add("EmpName");
            aDataTable.Columns.Add("EmpInfoId");
            aDataTable.Columns.Add("EmpMasterCode");
            aDataTable.Columns.Add("DesigName");
            aDataTable.Columns.Add("DeptName");
            aDataTable.Columns.Add("LoginName");
            aDataTable.Columns.Add("SanctionDate");
            aDataTable.Columns.Add("DeductionStartDate");
            aDataTable.Columns.Add("ClosingBalance");
            aDataTable.Columns.Add("Amount"); 
            aDataTable.Columns.Add("LoanAmountMain"); 
            aDataTable.Columns.Add("DeducTill"); 
            aDataTable.Columns.Add("TotalAmount");
            aDataTable.Columns.Add("TotalDeduc");
            aDataTable.Columns.Add("LastClosingBalance");
            aDataTable.Columns.Add("SumTotalDeduc");
            aDataTable.Columns.Add("SumLastClosingBalance");

            for (int i = 1; i <= 12; i++)
            {
                aDataTable.Columns.Add(Month(i.ToString()));
            }
            for (int i = 1; i <= 12; i++)
            {
                aDataTable.Columns.Add(Month(i.ToString())+"Total");
            }

            DataRow dataRow = null;

            DataTable dtavailEmp = aOtherReportDal.GetLoanAvailEmployee(year);


            decimal sumtotaldeduc = 0;
            decimal SumLastClosingBalance = 0;

            for (int i = 0; i < dtavailEmp.Rows.Count; i++)
            {
                dataRow = aDataTable.NewRow();
                dataRow["EmpInfoId"] = dtavailEmp.Rows[i]["EmpInfoId"].ToString();
                dataRow["EmpName"] = dtavailEmp.Rows[i]["EmpName"].ToString();
                dataRow["EmpMasterCode"] = dtavailEmp.Rows[i]["EmpMasterCode"].ToString();
                dataRow["DesigName"] = dtavailEmp.Rows[i]["DesigName"].ToString();
                dataRow["DeptName"] = dtavailEmp.Rows[i]["DeptName"].ToString();
                dataRow["LoginName"] = HttpContext.Current.Session["LoginName"].ToString();

                DataTable dtdate = aOtherReportDal.GetSancDate(dtavailEmp.Rows[i]["EmpInfoId"].ToString());

                dataRow["SanctionDate"] = Convert.ToDateTime(dtdate.Rows[0][0].ToString()).ToString("dd-MMM-yyyy");
                dataRow["DeductionStartDate"] = Convert.ToDateTime(dtdate.Rows[0][1].ToString()).ToString("dd-MMM-yyyy");


                decimal PrevData = 0;
                decimal PrevAmount = 0;
                decimal TotalDeduc = 0;
                decimal ThisYAmount= 0;
                decimal ClosingBalance = 0;

                DataTable dtprevYear = aOtherReportDal.GetPrevYearData(year, dtavailEmp.Rows[i]["EmpInfoId"].ToString());

                DataTable dtPrevAmount = aOtherReportDal.GetPrevYearLoan(year, dtavailEmp.Rows[i]["EmpInfoId"].ToString());
                DataTable dtThisyAmount = aOtherReportDal.ThisYearLoanAmount(year, dtavailEmp.Rows[i]["EmpInfoId"].ToString());
                DataTable dtloanamount = aOtherReportDal.MainLoanAmount(dtavailEmp.Rows[i]["EmpInfoId"].ToString());
                DataTable dtdeducloanamounttill = aOtherReportDal.TotalAmountDeducttillmonth(dtavailEmp.Rows[i]["EmpInfoId"].ToString(), dtdate.Rows[0][1].ToString(),DateTime.Now.ToString("dd-MMM-yyyy"));
                if (dtloanamount.Rows.Count>0)
                {
                    dataRow["LoanAmountMain"] = dtloanamount.Rows[0]["LoanAmount"].ToString();    
                }
                else
                {

                    dataRow["LoanAmountMain"] = "0";
                }
                if (dtdeducloanamounttill.Rows.Count > 0)
                {
                    dataRow["DeducTill"] = dtdeducloanamounttill.Rows[0][0].ToString();
                }
                else
                {

                    dataRow["DeducTill"] = "0";
                }
                if (dtprevYear.Rows.Count>0)
                {
                    PrevData = Convert.ToDecimal(Convert.ToDecimal(dtprevYear.Rows[0][1].ToString()).ToString("##.###"));
                }

                if (dtPrevAmount.Rows.Count>0)
                {
                    PrevAmount = Convert.ToDecimal(Convert.ToDecimal(dtPrevAmount.Rows[0][1].ToString()).ToString("##.###"));
                }
                if (dtThisyAmount.Rows.Count>0)
                {
                    ThisYAmount = Convert.ToDecimal(Convert.ToDecimal(dtThisyAmount.Rows[0][1].ToString()).ToString("##.###"));
                }
                for (int j = 1; j <= 12; j++)
                {
                    decimal value = 0;
                    decimal totalvalue = 0;
                    DataTable dtempamount = aOtherReportDal.LoanAmount(dtavailEmp.Rows[i]["EmpInfoId"].ToString(), j.ToString(),year);

                    if (dtempamount.Rows.Count>0)
                    {
                        dataRow[Month(j.ToString())] =string.IsNullOrEmpty(dtempamount.Rows[0][0].ToString())?"0":Convert.ToDecimal(dtempamount.Rows[0][0].ToString()).ToString("##.###");
                        

                        
                        
                        TotalDeduc = Convert.ToDecimal(TotalDeduc + (string.IsNullOrEmpty(dtempamount.Rows[0][0].ToString()) ? 0 : Convert.ToDecimal(dtempamount.Rows[0][0].ToString())));
                    }
                    else
                    {
                        dataRow[Month(j.ToString())] = "";
                    }
                    value = string.IsNullOrEmpty(dataRow[Month(j.ToString())].ToString())
                        ? 0
                        : Convert.ToDecimal(dataRow[Month(j.ToString())].ToString());
                    if (i == 0)
                    {
                        totalvalue = value;
                        dataRow[Month(j.ToString()) + "Total"] = totalvalue;
                    }
                    else
                    {
                        totalvalue = value +
                                     Convert.ToDecimal(
                                         string.IsNullOrEmpty(
                                             aDataTable.Rows[i - 1][Month(j.ToString()) + "Total"].ToString())
                                             ? 0
                                             : Convert.ToDecimal(
                                                 aDataTable.Rows[i - 1][Month(j.ToString()) + "Total"].ToString()));

                        dataRow[Month(j.ToString()) + "Total"] = totalvalue;
                    }

                    
                }
                //for (int j = 1; j <= 12; j++)
                //{
                //    DataTable dtempamount = aOtherReportDal.LoanAmount(dtavailEmp.Rows[i]["EmpInfoId"].ToString(), j.ToString(), year);

                //    if (dtempamount.Rows.Count > 0)
                //    {
                //        dataRow[Month(i.ToString()) + "Total"] = Convert.ToDecimal(dataRow[Month(j.ToString())])+;
                //        TotalDeduc = Convert.ToDecimal(TotalDeduc + (string.IsNullOrEmpty(dtempamount.Rows[0][0].ToString()) ? 0 : Convert.ToDecimal(dtempamount.Rows[0][0].ToString())));
                //    }
                //    else
                //    {
                //        dataRow[Month(j.ToString())] = "";
                //    }


                //}
                dataRow["ClosingBalance"] = Convert.ToDecimal(PrevAmount-PrevData).ToString("##.###");
                dataRow["Amount"] = Convert.ToDecimal(ThisYAmount).ToString("##.###");
                if (i==0)
                {
                    dataRow["TotalAmount"] = Convert.ToDecimal(ThisYAmount).ToString("##.###");    
                }
                else
                {
                    dataRow["TotalAmount"] = Convert.ToDecimal(ThisYAmount)+ Convert.ToDecimal(string.IsNullOrEmpty((aDataTable.Rows[i - 1]["TotalAmount"].ToString())) ? 0 : Convert.ToDecimal(aDataTable.Rows[i - 1]["TotalAmount"].ToString()));    
                }
                
                dataRow["TotalDeduc"] = Convert.ToDecimal(TotalDeduc).ToString("##.###");
                dataRow["LastClosingBalance"] = Convert.ToDecimal(ThisYAmount + (PrevAmount - PrevData)-TotalDeduc).ToString("##.###");

                sumtotaldeduc = sumtotaldeduc + (string.IsNullOrEmpty(dataRow["TotalDeduc"].ToString()) ? 0 : Convert.ToDecimal(dataRow["TotalDeduc"]));
                SumLastClosingBalance = SumLastClosingBalance + (string.IsNullOrEmpty(dataRow["LastClosingBalance"].ToString()) ? 0 : Convert.ToDecimal(dataRow["LastClosingBalance"]));
                dataRow["SumTotalDeduc"] = Convert.ToDecimal(sumtotaldeduc).ToString("##.###");
                dataRow["SumLastClosingBalance"] = Convert.ToDecimal(SumLastClosingBalance).ToString("##.###");

                aDataTable.Rows.Add(dataRow);
            }

            return aDataTable;
        }
    }

}
