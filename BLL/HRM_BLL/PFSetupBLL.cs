﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class PFSetupBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        PFSetupDAL aPfSetupDal= new PFSetupDAL();
        public bool SaveDataForPFSetup(PFSetup aPfSetup)
        {
            try
            {
                if (!aPfSetupDal.HasPFBenefit(aPfSetup))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aPfSetup.PFSId = aClsPrimaryKeyFind.PrimaryKeyMax("PFSId", "tblPFSetup", "HRDB");
                    aPfSetupDal.SavePFSetup(aPfSetup);
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aPfSetupDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable PFReport(string month,string year,string monthname)
        {
            return aPfSetupDal.PFReport(month,year,monthname);
        }
        public DataTable PFReportYearly(string year)
        {

            return aPfSetupDal.PFReportYearly(year);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {

            aPfSetupDal.LoadApprovalControlDAL(rdl, pageName, userName);

        }

        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aPfSetupDal.LoadForApprovalConditionDAL(pageName, userName);
        }
        public DataTable LoadPFSetupViewForApproval(string ActionStatus)
        {
            return aPfSetupDal.LoadPFSetupViewForApproval(ActionStatus);
        }
        public bool ApprovalUpdateBLL(PFSetup aPfSetup)
        {
            return aPfSetupDal.ApprovalUpdateDALL(aPfSetup);
        }

        public string Month(string number)
        {
            string monthname = "";

            if (number=="1")
            {
                monthname = "January";
            }
            if (number == "2")
            {
                monthname = "February";
            }
            if (number == "3")
            {
                monthname = "March";
            }
            if (number == "4")
            {
                monthname = "April";
            }
            if (number == "5")
            {
                monthname = "May";
            }
            if (number == "6")
            {
                monthname = "June";
            }
            if (number == "7")
            {
                monthname = "July";
            }
            if (number == "8")
            {
                monthname = "August";
            }
            if (number == "9")
            {
                monthname = "September";
            }
            if (number == "10")
            {
                monthname = "October";
            }
            if (number == "11")
            {
                monthname = "November";
            }
            if (number == "12")
            {
                monthname = "December";
            }
            return monthname;
        }
        public DataTable PFSingleEmp(string year,string empCode)
        {
            DataTable aDataTable=new DataTable();
            aDataTable.Columns.Add("EmpInfoId");
            aDataTable.Columns.Add("EmpMasterCode");
            aDataTable.Columns.Add("EmpName");
            
            aDataTable.Columns.Add("DeptName");
            aDataTable.Columns.Add("DesigName");
            aDataTable.Columns.Add("PreviousAllYearPf");
            aDataTable.Columns.Add("PreviousAllYearComPf");
            aDataTable.Columns.Add("Month");
            aDataTable.Columns.Add("PrFund");
            aDataTable.Columns.Add("UnitName");
            aDataTable.Columns.Add("JoiningDate");
            aDataTable.Columns.Add("PFStartDate");
            aDataTable.Columns.Add("LoginName");
            aDataTable.Columns.Add("TotalPF");
            aDataTable.Columns.Add("TotalPFCom");
            aDataTable.Columns.Add("Year");
            aDataTable.Columns.Add("AllTotal");

            DataRow dataRow = null;

            DataTable dtprev = aPfSetupDal.PFReportYearly(year,empCode);

            if (dtprev.Rows.Count > 0)
            {

                decimal prfund = 0;

                for (int i = 1; i <= 12; i++)
                {
                    DataTable dtdetail = aPfSetupDal.PFDetails(year, i.ToString(), empCode);

                    
                    dataRow = aDataTable.NewRow();

                    dataRow["EmpInfoId"] = dtprev.Rows[0]["EmpInfoId"].ToString();
                    dataRow["EmpMasterCode"] = dtprev.Rows[0]["EmpMasterCode"].ToString();
                    //dataRow["UnitName"] = dtprev.Rows[0]["UnitName"].ToString();
                    dataRow["EmpName"] = dtprev.Rows[0]["EmpName"].ToString();
                    dataRow["DeptName"] = dtprev.Rows[0]["DeptName"].ToString();
                    dataRow["DesigName"] = dtprev.Rows[0]["DesigName"].ToString();
                    dataRow["PreviousAllYearPf"] = Convert.ToDecimal(dtprev.Rows[0]["PreviousAllYearPf"].ToString());
                    dataRow["PreviousAllYearComPf"] = Convert.ToDecimal(dtprev.Rows[0]["PreviousAllYearComPf"].ToString());
                    dataRow["UnitName"] = dtprev.Rows[0]["UnitName"].ToString();
                    dataRow["Month"] = Month(i.ToString());
                    dataRow["JoiningDate"] = Convert.ToDateTime(dtprev.Rows[0]["JoiningDate"].ToString()).ToString("dd-MMM-yyyy");
                    dataRow["PFStartDate"] = Convert.ToDateTime(dtprev.Rows[0]["PFStartDate"].ToString()).ToString("dd-MMM-yyyy");
                    dataRow["LoginName"] = dtprev.Rows[0]["LoginName"].ToString();
                    dataRow["Year"] = year;
                    
                    if (dtdetail.Rows.Count>0)
                    {

                        dataRow["PrFund"] = dtdetail.Rows[0]["PrFund"].ToString();


                    }
                    else
                    {
                        dataRow["PrFund"] = "0.00";
                    }

                    prfund =prfund+Convert.ToDecimal(dataRow["PrFund"].ToString());

                    dataRow["TotalPF"] = Convert.ToDecimal(dtprev.Rows[0]["PreviousAllYearPf"].ToString()) + prfund;
                    dataRow["TotalPFCom"] = Convert.ToDecimal(dtprev.Rows[0]["PreviousAllYearComPf"].ToString()) + prfund;
                    dataRow["AllTotal"] = Convert.ToDecimal(dataRow["TotalPF"]) +
                                          Convert.ToDecimal(dataRow["TotalPFCom"]);

                    aDataTable.Rows.Add(dataRow);
                }

                

            }

            return aDataTable;
        }

        public bool UpdatePFEligibilityStatus(string empId)
        {
            return aPfSetupDal.UpdatePFEligibilityStatus(empId);
        }
    }
}
