﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class PFStartStopBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        PFStartStopDAL aPfStartStopDal = new PFStartStopDAL();
        public bool SaveDataForPFStartStop(PFStartStop aPfStartStop)
        {
            try
            {
                //if (!aArrearDal.HasArrear(aArrear))
                //{
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aPfStartStop.PFSSId = aClsPrimaryKeyFind.PrimaryKeyMax("PFSSId", "tblPFStartStop", "HRDB");
                aPfStartStopDal.SavePFStartStop(aPfStartStop);
                return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aPfStartStopDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadPFSetup(string empInfoId)
        {
            return aPfStartStopDal.LoadPFSetup(empInfoId);
        }
        public bool UpdatePFSetup(bool status, string empinfoId)
        {
            return aPfStartStopDal.UpdatePFSetup(status, empinfoId);
        }
    }
}
