﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ProbationBLL
    {
        ProbationDAL aProbationDAL = new ProbationDAL();
        public bool SaveDataForProbation(Probation aProbation)
        {
            try
            {

                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                aProbation.ProbitionId = aClsPrimaryKeyFind.PrimaryKeyMax("ProbitionId", "tblEmpProbation", "HRDB");
                aProbationDAL.SaveProbationStatus(aProbation);
                return true;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public DataTable LoadEmpBirthData(string day, string month)
        {
            return aProbationDAL.LoadEmpBirthData(day, month);
        }

        public DataTable LoadEmployee(string date)
        {
            return aProbationDAL.LoadEmployee(date);
        }
        public DataTable LoadEmployeeInfo(string empid)
        {
            return aProbationDAL.LoadEmployeeInfo(empid);
        }
        public bool UpdateProbation(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aProbationDAL.UpdateProbation(aEmpGeneralInfo);
        }
        public bool UpdateConfirm(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aProbationDAL.UpdateConfirm(aEmpGeneralInfo);
        }
    }
}
