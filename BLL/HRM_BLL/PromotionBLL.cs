﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class PromotionBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        PromotionDAL aPromotionDAL = new PromotionDAL();
        public bool SaveDataForPromotion(Promotion aPromotion)
        {
            try
            {
                if (!aPromotionDAL.HasPromotion(aPromotion))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aPromotion.PromotionId = aClsPrimaryKeyFind.PrimaryKeyMax("PromotionId", "tblPromotion", "HRDB");
                    aPromotionDAL.SavePromotion(aPromotion);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool SaveDataForPromotionWithInc(Promotion aPromotion,out int promotionId)
        {
            try
            {
                if (!aPromotionDAL.HasPromotion(aPromotion))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aPromotion.PromotionId = aClsPrimaryKeyFind.PrimaryKeyMax("PromotionId", "tblPromotion", "HRDB");

                    promotionId = aPromotion.PromotionId;

                    aPromotionDAL.SavePromotion(aPromotion);
                    return true;
                }
                else
                {
                    promotionId = 0;
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aPromotionDAL.LoadGradeName(aDropDownList);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aPromotionDAL.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aPromotionDAL.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aPromotionDAL.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aPromotionDAL.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aPromotionDAL.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aPromotionDAL.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aPromotionDAL.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aPromotionDAL.LoadSalGradeName(aDropDownList);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aPromotionDAL.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForPromotion(Promotion aPromotion)
        {
            return aPromotionDAL.UpdatePromotion(aPromotion);
        }
        public DataTable LoadPromotion()
        {
            return aPromotionDAL.LoadPromotionView();
        }
        public Promotion PromotionEditLoad(string PromotionId)
        {
            return aPromotionDAL.PromotionEditLoad(PromotionId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aPromotionDAL.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aPromotionDAL.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aPromotionDAL.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aPromotionDAL.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aPromotionDAL.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aPromotionDAL.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aPromotionDAL.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aPromotionDAL.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aPromotionDAL.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aPromotionDAL.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(Promotion aPromotion)
        {
            return aPromotionDAL.ApprovalUpdateDAL(aPromotion);
        }
       
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aPromotionDAL.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public DataTable LoadPromotionViewForApproval(string ActionStatus)
        {
            return aPromotionDAL.LoadPromotionViewForApproval(ActionStatus);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aPromotionDAL.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aPromotionDAL.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void DeleteDataBLL(string PromotionId)
        {
            aPromotionDAL.DeleteData(PromotionId);

        }
    }
}
