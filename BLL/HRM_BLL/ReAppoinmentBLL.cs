﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ReAppoinmentBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        ReAppointmentDAL aReAppointmentDAL = new ReAppointmentDAL();
        public bool SaveDataForReAppointment(ReAppointment aReAppointment)
        {
            try
            {
                if (!aReAppointmentDAL.HasReAppointment(aReAppointment))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aReAppointment.ReAppointmentId = aClsPrimaryKeyFind.PrimaryKeyMax("ReAppointmentId", "dbo.tblReApointment", "HRDB");
                    aReAppointmentDAL.SaveReAppointment(aReAppointment);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aReAppointmentDAL.LoadDesignationName(aDropDownList);
        }
        public bool HasjobLeft(string Empid)
        {
            if ( aReAppointmentDAL.HasjobLeft(Empid))
            {
                return true;
            }
            else
            {
                return false;
            }
            //aReAppointmentDAL.HasjobLeft(Empid);
            //return true;
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aReAppointmentDAL.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReAppointmentDAL.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReAppointmentDAL.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aReAppointmentDAL.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReAppointmentDAL.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aReAppointmentDAL.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReAppointmentDAL.LoadSalGradeName(aDropDownList);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReAppointmentDAL.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(ReAppointment aReAppointment)
        {
            return aReAppointmentDAL.UpdateReAppointment(aReAppointment);
        }
        public DataTable LoadReAppointment()
        {
            return aReAppointmentDAL.LoadReAppointmentView();
        }
        public ReAppointment ReAppointmentEditLoad(string ReAppointmentId)
        {
            return aReAppointmentDAL.ReAppointmentEditLoad(ReAppointmentId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aReAppointmentDAL.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aReAppointmentDAL.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aReAppointmentDAL.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aReAppointmentDAL.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aReAppointmentDAL.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aReAppointmentDAL.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aReAppointmentDAL.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aReAppointmentDAL.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aReAppointmentDAL.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aReAppointmentDAL.LoadEmpType(EmpTypeId);
        }
        public bool EmployeeStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aReAppointmentDAL.EmployeeStatus(aEmpGeneralInfo);
        }
        public bool ApprovalUpdateBLL(ReAppointment aReAppointment)
        {
            return aReAppointmentDAL.ApprovalUpdateDAL(aReAppointment);
        }
        public DataTable LoadReAppointmentViewForApproval(string ActionStatus)
        {
            return aReAppointmentDAL.LoadReAppointmentViewForApproval(ActionStatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aReAppointmentDAL.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aReAppointmentDAL.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aReAppointmentDAL.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void DeleteDataBLL(string ReAppointmentId)
        {
            aReAppointmentDAL.DeleteData(ReAppointmentId);

        }
    }
}
