﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ReleaseSuspendBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        ReleaseSuspendDAL aReleaseSuspendDal = new ReleaseSuspendDAL();
        public bool SaveDataForReleaseSuspend(ReleaseSuspend aReleaseSuspend)
        {
            try
            {
                if (!aReleaseSuspendDal.HasReleaseSuspend(aReleaseSuspend))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aReleaseSuspend.RelsSuspendId = aClsPrimaryKeyFind.PrimaryKeyMax("RelsSuspendId", "tblSuspendRelease", "HRDB");
                    aReleaseSuspendDal.SaveReleaseSuspend(aReleaseSuspend);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aReleaseSuspendDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList,string divisionId)
        {
            aReleaseSuspendDal.LoadDepartmentName(aDropDownList,divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReleaseSuspendDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReleaseSuspendDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList,string companyId)
        {
            aReleaseSuspendDal.LoadUnitName(aDropDownList,companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReleaseSuspendDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList,string DeptId)
        {
            aReleaseSuspendDal.LoadSectionName(aDropDownList,DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReleaseSuspendDal.LoadSalGradeName(aDropDownList);
        }
        
        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aReleaseSuspendDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(ReleaseSuspend aReleaseSuspend)
        {
            return aReleaseSuspendDal.UpdateReleaseSuspend(aReleaseSuspend);
        }
       
        public DataTable LoadReleaseSuspend()
        {
            return aReleaseSuspendDal.LoadReleaseSuspendView();
        }
        public ReleaseSuspend EmpSalBenefitEditLoad(string RelsSuspendId)
        {
            return aReleaseSuspendDal.EmpReleaseSuspendEditLoad(RelsSuspendId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aReleaseSuspendDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aReleaseSuspendDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aReleaseSuspendDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aReleaseSuspendDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aReleaseSuspendDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aReleaseSuspendDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aReleaseSuspendDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aReleaseSuspendDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aReleaseSuspendDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aReleaseSuspendDal.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(ReleaseSuspend aReleaseSuspend)
        {
            return aReleaseSuspendDal.ApprovalUpdateDAL(aReleaseSuspend);
        }
        public DataTable LoadReleaseSuspendViewForApproval(string actionstatus)
        {
            return aReleaseSuspendDal.LoadReleaseSuspendViewForApproval(actionstatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aReleaseSuspendDal.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public void DeleteDataBLL(string RelsSuspendId)
        {
            aReleaseSuspendDal.DeleteData(RelsSuspendId);

        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aReleaseSuspendDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aReleaseSuspendDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
