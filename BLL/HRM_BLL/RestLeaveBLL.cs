﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;
using Library.DAL.HRM_DAL;
using System.Data;

namespace Library.BLL.HRM_BLL
{
    public class RestLeaveBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        RestLeaveDAL aRestLeaveDal = new RestLeaveDAL();
        public bool SaveDataForRestLeave(RestLeave aOnDuty)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aOnDuty.RestLeaveId = aClsPrimaryKeyFind.PrimaryKeyMax("RestLeaveId", "tblRestLeave", "HRDB");
                aRestLeaveDal.SaveDataForOnDuty(aOnDuty);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateDataForOnDuty(RestLeave aOnDuty)
        {
            return aRestLeaveDal.UpdateOnDuty(aOnDuty);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadRestLeaveView()
        {
            return aRestLeaveDal.LoadRestLeaveView();
        }
        public DataTable LoadRestLeaveViewForApproval(string actionstatus)
        {
            return aRestLeaveDal.LoadRestLeaveViewForApproval(actionstatus);
        }
        public RestLeave OnDutyEditLoad(string OnDutyId)
        {
            return aRestLeaveDal.OnDutyEditLoad(OnDutyId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aRestLeaveDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aRestLeaveDal.LoadEmpInfoCode(EmpInfoId);
        }

        public bool ApprovalUpdateBLL(RestLeave aOnDuty)
        {
            return aRestLeaveDal.ApprovalUpdateDAL(aOnDuty);
        }
        public void DeleteDataBLL(string OnDutyId)
        {
            aRestLeaveDal.DeleteData(OnDutyId);

        }
        public DataTable LoadGroupWiseEmp(string groupId)
        {
            return aRestLeaveDal.LoadGroupWiseEmp(groupId);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aRestLeaveDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aRestLeaveDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
