﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class RptHeadingBLL
    {
        RptHeadingDAL aRptHeadingDAL = new RptHeadingDAL();
        public bool SaveRptHeaderData(RptHeader aRptHeader,out int rptId)
        {
            try
            {
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                aRptHeader.RptId = aClsPrimaryKeyFind.PrimaryKeyMax("RptId", "tblReportHeading", "HRDB");
                rptId = 0;
                rptId = aRptHeader.RptId;
                aRptHeadingDAL.SaveDataForRptHeader(aRptHeader);
                return true;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        
        public DataTable LoadRptHeaderReport()
        {
            return aRptHeadingDAL.LoadRptHeaderReport();
        }

        public bool UpdateDataForRptHeader(RptHeader aRptHeader)
        {
            return aRptHeadingDAL.UpdateRptHeader(aRptHeader);
        }
        public bool UpdateRptImage(RptHeader aRptHeader)
        {
            return aRptHeadingDAL.UpdateRptImage(aRptHeader);
        }
        public DataTable LoadRptHeader()
        {
            return aRptHeadingDAL.LoadRptHeader();
        }

        public RptHeader RptHeaderEditLoad(string RptHeaderId)
        {
            return aRptHeadingDAL.RptHeaderEditLoad(RptHeaderId);
        }


    }
}
