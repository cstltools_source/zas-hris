﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;

namespace Library.BLL.HRM_BLL
{
    public class RptImageBLL
    {
        RptImageDAL aRptImageDal=new RptImageDAL();
        public bool SaveEmpImage(byte[] imagebyt, int rptId)
        {
            return aRptImageDal.SaveEmpImage(imagebyt, rptId);

        }
    }
}
