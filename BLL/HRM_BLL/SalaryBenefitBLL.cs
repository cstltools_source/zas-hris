﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SalaryBenefitBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        SalaryBenefitDAL aSalBenefitDal = new SalaryBenefitDAL();
        public bool SaveDataForEmpSalBenefit(EmpSalBenefit aEmpSalBenefit)
        {
            try
            {
                if (!aSalBenefitDal.HasEmpSalBenefit(aEmpSalBenefit))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aEmpSalBenefit.BenefitId = aClsPrimaryKeyFind.PrimaryKeyMax("BenefitId", "tblSalaryBenefit", "HRDB");
                    aSalBenefitDal.SaveEmpSalBenefit(aEmpSalBenefit);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aSalBenefitDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList,string divisionId)
        {
            aSalBenefitDal.LoadDepartmentName(aDropDownList,divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSalBenefitDal.LoadEmployeeName(aDropDownList);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSalBenefitDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList,string companyId)
        {
            aSalBenefitDal.LoadUnitName(aDropDownList,companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSalBenefitDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList,string DeptId)
        {
            aSalBenefitDal.LoadSectionName(aDropDownList,DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSalBenefitDal.LoadSalGradeName(aDropDownList);
        }
        public void LoadGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSalBenefitDal.LoadGradeName(aDropDownList);
        }
        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSalBenefitDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(EmpSalBenefit aEmpSalBenefit)
        {
            return aSalBenefitDal.UpdateEmpSalBenefit(aEmpSalBenefit);
        }
        public DataTable LoadEmpSalBenefitView()
        {
            return aSalBenefitDal.LoadEmpSalBenefitView();
        }
        public EmpSalBenefit EmpSalBenefitEditLoad(string BenefitId)
        {
            return aSalBenefitDal.EmpSalBenefitEditLoad(BenefitId);
        }
        public DataTable LoadEmpInfo(string empcode)
        {
            return aSalBenefitDal.LoadEmpInfo(empcode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aSalBenefitDal.LoadEmpInfoCode(EmpInfoId);
        }
        public bool ApprovalUpdateBLL(EmpSalBenefit aEmpSalBenefit)
        {
            return aSalBenefitDal.ApprovalUpdateDAL(aEmpSalBenefit);
        }
        public DataTable LoadSalaryBenefitViewForApproval(string ActionStatus)
        {
            return aSalBenefitDal.LoadSalaryBenefitViewForApproval(ActionStatus);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aSalBenefitDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aSalBenefitDal.LoadForApprovalConditionDAL(pageName, userName);
        }
        public void DeleteDataBLL(string BenefitId)
        {
            aSalBenefitDal.DeleteData(BenefitId);
        }
    }
}
