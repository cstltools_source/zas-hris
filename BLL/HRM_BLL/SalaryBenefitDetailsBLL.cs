﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SalaryBenefitDetailsBLL
    {
        SalaryBenefitDetailsDAL aBenefitDetailsDal = new SalaryBenefitDetailsDAL();
        public bool SaveSalaryBenefitDetails(SalaryBenefitDetails aBenefitDetails)
        {
            try
            {
                if (!aBenefitDetailsDal.HasSalaryBenefitDetails(aBenefitDetails))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aBenefitDetails.SBDId = aClsPrimaryKeyFind.PrimaryKeyMax("SBDId", "tblSalaryBenefitDetails","HRDB");
                    aBenefitDetailsDal.HasSalaryBenefitDetails( aBenefitDetails);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateaDataforBenefitDetails(SalaryBenefitDetails aBenefitDetails)
        {
            return aBenefitDetailsDal.UpdateaBenefitDetails(aBenefitDetails);
        }

        public DataTable LoadDataforSalaryBenefitDetailsView()
        {
            return aBenefitDetailsDal.LoadSalaryBenefitDetailsView();
        }

        public SalaryBenefitDetails SelBenefitDetailsEditLoad(string SBDId)
        {
            return aBenefitDetailsDal.SalaryBenefitDetailsEditLoad(SBDId);
        }
    }
}
