﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SalaryDeductionBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        SalaryDeductionDAL aDeductionDal = new SalaryDeductionDAL();
        public bool SaveDataForSalaryDeduction(SalaryDeduction aSalaryDeduction)
        {
            try
            {
               ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
               aSalaryDeduction.SDId = aClsPrimaryKeyFind.PrimaryKeyMax("SDId", "tblSalaryDeduction","HRDB");
                aDeductionDal.SaveSalaryDeduction(aSalaryDeduction);
               return true; 
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateaDataforSalaryDeduction(SalaryDeduction aSalaryDeduction)
        {
            return aDeductionDal.UpdateSalaryDeduction(aSalaryDeduction);
        }
        public DataTable LoadDataforSalaryDeductionView()
        {
            return aDeductionDal.LoadSalaryDeductionView();
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public SalaryDeduction SalaryDeductionEditLoad(string SDId)
        {
            return aDeductionDal.SalaryDeductionEditLoad(SDId);
        }
        public void LoadSalHeadName(DropDownList ddl)
        {
            aDeductionDal.LoadSalHeadName(ddl);
        }
        public bool ApprovalUpdateBLL(SalaryDeduction aSalaryDeduction)
        {
            return aDeductionDal.ApprovalUpdateDAL(aSalaryDeduction);
        }
        
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList,string divisionId)
        {
            aDeductionDal.LoadDepartmentName(aDropDownList,divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList,string companyId)
        {
            aDeductionDal.LoadUnitName(aDropDownList,companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList,string DeptId)
        {
            aDeductionDal.LoadSectionName(aDropDownList,DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadSalGradeName(aDropDownList);
        }
        
        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aDeductionDal.LoadEmpTypeName(aDropDownList);
        }
        
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aDeductionDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aDeductionDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aDeductionDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aDeductionDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aDeductionDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aDeductionDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aDeductionDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aDeductionDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aDeductionDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aDeductionDal.LoadEmpType(EmpTypeId);
        }
        public void DeleteDataBLL(string SDId)
        {
            aDeductionDal.DeleteData(SDId);
        }
        public DataTable LoadSalaryDeductionViewForApproval(string ActionStatus)
        {
            return aDeductionDal.LoadSalaryDeductionViewForApproval(ActionStatus);
        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aDeductionDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aDeductionDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
