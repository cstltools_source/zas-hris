﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SalaryHeadBLL
    {
        SalaryHeadDAL aSalaryHeadDal = new SalaryHeadDAL();
        public bool SaveForSalaryHead(SalaryHead aSalaryHead)
        {
            try 
            {
                if (!aSalaryHeadDal.HasSalaryHeadName(aSalaryHead))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aSalaryHead.SalaryHeadId = aClsPrimaryKeyFind.PrimaryKeyMax("SalaryHeadId", "tblSalaryHead","HRDB");
                    aSalaryHead.SalaryHeadCode = SalaryHeadCodeGenerator(aSalaryHead.SalaryHeadId);
                    aSalaryHeadDal.SaveSalaryHead(aSalaryHead);
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public string SalaryHeadCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "SHEAD-" + Id;
            return code;
        }

        public bool UpdateDataForSalaryHead(SalaryHead aSalaryHead)
        {
            return aSalaryHeadDal.UpdateSalaryHeadInfoUpdate(aSalaryHead);
        }
        public DataTable LoadSalaryHeadView()
        {
            return aSalaryHeadDal.LoadSalaryHead();
        }
        public SalaryHead SalaryHeadEditLoad(string salaryHeadId)
        {
            return aSalaryHeadDal.SalaryHeadEditLoad(salaryHeadId);
        }
        public void LoadSalaryHeadType(DropDownList ddl)
        {
            aSalaryHeadDal.LoadSalaryHeadType(ddl);
        }
    }
}
