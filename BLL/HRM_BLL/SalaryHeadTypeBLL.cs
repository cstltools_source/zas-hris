﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SalaryHeadTypeBLL
    {
        SalaryHeadTypeDAL aSalaryHeadTypeDal = new SalaryHeadTypeDAL();
        public bool SaveDataForSalaryHeadType(SalaryHeadType aSalaryHeadType)
        {
            try
            {
                if (!aSalaryHeadTypeDal.HasSalaryHeadType(aSalaryHeadType))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aSalaryHeadType.SHeadTypeId = aClsPrimaryKeyFind.PrimaryKeyMax("SHeadTypeId", "tblSalaryHeadType", "HRDB");
                    aSalaryHeadTypeDal.SaveDataForSalaryHeadType(aSalaryHeadType);
                    return true;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateSalaryHeadType(SalaryHeadType aSalaryHeadType)
        {
            return aSalaryHeadTypeDal.UpdateSalaryHeadType(aSalaryHeadType);
        }

        public DataTable LoadSalaryHeadTypeView()
        {
            return aSalaryHeadTypeDal.LoadSalaryHeadTypeView();
        }

        public SalaryHeadType SalaryHeadTypeEditLoad(string SHeadTypeId)
        {
            return aSalaryHeadTypeDal.SalaryHeadTypeEditLoad(SHeadTypeId);
        }
    }
}
