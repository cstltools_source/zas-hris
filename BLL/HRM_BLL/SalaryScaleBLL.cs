﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SalaryScaleBll
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        SalaryScaleDAL aSalaryScaleDal = new SalaryScaleDAL();
        public bool SaveDataForSalaryScale(SalaryScale aSalaryScale)
        {
            try
            {
                
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                aSalaryScale.SalScaleId = aClsPrimaryKeyFind.PrimaryKeyMax("SalScaleId", "tblSalaryGradeOrScale", "HRDB");
                aSalaryScale.SalScaleCode = SalGradeCodeGenerator(aSalaryScale.SalScaleId);
                aSalaryScale.Gross = Gross(aSalaryScale.Medical, aSalaryScale.HouseRent, aSalaryScale.Conveyance,
                                           aSalaryScale.Foodallowance, aSalaryScale.BasicSalary);
                aSalaryScaleDal.SaveGradeSalary(aSalaryScale);
                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public  decimal Gross(decimal medical,decimal houserent,decimal conveyance, decimal foodallowance,decimal basic)
        {
            decimal grs =0;
            grs = (medical + houserent + conveyance + foodallowance + basic);
            return grs;
        }

        public string SalGradeCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "GRAD-" + Id;
            return code;
        }

        public SalaryScale SalaryScaleEditLoad(string gradeId)
        {
            return aSalaryScaleDal.SalaryScaleEditLoad(gradeId);
        }

        public bool UpdateDataForSalaryScale(SalaryScale aGradeSalary)
        {
            aGradeSalary.Gross = Gross(aGradeSalary.Medical, aGradeSalary.HouseRent, aGradeSalary.Conveyance,
                                           aGradeSalary.Foodallowance, aGradeSalary.BasicSalary);
            return aSalaryScaleDal.UpdateSalaryScale(aGradeSalary);
        }

        public DataTable LoadSalaryScaleView()
        {
            return aSalaryScaleDal.LoadSalaryScaleView();
        }
        public bool ApprovalUpdateDAL(SalaryScale aSalaryScale)
        {
            return aSalaryScaleDal.ApprovalUpdateDAL(aSalaryScale);
        }

        public DataTable LoadSalaryScaleViewForApproval(string actionstatus)
        {
            return aSalaryScaleDal.LoadSalaryScaleViewForApproval(actionstatus);
        }
        public void DeleteDataBLL(string SalScaleId)
        {
            aSalaryScaleDal.DeleteData(SalScaleId);

        }

        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aSalaryScaleDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aSalaryScaleDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
