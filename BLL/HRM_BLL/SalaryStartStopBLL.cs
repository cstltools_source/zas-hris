﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SalaryStartStopBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        SalaryStartStopDAL aSalaryStartStopDal = new SalaryStartStopDAL();
        public bool SaveDataForSalaryStartStop(SalaryStartStop aSalaryStartStop)
        {
            try
            {
                if (!aSalaryStartStopDal.HsSalaryStartStop(aSalaryStartStop))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aSalaryStartStop.SalaryStopId = aClsPrimaryKeyFind.PrimaryKeyMax("SalaryStopId", "tblSalaryStartStop", "HRDB");
                    aSalaryStartStopDal.SaveSalaryStartStop(aSalaryStartStop);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool UpdateSalaryStartStop(SalaryStartStop aSalaryStartStop)
        {
            return aSalaryStartStopDal.UpdateSalaryStartStop(aSalaryStartStop);
        }
        public void LoadFinancialYear(DropDownList ddl)
        {
            
            aSalaryStartStopDal.LoadFinancialYear(ddl);
        }
        public DataTable LoadSalaryStop(string fytId, string year, string month)
        {
            return aSalaryStartStopDal.LoadSalaryStop(fytId, year, month);
        }
    }
}
