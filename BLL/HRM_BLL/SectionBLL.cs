﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SectionBLL
    {
        SectionDAL aSectionDal = new SectionDAL();
        public bool SaveDataForSection(Section aSection)
        {
            try
            {
                if (!aSectionDal.HasSectionName(aSection))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aSection.SectionId = aClsPrimaryKeyFind.PrimaryKeyMax("SectionId", "tblSection", "HRDB");
                    aSection.SectionCode = SectionCodeGenerator(aSection.SectionId);
                    aSectionDal.SaveSectionInfo(aSection);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool UpdateDataForSection(Section aSection)
        {
            return aSectionDal.UpdateSectionInfo(aSection);
        }

        public string SectionCodeGenerator(int id)
        {
            string code = string.Empty;
            string Id = id.ToString();
            if (Id.Length == 1)
            {
                Id = "00" + Id;
            }
            if (Id.Length == 2)
            {
                Id = "0" + Id;
            }
            code = "SEC-" + Id;
            return code;
        }

        public void LoadDepartment(DropDownList ddl)
        {
            SectionDAL aSectionDal = new SectionDAL();
            aSectionDal.LoadDepartmentName(ddl);
        }

        public DataTable LoadSectionView()
        {
            return aSectionDal.LoadSectionView();
        }

        public Section SectionEditLoad(string sectionId)
        {
            return aSectionDal.SectionEditLoad(sectionId);
        }
    }
}
