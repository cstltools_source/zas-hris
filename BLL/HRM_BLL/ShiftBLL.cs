﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ShiftBLL
    {      
        ShiftDAL aShiftDal = new ShiftDAL();
        public bool SaveDataForShift(Shift aShift)
        {
            try
            {
                if (!aShiftDal.HasShiftName(aShift))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aShift.ShiftId = aClsPrimaryKeyFind.PrimaryKeyMax("ShiftId", "tblShift", "HRDB");
                    aShiftDal.SaveShift(aShift);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool UpdateDataForShift(Shift aShift)
        {
            return aShiftDal.UpdateShift(aShift);
        }

        public DataTable LoadShiftView()
        {
            return aShiftDal.LoadShiftView();
        }

        public Shift ShiftEditLoad(string shiftId)
        {
            return aShiftDal.ShiftEditLoad(shiftId);
        }
    }
}
