﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class ShiftWiseGroupBLL
    {
        ShiftWiseGroupDAL aShiftWiseGroupDal = new ShiftWiseGroupDAL();
        public bool SaveShiftWiseGroupData(ShiftWiseGroup aShiftWiseGroup)
        {
            try
            {

                //if (!aShiftWiseGroupDal.HasHolidayDate(aShiftWiseGroup))
                //{
                ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                aShiftWiseGroup.GSAId = aClsPrimaryKeyFind.PrimaryKeyMax("GSAId", "dbo.tblShiftWiseGroup", "HRDB");

                aShiftWiseGroupDal.SaveDataForShiftWiseGroup(aShiftWiseGroup);
                return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public void LoadGroupName(DropDownList ddl, string unitId)
        {
            aShiftWiseGroupDal.LoadGroupName(ddl,unitId);
        }

        public void LoadGroup(DropDownList ddl)
        {
            aShiftWiseGroupDal.LoadGroupName(ddl);
        }
        public DataTable LoadDate(string groupId, string fromdt, string todt)
        {
            return aShiftWiseGroupDal.LoadDate(groupId, fromdt, todt);
        }
        public void LoadShift(DropDownList ddl)
        {
            aShiftWiseGroupDal.LoadShift(ddl);
        }
        public bool HasWeeklyHoliday(string dayname, string empid)
        {
            return aShiftWiseGroupDal.HasWeeklyHoliday(dayname, empid);
        }
        public string DayName(string dayName)
        {
            return aShiftWiseGroupDal.DayName(dayName);
        }
        public DataTable LoadShift(string shiftId)
        {
            return aShiftWiseGroupDal.LoadShift(shiftId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aShiftWiseGroupDal.LoadEmpInfo(EmpMasterCode);
        }

        public bool ApprovalUpdateDAL(ShiftWiseGroup aShiftWiseGroup)
        {
            return aShiftWiseGroupDal.ApprovalUpdateDAL(aShiftWiseGroup);
        }
        public DataTable LoadShiftWiseGroupViewApproval(string actionstatus)
        {
            return aShiftWiseGroupDal.LoadShiftWiseGroupViewApproval(actionstatus);
        }

        public DataTable LoadHolidaView()
        {
            return aShiftWiseGroupDal.LoadHolidayView();
        }

        //public ShiftWiseGroup HolidayEditLoad(string holidaId)
        //{
        //    return aShiftWiseGroupDal.ShiftWiseGroupEditLoad(holidaId);
        //}

        //public bool UpdateDataForHoliday(ShiftWiseGroup aShiftWiseGroup)
        //{
        //    return aShiftWiseGroupDal.UpdateShiftWiseGroup(aShiftWiseGroup);
        //}
        public DataTable LoadShiftGroupView()
        {
            return aShiftWiseGroupDal.LoadShiftGroupView();
        }

        public bool DeleteShiftGroupDAL(string id)
        {
            return aShiftWiseGroupDal.DeleteShiftGroupDAL(id);
        }

        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aShiftWiseGroupDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aShiftWiseGroupDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
