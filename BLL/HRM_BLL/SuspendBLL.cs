﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class SuspendBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        SuspendDAL aSuspendDal = new SuspendDAL();
        public bool SaveDataForSuspend(Suspend aSuspend)
        {
            try
            {
                if (!aSuspendDal.HasSuspend(aSuspend))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aSuspend.SuspendId = aClsPrimaryKeyFind.PrimaryKeyMax("SuspendId", "tblSuspend", "HRDB");
                    aSuspendDal.SaveSuspend(aSuspend);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aSuspendDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList,string divisionId)
        {
            aSuspendDal.LoadDepartmentName(aDropDownList,divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSuspendDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSuspendDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList,string companyId)
        {
            aSuspendDal.LoadUnitName(aDropDownList,companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSuspendDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList,string DeptId)
        {
            aSuspendDal.LoadSectionName(aDropDownList,DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSuspendDal.LoadSalGradeName(aDropDownList);
        }
        
        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aSuspendDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(Suspend aSuspend)
        {
            return aSuspendDal.UpdateSuspend(aSuspend);
        }
        public DataTable LoadSuspend()
        {
            return aSuspendDal.LoadSuspendView();
        }
        public Suspend EmpSalBenefitEditLoad(string SuspendId)
        {
            return aSuspendDal.EmpSuspendEditLoad(SuspendId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aSuspendDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aSuspendDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aSuspendDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aSuspendDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aSuspendDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aSuspendDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aSuspendDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aSuspendDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aSuspendDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aSuspendDal.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(Suspend aSuspend)
        {
            return aSuspendDal.ApprovalUpdateDAL(aSuspend);
        }
        public DataTable LoadSuspendViewForApproval(string actionstatus)
        {
            return aSuspendDal.LoadSuspendViewForApproval(actionstatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aSuspendDal.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public void DeleteDataBLL(string SuspendId)
        {
            aSuspendDal.DeleteData(SuspendId);

        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aSuspendDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aSuspendDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
