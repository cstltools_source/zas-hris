﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;

namespace Library.BLL.HRM_BLL
{
    public class SuspendJobLeftReportBLL
    {
        SuspendJobLeftReportDAL aJobLeftReportDal=new SuspendJobLeftReportDAL();
        public DataTable RptHeader()
        {
            return aJobLeftReportDal.RptHeader();
        }
        public DataTable SuspendRpt(string empid)
        {
            return aJobLeftReportDal.SuspendRpt(empid);
        }
        public DataTable JobLeftRpt(string empid)
        {
            return aJobLeftReportDal.JobLeftRpt(empid);
        }
    }
}
