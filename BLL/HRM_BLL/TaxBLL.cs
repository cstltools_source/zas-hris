﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class TaxBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        TaxDAL aTaxDal = new TaxDAL();
        public bool SaveDataForTax(Tax aTax)
        {
            try
            {
                if (!aTaxDal.HasTax(aTax))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aTax.TaxId = aClsPrimaryKeyFind.PrimaryKeyMax("TaxId", "tblTax", "HRDB");
                    aTaxDal.SaveTax(aTax);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public bool SaveDataForTaxDetails(List<TaxDetails> aTaxDetailsList)
        {
            try
            {
                TaxDetailsDAL aTaxDetailsDal=new TaxDetailsDAL();
                foreach (var taxDetailse in aTaxDetailsList)
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    taxDetailse.TaxDetailsId = aClsPrimaryKeyFind.PrimaryKeyMax("TaxDetailsId", "tblTAXDetails", "HRDB");
                    aTaxDetailsDal.SaveDataforTaxDetails(taxDetailse);
                }
                
                return true;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public DataTable GetEmployeeInformation(string parameter)
        {
            TaxDetailsDAL aTaxDetailsDal=new TaxDetailsDAL();
            return aTaxDetailsDal.GetEmployeeInformation(parameter);
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList)
        {
            aTaxDal.LoadDesignationName(aDropDownList);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aTaxDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTaxDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTaxDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aTaxDal.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTaxDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aTaxDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTaxDal.LoadSalGradeName(aDropDownList);
        }
        public void LoadFinancialYear(DropDownList ddl)
        {
            TaxDetailsDAL aTaxDetailsDal=new TaxDetailsDAL();
            aTaxDetailsDal.LoadFinancialYear(ddl);
        }
        public bool UpdateTaxDetail(TaxDetails aTaxDetails)
        {
            TaxDetailsDAL aTaxDetailsDal=new TaxDetailsDAL();
            return aTaxDetailsDal.UpdateTaxDetail(aTaxDetails);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTaxDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForaTax(Tax aTax)
        {
            return aTaxDal.UpdateTax(aTax);
        }
        public DataTable LoadTax()
        {
            return aTaxDal.LoadTaxView();
        }
        public Tax TaxEditLoad(string TaxId)
        {
            return aTaxDal.TaxEditLoad(TaxId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aTaxDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aTaxDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aTaxDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aTaxDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aTaxDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aTaxDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aTaxDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aTaxDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aTaxDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aTaxDal.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(Tax aTax)
        {
            return aTaxDal.ApprovalUpdateDAL(aTax);
        }
        public DataTable LoadTaxViewForApproval(string actionstatus)
        {
            return aTaxDal.LoadTaxViewForApproval(actionstatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aTaxDal.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public void DeleteDataBLL(string TaxId)
        {
            aTaxDal.DeleteData(TaxId);

        }

        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aTaxDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aTaxDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
