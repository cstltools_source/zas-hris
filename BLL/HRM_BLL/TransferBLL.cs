﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class TransferBLL
    {
        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        TransferDAL aTransferDal = new TransferDAL();
        public bool SaveDataForTransfer(Transfer aTransfer)
        {
            try
            {
                //if (!aTransferDal.HasTransfer(aTransfer))
                //{
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aTransfer.TransferId = aClsPrimaryKeyFind.PrimaryKeyMax("TransferId", "tblTransfer", "HRDB");
                    aTransferDal.SaveTransfer(aTransfer);
                    return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public void LoadGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTransferDal.LoadGradeName(aDropDownList);
        }
        public void LoadDesignationToDropDownBLL(DropDownList aDropDownList, string gradeId)
        {
            aTransferDal.LoadDesignationName(aDropDownList, gradeId);
        }
        public void LoadDepartmentToDropDownBLL(DropDownList aDropDownList, string divisionId)
        {
            aTransferDal.LoadDepartmentName(aDropDownList, divisionId);
        }
        public void LoadEmployeeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTransferDal.LoadEmployeeName(aDropDownList);
        }
        public void LoadCompanyNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTransferDal.LoadCompanyName(aDropDownList);
        }
        public void LoadUnitNameToDropDownBLL(DropDownList aDropDownList, string companyId)
        {
            aTransferDal.LoadUnitName(aDropDownList, companyId);
        }
        public void LoadDivisionNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTransferDal.LoadDivisionName(aDropDownList);
        }
        public void LoadSectionNameToDropDownBLL(DropDownList aDropDownList, string DeptId)
        {
            aTransferDal.LoadSectionName(aDropDownList, DeptId);
        }
        public void LoadSalGradeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTransferDal.LoadSalGradeName(aDropDownList);
        }

        public void LoadEmpTypeNameToDropDownBLL(DropDownList aDropDownList)
        {
            aTransferDal.LoadEmpTypeName(aDropDownList);
        }
        public bool UpdateDataForEmpSalBenefit(Transfer aTransfer)
        {
            return aTransferDal.UpdateTransfer(aTransfer);
        }
        public DataTable LoadTransfer()
        {
            return aTransferDal.LoadTransferView();
        }
        public Transfer TransferEditLoad(string TransferId)
        {
            return aTransferDal.TransferEditLoad(TransferId);
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aTransferDal.LoadEmpInfo(EmpMasterCode);
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            return aTransferDal.LoadEmpInfoCode(EmpInfoId);
        }
        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            return aTransferDal.LoadCompanyInfo(CompanyInfoId);
        }

        public DataTable LoadUnit(string UnitId)
        {
            return aTransferDal.LoadUnit(UnitId);
        }

        public DataTable Loadivision(string DivisionId)
        {
            return aTransferDal.Loadivision(DivisionId);
        }
        public DataTable LoadDepartment(string DeptId)
        {
            return aTransferDal.LoadDepartment(DeptId);
        }

        public DataTable LoadDesignation(string DesigId)
        {
            return aTransferDal.LoadDesignation(DesigId);
        }

        public DataTable LoadSection(string SectionId)
        {
            return aTransferDal.LoadSection(SectionId);
        }

        public DataTable LoadGrade(string GradeId)
        {
            return aTransferDal.LoadGrade(GradeId);
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            return aTransferDal.LoadEmpType(EmpTypeId);
        }
        public bool ApprovalUpdateBLL(Transfer aTransfer)
        {
            return aTransferDal.ApprovalUpdateDAL(aTransfer);
        }
        public DataTable LoadTransferViewForApproval(string actionstatus)
        {
            return aTransferDal.LoadTransferViewForApproval(actionstatus);
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            return aTransferDal.PlaceEmpStatus(aEmpGeneralInfo);
        }
        public void DeleteDataBLL(string TransferId)
        {
            aTransferDal.DeleteData(TransferId);

        }
        public void LoadApprovalControlBLL(RadioButtonList rdl, string pageName, string userName)
        {
            aTransferDal.LoadApprovalControlDAL(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionBLL(string pageName, string userName)
        {
            return aTransferDal.LoadForApprovalConditionDAL(pageName, userName);
        }
    }
}
