﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class UnRestShiftBLL
    {      
        UnRestShiftDAL aUnRestShiftDAL = new UnRestShiftDAL();
        public bool SaveDataForShift(UnRestShift aShift)
        {
            try
            {
                if (!aUnRestShiftDAL.HasShiftName(aShift))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aShift.UnRestShiftId = aClsPrimaryKeyFind.PrimaryKeyMax("UnRestShiftId", "dbo.tblUnrestShift", "HRDB");
                    aUnRestShiftDAL.SaveShift(aShift);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool UpdateDataForShift(UnRestShift aShift)
        {
            return aUnRestShiftDAL.UpdateShift(aShift);
        }

        public DataTable LoadShiftView()
        {
            return aUnRestShiftDAL.LoadShiftView();
        }

        public UnRestShift ShiftEditLoad(string shiftId)
        {
            return aUnRestShiftDAL.ShiftEditLoad(shiftId);
        }
    }
}
