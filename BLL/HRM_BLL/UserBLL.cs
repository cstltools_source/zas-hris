﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using DAO.UA_DAO;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class UserBLL
    {
        UserDAL aUserDal = new UserDAL();
        public bool SaveDataForUser(UserInformation aUserInformation)
        {
            try
            {
                if (!aUserDal.HasUserInformationName(aUserInformation))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();

                    aUserInformation.UserId = aClsPrimaryKeyFind.PrimaryKeyMax("UserId", "tblUser", "HRDB");
                    aUserDal.SaveUserInformation(aUserInformation);
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public DataTable LoadUserUnitById(string userId)
        {
            return aUserDal.LoadUserUnitById(userId);
        }

        public bool DeleteUserUnitInfo(string userId)
        {
            return aUserDal.DeleteUserUnitInfo(userId);
        }

        public bool SaveUserUnit(UserUnitDAO aUserUnitDao)
        {
            return aUserDal.SaveUserUnit(aUserUnitDao);
        }

        public bool HasUserInformationName(UserInformation aUserInformation)
        {
            return aUserDal.HasUserInformationName(aUserInformation);
        }

        public DataTable LoadUserUnit()
        {
            return aUserDal.LoadUserUnit();
        }

        public bool UpdateDataForUser(UserInformation aUserInformation)
        {
            return aUserDal.UpdateUserInfo(aUserInformation);
        }

        public DataTable LoadUserInfo()
        {
            return aUserDal.LoadUserView();
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            return aUserDal.LoadEmpInfo(EmpMasterCode);
        }
        public UserInformation UserEditLoad(string userId)
        {
            return aUserDal.UserInformationEditLoad(userId);
        }

        public bool DeleteUserInfo(UserInformation aUserInformation)
        {
            return aUserDal.DeleteUserInfo(aUserInformation);
        }

    }
}
