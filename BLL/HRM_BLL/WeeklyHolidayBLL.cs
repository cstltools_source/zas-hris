﻿using System;
using System.Data;
using Library.DAL.HRM_DAL;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class WeeklyHolidayBLL
    {
        WeeklyHolidayDAL aWeeklyHolidayDal = new WeeklyHolidayDAL();
        public bool SaveDataForWeeklyHoliday(WeeklyHoliday aWeeklyHoliday)
        {
            try
            {
                if (!aWeeklyHolidayDal.HasHolidayName(aWeeklyHoliday))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aWeeklyHoliday.WeeklyHolidayId = aClsPrimaryKeyFind.PrimaryKeyMax("WeeklyHolidayId", "tblEmpWeeklyHoliday", "HRDB");
                    aWeeklyHolidayDal.SaveWeeklyHoliday(aWeeklyHoliday);
                    return true;    
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }

        public bool UpdateDataForWeeklyHoliday(WeeklyHoliday aWeeklyHoliday)
        {
            return aWeeklyHolidayDal.UpdateWeeklyHolidayInfo(aWeeklyHoliday);
        }
        
        public DataTable LoadWeeklyHolidayView()
        {
            return aWeeklyHolidayDal.LoadWeeklyHolidayView();
        }

        public DataTable LoadEmpMasterCode(string empid)
        {
            return aWeeklyHolidayDal.LoadEmpMasterCode(empid);
        }
        public DataTable LoadEmpId(string EmpMasterCode)
        {
            return aWeeklyHolidayDal.LoadEmpId(EmpMasterCode);
        }

        public WeeklyHoliday WeeklyHolidayEditLoad(string WeeklyHolidayId)
        {
            return aWeeklyHolidayDal.WeeklyHolidayEditLoad(WeeklyHolidayId);
        }
    }
}
