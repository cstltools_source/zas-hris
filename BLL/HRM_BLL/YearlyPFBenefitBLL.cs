﻿ using System;
using System.Collections.Generic;
 using System.Data;
 using System.Linq;
using System.Text;
 using System.Web.UI.WebControls;
 using Library.DAL.HRM_DAL;
 using Library.DAL.InternalCls;
 using Library.DAO.HRM_Entities;

namespace Library.BLL.HRM_BLL
{
    public class YearlyPFBenefitBLL
    {

        ClsCommonOperationDAL aClsCommonOperationDal = new ClsCommonOperationDAL();
        YearlyPFBenefitDAL aYearlyPfBenefitDal = new YearlyPFBenefitDAL();
        public bool SaveDataForYearlyPFBenefit(YearlyPFBenefit aYearlyPfBenefit)
        {
            try
            {
                if (!aYearlyPfBenefitDal.HasYearlyPFBenefit(aYearlyPfBenefit))
                {
                    ClsPrimaryKeyFind aClsPrimaryKeyFind = new ClsPrimaryKeyFind();
                    aYearlyPfBenefit.PFBenefitId = aClsPrimaryKeyFind.PrimaryKeyMax("PFBenefitId", "tblYearlyPFBenefit", "HRDB");
                    aYearlyPfBenefitDal.SaveYearlyPFBenefit(aYearlyPfBenefit);
                    return true;
                }
                else
                {
                    aYearlyPfBenefitDal.UpdateYearlyPFBenefit(aYearlyPfBenefit);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { }
        }
        public void CancelDataMarkBLL(GridView aGridView, DataTable aDataTable)
        {
            aClsCommonOperationDal.CancelDataMark(aGridView, aDataTable);
        }
        public DataTable LoadPFSetup(string year, string percentage, string mainpercent)
        {
            return aYearlyPfBenefitDal.LoadPFSetup(year,percentage,mainpercent);
        }
        public void LoadFinancialYear(DropDownList ddl)
        {
            aYearlyPfBenefitDal.LoadFinancialYear(ddl);
        }
    }
}
