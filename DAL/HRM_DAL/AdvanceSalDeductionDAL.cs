﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class AdvanceSalDeductionDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();

        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveAdvanceSalDeduction(AdvanceSalDeduction aAdvanceSalDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ASDId", aAdvanceSalDeduction.ASDId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aAdvanceSalDeduction.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@DeductionAmount", aAdvanceSalDeduction.DeductionAmount));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aAdvanceSalDeduction.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aAdvanceSalDeduction.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aAdvanceSalDeduction.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aAdvanceSalDeduction.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aAdvanceSalDeduction.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aAdvanceSalDeduction.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aAdvanceSalDeduction.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aAdvanceSalDeduction.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aAdvanceSalDeduction.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aAdvanceSalDeduction.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aAdvanceSalDeduction.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aAdvanceSalDeduction.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aAdvanceSalDeduction.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aAdvanceSalDeduction.IsActive));

            string insertQuery = @"insert into tblAdvanceSalaryDeduction (ASDId,EmpInfoId,DeductionAmount,Purpose,EffectiveDate,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@ASDId,@EmpInfoId,@DeductionAmount,@Purpose,@EffectiveDate,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasAdvanceSalDeduction(AdvanceSalDeduction aAdvanceSalDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aAdvanceSalDeduction.EmpInfoId));
            string query = "select * from tblAdvanceSalaryDeduction where EmpInfoId=@EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadAdvanceSalDeductionView()
        {
            string query = @"SELECT  ASDId,EmpMasterCode ,EmpName , EffectiveDate , DeductionAmount,DesigName,DeptName,tblAdvanceSalaryDeduction.ActionStatus,* FROM tblAdvanceSalaryDeduction 
                 LEFT JOIN tblEmpGeneralInfo ON tblAdvanceSalaryDeduction.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblAdvanceSalaryDeduction.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblAdvanceSalaryDeduction.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblAdvanceSalaryDeduction.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblAdvanceSalaryDeduction.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblAdvanceSalaryDeduction.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblAdvanceSalaryDeduction.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblAdvanceSalaryDeduction.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblAdvanceSalaryDeduction.DeptId = tblDepartment.DeptId where tblAdvanceSalaryDeduction.ActionStatus in ('Posted','Cancel') and tblAdvanceSalaryDeduction.IsActive=1 and tblAdvanceSalaryDeduction.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblAdvanceSalaryDeduction.ASDId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation ORDER BY DesigName ";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "' ORDER BY DeptName ";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo ORDER BY EmployeeName ";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision ORDER BY DivName ";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "' ORDER BY SectionName ";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale ORDER BY SalGradeName ";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' and tblEmpGeneralInfo.IsActive=1 ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        { 
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' and IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteAdvanceSalDeductionDAL(string ASDId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ASDId", ASDId));
            string query = "select * from tblAdvanceSalaryDeduction where ASDId = @ASDId";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, aSqlParameterlist, "HRDB");
        }
        public AdvanceSalDeduction AdvanceSalDeductionEditLoad(string ASDId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ASDId", ASDId));
            string query = "select * from tblAdvanceSalaryDeduction where ASDId = @ASDId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            AdvanceSalDeduction aAdvanceSalDeduction = new AdvanceSalDeduction();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aAdvanceSalDeduction.ASDId = Int32.Parse(dataReader["ASDId"].ToString());
                    aAdvanceSalDeduction.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aAdvanceSalDeduction.DeductionAmount = Convert.ToDecimal(dataReader["DeductionAmount"].ToString());
                    aAdvanceSalDeduction.Purpose = dataReader["Purpose"].ToString();
                    aAdvanceSalDeduction.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aAdvanceSalDeduction.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aAdvanceSalDeduction.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aAdvanceSalDeduction.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aAdvanceSalDeduction.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aAdvanceSalDeduction.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aAdvanceSalDeduction.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aAdvanceSalDeduction.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aAdvanceSalDeduction.ActionStatus = dataReader["ActionStatus"].ToString();
                    aAdvanceSalDeduction.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());

                }
            }
            return aAdvanceSalDeduction;
        }

        public bool UpdateAdvanceSalDeduction(AdvanceSalDeduction aAdvanceSalDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ASDId", aAdvanceSalDeduction.ASDId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aAdvanceSalDeduction.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@DeductionAmount", aAdvanceSalDeduction.DeductionAmount));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aAdvanceSalDeduction.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aAdvanceSalDeduction.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aAdvanceSalDeduction.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aAdvanceSalDeduction.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aAdvanceSalDeduction.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aAdvanceSalDeduction.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aAdvanceSalDeduction.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aAdvanceSalDeduction.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aAdvanceSalDeduction.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aAdvanceSalDeduction.GradeId));

            string query = @"UPDATE tblAdvanceSalaryDeduction SET EmpInfoId=@EmpInfoId,DeductionAmount=@DeductionAmount,Purpose=@DeductionAmount,EffectiveDate=@EffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId WHERE ASDId=@ASDId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(AdvanceSalDeduction aAdvanceSalDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ASDId", aAdvanceSalDeduction.ASDId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aAdvanceSalDeduction.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aAdvanceSalDeduction.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aAdvanceSalDeduction.ApprovedDate));

            string query = @"UPDATE tblAdvanceSalaryDeduction SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE ASDId=@ASDId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadAdvanceSalDeductionViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From tblAdvanceSalaryDeduction
                LEFT JOIN tblEmpGeneralInfo ON tblAdvanceSalaryDeduction.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblAdvanceSalaryDeduction.ActionStatus='" + ActionStatus + "' and tblAdvanceSalaryDeduction.IsActive=1   order by tblAdvanceSalaryDeduction.ASDId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));

            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteAdvanceSalDeduction(string ASDId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblAdvanceSalaryDeduction", "ASDId", ASDId);
            
        }
    }
}
