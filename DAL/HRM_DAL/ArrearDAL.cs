﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ArrearDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
       public string LoadForApprovalConditionDAL(string pageName, string userName)
       {
           return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
       }

        public bool SaveArrear(Arrear aArrear)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ArrearId", aArrear.ArrearId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aArrear.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ArearAmount", aArrear.ArearAmount));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aArrear.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aArrear.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aArrear.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aArrear.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aArrear.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aArrear.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aArrear.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aArrear.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aArrear.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aArrear.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aArrear.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aArrear.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aArrear.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aArrear.IsActive));

            string insertQuery = @"insert into tblArrear (ArrearId,EmpInfoId,ArearAmount,Purpose,EffectiveDate,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@ArrearId,@EmpInfoId,@ArearAmount,@Purpose,@EffectiveDate,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasArrear(Arrear aArrear)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aArrear.EmpInfoId));
            string query = "select * from tblArrear where EmpInfoId=@EmpInfoId and ActionStatus in ('Posted ' , 'Verified')  ";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadArrearView()
        {
            string query = @"SELECT  ArrearId,EmpMasterCode ,EmpName , EffectiveDate ,ArearAmount,DesigName,DeptName,tblArrear.EntryUser,tblArrear.EntryDate,tblArrear.ActionStatus FROM tblArrear 
                 LEFT JOIN tblEmpGeneralInfo ON tblArrear.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblArrear.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblArrear.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblArrear.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblArrear.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblArrear.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblArrear.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblArrear.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblArrear.DeptId = tblDepartment.DeptId where tblArrear.ActionStatus<>'Accepted' and tblArrear.IsActive=1 and tblArrear.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "' order by tblArrear.ArrearId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation ORDER BY DesigName ";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "' ORDER BY DeptName ";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo ORDER BY EmployeeName ";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "' ORDER BY UnitName ";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision ORDER BY DivName ";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'ORDER BY SectionName ";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale ORDER BY SalGradeName ";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType ORDER BY EmpType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,tblEmpGeneralInfo.EmpGradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' and tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' and tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteArrearDAL(string ArrearId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ArrearId", ArrearId));
            string query = "select * from tblArrear where ArrearId = @ArrearId";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, aSqlParameterlist, "HRDB");
        }
        public Arrear ArrearEditLoad(string ArrearId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ArrearId", ArrearId));
            string query = "select * from tblArrear where ArrearId = @ArrearId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Arrear aArrear = new Arrear();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aArrear.ArrearId = Int32.Parse(dataReader["ArrearId"].ToString());
                    aArrear.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aArrear.ArearAmount = Convert.ToDecimal(dataReader["ArearAmount"].ToString());
                    aArrear.Purpose = dataReader["Purpose"].ToString();
                    aArrear.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aArrear.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aArrear.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aArrear.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aArrear.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aArrear.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aArrear.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aArrear.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aArrear.ActionStatus = dataReader["ActionStatus"].ToString();
                    aArrear.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());

                }
            }
            return aArrear;
        }

        public bool UpdateArrear(Arrear aArrear)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ArrearId", aArrear.ArrearId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aArrear.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ArearAmount", aArrear.ArearAmount));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aArrear.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aArrear.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aArrear.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aArrear.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aArrear.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aArrear.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aArrear.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aArrear.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aArrear.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aArrear.GradeId));

            string query = @"UPDATE tblArrear SET EmpInfoId=@EmpInfoId,ArearAmount=@ArearAmount,Purpose=@Purpose,EffectiveDate=@EffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId WHERE ArrearId=@ArrearId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(Arrear aArrear)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ArrearId", aArrear.ArrearId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aArrear.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aArrear.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aArrear.ApprovedDate));

            string query = @"UPDATE tblArrear SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE ArrearId=@ArrearId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
       
        public DataTable LoadArrearViewForApproval(string ActionStatus)
        {
            string query = @"SELECT *,tblDepartment.DeptName,tblDesignation.DesigName From tblArrear
                LEFT JOIN tblEmpGeneralInfo ON tblArrear.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                LEFT JOIN dbo.tblDepartment ON dbo.tblArrear.DeptId=dbo.tblDepartment.DeptId
                LEFT JOIN dbo.tblSection ON dbo.tblArrear.SectionId=dbo.tblSection.SectionId
                LEFT JOIN dbo.tblDesignation ON dbo.tblArrear.DesigId=dbo.tblDesignation.DesigId
                where tblArrear.ActionStatus='" + ActionStatus + "' and tblArrear.IsActive=1  order by tblArrear.ArrearId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));

            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        //public bool DeleteArrear(string ArrearId)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@ArrearId", ArrearId));

        //    string query = @"DELETE FROM dbo.tblArrear WHERE ArrearId=@ArrearId";
        //    return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        //}
        public bool DeleteArrear(string ArrearId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblArrear", "ArrearId", ArrearId);
        }
    }
}
