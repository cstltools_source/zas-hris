﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
   public class AttedanceOperationDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
       public void AttOperationInsertDAL(DateTime attDate,int empId,string AttStatus,string Remarks)
       {
           Attendence attendence = GetAttendance(attDate, empId, AttStatus, Remarks);

           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
           aSqlParameterlist.Add(new SqlParameter("@EmpId", attendence.EmpId));
           aSqlParameterlist.Add(new SqlParameter("@ATTDate", attendence.ATTDate));
           aSqlParameterlist.Add(new SqlParameter("@DayName", attendence.DayName));
           aSqlParameterlist.Add(new SqlParameter("@ShiftId", attendence.ShiftId));
           aSqlParameterlist.Add(new SqlParameter("@ShiftStart", attendence.ShiftStart));
           aSqlParameterlist.Add(new SqlParameter("@ShiftEnd", attendence.ShiftEnd));
           aSqlParameterlist.Add(new SqlParameter("@InTime", attendence.InTime));
           aSqlParameterlist.Add(new SqlParameter("@OutTime", attendence.OutTime));
           aSqlParameterlist.Add(new SqlParameter("@ComOutTime", attendence.ComOutTime));
           aSqlParameterlist.Add(new SqlParameter("@OverTimeDuration",  attendence.OTDuration));
           aSqlParameterlist.Add(new SqlParameter("@ComOverTimeDuration", attendence.ComOTDuration));
           aSqlParameterlist.Add(new SqlParameter("@DutyDuration", attendence.DutyDuration));
           aSqlParameterlist.Add(new SqlParameter("@ComDutyDuration", attendence.ComDutyDuration));
           aSqlParameterlist.Add(new SqlParameter("@ATTStatus", attendence.ATTStatus));
           aSqlParameterlist.Add(new SqlParameter("@Remarks", attendence.Remarks));
           string query = @"INSERT INTO dbo.tblAttendanceRecord ( EmpId ,ATTDate ,DayName ,ShiftId ,ShiftStart ,ShiftEnd ,InTime ,OutTime ,ComOutTime ,OverTimeDuration ,ComOverTimeDuration ,DutyDuration ,ComDutyDuration ,ATTStatus ,Remarks )
           VALUES  ( @EmpId ,@ATTDate ,@DayName ,@ShiftId ,@ShiftStart ,@ShiftEnd ,@InTime ,@OutTime ,@ComOutTime ,@OverTimeDuration ,@ComOverTimeDuration ,@DutyDuration ,@ComDutyDuration ,@ATTStatus ,@Remarks)";
           aCommonInternalDal.SaveDataByInsertCommand(query, aSqlParameterlist, "HRDB");

       }
       public void AttOperationUpdateDAL(DateTime attDate, int empId, string AttStatus, string Remarks)
       {
           Attendence attendence = GetAttendance(attDate, empId, AttStatus, Remarks);

           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
           aSqlParameterlist.Add(new SqlParameter("@EmpId", attendence.EmpId));
           aSqlParameterlist.Add(new SqlParameter("@ATTDate", attendence.ATTDate));
           aSqlParameterlist.Add(new SqlParameter("@DayName", attendence.DayName));
           aSqlParameterlist.Add(new SqlParameter("@ShiftId", attendence.ShiftId));
           aSqlParameterlist.Add(new SqlParameter("@ShiftStart", attendence.ShiftStart));
           aSqlParameterlist.Add(new SqlParameter("@ShiftEnd", attendence.ShiftEnd));
           aSqlParameterlist.Add(new SqlParameter("@InTime", attendence.InTime));
           aSqlParameterlist.Add(new SqlParameter("@OutTime", attendence.OutTime));
           aSqlParameterlist.Add(new SqlParameter("@ComOutTime", attendence.ComOutTime));
           aSqlParameterlist.Add(new SqlParameter("@OverTimeDuration", attendence.OTDuration));
           aSqlParameterlist.Add(new SqlParameter("@ComOverTimeDuration", attendence.ComOTDuration));
           aSqlParameterlist.Add(new SqlParameter("@DutyDuration", attendence.DutyDuration));
           aSqlParameterlist.Add(new SqlParameter("@ComDutyDuration", attendence.ComDutyDuration));
           aSqlParameterlist.Add(new SqlParameter("@ATTStatus", attendence.ATTStatus));
           aSqlParameterlist.Add(new SqlParameter("@Remarks", attendence.Remarks));
           
           string updateQuery = @"update tblAttendanceRecord SET DayName=@DayName,ShiftId=@ShiftId,ShiftStart=@ShiftStart,ShiftEnd=@ShiftEnd,InTime=@InTime,OutTime=@OutTime,
                                  ComOutTime=@ComOutTime,OverTimeDuration=@OverTimeDuration,ComOverTimeDuration=@ComOverTimeDuration,DutyDuration=@DutyDuration,ComDutyDuration=@ComDutyDuration,ATTStatus=@ATTStatus,
                                  Remarks=@Remarks where  ATTDate=@ATTDate and EmpId=@EmpId ";
           aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
       }


       public DataTable CheckAttData(DateTime attDate, int empId)
       {
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
           aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));
           aSqlParameterlist.Add(new SqlParameter("@attDate", attDate));

           string query = @"select * from tblAttendanceRecord where ATTDate=@attDate and EmpId=@EmpInfoId";
           return aCommonInternalDal.DataContainerDataTable(query, aSqlParameterlist, "HRDB");
       }

       private Attendence GetAttendance(DateTime attDate, int EmpInfoId,string AttStatus,string Remarks)
       {
           DataTable dataTableAtt = new DataTable();
           dataTableAtt = LoadEmpInfoCode(EmpInfoId);

           Attendence attendence = new Attendence();
           if (dataTableAtt.Rows.Count>0)
           {
               attendence.EmpId = EmpInfoId;
               attendence.ATTDate = attDate;
               attendence.DayName = GetDayNameByDate(attDate);
               attendence.ShiftId = dataTableAtt.Rows[0]["ShiftId"].ToString();
               attendence.ShiftStart = Convert.ToDateTime(dataTableAtt.Rows[0]["ShiftInTime"].ToString()).TimeOfDay;
               attendence.ShiftEnd = Convert.ToDateTime(dataTableAtt.Rows[0]["ShiftOutTime"].ToString()).TimeOfDay;
               attendence.InTime = Convert.ToDateTime(dataTableAtt.Rows[0]["ShiftInTime"].ToString()).TimeOfDay;
               attendence.OutTime = Convert.ToDateTime(dataTableAtt.Rows[0]["ShiftOutTime"].ToString()).TimeOfDay;
               attendence.ComOutTime = Convert.ToDateTime(dataTableAtt.Rows[0]["ShiftOutTime"].ToString()).TimeOfDay;
               attendence.OTDuration = Convert.ToDateTime(OverTimeCaLCulation(dataTableAtt.Rows[0]["ShiftOutTime"].ToString(),
                   dataTableAtt.Rows[0]["ShiftOutTime"].ToString())).TimeOfDay;
               attendence.ComOTDuration = Convert.ToDateTime(ComOverTimeCaLCulation(dataTableAtt.Rows[0]["ShiftOutTime"].ToString(),
                   dataTableAtt.Rows[0]["ShiftOutTime"].ToString())).TimeOfDay;

               attendence.DutyDuration = Convert.ToDateTime(DutyDuration(dataTableAtt.Rows[0]["ShiftInTime"].ToString(),
                   dataTableAtt.Rows[0]["ShiftOutTime"].ToString())).TimeOfDay;

               attendence.ComDutyDuration = Convert.ToDateTime(DutyDuration(dataTableAtt.Rows[0]["ShiftInTime"].ToString(),
                   dataTableAtt.Rows[0]["ShiftOutTime"].ToString())).TimeOfDay;
               attendence.ATTStatus = AttStatus;
               attendence.Remarks = Remarks;
           }
          
           return attendence;
       }

       private DataTable LoadEmpInfoCode(int EmpInfoId)
       {
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

           aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", EmpInfoId));
           string query = @"SELECT * FROM tblEmpGeneralInfo
                            INNER JOIN dbo.tblShift ON dbo.tblEmpGeneralInfo.ShiftId = dbo.tblShift.ShiftId
                             WHERE EmpInfoId=@EmpInfoId AND tblEmpGeneralInfo.IsActive=1";
           return aCommonInternalDal.DataContainerDataTable(query,aSqlParameterlist, "HRDB");
       }

       public DataTable CheckWeeklyHoliday(int EmpInfoId, DateTime attDate)
       {
           string dayName = GetDayNameByDate(attDate);
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

           aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", EmpInfoId));
           aSqlParameterlist.Add(new SqlParameter("@dayName", dayName));

           string query = @"SELECT * FROM dbo.tblEmpWeeklyHoliday WHERE EmpId=@EmpInfoId AND (FirstHolidayName=@dayName OR SecondHolidayName=@dayName)";
           return aCommonInternalDal.DataContainerDataTable(query, aSqlParameterlist, "HRDB");
       }
       public DataTable LoadAttendanceRecordData(int EmpInfoId, string attDate)
       {
        //   string dayName = GetDayNameByDate(attDate);
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

           aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", EmpInfoId));
           aSqlParameterlist.Add(new SqlParameter("@ATTDate", attDate));

           string query = @"SELECT * FROM dbo.tblAttendanceRecord WHERE EmpId=@EmpInfoId AND ATTDate=@ATTDate";
           return aCommonInternalDal.DataContainerDataTable(query, aSqlParameterlist, "HRDB");
       }
       public DataTable CheckWeeklyHolidayReplace(int EmpInfoId, DateTime attDate)
       {
          
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

           aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", EmpInfoId));
           aSqlParameterlist.Add(new SqlParameter("@attDate", attDate));

           string query = @"SELECT * FROM dbo.tblWeeklyHolidayReplacement WHERE EmpInfoId=@EmpInfoId AND AlternativeDate=@attDate and ActionStatus='Accepted'";
           return aCommonInternalDal.DataContainerDataTable(query, aSqlParameterlist, "HRDB");
       }

       private string GetDayNameByDate(DateTime attDate)
       {
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

           aSqlParameterlist.Add(new SqlParameter("@attDate", attDate));
           string query = @"SELECT DATENAME(dw,@attDate) as WeekDayName";
           return aCommonInternalDal.DataContainerDataTable(query,aSqlParameterlist, "HRDB").Rows[0][0].ToString();
       }

       private string OverTimeCaLCulation(string ShiftOutTime, string OutTime)
       {
           string overTime = "00:00:00";
           
           if (Convert.ToDateTime(ShiftOutTime).TimeOfDay < Convert.ToDateTime(OutTime).TimeOfDay)
           {
               TimeSpan shiftEndTime = Convert.ToDateTime(ShiftOutTime).TimeOfDay;
               TimeSpan shiftOutTime = Convert.ToDateTime(OutTime).TimeOfDay;
               overTime = shiftOutTime.Subtract(shiftEndTime).ToString();
           }
           return overTime;
       }

       private string ComOverTimeCaLCulation(string ShiftOutTime, string OutTime)
       {
           string overTime = "00:00:00";
           
           if (Convert.ToDateTime(ShiftOutTime).TimeOfDay < Convert.ToDateTime(OutTime).TimeOfDay)
           {
               TimeSpan shiftEndTime = Convert.ToDateTime(ShiftOutTime).TimeOfDay;
               TimeSpan shiftOutTime = Convert.ToDateTime(OutTime).TimeOfDay;
               overTime = shiftOutTime.Subtract(shiftEndTime).ToString();
           }
           return overTime;
       }
       private string DutyDuration(string InTime, string OutTime)
       {
           string dutyTime = "00:00:00";

           if (InTime == "00:00:00")
           {
               return dutyTime;
           }
           if (OutTime == "00:00:00")
           {
               return dutyTime;
           }
           TimeSpan shiftEndTime = Convert.ToDateTime(InTime).TimeOfDay;
           TimeSpan shiftOutTime = Convert.ToDateTime(OutTime).TimeOfDay;
           dutyTime = shiftOutTime.Subtract(shiftEndTime).Duration().ToString();


           return dutyTime;
       }
       public bool DeleteArrear(string desigId)
       {
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
           aSqlParameterlist.Add(new SqlParameter("@DesigId", desigId));

           string query = @"DELETE FROM dbo.tblArrear WHERE ArrearId=@ArrearId";
           return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
       }
    }
}
