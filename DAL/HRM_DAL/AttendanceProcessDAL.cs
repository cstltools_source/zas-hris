﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Providers.Entities;
using Library.DAL.InternalCls;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace Library.DAL.HRM_DAL
{
   public class AttendanceProcessDAL
    {
       private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
       public int AttendanceProcess(DateTime fromDateTime, DateTime toDateTime, string empCode, string dataBaseName)
       {
           try
           {
               int count = 0;

               Database db;
               DbCommand dbCommand;
               //Prepare Database Call
               db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
               dbCommand = db.GetStoredProcCommand("AttendanceProcessSPAKAcc");
               db.AddInParameter(dbCommand, "AttFromDate", DbType.DateTime, fromDateTime);
               db.AddInParameter(dbCommand, "AttToDate", DbType.DateTime, toDateTime);
               db.AddInParameter(dbCommand, "SingleEmpMasterCode", DbType.String, empCode);
               db.AddOutParameter(dbCommand, "@CountData", DbType.Int32, 5);

               if (db.ExecuteNonQuery(dbCommand) > 0)
               {
                   count = int.Parse(dbCommand.Parameters["@CountData"].Value.ToString());
                   return count;
               }

               return count;
           }
           catch (Exception exception)
           {
               throw exception;
           }
       }
       public int SalaryProcess(DateTime fromDateTime, DateTime toDateTime, string empCode,string salaryId, string dataBaseName, string processBy)
       {
           try
           {
               int count = 0;

               Database db;
               DbCommand dbCommand;
               //Prepare Database Call
               db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
               dbCommand = db.GetStoredProcCommand("AptechSalaryProcessSP");
               db.AddInParameter(dbCommand, "FromDate", DbType.DateTime, fromDateTime);
               db.AddInParameter(dbCommand, "ToDate", DbType.DateTime, toDateTime);
               db.AddInParameter(dbCommand, "Param", DbType.String, empCode);
               db.AddInParameter(dbCommand, "ProcessBy", DbType.String, processBy);
               db.AddInParameter(dbCommand, "SalaryId", DbType.String, salaryId);
               db.AddOutParameter(dbCommand,"@CountData", DbType.Int32, 5);

               if (db.ExecuteNonQuery(dbCommand) > 0)
               {
                   count = int.Parse(dbCommand.Parameters["@CountData"].Value.ToString());
                   return count;
               }

               return count;
           }
           catch (Exception exception)
           {
               throw exception;
           }
       }
       public DataTable LockStatus(string year,string month)
       {
           string query = @"SELECT * FROM dbo.tblSalaryStartStop WHERE Year='"+year+"' AND Month='"+month+"' AND Status='Locked'";
           return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
       }
//       public DataTable LoadEmployee(string startdt,string enddt)
//       {
//           string query = @"SELECT EmpInfoId,EmpMasterCode,JoiningDate,EmpTypeId,ShiftEmp FROM dbo.tblEmpGeneralInfo WHERE EmployeeStatus='Active' AND tblEmpGeneralInfo.IsActive=1 
//AND  tblEmpGeneralInfo.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='"+HttpContext.Current.Session["UserId"].ToString()+"') AND tblEmpGeneralInfo.EmpMasterCode in (select ProximityID from tblProximityRecord where PRDate between '"+startdt+"' and '"+enddt+"')  order by EmpInfoId asc";
//           return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
//       }
       public DataTable LoadEmployee()
       {
           string query = @"SELECT EmpInfoId,EmpMasterCode,JoiningDate,EmpTypeId,ShiftEmp FROM dbo.tblEmpGeneralInfo WHERE EmployeeStatus='Active' AND tblEmpGeneralInfo.IsActive=1 AND  tblEmpGeneralInfo.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" + HttpContext.Current.Session["UserId"].ToString() + "') order by EmpInfoId asc";
           return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
       }

       public DataTable LoadEmployeeForSalary(string parameter)
       {
           string query = @"SELECT EmpInfoId FROM dbo.tblEmpGeneralInfo S WHERE S.EmployeeStatus='Active' AND S.IsActive=1 and " + parameter + " order by S.EmpInfoId asc";
           return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
       }
       public bool SaveProximityData(string ProximityID, string PRDate, DateTime PRTime)
       {
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
           aSqlParameterlist.Add(new SqlParameter("@ProximityID", ProximityID));
           aSqlParameterlist.Add(new SqlParameter("@PRDate", PRDate));
           aSqlParameterlist.Add(new SqlParameter("@PRTime", PRTime));
           

           string insertQuery = @"insert into tblProximityRecord (ProximityID,PRDate,PRTime,AttnUp)	values (@ProximityID,@PRDate,@PRTime,'true')";

           return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
       }
       public bool DeleteProximityData(string ProximityID,  DateTime PRTime)
       {
           List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
           aSqlParameterlist.Add(new SqlParameter("@ProximityID", ProximityID));
           
           aSqlParameterlist.Add(new SqlParameter("@PRTime", PRTime));


           string insertQuery = @"DELETE FROM tblProximityRecord WHERE ProximityID=@ProximityID and PRDate=@PRTime";

           return aCommonInternalDal.DeleteDataByDeleteCommand(insertQuery, aSqlParameterlist, "HRDB");
       }
       public DataTable CheckEmployee(string USERID,DateTime CHECKTIME)
       {
           string query = @"Select * from tblProximityRecord where ProximityID='" + USERID + "' and PRTime='" + CHECKTIME + "' ";
           return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
       }
    }
}
