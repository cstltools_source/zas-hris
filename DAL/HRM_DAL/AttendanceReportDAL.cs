﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class AttendanceReportDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public DataTable EmployeeAttendanceRecordDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT A.*,E.EmpMasterCode,E.EmpName,E.EmpInfoId,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate,E.JoiningDate,EmployeeImage  FROM dbo.tblAttendanceRecord A" +
                            " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId " +
                            " LEFT JOIN dbo.tblEmpImage EI ON A.EmpId=EI.EmpInfoId " +
                            " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId " +
                            " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId " +
                            " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId " +
                            " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId " +
                            " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId " +
                            " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId " +
                            " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId " +
                            " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                            " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where ATTDate between '" + fromDate + "' and '" + toDate + "'   " + parameter + "   ORDER BY E.EmpInfoId ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable DeptWiseAttendanceSummaryDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT A.ATTDate,DP.DeptName,COUNT(E.EmpMasterCode) AS  Total, 
                            (SUM(CASE WHEN A.ATTStatus IN ('P','OD') THEN 1 ELSE 0 END)) AS Present,
                            (SUM(CASE WHEN A.ATTStatus IN ('L') THEN 1 ELSE 0 END)) AS Late,
                            (SUM(CASE WHEN A.ATTStatus IN ('A') THEN 1 ELSE 0 END)) AS Absent,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate "+
                            " FROM dbo.tblAttendanceRecord A "+
                            " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId "+
                            " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId "+
                            " Where ATTDate between '" + fromDate + "' and '" + toDate + "'   " + parameter + "" +
                            " GROUP BY A.ATTDate,DP.DeptName";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable PresentAllRecordDAL(string fromDate,string toDate,string parameter)
        {
            string query = @"SELECT A.*,E.EmpMasterCode,E.EmpName,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate FROM dbo.tblAttendanceRecord A" +
                            " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId "+
                            " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId "+
                            " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId "+
                            " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId "+
                            " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId "+
                            " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId "+
                            " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId "+
                            " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId "+
                            " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                            " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where ATTDate between '" + fromDate + "' and '" + toDate + "'   " + parameter + "   ORDER BY E.EmpInfoId ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable PresentRecordDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT A.*,E.EmpMasterCode,E.EmpName,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate FROM dbo.tblAttendanceRecord A" +
                            " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId " +
                            " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId " +
                            " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId " +
                            " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId " +
                            " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId " +
                            " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId " +
                            " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId " +
                            " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId " +
                            " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                            " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where A.ATTStatus in ('P','L','OD','H','WH')  " + parameter + "  and ATTDate between '" + fromDate + "' and '" + toDate + "' ORDER BY E.EmpInfoId ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ManualRecordDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT Convert(varchar,ATTDate,103) as ATTDATECHAR,A.*,E.EmpMasterCode,E.EmpName,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate FROM dbo.tblAttendanceRecord A" +
                            " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId " +
                            " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId " +
                            " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId " +
                            " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId " +
                            " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId " +
                            " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId " +
                            " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId " +
                            " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId " +
                            " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                            " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where A.Remarks LIKE '%ME%'  " + parameter + "  and ATTDate between '" + fromDate + "' and '" + toDate + "' ORDER BY ATTDate ASC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AbsentRecordDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT A.*,E.EmpMasterCode,E.EmpName,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate FROM dbo.tblAttendanceRecord A" +
                           " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId " +
                           " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId " +
                           " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId " +
                           " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId " +
                           " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId " +
                           " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId " +
                           " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId " +
                           " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId " +
                           " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                           " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where A.ATTStatus in ('A')  " + parameter + "  and ATTDate between '" + fromDate + "' and '" + toDate + "' ORDER BY E.EmpInfoId ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LateRecordDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT A.*,E.EmpMasterCode,E.EmpName,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate FROM dbo.tblAttendanceRecord A" +
                           " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId " +
                           " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId " +
                           " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId " +
                           " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId " +
                           " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId " +
                           " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId " +
                           " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId " +
                           " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId " +
                           " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                           " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where A.ATTStatus in ('L')  " + parameter + "  and ATTDate between '" + fromDate + "' and '" + toDate + "' ORDER BY E.EmpInfoId ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LeaveRecordDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT A.*,E.EmpMasterCode,E.EmpName,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate FROM dbo.tblAttendanceRecord A" +
                           " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId " +
                           " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId " +
                           " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId " +
                           " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId " +
                           " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId " +
                           " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId " +
                           " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId " +
                           " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId " +
                           " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                           " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where A.ATTStatus in ('LV')  " + parameter + "  and ATTDate between '" + fromDate + "' and '" + toDate + "' ORDER BY E.EmpInfoId ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable OnDutyRecordDAL(string fromDate, string toDate, string parameter)
        {
            string query = @"SELECT A.*,E.EmpMasterCode,E.EmpName,C.CompanyName,(C.ComShortName+':'+CU.UnitName) as UnitName,S.SectionName,D.DivName,DP.DeptName,SH.ShiftName,L.LineName,G.GradeName,DS.DesigName,'" + fromDate + "' as FromDate , '" + toDate + "' as ToDate FROM dbo.tblAttendanceRecord A" +
                           " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId " +
                           " LEFT JOIN dbo.tblCompanyInfo C ON E.CompanyInfoId=C.CompanyInfoId " +
                           " LEFT JOIN dbo.tblCompanyUnit CU ON E.UnitId=CU.UnitId " +
                           " LEFT JOIN dbo.tblSection S ON E.SectionId=S.SectionId " +
                           " LEFT JOIN dbo.tblDivision D ON E.DivisionId=D.DivisionId " +
                           " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId " +
                           " LEFT JOIN dbo.tblShift SH ON E.ShiftId=SH.ShiftId " +
                           " LEFT JOIN dbo.tblLine L ON E.LineId=L.LineId " +
                           " LEFT JOIN dbo.tblEmployeeGrade G ON E.EmpGradeId=G.GradeId " +
                           " LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId  Where A.ATTStatus in ('OD')  " + parameter + "  and ATTDate between '" + fromDate + "' and '" + toDate + "' ORDER BY E.EmpInfoId ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable RptHeader()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel,'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TotalPresent(string empInfoId,DateTime fromdt,DateTime todt)
        {
            string query = @"SELECT SUM(CASE WHEN ATTStatus<>'A' THEN 1 ELSE 0 END) AS TotalPresent FROM dbo.tblAttendanceRecord WHERE EmpId='" + empInfoId + "' AND ATTDate BETWEEN '"+fromdt+"' AND '"+todt+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TotalAbsent(string empInfoId, DateTime fromdt, DateTime todt)
        {
            string query = @"SELECT SUM(CASE WHEN ATTStatus='A' THEN 1 ELSE 0 END) AS TotalAbsent FROM dbo.tblAttendanceRecord WHERE EmpId='" + empInfoId + "' AND ATTDate BETWEEN '" + fromdt + "' AND '" + todt + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpInfo(string parameter)
        {
            string query = @"SELECT E.EmpInfoId,E.EmpMasterCode,E.EmpName,E.JoiningDate,DS.DesigName,DP.DeptName FROM dbo.tblEmpGeneralInfo E LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId
                            LEFT JOIN dbo.tblDesignation DS ON E.DesigId=DS.DesigId WHERE E.EmployeeStatus='Active' AND E.ActionStatus='Accepted' "+parameter+"";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public string GetMonthDays(DateTime fromdt, DateTime todt)
        {
            string query = "SELECT DATEDIFF(dd,'" + fromdt + "','" + todt + "')+1 AS DaysQty";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString();
        }
        public DataTable GetOverTimeDataDAL(string EmpInfoId, DateTime fromdate, DateTime todate)
        {

            string query = @"select CONVERT(TIME(0),OverTimeDuration) AS OverTimeDuration from  tblAttendanceRecord where EmpId='" + EmpInfoId + "'  and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Casual(string EmpInfoId, DateTime fromdate, DateTime todate)
        {

            string query = @"SELECT ISNULL(SUM(AvailLeaveQty),0)AS LeaveDay FROM dbo.tblLeaveAvail
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLeaveAvail.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            WHERE dbo.tblEmpGeneralInfo.EmpInfoId='" +EmpInfoId+"' AND LeaveName='Casual' AND ((FromDate BETWEEN '"+fromdate+"' AND '"+todate+"') OR (ToDate BETWEEN '"+fromdate+"' AND '"+todate+"'))";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Medical(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"SELECT ISNULL(SUM(AvailLeaveQty),0)AS LeaveDay FROM dbo.tblLeaveAvail
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLeaveAvail.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            WHERE dbo.tblEmpGeneralInfo.EmpInfoId='" + EmpInfoId + "' AND LeaveName='Medical' AND ((FromDate BETWEEN '" + fromdate + "' AND '" + todate + "') OR (ToDate BETWEEN '" + fromdate + "' AND '" + todate + "'))";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Earn(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"SELECT ISNULL(SUM(AvailLeaveQty),0)AS LeaveDay FROM dbo.tblLeaveAvail
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLeaveAvail.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            WHERE dbo.tblEmpGeneralInfo.EmpInfoId='" + EmpInfoId + "' AND LeaveName='Earn' AND ((FromDate BETWEEN '" + fromdate + "' AND '" + todate + "') OR (ToDate BETWEEN '" + fromdate + "' AND '" + todate + "'))";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Maternity(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"SELECT ISNULL(SUM(AvailLeaveQty),0)AS LeaveDay FROM dbo.tblLeaveAvail
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLeaveAvail.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            WHERE dbo.tblEmpGeneralInfo.EmpInfoId='" + EmpInfoId + "' AND LeaveName='Maternity' AND ((FromDate BETWEEN '" + fromdate + "' AND '" + todate + "') OR (ToDate BETWEEN '" + fromdate + "' AND '" + todate + "'))";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Other(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"SELECT ISNULL(SUM(AvailLeaveQty),0)AS LeaveDay FROM dbo.tblLeaveAvail
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLeaveAvail.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            WHERE dbo.tblEmpGeneralInfo.EmpInfoId='" + EmpInfoId + "' AND LeaveName='Other' AND ((FromDate BETWEEN '" + fromdate + "' AND '" + todate + "') OR (ToDate BETWEEN '" + fromdate + "' AND '" + todate + "'))";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable TotalHoliday(DateTime fromdate, DateTime todate)
        {
            string query = @"select datediff(day, -3, '"+todate+"')/7-datediff(day, -2, '"+fromdate+"')/7 AS TotalHoliday";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Present(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"SELECT COUNT(*)AS Present FROM dbo.tblAttendanceRecord WHERE EmpId='"+EmpInfoId+"' AND ATTStatus='P' AND ATTDate BETWEEN '"+fromdate+"' AND '"+todate+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable Absent(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"SELECT COUNT(*)AS Absent FROM dbo.tblAttendanceRecord WHERE EmpId='" + EmpInfoId + "'  AND ATTStatus='A' AND ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LatePresent(string EmpInfoId, DateTime fromdate, DateTime todate)
        {

            string query = @"SELECT COUNT(*)AS LatePresent FROM dbo.tblAttendanceRecord WHERE EmpId='" + EmpInfoId + "'  AND ATTStatus='L' AND ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Total(string EmpInfoId, DateTime fromdate, DateTime todate)
        {

            string query = @"SELECT E.EmpInfoId,RIGHT('0' + CAST((sum(datediff(minute, 0, OverTimeDuration)) / 60) % 60 AS VARCHAR),2)  + ':' +RIGHT('0' + CAST(sum(datediff(minute, 0, OverTimeDuration)) % 60 AS VARCHAR),2) as TotalOT,
                            (SUM(CASE WHEN A.ATTStatus IN ('P','OD') THEN 1 ELSE 0 END)) AS TotalPresent,
                            (SUM(CASE WHEN A.ATTStatus IN ('L') THEN 1 ELSE 0 END)) AS TotalLate,
                            (SUM(CASE WHEN A.ATTStatus IN ('LV') THEN 1 ELSE 0 END)) AS TotalLeave,
                            (SUM(CASE WHEN A.ATTStatus IN ('A') THEN 1 ELSE 0 END)) AS TotalAbsent,'" +fromdate+"' as FromDate , '"+todate+"' as ToDate  "+
                             " FROM dbo.tblAttendanceRecord A "+
                             " LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId "+
                             " LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId "+
                             " Where ATTDate between '"+fromdate+"' and '"+todate+"' AND EmpId='"+EmpInfoId+"' "+
                             " GROUP BY E.EmpInfoId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable TotalLate(string EmpInfoId, DateTime fromdate, DateTime todate)
        {

            string query = @"SELECT RIGHT('0' + CAST((SUM(DATEDIFF(minute, ShiftStart , InTime)) / 60) % 60 AS VARCHAR),2)  + ':' +RIGHT('0' + CAST(SUM(DATEDIFF(minute, ShiftStart , InTime)) % 60 AS VARCHAR),2) as TotalLateHr  from dbo.tblAttendanceRecord Where ATTDate between '" + fromdate + "' and '" + todate + "' AND EmpId='" + EmpInfoId + "' AND ATTStatus='L' AND InTime<>'00:00:00'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable TotalOT(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"SELECT E.EmpInfoId,
                          CONVERT(NVARCHAR(5),(SUM(CAST((datediff(hour, 0, OverTimeDuration))AS INT)))) AS OTFirst ,
                         CONVERT(NVARCHAR(5),(SUM(CAST(( datediff(minute, 0, OverTimeDuration)-(datediff(hour, 0, OverTimeDuration)*60)) AS INT)) )) AS  OTFirstMin,
                        (SUM(CAST((datediff(hour, 0, OverTimeDuration))AS INT)))+ ((((SUM(CAST(( datediff(minute, 0, OverTimeDuration)-(datediff(hour, 0, OverTimeDuration)*60)) AS INT)))) -(((SUM(CAST(( datediff(minute, 0, OverTimeDuration)-(datediff(hour, 0, OverTimeDuration)*60)) AS INT))))%60))/60) AS OTSecond
                           ,(((SUM(CAST(( datediff(minute, 0, OverTimeDuration)-(datediff(hour, 0, OverTimeDuration)*60)) AS INT))))%60) AS  OTSecondMin
                   FROM dbo.tblAttendanceRecord A  LEFT JOIN dbo.tblEmpGeneralInfo E ON A.EmpId=E.EmpInfoId 
                    LEFT JOIN dbo.tblDepartment DP ON E.DepId=DP.DeptId  
                    Where ATTDate between '" + fromdate + "' and '" + todate + "' AND EmpId='" + EmpInfoId + "'  GROUP BY  E.EmpInfoId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable EmpImage(string EmpInfoId)
        {

            string query = @"SELECT EmployeeImage FROM dbo.tblEmpGeneralInfo
                             LEFT JOIN dbo.tblEmpImage ON dbo.tblEmpGeneralInfo.EmpInfoId = dbo.tblEmpImage.EmpInfoId WHERE dbo.tblEmpGeneralInfo.EmpMasterCode='"+EmpInfoId+"'  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public string OverTimeHour(string empid, DateTime fromdt, DateTime todt)
        {
            DataTable dataTableAttData = GetOverTimeDataDAL(empid, fromdt, todt);
            int hour = 0;
            int min = 0;
            int totalHour = 0;
            int totalMin = 0;
            if (dataTableAttData.Rows.Count > 0)
            {
                foreach (DataRow row in dataTableAttData.Rows)
                {
                    string fullTime = row[0].ToString().Trim();
                    string[] fullTimeSp = fullTime.Split(':');

                    hour = Convert.ToInt32(fullTimeSp[0]);
                    min = Convert.ToInt32(fullTimeSp[1]);
                    if (hour > 0)
                    {
                        totalHour = totalHour + hour;
                        //if (min > 29 && min < 50)
                        //{
                        //    min = 30;
                        //    totalMin = totalMin + min;
                        //}
                        //else
                        //{
                        if (min >= 50)
                        {
                            min = 60;
                            totalMin = totalMin + min;
                        }
                        //}
                    }
                }

                if (totalHour > 0)
                {
                    if (totalMin > 0)
                    {
                        totalHour = totalHour + (totalMin / 60);
                    }
                    else
                    {
                        totalHour = totalHour;
                    }

                }

            }
            return totalHour.ToString();
        }
        public DataTable MonthlyAttendance(string Date, string EmpCode)
        {
            string query = @"SELECT *,CONVERT(DATETIME,'" + Date + "') AS Date FROM func_Emp_Att ('" + Date + "','" + EmpCode + "') ORDER BY CONVERT(int , dbo.udf_GetNumeric(EmpMasterCode) ) ASC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable TotalStats(string fromdate, string todate)
        {
            string query = @"SELECT EmpMasterCode,SUM((CASE WHEN ATTStatus IN ('P','LV') THEN 1 ELSE 0 END))AS TotalPresent,SUM((CASE WHEN ATTStatus='A' THEN 1 ELSE 0 END)) AS TotalAbsent,SUM((CASE WHEN ATTStatus='L' THEN 1 ELSE 0 END))AS TotalLate FROM dbo.tblAttendanceRecord
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblAttendanceRecord.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId
                            WHERE ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "' GROUP BY EmpMasterCode ORDER BY CONVERT(int , dbo.udf_GetNumeric(EmpMasterCode) ) ASC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable DayName(string fromdate, string todate)
        {
            string query = @"declare @start_date		datetime
                            declare @end_date		datetime
                            declare @days			int " +

                           " select @start_date	= '" + fromdate + "' " +
                           " select @end_date	= '" + todate + "' " +
                           " select @days = datediff(dd,@start_date,@end_date) +1 " +

                           " select" +
                           "    [Date]		= dateadd(dd,number-1,@start_date), " +
                           "    [Day of Week]	= datename(weekday,dateadd(dd,number-1,@start_date)) " +
                           " from" +
                           "    dbo.F_TABLE_NUMBER_RANGE( 1, @days )" +
                           " order by" +
                           "    number";
                            
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
    }
}
