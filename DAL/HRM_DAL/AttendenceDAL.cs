﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class AttendenceDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveAttendence(Attendence attendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@AttendenceId", attendence.AttendenceId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", attendence.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", attendence.ATTDate));
            aSqlParameterlist.Add(new SqlParameter("@DayName", attendence.DayName));
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", attendence.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftStart", attendence.ShiftStart));
            aSqlParameterlist.Add(new SqlParameter("@ShiftEnd", attendence.ShiftEnd));
            aSqlParameterlist.Add(new SqlParameter("@InTime", attendence.InTime));
            aSqlParameterlist.Add(new SqlParameter("@OutTime", attendence.OutTime));
            aSqlParameterlist.Add(new SqlParameter("@OTDuration", attendence.OTDuration));
            aSqlParameterlist.Add(new SqlParameter("@DutyDuration", attendence.DutyDuration));
            aSqlParameterlist.Add(new SqlParameter("@ATTStatus", attendence.ATTStatus));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", attendence.Remarks));

            string insertQuery = @"insert into tblAttendanceRecord (AttendenceId,EmpId,ATTDate,DayName,ShiftId,ShiftStart,ShiftEnd,InTime,OutTime,OTDuration,DutyDuration,ATTStatus,Remarks) 
            values (@AttendenceId,@EmpId,@ATTDate,@DayName,@ShiftId,@ShiftStart,@ShiftEnd,@InTime,@OutTime,@OTDuration,@DutyDuration,@ATTStatus,@Remarks)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public DataTable LoadEmployee(string shiftId,string toDay)
        {
            string query = @"select * from dbo.View_EmpInfo where ShiftId='"+shiftId.Trim()+"' and EmpMasterCode not in (select EmpMasterCode from tblAttendanceRecord where AttDate = '"+toDay.Trim()+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDeaprtment(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment ORDER BY DeptName";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr,"HRDB");
        }

        public void LoadShift(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblShift";
            aInternalDal.LoadDropDownValue(ddl, "ShiftName", "ShiftId", queryStr, "HRDB");
        }

        public DataTable LoadShift(string shiftId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"select * from tblShift where ShiftId='"+shiftId+"' ";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");
            return aDataTableEmpInfo;
        }
    }
}
