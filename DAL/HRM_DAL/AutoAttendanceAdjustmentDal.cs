﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.InternalCls;

namespace DAL.HRM_DAL
{
    public class AutoAttendanceAdjustmentDal
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public DataTable LoadEmployeeInfo(string pram)
        {
            string query = @"SELECT EmpInfoId,EmpMasterCode FROM dbo.tblEmpGeneralInfo " + pram;
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public int SaveAdjustmentInfo(DateTime day, string empid)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@AttendanceDate", day));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", empid));

            return aCommonInternalDal.RunStoreProcedure("sp_AutoAttendenceAdjusment", aSqlParameterlist, "HRDB");
        }
    }
}
