﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class CompanyInfoDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public bool SaveDataForCompanyInfo(CompanyInfo aCompanyInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aCompanyInfo.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyCode", aCompanyInfo.CompanyCode));
            aSqlParameterlist.Add(new SqlParameter("@CompanyName", aCompanyInfo.CompanyName));
            aSqlParameterlist.Add(new SqlParameter("@Address", aCompanyInfo.Address));
            aSqlParameterlist.Add(new SqlParameter("@ContactNo", aCompanyInfo.ContactNo));
            aSqlParameterlist.Add(new SqlParameter("@FaxNo", aCompanyInfo.FaxNo));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", aCompanyInfo.Remarks));

            string insertQuery =
                @"insert into tblCompanyInfo (CompanyInfoId,CompanyCode,CompanyName,Address,ContactNo,FaxNo,Remarks) 
            values (@CompanyInfoId,@CompanyCode,@CompanyName,@Address,@ContactNo,@FaxNo,@Remarks)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasCompanyName(CompanyInfo aCompanyInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@CompanyName", aCompanyInfo.CompanyName));
            string query = "select * from tblCompanyInfo where CompanyName = @CompanyName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadCompanyInfoReport()
        {
            string query = @"SELECT * FROM tblCompanyInfo ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadCompanyInfo()
        {
            string query =
                @"SELECT tblCompanyInfo.CompanyInfoId, tblCompanyInfo.CompanyName,tblCompanyInfo.Address,tblCompanyInfo.ContactNo,tblCompanyInfo.FaxNo,tblCompanyInfo.Remarks FROM tblCompanyInfo  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public CompanyInfo CompanyInfoEditLoad(string companyInfoId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", companyInfoId));
            string query = "select * from tblCompanyInfo where CompanyInfoId = @CompanyInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            CompanyInfo aCompanyInfo = new CompanyInfo();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aCompanyInfo.CompanyInfoId = Int32.Parse(dataReader["CompanyInfoId"].ToString());
                    aCompanyInfo.CompanyCode = dataReader["CompanyCode"].ToString();
                    aCompanyInfo.CompanyName = dataReader["CompanyName"].ToString();
                    aCompanyInfo.ContactNo = dataReader["ContactNo"].ToString();
                    aCompanyInfo.Address = dataReader["Address"].ToString();
                    aCompanyInfo.FaxNo = dataReader["FaxNo"].ToString();
                    aCompanyInfo.Remarks = dataReader["Remarks"].ToString();

                }
            }
            return aCompanyInfo;
        }

        public bool UpdateCompanyInfo(CompanyInfo aCompanyInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aCompanyInfo.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyName", aCompanyInfo.CompanyName));
            aSqlParameterlist.Add(new SqlParameter("@Address", aCompanyInfo.Address));
            aSqlParameterlist.Add(new SqlParameter("@ContactNo", aCompanyInfo.ContactNo));
            aSqlParameterlist.Add(new SqlParameter("@FaxNo", aCompanyInfo.FaxNo));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", aCompanyInfo.Remarks));

            string query =
                @"UPDATE tblCompanyInfo SET CompanyName=@CompanyName,Address=@Address,ContactNo=@ContactNo,FaxNo=@FaxNo,Remarks=@Remarks WHERE CompanyInfoId=@CompanyInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }

        public bool DeleteCompanyInfo(string CompanyInfoId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", CompanyInfoId));

            string query = @"DELETE FROM dbo.tblCompanyInfo WHERE CompanyInfoId=@CompanyInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        
        public void AutoSalaryDAL()
        {
            string query = @"SELECT * from tblEmpGeneralInfo ";
            DataTable dtEmp = aCommonInternalDal.DataContainerDataTable(query, "HRDB");
            DataTable dtEmpSalary;
            foreach (DataRow empRow in dtEmp.Rows)
            {
                dtEmpSalary = new DataTable();
                string query1 = @"SELECT * FROM dbo.tblSalaryInformation WHERE SalHeadName='Gross' AND EmpInfoId='" + empRow["EmpInfoId"] + "'";
                dtEmpSalary = aCommonInternalDal.DataContainerDataTable(query1, "HRDB");
                if (dtEmpSalary.Rows.Count>0)
                {
                    string insertQuery = @"INSERT INTO dbo.tblSalaryRecordPerMonth
        ( EmpInfoId ,
        EmpMasterCode ,
        SalaryStartDate ,
        SalaryEndDate ,
        JoiningDate ,
        CompanyInfoId ,
        UnitId ,
        DivisionId ,
        DepId ,
        SectionId ,
        DesigId ,
        EmpTypeId ,
        EmpGradeId ,
        SalScaleId ,
        MonthDays ,
        PunchDay ,
        TotalHoliday ,
        TotalLeave ,
        WorkingDays ,
        AbsentDays ,
        Absenteeism ,
        Gross ,
        ActualGross ,
        Basic ,
        HouseRent ,
        Medical ,
        MobileAllowance ,
        OtherAllowances ,
        ConveyanceAllowance ,
        Tax ,
        OtherDeduction ,
        SpecialAllowance ,
        OTHours ,
        OTRate ,
        OTAmount ,
        AttnBonusRate ,
        SalaryAdvance ,
        FoodCharge ,
        TiffinAllowance ,
        Arrear ,
        SpecialBonus ,
        HolidayBillAmt ,
        NightBillAmt ,
        LunchAllowance ,
        NetPayable ,
        PayBankorCash ,
        BankId ,
        BankAccNo ,
        SalaryId
        )   " +
" VALUES  ('" + empRow["EmpInfoId"] + "'  ," +
         " '" + empRow["EmpMasterCode"] + "'  ," +
         "  '01-Nov-2014' ," +
         "  '30-Nov-2014' ," +
         "  '" + empRow["JoiningDate"] + "' ," +
         "  '" + empRow["CompanyInfoId"] + "' ," +
         "  '" + empRow["UnitId"] + "' ," +

         "  '" + empRow["DivisionId"] + "' ," +
         "  '" + empRow["DepId"] + "' ," +
        "   '" + empRow["SectionId"] + "' ," +
        "   '" + empRow["DesigId"] + "' ," +
       "    '" + empRow["EmpTypeId"] + "' ," +
        "   '" + empRow["EmpGradeId"] + "' ," +
        "   '" + empRow["SalScaleId"] + "' ," +
        "   '30' ," +
        "   '26' ," +
        "   '4' ," +
         "  '0' ," +
         "  '26' ," +
        "   '0' ," +
        "   '0' ," +
        "   '" + dtEmpSalary.Rows[0]["Amount"].ToString() + "' ," +
        "   '" + dtEmpSalary.Rows[0]["Amount"].ToString() + "' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
         "  '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '0' ," +
        "   '" + dtEmpSalary.Rows[0]["Amount"].ToString() + "' ," +
        "   '" + empRow["PayType"] + "' ," +
        "   '" + empRow["BankId"] + "' ," +
        "   '" + empRow["BankAccNo"] + "' ," +
        "   'SID-001'" +
    "        )";


                    aCommonInternalDal.SaveDataByInsertCommand(insertQuery, "HRDB");
                }
                
            }

        }
    }
}
