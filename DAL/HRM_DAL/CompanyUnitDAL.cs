﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class CompanyUnitDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveCompanyUnit(CompanyUnit aCompanyUnit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aCompanyUnit.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@UnitCode", aCompanyUnit.UnitCode));
            aSqlParameterlist.Add(new SqlParameter("@UnitName", aCompanyUnit.UnitName));
            aSqlParameterlist.Add(new SqlParameter("@UnitAddress", aCompanyUnit.UnitAddress));
            aSqlParameterlist.Add(new SqlParameter("@PhoneNo", aCompanyUnit.PhoneNo));
            aSqlParameterlist.Add(new SqlParameter("@MobileNo", aCompanyUnit.MobileNo));
            aSqlParameterlist.Add(new SqlParameter("@FaxNo", aCompanyUnit.FaxNo));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aCompanyUnit.CompanyInfoId));

            string insertQuery = @"insert into tblCompanyUnit (UnitId,UnitCode,UnitName,UnitAddress,PhoneNo,MobileNo,FaxNo,CompanyInfoId) 
            values (@UnitId,@UnitCode,@UnitName,@UnitAddress,@PhoneNo,@MobileNo,@FaxNo,@CompanyInfoId)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasCompanyUnitName(CompanyUnit aCompanyUnit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@UnitName", aCompanyUnit.UnitName));
            string query = "select * from tblCompanyUnit where UnitName = @UnitName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
               while (dataReader.Read())
                 {
                    return true;
                 }
            }
            return false;
        }
        
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyUnitView()
        {
            string query = @"SELECT tblCompanyUnit.UnitId,tblCompanyUnit.UnitName,tblCompanyUnit.UnitAddress,tblCompanyUnit.PhoneNo,tblCompanyUnit.MobileNo,tblCompanyUnit.FaxNo,tblCompanyInfo.CompanyName FROM tblCompanyUnit " +
                            " LEFT JOIN tblCompanyInfo ON tblCompanyUnit.CompanyInfoId = tblCompanyInfo.CompanyInfoId order by tblCompanyUnit.UnitId desc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public CompanyUnit CompaniUnitEditLoad(string UnitId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@UnitId", UnitId));
            string query = "select * from tblCompanyUnit where UnitId = @UnitId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            CompanyInfo aCompanyInfo = new CompanyInfo();
            CompanyUnit aCompanyUnit = new CompanyUnit();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aCompanyInfo.CompanyInfoId = Int32.Parse(dataReader["CompanyInfoId"].ToString());
                    aCompanyUnit.UnitId = Int32.Parse(dataReader["UnitId"].ToString());
                    aCompanyUnit.UnitCode = dataReader["UnitCode"].ToString();
                    aCompanyUnit.UnitName = dataReader["UnitName"].ToString();
                    aCompanyUnit.UnitAddress = dataReader["UnitAddress"].ToString();
                    aCompanyUnit.PhoneNo = dataReader["PhoneNo"].ToString();
                    aCompanyUnit.MobileNo = dataReader["MobileNo"].ToString();
                    aCompanyUnit.FaxNo = dataReader["FaxNo"].ToString();
                }
            }
            return aCompanyUnit;
        }


        public bool UpdateCompaniUnit(CompanyUnit aCompanyUnit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aCompanyUnit.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@UnitName", aCompanyUnit.UnitName));
            aSqlParameterlist.Add(new SqlParameter("@UnitAddress", aCompanyUnit.UnitAddress));
            aSqlParameterlist.Add(new SqlParameter("@PhoneNo", aCompanyUnit.PhoneNo));
            aSqlParameterlist.Add(new SqlParameter("@MobileNo", aCompanyUnit.MobileNo));
            aSqlParameterlist.Add(new SqlParameter("@FaxNo", aCompanyUnit.FaxNo));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aCompanyUnit.CompanyInfoId));

            string query = @"UPDATE tblCompanyUnit SET UnitName=@UnitName,UnitAddress=@UnitAddress,PhoneNo=@PhoneNo,MobileNo=@MobileNo,FaxNo=@FaxNo,CompanyInfoId=@CompanyInfoId  WHERE UnitId=@UnitId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteUniInfo(string UnitId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@UnitId", UnitId));

            string query = @"DELETE FROM dbo.tblCompanyInfo WHERE UnitId=@UnitId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
