﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.InternalCls;

namespace DAL.HRM_DAL
{
    public class DashBoardDal
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public DataTable GetCompanyUnitInfo()
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable GetEmployeeInfo()
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo WHERE IsActive = 'True' AND EmployeeStatus='Active'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        
        }
        public DataTable GetEmpAtt(string attdate)
        {
            string query = @"SELECT SUM(Late)Late,SUM(Leave)Leave,SUM(Present)Present,SUM(Absent)Absent,SUM(OnDuty)OnDuty FROM (SELECT COUNT(*)Late,'0'Leave,'0'Present,'0'Absent,'0'OnDuty  FROM dbo.tblAttendanceRecord WHERE ATTDate='" + attdate + "' AND ATTStatus='L' " + 
                            " UNION ALL "+
                            " SELECT '0'Late,COUNT(*)Leave,'0'Present,'0'Absent,'0'OnDuty FROM dbo.tblAttendanceRecord WHERE ATTDate='"+attdate+"' AND ATTStatus='LV'"+
                            " UNION ALL "+
                            " SELECT '0'Late,'0'Leave,COUNT(*)Present,'0'Absent,'0'OnDuty FROM dbo.tblAttendanceRecord WHERE ATTDate='" + attdate + "' AND ATTStatus='P' " +
                            " UNION ALL "+
                            " SELECT '0'Late,'0'Leave,'0'Present,COUNT(*)Absent,'0'OnDuty FROM dbo.tblAttendanceRecord WHERE ATTDate='" + attdate + "' AND ATTStatus='A' " + 
                            " UNION ALL "+
                            " SELECT '0'Late,'0'Leave,'0'Present,'0'Absent,COUNT(*)OnDuty FROM dbo.tblAttendanceRecord WHERE ATTDate='" + attdate + "' AND ATTStatus='OD')AS tbltemp";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        


    }
}
