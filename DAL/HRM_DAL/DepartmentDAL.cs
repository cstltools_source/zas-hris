﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class DepartmentDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDepartmentInfo(Department aDepartment)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aDepartment.DepartmentId));
            aSqlParameterlist.Add(new SqlParameter("@DeptCode", aDepartment.DeaprtmentCode));
            aSqlParameterlist.Add(new SqlParameter("@DeptName", aDepartment.DepartmentName));
            aSqlParameterlist.Add(new SqlParameter("@DeptShortName", aDepartment.DeptShortName));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDepartment.DivisionId));
            
            string insertQuery = @"insert into tblDepartment (DeptId,DeptCode,DeptName,DeptShortName,DivisionId) 
            values (@DeptId,@DeptCode,@DeptName,@DeptShortName,@DivisionId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasDeptName(Department aDepartment)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DepartmentName", aDepartment.DepartmentName));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDepartment.DivisionId));
            string query = "select * from tblDepartment where DeptName = @DepartmentName AND DivisionId=@DivisionId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadDepartmentView()
        {
            string query = @"SELECT * FROM tblDepartment 
                            LEFT join tblDivision ON tblDepartment.DivisionId=tblDivision.DivisionId ";             
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public Department DepartmentEditLoad(string departmentId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DepartmentId", departmentId));
            string query = "select * from tblDepartment where DeptId = @DepartmentId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            Department aDepartment = new Department();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aDepartment.DepartmentId = Int32.Parse(dataReader["DeptId"].ToString());
                    aDepartment.DeaprtmentCode = dataReader["DeptCode"].ToString();
                    aDepartment.DepartmentName = dataReader["DeptName"].ToString();
                    aDepartment.DeptShortName = dataReader["DeptShortName"].ToString();
                    aDepartment.DivisionId = Int32.Parse(dataReader["DivisionId"].ToString());
                }
            }
            return aDepartment;
        }

        public bool UpdateDepartmentInfo(Department aDepartment)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aDepartment.DepartmentId));
            aSqlParameterlist.Add(new SqlParameter("@DeptName", aDepartment.DepartmentName));
            aSqlParameterlist.Add(new SqlParameter("@DeptShortName", aDepartment.DeptShortName));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDepartment.DivisionId));

            string query = @"UPDATE tblDepartment SET DeptName=@DeptName,DeptShortName=@DeptShortName,DivisionId=@DivisionId WHERE DeptId=@DeptId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteDepartmentInfo(string DeptId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DeptId", DeptId));

            string query = @"DELETE FROM dbo.tblDepartment WHERE DeptId=@DeptId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
