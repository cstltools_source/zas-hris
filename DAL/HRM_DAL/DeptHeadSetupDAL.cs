﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class DeptHeadSetupDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDeptHeadSetup(DeptHeadSetup aDeptHeadSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DHSId", aDeptHeadSetup.DHSId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aDeptHeadSetup.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aDeptHeadSetup.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aDeptHeadSetup.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aDeptHeadSetup.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aDeptHeadSetup.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDeptHeadSetup.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aDeptHeadSetup.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aDeptHeadSetup.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aDeptHeadSetup.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aDeptHeadSetup.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aDeptHeadSetup.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aDeptHeadSetup.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aDeptHeadSetup.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aDeptHeadSetup.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aDeptHeadSetup.IsActive));

            string insertQuery = @"insert into tblDeptHeadSetup (DHSId,EmpInfoId,Purpose,EffectiveDate,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@DHSId,@EmpInfoId,@Purpose,@EffectiveDate,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasDeptHeadSetup(DeptHeadSetup aDeptHeadSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aDeptHeadSetup.EmpInfoId));
            string query = "select * from tblDeptHeadSetup where EmpInfoId=@EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadDeptHeadSetupView()
        {
            string query = @"SELECT  DHSId,EmpMasterCode ,EmpName , EffectiveDate ,DesigName,DeptName,tblDeptHeadSetup.ActionStatus FROM tblDeptHeadSetup 
                 LEFT JOIN tblEmpGeneralInfo ON tblDeptHeadSetup.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblDeptHeadSetup.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblDeptHeadSetup.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblDeptHeadSetup.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblDeptHeadSetup.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblDeptHeadSetup.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblDeptHeadSetup.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblDeptHeadSetup.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblDeptHeadSetup.DeptId = tblDepartment.DeptId where tblDeptHeadSetup.ActionStatus in ('Posted','Cancel') and tblDeptHeadSetup.IsActive=1 and tblDeptHeadSetup.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblDeptHeadSetup.DHSId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,tblEmpGeneralInfo.EmpGradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' and  tblEmpGeneralInfo.ActionStatus='Accepted' and tblEmpGeneralInfo.EmployeeStatus='Active' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteDeptHeadSetupDAL(string DHSId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DHSId", DHSId));
            string query = "select * from tblDeptHeadSetup where DHSId = @DHSId";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, aSqlParameterlist, "HRDB");
        }
        public DeptHeadSetup DeptHeadSetupEditLoad(string DHSId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DHSId", DHSId));
            string query = "select * from tblDeptHeadSetup where DHSId = @DHSId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            DeptHeadSetup aDeptHeadSetup = new DeptHeadSetup();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aDeptHeadSetup.DHSId = Int32.Parse(dataReader["DHSId"].ToString());
                    aDeptHeadSetup.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aDeptHeadSetup.Purpose = dataReader["Purpose"].ToString();
                    aDeptHeadSetup.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aDeptHeadSetup.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aDeptHeadSetup.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aDeptHeadSetup.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aDeptHeadSetup.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aDeptHeadSetup.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aDeptHeadSetup.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aDeptHeadSetup.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aDeptHeadSetup.ActionStatus = dataReader["ActionStatus"].ToString();
                    aDeptHeadSetup.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());

                }
            }
            return aDeptHeadSetup;
        }

        public bool UpdateDeptHeadSetup(DeptHeadSetup aDeptHeadSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DHSId", aDeptHeadSetup.DHSId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aDeptHeadSetup.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aDeptHeadSetup.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aDeptHeadSetup.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aDeptHeadSetup.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aDeptHeadSetup.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDeptHeadSetup.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aDeptHeadSetup.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aDeptHeadSetup.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aDeptHeadSetup.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aDeptHeadSetup.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aDeptHeadSetup.GradeId));

            string query = @"UPDATE tblDeptHeadSetup SET EmpInfoId=@EmpInfoId,Purpose=@Purpose,EffectiveDate=@EffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId WHERE DHSId=@DHSId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(DeptHeadSetup aDeptHeadSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DHSId", aDeptHeadSetup.DHSId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aDeptHeadSetup.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aDeptHeadSetup.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aDeptHeadSetup.ApprovedDate));

            string query = @"UPDATE tblDeptHeadSetup SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE DHSId=@DHSId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadDeptHeadSetupViewForApproval()
        {
            string query = @"SELECT * From tblDeptHeadSetup
                LEFT JOIN tblEmpGeneralInfo ON tblDeptHeadSetup.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblDeptHeadSetup.ActionStatus='Posted'   order by tblDeptHeadSetup.DHSId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));

            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteDeptHeadSetup(string DHSId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblDeptHeadSetup", "DHSId", DHSId);
        }
    }
}
