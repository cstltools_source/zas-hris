﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class DesignationDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDesignationInfo(Designation aDesignation)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aDesignation.DesignationId));
            aSqlParameterlist.Add(new SqlParameter("@DesigCode", aDesignation.DesignaitonCode));
            aSqlParameterlist.Add(new SqlParameter("@DesigName", aDesignation.DesignationName));
            aSqlParameterlist.Add(new SqlParameter("@HDutyBill", aDesignation.HDutyBill));
            aSqlParameterlist.Add(new SqlParameter("@NightBill", aDesignation.NightBill));
            aSqlParameterlist.Add(new SqlParameter("@OTRate", aDesignation.OTRate));
            aSqlParameterlist.Add(new SqlParameter("@TiffinAllow", aDesignation.TiffinAllow));

            string insertQuery = @"insert into tblDesignation (DesigId,DesigCode,DesigName,HDutyBill,NightBill,OTRate,TiffinAllow) 
            values (@DesigId,@DesigCode,@DesigName,@HDutyBill,@NightBill,@OTRate,@TiffinAllow)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasDesigName(Designation aDesignation)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DesignationName", aDesignation.DesignationName));
            string query = "select * from tblDesignation where DesigName = @DesignationName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadDesignationView()
        {
            string query = @"SELECT * FROM tblDesignation ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public Designation DesignationEditLoad(string DesigId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DesignationId", DesigId));
            string query = "select * from tblDesignation where DesigId = @DesignationId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            Designation aDesignation = new Designation();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aDesignation.DesignationId = Int32.Parse(dataReader["DesigId"].ToString());
                    aDesignation.DesignaitonCode = dataReader["DesigCode"].ToString();
                    aDesignation.DesignationName = dataReader["DesigName"].ToString();
                    aDesignation.HDutyBill = Convert.ToDecimal(dataReader["HDutyBill"].ToString());
                    aDesignation.NightBill = Convert.ToDecimal(dataReader["NightBill"].ToString());
                    aDesignation.OTRate = Convert.ToDecimal(dataReader["OTRate"].ToString());
                    aDesignation.TiffinAllow = Convert.ToDecimal(dataReader["TiffinAllow"].ToString());
                }
            }
            return aDesignation;
        }
        public DataTable RptHeader()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel,'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool UpdateDesignationInfo(Designation aDesignation)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aDesignation.DesignationId));
            aSqlParameterlist.Add(new SqlParameter("@DesigName", aDesignation.DesignationName));
            aSqlParameterlist.Add(new SqlParameter("@HDutyBill", aDesignation.HDutyBill));
            aSqlParameterlist.Add(new SqlParameter("@NightBill", aDesignation.NightBill));
            aSqlParameterlist.Add(new SqlParameter("@OTRate", aDesignation.OTRate));
            aSqlParameterlist.Add(new SqlParameter("@TiffinAllow", aDesignation.TiffinAllow));

            string query = @"UPDATE tblDesignation SET DesigName=@DesigName,HDutyBill=@HDutyBill,NightBill=@NightBill,OTRate=@OTRate,TiffinAllow=@TiffinAllow  WHERE DesigId=@DesigId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteDesignationInfo(string desigId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DesigId", desigId));
            
            string query = @"DELETE FROM dbo.tblDesignation WHERE DesigId=@DesigId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
