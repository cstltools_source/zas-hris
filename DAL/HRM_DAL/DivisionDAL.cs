﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class DivisionDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDevision(Division aDivision)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDivision.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DivCode", aDivision.DivCode));
            aSqlParameterlist.Add(new SqlParameter("@DivName", aDivision.DivName));
            aSqlParameterlist.Add(new SqlParameter("@DivShortName", aDivision.DivShortName));
            
            string insertQuery = @"insert into tblDivision (DivisionId,DivCode,DivName,DivShortName) 
            values (@DivisionId,@DivCode,@DivName,@DivShortName)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasDivisionName(Division aDivision)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivName", aDivision.DivName));
            string query = "select * from tblDivision where DivName = @DivName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadDivisionView()
        {
            string query =
                @"SELECT * from tblDivision";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Division DivisionEditLoad(string DivisionId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", DivisionId));
            string query = "select * from tblDivision where DivisionId = @DivisionId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            Division aDivision = new Division();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aDivision.DivisionId = Int32.Parse(dataReader["DivisionId"].ToString());
                    aDivision.DivCode = dataReader["DivCode"].ToString();
                    aDivision.DivName = dataReader["DivName"].ToString();
                    aDivision.DivShortName = dataReader["DivShortName"].ToString();
                }
            }
            return aDivision;
        }

        public bool UpdateDivision(Division aDivision)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDivision.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DivName", aDivision.DivName));
            aSqlParameterlist.Add(new SqlParameter("@DivShortName", aDivision.DivShortName));

            string query = @"UPDATE tblDivision SET DivName=@DivName,DivShortName=@DivShortName WHERE DivisionId=@DivisionId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteDivisionInfo(string DivisionId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", DivisionId));

            string query = @"DELETE FROM dbo.tblDivision WHERE DivisionId=@DivisionId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
