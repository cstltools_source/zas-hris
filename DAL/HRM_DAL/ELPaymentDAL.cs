﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Web;
using Library.DAL.InternalCls;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Library.DAL.HRM_DAL
{
    public class ELPaymentDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public int ELPaymentProcess(string Year, int empcategoryId, string executeby, string dataBaseName)
        {
            try
            {
                int count = 0;

                Database db;
                DbCommand dbCommand;
                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetStoredProcCommand("sp_ELPaymentDataProcess");
                //db.AddInParameter(dbCommand, "FestivalId", DbType.Int32, fastivalId);
                db.AddInParameter(dbCommand, "ELYear", DbType.String, Year);
                db.AddInParameter(dbCommand, "EmpCategoryId", DbType.Int32, empcategoryId);
                db.AddInParameter(dbCommand, "ProcessExecutedBy", DbType.String, executeby);
                //db.AddOutParameter(dbCommand, "ProcessExecutedTime", DbType.DateTime, executeTime);
                db.AddInParameter(dbCommand, "ProcessExecutedTime", DbType.DateTime, DateTime.Now);

                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    //count = int.Parse(dbCommand.Parameters["@CountData"].Value.ToString());
                    count = 0;
                    return count;
                }

                return count;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public DataTable ELPaymentReport(string year, string parameter)
        {
            string query = @"SELECT (ELAmount-10)ELAmount, *,'" + HttpContext.Current.Session["LoginName"].ToString() + "' as LoginName FROM dbo.tblELPaymentProcessData " +
                            " LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblELPaymentProcessData.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId"+
                            " LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId"+
                            " LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId" +
                            " LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId WHERE Year='"+year+"'  "+parameter+" ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
