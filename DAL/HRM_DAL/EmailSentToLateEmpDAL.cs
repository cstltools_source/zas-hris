﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class EmailSentToLateEmpDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public DataTable LoadEmployee(string division, string department, string section)
        {
            string query = @"SELECT EmpInfoId AS EmpId, EmpMasterCode, EmpName ,Email FROM dbo.tblEmpGeneralInfo WHERE IsActive='True' AND  DivisionId='" + division + "'AND DepId= '" + department + "' AND SectionId='" + section + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmployeeATT(DateTime date)
        {
            string query = @"SELECT EmpId, ATTDate,CONVERT(VARCHAR(5),InTime,108)as Intime, CONVERT(VARCHAR(5),ShiftStart,108) as ShiftStart  ,
(RIGHT('0' + CAST(DATEDIFF(S, ShiftStart, InTime)        / 3600 AS VARCHAR(2)), 2) + ':'
    + RIGHT('0' + CAST(DATEDIFF(S, ShiftStart, InTime) % 3600 /   60 AS VARCHAR(2)), 2) + ':'
    + RIGHT('0' + CAST(DATEDIFF(S, ShiftStart, InTime) %   60        AS VARCHAR(2)), 2))LateTime
FROM dbo.tblAttendanceRecord where ATTStatus='L' AND ATTDate='" + date + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmployeeATTAllDAL(DateTime date)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpMasterCode as EmployeeCode,EmpName as EmployeeName,Email, EmpId, ATTDate,CONVERT(VARCHAR(5),InTime,108)as Intime, CONVERT(VARCHAR(5),ShiftStart,108) as ShiftStart ,datediff( minute, ShiftStart,InTime) as LateTime 
                             FROM dbo.tblAttendanceRecord 
                             LEFT JOIN tblEmpGeneralInfo ON dbo.tblAttendanceRecord.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId
                             where ATTStatus='L' AND ATTDate='" + date + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
