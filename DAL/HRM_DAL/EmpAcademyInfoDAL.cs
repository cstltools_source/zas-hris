﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;


namespace Library.DAL.HRM_DAL
{
    public class EmpAcademyInfoDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForAcademicInfo(AreaOfStaudy aAreaOfStaudy)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@StudyId", aAreaOfStaudy.StudyId));
            aSqlParameterlist.Add(new SqlParameter("@AreaofStudy", aAreaOfStaudy.AreaofStudy));

            string insertQuery = @"insert into tblAreaofStudy (StudyId,AreaofStudy) 
                                                           values(@StudyId,@AreaofStudy)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool SaveEmpEduInstitute(EmpEduInstitute aEmpEduInstitute)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EduInstituteId", aEmpEduInstitute.EduInstituteId));
            aSqlParameterlist.Add(new SqlParameter("@EduInstituteName", aEmpEduInstitute.EduInstituteName));

            string insertQuery = @"insert into tblEduInstitute (EduInstituteId,EduInstituteName) 
                                    values(@EduInstituteId,@EduInstituteName)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool SaveDataForEmpExamp(EmpExam aEmpExam)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ExamId", aEmpExam.ExamId));
            aSqlParameterlist.Add(new SqlParameter("@ExamName", aEmpExam.ExamName));

            string insertQuery = @"insert into tblExam (ExamId,ExamName) 
                                                         values(@ExamId,@ExamName)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool SaveDataForEmpQualification(EmpQualification aQualification)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@QualificationId", aQualification.QualificationId));
            aSqlParameterlist.Add(new SqlParameter("@Qualification", aQualification.Qualification));

            string insertQuery = @"insert into tblQualification (QualificationId,Qualification) 
                                    values(@QualificationId,@Qualification)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        
        public DataTable LoadEmployee(string EmpMasterCode)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo where dbo.tblEmpGeneralInfo.EmpMasterCode='" + EmpMasterCode + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public void LoadBoardName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from dbo.tblEduInstitute ";
            aInternalDal.LoadDropDownValue(ddl, "EduInstituteName", "EduInstituteId", queryStr, "HRDB");
        }

        public void LoadQualificationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from dbo.tblQualification ";
            aInternalDal.LoadDropDownValue(ddl, "Qualification", "QualificationId", queryStr, "HRDB");
        }

        public void LoadExam(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from dbo.tblExam ";
            aInternalDal.LoadDropDownValue(ddl, "ExamName", "ExamId", queryStr, "HRDB");
        }

        public void LoadAreaStudy(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from dbo.tblAreaofStudy ";
            aInternalDal.LoadDropDownValue(ddl, "AreaofStudy", "StudyId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        
        public EmpGeneralInfo EmpInfoEditLoad(string employeeId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", employeeId));
            string query = "select * from tblEmpGeneralInfo where EmpInfoId = @EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            EmpGeneralInfo aEmpGeneralInfo = new EmpGeneralInfo();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aEmpGeneralInfo.EmpInfoId = Int32.Parse(dataReader["EmpInfoId"].ToString());
                    aEmpGeneralInfo.EmpMasterCode = dataReader["EmpMasterCode"].ToString();
                    aEmpGeneralInfo.ShiftEmployee = dataReader["ShiftEmp"].ToString();
                    aEmpGeneralInfo.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                }
            }
            return aEmpGeneralInfo;
        }
        public void EmpApprovalDAL(string EmpInfoId, string joiningDate, string AppUser, DateTime appDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", "Accepted"));
            aSqlParameterlist.Add(new SqlParameter("@AppDate", appDate));
            string query = @"UPDATE tblEmpGeneralInfo SET ActionStatus=@ActionStatus,ApprovalDate=@AppDate where  EmpInfoId=@EmpInfoId";
            aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
            
        }
        public bool UpdateEmployeeInfo(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", aEmpGeneralInfo.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aEmpGeneralInfo.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", aEmpGeneralInfo.Remarks));
            aSqlParameterlist.Add(new SqlParameter("@ShiftEmp", aEmpGeneralInfo.ShiftEmployee));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aEmpGeneralInfo.ActionStatus));

            string query = @"UPDATE tblEmpGeneralInfo SET EmpName=@EmpName,ShortName=@ShortName, WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        
        public List<EmpGeneralInfo> ViewAllEmployee()
        {
            List<EmpGeneralInfo> allEmpGeneralInfoList = new List<EmpGeneralInfo>();
            string query = @"select * from tblEmpGeneralInfo";

            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, "HRDB");

            while (dataReader.Read())
            {
                EmpGeneralInfo aGeneralInfo = new EmpGeneralInfo();
                aGeneralInfo.EmpInfoId = Int32.Parse(dataReader["EmpInfoId"].ToString());
                aGeneralInfo.EmpMasterCode = (dataReader["EmpMasterCode"].ToString());
                aGeneralInfo.EmpName = dataReader["EmpName"].ToString();
                allEmpGeneralInfoList.Add(aGeneralInfo);
            }

            return allEmpGeneralInfoList;
        }
        
        public bool DeleteEmpInfo(string EmpInfoId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", EmpInfoId));

            string query = @"DELETE FROM dbo.tblEmpGeneralInfo WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
