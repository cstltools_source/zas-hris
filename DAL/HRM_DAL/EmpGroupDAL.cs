﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class EmpGroupDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDevision(EmpGroup aEmpGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aEmpGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@GroupName", aEmpGroup.GroupName));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aEmpGroup.UnitId));

            string insertQuery = @"insert into tblGroup (GroupId,GroupName,UnitId) 
            values (@GroupId,@GroupName,@UnitId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasEmpGroupName(EmpGroup aEmpGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupName", aEmpGroup.GroupName));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aEmpGroup.UnitId));
            string query = "select * from tblGroup where GroupName = @GroupName AND UnitId=@UnitId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadEmpGroupView()
        {
            string query =
                @"select * from tblGroup
                left join tblCompanyUnit on tblCompanyUnit.UnitId=tblGroup.UnitId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public EmpGroup EmpGroupEditLoad(string GroupId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", GroupId));
            string query = "select * from tblGroup where GroupId = @GroupId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            EmpGroup aEmpGroup = new EmpGroup();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aEmpGroup.GroupId = Int32.Parse(dataReader["GroupId"].ToString());
                    aEmpGroup.GroupName = dataReader["GroupName"].ToString();
                    aEmpGroup.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                }
            }
            return aEmpGroup;
        }

        public bool UpdateEmpGroup(EmpGroup aEmpGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aEmpGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@GroupName", aEmpGroup.GroupName));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aEmpGroup.UnitId));

            string query = @"UPDATE tblGroup SET GroupName=@GroupName,UnitId=@UnitId WHERE GroupId=@GroupId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteEmpGroupInfo(string GroupId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", GroupId));

            string query = @"DELETE FROM dbo.tblGroup WHERE GroupId=@GroupId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
