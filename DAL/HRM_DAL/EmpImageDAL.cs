﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class EmpImageDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public bool SaveEmpImage(byte[] imagebyt, byte[] sigimagebyt,int empid)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmployeeImage", imagebyt));
            aSqlParameterlist.Add(new SqlParameter("@SigImage", sigimagebyt));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empid));

            string insertQuery = @"insert into dbo.tblEmpImage (EmployeeImage,SigImage,EmpInfoId)values(@EmployeeImage,@SigImage,@EmpInfoId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool DeleteData(int empid)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empid));

            string insertQuery = @"DELETE FROM dbo.tblEmpImage WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
    }
}
