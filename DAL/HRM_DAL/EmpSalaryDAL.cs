﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;
using Microsoft.SqlServer.Server;

namespace Library.DAL.HRM_DAL
{
    public class EmpSalaryDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public void LoadSalaryRuleToDropDown(DropDownList aDropDownList, string strSalScaleId)
        {
            string query = @"SELECT DISTINCT RuleName,RuleName AS NameRule FROM dbo.tblSalaryRules where SalScaleId='" + strSalScaleId + "'";
            aCommonInternalDal.LoadDropDownValue(aDropDownList, "RuleName", "NameRule", query, "HRDB");
        }

        public void UpdateSalaryHistoryStatus(string empId, DateTime inactiveDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@InactiveDate", inactiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));

            string updateQuery = @"UPDATE tblEmpSalaryHistory SET InactiveDate=@InactiveDate, Status='Inactive' WHERE EmpInfoId=@EmpInfoId and Status='active'";
            aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }
        public void UpdateSalaryDetailStatus(string empId, DateTime inactiveDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@InactiveDate", inactiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));

            string updateQuery = @"UPDATE tblSalaryInformation SET InactiveDate=@InactiveDate, Status='Inactive' WHERE EmpInfoId=@EmpInfoId and Status='active'";
            aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }

        public DataTable SalaryDetailCheck(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * from tblEmpSalary where EmpInfoId = '" + empId.Trim() + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public bool HasEmployeeSalary(EmployeeSalary aEmployeeSalary)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmployeeSalary.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Year", aEmployeeSalary.ActiveDate.Year));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aEmployeeSalary.SalaryHeadId));
            string query = "select * from dbo.tblSalaryInformation where EmpInfoId=@EmpInfoId AND YEAR(ActiveDate)=@Year AND SalaryHeadId=@SalaryHeadId AND IsActive=1";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable SalaryHistoryCheck(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * from tblEmpSalaryHistory where EmpInfoId = '" + empId.Trim() + "' and Status='active'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }

        public DataTable StartingSalaryCheck(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * from tblSalaryInformation where EmpInfoId = '" + empId.Trim() + "' AND  IsActive=1";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");
            return aDataTableEmpInfo;
        }

        public DataTable EmpInformationDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT EG.EmpInfoId,EG.EmpMasterCode,EG.EmpName,EG.DesigId,DS.DesigName,EG.DepId,DE.DeptName,EG.SectionId, 
                             SE.SectionName , G.GradeId,G.GradeType,EG.SalScaleId 
                             FROM dbo.tblEmpGeneralInfo EG 
                             LEFT JOIN dbo.tblDesignation DS ON EG.DesigId=DS.DesigId 
                             LEFT JOIN dbo.tblDepartment DE ON EG.DepId=DE.DeptId 
                             LEFT JOIN dbo.tblSection SE ON EG.SectionId=SE.SectionId 
                             LEFT JOIN dbo.tblEmployeeGrade G ON EG.EmpGradeId=G.GradeId 
                             WHERE EG.EmpMasterCode='" + empId + "' and EG.EmployeeStatus='Inactive' and EG.ActionStatus in ('Posted','Cancel') " +
                           "and EG.IsActive=1";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }

        //public bool UpdateEmpInfoForPromotion(EmployeeSalaryHistory aSalaryHistory)
        //{
        //    string updateQuery = @"UPDATE dbo.tblEmpGeneralInfo SET   WHERE EmpInfoId='" + aSalaryHistory.EmpInfoId + "'";
        //    return aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, "HRDB");
        //}


        public DataTable EmpSalaryDetailDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT ES.SalaryHeadId,SH.SalaryName,ES.Amount FROM tblSalaryInformation ES  " +
                            " LEFT JOIN dbo.tblSalaryHead SH ON ES.SalaryHeadId = SH.SalaryHeadId  " +
                            " WHERE ES.Status='active' AND ES.EmpInfoId='" + empId.Trim() + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public DataTable SalaryRule(string ruleName)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * FROM dbo.tblSalaryRules WHERE RuleName='" + ruleName + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public DataTable EmpInformationForPromotionDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT EG.EmpInfoId,EG.EmpMasterCode,EG.EmpName,EG.DesignationId,DS.DesigName,EG.DepartmentId,DE.DeptName,EG.SectionId, " +
                            " SE.SectionName ,EC.EmpCategoryId,EC.EmpCategoryName, G.GradeId,G.GradeType ,Convert(nvarchar(11),ESH.ActiveDate,106) as ActiveDate,ESH.Purpose,ESH.TotalSalary " +
                            " FROM dbo.tblEmpGeneralInfo EG " +
                            " LEFT JOIN tblEmpSalaryHistory ESH ON EG.EmpInfoId=ESH.EmpInfoId " +
                            " LEFT JOIN dbo.tblDesignation DS ON EG.DesignationId=DS.DesigId " +
                            " LEFT JOIN dbo.tblDepartment DE ON EG.DepartmentId=DE.DeptId " +
                            " LEFT JOIN dbo.tblSection SE ON EG.SectionId=SE.SectionId " +
                            " LEFT JOIN dbo.tblEmpCategory EC ON EG.EmpCategoryId=EC.EmpCategoryId " +
                            " LEFT JOIN dbo.tblGrade G ON EG.EmpGradeId=G.GradeId " +
                            " WHERE EG.EmpMasterCode='" + empId.Trim() + "' AND ESH.Status='active' ";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }

        public bool SalaryDetailSave(EmployeeSalary aEmployeeSalary)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalaryInfoId", aEmployeeSalary.SalaryInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmployeeSalary.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aEmployeeSalary.SalaryHeadId));
            aSqlParameterlist.Add(new SqlParameter("@SalHeadName", aEmployeeSalary.SalaryHead));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aEmployeeSalary.Amount));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aEmployeeSalary.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aEmployeeSalary.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aEmployeeSalary.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aEmployeeSalary.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@Modified", aEmployeeSalary.Modified));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aEmployeeSalary.IsActive));


            string insertQuery = @"insert into tblSalaryInformation (SalaryInfoId,EmpInfoId,SalaryHeadId,SalHeadName,Amount,ActiveDate,EntryUser,EntryDate,ActionStatus,Modified,IsActive) 
            values (@SalaryInfoId,@EmpInfoId,@SalaryHeadId,@SalHeadName,@Amount,@ActiveDate,@EntryUser,@EntryDate,@ActionStatus,@Modified,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string EmpInfoId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblSalaryInformation", "EmpInfoId", EmpInfoId);

        }

        public DataTable LoadEmployeeViewDeptWise()
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo 
             LEFT JOIN tblDesignation ON tblEmpGeneralInfo.DesigId = tblDesignation.DesigId 
             LEFT JOIN tblDepartment ON tblEmpGeneralInfo.DepId = tblDepartment.DeptId 
             LEFT JOIN tblSection ON tblEmpGeneralInfo.SectionId = tblSection.SectionId 
             LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId=dbo.tblEmployeeGrade.GradeId
             LEFT JOIN dbo.tblSalaryGradeOrScale ON dbo.tblEmpGeneralInfo.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
             LEFT JOIN tblEmployeeType ON tblEmpGeneralInfo.EmpTypeId = tblEmployeeType.EmpTypeId 
             INNER JOIN
             (SELECT EmpInfoId,Amount FROM dbo.tblSalaryInformation WHERE ActionStatus='Posted' AND SalHeadName='Gross' AND  IsActive=1) AS tblSal
             ON tblEmpGeneralInfo.EmpInfoId=tblSal.EmpInfoId
             WHERE tblEmpGeneralInfo.ActionStatus in ('Posted','Cancel') and tblEmpGeneralInfo.IsActive=1 and tblEmpGeneralInfo.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblEmpGeneralInfo.EmpInfoId desc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadSalaryView(string empinfoId)
        {
            string query = @"SELECT * FROM dbo.tblSalaryInformation WHERE EmpInfoId='" + empinfoId + "' AND IsActive=1 AND ActionStatus='Posted'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpView(string empinfoId)
        {
            string query = @"SELECT EmpMasterCode,EmpName,DeptName,DesigName FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                            WHERE EmpInfoId='" + empinfoId + "' AND tblEmpGeneralInfo.IsActive=1 AND tblEmpGeneralInfo.ActionStatus='Posted'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

    }
}
