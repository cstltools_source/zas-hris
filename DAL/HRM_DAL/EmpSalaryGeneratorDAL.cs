﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class EmpSalaryGeneratorDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForEmpSalaryGeneartor(EmpSalaryGenerator aEmpSalaryGenerator)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            //aSqlParameterlist.Add(new SqlParameter("@SalGeneratId", aEmpSalaryGenerator.SalGeneratId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpSalaryGenerator.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryId", aEmpSalaryGenerator.SalaryId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryStartDate", aEmpSalaryGenerator.SalaryStartDate));
            aSqlParameterlist.Add(new SqlParameter("@SalaryEndDate", aEmpSalaryGenerator.SalaryEndDate));
            aSqlParameterlist.Add(new SqlParameter("@JoiningDate", aEmpSalaryGenerator.JoiningDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aEmpSalaryGenerator.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aEmpSalaryGenerator.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aEmpSalaryGenerator.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aEmpSalaryGenerator.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DepId", aEmpSalaryGenerator.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aEmpSalaryGenerator.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aEmpSalaryGenerator.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aEmpSalaryGenerator.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aEmpSalaryGenerator.EmpGradeId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aEmpSalaryGenerator.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@MonthDays", aEmpSalaryGenerator.MonthDays));
            aSqlParameterlist.Add(new SqlParameter("@PunchDay", aEmpSalaryGenerator.PunchDay));
            aSqlParameterlist.Add(new SqlParameter("@TotalHoliday", aEmpSalaryGenerator.TotalHoliday));
            aSqlParameterlist.Add(new SqlParameter("@TotalLeave", aEmpSalaryGenerator.TotalLeave));
            aSqlParameterlist.Add(new SqlParameter("@WorkingDays", aEmpSalaryGenerator.WorkingDays));
            aSqlParameterlist.Add(new SqlParameter("@AbsentDays", aEmpSalaryGenerator.AbsentDays));
            aSqlParameterlist.Add(new SqlParameter("@Gross", aEmpSalaryGenerator.Gross));
            aSqlParameterlist.Add(new SqlParameter("@ActualGross", aEmpSalaryGenerator.ActualGross));
            aSqlParameterlist.Add(new SqlParameter("@Basic", aEmpSalaryGenerator.Basic));
            aSqlParameterlist.Add(new SqlParameter("@HouseRent", aEmpSalaryGenerator.HouseRent));
            aSqlParameterlist.Add(new SqlParameter("@Medical", aEmpSalaryGenerator.Medical));
            aSqlParameterlist.Add(new SqlParameter("@LunchAllowance", aEmpSalaryGenerator.LunchAllowance));
            aSqlParameterlist.Add(new SqlParameter("@ConveyanceAllowance", aEmpSalaryGenerator.ConveyanceAllowance));
            aSqlParameterlist.Add(new SqlParameter("@OtherAllowances", aEmpSalaryGenerator.OtherAllowances));
            aSqlParameterlist.Add(new SqlParameter("@MobileAllowance", aEmpSalaryGenerator.MobileAllowance));
            aSqlParameterlist.Add(new SqlParameter("@SpecialAllowance", aEmpSalaryGenerator.SpecialAllowance));
            aSqlParameterlist.Add(new SqlParameter("@HolidayBillAmt", aEmpSalaryGenerator.HolidayBillAmt));
            aSqlParameterlist.Add(new SqlParameter("@NightBillAmt", aEmpSalaryGenerator.NightBillAmt));
            aSqlParameterlist.Add(new SqlParameter("@Arrear", aEmpSalaryGenerator.Arrear));
            aSqlParameterlist.Add(new SqlParameter("@OTHours", aEmpSalaryGenerator.OTHours));
            aSqlParameterlist.Add(new SqlParameter("@OTAmount", aEmpSalaryGenerator.OTAmount));
            aSqlParameterlist.Add(new SqlParameter("@OTRate", aEmpSalaryGenerator.OTRate));
            aSqlParameterlist.Add(new SqlParameter("@TiffinCharge", aEmpSalaryGenerator.TiffinCharge));
            aSqlParameterlist.Add(new SqlParameter("@AttnBonusRate", aEmpSalaryGenerator.AttnBonusRate));
            aSqlParameterlist.Add(new SqlParameter("@SpecialBonus", aEmpSalaryGenerator.SpecialBonus));
            aSqlParameterlist.Add(new SqlParameter("@SalaryAdvance", aEmpSalaryGenerator.SalaryAdvance));
            aSqlParameterlist.Add(new SqlParameter("@Tax", aEmpSalaryGenerator.Tax));
            aSqlParameterlist.Add(new SqlParameter("@Absenteeism", aEmpSalaryGenerator.Absenteeism));
            aSqlParameterlist.Add(new SqlParameter("@OtherDeduction", aEmpSalaryGenerator.OtherDeduction));
            aSqlParameterlist.Add(new SqlParameter("@SalAdvanceDeduc", aEmpSalaryGenerator.SalAdvDeduct));
            aSqlParameterlist.Add(new SqlParameter("@FoodCharge", aEmpSalaryGenerator.FoodCharge));
            aSqlParameterlist.Add(new SqlParameter("@NetPayable", aEmpSalaryGenerator.NetPayable));
            aSqlParameterlist.Add(new SqlParameter("@BankId", aEmpSalaryGenerator.BankId));
            aSqlParameterlist.Add(new SqlParameter("@BankAccNo", aEmpSalaryGenerator.BankAccNo));
            aSqlParameterlist.Add(new SqlParameter("@PayBankorCash", aEmpSalaryGenerator.PayBankorCash));
            aSqlParameterlist.Add(new SqlParameter("@PrFund", aEmpSalaryGenerator.PrFund));
            aSqlParameterlist.Add(new SqlParameter("@TiffinAllowance", aEmpSalaryGenerator.TiffinCharge));


            string insertQuery = @"insert into dbo.tblSalaryRecordPerMonth (SalaryId,EmpInfoId,EmpMasterCode,SalaryStartDate,SalaryEndDate,JoiningDate,CompanyInfoId,UnitId,DivisionId,DepId,SectionId,DesigId,EmpTypeId,EmpGradeId,SalScaleId,MonthDays,PunchDay,TotalHoliday,TotalLeave,WorkingDays,AbsentDays,Gross,ActualGross,Basic,HouseRent,Medical,ConveyanceAllowance,LunchAllowance,OtherAllowances,MobileAllowance,SpecialAllowance,HolidayBillAmt,NightBillAmt,Arrear,OTHours,OTRate,OTAmount,TiffinAllowance,AttnBonusRate,SpecialBonus,SalaryAdvance,Tax,Absenteeism,OtherDeduction,SalAdvanceDeduc,FoodCharge,NetPayable,PayBankorCash,BankId,BankAccNo,PrFund) 
                                                                  values (@SalaryId,@EmpInfoId,@EmpMasterCode,@SalaryStartDate,@SalaryEndDate,@JoiningDate,@CompanyInfoId,@UnitId,@DivisionId,@DepId,@SectionId,@DesigId,@EmpTypeId,@EmpGradeId,@SalScaleId,@MonthDays,@PunchDay,@TotalHoliday,@TotalLeave,@WorkingDays,@AbsentDays,@Gross,@ActualGross,@Basic,@HouseRent,@Medical,@ConveyanceAllowance,@LunchAllowance,@OtherAllowances,@MobileAllowance,@SpecialAllowance,@HolidayBillAmt,@NightBillAmt,@Arrear,@OTHours,@OTRate,@OTAmount,@TiffinAllowance,@AttnBonusRate,@SpecialBonus,@SalaryAdvance,@Tax,@Absenteeism,@OtherDeduction,@SalAdvanceDeduc,@FoodCharge,@NetPayable,@PayBankorCash,@BankId,@BankAccNo,@PrFund)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }

        public DataTable Salary(string empId)
        {
            string query = @"select * from dbo.tblEmpSalaryHistory where EmpInfoId='"+empId+"' and OverTimeHour='active' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable ActiveEmpGenInfo()
        {
            string query = @"select * from tblEmpGeneralInfo where EmployeeStatus='Active' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable Arrear(string EmpInfoId,DateTime fromdt,DateTime todt)
        {
            string query = "SELECT ArearAmount FROM dbo.tblArrear WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' AND tblArrear.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB"); 
        }
        public DataTable OtherAllowance(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(Amount),0)AS Amount FROM dbo.tblOtherAllowance WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' AND tblOtherAllowance.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable FoodCharge(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(FoodCAmount),0)AS FoodCAmount FROM dbo.tblFoodCharge WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' AND tblFoodCharge.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GrossChk(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='6' AND EmpInfoId='" + EmpInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public decimal Gross(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='6' AND EmpInfoId='"+EmpInfoId+"'";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
        }
        public DataTable BasicChk(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='1' AND EmpInfoId='" + EmpInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public decimal Basic(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='1' AND EmpInfoId='"+EmpInfoId+"' AND IsActive=1";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
        }
        public DataTable LunchAllowanceChk(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='5' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public decimal LunchAllowance(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='5' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
        }
        public DataTable ConveyanceAllowanceChk(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='3' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public decimal ConveyanceAllowance(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='3' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
        }
        public DataTable HouseRentChk(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='2' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public decimal HouseRent(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='2' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
        }
        public DataTable MedicalChk(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='4' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public decimal Medical(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount  FROM dbo.tblSalaryInformation WHERE SalaryHeadId='4' AND EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
        }
        public DataTable SalHeadNameconvChk(string empid)
        {
            string query = @"SELECT SalHeadName FROM dbo.tblSalaryInformation WHERE EmpInfoId='" + empid + "' AND SalHeadName='ConveyanceAllowance' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SalHeadNameLunchChk(string empid)
        {
            string query = @"SELECT SalHeadName FROM dbo.tblSalaryInformation WHERE EmpInfoId='" + empid + "' AND SalHeadName='Lunch Allowance' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpGeneralId(string empid)
        {
            string query = @"select CompanyInfoId,UnitId,DivisionId,DepId,SectionId,DesigId,EmpTypeId,EmpGradeId,SalScaleId,EmpMasterCode,EmpInfoId,EmployeeStatus,JoiningDate,BankId,BankAccNo from tblEmpGeneralInfo where EmpInfoId='" + empid + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable EmpCheck(string empid)
        {
            string query = @"SELECT DISTINCT dbo.tblEmpGeneralInfo.EmpInfoId FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId WHERE dbo.tblEmpGeneralInfo.EmpInfoId='" + empid + "' AND EmployeeStatus='Active' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public int AttendenceAbsent(string EmpInfoId,DateTime fromdate,DateTime todate)
        {
            string query = @"select count(*) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "' and ATTStatus='A' and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }

        public int AttendencePresent(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select count(*) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "' and ATTStatus='P' and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public int Leave(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select count(*) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "' and ATTStatus='LV' and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        //public int OverTime(string EmpInfoId,DateTime fromdate,DateTime todate)
        //{
        //    string query = "select sum(datediff(minute, 0, OverTimeDuration)) as OverTimeInMinute from  tblAttendanceRecord WHERE EmpId='" + EmpInfoId.Trim() + "' and ATTStatus='P' and ATTDate between '" + fromdate + "' and '" + todate + "'";
        //    return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        //}

        public string OTFlag(string EmpInfoId)
        {
            string query = "SELECT OTAllow FROM dbo.tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public string CheckEmpType(string EmpInfoId)
        {
            string query = "SELECT EmpType FROM dbo.tblEmpGeneralInfo LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId = dbo.tblEmployeeType.EmpTypeId WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        

        public decimal OTAmount(string EmpInfoId)
        {
            string query = "select tblEmpGeneralInfo.EmpMasterCode,tblSalaryGradeOrScale.OTRatePerHour from tblSalaryGradeOrScale LEFT JOIN tblEmpGeneralInfo ON tblSalaryGradeOrScale.SalGradeId = tblEmpGeneralInfo.SalaryScaleId where EmpInfoId='" + EmpInfoId + "' ";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
        }

        public int DayQtyofMonth(DateTime SalaryStartDate, DateTime SalaryEndDate)
        {
            string query = " DECLARE @SalaryStartDate DATETIME " +
                           " DECLARE @SalaryEndDate DATETIME " +

                           " SET @SalaryStartDate = '" + SalaryStartDate + "' " +
                           " SET @SalaryEndDate = '" + SalaryEndDate + "'; " +

                           " WITH dates(Date) AS "+
                        " ( "+
                           "  SELECT @SalaryStartDate as Date " +
                           "  UNION ALL "+
                           " SELECT DATEADD(d,1,[Date]) "+
                           "  END dates "+
                           "  WHERE DATE < @SalaryEndDate " +
                        ") "+

                          " SELECT count(Date) as DayQty "+
                          " FROM dates "+
                          " OPTION (MAXRECURSION 0) "+
                          " ";

            int dayQty = 0;
            DataTable aTable = new DataTable();
            aTable = aCommonInternalDal.DataContainerDataTable(query, "HRDB");
            dayQty = Convert.ToInt32(aTable.Rows[0]["DayQty"].ToString().Trim());
            return dayQty;

        }
    }
}
