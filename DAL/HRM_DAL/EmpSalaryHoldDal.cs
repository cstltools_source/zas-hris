﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;

namespace DAL.HRM_DAL
{
    public class EmpSalaryHoldDal
    {
        public void LoadUnitName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit  ORDER BY UnitName ";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }

        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }

        public DataTable GetEmployeeList(string pram)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();

            string query = @"SELECT EmpInfoId,EmpMasterCode,EmpName,UNT.UnitName,DG.DesigName,DV.DivName,DPT.DeptName,SEC.SectionName,EGI.IsSalary FROM dbo.tblEmpGeneralInfo AS EGI
                            LEFT JOIN dbo.tblCompanyUnit AS UNT ON UNT.UnitId = EGI.UnitId
                            LEFT JOIN dbo.tblDivision AS DV ON DV.DivisionId = EGI.DivisionId
                            LEFT JOIN dbo.tblDepartment AS DPT ON EGI.DepId = DPT.DeptId
                            LEFT JOIN dbo.tblSection AS SEC ON SEC.SectionId = EGI.SectionId
                            LEFT JOIN dbo.tblDesignation AS DG ON DG.DesigId = EGI.DesigId
                            WHERE EGI.EmployeeStatus IN ('Active') " + pram + " ORDER BY  CONVERT(int , dbo.udf_GetNumeric(EGI.EmpMasterCode) )  ASC";
            
            return aInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool UpdateSalaryHold(string empId, bool status)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();

            string query = @"UPDATE dbo.tblEmpGeneralInfo SET IsSalary = '" + status + "' WHERE EmployeeStatus IN ('Active') AND EmpInfoId = " + empId;

            return aInternalDal.UpdateDataByUpdateCommand(query, "HRDB");
        }
    }
}
