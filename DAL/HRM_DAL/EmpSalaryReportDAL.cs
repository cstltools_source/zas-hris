﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class EmpSalaryReportDAL
    {

        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public DataTable SalaryDeduction()
        {
            string query = @"SELECT * FROM dbo.tblSalaryDeduction
                 LEFT JOIN tblEmpGeneralInfo ON tblSalaryDeduction.EmpId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblSalaryDeduction.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblSalaryDeduction.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblSalaryDeduction.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblSalaryDeduction.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblSalaryDeduction.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblSalaryDeduction.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblSalaryDeduction.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblSalaryDeduction.DeptId = tblDepartment.DeptId  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ArrearRpt()
        {
            string query = @"SELECT * FROM dbo.tblArrear
	                 LEFT JOIN tblEmpGeneralInfo ON tblArrear.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblArrear.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblArrear.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblArrear.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblArrear.SectionId = tblSection.SectionId  
					 LEFT JOIN tblEmployeeGrade ON tblArrear.EmpGradeId = tblEmployeeGrade.GradeId  
					 LEFT JOIN tblEmployeeType ON tblArrear.EmpTypeId = tblEmployeeType.EmpTypeId  
					 LEFT JOIN tblDesignation ON tblArrear.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblArrear.DeptId = tblDepartment.DeptId    ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AdvanceSalDeducRpt()
        {
            string query = @"SELECT * FROM dbo.tblAdvanceSalaryDeduction
	                 LEFT JOIN tblEmpGeneralInfo ON tblAdvanceSalaryDeduction.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblAdvanceSalaryDeduction.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblAdvanceSalaryDeduction.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblAdvanceSalaryDeduction.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblAdvanceSalaryDeduction.SectionId = tblSection.SectionId  
					 LEFT JOIN tblEmployeeGrade ON tblAdvanceSalaryDeduction.EmpGradeId = tblEmployeeGrade.GradeId  
					 LEFT JOIN tblEmployeeType ON tblAdvanceSalaryDeduction.EmpTypeId = tblEmployeeType.EmpTypeId  
					 LEFT JOIN tblDesignation ON tblAdvanceSalaryDeduction.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblAdvanceSalaryDeduction.DeptId = tblDepartment.DeptId    ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpSalaryReportMainData(string parameter)
        {
            string query = @"SELECT * FROM dbo.tblSalaryRecordPerMonth
                                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryRecordPerMonth.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblSalaryRecordPerMonth.DesigId = dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblSalaryRecordPerMonth.DeptId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblSalaryRecordPerMonth.SecId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblSalaryGradeOrScale ON dbo.tblSalaryRecordPerMonth.SalGradeId=dbo.tblSalaryGradeOrScale.SalScaleId
                                 " + parameter + "";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpSalaryReportMainDataCom(string parameter)
        {
            string query = @"SELECT * FROM dbo.tblSalaryRecordPerMonthCom
                                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryRecordPerMonthCom.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblSalaryRecordPerMonthCom.DesigId = dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblSalaryRecordPerMonthCom.DeptId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblSalaryRecordPerMonthCom.SecId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblSalaryGradeOrScale ON dbo.tblSalaryRecordPerMonthCom.SalGradeId=dbo.tblSalaryGradeOrScale.SalScaleId
                                " + parameter + "";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable RptHeader()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel,'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoanAmountInfo(string frommonth,string tomonth,string year)
        {
            string query = @"SELECT S.EmpMasterCode,EmpName,SUM(SalaryAdvance) AS DeductLoan , LoanAmount,(LoanAmount-SUM(SalaryAdvance)) AS LoanDue,DeptName,DesigName,UnitName,DeductionStartDate FROM dbo.tblLoanMaster 
		    LEFT JOIN dbo.tblSalaryRecordPerMonth S ON S.EmpInfoId = dbo.tblLoanMaster.EmpInfoId
            LEFT JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId
            
            LEFT JOIN dbo.tblCompanyInfo ON S.CompanyInfoId = dbo.tblCompanyInfo.CompanyInfoId
            LEFT JOIN dbo.tblCompanyUnit ON S.UnitId=dbo.tblCompanyUnit.UnitId
            LEFT JOIN dbo.tblDivision ON S.DivisionId=dbo.tblDivision.DivisionId
            LEFT JOIN dbo.tblDepartment ON S.DepId=dbo.tblDepartment.DeptId
            LEFT JOIN dbo.tblSection ON S.SectionId=dbo.tblSection.SectionId
            LEFT JOIN dbo.tblEmployeeGrade ON S.EmpGradeId=dbo.tblEmployeeGrade.GradeId
            LEFT JOIN dbo.tblSalaryGradeOrScale ON S.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
            LEFT JOIN dbo.tblEmployeeType ON S.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
            LEFT JOIN dbo.tblBankInfo ON S.BankId=dbo.tblBankInfo.BankId
            LEFT JOIN dbo.tblDesignation ON S.DesigId=dbo.tblDesignation.DesigId  WHERE MONTH(S.SalaryStartDate) BETWEEN '" + frommonth + "' AND '" + tomonth + "' AND YEAR(SalaryStartDate)='" + year + "'  GROUP BY S.EmpMasterCode,EmpName,LoanAmount,DeptName,DesigName,UnitName,DeductionStartDate ORDER BY S.EmpMasterCode";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SalaryReportDetailsDAL(string parameter, string BankCash)
        {
                string query = @"SELECT  S.EmpInfoId ,
            S.EmpMasterCode ,
            E.EmpName,
            E.CellNumber,
            JL.JobLocationName,
            S.SalaryStartDate ,
            S.SalaryEndDate ,
            S.JoiningDate ,
            S.CompanyInfoId ,
            dbo.tblCompanyInfo.CompanyName,
            S.UnitId ,
            dbo.tblCompanyUnit.UnitName,
            S.DivisionId ,
            dbo.tblDivision.DivName,
            S.DepId ,
            dbo.tblDepartment.DeptName,
            S.SectionId ,
            dbo.tblSection.SectionName,
            S.DesigId ,
            dbo.tblDesignation.DesigName,
            S.EmpTypeId ,
            dbo.tblEmployeeType.EmpType,
            S.EmpGradeId ,
            dbo.tblEmployeeGrade.GradeName,
            S.SalScaleId ,
            dbo.tblSalaryGradeOrScale.SalScaleName,
            MonthDays ,
            PunchDay ,
            TotalHoliday ,
            TotalLeave ,
            WorkingDays ,
            AbsentDays ,
            Absenteeism ,
            (S.Gross+PrFund+MobileAllowance)as Gross ,
            ActualGross ,
            Basic ,
            S.HouseRent ,
            S.Medical ,
            MobileAllowance ,
            OtherAllowances ,
            ConveyanceAllowance ,
            Tax ,
            OtherDeduction ,
            SpecialAllowance ,
            OTHours ,
            S.OTRate ,
            OTAmount ,
            AttnBonusRate ,
            SalaryAdvance ,
            FoodCharge ,
            TiffinAllowance ,
            Arrear ,
            SpecialBonus ,
            HolidayBillAmt ,
            NightBillAmt ,
            LunchAllowance ,
            NetPayable ,
            PayBankorCash ,
            S.BankId ,
            BankName,
            S.BankAccNo ,Stamp,NationalIdNo,TINNo,OtherAmount,PFLoan,BikeLoan,LWP,
            SalaryId ,(MonthDays-AbsentDays) as PaymentDay, '" + BankCash + "' as BankCashFlag,'" + HttpContext.Current.Session["LoginName"].ToString() + "' as LoginName ,(OtherDeduction+S.BikeLoan+LWP+S.PFLoan+FoodCharge+(2*PrFund)+Absenteeism+SalaryAdvance+Tax) AS TotalDeduc,(2*PrFund) as PrFund,PrFund as PrFundMain,CONVERT(NVARCHAR(MAX),DATENAME(mm, SalaryStartDate))+'-'+CONVERT(NVARCHAR(MAX),DATENAME(yyyy, SalaryStartDate)) AS MonthYear  " +


         @"   FROM tblSalaryRecordPerMonth S
            LEFT JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId
            LEFT JOIN dbo.tblCompanyInfo ON S.CompanyInfoId = dbo.tblCompanyInfo.CompanyInfoId
            LEFT JOIN dbo.tblCompanyUnit ON S.UnitId=dbo.tblCompanyUnit.UnitId
            LEFT JOIN dbo.tblDivision ON S.DivisionId=dbo.tblDivision.DivisionId
            LEFT JOIN dbo.tblDepartment ON S.DepId=dbo.tblDepartment.DeptId
            LEFT JOIN dbo.tblSection ON S.SectionId=dbo.tblSection.SectionId
            LEFT JOIN dbo.tblEmployeeGrade ON S.EmpGradeId=dbo.tblEmployeeGrade.GradeId
            LEFT JOIN dbo.tblSalaryGradeOrScale ON S.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
            LEFT JOIN dbo.tblEmployeeType ON S.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
            LEFT JOIN dbo.tblBankInfo ON S.BankId=dbo.tblBankInfo.BankId
            LEFT JOIN tblJobLocation AS JL ON E.JobLocationisionId = JL.JobLocationisionId
            LEFT JOIN dbo.tblDesignation ON S.DesigId=dbo.tblDesignation.DesigId  WHERE  " + parameter + "";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SalaryReportDetailsComDAL(string parameter, string BankCash)
        {
            string query = @"SELECT  S.EmpInfoId ,
            S.EmpMasterCode ,
            E.EmpName,
            S.SalaryStartDate ,
            S.SalaryEndDate ,
            S.JoiningDate ,
            S.CompanyInfoId ,
            dbo.tblCompanyInfo.CompanyName,
            S.UnitId ,
            dbo.tblCompanyUnit.UnitName,
            S.DivisionId ,
            dbo.tblDivision.DivName,
            S.DepId ,
            dbo.tblDepartment.DeptName,
            S.SectionId ,
            dbo.tblSection.SectionName,
            S.DesigId ,
            dbo.tblDesignation.DesigName,
            S.EmpTypeId ,
            dbo.tblEmployeeType.EmpType,
            S.EmpGradeId ,
            dbo.tblEmployeeGrade.GradeName,
            S.SalScaleId ,
            dbo.tblSalaryGradeOrScale.SalScaleName,
            MonthDays ,
            PunchDay ,
            TotalHoliday ,
            TotalLeave ,
            WorkingDays ,
            AbsentDays ,
            Absenteeism ,
            S.Gross ,
            ActualGross ,
            Basic ,
            S.HouseRent ,
            S.Medical ,
            MobileAllowance ,
            OtherAllowances ,
            ConveyanceAllowance ,
            Tax ,
            OtherDeduction ,
            SpecialAllowance ,
            OTHours ,
            S.OTRate ,
            OTAmount ,
            AttnBonusRate ,
            SalaryAdvance ,
            FoodCharge ,
            TiffinAllowance ,
            Arrear ,
            SpecialBonus ,
            HolidayBillAmt ,
            NightBillAmt ,
            LunchAllowance ,
            NetPayable ,
            PayBankorCash ,
            S.BankId ,
            S.BankAccNo ,
            SalaryId , '" + BankCash + "' as BankCashFlag " +


     @"   FROM tblSalaryRecordPerMonthCom S
            LEFT JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId
            LEFT JOIN dbo.tblCompanyInfo ON S.CompanyInfoId = dbo.tblCompanyInfo.CompanyInfoId
            LEFT JOIN dbo.tblCompanyUnit ON S.UnitId=dbo.tblCompanyUnit.UnitId
            LEFT JOIN dbo.tblDivision ON S.DivisionId=dbo.tblDivision.DivisionId
            LEFT JOIN dbo.tblDepartment ON S.DepId=dbo.tblDepartment.DeptId
            LEFT JOIN dbo.tblSection ON S.SectionId=dbo.tblSection.SectionId
            LEFT JOIN dbo.tblEmployeeGrade ON S.EmpGradeId=dbo.tblEmployeeGrade.GradeId
            LEFT JOIN dbo.tblSalaryGradeOrScale ON S.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
            LEFT JOIN dbo.tblEmployeeType ON S.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
            LEFT JOIN dbo.tblDesignation ON S.DesigId=dbo.tblDesignation.DesigId
            LEFT JOIN tblJobLocation AS JL ON E.JobLocationisionId = JL.JobLocationisionId WHERE " + parameter + "  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpSalaryInfo(string empcode)
        {
            string query = @"SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId  where EmpMasterCode='"+empcode+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable FinalSattlementSalaryReportDetailsDAL(string parameter, string BankCash)
        {
            string query = @"SELECT  S.EmpInfoId ,
            S.EmpMasterCode ,
            E.EmpName,
            S.SalaryStartDate ,
            S.SalaryEndDate ,
            S.JoiningDate ,
            S.CompanyInfoId ,
            dbo.tblCompanyInfo.CompanyName,
            S.UnitId ,
            dbo.tblCompanyUnit.UnitName,
            S.DivisionId ,
            dbo.tblDivision.DivName,
            S.DepId ,
            dbo.tblDepartment.DeptName,
            S.SectionId ,
            dbo.tblSection.SectionName,
            S.DesigId ,
            dbo.tblDesignation.DesigName,
            S.EmpTypeId ,
            dbo.tblEmployeeType.EmpType,
            S.EmpGradeId ,
            dbo.tblEmployeeGrade.GradeName,
            S.SalScaleId ,
            dbo.tblSalaryGradeOrScale.SalScaleName,
            MonthDays ,
            PunchDay ,
            TotalHoliday ,
            TotalLeave ,
            WorkingDays ,
            AbsentDays ,
            Absenteeism ,
            S.Gross ,
            ActualGross ,
            Basic ,
            S.HouseRent ,
            S.Medical ,
            MobileAllowance ,
            OtherAllowances ,
            ConveyanceAllowance ,
            Tax ,
            OtherDeduction ,
            SpecialAllowance ,
            OTHours ,
            S.OTRate ,
            OTAmount ,
            AttnBonusRate ,
            SalaryAdvance ,
            FoodCharge ,
            TiffinAllowance ,
            Arrear ,
            SpecialBonus ,
            HolidayBillAmt ,
            NightBillAmt ,
            LunchAllowance ,
            NetPayable ,
            PayBankorCash ,
            S.BankId ,
            S.BankAccNo ,
            tblJobleft.EffectiveDate,
            tblJobleft.WorkYear,
             '" + BankCash + "' as BankCashFlag ,PrFund" +


     @"      FROM tblFinalSattlement S
            LEFT JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId
            LEFT JOIN dbo.tblCompanyInfo ON S.CompanyInfoId = dbo.tblCompanyInfo.CompanyInfoId
            LEFT JOIN dbo.tblCompanyUnit ON S.UnitId=dbo.tblCompanyUnit.UnitId
            LEFT JOIN dbo.tblDivision ON S.DivisionId=dbo.tblDivision.DivisionId
            LEFT JOIN dbo.tblDepartment ON S.DepId=dbo.tblDepartment.DeptId
            LEFT JOIN dbo.tblSection ON S.SectionId=dbo.tblSection.SectionId
            LEFT JOIN dbo.tblEmployeeGrade ON S.EmpGradeId=dbo.tblEmployeeGrade.GradeId
            LEFT JOIN dbo.tblSalaryGradeOrScale ON S.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
            LEFT JOIN dbo.tblEmployeeType ON S.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
            LEFT JOIN dbo.tblJobleft ON S.EmpInfoId=dbo.tblJobleft.EmpInfoId
            LEFT JOIN dbo.tblDesignation ON S.DesigId=dbo.tblDesignation.DesigId  WHERE  " + parameter + "  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable FinalSattlementSalaryReportDetailsComDAL(string parameter, string BankCash)
        {
            string query = @"SELECT  S.EmpInfoId ,
            S.EmpMasterCode ,
            E.EmpName,
            S.SalaryStartDate ,
            S.SalaryEndDate ,
            S.JoiningDate ,
            S.CompanyInfoId ,
            dbo.tblCompanyInfo.CompanyName,
            S.UnitId ,
            dbo.tblCompanyUnit.UnitName,
            S.DivisionId ,
            dbo.tblDivision.DivName,
            S.DepId ,
            dbo.tblDepartment.DeptName,
            S.SectionId ,
            dbo.tblSection.SectionName,
            S.DesigId ,
            dbo.tblDesignation.DesigName,
            S.EmpTypeId ,
            dbo.tblEmployeeType.EmpType,
            S.EmpGradeId ,
            dbo.tblEmployeeGrade.GradeName,
            S.SalScaleId ,
            dbo.tblSalaryGradeOrScale.SalScaleName,
            MonthDays ,
            PunchDay ,
            TotalHoliday ,
            TotalLeave ,
            WorkingDays ,
            AbsentDays ,
            Absenteeism ,
            S.Gross ,
            ActualGross ,
            Basic ,
            S.HouseRent ,
            S.Medical ,
            MobileAllowance ,
            OtherAllowances ,
            ConveyanceAllowance ,
            Tax ,
            OtherDeduction ,
            SpecialAllowance ,
            OTHours ,
            S.OTRate ,
            OTAmount ,
            AttnBonusRate ,
            SalaryAdvance ,
            FoodCharge ,
            TiffinAllowance ,
            Arrear ,
            SpecialBonus ,
            HolidayBillAmt ,
            NightBillAmt ,
            LunchAllowance ,
            NetPayable ,
            PayBankorCash ,
            S.BankId ,
            S.BankAccNo ,
             '" + BankCash + "' as BankCashFlag " +


     @"      FROM tblFinalSattlementCom S
            LEFT JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId
            LEFT JOIN dbo.tblCompanyInfo ON S.CompanyInfoId = dbo.tblCompanyInfo.CompanyInfoId
            LEFT JOIN dbo.tblCompanyUnit ON S.UnitId=dbo.tblCompanyUnit.UnitId
            LEFT JOIN dbo.tblDivision ON S.DivisionId=dbo.tblDivision.DivisionId
            LEFT JOIN dbo.tblDepartment ON S.DepId=dbo.tblDepartment.DeptId
            LEFT JOIN dbo.tblSection ON S.SectionId=dbo.tblSection.SectionId
            LEFT JOIN dbo.tblEmployeeGrade ON S.EmpGradeId=dbo.tblEmployeeGrade.GradeId
            LEFT JOIN dbo.tblSalaryGradeOrScale ON S.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
            LEFT JOIN dbo.tblEmployeeType ON S.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
            LEFT JOIN dbo.tblDesignation ON S.DesigId=dbo.tblDesignation.DesigId  WHERE  " + parameter + "  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }


        public DataTable EmpOTInfo(string empid,string month)
        {
            string query = @"SELECT OTHours,OTAmount FROM dbo.tblSalaryRecordPerMonth WHERE  EmpInfoId='" + empid + "' AND MONTH(SalaryStartDate)='" + month + "' ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpComOTInfo(string empid, string month)
        {
            string query = @"SELECT OTHours,OTAmount FROM dbo.tblSalaryRecordPerMonthCom WHERE  EmpInfoId='" + empid + "' AND MONTH(SalaryStartDate)='" + month + "' ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpGenInfo()
        {
            string query = @"SELECT EmpInfoId,EmpMasterCode,EmpName,DeptName,DesigName,SalScaleName,UnitName,SectionName FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                            LEFT JOIN dbo.tblSalaryGradeOrScale ON dbo.tblEmpGeneralInfo.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
                            LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                            LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId WHERE OTAllow='Yes'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Basic(string empcode)
        {
            string query = @"SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                             WHERE SalHeadName='Basic' AND EmpMasterCode='"+empcode+"'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable HouseRent(string empcode)
        {
            string query = @"SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                             WHERE SalHeadName='House Rent' AND EmpMasterCode='" + empcode + "'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable Medical(string empcode)
        {
            string query = @"SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                             WHERE SalHeadName='Medical' AND EmpMasterCode='" + empcode + "'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LunchAllow(string empcode)
        {
            string query = @"SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                             WHERE SalHeadName='Lunch Allowance' AND EmpMasterCode='" + empcode + "'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable Conveyance(string empcode)
        {
            string query = @"SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                             WHERE SalHeadName='Conveyance' AND EmpMasterCode='" + empcode + "'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

    }
}
