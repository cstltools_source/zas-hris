﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class EmployeeCategoryDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForEmployeeCategory(EmployeeCategory aCategory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryId", aCategory.EmpCategoryId));
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryCode", aCategory.EmpCategoryCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryName", aCategory.EmpCategoryName));
            
            string insertQuery = @"insert into tblEmpCategory (EmpCategoryId,EmpCategoryCode,EmpCategoryName) 
            values (@EmpCategoryId,@EmpCategoryCode,@EmpCategoryName)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasEmpCategoryName(EmployeeCategory aCategory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryName", aCategory.EmpCategoryName));
            string query = "select * from tblEmpCategory where EmpCategoryName = @EmpCategoryName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                   return true;
                }
            }
            return false;
        }

        public DataTable LoadEmployeeCetegoryView()
        {
            string query = @"SELECT * FROM tblEmpCategory ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public EmployeeCategory EmployeeCetegoryEditLoad(string employeeCetegoryId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryId", employeeCetegoryId));
            string query = "select * from tblEmpCategory where EmpCategoryId = @EmpCategoryId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            EmployeeCategory aEmployeeCategory = new EmployeeCategory();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aEmployeeCategory.EmpCategoryId = Int32.Parse(dataReader["EmpCategoryId"].ToString());
                    aEmployeeCategory.EmpCategoryCode = dataReader["EmpCategoryCode"].ToString();
                    aEmployeeCategory.EmpCategoryName = dataReader["EmpCategoryName"].ToString();
                }
            }
            return aEmployeeCategory;
        }

        public bool UpdateEmployeeCetegoryInfo(EmployeeCategory aEmployeeCategory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryId", aEmployeeCategory.EmpCategoryId));
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryName", aEmployeeCategory.EmpCategoryName));

            string query = @"UPDATE tblEmpCategory SET EmpCategoryName=@EmpCategoryName, HERE EmpCategoryId=@EmpCategoryId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteEmpCategoryInfo(string EmpCategoryId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryId", EmpCategoryId));

            string query = @"DELETE FROM dbo.tblEmpCategory WHERE EmpCategoryId=@EmpCategoryId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
