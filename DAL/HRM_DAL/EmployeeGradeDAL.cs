﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class EmployeeGradeDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveGradeInfo(EmployeeGrade aEmployeeGrade)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aEmployeeGrade.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeCode", aEmployeeGrade.GradeCode));
            aSqlParameterlist.Add(new SqlParameter("@GradeName", aEmployeeGrade.GradeName));
            aSqlParameterlist.Add(new SqlParameter("@GradeType", aEmployeeGrade.GradeType));

            string insertQuery = @"insert into tblEmployeeGrade (GradeId,GradeCode,GradeName,GradeType) 
            values (@GradeId,@GradeCode,@GradeName,@GradeType)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasGradeName(EmployeeGrade aEmployeeGrade)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GradeName", aEmployeeGrade.GradeName));
            string query = "select * from tblEmployeeGrade where GradeName = @GradeName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadGeadeView()
        {
            string query = @"SELECT * FROM tblEmployeeGrade ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public EmployeeGrade GradeEditLoad(string gradeId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GradeId", gradeId));
            string query = "select * from tblEmployeeGrade where GradeId = @GradeId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            EmployeeGrade aEmployeeGrade = new EmployeeGrade();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aEmployeeGrade.GradeId = Int32.Parse(dataReader["GradeId"].ToString());
                    aEmployeeGrade.GradeCode = dataReader["GradeCode"].ToString();
                    aEmployeeGrade.GradeName = dataReader["GradeName"].ToString();
                    aEmployeeGrade.GradeType = dataReader["GradeType"].ToString();
                }
            }
            return aEmployeeGrade;
        }
        
        public bool UpdateGradeInfo(EmployeeGrade aEmployeeGrade)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aEmployeeGrade.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeName", aEmployeeGrade.GradeName));
            aSqlParameterlist.Add(new SqlParameter("@GradeType", aEmployeeGrade.GradeType));

            string query = @"UPDATE tblEmployeeGrade SET GradeName=@GradeName,GradeType=@GradeType WHERE GradeId=@GradeId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteGradeInfo(string GradeId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GradeId", GradeId));

            string query = @"DELETE FROM dbo.tblEmployeeGrade WHERE GradeId=@GradeId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
