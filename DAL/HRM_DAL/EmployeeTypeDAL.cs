﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class EmployeeTypeDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForEmployeeType(EmployeeType aType)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aType.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpType", aType.EmpType));
            string insertQuery = @"insert into tblEmployeeType (EmpTypeId,EmpType) 
            values (@EmpTypeId,@EmpType)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasEmpTypeName(EmployeeType aType)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpType", aType.EmpType));
            string query = "select * from tblEmployeeType where EmpType=@EmpType";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadEmployeeTypeView()
        {
            string query = @"SELECT * FROM tblEmployeeType ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public EmployeeType EmployeeTypeEditLoad(string EmpTypeId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", EmpTypeId));
            string query = "select * from tblEmployeeType where EmpTypeId=@EmpTypeId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            EmployeeType aType = new EmployeeType();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aType.EmpTypeId = Int32.Parse(dataReader["EmpTypeId"].ToString());
                    aType.EmpType = dataReader["EmpType"].ToString();
                }
            }
            return aType;
        }

        public bool UpdateEmployeeType(EmployeeType aType)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aType.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpType", aType.EmpType));

            string query = @"UPDATE tblEmployeeType SET EmpType=@EmpType WHERE EmpTypeId=@EmpTypeId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteEmpTypeInfo(string EmpTypeId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", EmpTypeId));

            string query = @"DELETE FROM dbo.tblEmployeeType WHERE EmpTypeId=@EmpTypeId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
