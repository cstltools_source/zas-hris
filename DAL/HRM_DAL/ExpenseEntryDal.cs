﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using DAO.HRM_Entities;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace DAL.HRM_DAL
{
    public class ExpenseEntryDal
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();


        public void LoadFinancialYear(DropDownList ddl)
        {
            string queryStr = "select * from tblFinancialYear";
            aCommonInternalDal.LoadDropDownValue(ddl, "FiscalYearId", "FyrCode", queryStr, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName ,JoiningDate FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool SaveJobleft(ExpenseEntryDao aJobLeft)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aJobLeft.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aJobLeft.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aJobLeft.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aJobLeft.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aJobLeft.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aJobLeft.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aJobLeft.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aJobLeft.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aJobLeft.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@Month", aJobLeft.Month));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aJobLeft.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aJobLeft.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aJobLeft.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Year", aJobLeft.Year));
            aSqlParameterlist.Add(new SqlParameter("@ExpenseAmount", aJobLeft.ExpenseAmount));


            string insertQuery = @"insert into tblExpenseEntry (EmpInfoId,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,GradeId,Month,ActionStatus,EntryBy,EntryDate,Year,Amount) 
            values (@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@Month,@ActionStatus,@EntryBy,@EntryDate,@Year,@ExpenseAmount)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool CheckDuplicate(string code, string month, string year)
        {
            string query = @"SELECT * FROM tblExpenseEntry AS EP
                             LEFT JOIN tblEmpGeneralInfo AS EG ON EP.EmpInfoId = EG.EmpInfoId
                             WHERE EG.EmpMasterCode = '" + code + "' AND EP.Month = '" + month + "' AND Year = '" + year + "'";
            DataTable aTable = aCommonInternalDal.DataContainerDataTable(query, "HRDB");


            if (aTable.Rows.Count > 0)
            {
                return false;
            }
            return true;
        }

        public DataTable GetExpenseInfo(string parameter)
        {
            string query = @"SELECT EP.ExpenseId,EP.Amount,EP.Month,EP.Year,DSG.DesigName,UNT.UnitName,EG.EmpMasterCode,EG.EmpName,DPT.DeptName FROM tblExpenseEntry AS EP
                             LEFT JOIN tblEmpGeneralInfo AS EG ON EP.EmpInfoId = EG.EmpInfoId
                             LEFT JOIN tblCompanyUnit AS UNT ON EP.UnitId = UNT.UnitId
                             LEFT JOIN tblDepartment AS DPT ON EP.DeptId = DPT.DeptId
                             LEFT JOIN tblDesignation AS DSG ON EP.DesigId = DSG.DesigId
                             WHERE EP.Amount > 0 " + parameter;

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool DeleteData(string empidId)
        {
            string query = @"DELETE FROM tblExpenseEntry WHERE ExpenseId = " + empidId;
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, "HRDB");
        }

        public DataTable GetEmployeeInformation(string finId,string parameter)
        {
            string query = @"SELECT EMP.EmpInfoId,UnitName,EmpMasterCode,EmpName,DesigName,July,August,September,October,November,December,January,February,March,April,May,June,
                             CASE WHEN July > 0 OR August > 0 OR September > 0 OR October > 0 OR November > 0 OR December > 0 OR January > 0 OR February > 0 OR March > 0 OR April> 0 OR May > 0 OR June > 0 THEN 1 ELSE 0 END IsCheck FROM tblEmpGeneralInfo AS EMP
                             LEFT JOIN tblDesignation AS DSG ON EMP.DesigId = DSG.DesigId
                             LEFT JOIN tblCompanyUnit AS UNT ON EMP.UnitId = UNT.UnitId
                             LEFT JOIN (SELECT EmpInfoId,July,August,September,October,November,December,January,February,March,April,May,June 
                             FROM tblExpenseDetails AS ED WHERE FyrCode = " + finId + ") AS ED ON EMP.EmpInfoId = ED.EmpInfoId WHERE EMP.EmployeeStatus='Active' AND EMP.IsActive = 1 " + parameter + " ORDER BY CONVERT(int , dbo.udf_GetNumeric(EMP.EmpMasterCode) ) ASC ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool SaveExpenseDetails(ExpenseDetailsDao aTaxDetails, string entryBy, DateTime entryDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@FyrCode", aTaxDetails.FyrCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aTaxDetails.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@July", (object)(aTaxDetails.July ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@August", (object)(aTaxDetails.August ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@September", (object)(aTaxDetails.September ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@October", (object)(aTaxDetails.October ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@November", (object)(aTaxDetails.November ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@December", (object)(aTaxDetails.December ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@January", (object)(aTaxDetails.January ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@February", (object)(aTaxDetails.February ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@March", (object)(aTaxDetails.March ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@April", (object)(aTaxDetails.April ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@May", (object)(aTaxDetails.May ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@June", (object)(aTaxDetails.June ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", true));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", entryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", entryDate));

            string insertQuery = @"insert into tblExpenseDetails (FyrCode,EmpInfoId,July,August,September,October,November,December,January,February,March,April,May,June,IsActive,EntryBy,EntryDate) 
            values (@FyrCode,@EmpInfoId,@July,@August,@September,@October,@November,@December,@January,@February,@March,@April,@May,@June,@IsActive,@EntryBy,@EntryDate)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        

        public bool DeleteExistiongData(ExpenseDetailsDao aDao)
        {
            string query = @"DELETE FROM tblExpenseDetails WHERE EmpInfoId = " + aDao.EmpInfoId + " AND FyrCode = " + aDao.FyrCode;
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, "HRDB");
        }

        public DataTable GetExpenseReport(string reportParameter)
        {
            string query = @"SELECT UNT.UnitName,EG.EmpMasterCode,EG.EmpName,DesigName,JobLocationName,FY.FiscalYearId,ExpenseMonth,PerDayAmount,WorkingDayes,Amount FROM tblMonthlyExpense AS ED
                             LEFT JOIN tblEmpGeneralInfo AS EG ON ED.EmpInfoId = EG.EmpInfoId
                             LEFT JOIN tblDesignation AS DSG ON DSG.DesigId = EG.DesigId
                             LEFT JOIN tblCompanyUnit AS UNT ON UNT.UnitId = EG.UnitId
                             LEFT JOIN tblJobLocation AS JL ON JL.JobLocationisionId = EG.JobLocationisionId
                             LEFT JOIN tblFinancialYear AS FY ON ED.ExpenseYear = FY.FyrCode WHERE EmpMasterCode IS NOT NULL " + reportParameter + " ORDER BY CONVERT(int , dbo.udf_GetNumeric(EG.EmpMasterCode) ) ASC ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void ProcessExpense(string yearid, string month)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@Month", month));
            aSqlParameterlist.Add(new SqlParameter("@YearId", yearid));

            aCommonInternalDal.RunStoreProcedure("sp_GET_MonthlyExpenseProcess", aSqlParameterlist, "HRDB");
            
        }
    }
}
