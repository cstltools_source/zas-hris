﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Library.DAL.HRM_DAL
{
    public class FastibalBonusDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveFastibalBonusMastar(FastibalBonusMastar aFastibalBonusMastar)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FastivalBonusId", aFastibalBonusMastar.FastivalBonusId));
            aSqlParameterlist.Add(new SqlParameter("@FestivalId", aFastibalBonusMastar.FestivalId));
            aSqlParameterlist.Add(new SqlParameter("@FestivalYear", aFastibalBonusMastar.FestivalYear));
            aSqlParameterlist.Add(new SqlParameter("@PaymentDate", aFastibalBonusMastar.PaymentDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryId", aFastibalBonusMastar.EmpCategoryId));
            aSqlParameterlist.Add(new SqlParameter("@BonusApplicableOn", aFastibalBonusMastar.BonusApplicableOn));
            aSqlParameterlist.Add(new SqlParameter("@ServiceLenghMarginDate", aFastibalBonusMastar.ServiceLenghMarginDate));
            aSqlParameterlist.Add(new SqlParameter("@AddedBy", aFastibalBonusMastar.AddedBy));
            aSqlParameterlist.Add(new SqlParameter("@AddedDate", aFastibalBonusMastar.AddedDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aFastibalBonusMastar.Status));

            string insertQuery = @"INSERT INTO dbo.tblFastivalBonusMastar
                                    ( FastivalBonusId ,
                                      FestivalId ,
                                      FestivalYear ,
                                      PaymentDate ,
                                      EmpCategoryId ,
                                      BonusApplicableOn ,
                                      ServiceLenghMarginDate,
                                      AddedBy ,
                                      AddedDate ,
                                      Status 
                                    )
                            VALUES  ( @FastivalBonusId ,
                                      @FestivalId ,
                                      @FestivalYear ,
                                      @PaymentDate ,
                                      @EmpCategoryId ,
                                      @BonusApplicableOn ,
                                      @ServiceLenghMarginDate,
                                      @AddedBy ,
                                      @AddedDate ,
                                      @Status 
                                    )";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool SaveFastibalBonusDetail(FastibalBonusDetail aFastibalBonusDetail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FastivalBonusDetailsId", aFastibalBonusDetail.FastivalBonusDetailsId));
            aSqlParameterlist.Add(new SqlParameter("@FastivalBonusId", aFastibalBonusDetail.FastivalBonusId));
            aSqlParameterlist.Add(new SqlParameter("@FromMonth", aFastibalBonusDetail.FromMonth));
            aSqlParameterlist.Add(new SqlParameter("@ToMonth", aFastibalBonusDetail.ToMonth));
            aSqlParameterlist.Add(new SqlParameter("@Percentige", aFastibalBonusDetail.Percentige));

            string insertQuery = @"INSERT INTO dbo.tblFastivalBonusDetails
                                ( FastivalBonusDetailsId ,
                                  FastivalBonusId ,
                                  FromMonth ,
                                  ToMonth ,
                                  Percentige
                                )
                        VALUES  ( @FastivalBonusDetailsId ,
                                  @FastivalBonusId ,
                                  @FromMonth ,
                                  @ToMonth ,
                                  @Percentige
                                )";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasFastibalBonus(FastibalBonusMastar aFastibalBonusMastar)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FestivalId", aFastibalBonusMastar.FestivalId));
            aSqlParameterlist.Add(new SqlParameter("@FestivalYear", aFastibalBonusMastar.FestivalYear));
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryId", aFastibalBonusMastar.EmpCategoryId));
            aSqlParameterlist.Add(new SqlParameter("@BonusApplicableOn", aFastibalBonusMastar.BonusApplicableOn));
            string query = "SELECT * FROM dbo.tblFastivalBonusMastar WHERE FestivalId=@FestivalId AND FestivalYear=@FestivalYear AND EmpCategoryId=@EmpCategoryId AND BonusApplicableOn=@BonusApplicableOn ";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public int FestivalProcess(int fastivalId, string festivalYear, int empcategoryId, string executeby, string dataBaseName)
        {
            try
            {
                int count = 0;

                Database db;
                DbCommand dbCommand;
                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetStoredProcCommand("sp_FestivalBonusProcess");
                db.AddInParameter(dbCommand, "FestivalId", DbType.Int32, fastivalId);
                db.AddInParameter(dbCommand, "FestivalYear", DbType.String, festivalYear);
                db.AddInParameter(dbCommand, "EmpCategoryId", DbType.Int32, empcategoryId);
                db.AddInParameter(dbCommand, "ProcessExecutedBy", DbType.String, executeby);
                //db.AddOutParameter(dbCommand, "ProcessExecutedTime", DbType.DateTime, executeTime);
                db.AddInParameter(dbCommand, "ProcessExecutedTime", DbType.DateTime, DateTime.Now);

                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    //count = int.Parse(dbCommand.Parameters["@CountData"].Value.ToString());
                    count = 0;
                    return count;
                }

                return count;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        public DataTable GetFestivalBonusData(string festivalId,string festivalyear,string emcategoryId)
        {
            string query = @"SELECT * FROM dbo.tblFastivalBonusMastar WHERE EmpCategoryId='" + emcategoryId + "' AND FestivalId='" + festivalId + "' AND FestivalYear='"+festivalyear+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadFastibalBonusView()
        {
            string query = @"SELECT * FROM dbo.tblFastivalBonusMastar
                            LEFT JOIN dbo.tblFastivalName ON dbo.tblFastivalBonusMastar.FestivalId = dbo.tblFastivalName.FestivalId
                            LEFT JOIN dbo.tblEmpCategory ON dbo.tblFastivalBonusMastar.EmpCategoryId=dbo.tblEmpCategory.EmpCategoryId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadFastivalBonusEditData(string fastivalbonusId)
        {
            string query = @"SELECT * FROM dbo.tblFastivalBonusMastar
                            LEFT JOIN dbo.tblFastivalBonusDetails ON dbo.tblFastivalBonusMastar.FastivalBonusId = dbo.tblFastivalBonusDetails.FastivalBonusId
                            WHERE dbo.tblFastivalBonusMastar.FastivalBonusId='"+fastivalbonusId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable FestivalBonusReport(string year,string festivalId,string parameter)
        {
            string query = @"SELECT EmpMasterCode,EmpName,DeptName,DesigName,UnitName,dbo.tblCompanyUnit.UnitId,JoiningDate,Gross,Basic,BonusAmount,BonusPercentage,FestivalYear,FestivalName,dbo.tblFestivalBonusProcessData.PayType,tblEmpGeneralInfo.BankAccNo,'" + HttpContext.Current.Session["LoginName"] + "' as LoginName FROM dbo.tblFestivalBonusProcessData " +
                            " LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblFestivalBonusProcessData.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId "+
                             " LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId "+
                             " LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId" +
                             " LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId " +
                            " LEFT JOIN dbo.tblFastivalBonusMastar ON dbo.tblFestivalBonusProcessData.FastivalBonusId=dbo.tblFastivalBonusMastar.FastivalBonusId" +
                           " LEFT JOIN dbo.tblFastivalName ON dbo.tblFastivalBonusMastar.FestivalId = dbo.tblFastivalName.FestivalId  WHERE tblFastivalBonusMastar.FestivalId='" + festivalId + "'  AND tblFastivalBonusMastar.FestivalYear='" + year + "' " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadFestivalName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "SELECT * FROM dbo.tblFastivalName";
            aInternalDal.LoadDropDownValue(ddl, "FestivalName", "FestivalId", queryStr, "HRDB");
        }
        public void LoadEmpCateGory(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "SELECT * FROM dbo.tblEmpCategory";
            aInternalDal.LoadDropDownValue(ddl, "EmpCategoryName", "EmpCategoryId", queryStr, "HRDB");
        }
        //public FastibalName FastibalNameEditLoad(string FestivalId)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@FestivalId", FestivalId));
        //    string query = "select * from tblFastivalName where FestivalId = @FestivalId";
        //    IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

        //    FastibalName aFastibalName = new FastibalName();
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            aFastibalName.FestivalId = Int32.Parse(dataReader["FestivalId"].ToString());
        //            aFastibalName.FestivalName = dataReader["FestivalName"].ToString();
        //            aFastibalName.AddedBy = dataReader["AddedBy"].ToString();
        //            aFastibalName.AddedDate = Convert.ToDateTime(dataReader["AddedDate"].ToString());
        //            aFastibalName.IsActive = Convert.ToBoolean((dataReader["IsActive"].ToString()));
        //        }
        //    }
        //    return aFastibalName;
        //}

        public bool UpdateFastibalBonusMastar(FastibalBonusMastar aFastibalBonusMastar)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FastivalBonusId", aFastibalBonusMastar.FastivalBonusId));
            aSqlParameterlist.Add(new SqlParameter("@FestivalId", aFastibalBonusMastar.FestivalId));
            aSqlParameterlist.Add(new SqlParameter("@FestivalYear", aFastibalBonusMastar.FestivalYear));
            aSqlParameterlist.Add(new SqlParameter("@PaymentDate", aFastibalBonusMastar.PaymentDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpCategoryId", aFastibalBonusMastar.EmpCategoryId));
            aSqlParameterlist.Add(new SqlParameter("@BonusApplicableOn", aFastibalBonusMastar.BonusApplicableOn));
            aSqlParameterlist.Add(new SqlParameter("@ServiceLenghMarginDate", aFastibalBonusMastar.ServiceLenghMarginDate));
            aSqlParameterlist.Add(new SqlParameter("@UpdateBy", aFastibalBonusMastar.UpdateBy));
            aSqlParameterlist.Add(new SqlParameter("@UpdateDate", aFastibalBonusMastar.UpdateDate));
            //aSqlParameterlist.Add(new SqlParameter("@Status", aFastibalBonusMastar.Status));

            string query = @"UPDATE tblFastivalBonusMastar SET 
                                      FestivalId=@FestivalId ,
                                      FestivalYear=@FestivalYear ,
                                      PaymentDate=@PaymentDate ,
                                      EmpCategoryId=@EmpCategoryId ,
                                      BonusApplicableOn=@BonusApplicableOn ,
                                      ServiceLenghMarginDate=@ServiceLenghMarginDate,
                                      UpdateBy=@UpdateBy ,
                                      UpdateDate=@UpdateDate WHERE FastivalBonusId=@FastivalBonusId
                                       ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool UpdateFastibalBonusDetail(FastibalBonusDetail aFastibalBonusDetail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FastivalBonusDetailsId", aFastibalBonusDetail.FastivalBonusDetailsId));
            aSqlParameterlist.Add(new SqlParameter("@FastivalBonusId", aFastibalBonusDetail.FastivalBonusId));
            aSqlParameterlist.Add(new SqlParameter("@FromMonth", aFastibalBonusDetail.FromMonth));
            aSqlParameterlist.Add(new SqlParameter("@ToMonth", aFastibalBonusDetail.ToMonth));
            aSqlParameterlist.Add(new SqlParameter("@Percentige", aFastibalBonusDetail.Percentige));

            string query = @"UPDATE tblFastivalBonusDetails SET 
                                  FastivalBonusId=@FastivalBonusId ,
                                  FromMonth=@FromMonth ,
                                  ToMonth=@ToMonth ,
                                  Percentige=@Percentige WHERE FastivalBonusDetailsId=@FastivalBonusDetailsId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteFastibalBonusDetail(string festivalId)
        {
            string query = @"DELETE FROM dbo.tblFastivalBonusDetails WHERE FastivalBonusDetailsId='"+festivalId+"'";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, "HRDB");
        }
        //public bool DeleteFastibalName(string FestivalId)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@FestivalId", FestivalId));

        //    string query = "DELETE FROM dbo.tblFastivalName WHERE FestivalId=@FestivalId ";

        //    return aCommonInternalDal.DeleteDataByDeleteCommand(query, aSqlParameterlist, "HRDB");
        //}
    }
}
