﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class FastibalNameDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveFastibalName(FastibalName aFastibalName)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FestivalId", aFastibalName.FestivalId));
            aSqlParameterlist.Add(new SqlParameter("@FestivalName", aFastibalName.FestivalName));
            aSqlParameterlist.Add(new SqlParameter("@AddedBy", aFastibalName.AddedBy));
            aSqlParameterlist.Add(new SqlParameter("@AddedDate", aFastibalName.AddedDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aFastibalName.IsActive));

            string insertQuery = @"insert into tblFastivalName (FestivalId,FestivalName,AddedBy,AddedDate,IsActive) 
            values (@FestivalId,@FestivalName,@AddedBy,@AddedDate,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasFastibalName(FastibalName aFastibalName)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FestivalName", aFastibalName.FestivalName));
            string query = "select * from tblFastivalName where FestivalName = @FestivalName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadFastibalNameView()
        {
            string query = @"SELECT * FROM dbo.tblFastivalName";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        //public void LoadDivisionName(DropDownList ddl)
        //{
        //    ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
        //    string queryStr = "select * from tblDivision";
        //    aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        //}
        public FastibalName FastibalNameEditLoad(string FestivalId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FestivalId", FestivalId));
            string query = "select * from tblFastivalName where FestivalId = @FestivalId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            FastibalName aFastibalName = new FastibalName();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aFastibalName.FestivalId = Int32.Parse(dataReader["FestivalId"].ToString());
                    aFastibalName.FestivalName = dataReader["FestivalName"].ToString();
                    aFastibalName.AddedBy = dataReader["AddedBy"].ToString();
                    aFastibalName.AddedDate = Convert.ToDateTime(dataReader["AddedDate"].ToString());
                    aFastibalName.IsActive = Convert.ToBoolean((dataReader["IsActive"].ToString()));
                }
            }
            return aFastibalName;
        }

        public bool UpdateFastibalName(FastibalName aFastibalName)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FestivalId", aFastibalName.FestivalId));
            aSqlParameterlist.Add(new SqlParameter("@FestivalName", aFastibalName.FestivalName));
            
            string query = @"UPDATE tblFastivalName SET FestivalName=@FestivalName WHERE FestivalId=@FestivalId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteFastibalName(string FestivalId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FestivalId", FestivalId));

            string query = "DELETE FROM dbo.tblFastivalName WHERE FestivalId=@FestivalId ";

            return aCommonInternalDal.DeleteDataByDeleteCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
