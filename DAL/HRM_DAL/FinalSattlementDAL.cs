﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class FinalSattlementDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForEmpSalaryGeneartor(FinalSattlement aFinalSattlement)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            //aSqlParameterlist.Add(new SqlParameter("@SalGeneratId", aFinalSattlement.SalGeneratId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aFinalSattlement.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryStartDate", aFinalSattlement.SalaryStartDate));
            aSqlParameterlist.Add(new SqlParameter("@SalaryEndDate", aFinalSattlement.SalaryEndDate));
            aSqlParameterlist.Add(new SqlParameter("@JoiningDate", aFinalSattlement.JoiningDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aFinalSattlement.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aFinalSattlement.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aFinalSattlement.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aFinalSattlement.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DepId", aFinalSattlement.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aFinalSattlement.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aFinalSattlement.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aFinalSattlement.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aFinalSattlement.EmpGradeId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aFinalSattlement.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@MonthDays", aFinalSattlement.MonthDays));
            aSqlParameterlist.Add(new SqlParameter("@PunchDay", aFinalSattlement.PunchDay));
            aSqlParameterlist.Add(new SqlParameter("@TotalHoliday", aFinalSattlement.TotalHoliday));
            aSqlParameterlist.Add(new SqlParameter("@TotalLeave", aFinalSattlement.TotalLeave));
            aSqlParameterlist.Add(new SqlParameter("@WorkingDays", aFinalSattlement.WorkingDays));
            aSqlParameterlist.Add(new SqlParameter("@AbsentDays", aFinalSattlement.AbsentDays));
            aSqlParameterlist.Add(new SqlParameter("@Gross", aFinalSattlement.Gross));
            aSqlParameterlist.Add(new SqlParameter("@ActualGross", aFinalSattlement.ActualGross));
            aSqlParameterlist.Add(new SqlParameter("@Basic", aFinalSattlement.Basic));
            aSqlParameterlist.Add(new SqlParameter("@HouseRent", aFinalSattlement.HouseRent));
            aSqlParameterlist.Add(new SqlParameter("@Medical", aFinalSattlement.Medical));
            aSqlParameterlist.Add(new SqlParameter("@LunchAllowance", aFinalSattlement.LunchAllowance));
            aSqlParameterlist.Add(new SqlParameter("@ConveyanceAllowance", aFinalSattlement.ConveyanceAllowance));
            aSqlParameterlist.Add(new SqlParameter("@OtherAllowances", aFinalSattlement.OtherAllowances));
            aSqlParameterlist.Add(new SqlParameter("@MobileAllowance", aFinalSattlement.MobileAllowance));
            aSqlParameterlist.Add(new SqlParameter("@SpecialAllowance", aFinalSattlement.SpecialAllowance));
            aSqlParameterlist.Add(new SqlParameter("@HolidayBillAmt", aFinalSattlement.HolidayBillAmt));
            aSqlParameterlist.Add(new SqlParameter("@NightBillAmt", aFinalSattlement.NightBillAmt));
            aSqlParameterlist.Add(new SqlParameter("@Arrear", aFinalSattlement.Arrear));
            aSqlParameterlist.Add(new SqlParameter("@OTHours", aFinalSattlement.OTHours));
            aSqlParameterlist.Add(new SqlParameter("@OTAmount", aFinalSattlement.OTAmount));
            aSqlParameterlist.Add(new SqlParameter("@OTRate", aFinalSattlement.OTRate));
            aSqlParameterlist.Add(new SqlParameter("@TiffinCharge", aFinalSattlement.TiffinCharge));
            aSqlParameterlist.Add(new SqlParameter("@AttnBonusRate", aFinalSattlement.AttnBonusRate));
            aSqlParameterlist.Add(new SqlParameter("@SpecialBonus", aFinalSattlement.SpecialBonus));
            aSqlParameterlist.Add(new SqlParameter("@SalaryAdvance", aFinalSattlement.SalaryAdvance));
            aSqlParameterlist.Add(new SqlParameter("@Tax", aFinalSattlement.Tax));
            aSqlParameterlist.Add(new SqlParameter("@Absenteeism", aFinalSattlement.Absenteeism));
            aSqlParameterlist.Add(new SqlParameter("@OtherDeduction", aFinalSattlement.OtherDeduction));
            aSqlParameterlist.Add(new SqlParameter("@FoodCharge", aFinalSattlement.FoodCharge));
            aSqlParameterlist.Add(new SqlParameter("@NetPayable", aFinalSattlement.NetPayable));
            aSqlParameterlist.Add(new SqlParameter("@BankId", aFinalSattlement.BankId));
            aSqlParameterlist.Add(new SqlParameter("@BankAccNo", aFinalSattlement.BankAccNo));
            aSqlParameterlist.Add(new SqlParameter("@PayBankorCash", aFinalSattlement.PayBankorCash));
            aSqlParameterlist.Add(new SqlParameter("@TiffinAllowance", aFinalSattlement.TiffinCharge));
            aSqlParameterlist.Add(new SqlParameter("@PrFund", aFinalSattlement.PrFund));


            string insertQuery = @"insert into dbo.tblFinalSattlement (EmpInfoId,EmpMasterCode,SalaryStartDate,SalaryEndDate,JoiningDate,CompanyInfoId,UnitId,DivisionId,DepId,SectionId,DesigId,EmpTypeId,EmpGradeId,SalScaleId,MonthDays,PunchDay,TotalHoliday,TotalLeave,WorkingDays,AbsentDays,Gross,ActualGross,Basic,HouseRent,Medical,ConveyanceAllowance,LunchAllowance,OtherAllowances,MobileAllowance,SpecialAllowance,HolidayBillAmt,NightBillAmt,Arrear,OTHours,OTRate,OTAmount,TiffinAllowance,AttnBonusRate,SpecialBonus,SalaryAdvance,Tax,Absenteeism,OtherDeduction,FoodCharge,NetPayable,PayBankorCash,BankId,BankAccNo,PrFund) 
                                                                  values (@EmpInfoId,@EmpMasterCode,@SalaryStartDate,@SalaryEndDate,@JoiningDate,@CompanyInfoId,@UnitId,@DivisionId,@DepId,@SectionId,@DesigId,@EmpTypeId,@EmpGradeId,@SalScaleId,@MonthDays,@PunchDay,@TotalHoliday,@TotalLeave,@WorkingDays,@AbsentDays,@Gross,@ActualGross,@Basic,@HouseRent,@Medical,@ConveyanceAllowance,@LunchAllowance,@OtherAllowances,@MobileAllowance,@SpecialAllowance,@HolidayBillAmt,@NightBillAmt,@Arrear,@OTHours,@OTRate,@OTAmount,@TiffinAllowance,@AttnBonusRate,@SpecialBonus,@SalaryAdvance,@Tax,@Absenteeism,@OtherDeduction,@FoodCharge,@NetPayable,@PayBankorCash,@BankId,@BankAccNo,@PrFund)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool SaveDataForEmpSalaryGeneartorCom(FinalSattlement aFinalSattlement)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            //aSqlParameterlist.Add(new SqlParameter("@SalGeneratId", aFinalSattlement.SalGeneratId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aFinalSattlement.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryStartDate", aFinalSattlement.SalaryStartDate));
            aSqlParameterlist.Add(new SqlParameter("@SalaryEndDate", aFinalSattlement.SalaryEndDate));
            aSqlParameterlist.Add(new SqlParameter("@JoiningDate", aFinalSattlement.JoiningDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aFinalSattlement.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aFinalSattlement.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aFinalSattlement.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aFinalSattlement.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DepId", aFinalSattlement.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aFinalSattlement.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aFinalSattlement.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aFinalSattlement.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aFinalSattlement.EmpGradeId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aFinalSattlement.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@MonthDays", aFinalSattlement.MonthDays));
            aSqlParameterlist.Add(new SqlParameter("@PunchDay", aFinalSattlement.PunchDay));
            aSqlParameterlist.Add(new SqlParameter("@TotalHoliday", aFinalSattlement.TotalHoliday));
            aSqlParameterlist.Add(new SqlParameter("@TotalLeave", aFinalSattlement.TotalLeave));
            aSqlParameterlist.Add(new SqlParameter("@WorkingDays", aFinalSattlement.WorkingDays));
            aSqlParameterlist.Add(new SqlParameter("@AbsentDays", aFinalSattlement.AbsentDays));
            aSqlParameterlist.Add(new SqlParameter("@Gross", aFinalSattlement.Gross));
            aSqlParameterlist.Add(new SqlParameter("@ActualGross", aFinalSattlement.ActualGross));
            aSqlParameterlist.Add(new SqlParameter("@Basic", aFinalSattlement.Basic));
            aSqlParameterlist.Add(new SqlParameter("@HouseRent", aFinalSattlement.HouseRent));
            aSqlParameterlist.Add(new SqlParameter("@Medical", aFinalSattlement.Medical));
            aSqlParameterlist.Add(new SqlParameter("@LunchAllowance", aFinalSattlement.LunchAllowance));
            aSqlParameterlist.Add(new SqlParameter("@ConveyanceAllowance", aFinalSattlement.ConveyanceAllowance));
            aSqlParameterlist.Add(new SqlParameter("@OtherAllowances", aFinalSattlement.OtherAllowances));
            aSqlParameterlist.Add(new SqlParameter("@MobileAllowance", aFinalSattlement.MobileAllowance));
            aSqlParameterlist.Add(new SqlParameter("@SpecialAllowance", aFinalSattlement.SpecialAllowance));
            aSqlParameterlist.Add(new SqlParameter("@HolidayBillAmt", aFinalSattlement.HolidayBillAmt));
            aSqlParameterlist.Add(new SqlParameter("@NightBillAmt", aFinalSattlement.NightBillAmt));
            aSqlParameterlist.Add(new SqlParameter("@Arrear", aFinalSattlement.Arrear));
            aSqlParameterlist.Add(new SqlParameter("@OTHours", aFinalSattlement.OTHours));
            aSqlParameterlist.Add(new SqlParameter("@OTAmount", aFinalSattlement.OTAmount));
            aSqlParameterlist.Add(new SqlParameter("@OTRate", aFinalSattlement.OTRate));
            aSqlParameterlist.Add(new SqlParameter("@TiffinCharge", aFinalSattlement.TiffinCharge));
            aSqlParameterlist.Add(new SqlParameter("@AttnBonusRate", aFinalSattlement.AttnBonusRate));
            aSqlParameterlist.Add(new SqlParameter("@SpecialBonus", aFinalSattlement.SpecialBonus));
            aSqlParameterlist.Add(new SqlParameter("@SalaryAdvance", aFinalSattlement.SalaryAdvance));
            aSqlParameterlist.Add(new SqlParameter("@Tax", aFinalSattlement.Tax));
            aSqlParameterlist.Add(new SqlParameter("@Absenteeism", aFinalSattlement.Absenteeism));
            aSqlParameterlist.Add(new SqlParameter("@OtherDeduction", aFinalSattlement.OtherDeduction));
            aSqlParameterlist.Add(new SqlParameter("@FoodCharge", aFinalSattlement.FoodCharge));
            aSqlParameterlist.Add(new SqlParameter("@NetPayable", aFinalSattlement.NetPayable));
            aSqlParameterlist.Add(new SqlParameter("@BankId", aFinalSattlement.BankId));
            aSqlParameterlist.Add(new SqlParameter("@BankAccNo", aFinalSattlement.BankAccNo));
            aSqlParameterlist.Add(new SqlParameter("@PayBankorCash", aFinalSattlement.PayBankorCash));
            aSqlParameterlist.Add(new SqlParameter("@TiffinAllowance", aFinalSattlement.TiffinCharge));
            aSqlParameterlist.Add(new SqlParameter("@PrFund", aFinalSattlement.PrFund));


            string insertQuery = @"insert into dbo.tblFinalSattlementCom (EmpInfoId,EmpMasterCode,SalaryStartDate,SalaryEndDate,JoiningDate,CompanyInfoId,UnitId,DivisionId,DepId,SectionId,DesigId,EmpTypeId,EmpGradeId,SalScaleId,MonthDays,PunchDay,TotalHoliday,TotalLeave,WorkingDays,AbsentDays,Gross,ActualGross,Basic,HouseRent,Medical,ConveyanceAllowance,LunchAllowance,OtherAllowances,MobileAllowance,SpecialAllowance,HolidayBillAmt,NightBillAmt,Arrear,OTHours,OTRate,OTAmount,TiffinAllowance,AttnBonusRate,SpecialBonus,SalaryAdvance,Tax,Absenteeism,OtherDeduction,FoodCharge,NetPayable,PayBankorCash,BankId,BankAccNo,PrFund) 
                                                                  values (@EmpInfoId,@EmpMasterCode,@SalaryStartDate,@SalaryEndDate,@JoiningDate,@CompanyInfoId,@UnitId,@DivisionId,@DepId,@SectionId,@DesigId,@EmpTypeId,@EmpGradeId,@SalScaleId,@MonthDays,@PunchDay,@TotalHoliday,@TotalLeave,@WorkingDays,@AbsentDays,@Gross,@ActualGross,@Basic,@HouseRent,@Medical,@ConveyanceAllowance,@LunchAllowance,@OtherAllowances,@MobileAllowance,@SpecialAllowance,@HolidayBillAmt,@NightBillAmt,@Arrear,@OTHours,@OTRate,@OTAmount,@TiffinAllowance,@AttnBonusRate,@SpecialBonus,@SalaryAdvance,@Tax,@Absenteeism,@OtherDeduction,@FoodCharge,@NetPayable,@PayBankorCash,@BankId,@BankAccNo,@PrFund)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }


        public bool DeleteDataForEmpSalaryGeneartor(FinalSattlement aFinalSattlement)
        {

            string deleteQuery = "delete from dbo.tblFinalSattlement where EmpInfoId='" + aFinalSattlement.EmpInfoId + "' and " +
                                 " MONTH(SalaryStartDate)='" + aFinalSattlement.SalaryStartDate.Month.ToString() + "' ";
            return aCommonInternalDal.DeleteDataByDeleteCommand(deleteQuery, "HRDB");

        }

        public bool DeleteDataForEmpSalaryGeneartorCom(FinalSattlement aFinalSattlement)
        {

            string deleteQuery = "delete from dbo.tblFinalSattlementCom where EmpInfoId='" + aFinalSattlement.EmpInfoId + "' and " +
                                 " MONTH(SalaryStartDate)='" + aFinalSattlement.SalaryStartDate.Month.ToString() + "' ";
            return aCommonInternalDal.DeleteDataByDeleteCommand(deleteQuery, "HRDB");

        }

        public DataTable GetAllActiveEmployee()
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo WHERE EmployeeStatus='Active' order by EmpInfoId asc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetPrFundAmount(string empinfoId)
        {
            //string query = @"SELECT SUM(PrFundCom) AS PrFund,SUM(PrFund) AS PrFundTotal FROM dbo.tblSalaryRecordPerMonth WHERE EmpInfoId='" + empinfoId + "'";
            string query = @"SELECT SUM(PrFund) AS PrFund,SUM(PrFund) AS PrFundTotal FROM dbo.tblSalaryRecordPerMonth WHERE EmpInfoId='" + empinfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetWorkDays(string empinfoId)
        {
            string query = @"SELECT * FROM dbo.tblJobleft WHERE EmpInfoId='" + empinfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetAllActiveEmployee(string EmpMasterCode)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpMasterCode, tblEmpGeneralInfo.EmpInfoId,tblEmpGeneralInfo.EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblJobleft.CompanyInfoId , DesigName , tblJobleft.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,tblJobleft.GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName ,tblSal.Amount,PayType,JoiningDate,BankAccNo,BankId,SalScaleId,EmpGradeId,dbo.tblJobleft.EffectiveDate,DepId,OTAllow FROM dbo.tblJobleft 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblJobleft.GradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblJobleft.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblJobleft.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblJobleft.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblJobleft.DeptId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblJobleft.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblJobleft.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblJobleft.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
                                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblJobleft.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                                LEFT JOIN dbo.tblEmpSalaryHistory ON dbo.tblEmpGeneralInfo.EmpInfoId = dbo.tblEmpSalaryHistory.EmpInfoId
                                LEFT JOIN
								 (SELECT EmpInfoId,Amount FROM dbo.tblSalaryInformation WHERE SalHeadName='Gross' and IsActive=1) AS tblSal
								 ON dbo.tblJobleft.EmpInfoId=tblSal.EmpInfoId WHERE EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable GetEmployee(string EmpMasterCode)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblJobleft.CompanyInfoId , DesigName , tblJobleft.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,tblJobleft.GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName ,tblSal.Amount FROM dbo.tblJobleft 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblJobleft.GradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblJobleft.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblJobleft.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblJobleft.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblJobleft.DeptId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblJobleft.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblJobleft.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblJobleft.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
                                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblJobleft.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                                LEFT JOIN dbo.tblEmpSalaryHistory ON dbo.tblEmpGeneralInfo.EmpInfoId = dbo.tblEmpSalaryHistory.EmpInfoId
                                LEFT JOIN
								 (SELECT EmpInfoId,Amount FROM dbo.tblSalaryInformation WHERE SalHeadName='Gross') AS tblSal
								 ON dbo.tblJobleft.EmpInfoId=tblSal.EmpInfoId EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public string GetMonthDays(DateTime fromdt, DateTime todt)
        {

            string query = "SELECT DATEDIFF(dd,'" + fromdt + "','" + todt + "')+1 AS DaysQty";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString();
        }
        public int GetAttendencePunchDays(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select count(*) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "' and ATTStatus<>'A' and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }


        public decimal Gross(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='6' AND EmpInfoId='" + EmpInfoId + "' and ActionStatus='Accepted' and (InactiveDate is null or InactiveDate='' ) AND IsActive=1";
            decimal amount = 0;

            if (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows.Count > 0)
            {
                return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
            }
            else
            {
                return amount;
            }
        }

        public decimal Basic(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='1' AND EmpInfoId='" + EmpInfoId + "' and ActionStatus='Accepted' and (InactiveDate is null or InactiveDate='' ) AND IsActive=1";
            decimal amount = 0;

            if (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows.Count > 0)
            {
                return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
            }
            else
            {
                return amount;
            }
        }

        public decimal HouseRent(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='2' AND EmpInfoId='" + EmpInfoId + "' and ActionStatus='Accepted' and (InactiveDate is null or InactiveDate='' ) AND IsActive=1";
            decimal amount = 0;

            if (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows.Count > 0)
            {
                return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
            }
            else
            {
                return amount;
            }
        }

        public decimal Medical(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='4' AND EmpInfoId='" + EmpInfoId + "' and ActionStatus='Accepted' and (InactiveDate is null or InactiveDate='' ) AND IsActive=1";
            decimal amount = 0;

            if (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows.Count > 0)
            {
                return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
            }
            else
            {
                return amount;
            }
        }

        public decimal ConveyanceAllowance(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='3' AND EmpInfoId='" + EmpInfoId + "' and ActionStatus='Accepted' and (InactiveDate is null or InactiveDate='' ) AND IsActive=1";
            decimal amount = 0;

            if (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows.Count > 0)
            {
                return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
            }
            else
            {
                return amount;
            }
        }


        public decimal LunchAllowance(string EmpInfoId)
        {
            string query = "SELECT SalHeadName,Amount FROM dbo.tblSalaryInformation WHERE SalaryHeadId='5' AND EmpInfoId='" + EmpInfoId + "' and ActionStatus='Accepted' and (InactiveDate is null or InactiveDate='' ) AND IsActive=1";
            decimal amount = 0;

            if (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows.Count > 0)
            {
                return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][1].ToString());
            }
            else
            {
                return amount;
            }
        }
        public decimal GetTiffinCharge(string EmpInfoId, DateTime fromdate, DateTime todate)
        {

            decimal TiffinCharge = 0;
            int days = 0;

            string query = @"select count(*) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "' and ATTStatus in ('P','L','OD') and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            days = Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());

            string query1 = "SELECT D.* FROM dbo.tblDesignation D INNER JOIN dbo.tblEmpGeneralInfo E ON D.DesigId = E.DesigId WHERE E.EmpInfoId='" + EmpInfoId + "'";
            TiffinCharge = Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query1, "HRDB").Rows[0]["TiffinAllow"].ToString());
            TiffinCharge = TiffinCharge * days;

            return TiffinCharge;
        }

        public int GetHoliDays(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select count(*) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "' and ATTStatus in ('WH','GH') and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public DataTable FoodCharge(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(FoodCAmount),0)AS FoodCAmount FROM dbo.tblFoodCharge WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' and ActionStatus='Accepted' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable OtherAllowance(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(Amount),0)AS Amount FROM dbo.tblOtherAllowance WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' and ActionStatus='Accepted' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable Arrear(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(ArearAmount),0)AS Amount  FROM dbo.tblArrear WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' and ActionStatus='Accepted' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable MobileAllowance(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(MobAllowanceAmont),0)AS Amount  FROM dbo.tblMobileAllowance WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' and ActionStatus='Accepted' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public decimal SalaryAdvance(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(DeductionAmount),0)AS Amount  FROM dbo.tblAdvanceSalaryDeduction WHERE EmpInfoId='" + EmpInfoId + "' AND EffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' and ActionStatus='Accepted' AND IsActive=1";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public decimal LoanAmount(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = @"SELECT  
	                  ISNULL(d.Amount,0) 
	                   FROM dbo.tblLoanMaster m 
	          INNER JOIN dbo.tblLoanDetail d ON m.LoanId = d.LoanId
	          WHERE m.Status='Accepted' AND m.EmpInfoId='" + EmpInfoId + "' AND d.InstallmentDate BETWEEN '" + fromdt + "' AND '" + todt + "'";
            return (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows.Count < 1) ? 0 : Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public decimal OtherDeduction(string EmpInfoId, DateTime fromdt, DateTime todt)
        {
            string query = "SELECT ISNULL(SUM(SDAmount),0)AS Amount  FROM dbo.tblSalaryDeduction WHERE EmpId='" + EmpInfoId + "' AND SDEffectiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' and ActionStatus='Accepted' AND IsActive=1";
            return Convert.ToDecimal(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public int AttendenceAbsent(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select ISNULL(count(*),0) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "' and ATTStatus='A' and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            //int monthDays = Convert.ToInt32(GetMonthDays(fromdate, todate));
            //int allRecord = GetAttendenceRecord(EmpInfoId, fromdate, todate);
            //return (Convert.ToInt32(aTransection.DataWarpByDataTable(query).Rows[0][0].ToString())) + (monthDays - allRecord);
            return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }

        public int GetAttendenceRecord(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select count(*) as ATT from  tblAttendanceRecord where EmpId='" + EmpInfoId + "'  and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return Convert.ToInt32(aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public DataTable GetOverTimeDataDAL(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select CONVERT(TIME(0),OverTimeDuration) AS OverTimeDuration from  tblAttendanceRecord where EmpId='" + EmpInfoId + "'  and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetOverTimeDataDALCom(string EmpInfoId, DateTime fromdate, DateTime todate)
        {
            string query = @"select CONVERT(TIME(0),ComOverTimeDuration) AS ComOverTimeDuration from  tblAttendanceRecord where EmpId='" + EmpInfoId + "'  and ATTDate between '" + fromdate + "' and '" + todate + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public string ComOverTimeHour(string empid, DateTime SalaryStartDate, DateTime SalaryEndDate)
        {
            DataTable dataTableAttData = GetOverTimeDataDALCom(empid, SalaryStartDate, SalaryEndDate);

            int hour = 0;
            int min = 0;


            int totalHour = 0;
            int totalMin = 0;
            if (dataTableAttData.Rows.Count > 0)
            {
                foreach (DataRow row in dataTableAttData.Rows)
                {
                    string fullTime = row[0].ToString().Trim();

                    if (fullTime != "")
                    {
                        string[] fullTimeSp = fullTime.Split(':');
                        hour = Convert.ToInt32(fullTimeSp[0]);


                        min = Convert.ToInt32(fullTimeSp[1]);
                    }
                    else
                    {
                        hour = 0;
                        min = 0;
                    }

                    if (hour > 0)
                    {
                        totalHour = totalHour + hour;
                        //if (min > 29 && min < 50)
                        //{
                        //    min = 30;
                        //    totalMin = totalMin + min;
                        //}
                        //else
                        //{
                        if (min >= 50)
                        {
                            min = 60;
                            totalMin = totalMin + min;
                        }
                        //}
                    }
                }

                if (totalHour > 0)
                {
                    if (totalMin > 0)
                    {
                        totalHour = totalHour + (totalMin / 60);
                    }
                    else
                    {
                        totalHour = totalHour;
                    }

                }

            }
            return totalHour.ToString();
        }
        public string OverTimeHour(string empid, DateTime SalaryStartDate, DateTime SalaryEndDate)
        {
            DataTable dataTableAttData = GetOverTimeDataDAL(empid, SalaryStartDate, SalaryEndDate);

            int hour = 0;
            int min = 0;


            int totalHour = 0;
            int totalMin = 0;
            if (dataTableAttData.Rows.Count > 0)
            {
                foreach (DataRow row in dataTableAttData.Rows)
                {
                    string fullTime = row[0].ToString().Trim();
                    string[] fullTimeSp = fullTime.Split(':');

                    hour = Convert.ToInt32(fullTimeSp[0]);
                    min = Convert.ToInt32(fullTimeSp[1]);
                    if (hour > 0)
                    {
                        totalHour = totalHour + hour;
                        //if (min > 29 && min < 50)
                        //{
                        //    min = 30;
                        //    totalMin = totalMin + min;
                        //}
                        //else
                        //{
                        if (min >= 50)
                        {
                            min = 60;
                            totalMin = totalMin + min;
                        }
                        //}
                    }
                }

                if (totalHour > 0)
                {
                    if (totalMin > 0)
                    {
                        totalHour = totalHour + (totalMin / 60);
                    }
                    else
                    {
                        totalHour = totalHour;
                    }

                }

            }
            return totalHour.ToString();
        }
    }
}
