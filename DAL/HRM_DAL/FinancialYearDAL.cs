﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class FinancialYearDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
            public bool SaveFinancialYear(FinancialYearEntry aFinancialYearEntry)
            {
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@FiscalYearId", aFinancialYearEntry.FiscalYearId));
                aSqlParameterlist.Add(new SqlParameter("@StartDate", aFinancialYearEntry.StartDate));
                aSqlParameterlist.Add(new SqlParameter("@EndDate", aFinancialYearEntry.EndDate));
                aSqlParameterlist.Add(new SqlParameter("@CompanyId", aFinancialYearEntry.CompanyId));
                //aSqlParameterlist.Add(new SqlParameter("@Status", aFinancialYearEntry.Status));
                //aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aFinancialYearEntry.ActiveDate));
                //aSqlParameterlist.Add(new SqlParameter("@InActiveDate", aFinancialYearEntry.InActiveDate));

                string insertQuery = @"insert into tblFinancialYear (FiscalYearId,StartDate,EndDate,CompanyId) 
                                   values (@FiscalYearId,@StartDate,@EndDate,@CompanyId)";
                return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

            }

            public bool HasFinancialYear(FinancialYearEntry aFinancialYearEntry)
            {
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@StartDate", aFinancialYearEntry.StartDate));
                string query = "select * from tblFinancialYear where StartDate=@StartDate";
                IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        return true;
                    }
                }
                return false;
            }

            public void LoadCompanyInfo(DropDownList ddl)
            {
                ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
                string queryStr = "select * from tblCompanyInfo";
                aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
            }

            public DataTable LoadFinancialYear()
            {
                string query = @"SELECT * from tblFinancialYear ";
                return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
            }

            public FinancialYearEntry FinancialYearEditLoad(string FyrCode)
            {
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@FyrCode", FyrCode));
                string query = "select * from tblFinancialYear where FyrCode=@FyrCode";
                IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
                FinancialYearEntry aFinancialYearEntry = new FinancialYearEntry();
                if (dataReader != null)
                {
                    while (dataReader.Read())
                    {
                        aFinancialYearEntry.FyrCode = Int32.Parse(dataReader["FyrCode"].ToString());
                        aFinancialYearEntry.FiscalYearId = dataReader["FiscalYearId"].ToString();
                        aFinancialYearEntry.StartDate = Convert.ToDateTime(dataReader["StartDate"].ToString());
                        aFinancialYearEntry.EndDate = Convert.ToDateTime(dataReader["EndDate"].ToString());
                        aFinancialYearEntry.CompanyId = Convert.ToInt32(dataReader["CompanyId"].ToString());
                        //aFinancialYearEntry.Status = dataReader["Status"].ToString();
                        //aFinancialYearEntry.ActiveDate = Convert.ToDateTime(dataReader["ActiveDate"].ToString());
                        //aFinancialYearEntry.InActiveDate = Convert.ToDateTime(dataReader["InActiveDate"].ToString());
                    }

                }
                return aFinancialYearEntry;
            }

            public bool UpdateFinancialYear(FinancialYearEntry aFinancialYearEntry)
            {
                List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
                aSqlParameterlist.Add(new SqlParameter("@FyrCode", aFinancialYearEntry.FyrCode));
                //aSqlParameterlist.Add(new SqlParameter("@FiscalYearId", aFinancialYearEntry.FiscalYearId));
                aSqlParameterlist.Add(new SqlParameter("@StartDate", aFinancialYearEntry.StartDate));
                aSqlParameterlist.Add(new SqlParameter("@EndDate", aFinancialYearEntry.EndDate));
                aSqlParameterlist.Add(new SqlParameter("@CompanyId", aFinancialYearEntry.CompanyId));
                //aSqlParameterlist.Add(new SqlParameter("@Status", aFinancialYearEntry.Status));
                //aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aFinancialYearEntry.ActiveDate));
                //aSqlParameterlist.Add(new SqlParameter("@InActiveDate", aFinancialYearEntry.InActiveDate));

                string query = @"UPDATE tblFinancialYear SET StartDate=@StartDate,EndDate=@EndDate,CompanyId=@CompanyId WHERE FyrCode=@FyrCode";
                return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
            }
        }
}
