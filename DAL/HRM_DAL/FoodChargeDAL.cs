﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class FoodChargeDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();

        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
       public string LoadForApprovalConditionDAL(string pageName, string userName)
       {

           return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
       }
        public bool SaveFoodCharges(FoodCharge aFoodCharge)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FoodId", aFoodCharge.FoodId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aFoodCharge.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@FoodCAmount", aFoodCharge.FoodCAmount));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aFoodCharge.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aFoodCharge.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aFoodCharge.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aFoodCharge.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aFoodCharge.IsActive));

            string insertQuery = @"insert into tblFoodCharge (FoodId,EmpInfoId,FoodCAmount,EffectiveDate,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@FoodId,@EmpInfoId,@FoodCAmount,@EffectiveDate,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasFoodCAmount(FoodCharge aFoodCharge)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aFoodCharge.EmpInfoId));
            string query = "select * from tblFoodCharge where EmpInfoId = @EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadFoodChargeView(string pram)
        {
            string query =@"SELECT * FROM dbo.tblFoodCharge
                           LEFT join dbo.tblEmpGeneralInfo ON dbo.tblFoodCharge.EmpInfoId=dbo.tblEmpGeneralInfo.EmpInfoId  
                           where  tblFoodCharge.ActionStatus in ('Posted','Deactivate') and tblFoodCharge.IsActive=1 " + pram + " order by tblFoodCharge.FoodId desc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string empcode)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + empcode + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public FoodCharge FoodChargeEditLoad(string FoodId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FoodId", FoodId));
            string query = "select * from tblFoodCharge where FoodId = @FoodId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            FoodCharge aFoodCharge = new FoodCharge();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aFoodCharge.FoodId = Int32.Parse(dataReader["FoodId"].ToString());
                    aFoodCharge.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aFoodCharge.FoodCAmount = Convert.ToDecimal(dataReader["FoodCAmount"].ToString());
                    aFoodCharge.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    aFoodCharge.EntryUser = dataReader["EntryUser"].ToString();
                    aFoodCharge.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                }
            }
            return aFoodCharge;
        }

        public bool UpdateFoodCharge(FoodCharge aFoodCharge)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FoodId", aFoodCharge.FoodId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aFoodCharge.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@FoodCAmount", aFoodCharge.FoodCAmount));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aFoodCharge.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aFoodCharge.ActionStatus));

            string query = @"UPDATE tblFoodCharge SET FoodCAmount=@FoodCAmount,ActionStatus=@ActionStatus,EmpInfoId=@EmpInfoId,EffectiveDate=@EffectiveDate WHERE FoodId=@FoodId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDALL(FoodCharge aFoodCharge)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FoodId", aFoodCharge.FoodId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aFoodCharge.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aFoodCharge.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aFoodCharge.ApprovedDate));

            string query = @"UPDATE tblFoodCharge SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE FoodId=@FoodId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }

        public DataTable LoadFoodChargeViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From dbo.tblFoodCharge
                             LEFT JOIN tblEmpGeneralInfo ON dbo.tblFoodCharge.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblFoodCharge.ActionStatus='" + ActionStatus + "' AND tblFoodCharge.IsActive=1  order by dbo.tblFoodCharge.FoodId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteData(string fId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblFoodCharge", "FoodId", fId);

        }
    }
}
