﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;

namespace DAL.HRM_DAL
{
    public class FoodChargeDeactivationDal
    {
        ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
        public void LoadUnitName(DropDownList ddl)
        {
            
            string queryStr = "select * from tblCompanyUnit  ORDER BY UnitName ";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }

        public DataTable GetActiveFoodChargeList(string parameter)
        {
            string query = @"SELECT FD.FoodId,UNT.UnitName,EG.EmpMasterCode,Eg.EmpName,DPT.DeptName,DSG.DesigName,FD.EffectiveDate,FD.FoodCAmount FROM dbo.tblFoodCharge AS FD
                             LEFT JOIN tblEmpGeneralInfo AS EG ON FD.EmpInfoId = EG.EmpInfoId
                             LEFT JOIN tblCompanyUnit AS UNT ON EG.UnitId = UNT.UnitId
                             LEFT JOIN tblDepartment AS DPT ON EG.DepId = DPT.DeptId
                             LEFT JOIN tblDesignation AS DSG ON DSG.DesigId = EG.DesigId
                             WHERE FD.IsActive = 1 AND FD.ActionStatus = 'Accepted' " + parameter;

            return aInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool DeactivateFoodCharge(int foodChargeId, string deleteby, DateTime deleteDate)
        {
            string queryStr = "UPDATE tblFoodCharge SET ActionStatus = 'Deactivate', DeleteBy = '" + deleteby + "', DeleteDate = '" + deleteDate + "' WHERE FoodId = " + foodChargeId;
            return aInternalDal.UpdateDataByUpdateCommand(queryStr, "HRDB");
        }
    }
}
