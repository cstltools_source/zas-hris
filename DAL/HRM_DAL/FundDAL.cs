﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;
using Microsoft.SqlServer.Server;

namespace Library.DAL.HRM_DAL
{
    public class FundDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForFund(Fund aFund)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FundId", aFund.FundId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aFund.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aFund.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aFund.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@FundName", aFund.FundName));
            aSqlParameterlist.Add(new SqlParameter("@Balance", aFund.Balance));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aFund.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aFund.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aFund.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aFund.Status));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aFund.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aFund.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aFund.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aFund.SectionId));

            string insertQuery = @"insert into tblFund (FundId,EmpInfoId,CompanyInfoId,UnitId,DivisionId,SectionId,FundName,Balance,ActiveDate,EntryBy,EntryDate,Status,DeptId,DesigId) 
                                   values (@FundId,@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@SectionId,@FundName,@Balance,@ActiveDate,@EntryUser,@EntryDate,@Status,@DeptId,@DesigId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasFund(Fund aFund)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aFund.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Year", aFund.ActiveDate.Year));
            aSqlParameterlist.Add(new SqlParameter("@FundId", aFund.FundId));
            string query = "select * from dbo.tblFund where EmpInfoId=@EmpInfoId AND YEAR(ActiveDate)=@Year AND FundId=@FundId  AND Status='Active'";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable EmpInformationDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT EG.EmpInfoId,EG.EmpMasterCode,EG.EmpName,EG.DesigId,DS.DesigName,EG.DepId,DE.DeptName,EG.SectionId,DVI.DivName,EG.DivisionId, 
                             SE.SectionName , G.GradeId,G.GradeType,EG.SalScaleId ,EG.CompanyInfoId,EG.UnitId,SE.SectionName,DVI.DivName
                             FROM dbo.tblEmpGeneralInfo EG 
                             LEFT JOIN dbo.tblDesignation DS ON EG.DesigId=DS.DesigId 
                             LEFT JOIN dbo.tblDepartment DE ON EG.DepId=DE.DeptId 
                             LEFT JOIN dbo.tblSection SE ON EG.SectionId=SE.SectionId 
                             LEFT JOIN dbo.tblEmployeeGrade G ON EG.EmpGradeId=G.GradeId 
                             LEFT JOIN dbo.tblCompanyInfo COM ON EG.CompanyInfoId=COM.CompanyInfoId 
                             LEFT JOIN dbo.tblCompanyUnit CU ON EG.UnitId=CU.UnitId 
                             LEFT JOIN dbo.tblDivision DVI ON EG.DivisionId=DVI.DivisionId 
                             LEFT JOIN dbo.tblSalaryGradeOrScale SG ON EG.SalScaleId=SG.SalScaleId
                             WHERE EG.EmpMasterCode='" + empId + "' and EG.PFEligibility= 'Yes' and EG.IsActive = 'True' and EG.ActionStatus = 'Accepted' ";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
            //,EG.SalaryType,SG.BasicSalary,SG.SalScaleName
        }
        public DataTable CheckFund(string empId,string fund)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * FROM dbo.tblFund WHERE EmpInfoId='"+empId+"' AND FundName='"+fund+"' AND Status='Active'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public Fund FundEditLoad(string fundId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FundId", fundId));
            string query = "select * from tblFund where FundId = @FundId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Fund afund = new Fund();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    afund.FundId = Int32.Parse(dataReader["FundId"].ToString());
                    afund.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    afund.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    afund.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    afund.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    afund.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    afund.FundName = dataReader["FundName"].ToString();
                    afund.Balance = Convert.ToDecimal(dataReader["Balance"].ToString());
                    afund.ActiveDate = Convert.ToDateTime(dataReader["ActiveDate"].ToString());

                }
            }
            return afund;
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * ,tblDesignation.DesigName,tblDepartment.DeptName,tblSection.SectionName
               FROM tblEmpGeneralInfo 
                 LEFT JOIN dbo.tblDesignation ON tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                 LEFT JOIN dbo.tblDepartment ON tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                 LEFT JOIN dbo.tblSection ON tblEmpGeneralInfo.SectionId = dbo.tblSection.SectionId
                 LEFT JOIN dbo.tblDivision ON tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                 WHERE EmpInfoId='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadFundView()
        {
            string query = @"SELECT  *,tblDesignation.DesigName,tblDepartment.DeptName,tblSection.SectionName,tblDivision.DivName
                 FROM tblFund 
                 LEFT JOIN tblEmpGeneralInfo ON tblFund.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN dbo.tblDesignation ON tblFund.DesigId = dbo.tblDesignation.DesigId
                 LEFT JOIN dbo.tblDepartment ON tblFund.DeptId = dbo.tblDepartment.DeptId
                 LEFT JOIN dbo.tblSection ON tblFund.SectionId = dbo.tblSection.SectionId
                 LEFT JOIN dbo.tblDivision ON tblFund.DivisionId = dbo.tblDivision.DivisionId
                 where tblFund.Status='Active' and FundName= 'Provident Fund' and tblFund.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblFund.FundId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void UpdateFund(string empId, DateTime inactiveDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@InactiveDate", inactiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));

            string updateQuery = @"UPDATE tblFund SET InactiveDate=@InactiveDate, Status='Inactive' WHERE EmpInfoId=@EmpInfoId and Status='active'";
            aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string PromotionId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblFund", "FundId", PromotionId);
        }
        public bool Updatefund(Fund aPromotion)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FundId", aPromotion.FundId));
            aSqlParameterlist.Add(new SqlParameter("@Balance", aPromotion.Balance));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aPromotion.ActiveDate));

            string query = @"UPDATE tblFund SET Balance=@Balance,ActiveDate=@ActiveDate WHERE FundId=@FundId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
