﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class GeneralDutyDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveDataForGeneralDuty(GeneralDuty aGeneralDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GDutyId", aGeneralDuty.GDutyId));
            //aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aGeneralDuty.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@GDDate", aGeneralDuty.GDDate));
            //aSqlParameterlist.Add(new SqlParameter("@DutyLocation", aGeneralDuty.DutyLocation));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aGeneralDuty.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aGeneralDuty.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aGeneralDuty.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aGeneralDuty.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aGeneralDuty.IsActive));

            string insertQuery = @"insert into tblGeneralDuty (GDutyId,GDDate,Purpose,ActionStatus,EntryDate,EntryUser,IsActive) 
            values (@GDutyId,@GDDate,@Purpose,@ActionStatus,@EntryDate,@EntryUser,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public DataTable LoadGeneralDutyViewForApproval(string actionstatus)
        {
            string query = @"SELECT * From tblGeneralDuty
                LEFT JOIN tblEmpGeneralInfo ON tblGeneralDuty.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblGeneralDuty.ActionStatus='" + actionstatus + "' AND tblGeneralDuty.IsActive=1   order by tblGeneralDuty.GDutyId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadGeneralDutyView()
        {
            string query = @"SELECT  dbo.tblGeneralDuty.ActionStatus ,
                ApprovedDate ,
                ApprovedUser ,
                dbo.tblGeneralDuty.IsActive ,
                DutyLocation ,
                dbo.tblGeneralDuty.DeleteDate ,
                dbo.tblGeneralDuty.EntryDate ,
                GDDate, EmpMasterCode ,
                EmpName,Purpose,* From tblGeneralDuty
                LEFT JOIN tblEmpGeneralInfo ON tblGeneralDuty.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblGeneralDuty.ActionStatus in ('Posted','Cancel') and tblGeneralDuty.IsActive=1 and tblGeneralDuty.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblGeneralDuty.GDutyId desc  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public GeneralDuty GeneralDutyEditLoad(string GDutyId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GDutyId", GDutyId));
            string query = "select * from tblGeneralDuty where GDutyId = @GDutyId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            GeneralDuty aGeneralDuty = new GeneralDuty();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aGeneralDuty.GDutyId = Int32.Parse(dataReader["GDutyId"].ToString());
                    aGeneralDuty.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aGeneralDuty.GDDate = Convert.ToDateTime(dataReader["GDDate"].ToString());
                    aGeneralDuty.DutyLocation = dataReader["DutyLocation"].ToString();
                    aGeneralDuty.Purpose = dataReader["Purpose"].ToString();
                    aGeneralDuty.ActionStatus = dataReader["ActionStatus"].ToString();
                    aGeneralDuty.ActionRemarks = dataReader["ActionRemarks"].ToString();
                    aGeneralDuty.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    
                    
                }
            }
            return aGeneralDuty;
        }

        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool UpdateGeneralDuty(GeneralDuty aGeneralDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GDutyId", aGeneralDuty.GDutyId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aGeneralDuty.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@GDDate", aGeneralDuty.GDDate));
            aSqlParameterlist.Add(new SqlParameter("@DutyLocation", aGeneralDuty.DutyLocation));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aGeneralDuty.Purpose));
            

            string query = @"UPDATE tblGeneralDuty SET EmpInfoId=@EmpInfoId,DutyLocation=@DutyLocation,GDDate=@GDDate,Purpose=@Purpose  WHERE GDutyId=@GDutyId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }


        public bool ApprovalUpdateDAL(GeneralDuty aGeneralDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GDutyId", aGeneralDuty.GDutyId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aGeneralDuty.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedUser", aGeneralDuty.ApprovedUser));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aGeneralDuty.ApprovedDate));

            string query = @"UPDATE tblGeneralDuty SET ActionStatus=@ActionStatus,ApprovedUser=@ApprovedUser,ApprovedDate=@ApprovedDate WHERE GDutyId=@GDutyId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");


        }
        public bool DeleteData(string GDutyId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblGeneralDuty", "GDutyId", GDutyId);
            
        }
    }
}
