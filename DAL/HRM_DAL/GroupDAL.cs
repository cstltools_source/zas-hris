﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class GroupDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveGroupInfo(Group aGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aGroup.EmpId));


            string insertQuery = @"insert into tblEmpWiseGroup (GroupId,EmpId) 
            values (@GroupId,@EmpId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasEmp(Group aGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aGroup.EmpId));
            string query = "select * from tblEmpWiseGroup where EmpId = @EmpId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadGroupView()
        {
            string query = @"SELECT * FROM tblEmpWiseGroup ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGroupReport(string groupId,string fromdt,string todt)
        {
            string query = @"SELECT EmpMasterCode,UnitName,EmpName,GroupName,DeptName,ShiftName,dbo.tblShift.ShiftInTime,dbo.tblShift.ShiftOutTime,FromDate,ToDate FROM dbo.tblEmpWiseGroup 
                            LEFT JOIN dbo.tblGroup ON dbo.tblEmpWiseGroup.GroupId = dbo.tblGroup.GroupId
                            LEFT JOIN dbo.tblCompanyUnit ON tblGroup.UnitId = dbo.tblCompanyUnit.UnitId
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblEmpWiseGroup.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId= dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblShiftWiseGroup ON dbo.tblEmpWiseGroup.GroupId=dbo.tblShiftWiseGroup.GroupId
                            LEFT JOIN dbo.tblShift ON dbo.tblShiftWiseGroup.ShiftId = dbo.tblShift.ShiftId    WHERE tblGroup.GroupId='" + groupId + "' and (FromDate BETWEEN '"+fromdt+"' AND '"+todt+"') AND (ToDate BETWEEN '"+fromdt+"' AND '"+todt+"')  ORDER BY  CONVERT(int , dbo.udf_GetNumeric(tblEmpGeneralInfo.EmpMasterCode) )  ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        
        }
        public DataTable LoadGroupReport(string fromdt,string todt)
        {
            string query = @"SELECT EmpMasterCode,UnitName,EmpName,GroupName,DeptName,ShiftName,dbo.tblShift.ShiftInTime,dbo.tblShift.ShiftOutTime,FromDate,ToDate FROM dbo.tblEmpWiseGroup 
                            LEFT JOIN dbo.tblGroup ON dbo.tblEmpWiseGroup.GroupId = dbo.tblGroup.GroupId
                            LEFT JOIN dbo.tblCompanyUnit ON tblGroup.UnitId = dbo.tblCompanyUnit.UnitId
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblEmpWiseGroup.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId= dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblShiftWiseGroup ON dbo.tblEmpWiseGroup.GroupId=dbo.tblShiftWiseGroup.GroupId
                            LEFT JOIN dbo.tblShift ON dbo.tblShiftWiseGroup.ShiftId = dbo.tblShift.ShiftId   WHERE  (FromDate BETWEEN '" + fromdt + "' AND '" + todt + "') AND (ToDate BETWEEN '" + fromdt + "' AND '" + todt + "')  ORDER BY  CONVERT(int , dbo.udf_GetNumeric(tblEmpGeneralInfo.EmpMasterCode) )  ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadGroupName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblGroup";
            aInternalDal.LoadDropDownValue(ddl, "GroupName", "GroupId", queryStr, "HRDB");
        }

        public DataTable LoadEmpInfo(string empcode)
        {
            string query = @"SELECT EmpMasterCode,EmpName,DeptName,EmpInfoId FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId WHERE EmpMasterCode='" + empcode + "' and ShiftEmp='Yes'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartmentWiseEmp(string deptid)
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                             WHERE  DepId='" + deptid + "' and EmployeeStatus='Active'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadSectionWiseEmp(string sectionId)
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
							left join tblSection on tblSection.SectionId=tblEmpGeneralInfo.SectionId
                            left join tblEmpWiseGroup on tblEmpGeneralInfo.EmpInfoId=tblEmpWiseGroup.EmpId
                            left join tblGroup on tblEmpWiseGroup.GroupId=tblGroup.GroupId
                             WHERE  tblEmpGeneralInfo.SectionId='" + sectionId + "' and EmployeeStatus='Active'  ORDER BY  CONVERT(int , dbo.udf_GetNumeric(tblEmpGeneralInfo.EmpMasterCode) )  ASC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool UpdateGroupInfo(Group aGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aGroup.EmpId));


            string query = @"UPDATE tblEmpWiseGroup SET  GroupId=@GroupId  WHERE EmpId=@EmpId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DelGroupEmpInfo(string empId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", empId));
            


            string query = @"DELETE FROM tblEmpWiseGroup WHERE  EmpId=@EmpId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public void LoadGroupByUnit(DropDownList ddl, string unitId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblGroup WHERE UnitId = " + unitId;
            aInternalDal.LoadDropDownValue(ddl, "GroupName", "GroupId", queryStr, "HRDB");
        }
    }
}
