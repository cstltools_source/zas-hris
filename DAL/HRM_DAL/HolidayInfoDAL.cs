﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class HolidayInfoDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForHolidayInfo(HolidayInfo aHolidayInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayId", aHolidayInfo.HolidayId));
            aSqlParameterlist.Add(new SqlParameter("@HolidayDate", aHolidayInfo.HolidayfromDate));
            aSqlParameterlist.Add(new SqlParameter("@Details", aHolidayInfo.Details));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aHolidayInfo.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aHolidayInfo.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", true));

            string insertQuery = @"insert into tblHolidayInformation (HolidayId,HolidayDate,Details,CompanyInfoId,UnitId,IsActive) 
            values (@HolidayId,@HolidayDate,@Details,@CompanyInfoId,@UnitId,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        
        public bool HasHolidayDate(HolidayInfo aHolidayInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayDate", aHolidayInfo.HolidayfromDate));
            string query = "select * from tblHolidayInformation where HolidayDate = @HolidayDate";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }                   
            }
            return false;
        }

        public void LoadUnitName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr,"HRDB");
        }

        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr,"HRDB");
        }

        public DataTable LoadHolidayView()
        {
            string query = @"SELECT tblHolidayInformation.HolidayId,tblHolidayInformation.HolidayDate,tblHolidayInformation.Details,tblCompanyUnit.UnitName,tblCompanyInfo.CompanyName FROM tblHolidayInformation " +
                            " LEFT JOIN tblCompanyUnit ON tblHolidayInformation.UnitId = tblCompanyUnit.UnitId "+
                            " LEFT JOIN tblCompanyInfo ON tblHolidayInformation.CompanyInfoId = tblCompanyInfo.CompanyInfoId WHERE Year(tblHolidayInformation.HolidayDate)='" + DateTime.Now.Year.ToString() + "' order by tblHolidayInformation.HolidayId  desc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public HolidayInfo HolidayInfoEditLoad(string holidayId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayId", holidayId));
            string query = "select * from tblHolidayInformation where HolidayId = @HolidayId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            HolidayInfo aHolidayInfo = new HolidayInfo();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aHolidayInfo.CompanyInfoId = Int32.Parse(dataReader["CompanyInfoId"].ToString());
                    aHolidayInfo.UnitId = Int32.Parse(dataReader["UnitId"].ToString());
                    aHolidayInfo.HolidayId = Int32.Parse(dataReader["HolidayId"].ToString());
                    aHolidayInfo.HolidayfromDate = Convert.ToDateTime(dataReader["HolidayDate"].ToString());
                    aHolidayInfo.Details = dataReader["Details"].ToString();
                    
                }

            }
            return aHolidayInfo;
        }
        public bool UpdateHolidayInfo(HolidayInfo aHolidayInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayId", aHolidayInfo.HolidayId));
            aSqlParameterlist.Add(new SqlParameter("@HolidayDate", aHolidayInfo.HolidayfromDate));
            aSqlParameterlist.Add(new SqlParameter("@Details", aHolidayInfo.Details));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aHolidayInfo.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aHolidayInfo.CompanyInfoId));

            string query = @"UPDATE tblHolidayInformation SET HolidayDate=@HolidayDate,Details=@Details,UnitId=@UnitId,CompanyInfoId=@CompanyInfoId WHERE HolidayId=@HolidayId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }

    }
}
