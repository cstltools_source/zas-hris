﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class HolidayReplaceDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();

        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }

        public bool SaveDataForHolidayReplace(HolidayReplace aHolidayReplace)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@WHRId", aHolidayReplace.WHRId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aHolidayReplace.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aHolidayReplace.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", aHolidayReplace.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@WeekHolidaydate", aHolidayReplace.WeekHolidaydate));
            aSqlParameterlist.Add(new SqlParameter("@WeekHolidayDayName", aHolidayReplace.WeekHolidayDayName));
            aSqlParameterlist.Add(new SqlParameter("@AlternativeDate", aHolidayReplace.AlternativeDate));
            aSqlParameterlist.Add(new SqlParameter("@AlternativeDayName", aHolidayReplace.AlternativeDayName));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aHolidayReplace.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aHolidayReplace.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aHolidayReplace.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aHolidayReplace.IsActive));

            string insertQuery = @"insert into dbo.tblWeeklyHolidayReplacement (WHRId,EmpInfoId,EmpMasterCode,EmpName,WeekHolidaydate,WeekHolidayDayName,AlternativeDate,AlternativeDayName,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@WHRId,@EmpInfoId,@EmpMasterCode,@EmpName,@WeekHolidaydate,@WeekHolidayDayName,@AlternativeDate,@AlternativeDayName,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasWeeklyHoliday(string dayname, string empid)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", empid));
            aSqlParameterlist.Add(new SqlParameter("@DayName", dayname));
            string query = "SELECT * FROM dbo.tblEmpWeeklyHoliday WHERE EmpId=@EmpId AND (FirstHolidayName=@DayName OR SecondHolidayName=@DayName)";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        //public bool HasHolidayDate(HolidayReplace aHolidayReplace)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@HolidayDate", aHolidayReplace.HolidayDate));
        //    string query = "select * from tblWeeklyHolidayReplacement where HolidayDate = @HolidayDate";
        //    IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        public DataTable LoadHolidayView()
        {
            string query = @"SELECT * FROM dbo.tblWeeklyHolidayReplacement where tblWeeklyHolidayReplacement.ActionStatus<>'Accepted' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public string DayName(string dayName)
        {
            string query = @"SELECT (DATENAME(dw,'"+dayName+"')) AS DayName";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString();
        }
        public HolidayReplace HolidayReplaceEditLoad(string WHRId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@WHRId", WHRId));
            string query = "select * from tblWeeklyHolidayReplacement where WHRId = @WHRId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            HolidayReplace aHolidayReplace = new HolidayReplace();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aHolidayReplace.EmpInfoId = Int32.Parse(dataReader["EmpInfoId"].ToString());
                    aHolidayReplace.WHRId = Int32.Parse(dataReader["WHRId"].ToString());
                    aHolidayReplace.EmpMasterCode = dataReader["EmpMasterCode"].ToString();
                    aHolidayReplace.EmpName = dataReader["EmpName"].ToString();
                    aHolidayReplace.WeekHolidaydate = Convert.ToDateTime(dataReader["WeekHolidaydate"].ToString());
                    aHolidayReplace.WeekHolidayDayName = dataReader["WeekHolidayDayName"].ToString();
                    aHolidayReplace.AlternativeDate = Convert.ToDateTime(dataReader["AlternativeDate"].ToString());
                    aHolidayReplace.AlternativeDayName = dataReader["AlternativeDayName"].ToString();
                    

                }

            }
            return aHolidayReplace;
        }
        public bool UpdateHolidayReplace(HolidayReplace aHolidayReplace)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@WHRId", aHolidayReplace.WHRId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aHolidayReplace.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aHolidayReplace.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", aHolidayReplace.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@WeekHolidaydate", aHolidayReplace.WeekHolidaydate));
            aSqlParameterlist.Add(new SqlParameter("@WeekHolidayDayName", aHolidayReplace.WeekHolidayDayName));
            aSqlParameterlist.Add(new SqlParameter("@AlternativeDate", aHolidayReplace.AlternativeDate));
            aSqlParameterlist.Add(new SqlParameter("@AlternativeDayName", aHolidayReplace.AlternativeDayName));
            

            string query = @"UPDATE tblWeeklyHolidayReplacement SET EmpInfoId=@EmpInfoId,EmpMasterCode=@EmpMasterCode,EmpName=@EmpName,WeekHolidaydate=@WeekHolidaydate,WeekHolidayDayName=@WeekHolidayDayName,AlternativeDate=@AlternativeDate,AlternativeDayName=@AlternativeDayName WHERE WHRId=@WHRId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =@"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool ApprovalUpdateDAL(HolidayReplace aHolidayReplace)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@WHRId", aHolidayReplace.WHRId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aHolidayReplace.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aHolidayReplace.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aHolidayReplace.ApprovedDate));

            string query = @"UPDATE tblWeeklyHolidayReplacement SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE WHRId=@WHRId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadHolidayReplaceViewApproval(string ActionStatus)
        {
            string query = @"SELECT * FROM tblWeeklyHolidayReplacement where tblWeeklyHolidayReplacement.ActionStatus='"+ActionStatus+"'  order by tblWeeklyHolidayReplacement.WHRId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteData(string WHRId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblWeeklyHolidayReplacement", "WHRId", WHRId);
            
        }
    }
}
