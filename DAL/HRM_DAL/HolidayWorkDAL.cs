﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class HolidayWorkDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();

        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveDataForHolidayWork(HolidayWork aHolidayWork)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayWorkId", aHolidayWork.HolidayWorkId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aHolidayWork.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@HworkDate", aHolidayWork.HworkDate));
            aSqlParameterlist.Add(new SqlParameter("@DutyLocation", aHolidayWork.DutyLocation));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aHolidayWork.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aHolidayWork.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ActionRemarks", aHolidayWork.ActionRemarks));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aHolidayWork.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aHolidayWork.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aHolidayWork.IsActive));

            string insertQuery = @"insert into tblHolidayWork (HolidayWorkId,EmpInfoId,HworkDate,Purpose,DutyLocation,ActionStatus,EntryDate,EntryUser,ActionRemarks,IsActive) 
            values (@HolidayWorkId,@EmpInfoId,@HworkDate,@Purpose,@DutyLocation,@ActionStatus,@EntryDate,@EntryUser,@ActionRemarks,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasHolidayDate(HolidayInfo aHolidayInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayDate", aHolidayInfo.HolidayfromDate));
            string query = "select * from tblHolidayInformation where HolidayDate = @HolidayDate";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public bool HasHolidayWork(HolidayWork aHolidayWork)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HworkDate", aHolidayWork.HworkDate));
            string query = "select * from tblHolidayWork where HworkDate = @HworkDate";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public bool HasAlterNativedate(string AlternativeDayName)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@AlternativeDayName", AlternativeDayName));
            string query = "select * from dbo.tblWeeklyHolidayReplacement where AlternativeDayName = @AlternativeDayName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public bool HasWeeklyHoliday(string  dayname,string empid)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", empid));
            aSqlParameterlist.Add(new SqlParameter("@DayName", dayname));
            string query = "SELECT * FROM dbo.tblEmpWeeklyHoliday WHERE EmpId=@EmpId AND (FirstHolidayName=@DayName OR SecondHolidayName=@DayName)";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadHolidayWorkViewForApproval(string actionstatus)
        {
            string query = @"SELECT *,DesigName,DeptName   From tblHolidayWork
                LEFT JOIN tblEmpGeneralInfo ON tblHolidayWork.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                LEFT JOIN tblDesignation ON tblEmpGeneralInfo.DesigId = tblDesignation.DesigId 
                LEFT JOIN tblDepartment ON tblEmpGeneralInfo.DepId = tblDepartment.DeptId 
                where tblHolidayWork.ActionStatus='" + actionstatus + "'   order by tblHolidayWork.HolidayWorkId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public string DayName(string date)
        {
            string query = "SELECT DATENAME(dw,'"+date+"') AS DayName";
            return (aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString());
        }
        public DataTable LoadHolidayWorkView()
        {
            string query = @"SELECT *,DesigName,DeptName  From tblHolidayWork
                LEFT JOIN tblEmpGeneralInfo ON tblHolidayWork.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                LEFT JOIN tblDesignation ON tblEmpGeneralInfo.DesigId = tblDesignation.DesigId 
                LEFT JOIN tblDepartment ON tblEmpGeneralInfo.DepId = tblDepartment.DeptId 
               where tblHolidayWork.ActionStatus in ('Posted','Cancel') and tblHolidayWork.IsActive=1 and tblHolidayWork.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblHolidayWork.HolidayWorkId desc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public HolidayWork HolidayWorkEditLoad(string HolidayWorkId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayWorkId", HolidayWorkId));
            string query = "select * from tblHolidayWork where HolidayWorkId = @HolidayWorkId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            HolidayWork aHolidayWork = new HolidayWork();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aHolidayWork.HolidayWorkId = Int32.Parse(dataReader["HolidayWorkId"].ToString());
                    aHolidayWork.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aHolidayWork.HworkDate = Convert.ToDateTime(dataReader["HworkDate"].ToString());
                    aHolidayWork.DutyLocation = dataReader["DutyLocation"].ToString();
                    aHolidayWork.Purpose = dataReader["Purpose"].ToString();
                    aHolidayWork.ActionRemarks = dataReader["ActionRemarks"].ToString();
                    aHolidayWork.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());


                }
            }
            return aHolidayWork;
        }

        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool UpdateHolidayWork(HolidayWork aHolidayWork)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayWorkId", aHolidayWork.HolidayWorkId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aHolidayWork.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@HworkDate", aHolidayWork.HworkDate));
            aSqlParameterlist.Add(new SqlParameter("@DutyLocation", aHolidayWork.DutyLocation));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aHolidayWork.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@ActionRemarks", aHolidayWork.ActionRemarks));

            string query = @"UPDATE tblHolidayWork SET EmpInfoId=@EmpInfoId,DutyLocation=@DutyLocation,HworkDate=@HworkDate,Purpose=@Purpose ,ActionRemarks=@ActionRemarks WHERE HolidayWorkId=@HolidayWorkId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }


        public bool ApprovalUpdateDAL(HolidayWork aHolidayWork)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@HolidayWorkId", aHolidayWork.HolidayWorkId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aHolidayWork.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedUser", aHolidayWork.ApprovedUser));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aHolidayWork.ApprovedDate));

            string query = @"UPDATE tblHolidayWork SET ActionStatus=@ActionStatus,ApprovedUser=@ApprovedUser,ApprovedDate=@ApprovedDate WHERE HolidayWorkId=@HolidayWorkId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string HolidayWorkId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblHolidayWork", "HolidayWorkId", HolidayWorkId);
            
        }
    }
}
