﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class JobDescriptionDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        
        public bool SaveJobDescreption(JobDescription aJobDescription)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@JobDscId", aJobDescription.JobDscId));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aJobDescription.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", aJobDescription.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aJobDescription.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aJobDescription.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@JoiningDate", aJobDescription.JoiningDate));
            aSqlParameterlist.Add(new SqlParameter("@ReportingTo", aJobDescription.ReportingTo));
            aSqlParameterlist.Add(new SqlParameter("@JobObjective", aJobDescription.JobObjective));
            aSqlParameterlist.Add(new SqlParameter("@DutiesTasks", aJobDescription.DutiesTasks));
            aSqlParameterlist.Add(new SqlParameter("@ImediateSupport", aJobDescription.ImediateSupport));
            aSqlParameterlist.Add(new SqlParameter("@KeyPerformArea", aJobDescription.KeyPerformArea));

            string insertQuery = @"insert into tblJobDescription (JobDscId,EmpMasterCode,EmpName,DesigId,DeptId,JoiningDate,ReportingTo,JobObjective,DutiesTasks,ImediateSupport,KeyPerformArea) 
            values (@JobDscId,@EmpMasterCode,@EmpName,@DesigId,@DeptId,@JoiningDate,@ReportingTo,@JobObjective,@DutiesTasks,@ImediateSupport,@KeyPerformArea)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public DataTable LoadJobDescriptionView()
        {
            string query =
                @"SELECT tblJobDescription.JobDscId, tblJobDescription.EmpMasterCode,tblJobDescription.EmpName,tblDesignation.DesigName,tblDepartment.DeptName,tblJobDescription.JoiningDate,tblJobDescription.ReportingTo,tblJobDescription.JobObjective,tblJobDescription.DutiesTasks,tblJobDescription.ImediateSupport,tblJobDescription.KeyPerformArea FROM tblJobDescription " +
                " LEFT JOIN tblDesignation ON tblJobDescription.DesigId = tblDesignation.DesigId  " +
                " LEFT JOIN tblDepartment ON tblJobDescription.DeptId = tblDepartment.DeptId   order by tblJobDescription.JobDscId desc ";
                
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }

        public DataTable LoadEmpInfoCode(string empcode)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + empcode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public JobDescription JobDescriptionEditLoad(string JobDscId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@JobDscId", JobDscId));
            string query = "select * from tblJobDescription where JobDscId = @JobDscId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            JobDescription aJobDescription = new JobDescription();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aJobDescription.JobDscId = Int32.Parse(dataReader["JobDscId"].ToString());
                    aJobDescription.EmpMasterCode = dataReader["EmpMasterCode"].ToString();
                    aJobDescription.EmpName = dataReader["EmpName"].ToString();
                    aJobDescription.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aJobDescription.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aJobDescription.JoiningDate = Convert.ToDateTime(dataReader["JoiningDate"].ToString());
                    aJobDescription.ReportingTo = dataReader["ReportingTo"].ToString();
                    aJobDescription.JobObjective = dataReader["JobObjective"].ToString();
                    aJobDescription.DutiesTasks = dataReader["DutiesTasks"].ToString();
                    aJobDescription.ImediateSupport = dataReader["ImediateSupport"].ToString();
                    aJobDescription.KeyPerformArea = dataReader["KeyPerformArea"].ToString();
                    
                }
            }
            return aJobDescription;
        }

        public bool UpdateJobDescription(JobDescription aJobDescription)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@JobDscId", aJobDescription.JobDscId));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aJobDescription.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", aJobDescription.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aJobDescription.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aJobDescription.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@JoiningDate", aJobDescription.JoiningDate));
            aSqlParameterlist.Add(new SqlParameter("@ReportingTo", aJobDescription.ReportingTo));
            aSqlParameterlist.Add(new SqlParameter("@JobObjective", aJobDescription.JobObjective));
            aSqlParameterlist.Add(new SqlParameter("@DutiesTasks", aJobDescription.DutiesTasks));
            aSqlParameterlist.Add(new SqlParameter("@ImediateSupport", aJobDescription.ImediateSupport));
            aSqlParameterlist.Add(new SqlParameter("@KeyPerformArea", aJobDescription.KeyPerformArea));

            string query = @"UPDATE tblJobDescription SET EmpName=@EmpName,EmpMasterCode=@EmpMasterCode,DesigId=@DesigId,DeptId=@DeptId,JoiningDate=@JoiningDate,ReportingTo=@ReportingTo," +
                           "JobObjective=@JobObjective,DutiesTasks=@DutiesTasks,ImediateSupport=@ImediateSupport,KeyPerformArea=@KeyPerformArea WHERE JobDscId=@JobDscId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string HolidayWorkId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblJobDescription", "JobDscId", HolidayWorkId);
            
        }
    }
}
