﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class JobLeftDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveJobleft(JobLeft aJobLeft)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@JobLeftId", aJobLeft.JobLeftId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aJobLeft.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aJobLeft.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aJobLeft.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aJobLeft.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aJobLeft.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aJobLeft.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aJobLeft.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aJobLeft.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aJobLeft.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aJobLeft.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aJobLeft.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aJobLeft.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aJobLeft.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@WorkYear", aJobLeft.WorkYear));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aJobLeft.IsActive));


            string insertQuery = @"insert into tblJobleft (JobLeftId,EmpInfoId,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,GradeId,EffectiveDate,ActionStatus,EntryBy,EntryDate,WorkYear,IsActive) 
            values (@JobLeftId,@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EffectiveDate,@ActionStatus,@EntryBy,@EntryDate,@WorkYear,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasJobleft(JobLeft aJobLeft)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aJobLeft.EmpInfoId));
            string query = "select * from tblJobleft where EmpInfoId = @EmpInfoId AND ActionStatus='Accepted'";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadJobleftView()
        {
            string query =
                @"SELECT * FROM tblJobleft " +
                " LEFT JOIN tblEmpGeneralInfo ON tblJobleft.EmpInfoId = tblEmpGeneralInfo.EmpInfoId  " +
                " LEFT JOIN tblCompanyInfo ON tblJobleft.CompanyInfoId = tblCompanyInfo.CompanyInfoId  " +
                " LEFT JOIN tblCompanyUnit ON tblJobleft.UnitId = tblCompanyUnit.UnitId  " +
                " LEFT JOIN tblDivision ON tblJobleft.DivisionId = tblDivision.DivisionId  " +
                " LEFT JOIN tblSection ON tblJobleft.SectionId = tblSection.SectionId  " +
                " LEFT JOIN tblEmployeeGrade ON tblJobleft.GradeId = tblEmployeeGrade.GradeId  " +
                " LEFT JOIN tblEmployeeType ON tblJobleft.EmpTypeId = tblEmployeeType.EmpTypeId  " +
                " LEFT JOIN tblDesignation ON tblJobleft.DesigId = tblDesignation.DesigId  " +
                " LEFT JOIN tblDepartment ON tblJobleft.DeptId = tblDepartment.DeptId where tblJobleft.ActionStatus in ('Posted','Cancel') and tblJobleft.IsActive=1 and tblJobleft.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblJobleft.JobleftId desc ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }
        
        public void LoadDepartmentName(DropDownList ddl,string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='"+divisionId+"'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl,string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl,string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='"+deptId+"'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadEmpGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from dbo.tblEmployeeGrade";
            aInternalDal.LoadDropDownValue(ddl, "GradeName", "GradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName ,JoiningDate FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public JobLeft EmpJobleftEditLoad(string JobLeftId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@JobLeftId", JobLeftId));
            string query = "select * from tblJobleft where JobLeftId = @JobLeftId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            JobLeft aJobLeft = new JobLeft();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aJobLeft.JobLeftId = Int32.Parse(dataReader["JobLeftId"].ToString());
                    aJobLeft.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aJobLeft.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aJobLeft.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aJobLeft.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aJobLeft.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aJobLeft.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aJobLeft.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aJobLeft.GradeId = Convert.ToInt32(dataReader["GradeId"].ToString());
                    aJobLeft.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aJobLeft.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    aJobLeft.ActionStatus = dataReader["ActionStatus"].ToString();
                    aJobLeft.EntryBy = dataReader["EntryBy"].ToString();
                    aJobLeft.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                   // aJobLeft.Status = dataReader["BenefitType"].ToString();
                    aJobLeft.ApprovedBy = dataReader["ApprovedBy"].ToString();
                  //  aJobLeft.ApprovedDate = Convert.ToDateTime(dataReader["ApprovedDate"].ToString());
                  //  aJobLeft.RelesedOn = Convert.ToDateTime(dataReader["RelesedOn"].ToString());
                }
            }
            return aJobLeft;
        }

        public bool UpdateJobleft(JobLeft aJobLeft)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@JobLeftId", aJobLeft.JobLeftId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aJobLeft.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aJobLeft.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aJobLeft.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aJobLeft.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aJobLeft.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aJobLeft.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aJobLeft.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aJobLeft.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aJobLeft.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aJobLeft.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aJobLeft.ActionStatus));
            
            aSqlParameterlist.Add(new SqlParameter("@Status", aJobLeft.Status));
            

            string query = @"UPDATE tblJobleft SET CompanyInfoId=@CompanyInfoId,EmpInfoId=@EmpInfoId,DesigId=@DesigId,DeptId=@DeptId,UnitId=@UnitId," +
                            "SectionId=@SectionId,GradeId=@GradeId,Status=@Status,EmpTypeId=@EmpTypeId,EffectiveDate=@EffectiveDate,DivisionId=@DivisionId " +
                           " WHERE JobLeftId=@JobLeftId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }

        public bool ApprovalUpdateDAL(JobLeft aJobLeft)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@JobleftId", aJobLeft.JobLeftId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aJobLeft.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aJobLeft.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aJobLeft.ApprovedDate));

            string query = @"UPDATE tblJobLeft SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE JobleftId=@JobleftId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));
           
            aSqlParameterlist.Add(new SqlParameter("@InactiveReason", "JobLeft"));
            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus,InactiveReason=@InactiveReason WHERE EmpInfoId=@EmpInfoId"; 
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadJobLeftViewForApproval(string ActionStatus)
        {
            string query = @"SELECT *,DesigName, DeptName From dbo.tblJobLeft
                LEFT JOIN tblEmpGeneralInfo ON tblJobLeft.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                where tblJobLeft.ActionStatus='" + ActionStatus + "'  order by tblJobLeft.JobleftId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB"); 
        }
        public bool DeleteData(string HolidayWorkId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblJobLeft", "JobleftId", HolidayWorkId); 
        }
    }
}

