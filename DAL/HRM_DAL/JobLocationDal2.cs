﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO.HRM_Entities;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace DAL.HRM_DAL
{
    public class JobLocationDal2
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDevision(JobLocationDao2 aDivision)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDivision.JobLocationisionId));
            aSqlParameterlist.Add(new SqlParameter("@DivCode", aDivision.JobLocationCode));
            aSqlParameterlist.Add(new SqlParameter("@DivName", aDivision.JobLocationName));
            aSqlParameterlist.Add(new SqlParameter("@DivShortName", aDivision.JobLocationShortName));

            string insertQuery = @"insert into tblJobLocation (JobLocationisionId,JobLocationCode,JobLocationName,JobLocationShortName) 
            values (@DivisionId,@DivCode,@DivName,@DivShortName)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasDivisionName(JobLocationDao2 aDivision)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivName", aDivision.JobLocationName));
            string query = "select * from tblJobLocation where JobLocationName = @DivName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadDivisionView()
        {
            string query = @"SELECT * from tblJobLocation";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Division DivisionEditLoad(string DivisionId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", DivisionId));
            string query = "select * from tblJobLocation where JobLocationisionId = @DivisionId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            Division aDivision = new Division();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aDivision.DivisionId = Int32.Parse(dataReader["JobLocationisionId"].ToString());
                    aDivision.DivCode = dataReader["JobLocationCode"].ToString();
                    aDivision.DivName = dataReader["JobLocationName"].ToString();
                    aDivision.DivShortName = dataReader["JobLocationShortName"].ToString();
                }
            }
            return aDivision;
        }

        public bool UpdateDivision(JobLocationDao2 aDivision)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();

            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aDivision.JobLocationisionId));
            aSqlParameterlist.Add(new SqlParameter("@DivName", aDivision.JobLocationName));
            aSqlParameterlist.Add(new SqlParameter("@DivShortName", aDivision.JobLocationShortName));

            string query = @"UPDATE tblJobLocation SET JobLocationName=@DivName,JobLocationShortName=@DivShortName WHERE JobLocationisionId=@DivisionId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteDivisionInfo(string DivisionId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", DivisionId));

            string query = @"DELETE FROM dbo.tblJobLocation WHERE JobLocationisionId=@DivisionId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
