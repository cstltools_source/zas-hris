﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class LateApproveDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }

        public bool SaveLateApprove(LateApprove aLateApprove)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LateApproveId", aLateApprove.LateApproveId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLateApprove.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftIntime", aLateApprove.ShiftIntime));
            aSqlParameterlist.Add(new SqlParameter("@AttIntime", aLateApprove.AttIntime));
            aSqlParameterlist.Add(new SqlParameter("@LateIntime", aLateApprove.LateIntime));
            aSqlParameterlist.Add(new SqlParameter("@LateReason", aLateApprove.LateReason));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aLateApprove.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aLateApprove.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aLateApprove.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aLateApprove.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aLateApprove.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aLateApprove.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aLateApprove.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aLateApprove.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aLateApprove.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aLateApprove.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aLateApprove.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aLateApprove.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@LateDate", aLateApprove.LateDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aLateApprove.IsActive));

            string insertQuery = @"insert into tblLateApprove (LateApproveId,EmpInfoId,ShiftIntime,AttIntime,LateIntime,LateReason,EffectiveDate,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EntryUser,EntryDate,ActionStatus,LateDate,IsActive) 
            values (@LateApproveId,@EmpInfoId,@ShiftIntime,@AttIntime,@LateIntime,@LateReason,@EffectiveDate,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EntryUser,@EntryDate,@ActionStatus,@LateDate,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasLateApprove(LateApprove aLateApprove)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLateApprove.EmpInfoId));
            string query = "select * from tblLateApprove where EmpInfoId=@EmpInfoId and LateDate= '" + aLateApprove.LateDate + "' AND ActionStatus='Accepted' ";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadLateApproveView()
        {
            string query = @"SELECT  LateApproveId,EmpMasterCode ,EmpName , EffectiveDate , ShiftIntime,AttIntime,LateIntime,EffectiveDate,DesigName,DeptName,tblLateApprove.ActionStatus,EntryUser,tblLateApprove.EntryDate FROM tblLateApprove 
                             LEFT JOIN tblEmpGeneralInfo ON tblLateApprove.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                             LEFT JOIN tblCompanyInfo ON tblLateApprove.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                             LEFT JOIN tblCompanyUnit ON tblLateApprove.UnitId = tblCompanyUnit.UnitId 
                             LEFT JOIN tblDivision ON tblLateApprove.DivisionId = tblDivision.DivisionId  
                             LEFT JOIN tblSection ON tblLateApprove.SectionId = tblSection.SectionId  
                             LEFT JOIN tblEmployeeGrade ON tblLateApprove.EmpGradeId = tblEmployeeGrade.GradeId  
                             LEFT JOIN tblEmployeeType ON tblLateApprove.EmpTypeId = tblEmployeeType.EmpTypeId  
                             LEFT JOIN tblDesignation ON tblLateApprove.DesigId = tblDesignation.DesigId  
                             LEFT JOIN tblDepartment ON tblLateApprove.DeptId = tblDepartment.DeptId where tblLateApprove.ActionStatus in ('Posted','Cancel') and tblLateApprove.IsActive=1 and tblLateApprove.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblLateApprove.LateApproveId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation ORDER BY DesigName";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "' ORDER BY DeptName ";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo ORDER BY EmployeeName ";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "' ORDER BY UnitName ";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision ORDER BY DivName ";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "' ORDER BY SectionName ";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale ORDER BY SalGradeName ";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType ORDER BY EmpType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLateTime(DateTime date,string empid)
        {
            string query = @"SELECT * FROM dbo.tblAttendanceRecord WHERE ATTDate='"+date+"' AND EmpId='"+empid+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public LateApprove LateApproveEditLoad(string LateApproveId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LateApproveId", LateApproveId));
            string query = "select * from tblLateApprove where LateApproveId = @LateApproveId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            LateApprove aLateApprove = new LateApprove();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aLateApprove.LateApproveId = Int32.Parse(dataReader["LateApproveId"].ToString());
                    aLateApprove.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aLateApprove.ShiftIntime = Convert.ToDateTime((dataReader["ShiftIntime"].ToString())).TimeOfDay;
                    aLateApprove.AttIntime = Convert.ToDateTime((dataReader["AttIntime"].ToString())).TimeOfDay;
                    aLateApprove.LateIntime = Convert.ToDateTime((dataReader["LateIntime"].ToString())).TimeOfDay;
                    aLateApprove.LateReason = dataReader["LateReason"].ToString();
                    aLateApprove.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aLateApprove.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aLateApprove.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aLateApprove.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aLateApprove.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aLateApprove.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aLateApprove.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aLateApprove.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aLateApprove.ActionStatus = dataReader["ActionStatus"].ToString();
                    aLateApprove.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    aLateApprove.LateDate = Convert.ToDateTime(dataReader["LateDate"].ToString());

                }
            }
            return aLateApprove;
        }

        public bool UpdateLateApprove(LateApprove aLateApprove)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LateApproveId", aLateApprove.LateApproveId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLateApprove.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftIntime", aLateApprove.ShiftIntime));
            aSqlParameterlist.Add(new SqlParameter("@AttIntime", aLateApprove.AttIntime));
            aSqlParameterlist.Add(new SqlParameter("@LateIntime", aLateApprove.LateIntime));
            aSqlParameterlist.Add(new SqlParameter("@LateReason", aLateApprove.LateReason));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aLateApprove.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aLateApprove.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aLateApprove.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aLateApprove.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aLateApprove.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aLateApprove.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aLateApprove.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aLateApprove.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aLateApprove.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@LateDate", aLateApprove.LateDate));

            string query = @"UPDATE tblLateApprove SET EmpInfoId=@EmpInfoId,ShiftIntime=@ShiftIntime,AttIntime=@AttIntime,LateIntime=@LateIntime,LateReason=@LateReason,EffectiveDate=@EffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId,LateDate=@LateDate WHERE LateApproveId=@LateApproveId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(LateApprove aLateApprove)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LateApproveId", aLateApprove.LateApproveId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aLateApprove.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aLateApprove.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aLateApprove.ApprovedDate));

            string query = @"UPDATE tblLateApprove SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE LateApproveId=@LateApproveId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadLateApproveViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From tblLateApprove
                LEFT JOIN tblEmpGeneralInfo ON tblLateApprove.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                  
                where tblLateApprove.ActionStatus='" + ActionStatus + "' AND tblLateApprove.IsActive=1  order by tblLateApprove.LateApproveId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceLate(string empid,DateTime date,string status)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", empid));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", date));
            aSqlParameterlist.Add(new SqlParameter("@ATTStatus", status));

            string query = @"UPDATE dbo.tblAttendanceRecord SET ATTStatus=@ATTStatus,Remarks='Present,LA' WHERE EmpId=@EmpId and ATTDate=@ATTDate";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string HolidayWorkId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblLateApprove", "LateApproveId", HolidayWorkId);
            
        }
        public DataTable VarifyLate(string attdate, string empid)
        {
            string query = @"SELECT * From tblAttendanceRecord
             where ATTDate='" + attdate + "' AND ATTStatus='L'  AND EmpId = '" + empid + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
