﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;
using Microsoft.SqlServer.Server;

namespace Library.DAL.HRM_DAL
{
    public  class LeaveAvailDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveLeaveAvail(LeaveAvail avail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveAvailId", avail.LeaveAvailId));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", avail.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", avail.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@LeaveInventoryId", avail.LeaveInventoryId));
            aSqlParameterlist.Add(new SqlParameter("@FromDate", avail.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", avail.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", avail.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@AvailLeaveQty", avail.AvailLeaveQty));
            aSqlParameterlist.Add(new SqlParameter("@LeaveReason", avail.LeaveReason));
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", avail.LeaveName));
            aSqlParameterlist.Add(new SqlParameter("@Status", avail.Status));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", avail.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", avail.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", avail.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", avail.ActionStatus));

            string insertQuery = @"insert into tblLeaveAvail (LeaveAvailId,EmpMasterCode,EmpName,LeaveInventoryId,FromDate,ToDate,EmpInfoId,AvailLeaveQty,LeaveReason,LeaveName,Status,EntryBy,EntryDate,IsActive,ActionStatus) 
            values (@LeaveAvailId,@EmpMasterCode,@EmpName,@LeaveInventoryId,@FromDate,@ToDate,@EmpInfoId,@AvailLeaveQty,@LeaveReason,@LeaveName,@Status,@EntryBy,@EntryDate,@IsActive,@ActionStatus)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public DataTable LoadLeaveEmployeeLeaveQuantity(string empcode)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpMasterCode,EmpName,JoiningDate,DesigName,DeptName,
                        ISNULL(tblLeaveMain.YearCasual,0) AS YearCasual ,
                        ISNULL(tblLeaveMain.CasualRemain,0) AS CasualRemain,
                        ISNULL(tblLeaveMain.YearMedical,0) AS YearMedical,
                        ISNULL(tblLeaveMain.MedicalRemain,0) AS MedicalRemain,
                        ISNULL(tblLeaveMain.YearEarn,0) AS YearEarn,
                        ISNULL(tblLeaveMain.EarnRemain,0) AS EarnRemain,
                        ISNULL(tblLeaveMain.CasualEnjoyed,0)AS CasualEnjoyed,
                        ISNULL(tblLeaveMain.MedicalEnjoyed,0)AS MedicalEnjoyed,
                        ISNULL(tblLeaveMain.EarnEnjoyed,0)AS EarnEnjoyed,
                        (CASE WHEN ISNULL(tblLeaveMain.YearCasual,0)=0 THEN 'Leave Not Define' ELSE 'ok' END) AS Remarks


                        FROM dbo.tblEmpGeneralInfo
                        LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                        LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                        LEFT JOIN 

                        (SELECT tblMainLeave.EmpMasterCode,
                        ISNULL(SUM(tblMainLeave.YearCasual),0) AS YearCasual
                        ,ISNULL(SUM(tblMainLeave.CasualRemain),0) AS CasualRemain,

                        (ISNULL(SUM(tblMainLeave.YearCasual),0)-ISNULL(SUM(tblMainLeave.CasualRemain),0)) AS CasualEnjoyed
                        ,ISNULL(SUM(tblMainLeave.YearMedical),0) AS YearMedical
                        ,ISNULL(SUM(tblMainLeave.MedicalRemain),0) AS MedicalRemain,
                        (ISNULL(SUM(tblMainLeave.YearMedical),0)-ISNULL(SUM(tblMainLeave.MedicalRemain),0)) AS MedicalEnjoyed
                        ,ISNULL(SUM(tblMainLeave.YearEarn),0) AS YearEarn
                        ,ISNULL(SUM(tblMainLeave.EarnRemain),0) AS EarnRemain 
                        ,(ISNULL(SUM(tblMainLeave.YearEarn),0)-ISNULL(SUM(tblMainLeave.EarnRemain),0)) AS EarnEnjoyed

                         FROM(SELECT EmpMasterCode,(CASE WHEN LeaveName='Casual' THEN YearDayQty END) AS YearCasual,(CASE WHEN LeaveName='Casual' THEN DayQty END) AS CasualRemain,
                        (CASE WHEN LeaveName='Medical' THEN YearDayQty END) AS YearMedical,(CASE WHEN LeaveName='Medical' THEN DayQty END) AS MedicalRemain,(CASE WHEN LeaveName='Earn' THEN YearDayQty END) AS YearEarn,
                        (CASE WHEN LeaveName='Earn' THEN DayQty END) AS EarnRemain

                         FROM dbo.tblLeaveInventory WHERE LeaveYear='2015') AS tblMainLeave 
                         GROUP BY tblMainLeave.EmpMasterCode) AS tblLeaveMain ON dbo.tblEmpGeneralInfo.EmpMasterCode=tblLeaveMain.EmpMasterCode
                          WHERE dbo.tblEmpGeneralInfo.EmployeeStatus='Active' AND tblEmpGeneralInfo.IsActive=1 AND dbo.tblEmpGeneralInfo.EmpMasterCode='" + empcode + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLeaveAvailView()
        {
            string query = @"SELECT * FROM tblLeaveAvail where tblLeaveAvail.ActionStatus in ('Posted','Cancel') and tblLeaveAvail.IsActive=1 and tblLeaveAvail.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblLeaveAvail.LeaveAvailId desc"; 
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLeave(string fromdate,string empInfoId)
        {
            string query = @"SELECT * FROM dbo.tblLeaveAvail WHERE ActionStatus IN ('Accepted','Posted') AND '" + fromdate + "' BETWEEN FromDate AND ToDate AND EmpInfoId='"+empInfoId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLeaveAvailView(string fromdate,string todate)
        {
            string query = @"SELECT * FROM dbo.tblLeaveAvail
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLeaveAvail.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId WHERE tblLeaveAvail.EntryDate BETWEEN '" + fromdate + "' AND '" + todate + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public LeaveAvail LeaveAvailEditLoad(string leaveId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveId", leaveId));
            string query = "select * from tblLeaveAvail where LeaveAvailId = @LeaveId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            LeaveAvail avail = new LeaveAvail();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    avail.EmpInfoId = Int32.Parse(dataReader["EmpInfoId"].ToString());
                    avail.EmpMasterCode = dataReader["EmpMasterCode"].ToString();
                    avail.EmpName = dataReader["EmpName"].ToString();
                    avail.LeaveInventoryId = Convert.ToInt32(dataReader["LeaveInventoryId"].ToString());
                    avail.FromDate = Convert.ToDateTime(dataReader["FromDate"].ToString());
                    avail.ToDate = Convert.ToDateTime(dataReader["ToDate"].ToString());
                    avail.AvailLeaveQty = dataReader["AvailLeaveQty"].ToString();
                    avail.LeaveReason = dataReader["LeaveReason"].ToString();

                }
            }
            return avail;
        }

        public bool UpdateLeaveInventory(LeaveInventory aInventory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveInventoryId", aInventory.LeaveInventoryId));
            aSqlParameterlist.Add(new SqlParameter("@DayQty", aInventory.DayQty));
            string query = @"UPDATE dbo.tblLeaveInventory SET DayQty='" + aInventory.DayQty + "' WHERE LeaveInventoryId=" + aInventory.LeaveInventoryId + "";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool UpdateLeaveAvailInfo(LeaveAvail avail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveAvailId", avail.LeaveAvailId));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", avail.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", avail.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@LeaveInventoryId", avail.LeaveInventoryId));
            aSqlParameterlist.Add(new SqlParameter("@FromDate", avail.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", avail.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", avail.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@AvailLeaveQty", avail.AvailLeaveQty));
            aSqlParameterlist.Add(new SqlParameter("@LeaveReason", avail.LeaveReason));
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", avail.LeaveName));
            
            string query = @"UPDATE tblLeaveAvail SET EmpMasterCode=@EmpMasterCode,EmpName=@EmpName,LeaveInventoryId=@LeaveInventoryId,FromDate=@FromDate,ToDate=@ToDate,EmpInfoId=@EmpInfoId,AvailLeaveQty=@AvailLeaveQty,LeaveReason=@LeaveReason,LeaveName=@LeaveName WHERE LeaveAvailId=@LeaveAvailId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB"); 
        }
        public void LoadLeaveName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblLeaveInventory";
            aInternalDal.LoadDropDownValue(ddl, "LeaveName", "LeaveInventoryId", queryStr, "HRDB");
        }
        public void LoadLeaveName(DropDownList ddl,string empId, string year)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblLeaveInventory where EmpInfoId='" + empId.Trim() + "' and LeaveYear = '" + year.Trim() + "'ORDER BY LeaveName";
            aInternalDal.LoadDropDownValue(ddl, "LeaveName", "LeaveId", queryStr, "HRDB");
        }
        public DataTable LeaveQtyCheck(string leaveId, string empId, string year)
        {
            DataTable aTableLeave = new DataTable();
            string queryStr = "select * from tblLeaveInventory where EmpInfoId='" + empId.Trim() + "' and LeaveYear = '" + year.Trim() + "' and LeaveId='" + leaveId.Trim() + "'";
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            aTableLeave = aInternalDal.DataContainerDataTable(queryStr, "HRDB");
            return aTableLeave;
        }
        public DataTable DeptHead(string empinfoId)
        {
            string queryStr = @"SELECT tblDeptHeadSetup.EmpInfoId,tblDeptHeadSetup.DeptId,EmpMasterCode FROM dbo.tblDeptHeadSetup 
                                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblDeptHeadSetup.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                                WHERE EmpMasterCode='"+empinfoId+"'";
            return aCommonInternalDal.DataContainerDataTable(queryStr, "HRDB");
        }

        public DataTable CheckGovtHoliday(string fromdate, string todate, string empinfoId)
        {
            string query = @"select * from tblHolidayInformation where HolidayDate between '" + fromdate + "' and '" + todate + "' and IsActive='1' and UnitId in (select UnitId from tblEmpGeneralInfo where EmpInfoId='" + empinfoId + "')";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable CheckWeekHoliday( string empinfoId,string dayname)
        {
            string query = @"select * from tblEmpWeeklyHoliday where EmpId='" + empinfoId + "' AND FirstHolidayName='"+dayname+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetInventory(string leaveInventoryId)
        {
            string queryStr = @"SELECT * FROM dbo.tblLeaveInventory WHERE LeaveInventoryId='"+leaveInventoryId+"'";
            return aCommonInternalDal.DataContainerDataTable(queryStr, "HRDB");
        }
        public DataTable LeaveInfo(string empinfoId)
        {
            string queryStr = @"SELECT LeaveName,DayQty,(YearDayQty-DayQty)AS LeaveEnjoy, YearDayQty FROM dbo.tblLeaveInventory WHERE EmpInfoId='"+empinfoId+"'";
            return aCommonInternalDal.DataContainerDataTable(queryStr, "HRDB");
        }
        public bool ApprovalUpdateDAL(LeaveAvail aLeaveAvail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveAvailId", aLeaveAvail.LeaveAvailId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aLeaveAvail.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aLeaveAvail.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aLeaveAvail.ApprovedDate));
            aSqlParameterlist.Add(new SqlParameter("@FromDate", aLeaveAvail.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", aLeaveAvail.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@AvailLeaveQty", aLeaveAvail.AvailLeaveQty));

            string query = @"UPDATE tblLeaveAvail SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate,ToDate=@ToDate,FromDate=@FromDate,AvailLeaveQty=@AvailLeaveQty WHERE LeaveAvailId=@LeaveAvailId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadLeaveAvailViewApproval(string ActionStatus)
        {
            string query = @"SELECT  LeaveAvailId,dbo.tblLeaveAvail.EmpInfoId ,
                            dbo.tblLeaveAvail.EmpMasterCode ,
                            dbo.tblLeaveAvail.EmpName ,
                            dbo.tblLeaveAvail.EntryBy ,
                            dbo.tblLeaveAvail.EntryDate ,
                            AvailLeaveQty ,
                            DeptName ,
                            DeptShortName ,
                            DesigName ,
                            FromDate ,
                            dbo.tblLeaveAvail.LeaveInventoryId ,
                            dbo.tblLeaveAvail.LeaveName ,
                            LeaveReason ,
                            LeaveYear ,
                            SectionName ,
                            SectionShortName ,
                            ToDate ,
                            YearDayQty,DayQty FROM tblLeaveAvail 
            LEFT JOIN tblEmpGeneralInfo ON tblLeaveAvail.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId 
            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId 
            LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId 
            LEFT JOIN dbo.tblLeaveInventory ON dbo.tblLeaveAvail.LeaveInventoryId=dbo.tblLeaveInventory.LeaveInventoryId where tblLeaveAvail.ActionStatus='" + ActionStatus + "' AND tblLeaveAvail.IsActive=1 order by tblLeaveAvail.LeaveAvailId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable DeptHeadLoadLeaveAvailViewApproval(string ActionStatus,string deptId)
        {
            string query = @"SELECT dbo.tblLeaveAvail.LeaveAvailId,dbo.tblLeaveAvail.EmpInfoId ,
                            dbo.tblLeaveAvail.EmpMasterCode ,
                            dbo.tblLeaveAvail.EmpName ,
                            dbo.tblLeaveAvail.EntryBy ,
                            dbo.tblLeaveAvail.EntryDate ,
                            AvailLeaveQty ,
                            DeptName ,
                            DeptShortName ,
                            DesigName ,
                            CONVERT(NVARCHAR(11),FromDate,106) AS FromDate,
                            dbo.tblLeaveAvail.LeaveInventoryId ,
                            dbo.tblLeaveAvail.LeaveName ,
                            LeaveReason ,
                            LeaveYear ,
                            SectionName ,
                            SectionShortName ,
                            CONVERT(NVARCHAR(11),ToDate,106) AS ToDate,
                            YearDayQty,DayQty FROM tblLeaveAvail 
            LEFT JOIN tblEmpGeneralInfo ON tblLeaveAvail.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId 
            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId 
            LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId 
            LEFT JOIN dbo.tblLeaveInventory ON dbo.tblLeaveAvail.LeaveInventoryId=dbo.tblLeaveInventory.LeaveInventoryId where tblLeaveAvail.ActionStatus='" + ActionStatus + "' and  dbo.tblDepartment.DeptId='" + deptId + "' AND  tblLeaveAvail.IsActive=1  order by tblLeaveAvail.LeaveAvailId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool DeleteData(string LMID)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblLeaveAvail", "LeaveAvailId", LMID);
            
        }
    }
}
