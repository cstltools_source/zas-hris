﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class LeaveDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveLeaveInfo(Leave aLeave)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveId", aLeave.LeaveId));
            aSqlParameterlist.Add(new SqlParameter("@LeaveCode", aLeave.LeaveCode));
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", aLeave.LeaveName));

            string insertQuery = @"insert into tblLeave (LeaveId,LeaveCode,LeaveName) 
            values (@LeaveId,@LeaveCode,@LeaveName)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasLeaveName(Leave aLeave)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", aLeave.LeaveName));
            string query = "select * from tblLeave where LeaveName = @LeaveName ";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadLeaveView()
        {
            string query = @"SELECT * FROM tblLeave ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Leave LeaveEditLoad(string LeaveId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveId", LeaveId));
            string query = "select * from tblLeave where LeaveId = @LeaveId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            Leave aLeave = new Leave();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aLeave.LeaveId = Int32.Parse(dataReader["LeaveId"].ToString());
                    aLeave.LeaveCode = dataReader["LeaveCode"].ToString();
                    aLeave.LeaveName = dataReader["LeaveName"].ToString();
                }
            }
            return aLeave;
        }

        public bool UpdateLeaveInfo(Leave aLeave)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveId", aLeave.LeaveId));
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", aLeave.LeaveName));

            string query = @"UPDATE tblLeave SET LeaveName=@LeaveName WHERE LeaveId=@LeaveId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
