﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class LeaveInventoryDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public bool SaveLeaveInventory(LeaveInventory aLeaveInventory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveInventoryId", aLeaveInventory.LeaveInventoryId));
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", aLeaveInventory.LeaveName));
            aSqlParameterlist.Add(new SqlParameter("@DayQty", aLeaveInventory.DayQty));
            aSqlParameterlist.Add(new SqlParameter("@LeaveYear", aLeaveInventory.LeaveYear));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aLeaveInventory.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", aLeaveInventory.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLeaveInventory.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@LeaveId", aLeaveInventory.LeaveId));
            aSqlParameterlist.Add(new SqlParameter("@YearDayQty", aLeaveInventory.YearDayQty));

            string insertQuery = @"insert into tblLeaveInventory (LeaveInventoryId,LeaveName,DayQty,EmpMasterCode,LeaveYear,EmpName,EmpInfoId,LeaveId,YearDayQty) 
            values (@LeaveInventoryId,@LeaveName,@DayQty,@EmpMasterCode,@LeaveYear,@EmpName,@EmpInfoId,@LeaveId,@YearDayQty)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }

        public bool HasLeaveName(LeaveInventory aInventory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", aInventory.LeaveName));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aInventory.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@LeaveYear", aInventory.LeaveYear));
            string query = "select * from tblLeaveInventory where LeaveName=@LeaveName and EmpInfoId=@EmpInfoId and LeaveYear=@LeaveYear";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadLeaveInventoryView()
        {
            string query = @"SELECT *,tblLeave.LeaveName FROM tblLeaveInventory " +
                " LEFT JOIN tblLeave ON tblLeaveInventory.LeaveId = tblLeave.LeaveId order by tblLeaveInventory.LeaveInventoryId desc ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LeaveInventoryRpt(string parameter)
        {
            string query = @"SELECT * FROM tblLeaveInventory "+parameter+"";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable RptHeader()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel,'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLeave(DateTime fromdt,DateTime todt,string empinfoId)
        {
            string query = @"SELECT * FROM dbo.tblAttendanceRecord WHERE ATTStatus='LV' AND ATTDate BETWEEN '"+fromdt+"' AND '"+todt+"' AND EmpId='"+empinfoId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLeaveInv(string empinfoId,string parameter)
        {
            string query = @"SELECT * FROM dbo.tblLeaveInventory WHERE EmpInfoId='"+empinfoId+"' AND LeaveYear='"+DateTime.Now.Year+"'  "+parameter+"";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LeaveAvailRpt(string parameter)
        {
            string query = @"SELECT * FROM dbo.tblLeaveAvail "+parameter+" ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfoCode(string empcode)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + empcode + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string parameter)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmployeeStatus='Active' "+parameter+"";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AllLeaveReport(string year)
        {
            string query = @"SELECT * FROM dbo.tblLeaveInventory 
                                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLeaveInventory.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                                WHERE LeaveYear='" + year + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LeaveReport(string parameter,string year)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpMasterCode,EmpName,JoiningDate,DesigName,DeptName,UnitName,
                        ISNULL(tblLeaveMain.YearCasual,0) AS YearCasual ,
                        ISNULL(tblLeaveMain.CasualRemain,0) AS CasualRemain,
                        ISNULL(tblLeaveMain.YearMedical,0) AS YearMedical,
                        ISNULL(tblLeaveMain.MedicalRemain,0) AS MedicalRemain,
                        ISNULL(tblLeaveMain.YearEarn,0) AS YearEarn,
                        ISNULL(tblLeaveMain.EarnRemain,0) AS EarnRemain,
                        ISNULL(tblLeaveMain.YearMaternity ,0) AS YearMaternity ,
                        ISNULL(tblLeaveMain.MaternityRemain,0) AS MaternityRemain,
                        ISNULL(tblLeaveMain.CasualEnjoyed,0)AS CasualEnjoyed,
                        ISNULL(tblLeaveMain.MedicalEnjoyed,0)AS MedicalEnjoyed,
                        ISNULL(tblLeaveMain.EarnEnjoyed,0)AS EarnEnjoyed,
                        ISNULL(tblLeaveMain.MaternityEnjoyed,0)AS MaternityEnjoyed,
                        (CASE WHEN ISNULL(tblLeaveMain.YearCasual,0)=0 THEN 'Leave Not Define' ELSE 'ok' END) AS Remarks,CAST(ISNULL(C.LeaveCurrentMonth,0) AS int) AS LeaveAtCurrentMonth


                        FROM dbo.tblEmpGeneralInfo
                        LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                        LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                        LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
LEFT JOIN (SELECT EmpInfoId,SUM(AvailLeaveQty) LeaveCurrentMonth  FROM tblLeaveAvail WHERE ActionStatus = 'Accepted' AND MONTH(ToDate) = MONTH(GETDATE()) GROUP BY EmpInfoId) AS C ON C.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                        LEFT JOIN 

                        (SELECT tblMainLeave.EmpMasterCode,
                        ISNULL(SUM(tblMainLeave.YearCasual),0) AS YearCasual
                        ,ISNULL(SUM(tblMainLeave.CasualRemain),0) AS CasualRemain,
                        (ISNULL(SUM(tblMainLeave.YearCasual),0)-ISNULL(SUM(tblMainLeave.CasualRemain),0)) AS CasualEnjoyed
                        ,ISNULL(SUM(tblMainLeave.YearMedical),0) AS YearMedical
                        ,ISNULL(SUM(tblMainLeave.MedicalRemain),0) AS MedicalRemain,
                        (ISNULL(SUM(tblMainLeave.YearMedical),0)-ISNULL(SUM(tblMainLeave.MedicalRemain),0)) AS MedicalEnjoyed
                        ,ISNULL(SUM(tblMainLeave.YearEarn),0) AS YearEarn
                        ,ISNULL(SUM(tblMainLeave.EarnRemain),0) AS EarnRemain 
                        ,(ISNULL(SUM(tblMainLeave.YearEarn),0)-ISNULL(SUM(tblMainLeave.EarnRemain),0)) AS EarnEnjoyed
                        
                        ,ISNULL(SUM(tblMainLeave.YearMaternity),0) AS YearMaternity 
                        ,ISNULL(SUM(tblMainLeave.MaternityRemain),0) AS MaternityRemain 
                        ,(ISNULL(SUM(tblMainLeave.YearMaternity),0)-ISNULL(SUM(tblMainLeave.MaternityRemain),0)) AS MaternityEnjoyed


                         FROM(SELECT EmpMasterCode,(CASE WHEN LeaveName='Casual' THEN YearDayQty END) AS YearCasual,(CASE WHEN LeaveName='Casual' THEN DayQty END) AS CasualRemain,
                        (CASE WHEN LeaveName='Medical' THEN YearDayQty END) AS YearMedical,(CASE WHEN LeaveName='Medical' THEN DayQty END) AS MedicalRemain,(CASE WHEN LeaveName='Earn' THEN YearDayQty END) AS YearEarn,
                        (CASE WHEN LeaveName='Earn' THEN DayQty END) AS EarnRemain ,(CASE WHEN LeaveName='Maternity' THEN YearDayQty END) AS YearMaternity,
                        (CASE WHEN LeaveName='Maternity' THEN DayQty END) AS MaternityRemain 

                         FROM dbo.tblLeaveInventory WHERE LeaveYear='" + year + "') AS tblMainLeave " +
                         "  GROUP BY tblMainLeave.EmpMasterCode) AS tblLeaveMain ON dbo.tblEmpGeneralInfo.EmpMasterCode=tblLeaveMain.EmpMasterCode " +
                         "  WHERE  dbo.tblEmpGeneralInfo.EmployeeStatus='Active'  " + parameter + " AND  tblEmpGeneralInfo.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" +
                    HttpContext.Current.Session["UserId"].ToString() + "')  ORDER BY tblEmpGeneralInfo.EmpMasterCode";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public LeaveInventory LeaveInventoryEditLoad(string leaveId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveInventoryId", leaveId));
            string query = "select * from tblLeaveInventory where LeaveInventoryId = @LeaveInventoryId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            LeaveInventory aLeaveInventory = new LeaveInventory();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aLeaveInventory.LeaveId = Int32.Parse(dataReader["LeaveId"].ToString());
                    aLeaveInventory.LeaveName = dataReader["LeaveName"].ToString();
                    aLeaveInventory.LeaveYear = dataReader["LeaveYear"].ToString();
                    aLeaveInventory.DayQty = dataReader["DayQty"].ToString();
                    aLeaveInventory.EmpMasterCode = dataReader["EmpMasterCode"].ToString();
                    aLeaveInventory.EmpName = dataReader["EmpName"].ToString();
                    aLeaveInventory.YearDayQty = Convert.ToDecimal(dataReader["YearDayQty"].ToString());
                    aLeaveInventory.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                }
            }
            return aLeaveInventory;
        }

        public bool UpdateLeaveInfo(LeaveInventory aLeaveInventory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveInventoryId", aLeaveInventory.LeaveInventoryId));
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", aLeaveInventory.LeaveName));
            aSqlParameterlist.Add(new SqlParameter("@DayQty", aLeaveInventory.DayQty));
            aSqlParameterlist.Add(new SqlParameter("@LeaveYear", aLeaveInventory.LeaveYear));
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aLeaveInventory.EmpMasterCode));
            aSqlParameterlist.Add(new SqlParameter("@EmpName", aLeaveInventory.EmpName));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLeaveInventory.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@LeaveId", aLeaveInventory.LeaveId));
            aSqlParameterlist.Add(new SqlParameter("@YearDayQty", aLeaveInventory.YearDayQty));

            string query = @"UPDATE tblLeaveInventory SET LeaveName=@LeaveName,DayQty=@DayQty,LeaveYear=@LeaveYear,LeaveId=@LeaveId,EmpMasterCode=@EmpMasterCode ,EmpName=@EmpName,EmpInfoId=@EmpInfoId,YearDayQty=@YearDayQty WHERE LeaveInventoryId=@LeaveInventoryId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LeaveInv(string empinfoId, string year, string leaveName)
        {
            string query = @"SELECT * FROM dbo.tblLeaveInventory WHERE EmpInfoId='" + empinfoId + "' AND LeaveYear='" + year + "' AND LeaveName='" + leaveName + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public DataTable CheckGovtHoliday(string fromdate,string todate,string empinfoId)
        {
            string query = @"select COUNT(*)as A from tblHolidayInformation where HolidayDate between '" + fromdate + "' and '" + todate + "' and IsActive='1' and UnitId in (select UnitId from tblEmpGeneralInfo where EmpInfoId='"+empinfoId+"')";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable CheckWeeklyHolidayDays(string fromdt,string todt,string empinfoId)
        {
            string query = @"Declare @startdate datetime
                            Declare @enddate datetime
                            Declare @count int
                            declare @wekdayno int
                            select @wekdayno=dbo.WeekDay(FirstHolidayName) from tblEmpWeeklyHoliday where EmpId='" + empinfoId + "' set @count=0 set @startdate='" + fromdt + "' set @enddate='" + todt + "' while @startdate<=@enddate Begin IF DatePart(WEEKDAY,@startdate) = @wekdayno SET @count=@count+1 SET @startdate=DateAdd(d,1,@startdate) END select @count";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLeave(string year, string empinfoId, string leaveName)
        {
            string query = @"SELECT * FROM dbo.tblLeaveAvail WHERE YEAR(FromDate)='" + year + "' AND  EmpInfoId='" + empinfoId + "' AND LeaveName='" + leaveName + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool UpdateLeave(string LeaveInventoryId, string DayQty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveInventoryId", LeaveInventoryId));
            aSqlParameterlist.Add(new SqlParameter("@DayQty", DayQty));


            string query = @"UPDATE tblLeaveInventory SET DayQty=@DayQty  WHERE LeaveInventoryId=@LeaveInventoryId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool UpdateLeave(LeaveInventory aLeaveInventory)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveName", aLeaveInventory.LeaveName));
            aSqlParameterlist.Add(new SqlParameter("@DayQty", aLeaveInventory.DayQty));
            aSqlParameterlist.Add(new SqlParameter("@LeaveYear", DateTime.Now.Year));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLeaveInventory.EmpInfoId));

            string query = @"UPDATE tblLeaveInventory SET DayQty=@DayQty  WHERE LeaveYear=@LeaveYear AND EmpInfoId=@EmpInfoId AND LeaveName=@LeaveName";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool UpdateLeaveAvail(LeaveAvail avail,DateTime fromdt, DateTime todt)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FromDate", avail.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", avail.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@AvailLeaveQty", avail.AvailLeaveQty));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", avail.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@FrmDt", fromdt));
            aSqlParameterlist.Add(new SqlParameter("@ToDt", todt));

            string query = @"UPDATE dbo.tblLeaveAvail SET FromDate=@FrmDt , ToDate=@ToDt , AvailLeaveQty=@AvailLeaveQty WHERE FromDate=@FromDate AND ToDate=@ToDate AND EmpInfoId=@EmpInfoId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteLeave(Attendence attendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", attendence.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", attendence.ATTDate));

            string query = @"DELETE FROM dbo.tblAttendanceRecord WHERE ATTDate=@ATTDate AND EmpId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteLeaveAvail(LeaveAvail avail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FromDate", avail.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", avail.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", avail.EmpInfoId));

            string query = @"DELETE FROM dbo.tblLeaveAvail WHERE FromDate=@FromDate AND ToDate=@ToDate AND EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteLeaveAvail(string availId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LeaveAvailId", availId));


            string query = @"DELETE FROM dbo.tblLeaveAvail WHERE LeaveAvailId=@LeaveAvailId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public void LoadLeaveName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblLeave";
            aInternalDal.LoadDropDownValue(ddl, "LeaveName", "LeaveId", queryStr, "HRDB");
        }

        public List<LeaveInventory> ViewAllInventory()
        {
            List<LeaveInventory> allLeaveList = new List<LeaveInventory>();
            string query = "select * from tblLeaveInventory  LEFT JOIN dbo.tblEmpGeneralInfo ON tblEmpGeneralInfo.EmpInfoId = tblLeaveInventory.EmpInfoId WHERE tblEmpGeneralInfo.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" +
                    HttpContext.Current.Session["UserId"].ToString() + "')";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, "HRDB");

            while (dataReader.Read())
            {
                LeaveInventory aLeaveInventory = new LeaveInventory();
                aLeaveInventory.EmpMasterCode = (dataReader["EmpMasterCode"].ToString());
                aLeaveInventory.EmpName = dataReader["EmpName"].ToString();
                aLeaveInventory.DayQty = dataReader["DayQty"].ToString();
                aLeaveInventory.LeaveInventoryId = Convert.ToInt32(dataReader["LeaveInventoryId"].ToString());
                aLeaveInventory.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                allLeaveList.Add(aLeaveInventory);
            }

            return allLeaveList;
        }

        public List<LeaveInventory> ViewEmpNameAndDayQty(string employeeId)
        {
            List<LeaveInventory> singleEmpNameAndDayQtyList = ViewAllInventory();
            List<LeaveInventory> singleEmpNameandDayQty = (from LeaveInventory aLeaveInventory in singleEmpNameAndDayQtyList
                                                  where aLeaveInventory.EmpMasterCode == employeeId
                                                  select aLeaveInventory).ToList();
            return singleEmpNameandDayQty;
        }

        public DataTable LeaveAvailReport1(string empcode, string year)
        {
            string query = @"
                            DECLARE @start DATETIME
                            DECLARE @end DATETIME
                            DECLARE @duration INT

                            SELECT  @end = GETDATE()

                            SELECT @start=JoiningDate FROM dbo.tblEmpGeneralInfo WHERE EmpMasterCode='"+empcode+"'"+


                            " SELECT @duration = DATEDIFF(mm, @start, @end) "+


                            " SELECT *,(CONVERT(NVARCHAR, @duration / 12) + ' Year ' + CONVERT(NVARCHAR, @duration % 12) +' Month') JobLength FROM dbo.tblLeaveAvail where EmpMasterCode='" + empcode + "' AND YEAR(FromDate)='" + year + "' AND ActionStatus='Accepted' ORDER BY FromDate";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LeaveAvailReport(string empcode, string year)
        {
            string query = @"SELECT * FROM GetDateWiseAttendence('" + empcode + "','" + year + "')";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LeaveAvailReport(string leaveavailId)
        {
            string query = @"SELECT *,EntryDate as AppliedOn FROM dbo.tblLeaveAvail where LeaveAvailId='" + leaveavailId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LeaveAvailReport2(string leaveavailId)
        {
            string query = @"
                                SELECT *,EntryDate as AppliedOn FROM GetDateWiseAttendence('AG1002','2018') E LEFT JOIN dbo.tblLeaveAvail ON dbo.tblLeaveAvail.LeaveAvailId=E.LeaveAvailId
 where E.LeaveAvailId='" + leaveavailId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LeaveInventoryReport2(string empcode)
        {
            string query = @"SELECT * FROM dbo.tblLeaveInventory
                            LEFT JOIN dbo.tblLeaveAvail ON dbo.tblLeaveInventory.LeaveInventoryId = dbo.tblLeaveAvail.LeaveInventoryId
                            WHERE dbo.tblLeaveInventory.EmpMasterCode='" + empcode + "'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetEmpInfo(string id)
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                            LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId where EmpInfoId='" + id + "'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

    }
}
