﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Library.DAL.HRM_DAL
{
    public class LeaveProcessDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public int LeaveProcess(string dataBaseName)
        {
            try
            {
                int count = 0;

                Database db;
                DbCommand dbCommand;
                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetStoredProcCommand("LeaveInventoryProcessSP");
                db.AddOutParameter(dbCommand, "@Count", DbType.Int32, 5);

                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    count = int.Parse(dbCommand.Parameters["@Count"].Value.ToString());
                    return count;
                }

                return count;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
