﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class LineDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveLineInfo(Line aLine)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LineId", aLine.LineId));
            aSqlParameterlist.Add(new SqlParameter("@LineName", aLine.LineName));

            string insertQuery = @"insert into tblLine (LineId,LineName) 
            values (@LineId,@LineName)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasLineName(Line aLine)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LineName", aLine.LineName));
            string query = "select * from tblLine where LineName = @LineName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                 while (dataReader.Read())
                 {
                     return true;
                 }
            }
            return false;
        }
        
        public DataTable LoadLineView()
        {
            string query = @"SELECT * FROM tblLine ";              
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public Line LineEditLoad(string LineId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LineId", LineId));
            string query = "select * from tblLine where LineId = @LineId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Line aLine = new Line();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aLine.LineId = Int32.Parse(dataReader["LineId"].ToString());
                    aLine.LineName = dataReader["LineName"].ToString();  
                }

            }
            return aLine;
        }
        public bool UpdateLineInfo(Line aLine)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LineId", aLine.LineId));
            aSqlParameterlist.Add(new SqlParameter("@LineName", aLine.LineName));
            string query = @"UPDATE tblLine SET LineName=@LineName WHERE LineId=@LineId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
