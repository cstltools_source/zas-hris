﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;
using Microsoft.SqlServer.Server;

namespace Library.DAL.HRM_DAL
{
    public class LoanMasterDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();

        public bool SaveDataForLoanMaster(LoanMaster aLoanMaster)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LoanId", aLoanMaster.LoanId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLoanMaster.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aLoanMaster.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aLoanMaster.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@TotalInstallment", aLoanMaster.TotalInstallment));
            aSqlParameterlist.Add(new SqlParameter("@LoanAmount", aLoanMaster.LoanAmount));
            aSqlParameterlist.Add(new SqlParameter("@SanctionDate", aLoanMaster.SanctionDate));
            aSqlParameterlist.Add(new SqlParameter("@DeductionStartDate", aLoanMaster.DeductionStartDate));
            aSqlParameterlist.Add(new SqlParameter("@InstallAmount", aLoanMaster.InstallAmount));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aLoanMaster.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aLoanMaster.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aLoanMaster.Status));
            aSqlParameterlist.Add(new SqlParameter("@LoanType", aLoanMaster.LoanType));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aLoanMaster.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", "Posted"));

            string insertQuery = @"insert into tblLoanMaster (LoanId,EmpInfoId,DesigId,DeptId,TotalInstallment,LoanAmount,SanctionDate,DeductionStartDate,InstallAmount,EntryBy,EntryDate,Status,LoanType,IsActive,ActionStatus) 
                                   values (@LoanId,@EmpInfoId,@DesigId,@DeptId,@TotalInstallment,@LoanAmount,@SanctionDate,@DeductionStartDate,@InstallAmount,@EntryUser,@EntryDate,@Status,@LoanType,@IsActive,@ActionStatus)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasLoanMaster(LoanMaster aLoanMaster)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aLoanMaster.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@LoanType", aLoanMaster.LoanType));
            aSqlParameterlist.Add(new SqlParameter("@Month", aLoanMaster.DeductionStartDate.Month));
            aSqlParameterlist.Add(new SqlParameter("@Year", aLoanMaster.DeductionStartDate.Year));
            string query = "select * from dbo.tblLoanMaster LEFT JOIN dbo.tblLoanDetail ON dbo.tblLoanMaster.LoanId = dbo.tblLoanDetail.LoanId where EmpInfoId=@EmpInfoId AND LoanType=@LoanType    AND MONTH(InstallmentDate)>=@Month AND YEAR(InstallmentDate)=@Year AND Status='Accepted'";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public bool SaveDataForLoanDetail(LoanDetail aLoanDetail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LoanDetailId", aLoanDetail.LoanDetailId));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aLoanDetail.Amount));
            aSqlParameterlist.Add(new SqlParameter("@InstallmentDate", aLoanDetail.InstallmentDate));
            aSqlParameterlist.Add(new SqlParameter("@LoanId", aLoanDetail.LoanId));

            string insertQuery = @"insert into tblLoanDetail (LoanDetailId ,
                                       InstallmentDate ,
                                       Amount ,
                                       LoanId) 
                                   values (@LoanDetailId ,
                                       @InstallmentDate ,
                                       @Amount ,
                                       @LoanId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadLoanApproveViewForApproval(string ActionStatus)
        {
            string query = @"SELECT  *,tblDepartment.DeptName,tblDesignation.DesigName
                 FROM tblLoanMaster 
                 LEFT JOIN tblEmpGeneralInfo ON tblLoanMaster.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN dbo.tblDesignation ON tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                 LEFT JOIN dbo.tblDepartment ON tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                 where tblLoanMaster.ActionStatus ='" + ActionStatus + "' AND tblLoanMaster.IsActive=1   order by tblLoanMaster.LoanId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoanInform(string epminfoId)
        {
            string query = @"SELECT * FROM dbo.tblLoanMaster
            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLoanMaster.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
             WHERE dbo.tblEmpGeneralInfo.EmpInfoId='" + epminfoId + "' ORDER BY LoanId DESC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable PaidLoanInform(string epminfoId,string startdate)
        {
            string query = @"SELECT ISNULL(SUM(SalaryAdvance),0)SA FROM dbo.tblSalaryRecordPerMonth WHERE SalaryStartDate BETWEEN '" + startdate + "' AND '" + DateTime.Now.ToShortDateString() + "' AND SalaryAdvance >0 AND EmpInfoId='" + epminfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteNewData(string  loanId,string startdate,string empInfoId)
        {


            string query = @"DELETE FROM dbo.tblLoanDetail WHERE LoanId='"+loanId+"' AND InstallmentDate NOT IN (SELECT SalaryStartDate FROM dbo.tblSalaryRecordPerMonth WHERE SalaryStartDate BETWEEN '" + startdate + "' AND '" + DateTime.Now.ToShortDateString() + "' AND SalaryAdvance >0 AND EmpInfoId='" + empInfoId + "')";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query,  "HRDB");
        }
        public bool ApprovalUpdateDAL(LoanMaster aLoanMaster)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LoanId", aLoanMaster.LoanId));
            aSqlParameterlist.Add(new SqlParameter("@Status", aLoanMaster.Status));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aLoanMaster.Status));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aLoanMaster.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aLoanMaster.ApprovedDate));

            string query = @"UPDATE tblLoanMaster SET Status=@Status,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate,ActionStatus=@ActionStatus WHERE LoanId=@LoanId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public DataTable EmpInformationDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT EG.EmpInfoId,EG.EmpMasterCode,EG.EmpName,EG.DesigId,DS.DesigName,EG.DepId,DE.DeptName,EG.SectionId, 
                             SE.SectionName , G.GradeId,G.GradeType,EG.SalScaleId ,SG.BasicSalary,SG.SalScaleName
                             FROM dbo.tblEmpGeneralInfo EG 
                             LEFT JOIN dbo.tblDesignation DS ON EG.DesigId=DS.DesigId 
                             LEFT JOIN dbo.tblDepartment DE ON EG.DepId=DE.DeptId 
                             LEFT JOIN dbo.tblSection SE ON EG.SectionId=SE.SectionId 
                             LEFT JOIN dbo.tblEmployeeGrade G ON EG.EmpGradeId=G.GradeId 
                             LEFT JOIN dbo.tblSalaryGradeOrScale SG ON EG.SalScaleId=SG.SalScaleId
                             WHERE EG.EmpMasterCode='" + empId + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        
        public bool UpdateLoanMaster(LoanMaster aMaster)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@LoanId", aMaster.LoanId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aMaster.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aMaster.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aMaster.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@TotalInstallment", aMaster.TotalInstallment));
            aSqlParameterlist.Add(new SqlParameter("@LoanAmount", aMaster.LoanAmount));
            aSqlParameterlist.Add(new SqlParameter("@SanctionDate", aMaster.SanctionDate));
            aSqlParameterlist.Add(new SqlParameter("@DeductionStartDate", aMaster.DeductionStartDate));
            aSqlParameterlist.Add(new SqlParameter("@InstallAmount", aMaster.InstallAmount));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aMaster.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aMaster.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aMaster.Status));

            string updateQuery = @"UPDATE tblLoanMaster SET LoanId=@LoanId,EmpInfoId=@EmpInfoId,DesigId=@DesigId,DeptId=@DeptId,TotalInstallment=@TotalInstallment,LoanAmount=@LoanAmount,SanctionDate=@SanctionDate,DeductionStartDate=@DeductionStartDate,InstallAmount=@InstallAmount,EntryUser=@EntryUser,EntryDate=@EntryDate,@Status  WHERE EmpInfoId=@EmpInfoId and Status='active'";
            return aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoanCheck(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * from tblLoanMaster where EmpInfoId = '" + empId.Trim() + "' and Status='active'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public bool DeleteData(string LoanID)
        {
            string query = @"Delete from  tblLoanDetail where LoanID= '" + LoanID + "'";
            aCommonInternalDal.DeleteDataByDeleteCommand(query, "HRDB");
            return aCommonInternalDal.DeleteStatusUpdate("tblLoanMaster", "LoanId", LoanID);
        }
        public DataTable LoadLoan()
        {
            string query = @"SELECT  *,tblDepartment.DeptName,tblDesignation.DesigName
                 FROM tblLoanMaster 
                 LEFT JOIN tblEmpGeneralInfo ON tblLoanMaster.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN dbo.tblDesignation ON tblLoanMaster.DesigId = dbo.tblDesignation.DesigId
                 LEFT JOIN dbo.tblDepartment ON tblLoanMaster.DeptId = dbo.tblDepartment.DeptId
                 where tblLoanMaster.ActionStatus in ('Posted','Cancel') and tblLoanMaster.IsActive=1 and tblLoanMaster.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblLoanMaster.LoanId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadLoanById(string id)
        {
            string query = @"SELECT *,InstallmentDate AS InstallDate FROM dbo.tblLoanMaster
            LEFT JOIN dbo.tblLoanDetail ON dbo.tblLoanMaster.LoanId = dbo.tblLoanDetail.LoanId
            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblEmpGeneralInfo.EmpInfoId=dbo.tblLoanMaster.EmpInfoId WHERE dbo.tblLoanDetail.LoanId='" + id + "'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteLoanMaster(string loanId)
        {
            string query = @"DELETE FROM dbo.tblLoanMaster WHERE LoanId='"+loanId+"'";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, "HRDB");
        }
        public bool DeleteLoanDetail(string loanId)
        {
            string query = @"DELETE FROM dbo.tblLoanDetail WHERE LoanId='" + loanId + "'";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, "HRDB");
        }
    }
}
