﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ManualAttEmpGroupArrangeDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveGroupInfo(Group aGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aGroup.EmpId));


            string insertQuery = @"insert into tblManualAttEmpWiseGroup (GroupId,EmpId) 
            values (@GroupId,@EmpId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasEmp(Group aGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aGroup.EmpId));
            string query = "select * from tblManualAttEmpWiseGroup where EmpId = @EmpId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadGroupView()
        {
            string query = @"SELECT * FROM tblManualAttEmpWiseGroup ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGroupReport(string groupId)
        {
            string query = @"SELECT EmpMasterCode,EmpName,GroupName,DeptName,ShiftName,dbo.tblShift.ShiftInTime,dbo.tblShift.ShiftOutTime,FromDate,ToDate FROM dbo.tblManualAttEmpWiseGroup 
                            LEFT JOIN dbo.tblGroup ON dbo.tblEmpWiseGroup.GroupId = dbo.tblGroup.GroupId
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblManualAttEmpWiseGroup.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId= dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblShiftWiseGroup ON dbo.tblManualAttEmpWiseGroup.GroupId=dbo.tblShiftWiseGroup.GroupId
                            LEFT JOIN dbo.tblShift ON dbo.tblShiftWiseGroup.ShiftId = dbo.tblShift.ShiftId    WHERE tblGroup.GroupId='" + groupId + "' AND EmployeeStatus='Active' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadGroupName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblManualAttGroup";
            aInternalDal.LoadDropDownValue(ddl, "GroupName", "GroupId", queryStr, "HRDB");
        }

        public DataTable LoadEmpInfo(string empcode)
        {
            string query = @"SELECT EmpMasterCode,EmpName,DeptName,EmpInfoId FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId WHERE EmpMasterCode='" + empcode + "' and ShiftEmp='Yes' AND EmployeeStatus='Active'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartmentWiseEmp(string deptid)
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                             WHERE  DepId='" + deptid + "' AND EmployeeStatus='Active' AND tblEmpGeneralInfo.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" +
                    HttpContext.Current.Session["UserId"].ToString() + "') ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadGroupEmp(string deptid,string groupId)
        {
            string query = @"SELECT * FROM dbo.tblManualAttEmpWiseGroup
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblManualAttEmpWiseGroup.EmpId = dbo.tblEmpGeneralInfo.EmpInfoId 
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                            WHERE   DepId='" + deptid + "' AND GroupId='" + groupId + "' AND EmployeeStatus='Active' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool UpdateGroupInfo(Group aGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aGroup.EmpId));


            string query = @"UPDATE tblManualAttEmpWiseGroup SET  GroupId=@GroupId  WHERE EmpId=@EmpId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteGroup(Group aGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aGroup.EmpId));


            string query = @"DELETE FROM dbo.tblManualAttEmpWiseGroup  WHERE  GroupId=@GroupId  AND EmpId=@EmpId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
