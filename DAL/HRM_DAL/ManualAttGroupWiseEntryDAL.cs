﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ManualAttGroupWiseEntryDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveDataForShiftWiseGroup(ShiftWiseGroup aShiftWiseGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GSAId", aShiftWiseGroup.GSAId));
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aShiftWiseGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@FromDate", aShiftWiseGroup.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", aShiftWiseGroup.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aShiftWiseGroup.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aShiftWiseGroup.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aShiftWiseGroup.Status));

            string insertQuery = @"insert into dbo.tblManualAttEmpGroupWiseDetail (GSAId,GroupId,FromDate,ToDate,EntryUser,EntryDate,ActionStatus) 
            values (@GSAId,@GroupId,@FromDate,@ToDate,@EntryUser,@EntryDate,@Status)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasWeeklyHoliday(string dayname, string empid)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", empid));
            aSqlParameterlist.Add(new SqlParameter("@DayName", dayname));
            string query = "SELECT * FROM dbo.tblEmpWeeklyHoliday WHERE EmpId=@EmpId AND (FirstHolidayName=@DayName OR SecondHolidayName=@DayName)";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadHolidayView()
        {
            string query = @"SELECT * FROM dbo.tblWeeklyShiftWiseGroupment";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public string DayName(string dayName)
        {
            string query = @"SELECT (DATENAME(dw,'" + dayName + "')) AS DayName";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString();
        }
        
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadShift(string shiftId)
        {
            string query =
                @"SELECT * FROM dbo.tblShift WHERE ShiftId='"+shiftId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDate(string groupId,string fromdt,string todt)
        {
            string query =
                @"SELECT * FROM tblManualAttEmpGroupWiseDetail WHERE GroupId ='"+groupId+"' AND ActionStatus IN ('Accepted','Posted') and (('"+fromdt+"' BETWEEN FromDate AND ToDate) OR ('"+todt+"' BETWEEN FromDate AND ToDate)) ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadGroupName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblManualAttGroup";
            aInternalDal.LoadDropDownValue(ddl, "GroupName", "GroupId", queryStr, "HRDB");
        }
        public void LoadShift(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblShift";
            aInternalDal.LoadDropDownValue(ddl, "ShiftName", "ShiftId", queryStr, "HRDB");
        }
        public bool ApprovalUpdateDAL(ShiftWiseGroup aShiftWiseGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GSAId", aShiftWiseGroup.GSAId));
            aSqlParameterlist.Add(new SqlParameter("@Status", aShiftWiseGroup.Status));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aShiftWiseGroup.ApproveUser));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aShiftWiseGroup.ApproveDate));

            string query = @"UPDATE tblManualAttEmpGroupWiseDetail SET ActionStatus=@Status,ApproveUser=@ApprovedBy,ApproveDate=@ApprovedDate WHERE GSAId=@GSAId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadShiftWiseGroupViewApproval(string actionstatus)
        {
            string query = @"SELECT * FROM dbo.tblManualAttEmpGroupWiseDetail
                            LEFT JOIN dbo.tblShift ON tblManualAttEmpGroupWiseDetail.ShiftId=dbo.tblShift.ShiftId where tblManualAttEmpGroupWiseDetail.ActionStatus='"+actionstatus+"'  order by dbo.tblManualAttEmpGroupWiseDetail.GSAId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpID(int id)
        {
            string query =
                @"SELECT * FROM dbo.tblManualAttEmpWiseGroup
                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblManualAttEmpWiseGroup.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId WHERE GroupId='" + id + "'  AND EmployeeStatus='Active' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
