﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ManualAttendenceDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveManualAttendenceInfo(ManualAttendence aManualAttendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MAttendenceId", aManualAttendence.MAttendenceId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aManualAttendence.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", aManualAttendence.ATTDate));
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aManualAttendence.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftInTime", aManualAttendence.ShiftInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftOutTime", aManualAttendence.ShiftOutTime));
            aSqlParameterlist.Add(new SqlParameter("@EntryReason", aManualAttendence.EntryReason));
            aSqlParameterlist.Add(new SqlParameter("@ATTStatus", aManualAttendence.ATTStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aManualAttendence.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aManualAttendence.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aManualAttendence.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", true));
            aSqlParameterlist.Add(new SqlParameter("@DayName", aManualAttendence.DayName));
            aSqlParameterlist.Add(new SqlParameter("@OverTimeDuration", aManualAttendence.OverTimeDuration));

            string insertQuery = @"insert into dbo.tblManualAttendence (MAttendenceId,EmpInfoId,ATTDate,ShiftId,ShiftInTime,ShiftOutTime,ATTStatus,EntryReason,EntryUser,EntryDate,ActionStatus,IsActive,DayName,OverTimeDuration) 
            values (@MAttendenceId,@EmpInfoId,@ATTDate,@ShiftId,@ShiftInTime,@ShiftOutTime,@ATTStatus,@EntryReason,@EntryUser,@EntryDate,@ActionStatus,@IsActive,@DayName,@OverTimeDuration)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasEmpAttendence(ManualAttendence aManualAttendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aManualAttendence.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", aManualAttendence.ATTDate));
            string query = "select * from tblManualAttendence where EmpInfoId=@EmpInfoId and ATTDate=@ATTDate";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadManualAttendenceView()
        {
            string query = @"SELECT * FROM tblAttendanceRecord ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpCode(string empcode)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", empcode));

            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo WHERE EmpMasterCode=@EmpMasterCode AND IsActive=1 and EmployeeStatus='Active' and UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" + HttpContext.Current.Session["UserId"].ToString() + "')";
            return aCommonInternalDal.DataContainerDataTable(query,aSqlParameterlist, "HRDB");
        }
        public DataTable Shift(string ShiftId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", ShiftId));

            string query = @"SELECT * FROM dbo.tblShift WHERE ShiftId=@ShiftId ";
            return aCommonInternalDal.DataContainerDataTable(query, aSqlParameterlist, "HRDB");
        }

        public void LoadShiftName(DropDownList ddl,string empinfoId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from dbo.tblShift LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblShift.ShiftId = dbo.tblEmpGeneralInfo.ShiftId WHERE EmpInfoId='"+empinfoId+"' ";
            aInternalDal.LoadDropDownValue(ddl, "ShiftName", "ShiftId", queryStr, "HRDB");
        }
        public void LoadShiftName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from dbo.tblShift ";
            aInternalDal.LoadDropDownValue(ddl, "ShiftName", "ShiftId", queryStr, "HRDB");
        }
        //public ManualAttendence ManualAttendenceEditLoad(string DesigId)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@MAttendenceId", DesigId));
        //    string query = "select * from tblAttendanceRecord where DesigId = @MAttendenceId";
        //    IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

        //    ManualAttendence aManualAttendence = new ManualAttendence();
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            aManualAttendence.MAttendenceId = Int32.Parse(dataReader["MAttendenceId"].ToString());
        //            aManualAttendence.ATTDate = Convert.ToDateTime(dataReader["ATTDate"].ToString());
        //            aManualAttendence.EntryReason = dataReader["EntryReason"].ToString();
        //        }
        //    }
        //    return aManualAttendence;
        //}
        public DataTable LoadView()
        {
            string query = @"SELECT M.MAttendenceId,E.EmpInfoId,E.EmpMasterCode,E.EmpName,E.DesigName,E.DeptName,CONVERT(NVARCHAR(11),M.ATTDate,106) AS ATTDate,
                M.ShiftInTime,M.ShiftOutTime ,M.OverTimeDuration
                FROM dbo.tblManualAttendence M 
                LEFT JOIN View_EmployeeInformation E ON M.EmpInfoId = E.EmpInfoId
                WHERE M.ActionStatus IN ('Posted','Cencel') and M.IsActive=1 and M.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  AND E.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" + HttpContext.Current.Session["UserId"].ToString() + "')  order by M.ATTDate  ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteData(string MAttendenceId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblManualAttendence", "MAttendenceId", MAttendenceId);

        }
        public bool HasEmpManualAttendence(ManualAttendence aManualAttendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aManualAttendence.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", aManualAttendence.ATTDate));
            string query = "select * from tblManualAttendence where EmpInfoId=@EmpInfoId and ATTDate=@ATTDate and ActionStatus<>'Cancel'";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadManualAttendence()
        {
            string query = @"SELECT * From tblManualAttendence
                LEFT JOIN tblEmpGeneralInfo ON tblManualAttendence.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where ActionStatus<>'Accepted' and EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "' AND tblManualAttendence.IsActive=1  order by tblManualAttendence.MAttendenceId  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
  
        }
        public void LoadShiftNameGroupWise(DropDownList ddl, string empinfoId,string date)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "SELECT * FROM dbo.tblShiftWiseGroup LEFT JOIN dbo.tblEmpWiseGroup ON  dbo.tblShiftWiseGroup.GroupId = dbo.tblEmpWiseGroup.GroupId LEFT JOIN dbo.tblShift ON dbo.tblShiftWiseGroup.ShiftId=dbo.tblShift.ShiftId WHERE EmpId='" + empinfoId + "' AND ('" + date + "' BETWEEN FromDate AND ToDate) AND ActionStatus='Accepted' ORDER BY GSAId DESC  ";
            aInternalDal.LoadDropDownValue(ddl, "ShiftName", "ShiftId", queryStr, "HRDB");
        }
        public DataTable ChkShiftData(string empId,string date)
        {
            string query = @"SELECT * FROM dbo.tblShiftWiseGroup 
                            LEFT JOIN dbo.tblEmpWiseGroup ON  dbo.tblShiftWiseGroup.GroupId = dbo.tblEmpWiseGroup.GroupId
                            LEFT JOIN dbo.tblShift ON dbo.tblShiftWiseGroup.ShiftId=dbo.tblShift.ShiftId WHERE EmpId='" + empId + "' AND ('" + date + "' BETWEEN FromDate AND ToDate) AND ActionStatus='Accepted' ORDER BY GSAId DESC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable LoadAttendanceRecordData(int empId, string date)
        {
            string query = @"SELECT * FROM tblAttendanceRecord where EmpId='" + empId + "'  and  ATTDate='" + date + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public bool UpdateManualAttendenceInfo(ManualAttendence aManualAttendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aManualAttendence.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", aManualAttendence.ATTDate));
            aSqlParameterlist.Add(new SqlParameter("@ShiftOutTime", aManualAttendence.ShiftOutTime));

            string query = @"UPDATE tblAttendanceRecord SET ShiftOutTime=@ShiftOutTime WHERE EmpId=@EmpInfoId and ATTDate=@ATTDate";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }

        public DataTable LoadManualAttendanceViewForApproval(string ActionStatus)
        {
            string query = @"SELECT M.*,E.EmpMasterCode,E.EmpName,E.DesigName,E.DeptName From tblManualAttendence M
                LEFT JOIN View_EmployeeInformation E ON M.EmpInfoId = E.EmpInfoId where M.ActionStatus='" + ActionStatus + "' AND  E.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" +
                    HttpContext.Current.Session["UserId"].ToString() + "') order by M.MAttendenceId asc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadManualAttData(int AttendenceId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MAttendenceId", AttendenceId));
            string query = @"SELECT dbo.tblManualAttendence.ShiftInTime AS InTime,dbo.tblManualAttendence.ShiftOutTime AS OutTime, MAttendenceId ,
                            EmpInfoId ,
                            OverTimeDuration,
                           convert(varchar, ATTDate, 121) as ATTDate,
                            dbo.tblManualAttendence.ShiftId ,
                            ATTStatus ,
                            EntryReason ,
                            EntryUser ,
                            EntryDate ,
                            ActionStatus ,
        
                            DayName ,
                            dbo.tblShift.ShiftId ,
                            ShiftName ,
                            dbo.tblShift.ShiftInTime ,
                            dbo.tblShift.ShiftOutTime 
                            From tblManualAttendence
                    LEFT JOIN dbo.tblShift ON dbo.tblManualAttendence.ShiftId = dbo.tblShift.ShiftId WHERE MAttendenceId=@MAttendenceId ";
            return aCommonInternalDal.DataContainerDataTable(query,aSqlParameterlist, "HRDB");
        }
        public DataTable ManualAttData(int AttendenceId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MAttendenceId", AttendenceId));
            string query = @"SELECT * From tblManualAttendence WHERE MAttendenceId=@MAttendenceId ";
            return aCommonInternalDal.DataContainerDataTable(query, aSqlParameterlist, "HRDB");
        }
        
        public bool ApprovalUpdateDAL(ManualAttendence aManualAttendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MAttendenceId", aManualAttendence.MAttendenceId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aManualAttendence.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApproveBy", aManualAttendence.Approveby));
            aSqlParameterlist.Add(new SqlParameter("@ApproveDate", aManualAttendence.ApproveDate));

            string query = @"UPDATE tblManualAttendence SET ActionStatus=@ActionStatus,ApproveBy=@ApproveBy,ApproveDate=@ApproveDate WHERE MAttendenceId=@MAttendenceId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(Attendence attendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", attendence.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", attendence.ATTDate));

            string query = @"DELETE FROM dbo.tblAttendanceRecord WHERE  EmpId=@EmpInfoId and ATTDate=@ATTDate ";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadManualAttendanceViewForApproval()
        {
            string query = @"SELECT * From tblManualAttendence
                LEFT JOIN tblEmpGeneralInfo ON tblManualAttendence.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblManualAttendence.ActionStatus='Posted'   order by tblManualAttendence.MAttendenceId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable GetEmpInfoForGroupManualAtt(string date)
        {
            string query = @"SELECT E.EmpInfoId,E.EmpMasterCode,E.EmpName,E.DesigName,E.DeptName,ShiftId,ShiftInTime,ShiftOutTime  From tblManualAttendanceGroup M
                LEFT JOIN View_EmployeeInformation E ON M.EmpInfoId = E.EmpInfoId WHERE E.EmployeeStatus='Active' AND E.JoiningDate<='" + date + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }



        public bool ApprovalUpdateDAL(Attendence attendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", attendence.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@ATTDate", attendence.ATTDate));
            aSqlParameterlist.Add(new SqlParameter("@ATTStatus", attendence.ATTStatus));
            aSqlParameterlist.Add(new SqlParameter("@DayName", attendence.DayName));
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", attendence.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftStart", attendence.ShiftStart));
            aSqlParameterlist.Add(new SqlParameter("@ShiftEnd", attendence.ShiftEnd));
            aSqlParameterlist.Add(new SqlParameter("@InTime", attendence.InTime));
            aSqlParameterlist.Add(new SqlParameter("@OutTime", attendence.OutTime));
            aSqlParameterlist.Add(new SqlParameter("@ComDutyDuration", attendence.ComDutyDuration));
            aSqlParameterlist.Add(new SqlParameter("@ComOTDuration", attendence.ComOTDuration));
            aSqlParameterlist.Add(new SqlParameter("@ComOutTime", attendence.ComOutTime));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", attendence.Remarks));
            aSqlParameterlist.Add(new SqlParameter("@OverTimeDuration", attendence.OTDuration));
            aSqlParameterlist.Add(new SqlParameter("@DutyDuration", attendence.DutyDuration));

            string insertQuery = @"insert into tblAttendanceRecord (EmpId,ATTDate,DayName,ShiftId,ShiftStart,ShiftEnd,InTime,OutTime,ATTStatus,Remarks,OverTimeDuration,DutyDuration,ComDutyDuration,ComOverTimeDuration,ComOutTime) 
                                    values (@EmpId,@ATTDate,@DayName,@ShiftId,@ShiftStart,@ShiftEnd,@InTime,@OutTime,@ATTStatus,@Remarks,@OverTimeDuration,@DutyDuration,@ComDutyDuration,@ComOTDuration,@ComOutTime)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
    }
}
