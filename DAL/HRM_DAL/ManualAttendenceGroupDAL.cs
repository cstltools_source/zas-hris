﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ManualAttendenceGroupDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        
        public bool SaveManualAttendenceGroupInfo(ManualAttendenceGroup aManualAttendence)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MAGId", aManualAttendence.MAGId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aManualAttendence.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ManualStartDate", aManualAttendence.ManualStartDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aManualAttendence.IsActive));

            string insertQuery = @"insert into dbo.tblManualAttendanceGroup (MAGId,EmpInfoId,ManualStartDate,IsActive) 
            values (@MAGId,@EmpInfoId,@ManualStartDate,@IsActive)";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string mgid)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblManualAttendanceGroup", "MAGId", mgid);
        }
        
        public DataTable LoadManualAttendenceView()
        {
            string query = @"SELECT * FROM tblManualAttendanceGroup 
                            LEFT JOIN dbo.tblEmpGeneralInfo ON tblManualAttendanceGroup.EmpInfoId=tblEmpGeneralInfo.EmpInfoId WHERE tblManualAttendanceGroup.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpCode(string empcode)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", empcode));

            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo WHERE EmpMasterCode=@EmpMasterCode AND IsActive=1 and EmployeeStatus='Active'";
            return aCommonInternalDal.DataContainerDataTable(query,aSqlParameterlist, "HRDB");
        }

    }
}
