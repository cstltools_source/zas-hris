﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ManualGroupDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDevision(EmpGroup aEmpGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aEmpGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@GroupName", aEmpGroup.GroupName));

            string insertQuery = @"insert into tblManualAttGroup (GroupId,GroupName) 
            values (@GroupId,@GroupName)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasEmpGroupName(EmpGroup aEmpGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupName", aEmpGroup.GroupName));
            string query = "select * from tblManualAttGroup where GroupName = @GroupName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadEmpGroupView()
        {
            string query =
                @"SELECT * from tblManualAttGroup";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public EmpGroup EmpGroupEditLoad(string GroupId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", GroupId));
            string query = "select * from tblManualAttGroup where GroupId = @GroupId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            EmpGroup aEmpGroup = new EmpGroup();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aEmpGroup.GroupId = Int32.Parse(dataReader["GroupId"].ToString());
                    aEmpGroup.GroupName = dataReader["GroupName"].ToString();
                }
            }
            return aEmpGroup;
        }

        public bool UpdateEmpGroup(EmpGroup aEmpGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aEmpGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@GroupName", aEmpGroup.GroupName));

            string query = @"UPDATE tblManualAttGroup SET GroupName=@GroupName WHERE GroupId=@GroupId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteEmpGroupInfo(string GroupId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GroupId", GroupId));

            string query = @"DELETE FROM dbo.tblManualAttGroup WHERE GroupId=@GroupId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
