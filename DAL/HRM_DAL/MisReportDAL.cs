﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class MisReportDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public DataTable DeptSalReport()
        {
            string query = @"SELECT UnitName,DeptName,DesigName,COUNT(dbo.tblEmpGeneralInfo.EmpInfoId) AS TotalEmp,SUM(Amount)AS TotalSalary FROM dbo.tblSalaryInformation
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN tblDesignation ON tblEmpGeneralInfo.DesigId = tblDesignation.DesigId 
                            LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                            LEFT JOIN tblDepartment ON tblEmpGeneralInfo.DepId = tblDepartment.DeptId 
                            LEFT JOIN tblSection ON tblEmpGeneralInfo.SectionId = tblSection.SectionId 
                            LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId=dbo.tblEmployeeGrade.GradeId WHERE SalHeadName='Gross'

                            GROUP BY UnitName,DeptName,DesigName";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable YearlyAttendanceStatement(string fromdate, string todate)
        {
            string query = @"SELECT '" + fromdate + "'FromDate,'" + todate + "'ToDate,EmpInfoId,EmpName,EmpMasterCode,DesigName,DeptName,SUM(TotalPresent)TotalPresent,SUM(TotalAbsent)TotalAbsent,SUM(TotalLate)TotalLate,SUM(CasualLeave)CasualLeave,SUM(EarnLeave)EarnLeave,SUM(MedicalLeave)MedicalLeave FROM ( " +
                            " SELECT EmpId,COUNT(*)TotalPresent,'0'TotalAbsent,'0'TotalLate,'0'CasualLeave,'0'MedicalLeave,'0'EarnLeave FROM dbo.tblAttendanceRecord WHERE ATTStatus='P' AND (ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "') GROUP BY EmpId " +
                            " UNION ALL " +
                            " SELECT EmpId,COUNT(*)TotalPresent,'0'TotalAbsent,'0'TotalLate,'0'CasualLeave,'0'MedicalLeave,'0'EarnLeave FROM dbo.tblAttendanceRecord WHERE ATTStatus='L' AND Remarks LIKE '%Weekly Holiday%'  AND (ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "') GROUP BY EmpId " +
                            " UNION ALL " +
                           " SELECT EmpId,'0'TotalPresent,COUNT(*)TotalAbsent,'0'TotalLate,'0'CasualLeave,'0'MedicalLeave,'0'EarnLeave FROM dbo.tblAttendanceRecord WHERE ATTStatus='A' AND (ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "') GROUP BY EmpId " +
                          "  UNION ALL " +
                          "  SELECT EmpId,'0'TotalPresent,'0'TotalAbsent,COUNT(*)TotalLate,'0'CasualLeave,'0'MedicalLeave,'0'EarnLeave FROM dbo.tblAttendanceRecord WHERE ATTStatus='L' AND Remarks NOT LIKE '%Weekly Holiday%' AND (ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "') GROUP BY EmpId " +
                          "  UNION ALL " +
                         "   SELECT EmpId,'0'TotalPresent,'0'TotalAbsent,'0'TotalLate,COUNT(*)CasualLeave,'0'MedicalLeave,'0'EarnLeave FROM dbo.tblAttendanceRecord WHERE ATTStatus='LV' AND Remarks='Casual' AND (ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "') GROUP BY EmpId " +
                          "  UNION ALL " +
                          "  SELECT EmpId,'0'TotalPresent,'0'TotalAbsent,'0'TotalLate,'0'CasualLeave,COUNT(*)MedicalLeave,'0'EarnLeave FROM dbo.tblAttendanceRecord WHERE ATTStatus='LV' AND Remarks='Medical' AND (ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "') GROUP BY EmpId " +
                          "  UNION ALL " +
                          "  SELECT EmpId,'0'TotalPresent,'0'TotalAbsent,'0'TotalLate,'0'CasualLeave,'0'MedicalLeave,COUNT(*)EarnLeave FROM dbo.tblAttendanceRecord WHERE ATTStatus='LV' AND Remarks='Earn' AND (ATTDate BETWEEN '" + fromdate + "' AND '" + todate + "') GROUP BY EmpId)AS tblTemp  " +
                          "  LEFT JOIN dbo.tblEmpGeneralInfo ON tblTemp.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId " +
                         "   LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId " +
                          "  LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId " +

                         "   GROUP BY EmpInfoId,EmpName,EmpMasterCode,DesigName,DeptName  ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");

        }
        public DataTable Designation()
        {
            string query = @"SELECT * FROM dbo.tblDesignation";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable Temp()
        {
            string query = @"SELECT * FROM tbltemp";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable YearlySalary(string from,string to)
        {
            string query = @"SELECT YEAR(SalaryStartDate)AS YEAR,SUM(Gross)AS Gross FROM dbo.tblSalaryRecordPerMonth WHERE YEAR(SalaryStartDate) BETWEEN '"+from+"' AND '"+to+"' GROUP BY YEAR(SalaryStartDate)";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JanJoin(string desigId,string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='1' AND DesigId='"+desigId+"' AND YEAR(JoiningDate)='"+year+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JanLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='1' AND DesigId='"+desigId+"' AND YEAR(EffectiveDate)='"+year+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable FebJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='2' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable FebLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='2' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable MarJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='3' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable MarLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='3' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AprJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='4' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AprLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='4' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable MayJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='5' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable MayLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='5' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JunJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='6' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JunLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='6' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JulJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='7' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JulLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='7' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AugJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='8' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AugLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='8' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SepJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='9' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SepLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='9' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable OctJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='10' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable OctLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='10' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable NovJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='11' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable NovLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='11' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable DecJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE MONTH(JoiningDate)='12' AND DesigId='" + desigId + "' AND YEAR(JoiningDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable DecLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalLeft FROM dbo.tblJobleft WHERE MONTH(EffectiveDate)='12' AND DesigId='" + desigId + "' AND YEAR(EffectiveDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TotalJoin(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE YEAR(JoiningDate)='"+year+"' AND DesigId='"+desigId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TotalLeft(string desigId, string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END) AS TotalLeft FROM dbo.tblJobleft WHERE YEAR(EffectiveDate)='"+year+"' AND DesigId='"+desigId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AllTotalJoin( string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END)AS TotalJoin FROM dbo.tblEmpGeneralInfo WHERE YEAR(JoiningDate)='" + year + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AllTotalLeft(string year)
        {
            string query = @"SELECT  (CASE WHEN COUNT(*)=0 THEN NULL ELSE COUNT(*) END) AS TotalLeft FROM dbo.tblJobleft WHERE YEAR(EffectiveDate)='" + year + "' ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
