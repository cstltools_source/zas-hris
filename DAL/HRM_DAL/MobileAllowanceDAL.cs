﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class MobileAllowanceDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveMobAllowance(MobileAllowance aMobileAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MobAllowanceId", aMobileAllowance.MobAllowanceId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aMobileAllowance.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@MobAllowanceAmont", aMobileAllowance.MobAllowanceAmont));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aMobileAllowance.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aMobileAllowance.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aMobileAllowance.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aMobileAllowance.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aMobileAllowance.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aMobileAllowance.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aMobileAllowance.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aMobileAllowance.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aMobileAllowance.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aMobileAllowance.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aMobileAllowance.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aMobileAllowance.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aMobileAllowance.IsActive));
            
            string insertQuery = @"insert into tblMobileAllowance (MobAllowanceId,EmpInfoId,MobAllowanceAmont,EffectiveDate,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@MobAllowanceId,@EmpInfoId,@MobAllowanceAmont,@EffectiveDate,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasMobAllowance(MobileAllowance aMobileAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aMobileAllowance.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Month", aMobileAllowance.EffectiveDate.Month));
            aSqlParameterlist.Add(new SqlParameter("@Year", aMobileAllowance.EffectiveDate.Year));
            string query = "select * from tblMobileAllowance where EmpInfoId=@EmpInfoId and IsActive=1 AND MONTH(EffectiveDate)=@Month AND YEAR(EffectiveDate)=@Year AND ActionStatus='Posted'";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadMobAllowanceView()
        {
            string query = @"SELECT  MobAllowanceId,EmpMasterCode ,EmpName , EffectiveDate , MobAllowanceAmont,DesigName ,DeptName,tblMobileAllowance.ActionStatus,tblMobileAllowance.EntryUser,tblMobileAllowance.EntryDate FROM tblMobileAllowance 
                 LEFT JOIN tblEmpGeneralInfo ON tblMobileAllowance.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblMobileAllowance.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblMobileAllowance.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblMobileAllowance.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblMobileAllowance.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblMobileAllowance.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblMobileAllowance.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblMobileAllowance.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblMobileAllowance.DeptId = tblDepartment.DeptId  where tblMobileAllowance.ActionStatus in ('Posted','Cancel') and tblMobileAllowance.IsActive=1 and tblMobileAllowance.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblMobileAllowance.MobAllowanceId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpMobAllowanceInfo(string empInfoId)
        {
            string query = @"SELECT * FROM dbo.tblMobileAllowance WHERE EmpInfoId='" + empInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' and IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public MobileAllowance MobAllowanceEditLoad(string MobAllowanceId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MobAllowanceId", MobAllowanceId));
            string query = "select * from tblMobileAllowance where MobAllowanceId = @MobAllowanceId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            MobileAllowance aMobileAllowance = new MobileAllowance();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aMobileAllowance.MobAllowanceId = Int32.Parse(dataReader["MobAllowanceId"].ToString());
                    aMobileAllowance.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aMobileAllowance.MobAllowanceAmont = Convert.ToDecimal(dataReader["MobAllowanceAmont"].ToString());
                    aMobileAllowance.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aMobileAllowance.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aMobileAllowance.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aMobileAllowance.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aMobileAllowance.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aMobileAllowance.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aMobileAllowance.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aMobileAllowance.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aMobileAllowance.ActionStatus = dataReader["ActionStatus"].ToString();
                    aMobileAllowance.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());

                }
            }
            return aMobileAllowance;
        }

        public bool UpdateMobAllowance(MobileAllowance aMobileAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MobAllowanceId", aMobileAllowance.MobAllowanceId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aMobileAllowance.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@MobAllowanceAmont", aMobileAllowance.MobAllowanceAmont));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aMobileAllowance.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aMobileAllowance.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aMobileAllowance.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aMobileAllowance.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aMobileAllowance.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aMobileAllowance.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aMobileAllowance.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aMobileAllowance.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aMobileAllowance.GradeId));

            string query = @"UPDATE tblMobileAllowance SET EmpInfoId=@EmpInfoId,MobAllowanceAmont=@MobAllowanceAmont,EffectiveDate=@EffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId  WHERE MobAllowanceId=@MobAllowanceId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(MobileAllowance aMobileAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@MobAllowanceId", aMobileAllowance.MobAllowanceId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aMobileAllowance.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aMobileAllowance.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aMobileAllowance.ApprovedDate));

            string query = @"UPDATE tblMobileAllowance SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE MobAllowanceId=@MobAllowanceId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool UpdatePreMobAllowanceDAL(string empInfoId,string effectiveDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", effectiveDate));

            string query = @"UPDATE dbo.tblMobileAllowance SET IsActive=0 WHERE EmpInfoId=@EmpInfoId AND IsActive=1 AND EffectiveDate<@EffectiveDate ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadMobileAllowanceViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From tblMobileAllowance
                            LEFT JOIN tblEmpGeneralInfo ON tblMobileAllowance.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                            where tblMobileAllowance.ActionStatus='" + ActionStatus + "' AND tblMobileAllowance.IsActive=1  order by tblMobileAllowance.MobAllowanceId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));

            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string LId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblMobileAllowance", "MobAllowanceId", LId);
            
        }
    }
}
