﻿using System.Data;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class MultipleReportDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public DataTable RptHeader()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel,'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable CompanyRpt()
        {
            string query = @"SELECT * FROM dbo.tblCompanyInfo";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SalaryGrade()
        {
            string query = @"SELECT * FROM dbo.tblSalaryGradeOrScale   ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable UnitRpt()
        {
            string query = @"SELECT UnitCode,UnitName,UnitAddress,CompanyName,PhoneNo,MobileNo,dbo.tblCompanyUnit.FaxNo FROM dbo.tblCompanyUnit
                             LEFT JOIN dbo.tblCompanyInfo ON dbo.tblCompanyUnit.CompanyInfoId = dbo.tblCompanyInfo.CompanyInfoId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable DivisionRpt()
        {
            string query = @"SELECT * FROM dbo.tblDivision";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable DepartmentRpt()
        {
            string query = @"SELECT * FROM dbo.tblDepartment
                             LEFT JOIN dbo.tblDivision ON dbo.tblDepartment.DivisionId = dbo.tblDivision.DivisionId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        } 
        public DataTable EmpGradeRpt()
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LineRpt()
        {
            string query = @"SELECT * FROM dbo.tblLine ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable DesignationRpt()
        {
            string query = @"SELECT * FROM dbo.tblDesignation ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SectionRpt()
        {
            string query = @"SELECT * FROM dbo.tblSection
                             LEFT JOIN dbo.tblDepartment ON dbo.tblSection.DeptId = dbo.tblDepartment.DeptId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpTypeRpt()
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ShiftRpt()
        {
            string query = @"SELECT * FROM dbo.tblShift";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable UserRpt()
        {
            string query = @"select UserName,Password,tblUser.EmpMasterCode ,CardNo,LoginName
                               from tblUser 
                              inner join tblEmpGeneralInfo on tblUser.EmpMasterCode=tblEmpGeneralInfo.EmpMasterCode";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable EmpAttGroupRpt()
        {
            string query = @"Select tblManualAttEmpWiseGroup.*,tblEmpGeneralInfo.EmpName,tblManualAttGroup.GroupName,
tblEmpGeneralInfo.EmpMasterCode,tblEmpGeneralInfo.EmpMasterCode,tblDesignation.DesigName
from tblManualAttEmpWiseGroup
inner join tblEmpGeneralInfo on tblEmpGeneralInfo.EmpInfoId=tblManualAttEmpWiseGroup.EmpId
inner join tblManualAttGroup on tblManualAttEmpWiseGroup.GroupId=tblManualAttGroup.GroupId
inner join tblDesignation on tblEmpGeneralInfo.DesigId=tblDesignation.DesigId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
