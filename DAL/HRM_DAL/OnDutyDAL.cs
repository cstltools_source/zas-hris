﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class OnDutyDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();

        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        
        public bool SaveDataForOnDuty(OnDuty aOnDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OnDutyId", aOnDuty.OnDutyId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aOnDuty.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@OnDDate", aOnDuty.OnDDate));
            aSqlParameterlist.Add(new SqlParameter("@DutyLocation", aOnDuty.DutyLocation));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aOnDuty.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOnDuty.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aOnDuty.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aOnDuty.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aOnDuty.IsActive));

            string insertQuery = @"insert into tblOnDuty (OnDutyId,EmpInfoId,OnDDate,Purpose,DutyLocation,ActionStatus,EntryDate,EntryUser,IsActive) 
            values (@OnDutyId,@EmpInfoId,@OnDDate,@Purpose,@DutyLocation,@ActionStatus,@EntryDate,@EntryUser,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public DataTable LoadOnDutyViewForApproval(string ActionStatus)
        {
            string query = @"SELECT *,DesigName,DeptName From tblOnDuty
                LEFT JOIN tblEmpGeneralInfo ON tblOnDuty.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                LEFT JOIN tblDesignation ON tblEmpGeneralInfo.DesigId = tblDesignation.DesigId 
                LEFT JOIN tblDepartment ON tblEmpGeneralInfo.DepId = tblDepartment.DeptId 
                 where tblOnDuty.ActionStatus='" + ActionStatus + "' AND tblOnDuty.IsActive=1  order by tblOnDuty.OnDutyId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadOnDutyView()
        {
            string query = @"SELECT *,DesigName,DeptName From tblOnDuty
                LEFT JOIN tblEmpGeneralInfo ON tblOnDuty.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                LEFT JOIN tblDesignation ON tblEmpGeneralInfo.DesigId = tblDesignation.DesigId 
                LEFT JOIN tblDepartment ON tblEmpGeneralInfo.DepId = tblDepartment.DeptId 
                where tblOnDuty.ActionStatus in ('Posted','Cancel') and tblOnDuty.IsActive=1 and tblOnDuty.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblOnDuty.OnDutyId desc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public OnDuty OnDutyEditLoad(string OnDutyId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OnDutyId", OnDutyId));
            string query = "select * from tblOnDuty where OnDutyId = @OnDutyId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            OnDuty aOnDuty = new OnDuty();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aOnDuty.OnDutyId = Int32.Parse(dataReader["OnDutyId"].ToString());
                    aOnDuty.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aOnDuty.OnDDate = Convert.ToDateTime(dataReader["OnDDate"].ToString());
                    aOnDuty.DutyLocation = dataReader["DutyLocation"].ToString();
                    aOnDuty.Purpose = dataReader["Purpose"].ToString();
                    aOnDuty.ActionStatus = dataReader["ActionStatus"].ToString();
                    aOnDuty.ActionRemarks = dataReader["ActionRemarks"].ToString();
                    aOnDuty.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                }
            }
            return aOnDuty;
        }

        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool UpdateOnDuty(OnDuty aOnDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OnDutyId", aOnDuty.OnDutyId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aOnDuty.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@OnDDate", aOnDuty.OnDDate));
            aSqlParameterlist.Add(new SqlParameter("@DutyLocation", aOnDuty.DutyLocation));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aOnDuty.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@ActionRemarks", aOnDuty.ActionRemarks));

            string query = @"UPDATE tblOnDuty SET EmpInfoId=@EmpInfoId,DutyLocation=@DutyLocation,OnDDate=@OnDDate,Purpose=@Purpose,ActionRemarks=@ActionRemarks  WHERE OnDutyId=@OnDutyId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }


        public bool ApprovalUpdateDAL(OnDuty aOnDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OnDutyId", aOnDuty.OnDutyId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOnDuty.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedUser", aOnDuty.ApprovedUser));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aOnDuty.ApprovedDate));

            string query = @"UPDATE tblOnDuty SET ActionStatus=@ActionStatus,ApprovedUser=@ApprovedUser,ApprovedDate=@ApprovedDate WHERE OnDutyId=@OnDutyId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");


        }
        public bool DeleteData(string OnDutyId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblOnDuty", "OnDutyId", OnDutyId);
        }
    }
}
