﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class OtherAllowanceDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveOtherAllowances(OtherAllowance aOtherAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OtherAllowanceId", aOtherAllowance.OtherAllowanceId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aOtherAllowance.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aOtherAllowance.Amount));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aOtherAllowance.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aOtherAllowance.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aOtherAllowance.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOtherAllowance.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aOtherAllowance.IsActive));

            string insertQuery = @"insert into tblOtherAllowance (OtherAllowanceId,EmpInfoId,Amount,EffectiveDate,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@OtherAllowanceId,@EmpInfoId,@Amount,@EffectiveDate,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasAmount(OtherAllowance aOtherAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aOtherAllowance.EmpInfoId));
            string query = "select * from tblOtherAllowance where EmpInfoId = @EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadOtherAllowanceView()
        {
            string query = @"SELECT * FROM dbo.tblOtherAllowance
                           LEFT join dbo.tblEmpGeneralInfo ON dbo.tblOtherAllowance.EmpInfoId=dbo.tblEmpGeneralInfo.EmpInfoId 
                           LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                           where tblOtherAllowance.ActionStatus in ('Posted','Cancel') and tblOtherAllowance.IsActive=1 and tblOtherAllowance.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblOtherAllowance.OtherAllowanceId desc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string empcode)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + empcode + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpDept(string sectionId)
        {
            string query = @"SELECT EmpInfoId,EmpMasterCode,EmpName,DeptName FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId= dbo.tblDepartment.DeptId WHERE SectionId='"+sectionId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public OtherAllowance OtherAllowanceEditLoad(string OtherAllowanceId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OtherAllowanceId", OtherAllowanceId));
            string query = "select * from tblOtherAllowance where OtherAllowanceId = @OtherAllowanceId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            OtherAllowance aOtherAllowance = new OtherAllowance();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aOtherAllowance.OtherAllowanceId = Int32.Parse(dataReader["OtherAllowanceId"].ToString());
                    aOtherAllowance.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aOtherAllowance.Amount = Convert.ToDecimal(dataReader["Amount"].ToString());
                    aOtherAllowance.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    aOtherAllowance.EntryUser = dataReader["EntryUser"].ToString();
                    aOtherAllowance.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                }
            }
            return aOtherAllowance;
        }

        public bool UpdateOtherAllowance(OtherAllowance aOtherAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OtherAllowanceId", aOtherAllowance.OtherAllowanceId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aOtherAllowance.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aOtherAllowance.Amount));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aOtherAllowance.EffectiveDate));

            string query = @"UPDATE tblOtherAllowance SET Amount=@Amount,EmpInfoId=@EmpInfoId,EffectiveDate=@EffectiveDate WHERE OtherAllowanceId=@OtherAllowanceId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDALL(OtherAllowance aOtherAllowance)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@OtherAllowanceId", aOtherAllowance.OtherAllowanceId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOtherAllowance.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aOtherAllowance.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aOtherAllowance.ApprovedDate));

            string query = @"UPDATE tblOtherAllowance SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE OtherAllowanceId=@OtherAllowanceId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadOtherAllowanceViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From tblOtherAllowance
                LEFT JOIN tblEmpGeneralInfo ON tblOtherAllowance.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblOtherAllowance.ActionStatus='" + ActionStatus + "' AND tblOtherAllowance.IsActive=1  order by tblOtherAllowance.OtherAllowanceId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteData(string OtherAllowanceId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblOtherAllowance", "OtherAllowanceId", OtherAllowanceId);
        }
    }
}
