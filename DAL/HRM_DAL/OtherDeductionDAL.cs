﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class OtherDeductionDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveOtherDeduction(OtherDeduction aOtherDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ODId", aOtherDeduction.ODId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aOtherDeduction.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@ODAmount", aOtherDeduction.ODAmount));
            aSqlParameterlist.Add(new SqlParameter("@ODReason", aOtherDeduction.ODReason));
            aSqlParameterlist.Add(new SqlParameter("@ODEffectiveDate", aOtherDeduction.ODEffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aOtherDeduction.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aOtherDeduction.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aOtherDeduction.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aOtherDeduction.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aOtherDeduction.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aOtherDeduction.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aOtherDeduction.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aOtherDeduction.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aOtherDeduction.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aOtherDeduction.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOtherDeduction.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aOtherDeduction.IsActive));


            string insertQuery = @"insert into tblOtherDeduction (ODId,EmpId,ODAmount,ODReason,ODEffectiveDate,CompanyInfoId,UnitId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,DivisionId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@ODId,@EmpId,@ODAmount,@ODReason,@ODEffectiveDate,@CompanyInfoId,@UnitId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@EmpGradeId,@DivisionId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public void LoadSalHeadName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryHead";
            aInternalDal.LoadDropDownValue(ddl, "SalaryName", "SalaryHeadId", queryStr, "HRDB");
        }
        public DataTable LoadOtherDeductionView()
        {
            string query = @"SELECT  ODId,EmpMasterCode ,EmpName , ODEffectiveDate , ODAmount,DesigName FROM tblOtherDeduction 
                 LEFT JOIN tblEmpGeneralInfo ON tblOtherDeduction.EmpId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblOtherDeduction.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblOtherDeduction.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblOtherDeduction.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblOtherDeduction.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblOtherDeduction.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblOtherDeduction.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblOtherDeduction.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblOtherDeduction.DeptId = tblDepartment.DeptId  where tblOtherDeduction.ActionStatus<>'Accepted' AND tblOtherDeduction.IsActive=1 order by tblOtherDeduction.ODId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadOtherDeductionViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From tblOtherDeduction
                LEFT JOIN tblEmpGeneralInfo ON tblOtherDeduction.EmpId = tblEmpGeneralInfo.EmpInfoId where tblOtherDeduction.ActionStatus='" + ActionStatus + "' AND tblOtherDeduction.IsActive=1  order by tblOtherDeduction.ODId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public OtherDeduction OtherDeductionEditLoad(string ODId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ODId", ODId));
            string query = "select * from tblOtherDeduction where ODId = @ODId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            OtherDeduction aOtherDeduction = new OtherDeduction();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aOtherDeduction.ODId = Int32.Parse(dataReader["ODId"].ToString());
                    aOtherDeduction.EmpId = Convert.ToInt32(dataReader["EmpId"].ToString());
                    aOtherDeduction.ODAmount = Convert.ToDecimal(dataReader["ODAmount"].ToString());
                    aOtherDeduction.ODReason = dataReader["ODReason"].ToString();
                    aOtherDeduction.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aOtherDeduction.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aOtherDeduction.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aOtherDeduction.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aOtherDeduction.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aOtherDeduction.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aOtherDeduction.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aOtherDeduction.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aOtherDeduction.ActionStatus = dataReader["ActionStatus"].ToString();
                    aOtherDeduction.ActionStatus = dataReader["ActionStatus"].ToString();
                    aOtherDeduction.EntryUser = dataReader["EntryUser"].ToString();
                    aOtherDeduction.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    aOtherDeduction.ODEffectiveDate = Convert.ToDateTime(dataReader["ODEffectiveDate"].ToString());

                }
            }
            return aOtherDeduction;
        }

        public bool UpdateOtherDeduction(OtherDeduction aOtherDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ODId", aOtherDeduction.ODId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aOtherDeduction.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@ODAmount", aOtherDeduction.ODAmount));
            aSqlParameterlist.Add(new SqlParameter("@ODReason", aOtherDeduction.ODReason));
            aSqlParameterlist.Add(new SqlParameter("@ODEffectiveDate", aOtherDeduction.ODEffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aOtherDeduction.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aOtherDeduction.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aOtherDeduction.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aOtherDeduction.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aOtherDeduction.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aOtherDeduction.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aOtherDeduction.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aOtherDeduction.EmpGradeId));

            string query = @"UPDATE tblOtherDeduction SET EmpId=@EmpId,ODAmount=@ODAmount,ODReason=@ODAmount,ODEffectiveDate=@ODEffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@EmpGradeId,DivisionId=@DivisionId WHERE ODId=@ODId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(OtherDeduction aOtherDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ODId", aOtherDeduction.ODId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOtherDeduction.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aOtherDeduction.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aOtherDeduction.ApprovedDate));

            string query = @"UPDATE tblOtherDeduction SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE ODId=@ODId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteOtherDeduction(string ODId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblOtherDeduction", "ODId", ODId);
            
        }
    }
}
