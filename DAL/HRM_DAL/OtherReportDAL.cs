﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class OtherReportDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public DataTable LateApproveRpt(string parameter)
        {
            string query = @"SELECT * ,datediff( minute, ShiftIntime,AttIntime) as TotalLatetime , CONVERT(VARCHAR(10), LateDate, 6) as AttLateDate FROM dbo.tblLateApprove
                 LEFT JOIN tblEmpGeneralInfo ON tblLateApprove.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblLateApprove.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblLateApprove.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblLateApprove.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblLateApprove.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeType ON tblLateApprove.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblLateApprove.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblLateApprove.DeptId = tblDepartment.DeptId   " + parameter+" ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable MobileAllowance(string parameter)
        {
            string query = @"SELECT * ,MobAllowanceAmont AS MobileAllowance FROM dbo.tblMobileAllowance
                LEFT JOIN tblEmpGeneralInfo ON tblMobileAllowance.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblMobileAllowance.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblMobileAllowance.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblMobileAllowance.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblMobileAllowance.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblMobileAllowance.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblMobileAllowance.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblMobileAllowance.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblMobileAllowance.DeptId = tblDepartment.DeptId   " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SalaryGrade(string parameter)
        {
            string query = @"SELECT * FROM dbo.tblSalaryGradeOrScale   " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SalaryDeduction(string parameter)
        {
            string query = @"SELECT * FROM dbo.tblSalaryDeduction
                 LEFT JOIN tblEmpGeneralInfo ON tblSalaryDeduction.EmpId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblSalaryDeduction.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblSalaryDeduction.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblSalaryDeduction.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblSalaryDeduction.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblSalaryDeduction.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblSalaryDeduction.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblSalaryDeduction.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblSalaryDeduction.DeptId = tblDepartment.DeptId   " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ArrearRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10), EffectiveDate, 6) as EffectiveDate1 FROM dbo.tblArrear
	                 LEFT JOIN tblEmpGeneralInfo ON tblArrear.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblArrear.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblArrear.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblArrear.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblArrear.SectionId = tblSection.SectionId  
					 LEFT JOIN tblEmployeeGrade ON tblArrear.EmpGradeId = tblEmployeeGrade.GradeId  
					 LEFT JOIN tblEmployeeType ON tblArrear.EmpTypeId = tblEmployeeType.EmpTypeId  
					 LEFT JOIN tblDesignation ON tblArrear.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblArrear.DeptId = tblDepartment.DeptId   " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable IncrementRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10),tblIncrement.ActiveDate, 6) as ActiveDate1,(IncrementAmount+PreviousGrossSalary) AS CurrentGrossSalary  FROM dbo.tblIncrement
	                 LEFT JOIN tblEmpGeneralInfo ON tblIncrement.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblIncrement.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblIncrement.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblIncrement.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblIncrement.SectionId = tblSection.SectionId
                     LEFT JOIN tblSalaryGradeOrScale ON tblIncrement.SalScaleId = tblSalaryGradeOrScale.SalScaleId   
					 LEFT JOIN tblDesignation ON tblIncrement.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblIncrement.DeptId = tblDepartment.DeptId     " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ManualAttRpt(string parameter)
        {
            string query = @" SELECT *,CONVERT(VARCHAR(10),tblManualAttendence.ATTDate, 6) as ATTDate1,CONVERT(VARCHAR(10),tblManualAttendence.EntryDate, 6) as EntryDate1,CONVERT(VARCHAR(10),tblManualAttendence.ApproveDate, 6) as ApproveDate1 FROM dbo.tblManualAttendence
	                 LEFT JOIN tblEmpGeneralInfo ON tblManualAttendence.EmpInfoId = tblEmpGeneralInfo.EmpInfoId " + parameter + " order by blManualAttendence.ATTDate ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
      
        public DataTable UserRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10),tblManualAttendence.ATTDate, 6) as ATTDate1,CONVERT(VARCHAR(10),tblManualAttendence.EntryDate, 6) as EntryDate1,CONVERT(VARCHAR(10),tblManualAttendence.ApproveDate, 6) as ApproveDate1 FROM dbo.tblManualAttendence
	                 LEFT JOIN tblEmpGeneralInfo ON tblManualAttendence.EmpInfoId = tblEmpGeneralInfo.EmpInfoId  " + parameter + " order by tblManualAttendence.ATTDate ASC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JobleftRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10),tblJobleft.EffectiveDate, 6) as EffectiveDate1 FROM dbo.tblJobleft
	                 LEFT JOIN tblEmpGeneralInfo ON tblJobleft.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblJobleft.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblJobleft.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblJobleft.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblJobleft.SectionId = tblSection.SectionId
                     LEFT JOIN tblEmployeeGrade ON tblJobleft.GradeId = tblEmployeeGrade.GradeId 
			     	 LEFT JOIN tblEmployeeType ON tblJobleft.EmpTypeId = tblEmployeeType.EmpTypeId 
					 LEFT JOIN tblDesignation ON tblJobleft.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblJobleft.DeptId = tblDepartment.DeptId     " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable HolidayRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10),tblHolidayInformation.HolidayDate, 6) as HolidayDate1 FROM dbo.tblHolidayInformation
	                    " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable AllEmpinfo()
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo WHERE EmployeeStatus='Active'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ReappointmentRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10),tblReApointment.EffectiveDate, 6) as EffectiveDate1 FROM dbo.tblReApointment
	                 LEFT JOIN tblEmpGeneralInfo ON tblReApointment.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblReApointment.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblReApointment.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblReApointment.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblReApointment.SectionId = tblSection.SectionId
                      LEFT JOIN tblEmployeeGrade ON tblReApointment.EmpGradeId = tblEmployeeGrade.GradeId 
			     	 LEFT JOIN tblEmployeeType ON tblReApointment.EmpTypeId = tblEmployeeType.EmpTypeId 
					 LEFT JOIN tblDesignation ON tblReApointment.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblReApointment.DeptId = tblDepartment.DeptId      " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SuspendRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10),tblSuspend.EffectiveDate, 6) as EffectiveDate1 FROM dbo.tblSuspend
	                 LEFT JOIN tblEmpGeneralInfo ON tblSuspend.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblSuspend.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblSuspend.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblSuspend.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblSuspend.SectionId = tblSection.SectionId
					 LEFT JOIN tblDesignation ON tblSuspend.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblSuspend.DeptId = tblDepartment.DeptId
					  LEFT JOIN tblEmployeeGrade ON tblSuspend.EmpGradeId = tblEmployeeGrade.GradeId
					   LEFT JOIN tblEmployeeType ON tblSuspend.EmpTypeId = tblEmployeeType.EmpTypeId     " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ReappontmentRpt(string parameter)
        {
            string query = @"SELECT *,CONVERT(VARCHAR(10),tblReApointment.EffectiveDate, 6) as EffectiveDate1 FROM dbo.tblReApointment
	                 LEFT JOIN tblEmpGeneralInfo ON tblReApointment.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblReApointment.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblReApointment.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblReApointment.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblReApointment.SectionId = tblSection.SectionId  
					 LEFT JOIN tblEmployeeGrade ON tblReApointment.EmpGradeId = tblEmployeeGrade.GradeId  
					 LEFT JOIN tblEmployeeType ON tblReApointment.EmpTypeId = tblEmployeeType.EmpTypeId  
					 LEFT JOIN tblDesignation ON tblReApointment.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblReApointment.DeptId = tblDepartment.DeptId   " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TransferRpt(string parameter)
        {
            string query = @"SELECT *,NewUnit.UnitName as NUnit,NewDivision.DivName as NDivision,NewSection.SectionName as NSection,NewDepartment.DeptName as NDepartment,CONVERT(VARCHAR(10), EffectiveDate, 6) as EffectiveDate1,DesigName FROM dbo.tblTransfer
	                 LEFT JOIN tblEmpGeneralInfo ON tblTransfer.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblTransfer.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
			    	 LEFT JOIN tblCompanyUnit  ON tblTransfer.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblCompanyUnit NewUnit ON tblTransfer.NewUnitId = NewUnit.UnitId 
					 LEFT JOIN tblDivision ON tblTransfer.DivisionId = tblDivision.DivisionId 
					 LEFT JOIN tblDivision NewDivision ON tblTransfer.NewDivisionId = NewDivision.DivisionId  
					 LEFT JOIN tblSection ON tblTransfer.SectionId = tblSection.SectionId 
				     LEFT JOIN tblSection NewSection ON tblTransfer.NewSectionId = NewSection.SectionId  
				    LEFT JOIN tblDepartment ON tblTransfer.DeptId = tblDepartment.DeptId
			      	  LEFT JOIN tblDepartment NewDepartment  ON tblTransfer.NewDeptId = NewDepartment.DeptId 
			      	  LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TaxRpt(string parameter)
        {
            string query = @"SELECT * FROM dbo.tblTax
	                 LEFT JOIN tblEmpGeneralInfo ON tblTax.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
					 LEFT JOIN tblCompanyInfo ON tblTax.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblTax.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblTax.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblTax.SectionId = tblSection.SectionId  
					 LEFT JOIN tblEmployeeGrade ON tblTax.EmpGradeId = tblEmployeeGrade.GradeId  
					 LEFT JOIN tblEmployeeType ON tblTax.EmpTypeId = tblEmployeeType.EmpTypeId  
					 LEFT JOIN tblDesignation ON tblTax.DesigId = tblDesignation.DesigId  
					 LEFT JOIN tblDepartment ON tblTax.DeptId = tblDepartment.DeptId  " + parameter + " ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable RptHeader()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel,'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable PromotionRptDAL(string ReportParameter)
        {
            string query = @"SELECT  *,PromotionId,EmpMasterCode ,EmpName ,tblSalaryGradeOrScale.SalScaleName as SalScaleName,tblDesignation.DesigName as DesigName,tblEmployeeGrade.GradeName as GradeName, EffectiveDate,tblPromotion.SalScaleId,
                            tblPromotion.EmpGradeId,tblPromotion.DesigId,salgrad.SalScaleName as NewSalScaleName,
                            grade.GradeName as NewEmpGrade, deg1.DesigName as NewDesig,tblPromotion.ActionStatus, tblCompanyUnit.UnitName,tblDivision.DivName,tblSection.SectionName,tblDepartment.DeptName, CONVERT(VARCHAR(10), EffectiveDate, 6) as PromotionDate
                 FROM tblPromotion 
                 LEFT JOIN tblEmpGeneralInfo ON tblPromotion.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN dbo.tblSalaryGradeOrScale ON tblPromotion.SalScaleId = dbo.tblSalaryGradeOrScale.SalScaleId
                 LEFT JOIN dbo.tblSalaryGradeOrScale salgrad ON tblPromotion.NewSalScaleId = salgrad.SalScaleId
                 LEFT JOIN dbo.tblDesignation ON tblPromotion.DesigId = dbo.tblDesignation.DesigId
                 LEFT JOIN dbo.tblDesignation deg1 ON tblPromotion.NewDesigId = deg1.DesigId
                 LEFT JOIN dbo.tblEmployeeGrade ON tblPromotion.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                 LEFT JOIN dbo.tblEmployeeGrade grade ON tblPromotion.NewEmpGradeId = grade.GradeId 
                   LEFT JOIN tblCompanyInfo ON tblPromotion.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
					 LEFT JOIN tblCompanyUnit ON tblPromotion.UnitId = tblCompanyUnit.UnitId 
					 LEFT JOIN tblDivision ON tblPromotion.DivisionId = tblDivision.DivisionId  
					 LEFT JOIN tblSection ON tblPromotion.SectionId = tblSection.SectionId  
					 LEFT JOIN tblDepartment ON tblPromotion.DeptId = tblDepartment.DeptId" + ReportParameter;
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoanRptDAL(string ReportParameter)
        {
            string query = @"SELECT  *  ,CONVERT(VARCHAR(10), SanctionDate, 6) as SanctionDate1,CONVERT(VARCHAR(10), DeductionStartDate, 6) as DeductionStartDate1,DATEADD(MONTH,TotalInstallment-1,DeductionStartDate) as DeductionEndDate
                 FROM tblLoanMaster 
                 LEFT JOIN tblEmpGeneralInfo ON tblLoanMaster.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN dbo.tblDesignation ON tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
			     LEFT JOIN tblDepartment ON dbo.tblEmpGeneralInfo.DepId = tblDepartment.DeptId " + ReportParameter;
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoanRptDALNew(string ReportParameter)
        {
            string query = @"SELECT Tbltemp.EmpInfoId,SUM(Tbltemp.LoanAmount) AS LoanAmount,SUM(Tbltemp.InstallAmount) AS InstallAmount
                        ,SUM(DeducAmount) AS  DeducAmount,(SUM(Tbltemp.LoanAmount)-SUM(DeducAmount)) AS RemainAmount,DeptName,DesigName,"+
                        "  EmpMasterCode,EmpName , '" + HttpContext.Current.Session["LoginName"] + "' as LoginName,'Accepted' as Status " +
                        " FROM (SELECT  EmpInfoId,LoanAmount,InstallAmount,'0' AS DeducAmount  FROM tblLoanMaster WHERE Status='Accepted' "+

                        " UNION ALL "+

                        " SELECT EmpInfoId,'0' AS LoanAmount,'0' AS InstallAmount,SUM(SalaryAdvance) AS  DeducAmount FROM dbo.tblSalaryRecordPerMonth GROUP BY EmpInfoId)AS Tbltemp "+
                        //" LEFT JOIN dbo.tblLoanMaster ON Tbltemp.EmpInfoId = dbo.tblLoanMaster.EmpInfoId "+
                        " LEFT JOIN dbo.tblEmpGeneralInfo ON Tbltemp.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId " +
                        " LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId "+
                        " LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId " +
                        " WHERE EmployeeStatus='Active' "+ReportParameter+" "+
                         " GROUP BY Tbltemp.EmpInfoId,DeptName,DesigName,EmpMasterCode,EmpName HAVING SUM(Tbltemp.LoanAmount)<>0 AND (SUM(Tbltemp.LoanAmount)-SUM(DeducAmount))<>0 ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoanAmount(string empinfoId, string month,string year)
        {
            string query = @"SELECT SalaryAdvance FROM dbo.tblSalaryRecordPerMonth WHERE MONTH(SalaryStartDate)='" + month + "' AND EmpInfoId='" + empinfoId + "' AND YEAR(SalaryStartDate)='" + year + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetSancDate(string empinfoId)
        {
            string query = @"SELECT SanctionDate,DeductionStartDate FROM dbo.tblLoanMaster WHERE EmpInfoId='" + empinfoId + "' ORDER BY SanctionDate DESC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetLoanAvailEmployee(string year)
        {
            string query = @"SELECT DISTINCT EG.EmpInfoId,EmpName,EmpMasterCode,DeptName,DesigName FROM dbo.tblLoanMaster LM
                            INNER JOIN dbo.tblLoanDetail LD ON LM.LoanId = LD.LoanId
                            INNER JOIN dbo.tblEmpGeneralInfo EG ON LM.EmpInfoId=EG.EmpInfoId
                            INNER JOIN dbo.tblDepartment DP ON EG.DepId = DP.DeptId
                            INNER JOIN dbo.tblDesignation DG ON EG.DesigId=DG.DesigId
                            WHERE year(LD.InstallmentDate)='"+year+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetPrevYearData(string year,string empinfoId)
        {
            string query = @"SELECT  LM.EmpInfoId,SUM(Amount)ClosingBalance FROM dbo.tblLoanMaster LM" +
                            " INNER JOIN dbo.tblLoanDetail LD ON LM.LoanId = LD.LoanId"+
                            " WHERE year(LD.InstallmentDate)<='"+(Convert.ToInt32(year)-1)+"' AND LM.LoanId IN (SELECT DISTINCT LM.LoanId FROM dbo.tblLoanMaster LM"+
                            " INNER JOIN dbo.tblLoanDetail LD ON LM.LoanId = LD.LoanId"+
                            " WHERE year(LD.InstallmentDate)='" + year + "') AND LM.EmpInfoId='" + empinfoId + "' AND LM.Status='Accepted' " +
                            " GROUP BY LM.EmpInfoId ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable GetPrevYearLoan(string year, string empinfoId)
        {
            string query = @"SELECT  EmpInfoId,SUM(LoanAmount) FROM dbo.tblLoanMaster LM
                            WHERE year(LM.SanctionDate)<'"+year+"' AND LM.LoanId IN (SELECT DISTINCT LM.LoanId FROM dbo.tblLoanMaster LM "+
                            " INNER JOIN dbo.tblLoanDetail LD ON LM.LoanId = LD.LoanId"+
                            " WHERE year(LD.InstallmentDate)='" + year + "') AND LM.EmpInfoId='" + empinfoId + "' AND LM.Status='Accepted' " +
                            " GROUP BY LM.EmpInfoId ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable ThisYearLoanAmount(string year, string empinfoId)
        {
            string query = @"SELECT  EmpInfoId,SUM(LoanAmount) FROM dbo.tblLoanMaster LM
                            WHERE year(LM.SanctionDate)='" + year + "' AND EmpInfoId='"+empinfoId+"' AND Status='Accepted' GROUP BY LM.EmpInfoId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable MainLoanAmount( string empinfoId)
        {
            string query = @"SELECT * FROM dbo.tblLoanMaster WHERE EmpInfoId='" + empinfoId + "' ORDER BY LoanId DESC ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TotalAmountDeducttillmonth(string empinfoId,string startdate,string enddate)
        {
            string query = @"SELECT SUM(SalaryAdvance) FROM dbo.tblSalaryRecordPerMonth WHERE EmpInfoId='"+empinfoId+"' AND SalaryStartDate BETWEEN '" + startdate + "' AND '" + enddate + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoanRptDALSingle(string ReportParameter, string empmastercode, string fromdate, string todate)
        {
            string query = @"SELECT dbo.tblEmpGeneralInfo.EmpMasterCode,EmpName,DesigName,DeptName,DeductionStartDate,SanctionDate,LoanAmount,InstallAmount,TotalInstallment,DATENAME(month, SalaryStartDate) AS MonthName,DATENAME(year, SalaryStartDate) AS YearName,SalaryAdvance AS DeducAmount,LoanType ,'"+HttpContext.Current.Session["LoginName"]+"' as LoginName FROM dbo.tblSalaryRecordPerMonth"+
                        " LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryRecordPerMonth.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId"+
                        " LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId"+
                        " LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId"+
                        " LEFT JOIN dbo.tblLoanMaster ON dbo.tblEmpGeneralInfo.EmpInfoId=dbo.tblLoanMaster.EmpInfoId WHERE DeductionStartDate<>''  AND dbo.tblEmpGeneralInfo.EmpMasterCode='" + empmastercode + "' AND SalaryStartDate BETWEEN SanctionDate AND DATEADD(MONTH,TotalInstallment-1,DeductionStartDate) " + ReportParameter;
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable TaxRpt(string fromdate, string todate)
        {
            string query = @"SELECT dbo.tblEmpGeneralInfo.EmpMasterCode,EmpName,DeptName,DesigName,UnitName,SUM(Tax) AS Tax ,dbo.tblEmpGeneralInfo.JoiningDate,'"+HttpContext.Current.Session["LoginName"]+"' as LoginName FROM dbo.tblSalaryRecordPerMonth "+
                            " LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryRecordPerMonth.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId "+
                            " LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId "+
                            " LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId "+
                            " LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId" +
                             " WHERE SalaryStartDate BETWEEN '"+fromdate+"' AND '"+todate+"' "+
                             " GROUP BY dbo.tblEmpGeneralInfo.EmpMasterCode,EmpName,DeptName,DesigName,UnitName,dbo.tblEmpGeneralInfo.JoiningDate HAVING SUM(Tax)<>0";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SalaryInfo(string empcode)
        {
            string query = @"SELECT * FROM dbo.tblSalaryInformation 
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblSalaryInformation.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                            WHERE EmpMasterCode='" + empcode + "' AND tblSalaryInformation.ActionStatus='Accepted' AND dbo.tblSalaryInformation.IsActive='True'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable PreSalaryInfo(string empcode,string row,string salheadId)
        {
            string query = @"SELECT * FROM (SELECT *,  ROW_NUMBER() OVER (order by SalaryInfoId desc) AS Row   FROM dbo.tblSalaryInformation WHERE EmpInfoId='" + empcode + "' AND dbo.tblSalaryInformation.IsActive=0 ) AS tbltemp WHERE Row<" + row + " AND tbltemp.SalaryHeadId='" + salheadId + "' ORDER BY SalaryInfoId DESC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        
        public DataTable Promotion(string empcode,string fromdt,string todt,string actionstatus)
        {
            string query = @"SELECT D.DesigName AS PreDesigName,ND.DesigName AS NewDesigName,EG.GradeName AS PreGradeName,NEG.GradeName AS NewGradeName FROM dbo.tblPromotion
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblPromotion.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDesignation D ON dbo.tblPromotion.DesigId=D.DesigId
                            LEFT JOIN dbo.tblDesignation ND ON dbo.tblPromotion.NewDesigId=ND.DesigId
                            LEFT JOIN dbo.tblEmployeeGrade EG ON dbo.tblPromotion.EmpGradeId=EG.GradeId
                            LEFT JOIN dbo.tblEmployeeGrade NEG ON dbo.tblPromotion.NewEmpGradeId=NEG.GradeId
                            WHERE EmpMasterCode='" + empcode + "' AND dbo.tblPromotion.ActionStatus='"+actionstatus+"' AND dbo.tblPromotion.IsActive='True' AND EffectiveDate BETWEEN '"+fromdt+"' AND '"+todt+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable IncrementAmount(string empcode, string salheadId, string fromdt, string todt, string actionstatus)
        {
            string query = @"SELECT * FROM dbo.tblIncrement 
                            LEFT JOIN dbo.tblIncrementDetails ON dbo.tblIncrement.IncrementId = dbo.tblIncrementDetails.IncrementId
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblIncrement.EmpInfoId=dbo.tblEmpGeneralInfo.EmpInfoId
                            WHERE EmpMasterCode='" + empcode + "' AND SalaryHeadId='" + salheadId + "' AND dbo.tblIncrement.ActionStatus='" + actionstatus + "' AND dbo.tblIncrement.ActiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' AND dbo.tblIncrement.IsActive='True' ORDER BY dbo.tblIncrement.IncrementId DESC";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable IncrementWithPromotion(string empcode, string salheadId, string fromdt, string todt, string actionstatus)
        {
            string query = @"SELECT Amount,D.DesigName AS PreDesigName,ND.DesigName AS NewDesigName,EG.GradeName AS PreGradeName,NEG.GradeName AS NewGradeName

                             FROM dbo.tblIncrement
                            LEFT JOIN dbo.tblIncrementDetails ON dbo.tblIncrement.IncrementId = dbo.tblIncrementDetails.IncrementId
                            LEFT JOIN dbo.tblPromotion ON dbo.tblIncrement.PromotionId=dbo.tblPromotion.PromotionId
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblPromotion.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDesignation D ON dbo.tblPromotion.DesigId=D.DesigId
                            LEFT JOIN dbo.tblDesignation ND ON dbo.tblPromotion.NewDesigId=ND.DesigId
                            LEFT JOIN dbo.tblEmployeeGrade EG ON dbo.tblPromotion.EmpGradeId=EG.GradeId
                            LEFT JOIN dbo.tblEmployeeGrade NEG ON dbo.tblPromotion.NewEmpGradeId=NEG.GradeId
                            WHERE tblEmpGeneralInfo.EmpMasterCode='" + empcode + "' AND tblIncrementDetails.SalaryHeadId='" + salheadId + "' AND dbo.tblIncrement.ActionStatus='" + actionstatus + "' AND dbo.tblIncrement.ActiveDate BETWEEN '" + fromdt + "' AND '" + todt + "' AND dbo.tblIncrement.IsActive='True'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadLoan(DropDownList ddl,string empMasterCode)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = @"SELECT ('Loan'+'-'+CAST((ROW_NUMBER() OVER(ORDER BY LoanId DESC)) AS NVARCHAR(MAX))) AS Row ,LoanId FROM dbo.tblLoanMaster
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblLoanMaster.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                             WHERE  Status='Accepted' AND EmpMasterCode='"+empMasterCode+"'";
            aInternalDal.LoadDropDownValue(ddl, "Row", "LoanId", queryStr, "HRDB");
        }
        
    }
}
