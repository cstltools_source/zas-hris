﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class PFSetupDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }

        public bool SavePFSetup(PFSetup aPfSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PFSId", aPfSetup.PFSId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aPfSetup.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aPfSetup.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aPfSetup.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aPfSetup.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aPfSetup.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aPfSetup.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aPfSetup.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aPfSetup.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aPfSetup.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aPfSetup.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aPfSetup.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aPfSetup.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aPfSetup.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@PFActiveDate", aPfSetup.PFActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@PreBalance", aPfSetup.PreBalance));
            aSqlParameterlist.Add(new SqlParameter("@EmpBalance", (object)(aPfSetup.EmpBalance ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@CompanyBalance", (object)(aPfSetup.CompanyBalance ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@PFStartDate", (object)(aPfSetup.PFStartDate ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@InterBalance", (object)(aPfSetup.InterBalance ?? (object)DBNull.Value)));



            string insertQuery = @"INSERT INTO dbo.tblPFSetup
                                ( PFSId ,
                                  EmpInfoId ,
                                  CompanyInfoId ,
                                  UnitId ,
                                  DivisionId ,
                                  DeptId ,
                                  SectionId ,
                                  DesigId ,
                                  EmpTypeId ,
                                  EmpGradeId ,
                                  PFActiveDate ,
                                  PreBalance ,
                                  EmpBalance ,
                                  CompanyBalance ,
                                  InterBalance,
                                  PFStartDate ,
                                  ActionStatus ,
                                  EntryUser ,
                                  EntryDate ,
                                  IsActive
                                )
                        VALUES  ( @PFSId ,
                                  @EmpInfoId ,
                                  @CompanyInfoId ,
                                  @UnitId ,
                                  @DivisionId ,
                                  @DeptId ,
                                  @SectionId ,
                                  @DesigId ,
                                  @EmpTypeId ,
                                  @EmpGradeId ,
                                  @PFActiveDate ,
                                  @PreBalance ,
                                  @EmpBalance ,
                                  @CompanyBalance ,
                                  @InterBalance,
                                  @PFStartDate ,
                                  @ActionStatus ,
                                  @EntryUser ,
                                  @EntryDate ,
                                  @IsActive
                                )";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasPFBenefit(PFSetup aPfSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aPfSetup.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@Year", aPfSetup.PFActiveDate.Year));
            string query = "SELECT * FROM dbo.tblPFSetup WHERE YEAR(PFActiveDate)=@Year AND EmpInfoId=@EmpInfoId AND ActionStatus IN ('Posted','Accepted')";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,tblEmpGeneralInfo.EmpGradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName,tblEmpGeneralInfo.JoiningDate FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' and tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable PFReport(string month,string year,string monthName)
        {
//            string query = @"SELECT dbo.tblEmpGeneralInfo.EmpMasterCode,tblEmpGeneralInfo.EmpInfoId,EmpName,DesigName,SUM(PrFund) AS EmpPrFund,SUM(PrFund) AS PrFund,(SUM(PrFund)+SUM(PrFund)) as TotalPrFund,DeptName,dbo.tblEmpGeneralInfo.JoiningDate,PFActiveDate AS  PFStartDate FROM dbo.tblPFSetup 
//                           INNER JOIN dbo.tblEmpGeneralInfo ON dbo.tblPFSetup.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
//                            INNER JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
//                            INNER JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
//                            
//                            INNER JOIN dbo.tblSalaryRecordPerMonth ON dbo.tblPFSetup.EmpInfoId=dbo.tblSalaryRecordPerMonth.EmpInfoId WHERE MONTH(SalaryStartDate)='"+month+"' AND EmployeeStatus='Active' GROUP BY dbo.tblEmpGeneralInfo.EmpMasterCode,EmpName,DesigName,tblEmpGeneralInfo.EmpInfoId,DeptName,dbo.tblEmpGeneralInfo.JoiningDate,PFActiveDate";

            string query = @"SELECT E.EmpInfoId,S.EmpMasterCode,EmpName,D.DesigName,Dp.DeptName,E.JoiningDate,UnitName,S.PrFund,S.PrFund AS ComPrFund,(S.PrFund*2)TotalPrFund , '" + monthName + "' as Month,'" + year + "' as Year,'" + HttpContext.Current.Session["LoginName"] + "' as LoginName FROM dbo.tblSalaryRecordPerMonth S  " +
                            " INNER JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId "+
                            " INNER JOIN dbo.tblDesignation D ON S.DesigId = D.DesigId "+
                            " INNER JOIN dbo.tblDepartment Dp ON S.DepId=Dp.DeptId "+
                            " INNER JOIN dbo.tblCompanyUnit CU ON S.UnitId=CU.UnitId " +

                             " WHERE MONTH(S.SalaryStartDate)='" + month + "' AND YEAR(S.SalaryStartDate)='" + year + "' AND S.PrFund>0 AND E.EmployeeStatus='"+HttpContext.Current.Session["Status"].ToString()+"' " +
                            "  ORDER BY UnitName,E.EmpInfoId";
            
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        //public DataTable PFReportYearly(string year)
        //{
            
        //    string query =
        //        @"SELECT E.EmpInfoId,S.EmpMasterCode,EmpName,D.DesigName,Dp.DeptName,E.JoiningDate,UnitName,SUM(S.PrFund)PrFund,SUM(S.PrFund) AS ComPrFund,SUM(S.PrFund*2)TotalPrFund , '" + year + "' as YEAR,(CASE WHEN PFStartDate IS NULL  THEN PFActiveDate ELSE PFStartDate END) AS PFStartDate FROM dbo.tblSalaryRecordPerMonth S  " +
        //        " INNER JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId " +
        //        " INNER JOIN dbo.tblDesignation D ON S.DesigId = D.DesigId " +
        //        " INNER JOIN dbo.tblDepartment Dp ON S.DepId=Dp.DeptId " +
        //        " INNER JOIN dbo.tblCompanyUnit CU ON S.UnitId=CU.UnitId " +
        //        " INNER JOIN dbo.tblPFSetup ON S.EmpInfoId=dbo.tblPFSetup.EmpInfoId" +
        //        "  WHERE  YEAR(S.SalaryStartDate)='" + year + "' AND S.PrFund>0 GROUP BY E.EmpInfoId,S.EmpMasterCode,EmpName,D.DesigName,Dp.DeptName,E.JoiningDate,UnitName,PFActiveDate,PFStartDate" +
        //                    "  ORDER BY UnitName,E.EmpInfoId";

        //    return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        //}
        public DataTable PFReportYearly(string year)
        {

            string query =
                @"
                
                SELECT tblX.EmpInfoId,SUM(tblX.EmpBalance+PrevYearPf)PreviousAllYearPf,SUM(tblX.CompanyBalance+PrevYearPfCom)PreviousAllYearComPf,SUM(CurrentYearPf)CurrentYearPf ,SUM(CurrentYearComPf)CurrentYearComPf,DeptName,DesigName,EmpMasterCode,EmpName,UnitName,(CASE WHEN PFStartDate IS NULL  THEN PFActiveDate ELSE PFStartDate END) AS PFStartDate,(SUM(tblX.EmpBalance+PrevYearPf)+SUM(tblX.CompanyBalance+PrevYearPfCom)+SUM(CurrentYearPf)+SUM(CurrentYearComPf))TotalPrFund,tblEmpGeneralInfo.JoiningDate,'" + year + "' as Year,'" + HttpContext.Current.Session["LoginName"].ToString() + "' as LoginName FROM  " +
                
                
                " (SELECT EmpInfoId,EmpBalance,CompanyBalance,0 AS CurrentYearPf ,0 AS CurrentYearComPf,0 AS PrevYearPf,0 AS PrevYearPfCom FROM dbo.tblPFSetup WHERE Year(PFStartDate)<'"+year+"' "+
                
                " UNION ALL "+
                " SELECT EmpInfoId,0 AS EmpBalance,0 AS CompanyBalance,PrFund as CurrentYearPf,PrFund AS CurrentYearComPf,0 AS PrevYearPf,0 AS PrevYearPfCom  FROM dbo.tblSalaryRecordPerMonth  WHERE year(SalaryStartDate)='"+year+"' "+
                  
                " UNION ALL "+
                  
                " SELECT EmpInfoId,0 AS EmpBalance,0 AS CompanyBalance,0 AS CurrentYearPf,0 AS CurrentYearComPf,SUM(PrFund) AS PrevYearPf,SUM(PrFund) AS PrevYearPfCom FROM dbo.tblSalaryRecordPerMonth WHERE SalaryStartDate<'1-Jan-"+year+"' GROUP BY EmpInfoId "+

                "  )AS tblX " +
                 
                 
                " LEFT JOIN dbo.tblEmpGeneralInfo ON tblX.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId" +
                " LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId" +
                " LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId" +
                " LEFT JOIN dbo.tblPFSetup ON dbo.tblEmpGeneralInfo.EmpInfoId=dbo.tblPFSetup.EmpInfoId" +
                " LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId  WHERE EmployeeStatus='" + HttpContext.Current.Session["Status"].ToString() + "'   GROUP BY tblX.EmpInfoId,DeptName,DesigName,EmpMasterCode,EmpName,PFStartDate,dbo.tblPFSetup.PFActiveDate,UnitName,tblEmpGeneralInfo.JoiningDate HAVING (SUM(tblX.EmpBalance)+SUM(tblX.CompanyBalance)+SUM(CurrentYearPf)+SUM(CurrentYearComPf))<>0";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable PFReportYearly(string year,string empCode)
        {

            string query =
                @"
                SELECT tblX.EmpInfoId,SUM(tblX.EmpBalance+PrevYearPf)PreviousAllYearPf,SUM(tblX.CompanyBalance+PrevYearPfCom)PreviousAllYearComPf,SUM(CurrentYearPf)CurrentYearPf ,SUM(CurrentYearComPf)CurrentYearComPf,DeptName,DesigName,EmpMasterCode,EmpName,UnitName,(CASE WHEN PFStartDate IS NULL  THEN PFActiveDate ELSE PFStartDate END) AS PFStartDate,(SUM(tblX.EmpBalance+PrevYearPf)+SUM(tblX.CompanyBalance+PrevYearPfCom)+SUM(CurrentYearPf)+SUM(CurrentYearComPf))TotalPrFund,JoiningDate,'" + year + "' as Year,'" + HttpContext.Current.Session["LoginName"].ToString() + "' as LoginName FROM  " +


                " (SELECT EmpInfoId,EmpBalance,CompanyBalance,0 AS CurrentYearPf ,0 AS CurrentYearComPf,0 AS PrevYearPf,0 AS PrevYearPfCom FROM dbo.tblPFSetup WHERE Year(PFStartDate)<'" + year + "' " +

                " UNION ALL " +
                " SELECT EmpInfoId,0 AS EmpBalance,0 AS CompanyBalance,PrFund as CurrentYearPf,PrFund AS CurrentYearComPf,0 AS PrevYearPf,0 AS PrevYearPfCom  FROM dbo.tblSalaryRecordPerMonth  WHERE year(SalaryStartDate)='" + year + "' " +

                " UNION ALL " +

                " SELECT EmpInfoId,0 AS EmpBalance,0 AS CompanyBalance,0 AS CurrentYearPf,0 AS CurrentYearComPf,SUM(PrFund) AS PrevYearPf,SUM(PrFund) AS PrevYearPfCom FROM dbo.tblSalaryRecordPerMonth WHERE SalaryStartDate<'1-Jan-" + year + "' GROUP BY EmpInfoId " +

                "  )AS tblX " +


                " LEFT JOIN dbo.tblEmpGeneralInfo ON tblX.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId" +
                " LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId" +
                " LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId" +
                " LEFT JOIN dbo.tblPFSetup ON dbo.tblEmpGeneralInfo.EmpInfoId=dbo.tblPFSetup.EmpInfoId" +
                " LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId  WHERE EmployeeStatus='" + HttpContext.Current.Session["Status"].ToString() + "' AND  tblEmpGeneralInfo.EmpMasterCode='" + empCode + "'   GROUP BY tblX.EmpInfoId,DeptName,DesigName,EmpMasterCode,EmpName,PFStartDate,dbo.tblPFSetup.PFActiveDate,UnitName,JoiningDate HAVING (SUM(tblX.EmpBalance)+SUM(tblX.CompanyBalance)+SUM(CurrentYearPf)+SUM(CurrentYearComPf))<>0";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable PFDetails(string year,string month,string empcode)
        {

            string query =
                @"SELECT * FROM dbo.tblSalaryRecordPerMonth WHERE MONTH(SalaryStartDate)='" + month + "' AND YEAR(SalaryStartDate)='" + year + "' AND EmpMasterCode='"+empcode+"'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool UpdatePFSetup(PFSetup aPfSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aPfSetup.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@PFActiveDate", aPfSetup.PFActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@PreBalance", aPfSetup.PreBalance));
            aSqlParameterlist.Add(new SqlParameter("@EmpBalance", (object)(aPfSetup.EmpBalance ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@CompanyBalance", (object)(aPfSetup.CompanyBalance ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@PFStartDate", (object)(aPfSetup.PFStartDate ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@InterBalance", (object)(aPfSetup.InterBalance ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@Year", aPfSetup.PFActiveDate.Year));



            string insertQuery = @"UPDATE dbo.tblPFSetup SET 
                               
                                  EmpInfoId=@EmpInfoId ,
                                  PFActiveDate=@PFActiveDate ,
                                  PreBalance=@PreBalance ,
                                  EmpBalance=@EmpBalance ,
                                  CompanyBalance=@CompanyBalance ,
                                  InterBalance=@InterBalance,
                                  PFStartDate=@PFStartDate 
                                  
                                WHERE YEAR(PFActiveDate)=@Year AND EmpInfoId=@EmpInfoId
                                ";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadPFSetupViewForApproval(string ActionStatus)
        {
            string query = @"SELECT PFSId,tblPFSetup.EmpInfoId,EmpMasterCode,EmpName,DesigName,DeptName,PFActiveDate,EntryBy,tblPFSetup.EntryDate FROM dbo.tblPFSetup
                             LEFT JOIN tblEmpGeneralInfo ON dbo.tblPFSetup.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                             INNER JOIN dbo.tblDesignation ON tblDesignation.DesigId = tblPFSetup.DesigId
                             INNER JOIN dbo.tblDepartment ON tblDepartment.DeptId = tblPFSetup.DeptId
                             where tblPFSetup.ActionStatus='" + ActionStatus + "' AND dbo.tblPFSetup.IsActive=1  order by dbo.tblPFSetup.PFSId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool ApprovalUpdateDALL(PFSetup aPfSetup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PFSId", aPfSetup.PFSId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aPfSetup.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aPfSetup.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aPfSetup.ApprovedDate));

            string query = @"UPDATE tblPFSetup SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE PFSId=@PFSId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }

        public bool UpdatePFEligibilityStatus(string empId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));

            string insertQuery = @"UPDATE tblEmpGeneralInfo SET PFEligibility='Yes'WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
    }
}
