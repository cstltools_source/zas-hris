﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class PFStartStopDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }

        public bool SavePFStartStop(PFStartStop aPfStartStop)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PFSSId", aPfStartStop.PFSSId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aPfStartStop.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aPfStartStop.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aPfStartStop.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aPfStartStop.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aPfStartStop.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@ActiveInactiveDate", (object)(aPfStartStop.ActiveInactiveDate ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@Action", aPfStartStop.Action));
            aSqlParameterlist.Add(new SqlParameter("@ActionDate", aPfStartStop.ActionDate));



            string insertQuery = @"INSERT INTO dbo.tblPFStartStop
                                    ( PFSSId ,
                                      EmpInfoId ,
                                      ActiveInactiveDate ,
                                      Action ,
                                      ActionDate ,
                                      ActionStatus ,
                                      EntryUser ,
                                      EntryDate ,
                                      IsActive
                                    )
                            VALUES  (@PFSSId ,
                                      @EmpInfoId ,
                                      @ActiveInactiveDate ,
                                      @Action ,
                                      @ActionDate ,
                                      @ActionStatus ,
                                      @EntryUser ,
                                      @EntryDate ,
                                      @IsActive
                                    )";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }


        public bool UpdatePFSetup(bool status,string empinfoId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@IsActive", status));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empinfoId));


            string Query = @"UPDATE dbo.tblPFSetup SET IsActive=@IsActive WHERE EmpInfoId=@EmpInfoId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(Query, aSqlParameterlist, "HRDB");
        }



        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT tblEmpGeneralInfo.EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,tblEmpGeneralInfo.EmpGradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' and tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadPFSetup(string empInfoId)
        {
            string query = @"SELECT * FROM dbo.tblPFSetup WHERE EmpInfoId='" + empInfoId + "' AND ActionStatus IN ('Accepted')";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
