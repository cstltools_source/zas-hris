﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.InternalCls;

namespace DAL.HRM_DAL
{
    public class PasswordResetMailDal
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public DataTable GetUserIsValidOrNotInfo(string email) 
        {
            string query = @"SELECT * FROM dbo.tblUser WHERE Email ='" + email + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public bool UpdateUserPasswordById(string password, string id)
        {
            string query = @"UPDATE dbo.tblUser SET Password ='" + password + "' WHERE	UserId = '" + id + "'";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, "HRDB");
        }
    }
}
