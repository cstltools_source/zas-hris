﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ProbationDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public bool SaveProbationStatus(Probation aProbation)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ProbitionId", aProbation.ProbitionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aProbation.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ConfirmationDate", (object)(aProbation.ConfirmationDate ?? (object)DBNull.Value)));
            aSqlParameterlist.Add(new SqlParameter("@ProbationPeriodTo", aProbation.ProbationPeriodTo));
            aSqlParameterlist.Add(new SqlParameter("@Action", aProbation.Action));

            string insertQuery = @"INSERT INTO dbo.tblEmpProbation
                                    ( ProbitionId ,
                                      EmpInfoId ,
                                      ConfirmationDate ,
                                      ProbationPeriodTo ,
                                      Action
                                    )
                            VALUES  ( @ProbitionId ,
                                      @EmpInfoId ,
                                      @ConfirmationDate ,
                                      @ProbationPeriodTo ,
                                      @Action
                                    )";

            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadEmployee(string parameter)
        {
            string query = @"SELECT UNT.UnitName,EmpInfoId,EmpMasterCode,EmpName,DesigName,DeptName,JoiningDate,ProbationPeriodTo FROM dbo.tblEmpGeneralInfo
                             LEFT JOIN tblEmployeeType TP ON tblEmpGeneralInfo.EmpTypeId = TP.EmpTypeId
                             LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                             LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                             LEFT JOIN tblCompanyUnit AS UNT ON UNT.UnitId = tblEmpGeneralInfo.UnitId
                             WHERE TP.EmpType = 'Provisional' AND EmployeeStatus='Active'" + parameter ;
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmployeeInfo(string empid)
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo WHERE EmpInfoId='"+empid+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpBirthData(string day,string month)
        {
            string query = @"SELECT EmpMasterCode,EmpName,DesigName,DeptName,Email,EmpInfoId FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId 
                            WHERE MONTH(DateOfBirth)='" + month + "' AND DAY(DateOfBirth)='" + day + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool UpdateProbation(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ProbationPeriodTo", aEmpGeneralInfo.ProbationPeriodTo));
            
            string query =
                @" UPDATE dbo.tblEmpGeneralInfo SET ProbationPeriodTo=@ProbationPeriodTo,ExtProbationPeriod=@ProbationPeriodTo WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool UpdateConfirm(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@ConfirmationDate", aEmpGeneralInfo.ConfirmationDate));

            string query =
                @" UPDATE dbo.tblEmpGeneralInfo SET ConfirmationDate=@ConfirmationDate,EmpTypeId='1' WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
