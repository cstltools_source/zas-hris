﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class PromotionDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SavePromotion(Promotion aPromotion)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PromotionId", aPromotion.PromotionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aPromotion.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aPromotion.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aPromotion.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aPromotion.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@NewDesigId", aPromotion.NewDesigId));
            aSqlParameterlist.Add(new SqlParameter("@NewSalScaleId", aPromotion.NewSalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aPromotion.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@NewEmpGradeId", aPromotion.NewGradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aPromotion.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aPromotion.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aPromotion.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aPromotion.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aPromotion.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aPromotion.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aPromotion.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aPromotion.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aPromotion.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", aPromotion.Remarks));

            string insertQuery = @"insert into tblPromotion (PromotionId,EmpInfoId,DesigId,SalScaleId,EmpGradeId,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,NewEmpGradeId,NewDesigId,NewSalScaleId,EntryUser,EntryDate,ActionStatus,EffectiveDate,IsActive,Remarks) 
                                                   values (@PromotionId,@EmpInfoId,@DesigId,@SalScaleId,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@EmpGradeId,@NewEmpGradeId,@NewDesigId,@NewSalScaleId,@EntryUser,@EntryDate,@ActionStatus,@EffectiveDate,@IsActive,@Remarks)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasPromotion(Promotion aPromotion)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aPromotion.EmpInfoId));
            string query = "select * from tblPromotion where EmpInfoId=@EmpInfoId and ActionStatus in ('Posted ' , 'Verified') ";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadPromotionView()
        {
            string query = @"SELECT  PromotionId,EmpMasterCode ,EmpName ,tblSalaryGradeOrScale.SalScaleName,tblDesignation.DesigName,tblEmployeeGrade.GradeName, EffectiveDate,tblPromotion.SalScaleId,
                       tblPromotion.EmpGradeId,tblPromotion.DesigId, salgrad.SalScaleName as NewSalScaleName, tblEmployeeGrade.GradeName as NewEmpGrade,
                         tblDesignation.DesigName as NewDesig, tblPromotion.ActionStatus
                 FROM tblPromotion 
                 LEFT JOIN tblEmpGeneralInfo ON tblPromotion.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN dbo.tblSalaryGradeOrScale ON tblPromotion.SalScaleId = dbo.tblSalaryGradeOrScale.SalScaleId
                 LEFT JOIN dbo.tblSalaryGradeOrScale salgrad ON tblPromotion.NewSalScaleId = salgrad.SalScaleId
                 LEFT JOIN dbo.tblDesignation ON tblPromotion.DesigId = dbo.tblDesignation.DesigId
                 LEFT JOIN dbo.tblDesignation deg1 ON tblPromotion.NewDesigId = deg1.DesigId
                 LEFT JOIN dbo.tblEmployeeGrade ON tblPromotion.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                   LEFT JOIN dbo.tblEmployeeGrade grade ON tblPromotion.NewEmpGradeId = grade.GradeId
                 where tblPromotion.ActionStatus in ('Posted','Cancel') and tblPromotion.IsActive=1 and tblPromotion.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblPromotion.PromotionId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation ORDER BY DesigName ";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'ORDER BY DeptName ";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo ORDER BY EmployeeName";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "' ORDER BY UnitName ";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeGrade ORDER BY GradeName ";
            aInternalDal.LoadDropDownValue(ddl, "GradeName", "GradeId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision ORDER BY DivName ";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "' ORDER BY SectionName";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale ORDER BY SalScaleName ";
            aInternalDal.LoadDropDownValue(ddl, "SalScaleName", "SalScaleId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType ORDER BY EmpType ";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName,dbo.tblEmpGeneralInfo.SalScaleId,SalScaleName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
                                LEFT JOIN dbo.tblSalaryGradeOrScale ON dbo.tblEmpGeneralInfo.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Promotion PromotionEditLoad(string PromotionId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PromotionId", PromotionId));
            string query = "select * from tblPromotion where PromotionId = @PromotionId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Promotion aPromotion = new Promotion();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aPromotion.PromotionId = Int32.Parse(dataReader["PromotionId"].ToString());
                    aPromotion.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aPromotion.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aPromotion.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aPromotion.SalScaleId = Convert.ToInt32(dataReader["SalScaleId"].ToString());
                    aPromotion.NewDesigId = Convert.ToInt32(dataReader["NewDesigId"].ToString());
                    aPromotion.NewSalScaleId = Convert.ToInt32(dataReader["NewSalScaleId"].ToString());
                    aPromotion.NewGradeId = Convert.ToInt32(dataReader["NewEmpGradeId"].ToString());
                    aPromotion.ActionStatus = dataReader["ActionStatus"].ToString();
                    aPromotion.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());

                }
            }
            return aPromotion;
        }

        public bool UpdatePromotion(Promotion aPromotion)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PromotionId", aPromotion.PromotionId));
            //aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aPromotion.EmpInfoId));
            //aSqlParameterlist.Add(new SqlParameter("@DesigId", aPromotion.DesigId));
            //aSqlParameterlist.Add(new SqlParameter("@DivisionId", aPromotion.DivisionId));
            //aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aPromotion.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aPromotion.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@NewDesigId", aPromotion.NewDesigId));
            aSqlParameterlist.Add(new SqlParameter("@NewSalScaleId", aPromotion.NewSalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@NewEmpGradeId", aPromotion.NewGradeId));


            string query = @"UPDATE tblPromotion SET NewEmpGradeId=@NewEmpGradeId,NewDesigId=@NewDesigId,NewSalScaleId=@NewSalScaleId,EffectiveDate=@EffectiveDate  WHERE PromotionId=@PromotionId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(Promotion aPromotion)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PromotionId", aPromotion.PromotionId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aPromotion.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aPromotion.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aPromotion.ApprovedDate));

            string query = @"UPDATE tblPromotion SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE PromotionId=@PromotionId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadPromotionViewForApproval(string ActionStatus)
        {
            string query = @"SELECT  tblPromotion.EmpInfoId,PromotionId,EmpMasterCode ,EmpName ,tblSalaryGradeOrScale.SalScaleName,tblDesignation.DesigName,tblEmployeeGrade.GradeName, EffectiveDate,tblPromotion.SalScaleId,
                       tblPromotion.EmpGradeId,tblPromotion.DesigId, salgrad.SalScaleName as NewSalScaleName, tblEmployeeGrade.GradeName as NewEmpGrade,
                         tblDesignation.DesigName as NewDesig, tblPromotion.ActionStatus,NewDesigId,NewSalScaleId,NewEmpGradeId
                 FROM tblPromotion 
                 LEFT JOIN tblEmpGeneralInfo ON tblPromotion.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN dbo.tblSalaryGradeOrScale ON tblPromotion.SalScaleId = dbo.tblSalaryGradeOrScale.SalScaleId
                 LEFT JOIN dbo.tblSalaryGradeOrScale salgrad ON tblPromotion.NewSalScaleId = salgrad.SalScaleId
                 LEFT JOIN dbo.tblDesignation ON tblPromotion.DesigId = dbo.tblDesignation.DesigId
                 LEFT JOIN dbo.tblDesignation deg1 ON tblPromotion.NewDesigId = deg1.DesigId
                 LEFT JOIN dbo.tblEmployeeGrade ON tblPromotion.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                 LEFT JOIN dbo.tblEmployeeGrade grade ON tblPromotion.NewEmpGradeId = grade.GradeId

                where tblPromotion.ActionStatus='" + ActionStatus + "' AND tblEmpGeneralInfo.IsActive=1  order by tblPromotion.PromotionId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aEmpGeneralInfo.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aEmpGeneralInfo.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aEmpGeneralInfo.EmpGradeId));


            string query = @"UPDATE tblEmpGeneralInfo SET SalScaleId=@SalScaleId,DesigId=@DesigId,EmpGradeId=@EmpGradeId WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
       
        public bool DeleteData(string PromotionId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblPromotion", "PromotionId", PromotionId);
        }
    }
}
