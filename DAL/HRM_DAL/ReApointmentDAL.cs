﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ReAppointmentDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveReAppointment(ReAppointment aReAppointment)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ReAppointmentId", aReAppointment.ReAppointmentId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aReAppointment.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aReAppointment.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aReAppointment.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aReAppointment.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aReAppointment.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aReAppointment.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aReAppointment.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aReAppointment.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aReAppointment.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aReAppointment.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aReAppointment.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aReAppointment.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aReAppointment.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aReAppointment.IsActive));

            string insertQuery = @"insert into dbo.tblReApointment (ReAppointmentId,EmpInfoId,EffectiveDate,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@ReAppointmentId,@EmpInfoId,@EffectiveDate,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasReAppointment(ReAppointment aReAppointment)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aReAppointment.EmpInfoId));
            string query = "select * from dbo.tblReApointment where EmpInfoId=@EmpInfoId and ActionStatus in ('Posted ' , 'Verified')";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public bool HasjobLeft(String EmpCode)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", EmpCode));
            string query = "select * from dbo.tblJobleft where EmpInfoId=@EmpInfoId and ActionStatus='Accepted'";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadReAppointmentView()
        {
            string query = @"SELECT  ReAppointmentId,EmpMasterCode ,EmpName , EffectiveDate , DesigName,DeptName,dbo.tblReApointment.ActionStatus,dbo.tblReApointment.EntryUser,dbo.tblReApointment.EntryDate FROM dbo.tblReApointment 
                 LEFT JOIN tblEmpGeneralInfo ON dbo.tblReApointment.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON dbo.tblReApointment.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON dbo.tblReApointment.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON dbo.tblReApointment.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON dbo.tblReApointment.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON dbo.tblReApointment.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON dbo.tblReApointment.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON dbo.tblReApointment.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON dbo.tblReApointment.DeptId = tblDepartment.DeptId where tblReApointment.ActionStatus in ('Posted','Cancel') and tblReApointment.IsActive=1 and tblReApointment.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblReApointment.ReAppointmentId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation ORDER BY DesigName ";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'ORDER BY DeptName ";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo ORDER BY EmployeeName";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision ORDER BY DivName ";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "' ORDER BY SectionName ";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale ORDER BY SalGradeName ";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType ORDER BY EmpType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public ReAppointment ReAppointmentEditLoad(string ReAppointmentId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ReAppointmentId", ReAppointmentId));
            string query = "select * from dbo.tblReApointment where ReAppointmentId = @ReAppointmentId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            ReAppointment aReAppointment = new ReAppointment();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aReAppointment.ReAppointmentId = Int32.Parse(dataReader["ReAppointmentId"].ToString());
                    aReAppointment.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aReAppointment.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aReAppointment.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aReAppointment.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aReAppointment.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aReAppointment.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aReAppointment.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aReAppointment.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aReAppointment.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aReAppointment.ActionStatus = dataReader["ActionStatus"].ToString();
                    aReAppointment.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());

                }
            }
            return aReAppointment;
        }

        public bool UpdateReAppointment(ReAppointment aReAppointment)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ReAppointmentId", aReAppointment.ReAppointmentId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aReAppointment.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aReAppointment.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aReAppointment.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aReAppointment.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aReAppointment.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aReAppointment.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aReAppointment.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aReAppointment.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aReAppointment.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aReAppointment.GradeId));

            string query = @"UPDATE dbo.tblReApointment SET EmpInfoId=@EmpInfoId,EffectiveDate=@EffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId WHERE ReAppointmentId=@ReAppointmentId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool EmployeeStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));

            string query = @"UPDATE dbo.tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus,InactiveReason='' WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(ReAppointment aReAppointment)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ReAppointmentId", aReAppointment.ReAppointmentId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aReAppointment.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aReAppointment.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aReAppointment.ApprovedDate));

            string query = @"UPDATE dbo.tblReApointment SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE ReAppointmentId=@ReAppointmentId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadReAppointmentViewForApproval(string ActionStatus)
        {
            string query = @"SELECT *, DeptName ,dbo.tblDepartment.DeptId  , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName From dbo.tblReApointment
                LEFT JOIN tblEmpGeneralInfo ON dbo.tblReApointment.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId

                where tblReApointment.ActionStatus='" + ActionStatus + "' AND tblReApointment.IsActive=1  order by dbo.tblReApointment.ReAppointmentId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));

            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string ReAppointmentId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblReApointment", "ReAppointmentId", ReAppointmentId);
            
        }

    }
}
