﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ReleaseSuspendDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveReleaseSuspend(ReleaseSuspend aReleaseSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RelsSuspendId", aReleaseSuspend.RelsSuspendId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aReleaseSuspend.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aReleaseSuspend.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aReleaseSuspend.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aReleaseSuspend.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aReleaseSuspend.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aReleaseSuspend.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aReleaseSuspend.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aReleaseSuspend.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aReleaseSuspend.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aReleaseSuspend.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aReleaseSuspend.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aReleaseSuspend.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aReleaseSuspend.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aReleaseSuspend.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@SuspendId", aReleaseSuspend.SuspendId));
            aSqlParameterlist.Add(new SqlParameter("@SuspendDate", aReleaseSuspend.SuspendDate));

            string insertQuery = @"insert into tblSuspendRelease (RelsSuspendId,EmpInfoId,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,GradeId,EffectiveDate,ActionStatus,EntryBy,EntryDate,IsActive,SuspendId,SuspendDate) 
            values (@RelsSuspendId,@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EffectiveDate,@ActionStatus,@EntryBy,@EntryDate,@IsActive,@SuspendId,@SuspendDate)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasReleaseSuspend(ReleaseSuspend aReleaseSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aReleaseSuspend.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@SuspendId", aReleaseSuspend.SuspendId));
            string query = "select * from tblSuspendRelease where EmpInfoId=@EmpInfoId and SuspendId=@SuspendId and IsActive=1 and ActionStatus in ('Posted ' , 'Verified')";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadReleaseSuspendView()
        {
            string query =
                @"SELECT *,CONVERT(NVARCHAR(11),tblSuspendRelease.SuspendDate,106) as SusDate,CONVERT(NVARCHAR(11),tblSuspendRelease.EffectiveDate,106) as  EfDate FROM tblSuspendRelease " +
                " LEFT JOIN tblEmpGeneralInfo ON tblSuspendRelease.EmpInfoId = tblEmpGeneralInfo.EmpInfoId  " +
                " LEFT JOIN tblCompanyInfo ON tblSuspendRelease.CompanyInfoId = tblCompanyInfo.CompanyInfoId  " +
                " LEFT JOIN tblCompanyUnit ON tblSuspendRelease.UnitId = tblCompanyUnit.UnitId  " +
                " LEFT JOIN tblDivision ON tblSuspendRelease.DivisionId = tblDivision.DivisionId  " +
                " LEFT JOIN tblSection ON tblSuspendRelease.SectionId = tblSection.SectionId  " +
                " LEFT JOIN tblEmployeeGrade ON tblSuspendRelease.GradeId = tblEmployeeGrade.GradeId  " +
                " LEFT JOIN tblEmployeeType ON tblSuspendRelease.EmpTypeId = tblEmployeeType.EmpTypeId  " +
                " LEFT JOIN tblDesignation ON tblSuspendRelease.DesigId = tblDesignation.DesigId  " +
                " LEFT JOIN tblDepartment ON tblSuspendRelease.DeptId = tblDepartment.DeptId  where tblSuspendRelease.ActionStatus in ('Posted','Cancel') and tblSuspendRelease.IsActive=1 and tblSuspendRelease.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblSuspendRelease.RelsSuspendId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }
        
        public void LoadDepartmentName(DropDownList ddl,string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='"+divisionId+"'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl,string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl,string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='"+deptId+"'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT * FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
                                INNER JOIN tblSuspend ON  dbo.tblEmpGeneralInfo.EmpInfoId=dbo.tblSuspend.EmpInfoId
                                WHERE  tblSuspend.IsActive=1 AND tblSuspend.ActionStatus='Accepted' AND tblSuspend.RelesedOn IS NULL and  tblEmpGeneralInfo.EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1 and tblEmpGeneralInfo.EmployeeStatus='Inactive' and tblEmpGeneralInfo.InactiveReason='Suspend'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public ReleaseSuspend EmpReleaseSuspendEditLoad(string RelsSuspendId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RelsSuspendId", RelsSuspendId));
            string query = "select * from tblSuspendRelease where RelsSuspendId=@RelsSuspendId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            ReleaseSuspend aReleaseSuspend = new ReleaseSuspend();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aReleaseSuspend.RelsSuspendId = Int32.Parse(dataReader["RelsSuspendId"].ToString());
                    aReleaseSuspend.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aReleaseSuspend.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aReleaseSuspend.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aReleaseSuspend.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aReleaseSuspend.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aReleaseSuspend.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aReleaseSuspend.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aReleaseSuspend.GradeId = Convert.ToInt32(dataReader["GradeId"].ToString());
                    aReleaseSuspend.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aReleaseSuspend.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    aReleaseSuspend.ActionStatus = dataReader["ActionStatus"].ToString();
                    aReleaseSuspend.EntryBy = dataReader["EntryBy"].ToString();
                    aReleaseSuspend.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    
                }
            }
            return aReleaseSuspend;
        }

        public bool UpdateReleaseSuspend(ReleaseSuspend aReleaseSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RelsSuspendId", aReleaseSuspend.RelsSuspendId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aReleaseSuspend.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aReleaseSuspend.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aReleaseSuspend.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aReleaseSuspend.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aReleaseSuspend.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aReleaseSuspend.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aReleaseSuspend.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aReleaseSuspend.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aReleaseSuspend.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aReleaseSuspend.EffectiveDate));
            

            string query = @"UPDATE tblSuspendRelease SET EmpInfoId=@EmpInfoId,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,EmpTypeId=@EmpTypeId,GradeId=@GradeId,DesigId=@DesigId," +
                            "EffectiveDate=@EffectiveDate WHERE RelsSuspendId=@RelsSuspendId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(ReleaseSuspend aReleaseSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RelsSuspendId", aReleaseSuspend.RelsSuspendId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aReleaseSuspend.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aReleaseSuspend.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aReleaseSuspend.ApprovedDate));

            string query = @"UPDATE tblSuspendRelease SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE RelsSuspendId=@RelsSuspendId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));//InactiveReason
            aSqlParameterlist.Add(new SqlParameter("@InactiveReason", aEmpGeneralInfo.InactiveReason));
            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus,InactiveReason=@InactiveReason WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadReleaseSuspendViewForApproval(string actionstatus)
        {
            string query = @"SELECT *,CONVERT(NVARCHAR(11),tblSuspendRelease.EffectiveDate,106) AS SUSEffectiveDate,CONVERT(NVARCHAR(11),tblSuspendRelease.EntryDate,106) AS SUSEntryDate From dbo.tblSuspendRelease
                inner JOIN tblEmpGeneralInfo ON tblSuspendRelease.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where tblSuspendRelease.ActionStatus='" + actionstatus + "' AND tblSuspendRelease.IsActive=1  order by tblSuspendRelease.RelsSuspendId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteData(string RelsSuspendId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblSuspendRelease", "RelsSuspendId", RelsSuspendId);
            
        }
    }
}

