﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class RestLeaveDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveDataForOnDuty(RestLeave aOnDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RestLeaveId", aOnDuty.RestLeaveId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aOnDuty.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aOnDuty.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aOnDuty.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOnDuty.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aOnDuty.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aOnDuty.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@FromDate", aOnDuty.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", aOnDuty.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aOnDuty.IsActive));

            string insertQuery = @"insert into tblRestLeave (RestLeaveId,EmpInfoId,DeptId,Purpose,ActionStatus,EntryDate,EntryUser,FromDate,ToDate,IsActive) 
            values (@RestLeaveId,@EmpInfoId,@DeptId,@Purpose,@ActionStatus,@EntryDate,@EntryUser,@FromDate,@ToDate,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public DataTable LoadRestLeaveViewForApproval(string actionstatus)
        {
            string query = @"SELECT * From tblRestLeave
                LEFT JOIN tblEmpGeneralInfo ON tblRestLeave.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblRestLeave.ActionStatus='" + actionstatus + "' AND tblRestLeave.IsActive=1   order by tblRestLeave.RestLeaveId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadRestLeaveView()
        {
            string query = @"SELECT * From tblRestLeave
                LEFT JOIN tblEmpGeneralInfo ON tblRestLeave.EmpInfoId = tblEmpGeneralInfo.EmpInfoId WHERE tblRestLeave.ActionStatus in ('Posted','Cancel') and tblRestLeave.IsActive=1 and tblRestLeave.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblRestLeave.RestLeaveId desc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public RestLeave OnDutyEditLoad(string RestLeaveId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RestLeaveId", RestLeaveId));
            string query = "select * from tblRestLeave where RestLeaveId = @RestLeaveId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            RestLeave aOnDuty = new RestLeave();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aOnDuty.RestLeaveId = Int32.Parse(dataReader["RestLeaveId"].ToString());
                    aOnDuty.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aOnDuty.FromDate = Convert.ToDateTime(dataReader["FromDate"].ToString());
                    aOnDuty.ToDate = Convert.ToDateTime(dataReader["ToDate"].ToString());
                    aOnDuty.Purpose = dataReader["Purpose"].ToString();
                    //aOnDuty.ActionStatus = dataReader["ActionStatus"].ToString();
                    aOnDuty.ActionRemarks = dataReader["StateRemarks"].ToString();
                    //aOnDuty.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                }
            }
            return aOnDuty;
        }

        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadGroupWiseEmp(string groupId)
        {
            string query = @"SELECT EmpInfoId,EmpMasterCode,EmpName FROM dbo.tblEmpWiseGroup
                            LEFT JOIN dbo.tblGroup ON dbo.tblEmpWiseGroup.GroupId = dbo.tblGroup.GroupId
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblEmpWiseGroup.EmpId=dbo.tblEmpGeneralInfo.EmpInfoId WHERE dbo.tblEmpWiseGroup.GroupId='"+groupId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool UpdateOnDuty(RestLeave aOnDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RestLeaveId", aOnDuty.RestLeaveId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aOnDuty.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@FromDate", aOnDuty.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", aOnDuty.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@Purpose", aOnDuty.Purpose));
            aSqlParameterlist.Add(new SqlParameter("@ActionRemarks", aOnDuty.ActionRemarks));

            string query = @"UPDATE tblRestLeave SET EmpInfoId=@EmpInfoId,FromDate=@FromDate,ToDate=@ToDate,Purpose=@Purpose,StateRemarks=@ActionRemarks  WHERE RestLeaveId=@RestLeaveId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }


        public bool ApprovalUpdateDAL(RestLeave aOnDuty)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RestLeaveId", aOnDuty.RestLeaveId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aOnDuty.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedUser", aOnDuty.ApprovedUser));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aOnDuty.ApprovedDate));

            string query = @"UPDATE tblRestLeave SET ActionStatus=@ActionStatus,ApprovedUser=@ApprovedUser,ApprovedDate=@ApprovedDate WHERE RestLeaveId=@RestLeaveId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string RestLeaveId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblRestLeave", "RestLeaveId", RestLeaveId);
            
        }
    }
}
