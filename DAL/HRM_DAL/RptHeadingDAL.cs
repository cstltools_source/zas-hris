﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class RptHeadingDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForRptHeader(RptHeader aRptHeader)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RptId", aRptHeader.RptId));
            aSqlParameterlist.Add(new SqlParameter("@RptHeader", aRptHeader.RptHeading));
            aSqlParameterlist.Add(new SqlParameter("@RptAddress", aRptHeader.RptAddress));
            aSqlParameterlist.Add(new SqlParameter("@RptTel", aRptHeader.RptTel));
            aSqlParameterlist.Add(new SqlParameter("@RptFax", aRptHeader.RptFax));
            aSqlParameterlist.Add(new SqlParameter("@RptEmail", aRptHeader.RptEmail));
            aSqlParameterlist.Add(new SqlParameter("@RptMessage", aRptHeader.RptMessage));

            string insertQuery = @"insert into tblReportHeading (RptId,RptHeader,RptAddress,RptFax,RptTel,RptEmail,RptMessage) 
            values (@RptId,@RptHeader,@RptAddress,@RptFax,@RptTel,@RptEmail,@RptMessage)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        
        public bool UpdateRptImage(RptHeader aHeader)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RptId", aHeader.RptId));
            aSqlParameterlist.Add(new SqlParameter("@RptImage", aHeader.RptImage));
            string query = @"UPDATE dbo.tblReportHeading SET RptImage=@RptImage WHERE RptId=@RptId";
            return aCommonInternalDal.SaveDataByInsertCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadRptHeaderReport()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel,'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadRptHeader()
        {
            string query = @"SELECT dbo.tblReportHeading.RptId, dbo.tblReportHeading.CompanyName,dbo.tblReportHeading.Address,dbo.tblReportHeading.ContactNo,dbo.tblReportHeading.FaxNo,dbo.tblReportHeading.Remarks FROM dbo.tblReportHeading ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public RptHeader RptHeaderEditLoad(string RptId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RptId", RptId));
            string query = "select * from tblReportHeading where RptId = @RptId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            RptHeader aRptHeader = new RptHeader();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aRptHeader.RptId = Int32.Parse(dataReader["RptId"].ToString());
                    aRptHeader.RptHeading = dataReader["RptHeader"].ToString();
                    aRptHeader.RptAddress = dataReader["RptAddress"].ToString();
                    aRptHeader.RptTel = dataReader["RptTel"].ToString();
                    aRptHeader.RptFax = dataReader["RptFax"].ToString();
                    aRptHeader.RptEmail = dataReader["RptEmail"].ToString();
                    aRptHeader.RptMessage = dataReader["RptMessage"].ToString();  
                }
            }
            return aRptHeader;
        }

        public bool UpdateRptHeader(RptHeader aRptHeader)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RptId", aRptHeader.RptId));
            aSqlParameterlist.Add(new SqlParameter("@RptHeader", aRptHeader.RptHeading));
            aSqlParameterlist.Add(new SqlParameter("@RptAddress", aRptHeader.RptAddress));
            aSqlParameterlist.Add(new SqlParameter("@RptTel", aRptHeader.RptTel));
            aSqlParameterlist.Add(new SqlParameter("@RptFax", aRptHeader.RptFax));
            aSqlParameterlist.Add(new SqlParameter("@RptEmail", aRptHeader.RptEmail));
            aSqlParameterlist.Add(new SqlParameter("@RptMessage", aRptHeader.RptMessage));

            string query = @"UPDATE dbo.tblReportHeading SET RptHeader=@RptHeading,RptAddress=@RptAddress,RptTel=@RptTel,RptFax=@RptFax,RptEmail=@RptEmail,RptMessage=@RptMessage WHERE RptId=@RptId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
