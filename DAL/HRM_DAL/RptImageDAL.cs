﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class RptImageDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();

        public bool SaveEmpImage(byte[] imagebyt, int rptId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@RptImage", imagebyt));
            aSqlParameterlist.Add(new SqlParameter("@RptId", rptId));

            string insertQuery = @"insert into dbo.tblRptImage (RptImage,RptId)values(@RptImage,@RptId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        
    }
}
