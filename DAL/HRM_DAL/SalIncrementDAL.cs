﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalIncrementDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool ApprovalUpdateDAL(Increment aIncrement)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@IncrementId", aIncrement.IncrementId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aIncrement.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aIncrement.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aIncrement.ApprovedDate));

            string query = @"UPDATE tblIncrement SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE IncrementId=@IncrementId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool IncrementSave(Increment aIncrement)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@IncrementId", aIncrement.IncrementId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aIncrement.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aIncrement.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aIncrement.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aIncrement.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aIncrement.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aIncrement.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aIncrement.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aIncrement.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@IncrementAmount", aIncrement.IncrementAmount));
            aSqlParameterlist.Add(new SqlParameter("@PreviousGrossSalary", aIncrement.PreviousGrossSalary));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aIncrement.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aIncrement.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aIncrement.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aIncrement.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@IncrementRemarks", aIncrement.IncrementRemarks));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aIncrement.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", aIncrement.Remarks));
            aSqlParameterlist.Add(new SqlParameter("@PromotionId", (object)(aIncrement.PromotionId ?? (object)DBNull.Value)));

            string insertQuery = @"insert into tblIncrement (IncrementId,EmpInfoId,CompanyInfoId,UnitId,DivisionId,SectionId,DesigId,DeptId,SalScaleId,IncrementAmount,PreviousGrossSalary,ActionStatus,EntryBy,EntryDate,ActiveDate,IncrementRemarks,IsActive,Remarks,PromotionId) 
            values (@IncrementId,@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@SectionId,@DesigId,@DeptId,@SalScaleId,@IncrementAmount,@PreviousGrossSalary,@ActionStatus,@EntryBy,@EntryDate,@ActiveDate,@IncrementRemarks,@IsActive,@Remarks,@PromotionId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool SaveIncrementDetailsDAL(IncrementDetail aIncrementDetail)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@IncrementDetailId", aIncrementDetail.IncrementDetailId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aIncrementDetail.SalaryHeadId));
            aSqlParameterlist.Add(new SqlParameter("@SalHeadName", aIncrementDetail.SalHeadName));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aIncrementDetail.Amount));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aIncrementDetail.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@IncrementId", aIncrementDetail.IncrementId));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aIncrementDetail.IsActive));

            string insertQuery = @"insert into tblIncrementDetails (IncrementDetailId,SalaryHeadId,SalHeadName,Amount,ActiveDate,IncrementId,IsActive) 
            values (@IncrementDetailId,@SalaryHeadId,@SalHeadName,@Amount,@ActiveDate,@IncrementId,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public void LoadSalaryRuleToDropDown(DropDownList aDropDownList, string strSalScaleId)
        {
            string query = @"SELECT DISTINCT RuleName,RuleName AS NameRule FROM dbo.tblSalaryRules where SalScaleId='" + strSalScaleId + "'";
            aCommonInternalDal.LoadDropDownValue(aDropDownList, "RuleName", "NameRule", query, "HRDB");
        }

        public void UpdateSalaryHistoryStatus(string empId, DateTime inactiveDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@InactiveDate", inactiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));

            string updateQuery = @"UPDATE tblEmpSalaryHistory SET InactiveDate=@InactiveDate, Status='Inactive' WHERE EmpInfoId=@EmpInfoId and Status='active'";
            aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }
        public void UpdateSalaryDetailStatus(string empId, DateTime inactiveDate)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@InactiveDate", inactiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));

            string updateQuery = @"UPDATE tblSalaryInformation SET InactiveDate=@InactiveDate, Status='Inactive' WHERE EmpInfoId=@EmpInfoId and Status='active'";
            aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }

        public DataTable SalaryDetailCheck(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * from tblEmpSalary where EmpInfoId = '" + empId.Trim() + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public DataTable SameMonthIncrementCheck(string empId,string month,string year)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * FROM dbo.tblIncrement WHERE EmpInfoId='" + empId + "' AND MONTH(ActiveDate)='" + month + "' AND YEAR(ActiveDate)='" + year + "' AND ActionStatus IN ('Posted')";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public bool HasEmployeeSalaryIncrement(Increment aIncrement)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aIncrement.EmpInfoId));

            string query = "select * from dbo.tblIncrement where EmpInfoId=@EmpInfoId and ActionStatus != 'Accepted' ";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable SalaryHistoryCheck(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * from tblEmpSalaryHistory where EmpInfoId = '" + empId.Trim() + "' and Status='active'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }

        public DataTable StartingSalaryCheck(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * from tblSalaryInformation where EmpInfoId = '" + empId.Trim() + "' AND  IsActive=1 AND (InactiveDate IS NULL OR InactiveDate='') ";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");
            return aDataTableEmpInfo;
        }

        public DataTable EmpInformationDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT EG.EmpInfoId,EG.EmpMasterCode,EG.EmpName,EG.DesigId,DS.DesigName,EG.DepId,DE.DeptName,EG.SectionId, 
                             SE.SectionName , G.GradeId,G.GradeType,EG.SalScaleId ,EG.CompanyInfoId,EG.UnitId,EG.DivisionId,DI.DivName,
                             CI.CompanyInfoId,UI.UnitId,DI.DivisionId
                             
                             FROM dbo.tblEmpGeneralInfo EG 
                             LEFT JOIN dbo.tblDesignation DS ON EG.DesigId=DS.DesigId 
                             LEFT JOIN dbo.tblCompanyInfo CI ON EG.CompanyInfoId=CI.CompanyInfoId
                             LEFT JOIN dbo.tblCompanyUnit UI ON EG.UnitId=UI.UnitId
                             LEFT JOIN dbo.tblDivision  DI ON EG.DivisionId =DI.DivisionId
                             LEFT JOIN dbo.tblDepartment DE ON EG.DepId=DE.DeptId 
                             LEFT JOIN dbo.tblSection SE ON EG.SectionId=SE.SectionId 
                             LEFT JOIN dbo.tblEmployeeGrade G ON EG.EmpGradeId=G.GradeId 
                             WHERE EG.EmpMasterCode='" + empId + "' and EG.EmployeeStatus='Active' and EG.ActionStatus in ('Accepted') and EG.IsActive=1 and EG.IsActive=1 AND EG.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" + HttpContext.Current.Session["UserId"].ToString() + "')";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public DataTable EmpSalInformationBll(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT SalaryInfoId,EmpInfoId,SalaryHeadId,SalHeadName,Amount
                             FROM tblSalaryInformation 
                             WHERE  IsActive=1 AND (InactiveDate IS NULL OR InactiveDate='')
                             AND SalHeadName='Gross' AND EmpInfoId='" + empId + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }


        public DataTable EmpSalaryDetailDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT ES.SalaryHeadId,SH.SalaryName,ES.Amount FROM tblSalaryInformation ES  " +
                            " LEFT JOIN dbo.tblSalaryHead SH ON ES.SalaryHeadId = SH.SalaryHeadId  " +
                            " WHERE ES.Status='active' AND ES.EmpInfoId='" + empId.Trim() + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public DataTable SalaryRule(string ruleName)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT * FROM dbo.tblSalaryRules WHERE RuleName='" + ruleName + "'";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public DataTable EmpInformationForPromotionDal(string empId)
        {
            DataTable aDataTableEmpInfo = new DataTable();
            string query = @"SELECT EG.EmpInfoId,EG.EmpMasterCode,EG.EmpName,EG.DesignationId,DS.DesigName,EG.DepartmentId,DE.DeptName,EG.SectionId, " +
                            " SE.SectionName ,EC.EmpCategoryId,EC.EmpCategoryName, G.GradeId,G.GradeType ,Convert(nvarchar(11),ESH.ActiveDate,106) as ActiveDate,ESH.Purpose,ESH.TotalSalary " +
                            " FROM dbo.tblEmpGeneralInfo EG " +
                            " LEFT JOIN tblEmpSalaryHistory ESH ON EG.EmpInfoId=ESH.EmpInfoId " +
                            " LEFT JOIN dbo.tblDesignation DS ON EG.DesignationId=DS.DesigId " +
                            " LEFT JOIN dbo.tblDepartment DE ON EG.DepartmentId=DE.DeptId " +
                            " LEFT JOIN dbo.tblSection SE ON EG.SectionId=SE.SectionId " +
                            " LEFT JOIN dbo.tblEmpCategory EC ON EG.EmpCategoryId=EC.EmpCategoryId " +
                            " LEFT JOIN dbo.tblGrade G ON EG.EmpGradeId=G.GradeId " +
                            " WHERE EG.EmpMasterCode='" + empId.Trim() + "' AND ESH.Status='active' ";
            aDataTableEmpInfo = aCommonInternalDal.DataContainerDataTable(query, "HRDB");

            return aDataTableEmpInfo;
        }
        public DataTable LoadIncrementViewForApproval(string ActionStatus)
        {
            string query = @"SELECT *,tblDesignation.DesigName,tblDepartment.DeptName FROM dbo.tblIncrement 
                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblIncrement.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                where tblIncrement.ActionStatus='" + ActionStatus + "' AND tblIncrement.IsActive=1 and tblEmpGeneralInfo.IsActive=1 AND tblEmpGeneralInfo.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" + HttpContext.Current.Session["UserId"].ToString() + "') order by tblIncrement.IncrementId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpIncrementView()
        {
            string query =
                @"SELECT *,tblDesignation.DesigName,tblDepartment.DeptName FROM dbo.tblIncrement 
                LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblIncrement.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId
                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId
                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                where tblIncrement.IsActive=1 and tblIncrement.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblIncrement.IncrementId desc ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadActdt(string ID)
        {
            string query =
                @"SELECT * FROM dbo.tblIncrementDetails where IncrementId='" + ID + "' and IsActive='true'";
                return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
      
        public bool DeleteData(string IncrementId)
        {
            string query = @"Delete from  tblIncrementDetails where IncrementId= '" + IncrementId + "'";
            aCommonInternalDal.DeleteDataByDeleteCommand(query, "HRDB");
            return aCommonInternalDal.DeleteStatusUpdate("tblIncrement", "IncrementId", IncrementId);
        }

        public DataTable LoadEmployeeViewDeptWise()
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo 
             LEFT JOIN tblDesignation ON tblEmpGeneralInfo.DesigId = tblDesignation.DesigId 
             LEFT JOIN tblDepartment ON tblEmpGeneralInfo.DepId = tblDepartment.DeptId 
             LEFT JOIN tblSection ON tblEmpGeneralInfo.SectionId = tblSection.SectionId 
             LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId=dbo.tblEmployeeGrade.GradeId
             LEFT JOIN dbo.tblSalaryGradeOrScale ON dbo.tblEmpGeneralInfo.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId
             LEFT JOIN tblEmployeeType ON tblEmpGeneralInfo.EmpTypeId = tblEmployeeType.EmpTypeId 
             INNER JOIN
             (SELECT EmpInfoId,Amount FROM dbo.tblSalaryInformation WHERE ActionStatus='Posted' AND SalHeadName='Gross' AND  IsActive=1) AS tblSal
             ON tblEmpGeneralInfo.EmpInfoId=tblSal.EmpInfoId
             WHERE tblEmpGeneralInfo.ActionStatus in ('Posted','Cancel') and tblEmpGeneralInfo.IsActive=1 and tblEmpGeneralInfo.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblEmpGeneralInfo.EmpInfoId desc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadSalaryView(string empinfoId)
        {
            string query = @"SELECT * FROM dbo.tblSalaryInformation WHERE EmpInfoId='" + empinfoId + "' AND IsActive=1 AND ActionStatus='Posted'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpView(string empinfoId)
        {
            string query = @"SELECT EmpMasterCode,EmpName,DeptName,DesigName FROM dbo.tblEmpGeneralInfo
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId = dbo.tblDepartment.DeptId
                            LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                            WHERE EmpInfoId='" + empinfoId + "' AND tblEmpGeneralInfo.IsActive=1 AND tblEmpGeneralInfo.ActionStatus='Posted'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadIncrement(string IncrementId)
        {
            string query = @"SELECT *,SalHeadName as SalaryHead FROM dbo.tblIncrement
                            LEFT JOIN dbo.tblIncrementDetails ON dbo.tblIncrement.IncrementId = dbo.tblIncrementDetails.IncrementId 
                            LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblIncrement.EmpInfoId=dbo.tblEmpGeneralInfo.EmpInfoId WHERE dbo.tblIncrement.IncrementId='" + IncrementId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool InsertIncrementDetailsBLL(EmployeeSalary aEmployeeSalary)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalaryInfoId", aEmployeeSalary.SalaryInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmployeeSalary.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aEmployeeSalary.SalaryHeadId));
            aSqlParameterlist.Add(new SqlParameter("@SalHeadName", aEmployeeSalary.SalaryHead));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aEmployeeSalary.Amount));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aEmployeeSalary.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aEmployeeSalary.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aEmployeeSalary.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aEmployeeSalary.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@Modified", aEmployeeSalary.Modified));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aEmployeeSalary.IsActive));


            string insertQuery = @"insert into tblSalaryInformation (SalaryInfoId,EmpInfoId,SalaryHeadId,SalHeadName,Amount,ActiveDate,EntryUser,EntryDate,ActionStatus,Modified,IsActive) 
            values (@SalaryInfoId,@EmpInfoId,@SalaryHeadId,@SalHeadName,@Amount,@ActiveDate,@EntryUser,@EntryDate,@ActionStatus,@Modified,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool UpdateIncrementDetailsDAL(EmployeeSalary aEmployeeSalary , string empId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", empId));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", "false"));
            aSqlParameterlist.Add(new SqlParameter("@InactiveDate", aEmployeeSalary.InactiveDate));

            string updateQuery = @"UPDATE tblSalaryInformation SET IsActive=@IsActive, InactiveDate=@InactiveDate WHERE EmpInfoId=@EmpInfoId";
            return  aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }

        public bool UpdateIncrement(Increment aIncrement)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@IncrementId", aIncrement.IncrementId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aIncrement.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aIncrement.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aIncrement.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aIncrement.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aIncrement.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aIncrement.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aIncrement.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aIncrement.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@IncrementAmount", aIncrement.IncrementAmount));
            aSqlParameterlist.Add(new SqlParameter("@PreviousGrossSalary", aIncrement.PreviousGrossSalary));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aIncrement.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aIncrement.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@IncrementRemarks", aIncrement.IncrementRemarks));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aIncrement.IsActive));

            string updateQuery = @"UPDATE tblIncrement SET EmpInfoId=@EmpInfoId,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,SectionId=@SectionId,DesigId=@DesigId,DeptId=@DeptId,SalScaleId=@SalScaleId,IncrementAmount=@IncrementAmount,PreviousGrossSalary=@PreviousGrossSalary,ActionStatus=@ActionStatus,ActiveDate=@ActiveDate,IncrementRemarks=@IncrementRemarks,IsActive=@IsActive WHERE IncrementId=@IncrementId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(updateQuery, aSqlParameterlist, "HRDB");
        }
        public bool DeleteIncrementDetail(string incrementId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@IncrementId", incrementId));

            string deleteQuery = @"DELETE FROM dbo.tblIncrementDetails WHERE IncrementId=@IncrementId";
            return aCommonInternalDal.DeleteDataByDeleteCommand(deleteQuery, aSqlParameterlist, "HRDB");
        }

    }
}
