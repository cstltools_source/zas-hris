﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalaryBenefitDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveEmpSalBenefit(EmpSalBenefit aEmpSalBenefit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@BenefitId", aEmpSalBenefit.BenefitId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpSalBenefit.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aEmpSalBenefit.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aEmpSalBenefit.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aEmpSalBenefit.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aEmpSalBenefit.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aEmpSalBenefit.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aEmpSalBenefit.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aEmpSalBenefit.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aEmpSalBenefit.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aEmpSalBenefit.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aEmpSalBenefit.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@BenefitType", aEmpSalBenefit.BenefitType));
            aSqlParameterlist.Add(new SqlParameter("@BenefitAmount", aEmpSalBenefit.BenefitAmount));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aEmpSalBenefit.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aEmpSalBenefit.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aEmpSalBenefit.Status));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", "Posted"));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", "True"));

            string insertQuery = @"insert into tblSalaryBenefit (BenefitId,EmpInfoId,CompanyInfoId,UnitId,DivisionId,DesigId,GradeId,DeptId,SectionId,EmpTypeId,SalScaleId,EffectiveDate,BenefitType,BenefitAmount,EntryBy,EntryDate,Status,ActionStatus,IsActive) 
            values (@BenefitId,@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@DesigId,@GradeId,@DeptId,@SectionId,@EmpTypeId,@SalScaleId,@EffectiveDate,@BenefitType,@BenefitAmount,@EntryBy,@EntryDate,@Status,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasEmpSalBenefit(EmpSalBenefit aEmpSalBenefit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpSalBenefit.EmpInfoId));
            string query = "select * from tblSalaryBenefit where EmpInfoId = @EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadEmpSalBenefitView()
        {
            string query =@"SELECT * FROM tblSalaryBenefit " +
                " LEFT JOIN tblEmpGeneralInfo ON tblSalaryBenefit.EmpInfoId = tblEmpGeneralInfo.EmpInfoId  " +
                " LEFT JOIN tblCompanyInfo ON tblSalaryBenefit.CompanyInfoId = tblCompanyInfo.CompanyInfoId  " +
                " LEFT JOIN tblCompanyUnit ON tblSalaryBenefit.UnitId = tblCompanyUnit.UnitId  " +
                " LEFT JOIN tblDivision ON tblSalaryBenefit.DivisionId = tblDivision.DivisionId  " +
                " LEFT JOIN tblSection ON tblSalaryBenefit.SectionId = tblSection.SectionId  " +
                " LEFT JOIN tblSalaryGradeOrScale ON tblSalaryBenefit.SalScaleId = tblSalaryGradeOrScale.SalScaleId  " +
                " LEFT JOIN tblEmployeeGrade ON tblSalaryBenefit.GradeId = tblEmployeeGrade.GradeId " +
                " LEFT JOIN tblEmployeeType ON tblSalaryBenefit.EmpTypeId = tblEmployeeType.EmpTypeId  " +
                " LEFT JOIN tblDesignation ON tblSalaryBenefit.DesigId = tblDesignation.DesigId  " +
                " LEFT JOIN tblDepartment ON tblSalaryBenefit.DeptId = tblDepartment.DeptId where tblSalaryBenefit.ActionStatus in ('Posted','Cancel') and tblSalaryBenefit.IsActive=1 and tblSalaryBenefit.Entryby='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblSalaryBenefit.BenefitId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadSalaryBenefitViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From tblSalaryBenefit
                            LEFT JOIN tblEmpGeneralInfo ON tblSalaryBenefit.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                            LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                            where tblSalaryBenefit.ActionStatus='" + ActionStatus + "' AND tblSalaryBenefit.IsActive=1 order by tblSalaryBenefit.BenefitId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr,"HRDB");
        }
        
        public void LoadDepartmentName(DropDownList ddl,string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='"+divisionId+"'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr,"HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr,"HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr,"HRDB");
        }
        public void LoadUnitName(DropDownList ddl,string CompanyInfoId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + CompanyInfoId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr,"HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr,"HRDB");
        }
        public void LoadSectionName(DropDownList ddl,string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='"+deptId+"'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr,"HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalScaleName", "SalScaleId", queryStr, "HRDB");
        }
        public void LoadGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeGrade";
            aInternalDal.LoadDropDownValue(ddl, "GradeName", "GradeId", queryStr,"HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }
        public DataTable LoadEmpInfo(string empcode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblCompanyInfo.CompanyInfoId , GradeId,SalScaleName,DesigName ,dbo.tblDesignation.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,dbo.tblEmpGeneralInfo.SalScaleId ,GradeName , dbo.tblSalaryGradeOrScale.SalScaleId , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo                        
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblSalaryGradeOrScale ON dbo.tblEmpGeneralInfo.SalScaleId=dbo.tblSalaryGradeOrScale.SalScaleId where EmpMasterCode='" + empcode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public EmpSalBenefit EmpSalBenefitEditLoad(string BenefitId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@BenefitId", BenefitId));
            string query = "select * from tblSalaryBenefit where BenefitId = @BenefitId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            EmpSalBenefit aEmpSalBenefit = new EmpSalBenefit();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aEmpSalBenefit.BenefitId = Int32.Parse(dataReader["BenefitId"].ToString());
                    aEmpSalBenefit.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aEmpSalBenefit.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aEmpSalBenefit.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aEmpSalBenefit.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aEmpSalBenefit.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aEmpSalBenefit.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aEmpSalBenefit.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aEmpSalBenefit.GradeId = Convert.ToInt32(dataReader["GradeId"].ToString());
                    aEmpSalBenefit.SalScaleId = Convert.ToInt32(dataReader["SalScaleId"].ToString());
                    aEmpSalBenefit.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aEmpSalBenefit.BenefitAmount = Convert.ToDecimal(dataReader["BenefitAmount"].ToString());
                    aEmpSalBenefit.BenefitType = dataReader["BenefitType"].ToString();
                    //aEmpSalBenefit.EntryBy = dataReader["EntryBy"].ToString();
                    //aEmpSalBenefit.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    aEmpSalBenefit.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    //aEmpSalBenefit.ApprovedBy = dataReader["ApprovedBy"].ToString();
                    //aEmpSalBenefit.ApprovedDate = Convert.ToDateTime(dataReader["ApprovedDate"].ToString());
                  //  aEmpSalBenefit.Status = dataReader["Status"].ToString();
                }
            }
            return aEmpSalBenefit;
        }

        public bool UpdateEmpSalBenefit(EmpSalBenefit aEmpSalBenefit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@BenefitId", aEmpSalBenefit.BenefitId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpSalBenefit.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aEmpSalBenefit.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aEmpSalBenefit.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aEmpSalBenefit.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aEmpSalBenefit.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aEmpSalBenefit.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aEmpSalBenefit.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aEmpSalBenefit.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aEmpSalBenefit.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aEmpSalBenefit.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aEmpSalBenefit.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@BenefitType", aEmpSalBenefit.BenefitType));
            aSqlParameterlist.Add(new SqlParameter("@BenefitAmount", aEmpSalBenefit.BenefitAmount));
            

            string query = @"UPDATE tblSalaryBenefit SET CompanyInfoId=@CompanyInfoId,EmpInfoId=@EmpInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,GradeId=@GradeId,EmpTypeId=@EmpTypeId,SalScaleId=@SalScaleId,EffectiveDate=@EffectiveDate,BenefitType=@BenefitType,BenefitAmount=@BenefitAmount WHERE BenefitId=@BenefitId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(EmpSalBenefit aEmpSalBenefit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@BenefitId", aEmpSalBenefit.BenefitId));
            aSqlParameterlist.Add(new SqlParameter("@Status", aEmpSalBenefit.Status));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aEmpSalBenefit.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aEmpSalBenefit.ApprovedDate));

            string query = @"UPDATE tblSalaryBenefit SET Status=@Status,ActionStatus=@Status,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE BenefitId=@BenefitId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string BenefitId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblSalaryBenefit", "BenefitId", BenefitId);
        }
    }
}
