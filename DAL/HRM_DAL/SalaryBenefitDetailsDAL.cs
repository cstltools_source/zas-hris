﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalaryBenefitDetailsDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveSalaryBenefitDetails(SalaryBenefitDetails aBenefitDetails)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SBDId", aBenefitDetails.SBDId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aBenefitDetails.SalaryHeadId));
            aSqlParameterlist.Add(new SqlParameter("@SalHeadName", aBenefitDetails.SalHeadName));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aBenefitDetails.Amount));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadType", aBenefitDetails.SalaryHeadType));
            aSqlParameterlist.Add(new SqlParameter("@BenefitId", aBenefitDetails.BenefitId));

            string insertQuery = @"insert into tblSalaryBenefitDetails (SBDId,SalaryHeadId,SalHeadName,Amount,SalaryHeadType,BenefitId) 
            values (@SBDId,@SalaryHeadId,@SalHeadName,@Amount,@SalaryHeadType,@BenefitId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        
        public bool HasSalaryBenefitDetails(SalaryBenefitDetails aBenefitDetails)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@Amount", aBenefitDetails.Amount));
            string query = "select * from tblSalaryBenefitDetails where Amount = @Amount";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadSalaryBenefitDetailsView()
        {
            string query =@"SELECT tblSalaryBenefitDetails.SBDId,tblSalaryHead.SalaryHeadId, tblSalaryHead.SalHeadName,tblSalaryBenefitDetails.Amount,tblSalaryHead.SalaryHeadType,tblSalaryBenefit.BenefitId, FROM tblSalaryBenefitDetails " +
                " LEFT JOIN tblSalaryHead ON tblSalaryBenefitDetails.SalaryHeadId = tblSalaryHead.SalaryHeadId  " +
                " LEFT JOIN tblSalaryBenefit ON tblSalaryBenefitDetails.BenefitId = tblSalaryBenefit.BenefitId  order by tblSalaryBenefitDetails.SBDId desc ";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadSalHeadName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryHead";
            aInternalDal.LoadDropDownValue(ddl, "SalHeadName", "SalaryHeadId", queryStr, "HRDB");
        }

        public void LoadBenefitName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryBenefit";
            aInternalDal.LoadDropDownValue(ddl, "BenefitName", "BenefitId", queryStr, "HRDB");
        }

        public SalaryBenefitDetails SalaryBenefitDetailsEditLoad(string SBDId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SBDId", SBDId));
            string query = "select * from tblSalaryBenefitDetails where SBDId = @SBDId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            SalaryBenefitDetails aBenefitDetails = new SalaryBenefitDetails();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aBenefitDetails.SBDId = Int32.Parse(dataReader["SBDId"].ToString());
                    aBenefitDetails.SalHeadName = dataReader["SalHeadName"].ToString();
                    aBenefitDetails.SalaryHeadId = Convert.ToInt32(dataReader["SalaryHeadId"].ToString());
                    aBenefitDetails.Amount = Convert.ToDecimal(dataReader["Amount"].ToString());
                    aBenefitDetails.SalaryHeadType = dataReader["SalaryHeadType"].ToString();
                    aBenefitDetails.BenefitId = Convert.ToInt32(dataReader["BenefitId"].ToString()); 
                }
            }
            return aBenefitDetails;
        }

        public bool UpdateaBenefitDetails(SalaryBenefitDetails aBenefitDetails)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SBDId", aBenefitDetails.SBDId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aBenefitDetails.SalaryHeadId));
            aSqlParameterlist.Add(new SqlParameter("@SalHeadName", aBenefitDetails.SalHeadName));
            aSqlParameterlist.Add(new SqlParameter("@Amount", aBenefitDetails.Amount));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadType", aBenefitDetails.SalaryHeadType));
            aSqlParameterlist.Add(new SqlParameter("@BenefitId", aBenefitDetails.BenefitId));

            string query = @"UPDATE tblSalaryBenefitDetails SET SalHeadName=@SalHeadName,SalaryHeadId=@SalaryHeadId,Amount=@Amount,SalaryHeadType=@SalaryHeadType,BenefitId=@BenefitId WHERE SBDId=@SBDId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
