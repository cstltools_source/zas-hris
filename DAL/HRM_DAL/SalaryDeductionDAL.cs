﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalaryDeductionDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveSalaryDeduction(SalaryDeduction aSalaryDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SDId", aSalaryDeduction.SDId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aSalaryDeduction.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@SDAmount", aSalaryDeduction.SDAmount));
            aSqlParameterlist.Add(new SqlParameter("@SDReason", aSalaryDeduction.SDReason));
            aSqlParameterlist.Add(new SqlParameter("@SDEffectiveDate", aSalaryDeduction.SDEffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aSalaryDeduction.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aSalaryDeduction.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aSalaryDeduction.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aSalaryDeduction.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aSalaryDeduction.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aSalaryDeduction.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aSalaryDeduction.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aSalaryDeduction.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aSalaryDeduction.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aSalaryDeduction.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aSalaryDeduction.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aSalaryDeduction.IsActive));


            string insertQuery = @"insert into tblSalaryDeduction (SDId,EmpId,SDAmount,SDReason,SDEffectiveDate,CompanyInfoId,UnitId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,DivisionId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@SDId,@EmpId,@SDAmount,@SDReason,@SDEffectiveDate,@CompanyInfoId,@UnitId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@EmpGradeId,@DivisionId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public void LoadSalHeadName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryHead";
            aInternalDal.LoadDropDownValue(ddl, "SalaryName", "SalaryHeadId", queryStr, "HRDB");
        }
        public DataTable LoadSalaryDeductionView()
        {
            string query = @"SELECT  SDId,EmpMasterCode ,EmpName , SDEffectiveDate , SDAmount,DesigName,tblSalaryDeduction.ActionStatus,tblSection.SectionName,tblDivision.DivName,tblDepartment.DeptName FROM tblSalaryDeduction 
                 LEFT JOIN tblEmpGeneralInfo ON tblSalaryDeduction.EmpId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblSalaryDeduction.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblSalaryDeduction.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblSalaryDeduction.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblSalaryDeduction.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblSalaryDeduction.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblSalaryDeduction.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblSalaryDeduction.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblSalaryDeduction.DeptId = tblDepartment.DeptId where tblSalaryDeduction.ActionStatus in ('Posted','Cancel') and tblSalaryDeduction.IsActive=1 and tblSalaryDeduction.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblSalaryDeduction.SDId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSalaryDeductionViewForApproval(string ActionStatus)
        {
            string query = @"SELECT *,tblSection.SectionName as SectionName,tblDepartment.DeptName as DepartmentName,tblDivision.DivName as DivName,tblDesignation.DesigName From tblSalaryDeduction
                LEFT JOIN tblEmpGeneralInfo ON tblSalaryDeduction.EmpId = tblEmpGeneralInfo.EmpInfoId 
                 LEFT JOIN tblCompanyInfo ON tblSalaryDeduction.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblSalaryDeduction.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblSalaryDeduction.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblDesignation ON tblSalaryDeduction.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblSalaryDeduction.DeptId = tblDepartment.DeptId
                 LEFT JOIN tblSection ON tblSalaryDeduction.SectionId = tblSection.SectionId 
                where tblSalaryDeduction.ActionStatus='" + ActionStatus + "' AND tblSalaryDeduction.IsActive=1 order by tblSalaryDeduction.SDId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public SalaryDeduction SalaryDeductionEditLoad(string SDId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SDId", SDId));
            string query = "select * from tblSalaryDeduction where SDId = @SDId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            SalaryDeduction aSalaryDeduction = new SalaryDeduction();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aSalaryDeduction.SDId = Int32.Parse(dataReader["SDId"].ToString());
                    aSalaryDeduction.EmpId = Convert.ToInt32(dataReader["EmpId"].ToString());
                    aSalaryDeduction.SDAmount = Convert.ToDecimal(dataReader["SDAmount"].ToString());
                    aSalaryDeduction.SDReason = dataReader["SDReason"].ToString();
                    aSalaryDeduction.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aSalaryDeduction.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aSalaryDeduction.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aSalaryDeduction.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aSalaryDeduction.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aSalaryDeduction.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aSalaryDeduction.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aSalaryDeduction.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aSalaryDeduction.ActionStatus = dataReader["ActionStatus"].ToString();
                    aSalaryDeduction.ActionStatus = dataReader["ActionStatus"].ToString();
                    aSalaryDeduction.EntryUser = dataReader["EntryUser"].ToString();
                    aSalaryDeduction.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    aSalaryDeduction.SDEffectiveDate = Convert.ToDateTime(dataReader["SDEffectiveDate"].ToString());

                }
            }
            return aSalaryDeduction;
        }

        public bool UpdateSalaryDeduction(SalaryDeduction aSalaryDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SDId", aSalaryDeduction.SDId));
            aSqlParameterlist.Add(new SqlParameter("@EmpId", aSalaryDeduction.EmpId));
            aSqlParameterlist.Add(new SqlParameter("@SDAmount", aSalaryDeduction.SDAmount));
            aSqlParameterlist.Add(new SqlParameter("@SDReason", aSalaryDeduction.SDReason));
            aSqlParameterlist.Add(new SqlParameter("@SDEffectiveDate", aSalaryDeduction.SDEffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aSalaryDeduction.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aSalaryDeduction.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aSalaryDeduction.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aSalaryDeduction.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aSalaryDeduction.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aSalaryDeduction.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aSalaryDeduction.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EmpGradeId", aSalaryDeduction.EmpGradeId));

            string query = @"UPDATE tblSalaryDeduction SET EmpId=@EmpId,SDAmount=@SDAmount,SDReason=@SDReason,SDEffectiveDate=@SDEffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@EmpGradeId,DivisionId=@DivisionId WHERE SDId=@SDId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(SalaryDeduction aSalaryDeduction)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SDId", aSalaryDeduction.SDId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aSalaryDeduction.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aSalaryDeduction.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aSalaryDeduction.ApprovedDate));

            string query = @"UPDATE tblSalaryDeduction SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE SDId=@SDId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string SDId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblSalaryDeduction", "SDId", SDId);
        }
    }
}
