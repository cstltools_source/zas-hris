﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalaryHeadDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveSalaryHead(SalaryHead aSalaryHead)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aSalaryHead.SalaryHeadId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadCode", aSalaryHead.SalaryHeadCode));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadName", aSalaryHead.SalaryHeadName));
            aSqlParameterlist.Add(new SqlParameter("@SHeadTypeId", aSalaryHead.SHeadTypeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aSalaryHead.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aSalaryHead.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", aSalaryHead.Remarks));

            string insertQuery = @"insert into tblSalaryHead (SalaryHeadId,SalaryHeadCode,SalaryHeadName,SHeadTypeId,EntryBy,EntryDate,Remarks) 
            values (@SalaryHeadId,@SalaryHeadCode,@SalaryHeadName,@SHeadTypeId,@EntryBy,@EntryDate,@Remarks)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }

        public bool HasSalaryHeadName(SalaryHead aSalaryHead)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadName", aSalaryHead.SalaryHeadName));
            string query = "select * from tblSalaryHead where SalaryHeadName = @SalaryHeadName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        
        public DataTable LoadSalaryHead()
        {
            string query = @"SELECT * from tblSalaryHead
                           LEFT JOIN dbo.tblSalaryHeadType ON dbo.tblSalaryHead.SHeadTypeId = dbo.tblSalaryHeadType.SHeadTypeId ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadSalaryHeadType(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryHeadType";
            aInternalDal.LoadDropDownValue(ddl, "SHeadType", "SHeadTypeId", queryStr, "HRDB");
        }

        public SalaryHead SalaryHeadEditLoad(string aSalaryHeadId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aSalaryHeadId));
            string query = "select * from tblSalaryHead where SalaryHeadId = @SalaryHeadId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            SalaryHead aSalaryHead = new SalaryHead();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aSalaryHead.SalaryHeadId = Int32.Parse(dataReader["SalaryHeadId"].ToString());
                    aSalaryHead.SalaryHeadCode = dataReader["SalaryHeadCode"].ToString();
                    aSalaryHead.SalaryHeadName = dataReader["SalaryHeadName"].ToString();
                    aSalaryHead.SHeadTypeId = Convert.ToInt32(dataReader["SHeadTypeId"].ToString());
                    aSalaryHead.EntryBy = dataReader["EntryBy"].ToString();
                    aSalaryHead.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    aSalaryHead.Remarks = dataReader["Remarks"].ToString();
                }

            }
            return aSalaryHead;
        }

        public bool UpdateSalaryHeadInfoUpdate(SalaryHead aSalaryHead)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadId", aSalaryHead.SalaryHeadId));
            aSqlParameterlist.Add(new SqlParameter("@SalaryHeadName", aSalaryHead.SalaryHeadName));
            aSqlParameterlist.Add(new SqlParameter("@SHeadTypeId", aSalaryHead.SHeadTypeId));
            aSqlParameterlist.Add(new SqlParameter("@Remarks", aSalaryHead.Remarks));

            string query = @"UPDATE tblSalaryHead SET SalaryHeadName=@SalaryHeadName,SHeadTypeId=@SHeadTypeId WHERE SalaryHeadId=@SalaryHeadId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}

