﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalaryHeadTypeDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveDataForSalaryHeadType(SalaryHeadType aSalaryHeadType)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SHeadTypeId", aSalaryHeadType.SHeadTypeId));
            aSqlParameterlist.Add(new SqlParameter("@SHeadType", aSalaryHeadType.SHeadType));
            string insertQuery = @"insert into tblSalaryHeadType (SHeadTypeId,SHeadType) 
            values (@SHeadTypeId,@SHeadType)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasSalaryHeadType(SalaryHeadType aSalaryHeadType)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SHeadType", aSalaryHeadType.SHeadType));
            string query = "select * from tblSalaryHeadType where SHeadType=@SHeadType";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public DataTable LoadSalaryHeadTypeView()
        {
            string query = @"SELECT * FROM tblSalaryHeadType ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public SalaryHeadType SalaryHeadTypeEditLoad(string SHeadTypeId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SHeadTypeId", SHeadTypeId));
            string query = "select * from tblSalaryHeadType where SHeadTypeId=@SHeadTypeId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            SalaryHeadType aSalaryHeadType = new SalaryHeadType();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aSalaryHeadType.SHeadTypeId = Int32.Parse(dataReader["SHeadTypeId"].ToString());
                    aSalaryHeadType.SHeadType = dataReader["SHeadType"].ToString();
                }
            }
            return aSalaryHeadType;
        }

        public bool UpdateSalaryHeadType(SalaryHeadType aSalaryHeadType)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SHeadTypeId", aSalaryHeadType.SHeadTypeId));
            aSqlParameterlist.Add(new SqlParameter("@SHeadType", aSalaryHeadType.SHeadType));

            string query = @"UPDATE tblSalaryHeadType SET SHeadType=@SHeadType WHERE SHeadTypeId=@SHeadTypeId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
