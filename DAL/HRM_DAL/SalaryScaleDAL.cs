﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalaryScaleDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveGradeSalary(SalaryScale aSalaryScale)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aSalaryScale.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleCode", aSalaryScale.SalScaleCode));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleName", aSalaryScale.SalScaleName));
            aSqlParameterlist.Add(new SqlParameter("@BasicSalary", aSalaryScale.BasicSalary));
            aSqlParameterlist.Add(new SqlParameter("@HouseRent", aSalaryScale.HouseRent));
            aSqlParameterlist.Add(new SqlParameter("@Medical", aSalaryScale.Medical));
            aSqlParameterlist.Add(new SqlParameter("@IncrementRate", aSalaryScale.IncrementRate));
            aSqlParameterlist.Add(new SqlParameter("@Gross", aSalaryScale.Gross));
            //aSqlParameterlist.Add(new SqlParameter("@ProvidentFund", aSalaryScale.ProvidentFund));
            aSqlParameterlist.Add(new SqlParameter("@Conveyance", aSalaryScale.Conveyance));
            aSqlParameterlist.Add(new SqlParameter("@Foodallowance", aSalaryScale.Foodallowance));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aSalaryScale.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aSalaryScale.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aSalaryScale.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aSalaryScale.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aSalaryScale.IsActive));

            string insertQuery = @"insert into tblSalaryGradeOrScale (SalScaleId,SalScaleCode,SalScaleName,BasicSalary,HouseRent,Medical,IncrementRate,Conveyance,Foodallowance,ActiveDate,ActionStatus,EntryBy,EntryDate,Gross,IsActive) 
            values (@SalScaleId,@SalScaleCode,@SalScaleName,@BasicSalary,@HouseRent,@Medical,@IncrementRate,@Conveyance,@Foodallowance,@ActiveDate,@ActionStatus,@EntryBy,@EntryDate,@Gross,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }

        public DataTable LoadSalaryScaleView()
        {
            string query = @"SELECT * FROM dbo.tblSalaryGradeOrScale where tblSalaryGradeOrScale.ActionStatus in ('Posted','Cancel') and tblSalaryGradeOrScale.IsActive=1 and tblSalaryGradeOrScale.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblSalaryGradeOrScale.SalScaleId desc";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadGrade(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "SELECT GradeId,GradeCode,(GradeName+':'+GradeType) AS EmployeeGrade FROM dbo.tblGrade";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeGrade", "GradeId", queryStr, "HRDB");
        }
        public bool HasSalScaleName(SalaryScale aSalaryScale)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalScaleName", aSalaryScale.SalScaleName));
            string query = "select * from tblSalaryGradeOrScale where SalScaleName = @SalScaleName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public SalaryScale SalaryScaleEditLoad(string SalScaleId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", SalScaleId));
            string query = "select * from tblSalaryGradeOrScale where SalScaleId = @SalScaleId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            SalaryScale aSalaryScale = new SalaryScale();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aSalaryScale.SalScaleId = Int32.Parse(dataReader["SalScaleId"].ToString());
                    aSalaryScale.SalScaleCode = dataReader["SalScaleCode"].ToString();
                    aSalaryScale.SalScaleName = dataReader["SalScaleName"].ToString();
                    aSalaryScale.BasicSalary = Convert.ToDecimal(dataReader["BasicSalary"].ToString());
                    aSalaryScale.HouseRent = Convert.ToDecimal(dataReader["HouseRent"].ToString());
                    aSalaryScale.Medical = Convert.ToDecimal(dataReader["Medical"].ToString());
                    aSalaryScale.IncrementRate = Convert.ToDecimal(dataReader["IncrementRate"].ToString());
                    aSalaryScale.Foodallowance = Convert.ToDecimal(dataReader["Foodallowance"].ToString());
                    aSalaryScale.Conveyance = Convert.ToDecimal(dataReader["Conveyance"].ToString());
                    aSalaryScale.ActiveDate = Convert.ToDateTime(dataReader["ActiveDate"].ToString());
                    aSalaryScale.ActionStatus = dataReader["ActionStatus"].ToString();
                    aSalaryScale.EntryBy = dataReader["EntryBy"].ToString();
                    aSalaryScale.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    
                }
            }
            return aSalaryScale;
        }

        public bool UpdateSalaryScale(SalaryScale aSalaryScale)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aSalaryScale.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@SalScaleName", aSalaryScale.SalScaleName));
            aSqlParameterlist.Add(new SqlParameter("@BasicSalary", aSalaryScale.BasicSalary));
            aSqlParameterlist.Add(new SqlParameter("@HouseRent", aSalaryScale.HouseRent));
            aSqlParameterlist.Add(new SqlParameter("@Medical", aSalaryScale.Medical));
            aSqlParameterlist.Add(new SqlParameter("@IncrementRate", aSalaryScale.IncrementRate));
            aSqlParameterlist.Add(new SqlParameter("@Conveyance", aSalaryScale.Conveyance));
            //aSqlParameterlist.Add(new SqlParameter("@ProvidentFund", aSalaryScale.ProvidentFund));
            aSqlParameterlist.Add(new SqlParameter("@Foodallowance", aSalaryScale.Foodallowance));
            aSqlParameterlist.Add(new SqlParameter("@ActiveDate", aSalaryScale.ActiveDate));
            aSqlParameterlist.Add(new SqlParameter("@Gross", aSalaryScale.Gross));

            string query = @"UPDATE tblSalaryGradeOrScale SET SalScaleName=@SalScaleName,BasicSalary=@BasicSalary,HouseRent=@HouseRent,Medical=@Medical,IncrementRate=@IncrementRate,Foodallowance=@Foodallowance,Conveyance=@Conveyance,ActiveDate=@ActiveDate,Gross=@Gross WHERE SalScaleId=@SalScaleId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(SalaryScale aSalaryScale)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalScaleId", aSalaryScale.SalScaleId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aSalaryScale.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aSalaryScale.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aSalaryScale.ApprovedDate));

            string query = @"UPDATE tblSalaryGradeOrScale SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE SalScaleId=@SalScaleId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        
        public DataTable LoadSalaryScaleViewForApproval(string ActionStatus)
        {
            string query = @"SELECT * From dbo.tblSalaryGradeOrScale WHERE tblSalaryGradeOrScale.ActionStatus='" + ActionStatus + "' AND IsActive=1 order by tblSalaryGradeOrScale.SalScaleId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteData(string SalScaleId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblSalaryGradeOrScale", "SalScaleId", SalScaleId);
            
        }
    }
}
