﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SalaryStartStopDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveSalaryStartStop(SalaryStartStop aSalaryStartStop)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SalaryStopId", aSalaryStartStop.SalaryStopId));
            aSqlParameterlist.Add(new SqlParameter("@FyrId", aSalaryStartStop.FyrId));
            aSqlParameterlist.Add(new SqlParameter("@Year", aSalaryStartStop.Year));
            aSqlParameterlist.Add(new SqlParameter("@Month", aSalaryStartStop.Month));
            aSqlParameterlist.Add(new SqlParameter("@LockedBy", aSalaryStartStop.LockedBy));
            aSqlParameterlist.Add(new SqlParameter("@LockedDate", aSalaryStartStop.LockedDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aSalaryStartStop.Status));

            string insertQuery = @"INSERT INTO dbo.tblSalaryStartStop
                                    ( SalaryStopId ,
                                      FyrId ,
                                      Year ,
                                      Month ,
                                      LockedBy ,
                                      LockedDate,
                                      Status 
                                    )
                            VALUES  (  @SalaryStopId ,
                                      @FyrId ,
                                      @Year ,
                                      @Month ,
                                      @LockedBy ,
                                      @LockedDate ,
                                      @Status
                                    )";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool UpdateSalaryStartStop(SalaryStartStop aSalaryStartStop)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FyrId", aSalaryStartStop.FyrId));
            aSqlParameterlist.Add(new SqlParameter("@Year", aSalaryStartStop.Year));
            aSqlParameterlist.Add(new SqlParameter("@Month", aSalaryStartStop.Month));
            aSqlParameterlist.Add(new SqlParameter("@UnlockedBy", aSalaryStartStop.UnlockedBy));
            aSqlParameterlist.Add(new SqlParameter("@UnlockedDate", aSalaryStartStop.UnlockedDate));

            string Query = @"UPDATE dbo.tblSalaryStartStop SET Status='Unlocked' , UnlockedBy=@UnlockedBy , UnlockedDate=@UnlockedDate  WHERE FyrId=@FyrId AND Month=@Month AND Year=@Year AND Status='Locked'";
            return aCommonInternalDal.UpdateDataByUpdateCommand(Query, aSqlParameterlist, "HRDB");
        }
        public bool HsSalaryStartStop(SalaryStartStop aSalaryStartStop)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@FyrId", aSalaryStartStop.FyrId));
            aSqlParameterlist.Add(new SqlParameter("@Year", aSalaryStartStop.Year));
            aSqlParameterlist.Add(new SqlParameter("@Month", aSalaryStartStop.Month));
            //aSqlParameterlist.Add(new SqlParameter("@Status", aSalaryStartStop.Status));
            string query = "SELECT * FROM dbo.tblSalaryStartStop WHERE FyrId=@FyrId AND Year=@Year AND Month=@Month AND Status='Locked' ";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public void LoadFinancialYear(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "SELECT * FROM dbo.tblFinancialYear";
            aInternalDal.LoadDropDownValue(ddl, "FiscalYearId", "FyrCode", queryStr, "HRDB");
        }
        public DataTable LoadSalaryStop(string fytId, string year, string month)
        {
            string query =
                @"SELECT * FROM dbo.tblSalaryStartStop WHERE FyrId='"+fytId+"' AND Year='"+year+"' AND Month='"+month+"' AND Status='Locked'";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

    }
}
