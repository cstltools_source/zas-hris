﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SectionDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveSectionInfo(Section aSection)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aSection.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@SectionCode", aSection.SectionCode));
            aSqlParameterlist.Add(new SqlParameter("@SectionName", aSection.SectionName));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aSection.DepartmentId));

            string insertQuery = @"insert into tblSection (SectionId,SectionCode,SectionName,DeptId) 
            values (@SectionId,@SectionCode,@SectionName,@DeptId)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasSectionName(Section aSection)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SectionName", aSection.SectionName));
            string query = "select * from tblSection where SectionName = @SectionName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        public void LoadDepartmentName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public DataTable LoadSectionView()
        {
            string query = @"SELECT *,tblDepartment.DeptName FROM tblSection " +
                            " LEFT JOIN tblDepartment ON tblSection.DeptId = tblDepartment.DeptId order by tblSection.SectionId desc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Section SectionEditLoad(string sectionId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SectionId", sectionId));
            string query = "select * from tblSection where SectionId = @SectionId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            Department aDepartment = new Department();
            Section aSection = new Section();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aSection.DepartmentId = Int32.Parse(dataReader["DeptId"].ToString());
                    aSection.SectionId = Int32.Parse(dataReader["SectionId"].ToString());
                    aSection.SectionCode = dataReader["SectionCode"].ToString();
                    aSection.SectionName = dataReader["SectionName"].ToString();
                }
            }
            return aSection;
        }
        public bool UpdateSectionInfo(Section aSection)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aSection.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@SectionName", aSection.SectionName));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aSection.DepartmentId));

            string query = @"UPDATE tblSection SET SectionName=@SectionName,DeptId=@DeptId WHERE SectionId=@SectionId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
