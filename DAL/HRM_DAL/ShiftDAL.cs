﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ShiftDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveShift(Shift aShift)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aShift.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftName", aShift.ShiftName));
            aSqlParameterlist.Add(new SqlParameter("@ShiftInTime", aShift.ShiftInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftOutTime", aShift.ShiftOutTime));
            aSqlParameterlist.Add(new SqlParameter("@ConsideredInMin", aShift.ConsideredInMin));
            aSqlParameterlist.Add(new SqlParameter("@BreakStart", aShift.BreakStart));
            aSqlParameterlist.Add(new SqlParameter("@BreakEnd", aShift.BreakEnd));
            aSqlParameterlist.Add(new SqlParameter("@DayOverflow", "0"));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", "1"));

            string insertQuery = @"insert into tblShift (ShiftId,ShiftName,ShiftInTime,ConsideredInMin,ShiftOutTime,BreakStart,BreakEnd,DayOverflow,IsActive) 
            values (@ShiftId,@ShiftName,@ShiftInTime,@ConsideredInMin,@ShiftOutTime,@BreakStart,@BreakEnd,@DayOverflow,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasShiftName(Shift aShift)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftName", aShift.ShiftName));
            string query = "select * from tblShift where ShiftName = @ShiftName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                if (dataReader.Read())
                {
                    while (dataReader.Read())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public DataTable LoadShiftView()
        {
            string query = @"select * from tblShift";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public Shift ShiftEditLoad(string ShiftId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", ShiftId));
            string query = "select * from tblShift where ShiftId = @ShiftId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Shift aShift = new Shift();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aShift.ShiftId = Int32.Parse(dataReader["ShiftId"].ToString());
                    aShift.ShiftName = dataReader["ShiftName"].ToString();
                    aShift.ShiftInTime = Convert.ToDateTime(dataReader["ShiftInTime"].ToString()).TimeOfDay;
                    aShift.ShiftOutTime =Convert.ToDateTime(dataReader["ShiftOutTime"].ToString()).TimeOfDay;
                    aShift.BreakEnd = Convert.ToDateTime(dataReader["BreakEnd"].ToString()).TimeOfDay;
                    aShift.BreakStart = Convert.ToDateTime(dataReader["BreakStart"].ToString()).TimeOfDay;
                    aShift.ConsideredInMin = Convert.ToInt32(dataReader["ConsideredInMin"].ToString());
                }
            }
            return aShift;
        }

        public bool UpdateShift(Shift aShift)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aShift.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftName", aShift.ShiftName));
            aSqlParameterlist.Add(new SqlParameter("@ShiftInTime", aShift.ShiftInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftOutTime", aShift.ShiftOutTime));
            aSqlParameterlist.Add(new SqlParameter("@ConsideredInMin", aShift.ConsideredInMin));
            aSqlParameterlist.Add(new SqlParameter("@BreakStart", aShift.BreakStart));
            aSqlParameterlist.Add(new SqlParameter("@BreakEnd", aShift.BreakEnd));

            string query = @"UPDATE tblShift SET ShiftName=@ShiftName,ShiftInTime=@ShiftInTime,ShiftOutTime=@ShiftOutTime,ConsideredInMin=@ConsideredInMin,BreakStart=@BreakStart,BreakEnd=@BreakEnd WHERE ShiftId=@ShiftId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
