﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class ShiftWiseGroupDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveDataForShiftWiseGroup(ShiftWiseGroup aShiftWiseGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GSAId", aShiftWiseGroup.GSAId));
            aSqlParameterlist.Add(new SqlParameter("@GroupId", aShiftWiseGroup.GroupId));
            aSqlParameterlist.Add(new SqlParameter("@FromDate", aShiftWiseGroup.FromDate));
            aSqlParameterlist.Add(new SqlParameter("@ToDate", aShiftWiseGroup.ToDate));
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aShiftWiseGroup.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftInTime", aShiftWiseGroup.ShiftInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftOutTime", aShiftWiseGroup.ShiftOutTime));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aShiftWiseGroup.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aShiftWiseGroup.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@Status", aShiftWiseGroup.Status));

            string insertQuery = @"insert into dbo.tblShiftWiseGroup (GSAId,GroupId,FromDate,ToDate,ShiftId,ShiftInTime,ShiftOutTime,EntryUser,EntryDate,ActionStatus) 
            values (@GSAId,@GroupId,@FromDate,@ToDate,@ShiftId,@ShiftInTime,@ShiftOutTime,@EntryUser,@EntryDate,@Status)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasWeeklyHoliday(string dayname, string empid)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpId", empid));
            aSqlParameterlist.Add(new SqlParameter("@DayName", dayname));
            string query = "SELECT * FROM dbo.tblEmpWeeklyHoliday WHERE EmpId=@EmpId AND (FirstHolidayName=@DayName OR SecondHolidayName=@DayName)";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }

        //public bool HasHolidayDate(ShiftWiseGroup aShiftWiseGroup)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@HolidayDate", aShiftWiseGroup.HolidayDate));
        //    string query = "select * from tblWeeklyShiftWiseGroupment where HolidayDate = @HolidayDate";
        //    IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        public DataTable LoadHolidayView()
        {
            string query = @"SELECT * FROM dbo.tblWeeklyShiftWiseGroupment";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadShiftGroupView()
        {
            string query = @"select *,FromDate,ToDate from tblShiftWiseGroup
            left join tblGroup on tblShiftWiseGroup.GroupId=tblGroup.GroupId
            left join tblShift on tblShift.ShiftId=tblShiftWiseGroup.ShiftId
            left join tblCompanyUnit on tblGroup.UnitId=tblCompanyUnit.UnitId";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public string DayName(string dayName)
        {
            string query = @"SELECT (DATENAME(dw,'" + dayName + "')) AS DayName";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB").Rows[0][0].ToString();
        }
        //public ShiftWiseGroup ShiftWiseGroupEditLoad(string WHRId)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@WHRId", WHRId));
        //    string query = "select * from tblWeeklyShiftWiseGroupment where WHRId = @WHRId";
        //    IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
        //    ShiftWiseGroup aShiftWiseGroup = new ShiftWiseGroup();
        //    if (dataReader != null)
        //    {
        //        while (dataReader.Read())
        //        {
        //            aShiftWiseGroup.EmpInfoId = Int32.Parse(dataReader["EmpInfoId"].ToString());
        //            aShiftWiseGroup.WHRId = Int32.Parse(dataReader["WHRId"].ToString());
        //            aShiftWiseGroup.EmpMasterCode = dataReader["EmpMasterCode"].ToString();
        //            aShiftWiseGroup.EmpName = dataReader["EmpName"].ToString();
        //            aShiftWiseGroup.WeekHolidaydate = Convert.ToDateTime(dataReader["WeekHolidaydate"].ToString());
        //            aShiftWiseGroup.WeekHolidayDayName = dataReader["WeekHolidayDayName"].ToString();
        //            aShiftWiseGroup.AlternativeDate = Convert.ToDateTime(dataReader["AlternativeDate"].ToString());
        //            aShiftWiseGroup.AlternativeDayName = dataReader["AlternativeDayName"].ToString();


        //        }

        //    }
        //    return aShiftWiseGroup;
        //}
        //public bool UpdateShiftWiseGroup(ShiftWiseGroup aShiftWiseGroup)
        //{
        //    List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
        //    aSqlParameterlist.Add(new SqlParameter("@WHRId", aShiftWiseGroup.WHRId));
        //    aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aShiftWiseGroup.EmpInfoId));
        //    aSqlParameterlist.Add(new SqlParameter("@EmpMasterCode", aShiftWiseGroup.EmpMasterCode));
        //    aSqlParameterlist.Add(new SqlParameter("@EmpName", aShiftWiseGroup.EmpName));
        //    aSqlParameterlist.Add(new SqlParameter("@WeekHolidaydate", aShiftWiseGroup.WeekHolidaydate));
        //    aSqlParameterlist.Add(new SqlParameter("@WeekHolidayDayName", aShiftWiseGroup.WeekHolidayDayName));
        //    aSqlParameterlist.Add(new SqlParameter("@AlternativeDate", aShiftWiseGroup.AlternativeDate));
        //    aSqlParameterlist.Add(new SqlParameter("@AlternativeDayName", aShiftWiseGroup.AlternativeDayName));


        //    string query = @"UPDATE tblWeeklyShiftWiseGroupment SET EmpInfoId=@EmpInfoId,EmpMasterCode=@EmpMasterCode,EmpName=@EmpName,WeekHolidaydate=@WeekHolidaydate,WeekHolidayDayName=@WeekHolidayDayName,AlternativeDate=@AlternativeDate,AlternativeDayName=@AlternativeDayName WHERE WHRId=@WHRId";
        //    return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        //}
        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query =
                @"SELECT * FROM tblEmpGeneralInfo WHERE EmpMasterCode='" + EmpMasterCode + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadShift(string shiftId)
        {
            string query =
                @"SELECT * FROM dbo.tblShift WHERE ShiftId='"+shiftId+"'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDate(string groupId,string fromdt,string todt)
        {
            string query =
                @"SELECT * FROM tblShiftWiseGroup WHERE GroupId ='"+groupId+"' AND ActionStatus IN ('Accepted','Posted') and (('"+fromdt+"' BETWEEN FromDate AND ToDate) OR ('"+todt+"' BETWEEN FromDate AND ToDate)) ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadGroupName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblGroup";
            aInternalDal.LoadDropDownValue(ddl, "GroupName", "GroupId", queryStr, "HRDB");
        }
        public void LoadGroupName(DropDownList ddl,string unitId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblGroup where UnitId='"+unitId+"'";
            aInternalDal.LoadDropDownValue(ddl, "GroupName", "GroupId", queryStr, "HRDB");
        }
        public void LoadShift(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblShift";
            aInternalDal.LoadDropDownValue(ddl, "ShiftName", "ShiftId", queryStr, "HRDB");
        }
        public bool ApprovalUpdateDAL(ShiftWiseGroup aShiftWiseGroup)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GSAId", aShiftWiseGroup.GSAId));
            aSqlParameterlist.Add(new SqlParameter("@Status", aShiftWiseGroup.Status));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aShiftWiseGroup.ApproveUser));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aShiftWiseGroup.ApproveDate));

            string query = @"UPDATE tblShiftWiseGroup SET ActionStatus=@Status,ApproveUser=@ApprovedBy,ApproveDate=@ApprovedDate WHERE GSAId=@GSAId ";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteShiftGroupDAL(string id)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@GSAId",id));

            string query = @"delete from tblShiftWiseGroup where GSAId=@GSAId ";
            return aCommonInternalDal.DeleteDataByDeleteCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadShiftWiseGroupViewApproval(string actionstatus)
        {
            string query = @"SELECT * FROM dbo.tblShiftWiseGroup
                            LEFT JOIN dbo.tblShift ON tblShiftWiseGroup.ShiftId=dbo.tblShift.ShiftId where tblShiftWiseGroup.ActionStatus='"+actionstatus+"'  order by dbo.tblShiftWiseGroup.GSAId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
