﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class SuspendDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveSuspend(Suspend aSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SuspendId", aSuspend.SuspendId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aSuspend.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aSuspend.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aSuspend.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aSuspend.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aSuspend.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aSuspend.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aSuspend.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aSuspend.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aSuspend.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aSuspend.EffectiveDate));
           
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aSuspend.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aSuspend.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aSuspend.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aSuspend.IsActive));
            
            string insertQuery = @"insert into tblSuspend (SuspendId,EmpInfoId,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EffectiveDate,ActionStatus,EntryBy,EntryDate,IsActive) 
            values (@SuspendId,@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EffectiveDate,@ActionStatus,@EntryBy,@EntryDate,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");

        }
        public bool HasSuspend(Suspend aSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aSuspend.EmpInfoId));
            string query = "select * from tblSuspend where EmpInfoId=@EmpInfoId and IsActive=1 and ActionStatus in ('Posted','Verified')";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadSuspendView()
        {
            string query =
                @"SELECT * FROM tblSuspend " +
                " LEFT JOIN tblEmpGeneralInfo ON tblSuspend.EmpInfoId = tblEmpGeneralInfo.EmpInfoId  " +
                " LEFT JOIN tblCompanyInfo ON tblSuspend.CompanyInfoId = tblCompanyInfo.CompanyInfoId  " +
                " LEFT JOIN tblCompanyUnit ON tblSuspend.UnitId = tblCompanyUnit.UnitId  " +
                " LEFT JOIN tblDivision ON tblSuspend.DivisionId = tblDivision.DivisionId  " +
                " LEFT JOIN tblSection ON tblSuspend.SectionId = tblSection.SectionId  " +
                " LEFT JOIN tblEmployeeGrade ON tblSuspend.EmpGradeId = tblEmployeeGrade.GradeId  " +
                " LEFT JOIN tblEmployeeType ON tblSuspend.EmpTypeId = tblEmployeeType.EmpTypeId  " +
                " LEFT JOIN tblDesignation ON tblSuspend.DesigId = tblDesignation.DesigId  " +
                " LEFT JOIN tblDepartment ON tblSuspend.DeptId = tblDepartment.DeptId  where tblSuspend.ActionStatus in ('Posted','Cancel') and tblSuspend.IsActive=1 and tblSuspend.EntryBy='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblSuspend.SuspendId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }
        
        public void LoadDepartmentName(DropDownList ddl,string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='"+divisionId+"'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl,string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl,string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='"+deptId+"'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1 and tblEmpGeneralInfo.EmployeeStatus='Active'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Suspend EmpSuspendEditLoad(string SuspendId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SuspendId", SuspendId));
            string query = "select * from tblSuspend where SuspendId=@SuspendId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Suspend aSuspend = new Suspend();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aSuspend.SuspendId = Int32.Parse(dataReader["SuspendId"].ToString());
                    aSuspend.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aSuspend.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aSuspend.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aSuspend.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aSuspend.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aSuspend.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aSuspend.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aSuspend.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aSuspend.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aSuspend.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    aSuspend.ActionStatus = dataReader["ActionStatus"].ToString();
                    aSuspend.EntryBy = dataReader["EntryBy"].ToString();
                    aSuspend.EntryDate = Convert.ToDateTime(dataReader["EntryDate"].ToString());
                    
                }
            }
            return aSuspend;
        }

        public bool UpdateSuspend(Suspend aSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SuspendId", aSuspend.SuspendId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aSuspend.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aSuspend.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aSuspend.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aSuspend.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aSuspend.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aSuspend.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aSuspend.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aSuspend.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aSuspend.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aSuspend.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aSuspend.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@EntryBy", aSuspend.EntryBy));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aSuspend.EntryDate));

            string query = @"UPDATE tblSuspend SET SuspendId=@SuspendId,EmpInfoId=@EmpInfoId,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId,DesigId=@DesigId," +
                            "EffectiveDate=@EffectiveDate,ActionStatus=@ActionStatus,EntryBy=@EntryBy,EntryDate=@EntryDate WHERE SuspendId=@SuspendId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(Suspend aSuspend)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@SuspendId", aSuspend.SuspendId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aSuspend.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aSuspend.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aSuspend.ApprovedDate));

            string query = @"UPDATE tblSuspend SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE SuspendId=@SuspendId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));
            aSqlParameterlist.Add(new SqlParameter("@InactiveReason", aEmpGeneralInfo.InactiveReason));
            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus,InactiveReason=@InactiveReason WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadSuspendViewForApproval(string actionstatus)
        {
            string query = @"SELECT *,CONVERT(NVARCHAR(11),tblSuspend.EffectiveDate,106) AS SUSEffectiveDate,CONVERT(NVARCHAR(11),tblSuspend.EntryDate,106) AS SUSEntryDate From dbo.tblSuspend
                inner JOIN tblEmpGeneralInfo ON tblSuspend.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where tblSuspend.ActionStatus='" + actionstatus + "' AND tblSuspend.IsActive=1  order by tblSuspend.SuspendId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool DeleteData(string SuspendId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblSuspend", "SuspendId", SuspendId);
            
        }
    }
}

