﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Library.DAL.InternalCls;

namespace Library.DAL.HRM_DAL
{
    public class SuspendJobLeftReportDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public DataTable RptHeader()
        {
            string query = @"SELECT  RptAddress ,RptEmail ,RptFax ,RptHeader ,dbo.tblRptImage.RptImage ,RptMessage ,RptTel, 'Copyright Creatrix-'+CONVERT(NVARCHAR(MAX),DATEPART(YEAR,GETDATE()))+', All Rights are Reserved'  AS CopyRight FROM dbo.tblReportHeading
                            LEFT JOIN dbo.tblRptImage ON dbo.tblReportHeading.RptId = dbo.tblRptImage.RptId ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable SuspendRpt(string parameter)
        {
            string query = @"SELECT  EmpMasterCode,EmpName,EffectiveDate,ActionStatus,EntryBy,S.EntryDate, ApprovedBy , ApprovedDate ,CompanyName ,  DeptName ,DesigName ,DivName , EmpType , GradeName ,RelesedOn ,SectionName ,ActionStatus ,Status ,UnitName FROM dbo.tblSuspend S
                            LEFT JOIN dbo.tblCompanyInfo C ON S.CompanyInfoId = C.CompanyInfoId
                            LEFT JOIN dbo.tblEmpGeneralInfo E ON S.EmpInfoId=E.EmpInfoId
                            LEFT JOIN dbo.tblCompanyUnit U ON S.UnitId=U.UnitId
                            LEFT JOIN dbo.tblDivision D ON S.DivisionId=D.DivisionId
                            LEFT JOIN dbo.tblDepartment DP ON S.DeptId=DP.DeptId
                            LEFT JOIN dbo.tblSection SC ON S.SectionId=SC.SectionId
                            LEFT JOIN dbo.tblDesignation DG ON S.DesigId=DG.DesigId
                            LEFT JOIN dbo.tblEmployeeType ET ON S.EmpTypeId=ET.EmpTypeId
                            LEFT JOIN dbo.tblEmployeeGrade EG ON S.EmpGradeId=EG.GradeId " + parameter + "";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable JobLeftRpt(string parameter)
        {
            string query = @"SELECT  EmpMasterCode,EmpName,EffectiveDate,ActionStatus,EntryBy,J.EntryDate, ApprovedBy , ApprovedDate ,CompanyName ,  DeptName ,DesigName ,DivName , EmpType , GradeName ,RelesedOn ,SectionName ,ActionStatus ,Status ,UnitName FROM dbo.tblJobleft J
                            LEFT JOIN dbo.tblCompanyInfo C ON J.CompanyInfoId = C.CompanyInfoId
                            LEFT JOIN dbo.tblEmpGeneralInfo E ON J.EmpInfoId=E.EmpInfoId
                            LEFT JOIN dbo.tblCompanyUnit U ON J.UnitId=U.UnitId
                            LEFT JOIN dbo.tblDivision D ON J.DivisionId=D.DivisionId
                            LEFT JOIN dbo.tblDepartment DP ON J.DeptId=DP.DeptId
                            LEFT JOIN dbo.tblSection SC ON J.SectionId=SC.SectionId
                            LEFT JOIN dbo.tblDesignation DG ON J.DesigId=DG.DesigId
                            LEFT JOIN dbo.tblEmployeeType ET ON J.EmpTypeId=ET.EmpTypeId
                            LEFT JOIN dbo.tblEmployeeGrade EG ON J.GradeId=EG.GradeId " + parameter + "";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
    }
}
