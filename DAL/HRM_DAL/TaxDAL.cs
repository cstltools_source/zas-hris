﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class TaxDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveTax(Tax aTax)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TaxId", aTax.TaxId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aTax.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@TaxAmount", aTax.TaxAmount));
            aSqlParameterlist.Add(new SqlParameter("@TaxLValue", aTax.TaxLValue));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aTax.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aTax.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aTax.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aTax.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aTax.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aTax.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aTax.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aTax.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aTax.GradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aTax.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aTax.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aTax.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aTax.IsActive));

            string insertQuery = @"insert into tblTax (TaxId,EmpInfoId,TaxAmount,TaxLValue,EffectiveDate,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,DesigId,EmpTypeId,EmpGradeId,EntryUser,EntryDate,ActionStatus,IsActive) 
            values (@TaxId,@EmpInfoId,@TaxAmount,@TaxLValue,@EffectiveDate,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@DesigId,@EmpTypeId,@GradeId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasTax(Tax aTax)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aTax.EmpInfoId));
            string query = "select * from tblTax where EmpInfoId=@EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        public DataTable LoadTaxView()
        {
            string query = @"SELECT  TaxId,EmpMasterCode ,EmpName , EffectiveDate , TaxAmount,DesigName,DeptName FROM tblTax 
                 LEFT JOIN tblEmpGeneralInfo ON tblTax.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblTax.CompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblTax.UnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblTax.DivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblTax.SectionId = tblSection.SectionId  
                 LEFT JOIN tblEmployeeGrade ON tblTax.EmpGradeId = tblEmployeeGrade.GradeId  
                 LEFT JOIN tblEmployeeType ON tblTax.EmpTypeId = tblEmployeeType.EmpTypeId  
                 LEFT JOIN tblDesignation ON tblTax.DesigId = tblDesignation.DesigId  
                 LEFT JOIN tblDepartment ON tblTax.DeptId = tblDepartment.DeptId  where tblTax.ActionStatus in ('Posted','Cancel') and tblTax.IsActive=1 and tblTax.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblTax.TaxId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public void LoadDesignationName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "'";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "'";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Tax TaxEditLoad(string TaxId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TaxId", TaxId));
            string query = "select * from tblTax where TaxId = @TaxId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Tax aTax = new Tax();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aTax.TaxId = Int32.Parse(dataReader["TaxId"].ToString());
                    aTax.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aTax.TaxAmount = Convert.ToDecimal(dataReader["TaxAmount"].ToString());
                    aTax.TaxLValue = Convert.ToDecimal(dataReader["TaxLValue"].ToString());
                    aTax.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aTax.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aTax.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aTax.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aTax.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aTax.DesigId = Convert.ToInt32(dataReader["DesigId"].ToString());
                    aTax.GradeId = Convert.ToInt32(dataReader["EmpGradeId"].ToString());
                    aTax.EmpTypeId = Convert.ToInt32(dataReader["EmpTypeId"].ToString());
                    aTax.ActionStatus = dataReader["ActionStatus"].ToString();
                    aTax.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());

                }
            }
            return aTax;
        }
        
        public bool UpdateTax(Tax aTax)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TaxId", aTax.TaxId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aTax.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@TaxAmount", aTax.TaxAmount));
            aSqlParameterlist.Add(new SqlParameter("@TaxLValue", aTax.TaxLValue));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aTax.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aTax.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aTax.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aTax.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aTax.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aTax.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@DesigId", aTax.DesigId));
            aSqlParameterlist.Add(new SqlParameter("@EmpTypeId", aTax.EmpTypeId));
            aSqlParameterlist.Add(new SqlParameter("@GradeId", aTax.GradeId));
           
            string query = @"UPDATE tblTax SET EmpInfoId=@EmpInfoId,TaxAmount=@TaxAmount,TaxLValue=@TaxLValue,EffectiveDate=@EffectiveDate,CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DeptId=@DeptId,SectionId=@SectionId,DesigId=@DesigId,EmpTypeId=@EmpTypeId,EmpGradeId=@GradeId WHERE TaxId=@TaxId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(Tax aTax)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TaxId", aTax.TaxId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aTax.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aTax.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aTax.ApprovedDate));

            string query = @"UPDATE tblTax SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE TaxId=@TaxId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadTaxViewForApproval(string actionstatus)
        {
            string query = @"SELECT * From tblTax
                LEFT JOIN tblEmpGeneralInfo ON tblTax.EmpInfoId = tblEmpGeneralInfo.EmpInfoId where tblTax.ActionStatus='" + actionstatus + "' AND tblTax.IsActive=1  order by tblTax.TaxId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EmployeeStatus", aEmpGeneralInfo.EmployeeStatus));

            string query = @"UPDATE tblEmpGeneralInfo SET EmployeeStatus=@EmployeeStatus WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string TaxId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblTax", "TaxId", TaxId);
            
        }
    }
}
