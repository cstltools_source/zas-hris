﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class TransferDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }
        public bool SaveTransfer(Transfer aTransfer)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TransferId", aTransfer.TransferId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aTransfer.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aTransfer.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aTransfer.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aTransfer.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DeptId", aTransfer.DeptId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aTransfer.SectionId));
            aSqlParameterlist.Add(new SqlParameter("@NewUnitId", aTransfer.NewUnitId));
            aSqlParameterlist.Add(new SqlParameter("@NewCompanyInfoId", aTransfer.NewCompId));
            aSqlParameterlist.Add(new SqlParameter("@NewDivisionId", aTransfer.NewDivisionId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aTransfer.EffectiveDate));
            aSqlParameterlist.Add(new SqlParameter("@NewDeptId", aTransfer.NewDeptId));
            aSqlParameterlist.Add(new SqlParameter("@NewSectionId", aTransfer.NewSectionId));
            aSqlParameterlist.Add(new SqlParameter("@NewDesigId", aTransfer.NewDesigId));
            aSqlParameterlist.Add(new SqlParameter("@NewGradeId", aTransfer.NewGradeId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aTransfer.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aTransfer.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aTransfer.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aTransfer.IsActive));

            string insertQuery = @"insert into tblTransfer (TransferId,EmpInfoId,CompanyInfoId,UnitId,DivisionId,DeptId,SectionId,NewCompanyInfoId,NewUnitId,NewDivisionId,EffectiveDate,NewDeptId,NewSectionId,EntryUser,EntryDate,ActionStatus,IsActive) 
                                                   values (@TransferId,@EmpInfoId,@CompanyInfoId,@UnitId,@DivisionId,@DeptId,@SectionId,@NewCompanyInfoId,@NewUnitId,@NewDivisionId,@EffectiveDate,@NewDeptId,@NewSectionId,@EntryUser,@EntryDate,@ActionStatus,@IsActive)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool HasTransfer(Transfer aTransfer)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aTransfer.EmpInfoId));
            string query = "select * from tblTransfer where EmpInfoId=@EmpInfoId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
        /*Load Transfer View page*/
        public DataTable LoadTransferView()
        {
            string query = @"SELECT TransferId, EmpMasterCode,EmpName ,tblCompanyInfo.CompanyName as NewCompanyName,  tblCompanyUnit.UnitName as NewUnitName,tblDepartment.DeptName as NewDeptName,tblSection.SectionName as NewSecName,tblDivision.DivName as NewDivName,EffectiveDate,tblTransfer.ActionStatus FROM tblTransfer 
                 LEFT JOIN tblEmpGeneralInfo ON tblTransfer.EmpInfoId = tblEmpGeneralInfo.EmpInfoId
                 LEFT JOIN tblCompanyInfo ON tblTransfer.NewCompanyInfoId = tblCompanyInfo.CompanyInfoId 
                 LEFT JOIN tblCompanyUnit ON tblTransfer.NewUnitId = tblCompanyUnit.UnitId 
                 LEFT JOIN tblDivision ON tblTransfer.NewDivisionId = tblDivision.DivisionId  
                 LEFT JOIN tblSection ON tblTransfer.NewSectionId = tblSection.SectionId  
                 LEFT JOIN tblDepartment ON tblTransfer.NewDeptId = tblDepartment.DeptId
                 where tblTransfer.ActionStatus in ('Posted','Cancel') and tblTransfer.IsActive=1 and tblTransfer.EntryUser='" + HttpContext.Current.Session["LoginName"].ToString() + "'  order by tblTransfer.TransferId desc";

            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        //
        public void LoadDesignationName(DropDownList ddl, string empgradeId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDesignation ORDER BY DesigName";
            aInternalDal.LoadDropDownValue(ddl, "DesigName", "DesigId", queryStr, "HRDB");
        }

        public void LoadDepartmentName(DropDownList ddl, string divisionId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDepartment where DivisionId='" + divisionId + "' ORDER BY DeptName";
            aInternalDal.LoadDropDownValue(ddl, "DeptName", "DeptId", queryStr, "HRDB");
        }
        public void LoadEmployeeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmpGeneralInfo ORDER BY EmployeeName";
            aInternalDal.LoadDropDownValue(ddl, "EmployeeName", "EmpInfoId", queryStr, "HRDB");
        }
        public void LoadCompanyName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyInfo ";
            aInternalDal.LoadDropDownValue(ddl, "CompanyName", "CompanyInfoId", queryStr, "HRDB");
        }
        public void LoadUnitName(DropDownList ddl, string companyId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblCompanyUnit where CompanyInfoId='" + companyId + "'";
            aInternalDal.LoadDropDownValue(ddl, "UnitName", "UnitId", queryStr, "HRDB");
        }
        public void LoadGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeGrade ORDER BY GradeName ";
            aInternalDal.LoadDropDownValue(ddl, "GradeName", "GradeId", queryStr, "HRDB");
        }
        public void LoadDivisionName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblDivision ORDER BY DivName";
            aInternalDal.LoadDropDownValue(ddl, "DivName", "DivisionId", queryStr, "HRDB");
        }
        public void LoadSectionName(DropDownList ddl, string deptId)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSection where DeptId='" + deptId + "' ORDER BY SectionName";
            aInternalDal.LoadDropDownValue(ddl, "SectionName", "SectionId", queryStr, "HRDB");
        }
        public void LoadSalGradeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblSalaryGradeOrScale ORDER BY SalGradeName ";
            aInternalDal.LoadDropDownValue(ddl, "SalGradeName", "SalGradeId", queryStr, "HRDB");
        }
        public void LoadEmpTypeName(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "select * from tblEmployeeType";
            aInternalDal.LoadDropDownValue(ddl, "EmpType", "EmpTypeId", queryStr, "HRDB");
        }

        public DataTable LoadCompanyInfo(string CompanyInfoId)
        {
            string query = @"SELECT * FROM tblCompanyInfo WHERE CompanyInfoId='" + CompanyInfoId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadUnit(string UnitId)
        {
            string query = @"SELECT * FROM dbo.tblCompanyUnit WHERE UnitId='" + UnitId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable Loadivision(string DivisionId)
        {
            string query = @"SELECT * FROM dbo.tblDivision WHERE DivisionId='" + DivisionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDepartment(string DeptId)
        {
            string query = @"SELECT * FROM dbo.tblDepartment WHERE DeptId='" + DeptId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadDesignation(string DesigId)
        {
            string query = @"SELECT * FROM dbo.tblDesignation WHERE DesigId='" + DesigId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadSection(string SectionId)
        {
            string query = @"SELECT * FROM dbo.tblSection WHERE SectionId='" + SectionId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadGrade(string GradeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeGrade WHERE GradeId='" + GradeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpType(string EmpTypeId)
        {
            string query = @"SELECT * FROM dbo.tblEmployeeType WHERE EmpTypeId='" + EmpTypeId + "'";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public DataTable LoadEmpInfo(string EmpMasterCode)
        {
            string query = @"SELECT EmpInfoId,EmpName,CompanyName, DeptName ,dbo.tblDepartment.DeptId ,dbo.tblEmpGeneralInfo.CompanyInfoId , DesigName , tblEmpGeneralInfo.DesigId , dbo.tblDivision.DivisionId ,DivName , EmpType , dbo.tblEmployeeType.EmpTypeId ,GradeId ,GradeName , dbo.tblSection.SectionId ,SectionName ,dbo.tblCompanyUnit.UnitId ,UnitName FROM dbo.tblEmpGeneralInfo 
								LEFT JOIN dbo.tblEmployeeGrade ON dbo.tblEmpGeneralInfo.EmpGradeId = dbo.tblEmployeeGrade.GradeId
                                LEFT JOIN dbo.tblCompanyInfo ON dbo.tblEmpGeneralInfo.CompanyInfoId=dbo.tblCompanyInfo.CompanyInfoId
                                LEFT JOIN dbo.tblCompanyUnit ON dbo.tblEmpGeneralInfo.UnitId=dbo.tblCompanyUnit.UnitId
                                LEFT JOIN dbo.tblDivision ON dbo.tblEmpGeneralInfo.DivisionId = dbo.tblDivision.DivisionId
                                LEFT JOIN dbo.tblDepartment ON dbo.tblEmpGeneralInfo.DepId=dbo.tblDepartment.DeptId
                                LEFT JOIN dbo.tblSection ON dbo.tblEmpGeneralInfo.SectionId=dbo.tblSection.SectionId
                                LEFT JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId=dbo.tblDesignation.DesigId
                                LEFT JOIN dbo.tblEmployeeType ON dbo.tblEmpGeneralInfo.EmpTypeId=dbo.tblEmployeeType.EmpTypeId 

where EmpMasterCode='" + EmpMasterCode + "' AND tblEmpGeneralInfo.IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public DataTable LoadEmpInfoCode(string EmpInfoId)
        {
            string query = @"SELECT * FROM tblEmpGeneralInfo WHERE EmpInfoId='" + EmpInfoId + "' AND IsActive=1";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }

        public Transfer TransferEditLoad(string TransferId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TransferId", TransferId));
            string query = "select * from tblTransfer where TransferId = @TransferId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            Transfer aTransfer = new Transfer();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aTransfer.TransferId = Int32.Parse(dataReader["TransferId"].ToString());
                    aTransfer.EmpInfoId = Convert.ToInt32(dataReader["EmpInfoId"].ToString());
                    aTransfer.CompanyInfoId = Convert.ToInt32(dataReader["CompanyInfoId"].ToString());
                    aTransfer.UnitId = Convert.ToInt32(dataReader["UnitId"].ToString());
                    aTransfer.DivisionId = Convert.ToInt32(dataReader["DivisionId"].ToString());
                    aTransfer.DeptId = Convert.ToInt32(dataReader["DeptId"].ToString());
                    aTransfer.SectionId = Convert.ToInt32(dataReader["SectionId"].ToString());
                    aTransfer.NewCompId = Convert.ToInt32(dataReader["NewCompanyInfoId"].ToString());
                    aTransfer.NewUnitId = Convert.ToInt32(dataReader["NewUnitId"].ToString());
                    aTransfer.NewDivisionId = Convert.ToInt32(dataReader["NewDivisionId"].ToString());
                    aTransfer.NewDeptId = Convert.ToInt32(dataReader["NewDeptId"].ToString());
                    aTransfer.NewSectionId = Convert.ToInt32(dataReader["NewSectionId"].ToString());
                   // aTransfer.NewGradeId = Convert.ToInt32(dataReader["NewGradeId"].ToString());
                    aTransfer.ActionStatus = dataReader["ActionStatus"].ToString();
                    aTransfer.EffectiveDate = Convert.ToDateTime(dataReader["EffectiveDate"].ToString());
                    
                }
            }
            return aTransfer;
        }

        public bool UpdateTransfer(Transfer aTransfer)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TransferId", aTransfer.TransferId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aTransfer.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@NewCompanyInfoId", aTransfer.NewCompId));
            aSqlParameterlist.Add(new SqlParameter("@NewUnitId", aTransfer.NewUnitId));
            aSqlParameterlist.Add(new SqlParameter("@NewDivisionId", aTransfer.NewDivisionId));
            aSqlParameterlist.Add(new SqlParameter("@NewDeptId", aTransfer.NewDeptId));
            aSqlParameterlist.Add(new SqlParameter("@NewSectionId", aTransfer.NewSectionId));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aTransfer.EffectiveDate));

            string query = @"UPDATE tblTransfer SET TransferId=@TransferId,EmpInfoId=@EmpInfoId,NewCompanyInfoId=@NewCompanyInfoId,NewUnitId=@NewUnitId,NewDivisionId=@NewDivisionId,NewDeptId=@NewDeptId,NewSectionId=@NewSectionId,EffectiveDate=@EffectiveDate WHERE TransferId=@TransferId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool ApprovalUpdateDAL(Transfer aTransfer)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@TransferId", aTransfer.TransferId));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aTransfer.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedBy", aTransfer.ApprovedBy));
            aSqlParameterlist.Add(new SqlParameter("@ApprovedDate", aTransfer.ApprovedDate));

            string query = @"UPDATE tblTransfer SET ActionStatus=@ActionStatus,ApprovedBy=@ApprovedBy,ApprovedDate=@ApprovedDate WHERE TransferId=@TransferId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public DataTable LoadTransferViewForApproval(string actionstatus)
        {
            string query = @"SELECT * ,tblCompanyInfo.CompanyName,tblCompanyUnit.UnitName,tblDepartment.DeptName,tblDivision.DivName,tblSection.SectionName From dbo.tblTransfer
                LEFT JOIN tblEmpGeneralInfo ON tblTransfer.EmpInfoId = tblEmpGeneralInfo.EmpInfoId 
                LEFT JOIN tblCompanyInfo ON tblTransfer.NewCompanyInfoId = tblCompanyInfo.CompanyInfoId 
                LEFT JOIN tblCompanyUnit ON tblTransfer.NewUnitId = tblCompanyUnit.UnitId
                LEFT JOIN tblDivision ON tblTransfer.NewDivisionId = tblDivision.DivisionId
                LEFT JOIN tblDepartment ON tblTransfer.NewDeptId = tblDepartment.DeptId
                LEFT JOIN tblSection ON tblTransfer.NewSectionId = tblSection.SectionId 
where tblTransfer.ActionStatus='" + actionstatus + "' AND tblTransfer.IsActive=1  order by tblTransfer.TransferId asc ";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public bool PlaceEmpStatus(EmpGeneralInfo aEmpGeneralInfo)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aEmpGeneralInfo.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@CompanyInfoId", aEmpGeneralInfo.CompanyInfoId));
            aSqlParameterlist.Add(new SqlParameter("@UnitId", aEmpGeneralInfo.UnitId));
            aSqlParameterlist.Add(new SqlParameter("@DivisionId", aEmpGeneralInfo.DivisionId));
            aSqlParameterlist.Add(new SqlParameter("@DepId", aEmpGeneralInfo.DepId));
            aSqlParameterlist.Add(new SqlParameter("@SectionId", aEmpGeneralInfo.SectionId));

            string query = @"UPDATE tblEmpGeneralInfo SET CompanyInfoId=@CompanyInfoId,UnitId=@UnitId,DivisionId=@DivisionId,DepId=@DepId,SectionId=@SectionId WHERE EmpInfoId=@EmpInfoId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
        public bool DeleteData(string TransferId)
        {
            return aCommonInternalDal.DeleteStatusUpdate("tblTransfer", "TransferId", TransferId);
        }
    }
}
