﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class UnRestShiftDAL
    {
        private ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        public bool SaveShift(UnRestShift aShift)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@UnRestShiftId", aShift.UnRestShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", aShift.ShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftName", aShift.ShiftName));
            aSqlParameterlist.Add(new SqlParameter("@ShiftInTime", aShift.ShiftInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftOutTime", aShift.ShiftOutTime));
            aSqlParameterlist.Add(new SqlParameter("@ConsideredInMin", aShift.ConsideredInMin));
            aSqlParameterlist.Add(new SqlParameter("@BreakStart", aShift.BreakStart));
            aSqlParameterlist.Add(new SqlParameter("@BreakEnd", aShift.BreakEnd));
            aSqlParameterlist.Add(new SqlParameter("@EffectiveDate", aShift.EffectiveDate));

            string insertQuery = @"insert into dbo.tblUnrestShift (UnRestShiftId,ShiftName,ShiftInTime,ConsideredInMin,ShiftOutTime,BreakStart,BreakEnd,ShiftId,EffectiveDate) 
            values (@UnRestShiftId,@ShiftName,@ShiftInTime,@ConsideredInMin,@ShiftOutTime,@BreakStart,@BreakEnd,@ShiftId,@EffectiveDate)";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }

        public bool HasShiftName(UnRestShift aShift)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftName", aShift.ShiftName));
            string query = "select * from tblUnrestShift where ShiftName = @ShiftName";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");

            if (dataReader != null)
            {
                if (dataReader.Read())
                {
                    while (dataReader.Read())
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public DataTable LoadShiftView()
        {
            string query = @"select * from tblUnrestShift";
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        
        public UnRestShift ShiftEditLoad(string ShiftId)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@ShiftId", ShiftId));
            string query = "select * from tblUnrestShift where UnRestShiftId = @ShiftId";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            UnRestShift aShift = new UnRestShift();
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    aShift.UnRestShiftId = Int32.Parse(dataReader["UnRestShiftId"].ToString());
                    aShift.ShiftName = dataReader["ShiftName"].ToString();
                    aShift.ShiftInTime = Convert.ToDateTime(dataReader["ShiftInTime"].ToString()).TimeOfDay;
                    aShift.ShiftOutTime =Convert.ToDateTime(dataReader["ShiftOutTime"].ToString()).TimeOfDay;
                    aShift.BreakEnd = Convert.ToDateTime(dataReader["ShiftOutTime"].ToString()).TimeOfDay;
                    aShift.BreakStart = Convert.ToDateTime(dataReader["ShiftOutTime"].ToString()).TimeOfDay;
                    aShift.ConsideredInMin = Convert.ToInt32(dataReader["ConsideredInMin"].ToString());
                }
            }
            return aShift;
        }

        public bool UpdateShift(UnRestShift aShift)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@UnRestShiftId", aShift.UnRestShiftId));
            aSqlParameterlist.Add(new SqlParameter("@ShiftName", aShift.ShiftName));
            aSqlParameterlist.Add(new SqlParameter("@ShiftInTime", aShift.ShiftInTime));
            aSqlParameterlist.Add(new SqlParameter("@ShiftOutTime", aShift.ShiftOutTime));
            aSqlParameterlist.Add(new SqlParameter("@ConsideredInMin", aShift.ConsideredInMin));
            aSqlParameterlist.Add(new SqlParameter("@BreakStart", aShift.BreakStart));
            aSqlParameterlist.Add(new SqlParameter("@BreakEnd", aShift.BreakEnd));

            string query = @"UPDATE tblUnrestShift SET ShiftName=@ShiftName,ShiftInTime=@ShiftInTime,ShiftOutTime=@ShiftOutTime,ConsideredInMin=@ConsideredInMin,BreakStart=@BreakStart,BreakEnd=@BreakEnd WHERE UnRestShiftId=@UnRestShiftId";
            return aCommonInternalDal.UpdateDataByUpdateCommand(query, aSqlParameterlist, "HRDB");
        }
    }
}
