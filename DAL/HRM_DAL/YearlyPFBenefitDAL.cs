﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Library.DAL.InternalCls;
using Library.DAO.HRM_Entities;

namespace Library.DAL.HRM_DAL
{
    public class YearlyPFBenefitDAL
    {
        ClsCommonInternalDAL aCommonInternalDal = new ClsCommonInternalDAL();
        ClsApprovalAction approvalAction = new ClsApprovalAction();
        public void LoadApprovalControlDAL(RadioButtonList rdl, string pageName, string userName)
        {
            approvalAction.LoadActionControlByUser(rdl, pageName, userName);
        }
        public string LoadForApprovalConditionDAL(string pageName, string userName)
        {
            return approvalAction.LoadForApprovalByUserCondition(pageName, userName);
        }

        public bool SaveYearlyPFBenefit(YearlyPFBenefit aYearlyPfBenefit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@PFBenefitId", aYearlyPfBenefit.PFBenefitId));
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aYearlyPfBenefit.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aYearlyPfBenefit.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aYearlyPfBenefit.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aYearlyPfBenefit.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aYearlyPfBenefit.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@FianacialYear", aYearlyPfBenefit.FianacialYear));
            aSqlParameterlist.Add(new SqlParameter("@IntPer", aYearlyPfBenefit.IntPer));
            aSqlParameterlist.Add(new SqlParameter("@EmpAmount", aYearlyPfBenefit.EmpAmount));
            aSqlParameterlist.Add(new SqlParameter("@CompAmount", aYearlyPfBenefit.CompAmount));
            aSqlParameterlist.Add(new SqlParameter("@InterAmount", aYearlyPfBenefit.InterAmount));
            aSqlParameterlist.Add(new SqlParameter("@TotalAmount", aYearlyPfBenefit.TotalAmount));
            
            string insertQuery = @"INSERT INTO dbo.tblYearlyPFBenefit
                                    ( PFBenefitId ,
                                      EmpInfoId ,
                                      FianacialYear ,
                                      IntPer ,
                                      EmpAmount ,
                                      CompAmount ,
                                      InterAmount ,
                                      TotalAmount ,
                                      ActionStatus ,
                                      EntryUser ,
                                      EntryDate ,
                                      IsActive
                                    )
                            VALUES  ( @PFBenefitId ,
                                      @EmpInfoId ,
                                      @FianacialYear ,
                                      @IntPer ,
                                      @EmpAmount ,
                                      @CompAmount ,
                                      @InterAmount ,
                                      @TotalAmount ,
                                      @ActionStatus ,
                                      @EntryUser ,
                                      @EntryDate ,
                                      @IsActive
                                    )";
            return aCommonInternalDal.SaveDataByInsertCommand(insertQuery, aSqlParameterlist, "HRDB");
        }
        public bool UpdateYearlyPFBenefit(YearlyPFBenefit aYearlyPfBenefit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aYearlyPfBenefit.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@EntryUser", aYearlyPfBenefit.EntryUser));
            aSqlParameterlist.Add(new SqlParameter("@EntryDate", aYearlyPfBenefit.EntryDate));
            aSqlParameterlist.Add(new SqlParameter("@ActionStatus", aYearlyPfBenefit.ActionStatus));
            aSqlParameterlist.Add(new SqlParameter("@IsActive", aYearlyPfBenefit.IsActive));
            aSqlParameterlist.Add(new SqlParameter("@FianacialYear", aYearlyPfBenefit.FianacialYear));
            aSqlParameterlist.Add(new SqlParameter("@IntPer", aYearlyPfBenefit.IntPer));
            aSqlParameterlist.Add(new SqlParameter("@EmpAmount", aYearlyPfBenefit.EmpAmount));
            aSqlParameterlist.Add(new SqlParameter("@CompAmount", aYearlyPfBenefit.CompAmount));
            aSqlParameterlist.Add(new SqlParameter("@InterAmount", aYearlyPfBenefit.InterAmount));
            aSqlParameterlist.Add(new SqlParameter("@TotalAmount", aYearlyPfBenefit.TotalAmount));

            string insertQuery = @"UPDATE dbo.tblYearlyPFBenefit SET 
                                      EmpInfoId=@EmpInfoId ,
                                      FianacialYear=@FianacialYear ,
                                      IntPer=@IntPer ,
                                      EmpAmount=@EmpAmount ,
                                      CompAmount=@CompAmount ,
                                      InterAmount=@InterAmount ,
                                      TotalAmount=@TotalAmount ,
                                      ActionStatus=@ActionStatus ,
                                      EntryUser=@EntryUser ,
                                      EntryDate=@EntryDate  WHERE EmpInfoId=@EmpInfoId AND FianacialYear=@FianacialYear";
            return aCommonInternalDal.UpdateDataByUpdateCommand(insertQuery, aSqlParameterlist, "HRDB");
        }


        public DataTable LoadPFSetup(string year,string percentage,string mainpercent)
        {
            string query =
                @"SELECT dbo.tblEmpGeneralInfo.EmpMasterCode,tblEmpGeneralInfo.EmpInfoId,EmpName,DesigName,SUM(PrFund) AS EmpAmount,SUM(PrFund) AS ComAmount,((SUM(PrFund)+SUM(PrFund))*" + percentage + ") as InterAmount,'" + mainpercent + "' as InterPer,((((SUM(PrFund)+SUM(PrFund))*" + percentage + "))+(SUM(PrFund)+SUM(PrFund))) as TotalAmount  FROM dbo.tblPFSetup " +
                           " INNER JOIN dbo.tblEmpGeneralInfo ON dbo.tblPFSetup.EmpInfoId = dbo.tblEmpGeneralInfo.EmpInfoId"+
                           " INNER JOIN dbo.tblDesignation ON dbo.tblEmpGeneralInfo.DesigId = dbo.tblDesignation.DesigId"+
                           " INNER JOIN dbo.tblSalaryRecordPerMonth ON dbo.tblPFSetup.EmpInfoId=dbo.tblSalaryRecordPerMonth.EmpInfoId WHERE YEAR(SalaryStartDate)='" + year + "' AND EmployeeStatus='Active' GROUP BY dbo.tblEmpGeneralInfo.EmpMasterCode,EmpName,DesigName,tblEmpGeneralInfo.EmpInfoId";
                            
            return aCommonInternalDal.DataContainerDataTable(query, "HRDB");
        }
        public void LoadFinancialYear(DropDownList ddl)
        {
            ClsCommonInternalDAL aInternalDal = new ClsCommonInternalDAL();
            string queryStr = "SELECT * FROM dbo.tblFinancialYear";
            aInternalDal.LoadDropDownValue(ddl, "FiscalYearId", "FyrCode", queryStr, "HRDB");
        }
        public bool HasYearlyPFBenefit(YearlyPFBenefit aYearlyPfBenefit)
        {
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@EmpInfoId", aYearlyPfBenefit.EmpInfoId));
            aSqlParameterlist.Add(new SqlParameter("@FianacialYear", aYearlyPfBenefit.FianacialYear));
            string query = "SELECT * FROM dbo.tblYearlyPFBenefit WHERE EmpInfoId=@EmpInfoId AND FianacialYear=@FianacialYear";
            IDataReader dataReader = aCommonInternalDal.DataContainerDataReader(query, aSqlParameterlist, "HRDB");
            if (dataReader != null)
            {
                while (dataReader.Read())
                {
                    return true;
                }
            }
            return false;
        }
    }
}
