﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Library.DAL.InternalCls
{
    internal class ClsCommonInternalDAL
    {

        internal void LoadAction(RadioButtonList rdl)
        {
            string query = @"select * from tblAction where IsShow=1 ";
            DataTable dtAction = DataContainerDataTable(query, "HRDB");
            rdl.DataSource = dtAction;
            rdl.DataTextField = "ActionText";
            rdl.DataValueField = "ActionId";
            rdl.DataBind();

        }
        internal void LoadDropDownValue(DropDownList ddl, string displayField, string valueField, string queryString, string dataBaseName)
        {
            try
            {
                DataTable dataDDL = new DataTable();
                Database db;
                DbCommand dbCommand;
                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);

                dataDDL = db.ExecuteDataSet(dbCommand).Tables[0];
                ddl.DataTextField = displayField;
                ddl.DataValueField = valueField;
                ddl.DataSource = dataDDL;
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("Select any one", String.Empty));
                ddl.SelectedIndex = 0;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        internal void LoadDropDownValue(DropDownList ddl, string displayField, string valueField, string queryString, List<SqlParameter> aSqlParameterlist, string dataBaseName)
        {
            try
            {
                DataTable dataDDL = new DataTable();
                Database db;
                DbCommand dbCommand;
                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                dbCommand.Parameters.Clear();
                dbCommand.Parameters.AddRange(aSqlParameterlist.ToArray());

                dataDDL = db.ExecuteDataSet(dbCommand).Tables[0];
                ddl.DataTextField = displayField;
                ddl.DataValueField = valueField;
                ddl.DataSource = dataDDL;
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("Select any one", String.Empty));
                ddl.SelectedIndex = 0;
                dbCommand.Parameters.Clear();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        internal IDataReader DataContainerDataReader(string queryString, List<SqlParameter> aSqlParameterlist, string dataBaseName)
        {
            try
            {
                IDataReader dataReader;
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                dbCommand.Parameters.Clear();
                dbCommand.Parameters.AddRange(aSqlParameterlist.ToArray());
                dataReader = db.ExecuteReader(dbCommand);
                dbCommand.Parameters.Clear();
                return dataReader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal IDataReader DataContainerDataReader(string queryString, string dataBaseName)
        {
            try
            {
                IDataReader dataReader;
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                dataReader = db.ExecuteReader(dbCommand);
                return dataReader;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal DataTable DataContainerDataTable(string queryString, List<SqlParameter> aSqlParameterlist, string dataBaseName)
        {
            try
            {
                DataTable dataContain = new DataTable();

                Database db;
                DbCommand dbCommand;
                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                dbCommand.Parameters.Clear();
                dbCommand.Parameters.AddRange(aSqlParameterlist.ToArray());
                dataContain = db.ExecuteDataSet(dbCommand).Tables[0];
                dbCommand.Parameters.Clear();
                return dataContain;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal DataTable DataContainerDataTable(string queryString, string dataBaseName)
        {
            try
            {
                DataTable dataContain = new DataTable();

                Database db;
                DbCommand dbCommand;
                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);

                dataContain = db.ExecuteDataSet(dbCommand).Tables[0];
                return dataContain;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal bool SaveDataByInsertCommand(string queryString, string dataBaseName)
        {
            try
            {
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
               

                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal int RunStoreProcedure(string sp, List<SqlParameter> aSqlParameterlist, string dataBaseName)
        {
            int pk = 0;
            try
            {
                DataTable dataContain = new DataTable();

                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                using (dbCommand = db.GetStoredProcCommand(sp))
                {
                    dbCommand.CommandTimeout = 120000;
                    dbCommand.Parameters.Clear();
                    dbCommand.Parameters.AddRange(aSqlParameterlist.ToArray());

                    if (db.ExecuteNonQuery(dbCommand) > 0)
                    {
                        pk = 1;
                        return pk;
                    }
                    else
                    {
                        dbCommand.Parameters.Clear();
                        return pk;
                    }
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal bool SaveDataByInsertCommand(string queryString, List<SqlParameter> aSqlParameterlist, string dataBaseName)
        {
            try
            {
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                dbCommand.Parameters.Clear();
                dbCommand.Parameters.AddRange(aSqlParameterlist.ToArray());

                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    dbCommand.Parameters.Clear();
                    return true;
                }
                else
                {
                    dbCommand.Parameters.Clear();
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        internal bool UpdateDataByUpdateCommand(string queryString, string dataBaseName)
        {
            try
            {
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                   
                    return true;
                }
                else
                {
                    
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        internal bool UpdateDataByUpdateCommand(string queryString, List<SqlParameter> aSqlParameterlist, string dataBaseName)
        {
            try
            {
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                dbCommand.Parameters.Clear();
                dbCommand.Parameters.AddRange(aSqlParameterlist.ToArray());
                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    dbCommand.Parameters.Clear();
                    return true;
                }
                else
                {
                    dbCommand.Parameters.Clear();
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
        internal bool DeleteDataByDeleteCommand(string queryString, List<SqlParameter> aSqlParameterlist, string dataBaseName)
        {
            try
            {
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);
                dbCommand.Parameters.Clear();
                dbCommand.Parameters.AddRange(aSqlParameterlist.ToArray());
                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    dbCommand.Parameters.Clear();
                    return true;
                }
                else
                {
                    dbCommand.Parameters.Clear();
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal bool DeleteDataByDeleteCommand(string queryString, string dataBaseName)
        {
            try
            {
                Database db;
                DbCommand dbCommand;

                //Prepare Database Call
                db = DatabaseFactory.CreateDatabase("SolutionConnectionString" + dataBaseName);
                dbCommand = db.GetSqlStringCommand(queryString);

                if (db.ExecuteNonQuery(dbCommand) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        internal bool DeleteStatusUpdate(string tableName,string primaryField,string primaryId)
        {
            try
            {
                //string query = @"update " + tableName + " SET DeleteBy='" + HttpContext.Current.Session["LoginName"].ToString() + "',DeleteDate='" + System.DateTime.Now + "', IsActive='False' where " + primaryField + " = '" + primaryId + "'";
              //return  UpdateDataByUpdateCommand(query, "HRDB");

                string query = "delete from " + tableName + " where " + primaryField + " = '" + primaryId + "'";
                string insertQuery =
                    "INSERT INTO dbo.tbl_DeleteLog ( TableName ,PrimaryFieldName , PrimaryId , DeleteDate ,DeleteTime , DeleteBy)VALUES  ('" + tableName + "','" + primaryField + "','" + primaryId + "','" + System.DateTime.Today.ToString() + "' ,'" + System.DateTime.Now.ToString() + "','" + HttpContext.Current.Session["LoginName"].ToString() + "' )";
                SaveDataByInsertCommand(insertQuery, "HRDB");
                return DeleteDataByDeleteCommand(query, "HRDB");
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
