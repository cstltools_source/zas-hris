﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Library.DAL.InternalCls
{
    public class ClsCommonOperationDAL
    {
        public void CancelDataMark(GridView aGridView, DataTable aDataTable)
        {
            for (int i = 0; i < aDataTable.Rows.Count; i++)
            {
                if (aDataTable.Rows[i]["ActionStatus"].ToString().Trim() == "Cancel")
                {
                    aGridView.Rows[i].BackColor = System.Drawing.Color.Fuchsia;
                }
            }
        }

    }
}
