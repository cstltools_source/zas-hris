﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Library.DAL.InternalCls;

namespace Library.DAL.PanalCls
{
    public class PanalClsDAL
    {
        private ClsCommonInternalDAL _aCommonInternalDal;
        public DataTable Login(string loginName, string password)
        {
            _aCommonInternalDal = new ClsCommonInternalDAL();
            List<SqlParameter> aSqlParameterlist = new List<SqlParameter>();
            aSqlParameterlist.Add(new SqlParameter("@loginName", loginName));
            aSqlParameterlist.Add(new SqlParameter("@password", password));

            string queryString = @" SELECT LoginName,UserId,UserType,dbo.tblEmpGeneralInfo.EmpMasterCode,EmpInfoId FROM dbo.tblUser
                                    LEFT JOIN dbo.tblEmpGeneralInfo ON dbo.tblUser.EmpMasterCode = dbo.tblEmpGeneralInfo.EmpMasterCode where LoginName=@loginName and Password=@password and UserStatus='active'";
            return _aCommonInternalDal.DataContainerDataTable(queryString, aSqlParameterlist, "HRDB");
        }

        public DataTable MainMenu()
        {
            _aCommonInternalDal = new ClsCommonInternalDAL();
            DataTable aTableMainMenu = new DataTable();
            string queryString = "select * from tblMainMenu where ParantId is null or ParantId='' order by SL asc";
            aTableMainMenu = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aTableMainMenu;
        }
        public DataTable MainMenu(int userId)
        {
            _aCommonInternalDal = new ClsCommonInternalDAL();
            DataTable aTableMainMenu = new DataTable();
            string queryString = @"select tblMainMenu.* from tblMainMenu " +
                                    " INNER JOIN dbo.tblMenuDistribution ON dbo.tblMainMenu.SL = dbo.tblMenuDistribution.MenuSL " +
                                    " WHERE UserId='" + userId + "' AND (ParantId is null or ParantId='') order by tblMainMenu.SL asc";
            aTableMainMenu = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aTableMainMenu;
        }
        public DataTable SubItem(string Id)
        {
            DataTable aDataTableSubItem = new DataTable();
            _aCommonInternalDal = new ClsCommonInternalDAL();
            string queryString = "select * from tblMainMenu where ParantId='" + Id + "' order by SL asc";
            aDataTableSubItem = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aDataTableSubItem;
        }
        public DataTable SubItem(string Id, int userId)
        {
            DataTable aDataTableSubItem = new DataTable();
            _aCommonInternalDal = new ClsCommonInternalDAL();

            string queryString = @"select tblMainMenu.* from tblMainMenu " +
                                   " INNER JOIN dbo.tblMenuDistribution ON dbo.tblMainMenu.SL = dbo.tblMenuDistribution.MenuSL " +
                                   " WHERE UserId='" + userId + "' AND ParantId='" + Id + "' order by tblMainMenu.SL asc";

            aDataTableSubItem = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aDataTableSubItem;
        }
        public DataTable SubSubItem(string Id)
        {
            DataTable aDataSubSubItem = new DataTable();
            _aCommonInternalDal = new ClsCommonInternalDAL();
            string queryString = "select * from tblMainMenu where ParantId='" + Id + "' order by SL asc";
            aDataSubSubItem = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aDataSubSubItem;
        }
        public DataTable SubSubItem(string Id, int userId)
        {
            DataTable aDataSubSubItem = new DataTable();
            _aCommonInternalDal = new ClsCommonInternalDAL();
            string queryString = @"select tblMainMenu.* from tblMainMenu " +
                                   " INNER JOIN dbo.tblMenuDistribution ON dbo.tblMainMenu.SL = dbo.tblMenuDistribution.MenuSL " +
                                   " WHERE UserId='" + userId + "' AND ParantId='" + Id + "' order by tblMainMenu.SL asc";
            aDataSubSubItem = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aDataSubSubItem;
        }
        public DataTable SubSubChildItem(string Id)
        {
            DataTable aDataSubSubChildItem = new DataTable();
            _aCommonInternalDal = new ClsCommonInternalDAL();
            string queryString = "select * from tblMainMenu where ParantId='" + Id + "' order by SL asc";
            aDataSubSubChildItem = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aDataSubSubChildItem;
        }
        public DataTable SubSubChildItem(string Id, int userId)
        {
            DataTable aDataSubSubChildItem = new DataTable();
            _aCommonInternalDal = new ClsCommonInternalDAL();
            string queryString = @"select tblMainMenu.* from tblMainMenu " +
                                    " INNER JOIN dbo.tblMenuDistribution ON dbo.tblMainMenu.SL = dbo.tblMenuDistribution.MenuSL " +
                                    " WHERE UserId='" + userId + "' AND ParantId='" + Id + "' order by tblMainMenu.SL asc";
            aDataSubSubChildItem = _aCommonInternalDal.DataContainerDataTable(queryString, "HRDB");
            return aDataSubSubChildItem;
        }
    }
}
