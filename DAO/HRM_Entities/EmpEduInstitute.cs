﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.HRM_Entities
{
    public class EmpEduInstitute
    {
        public int EduInstituteId { get; set; }
        public string EduInstituteName { get; set; }
       
    }
}
