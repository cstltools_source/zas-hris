﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.HRM_Entities
{
    public class EmpQualification
    {
        public int QualificationId { get; set; }
        public string Qualification { get; set; }
        
    }
}
