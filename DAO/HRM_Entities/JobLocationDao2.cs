﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.HRM_Entities
{
    public class JobLocationDao2
    {
        public int JobLocationisionId { get; set; }
        public string JobLocationCode { get; set; }
        public string JobLocationName { get; set; }
        public string JobLocationShortName { get; set; }
        public bool IsActive { get; set; }
    }
}
