﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.HRM_Entities
{
    public class SalaryHeadType
    {
        public int SHeadTypeId { get; set; }
        public string SHeadType { get; set; }
        public bool IsActive { get; set; }
    }
}
