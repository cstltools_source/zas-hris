﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library.DAO.Panal_Entities
{
   public class ObjActionPageWiseStep
    {
       public int PWASId { get; set; }
       public int ManuSL { get; set; }
       public int ASId { get; set; }
    }
}
