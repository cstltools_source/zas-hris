﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.UA_DAO
{
    public class ObjActionPageWiseStep
    {
        public int PWASId { get; set; }
        public int ManuSL { get; set; }
        public int ASId { get; set; }
    }
}
