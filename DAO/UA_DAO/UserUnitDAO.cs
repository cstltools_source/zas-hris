﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.UA_DAO
{
    public class UserUnitDAO
    {
        public int UserUnitId { get; set; }
        public int UserId { get; set; }
        public int UnitId { get; set; }
    }
}
