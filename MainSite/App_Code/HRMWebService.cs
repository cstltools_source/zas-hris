﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for HRMWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class HRMWebService : System.Web.Services.WebService
{

    public HRMWebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    private string strConnectionString = ConfigurationManager.ConnectionStrings["SolutionConnectionStringHRDB"].ToString();
    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetEmployee(string prefixText)
    {
        string sql = "SELECT EmpMasterCode  FROM tblEmpGeneralInfo WHERE EmpMasterCode like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, strConnectionString);
        da.SelectCommand.Parameters.Add("@prefixText", System.Data.SqlDbType.VarChar, 50).Value = prefixText + "%";
        DataTable DTLocal = new DataTable();
        da.Fill(DTLocal);
        string[] items = new string[DTLocal.Rows.Count];
        int i = 0;
        foreach (DataRow dr in DTLocal.Rows)
        {
            items.SetValue(dr["EmpMasterCode"].ToString(), i);
            i++;
        }
        return items;
    }

    [WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetCustomer(string prefixText)
    {
        string sql = "";

        if (Session["UserType"].ToString() == "Admin")
        {
            sql = "select CustomerCode+':'+CustomerName as Customer from tblCustMaster where CustomerName like @prefixText";
        }
        else
        {
            string comUnit = Session["ComUnitId"].ToString();
            sql = "select CustomerCode+':'+CustomerName as Customer from tblCustMaster where ComUnitId='" + comUnit.Trim() + "'  and  CustomerName like @prefixText";
        }

        SqlDataAdapter da = new SqlDataAdapter(sql, strConnectionString);
        da.SelectCommand.Parameters.Add("@prefixText", System.Data.SqlDbType.VarChar, 50).Value = prefixText + "%";
        DataTable DTLocal = new DataTable();
        da.Fill(DTLocal);
        string[] items = new string[DTLocal.Rows.Count];
        int i = 0;
        foreach (DataRow dr in DTLocal.Rows)
        {
            items.SetValue(dr["Customer"].ToString(), i);
            i++;
        }
        return items;
    }
    [WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetProduct2(string prefixText)
    {
        string sql = "SELECT ProductName+':'+ProductCode AS Product FROM tblProduct WHERE ProductName like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, strConnectionString);
        da.SelectCommand.Parameters.Add("@prefixText", System.Data.SqlDbType.VarChar, 50).Value = prefixText + "%";
        DataTable DTLocal = new DataTable();
        da.Fill(DTLocal);
        string[] items = new string[DTLocal.Rows.Count];
        int i = 0;
        foreach (DataRow dr in DTLocal.Rows)
        {
            items.SetValue(dr["Product"].ToString(), i);
            i++;
        }
        return items;
    }

    [WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetProduct3(string prefixText)
    {
        string sql = "SELECT ProductCode+':'+ProductName+':'+PackSize AS Product FROM tblProduct WHERE ProductName like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, strConnectionString);
        da.SelectCommand.Parameters.Add("@prefixText", System.Data.SqlDbType.VarChar, 50).Value = prefixText + "%";
        DataTable DTLocal = new DataTable();
        da.Fill(DTLocal);
        string[] items = new string[DTLocal.Rows.Count];
        int i = 0;
        foreach (DataRow dr in DTLocal.Rows)
        {
            items.SetValue(dr["Product"].ToString(), i);
            i++;
        }
        return items;
    }

    [WebMethod(EnableSession = true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetEmpInfo(string prefixText)
    {
        string sql = "";

        if (Session["UserType"].ToString() == "Admin")
        {
            sql = "select EmpMasterCode+':'+EmpName as EmpInfo from tblEmpGeneralInfo where EmpName like @prefixText";
        }
        else
        {
            string comUnit = Session["ComUnitId"].ToString();
            sql = "select EmpMasterCode+':'+EmpName as EmpInfo from tblEmpGeneralInfo where ComUnitId='" + comUnit.Trim() + "'  and  EmpName like @prefixText";
        }

        SqlDataAdapter da = new SqlDataAdapter(sql, strConnectionString);
        da.SelectCommand.Parameters.Add("@prefixText", System.Data.SqlDbType.VarChar, 50).Value = prefixText + "%";
        DataTable DTLocal = new DataTable();
        da.Fill(DTLocal);
        string[] items = new string[DTLocal.Rows.Count];
        int i = 0;
        foreach (DataRow dr in DTLocal.Rows)
        {
            items.SetValue(dr["EmpInfo"].ToString(), i);
            i++;
        }
        return items;
    }
}
