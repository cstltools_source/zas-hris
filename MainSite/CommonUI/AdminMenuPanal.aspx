﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="AdminMenuPanal.aspx.cs" Inherits="CommonUI_AdminMenuPanal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
     <%--<script type="text/javascript">

            $(document).ready(function () {
                $('#accordion').accordion({
                    collapsible: true,
                    active: 0,
                    autoHeight: false
                });
            });

            ////On UpdatePanel Refresh
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {

                        $("#accordion").accordion({
                            collapsible: true,
                            active: parseInt(document.getElementById('<%= this.hidAccordionIndex.ClientID %>').value),
                        autoHeight: false

                    });

                }
            });
        };

    </script>--%>

        <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="hidAccordionIndex" runat="server" Value="1" />
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-user"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Menu Panal Management </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                       
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <%--<li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">User Administration</a></li>--%>
                            <li class="breadcrumb-item"><a href="#">Menu Panal Management </a></li>
                        </ol>
                    </nav>
                </div>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="msgLabel" runat="server" Text=""></asp:Label>
                                    <div class="rw-accordion rw-accordion--w-animation">
		                                    <div class="rw-accordion__item open">
			                                    <div class="rw-accordion__header">
				                                    <h4 class="rw-accordion__title">Main Menu</h4>
			                                    </div>
			                                    <div class="rw-accordion__content">
				                                     <div class="form-row"  >
                                                        <div class="col-2">
                                                            <div class="form-group">
                                                                <label>Main Menu Name : </label>
                                                                <asp:TextBox ID="mainMenuNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="form-group">
                                                                <label>Add Page : Yes ? </label>
                                                                <asp:CheckBox ID="addPageCheckBox" runat="server" AutoPostBack="True" 
                                                                 oncheckedchanged="addPageCheckBox_CheckedChanged" />
                                                            </div>
                                                        </div>
                                                       <div  id="divAddpage" runat="server" Visible="False" ></div>
                                                        <div class="col-2"> 
                                                            <div class="form-group">
                                                                <label>Destination Folder : </label>
                                                                <asp:TextBox ID="mainMenuDesFolderTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="form-group">
                                                                <label>Page Name : </label>
                                                                <asp:TextBox ID="mainMenuPageNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                              </div>
                                                        </div>
                                                       <%--</div>--%>
                                                        <div class="col-2">
                                                            <br/>
                                                            <div class="form-group">
                                                               <asp:Button ID="saveMainMenuButton" Text="Save" CssClass="btn btn-sm btn-info btn-sm" runat="server" OnClick="saveMainMenuButton_Click" />
                                                             </div>
                                                        </div>
                                       
                                                     </div>
			                                    </div>
		                                    </div>
		                                    <div class="rw-accordion__item">
			                                    <div class="rw-accordion__header">
				                                    <h4 class="rw-accordion__title">Sub Menu</h4>
			                                    </div>
			                                    <div class="rw-accordion__content">
                                                    <div class="form-row" >
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Main Menu : </label>
                                                                <asp:DropDownList ID="ddlMMenuNameMPanal" runat="server" CssClass="form-control form-control-sm">
                                                                </asp:DropDownList>
                                                            </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Menu Name :</label>
                                                            <asp:TextBox ID="menuNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label> Add Page : Yes ? </label>
                                                            <asp:CheckBox ID="addPageCheckBoxMenu" runat="server" AutoPostBack="True" oncheckedchanged="addPageCheckBoxMenu_CheckedChanged" />
                                                            </div>
                                                    </div>
                                                    <div  id="divMenu" runat="server" Visible="False" ></div>
                                                        <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Destination Folder :</label>
                                                            <asp:TextBox ID="menuDisFolderTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label> Page Name : </label>
                                                            <asp:TextBox ID="menuPageTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <%--</div>--%>
                                                    <div class="col-2">
                                                        <br/>
                                                            <asp:Button ID="menuSaveButton" Text="Save" CssClass="btn btn-sm btn-info btn-sm" runat="server" OnClick="menuSaveButton_Click" />
                                                        </div>
                                                    </div>

			                                    </div>
		                                    </div>
		                                    <div class="rw-accordion__item">
			                                    <div class="rw-accordion__header">
				                                    <h4 class="rw-accordion__title">Sub Sub Menu</h4>
			                                    </div>
			                                    <div class="rw-accordion__content">
				                                     <div class="form-row"  >
                                                       <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Main Menu :</label>
                                                             <asp:DropDownList ID="ddlSubMMenuNameMPanal" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" 
                                                                onselectedindexchanged="ddlSubMMenuNameMPanal_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label> Menu Name : </label>
                                                            <asp:DropDownList ID="ddlMenuNameForSubMenu" runat="server" CssClass="form-control form-control-sm">
                                                          </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                   <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Sub Menu :</label>
                                                            <asp:TextBox ID="subMenuNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                       
                                                    <div class="col-1">
                                                        <div class="form-group">
                                                            <label>  Add Page : Yes ? </label>
                                                             <asp:CheckBox ID="addPageCheckBoxSubMenu" runat="server" AutoPostBack="True" oncheckedchanged="addPageCheckBoxSubMenu_CheckedChanged"/>
                                                        </div>
                                                    </div>
                                                  <div  id="divSubMenu" runat="server" Visible="False" ></div>
                                                       <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Destination :</label>
                                                             <asp:TextBox ID="submenuDisFolderTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-2">
                                                        <div class="form-group">
                                                            <label>Page Name : </label>
                                                            <asp:TextBox ID="submenuPageTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                         </div>
                                                    </div>
                                                   <div class="col-1">
                                                       <br/>
                                                        <div class="form-group">
                                                           <asp:Button ID="subMenuSaveButton" runat="server" Text="Save" CssClass="btn btn-sm btn-info" onclick="subMenuSaveButton_Click" />
                                                        </div>
                                                    </div>
                                                 <%--</div>--%>
                                                    </div>

			                                    </div>
		                                    </div>
                                            <div class="rw-accordion__item">
			                                    <div class="rw-accordion__header">
				                                    <h4 class="rw-accordion__title">Sub Sub Sub Menu</h4>
			                                    </div>
			                                    <div class="rw-accordion__content">
				                                        <div class="form-row">
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <label> Main Menu : </label>
                                                                    <asp:DropDownList ID="ddlChMMenuNameMPanal" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" 
                                                                        onselectedindexchanged="ddlChMMenuNameMPanal_SelectedIndexChanged">
                                                                    </asp:DropDownList>
                                                                  </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <label> Menu Name : </label>
                                                                    <asp:DropDownList ID="ddlMenuNameForChildMenu" runat="server" CssClass="form-control form-control-sm" 
                                                                        onselectedindexchanged="ddlMenuNameForChildMenu_SelectedIndexChanged" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                  </div>
                                                            </div>
                                                          <div class="col-2">
                                                               <div class="form-group">
                                                                    <label> Sub Menu Name : </label>
                                                                    <asp:DropDownList ID="ddlsubMenuName" runat="server" CssClass="form-control form-control-sm" >
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                           </div>
                                                           <div class="form-row">
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <label> Child Menu Name : </label>
                                                                    <asp:TextBox ID="childMenuNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <label> Destination Folder : </label>
                                                                    <asp:TextBox ID="childDisFolderTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <div class="form-group">
                                                                    <label> Page Name : </label>
                                                                    <asp:TextBox ID="childMenuPageTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-2">
                                                                <br/>
                                                                <div class="form-group">
                                                                    <asp:Button ID="saveChildMenu" Text="Save" CssClass="btn btn-sm btn-info btn-sm" runat="server" OnClick="saveChildMenu_Click" />
                                                                </div>
                                                            </div>
                                                           </div>

			                                    </div>
		                                    </div>
	                                    </div>
                                    </div>
                                     <asp:HiddenField ID="hiddenField" runat="server" />
                            
                                </div>
                            </div>
                         
                        </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

