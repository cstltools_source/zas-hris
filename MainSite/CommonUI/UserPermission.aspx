﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="UserPermission.aspx.cs" Inherits="CommonUI_UserPermissionPanal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-user"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">User Permission</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="detailsViewButton_OnClick" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">User Administration</a></li>
                            <li class="breadcrumb-item"><a href="#">User Permission</a></li>
                        </ol>
                    </nav>
                </div>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                         <%--   <div class="row">
                                <div class="col-sm-12 col-lg-12 col-md-12">
                                 
                                    <div class="box-content">
                                        <div class="form-group">
                                            <div class="col-sm-12 " style="z-index: 50">
                                                <div style="text-align: left; padding-left: 15px;">
                                                    <label for="" class="control-label ">
                                                    </label>
                                                </div>
                                                <asp:Label ID="msgLabel" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>--%>
                            <div class="row">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <%--<div class="col-sm-12">--%>
                                    <div class="box-content">
                                        <div class="form-group">
                                            <div class="col-sm-12 " style="z-index: 50">
                                                <div style="text-align: left; padding-left: 15px;">
                                                    <label for="" class="control-label ">
                                                        User Name :
                                                    </label>
                                                </div>
                                                <asp:DropDownList ID="userDropDownList" runat="server" CssClass=" form-control form-control-sm" AutoPostBack="True"
                                                    OnSelectedIndexChanged="userDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <%--</div>--%>
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <%--<div class="col-sm-12">--%>
                                    <div class="box-content">
                                        <div class="form-group">
                                            <div class="col-sm-12 ">
                                                <div style="text-align: left; padding-left: 15px;">
                                                    <label for="" class="control-label ">
                                                        Main Menu :
                                                    </label>
                                                </div>
                                                <asp:DropDownList ID="mainMenuDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="mainMenuDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <%--</div>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <%--<div class="col-sm-12">--%>
                                    <div class="box-content">
                                        <div class="form-group">
                                            <div class="col-sm-12 " style="z-index: 50">
                                                <div style="text-align: left; padding-left: 15px;">
                                                    <label for="" class="control-label ">
                                                        Main Menu Panal :
                                                    </label>
                                                </div>
                                                <div id="container1" style="height: 400px; overflow: auto; width: auto">
                                                    <asp:GridView ID="mainMenuGridView" runat="server" CssClass="table table-bordered text-center thead-dark" AutoGenerateColumns="False"
                                                        DataKeyNames="SL,Status" OnRowDataBound="mainMenuGridView_RowDataBound" OnRowCommand="mainMenuGridView_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="mainMenuCheckBox" runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Main Menu" DataField="ManuName" />
                                                            <asp:TemplateField HeaderText="Save">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="mainMenuSaveImageButton" runat="server" ImageUrl="~/Assets/img/save.png"
                                                                        CommandName="SaveData" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="deleteMainMenuBytton" runat="server" ImageAlign="Middle" ImageUrl="~/Assets/img/delete.png"
                                                                        CommandName="Remove" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <%--</div>--%>
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <div class="col-sm-12 ">
                                                <div style="text-align: left; padding-left: 15px;">
                                                    <label for="" class="control-label ">
                                                        Menu Option :
                                                    </label>
                                                </div>
                                                <div id="container2" style="height: 400px; overflow: auto; width: auto">
                                                    <asp:GridView ID="menuGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered text-center thead-dark"
                                                        DataKeyNames="SL,Status" OnRowDataBound="menuGridView_RowDataBound" OnRowCommand="menuGridView_RowCommand">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="menuAllCheckBox" runat="server" OnCheckedChanged="menuAllCheckBox_CheckedChanged" AutoPostBack="True" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="menuCheckBox" runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Main Menu" DataField="ManuName" />
                                                            <asp:TemplateField HeaderText="Save">
                                                                <HeaderTemplate>
                                                                    <asp:ImageButton ID="allsaveMenuImageButton" runat="server" ImageUrl="~/Assets/img/save.png" OnClick="allsaveMenuImageButton_Click" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="saveMenuImageButton" runat="server" ImageUrl="~/Assets/img/save.png"
                                                                        CommandName="SaveData" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="deleteMainMenuBytton" runat="server" ImageAlign="Middle" ImageUrl="~/Assets/img/delete.png"
                                                                        CommandName="Remove" CommandArgument='<%# Container.DataItemIndex %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                           <asp:HiddenField ID="hiddenField" runat="server" />
                        </div>
                    </div>
                </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

