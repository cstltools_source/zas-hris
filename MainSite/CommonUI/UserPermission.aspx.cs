﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.Panal_BLL;

public partial class CommonUI_UserPermissionPanal : System.Web.UI.Page
{
    PanalBLL aPanalBLL = new PanalBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdownList();
        }
    }
    public void LoadDropdownList()
    {
         
        aPanalBLL.UserDdl(userDropDownList);
         
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void userDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        MainMenuLoad();
         
        aPanalBLL.MainMenu(mainMenuDropDownList, userDropDownList.SelectedValue);
         
        menuGridView.DataSource = null;
        menuGridView.DataBind();
    }
    private void MainMenuLoad()
    {
         
        mainMenuGridView.DataSource = aPanalBLL.MainMenuLoad(userDropDownList.SelectedValue);
         
        mainMenuGridView.DataBind();
    }
    private void MainMenuLoad2()
    {
        mainMenuGridView.DataSource = aPanalBLL.MainMenuLoad(userDropDownList.SelectedValue);
        mainMenuGridView.DataBind();
    }
    private void MenuLoad(string userId, string parantId)
    {
         
        menuGridView.DataSource = aPanalBLL.OtherMenuLoad(userId, parantId);
         
        menuGridView.DataBind();
    }
    private void MenuLoad2(string userId, string parantId)
    {
        menuGridView.DataSource = aPanalBLL.OtherMenuLoad(userId, parantId);
        menuGridView.DataBind();
    }
    protected void mainMenuDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        MenuLoad(userDropDownList.SelectedValue, mainMenuDropDownList.SelectedValue);
    }
    protected void mainMenuGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string status = mainMenuGridView.DataKeys[e.Row.RowIndex].Values[1].ToString();
            CheckBox mainMenuCheckBox = (CheckBox)e.Row.FindControl("mainMenuCheckBox");
            if (status.Trim() == "1")
            {
                mainMenuCheckBox.Checked = true;
                mainMenuCheckBox.Enabled = false;
            }
        }
    }

    protected void mainMenuGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SaveData")
        {
            //Get rowindex
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string sL = mainMenuGridView.DataKeys[rowindex][0].ToString();
            CheckBox mainMenuCheckBox = (CheckBox)mainMenuGridView.Rows[rowindex].Cells[0].FindControl("mainMenuCheckBox");

            if (mainMenuCheckBox.Checked)
            {
                 
                if (mainMenuCheckBox.Enabled != false)
                {
                    bool ok = aPanalBLL.SaveMainMenu(Convert.ToInt32(sL), Convert.ToInt32(userDropDownList.SelectedValue));

                    if (ok == true)
                    {

                        MainMenuLoad2();
                        aPanalBLL.MainMenu(mainMenuDropDownList, userDropDownList.SelectedValue);
                        showMessageBox("Data Save Successfully");
                        
                    }
                    else
                    {
                        showMessageBox("Error!!");
                        
                    }

                }
                 
            }
            else
            {
                showMessageBox("Select First");  
            }
            

        }
        if (e.CommandName == "Remove")
        {
           
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string sL = mainMenuGridView.DataKeys[rowindex][0].ToString();
            CheckBox mainMenuCheckBox = (CheckBox)mainMenuGridView.Rows[rowindex].Cells[0].FindControl("mainMenuCheckBox");
             
            if (mainMenuCheckBox.Checked)
            {
                bool ok = aPanalBLL.MenuPermissionRemove(Convert.ToInt32(sL), Convert.ToInt32(userDropDownList.SelectedValue));
                if (ok == true)
                {
                    MainMenuLoad2();
                    aPanalBLL.MainMenu(mainMenuDropDownList, userDropDownList.SelectedValue);
                    MenuLoad2(userDropDownList.SelectedValue, mainMenuDropDownList.SelectedValue);
                    showMessageBox("Permission Delete");
                    
                }
                else
                {
                    showMessageBox("Error!!");
                }

            }
            else
            {
                showMessageBox("Select First");
            }
             
        }
    }

    protected void menuGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string status = menuGridView.DataKeys[e.Row.RowIndex].Values[1].ToString();
            CheckBox MenuCheckBox = (CheckBox)e.Row.FindControl("menuCheckBox");
            if (status.Trim() == "1")
            {
                MenuCheckBox.Checked = true;
                MenuCheckBox.Enabled = false;
            }
        }
    }
    protected void menuGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "SaveData")
        {
           int rowindex = Convert.ToInt32(e.CommandArgument);
            string sL = menuGridView.DataKeys[rowindex][0].ToString();
            CheckBox menuCheckBox = (CheckBox)menuGridView.Rows[rowindex].Cells[0].FindControl("menuCheckBox");

            if (menuCheckBox.Checked)
            {
                 
                if (menuCheckBox.Enabled != false)
                {
                    bool ok = aPanalBLL.SaveMenuBll(Convert.ToInt32(sL), Convert.ToInt32(userDropDownList.SelectedValue));
                    if (ok == true)
                    {
                        MenuLoad2(userDropDownList.SelectedValue, mainMenuDropDownList.SelectedValue);
                        showMessageBox("Permitted Data Successfully Save");
                    }
                    else
                    {
                        showMessageBox("Error!!");
                    }
                }
                 
            }
            else
            {
                showMessageBox("Select First");
               
            }
        }

        if (e.CommandName == "Remove")
        {
            
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string sL = menuGridView.DataKeys[rowindex][0].ToString();
            CheckBox mainMenuCheckBox = (CheckBox)menuGridView.Rows[rowindex].Cells[0].FindControl("menuCheckBox");
             
            if (mainMenuCheckBox.Checked)
            {
                bool ok = aPanalBLL.MenuPermissionRemove(Convert.ToInt32(sL), Convert.ToInt32(userDropDownList.SelectedValue));
                if (ok == true)
                {
                    MainMenuLoad2();
                    MenuLoad2(userDropDownList.SelectedValue, mainMenuDropDownList.SelectedValue);
                    showMessageBox("Permission Delete");
                   
                }
                else
                {
                    showMessageBox("Error!!");
                   
                }
            }
            else
            {
                showMessageBox("Select First");
                
            }
             
        }
    }

    protected void menuAllCheckBox_CheckedChanged(object sender, EventArgs e)
    {

        CheckBox ChkBoxHeader = (CheckBox)menuGridView.HeaderRow.FindControl("menuAllCheckBox");

        for (int i = 0; i < menuGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)menuGridView.Rows[i].Cells[0].FindControl("menuCheckBox");
            if (ChkBoxRows.Enabled == true)
            {
                if (ChkBoxHeader.Checked == true)
                {
                    ChkBoxRows.Checked = true;
                }
                else
                {
                    ChkBoxRows.Checked = false;
                }
            }

        }
    }

    protected void allsaveMenuImageButton_Click(object sender, ImageClickEventArgs e)
    {
        for (int i = 0; i < menuGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)menuGridView.Rows[i].Cells[0].FindControl("menuCheckBox");
            if (ChkBoxRows.Enabled)
            {
                if (ChkBoxRows.Checked)
                {

                    int rowindex = Convert.ToInt32(i);
                    string sL = menuGridView.DataKeys[rowindex][0].ToString();
                    CheckBox menuCheckBox = (CheckBox)menuGridView.Rows[rowindex].Cells[0].FindControl("menuCheckBox");

                    if (menuCheckBox.Checked)
                    {
                         
                        if (menuCheckBox.Enabled != false)
                        {
                            bool ok = aPanalBLL.SaveMenuBll(Convert.ToInt32(sL),
                                Convert.ToInt32(userDropDownList.SelectedValue));
                            if (ok == true)
                            {
                                showMessageBox("All Manu have been Save Successfully");
                            }
                            else
                            {
                                showMessageBox("Error!!");
                            }
                        }
                         
                    }
                    else
                    {
                        showMessageBox("Select First");
                    }
                }
            }
        }
         
        MenuLoad2(userDropDownList.SelectedValue, mainMenuDropDownList.SelectedValue);
         
    }

}