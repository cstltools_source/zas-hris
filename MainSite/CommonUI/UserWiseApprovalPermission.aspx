﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="UserWiseApprovalPermission.aspx.cs" Inherits="CommonUI_UserWiseApprovalPermission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Page Wise Approval Step Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                       <%-- <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Manu Panel </a></li>
                            <li class="breadcrumb-item"><a href="#">Page Wise Approval Step Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>User Name :</label>
                                        <asp:DropDownList ID="userDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                            AutoPostBack="True" 
                                            onselectedindexchanged="userDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                         
                                     </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Main Menu :</label>
                                        <asp:DropDownList ID="mainMenuDropDownList" runat="server" AutoPostBack="True" 
                                            CssClass="form-control form-control-sm" 
                                            onselectedindexchanged="mainMenuDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                         
                                     </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Menu Option :</label>
                                        <asp:DropDownList ID="appMenuDropDownList" runat="server" AutoPostBack="True" 
                                            CssClass="form-control form-control-sm" 
                                            onselectedindexchanged="appMenuDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                         
                                     </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                       </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <asp:TextBox ID="lblMsg" runat="server" CssClass="form-control form-control-sm"  ReadOnly="True"></asp:TextBox>
                                       </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <asp:Button ID="submitButton" Text="Submit" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                       </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                       </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                         <label>Approval Action :</label>
                                        <div id ="container2" style ="height:600px;overflow:auto;width:auto ">
                                             <asp:RadioButtonList ID="actionRadioButtonList" runat="server">
                                             </asp:RadioButtonList>
                                        </div>
                                       </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        
                                       </div>
                                </div>
                            </div>
                         
                            
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

