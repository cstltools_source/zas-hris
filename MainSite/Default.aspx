﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>


<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title> CSTL | Login Page </title>
	<link rel="stylesheet" type="text/css" href="Loginassets/css/logincss.css" media="all" />
</head>
<body>
	
	<!-- Container Start -->
	<div class="container" style="background-image: url(/assetsImg/img/backgrounds/Bridge.jpg);">
        <div class="login-box">
            <div class="mid">
                <!-- Logo -->
                <div class="logo">
                    <img src="Loginassets/images/logo.png" alt="" />
                </div>

                <!-- Branding hare -->
                <div class="brand">
                    <h2><span>#</span> CSTL-HRM <span>'FullAlert'</span> </h2>
                    <p>Human Resource Management System</p>
                </div>

                <!-- All input hare -->

                <form id="form" runat="server">
                    
                    <div class="input-panel">
                        <div class="panel-box">
                            <div class="panel">
                                <label>Username</label>
                                <asp:TextBox ID="userNameTextBox" class="input" runat="server" placeholder="Username"></asp:TextBox>
                                <%--<input type="text" class="input" name="username" id="username" placeholder="Username"/>--%>
                            </div>

                            <div class="panel">
                                <label>Password</label>
                                <asp:TextBox ID="passwordTextBox" class="input" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
                            </div>

                            <div class="panel">
                                <div class="box">
                                    <input type="checkbox" class="checkbox" style="background-color: #000;" checked=""><span> Remember me </span>
                                </div>
                                <div class="box">
                                    <asp:HyperLink ID="HyperLink1" class="link" NavigateUrl="ResetPassword/MailForPasswordReset.aspx" runat="server">Forgot password?</asp:HyperLink>
                                </div>
                            </div>

                            <div class="panel" style="text-align: center;">
                                <asp:Button ID="loginButton" Text="Login to account " runat="server" class="button" OnClick="loginButton_Click" />
                                <%--<input type="submit" class="button" value="Login to account" />--%>
                            </div>
                        </div>
                        
                        <asp:Label ID="msgLabel" CssClass="color: red; font-weight-bold" ForeColor="red" runat="server" Text=""></asp:Label>
                    </div>
                    
                </form>

                <!-- Contact box -->
                <div class="contact-box">
                    <h3>Contacts </h3>
                    <h2>Creatrix Soft Tech Ltd. </h2>
                    <h4>48,Kazi Nazrul Islam Aveneu, Dhaka </h4>
                    <h4>+88-02-9131516, </h4>
                </div>


            </div>
        </div>
    </div>
	<!-- Container End -->
	
	<script type="text/javascript" src="Loginassets/js/jquery.min.js"> </script>
	<script type="text/javascript"> 
	
	    $(document).ready(function(){
		
			height = $(window).height();
	        $(".container").css("height", height);
			
			
			height1 = $(window).height() - 30;
	        $(".login-box").css("height", height1);
	    });
		
	</script>
</body>
</html>
























