﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.Panal_BLL;

//using BLL.UA_BLL;


public partial class _Default : System.Web.UI.Page
{
    DataTable aTableLogin = new DataTable();
    PanalBLL aPanalBll = new PanalBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    //Update on 26/04/2019 by pulak
    protected void loginButton_Click(object sender, EventArgs e)
    {
        msgLabel.Text = "";
        string loginName = string.Empty;
        DateTime nowdate = Convert.ToDateTime(DateTime.Today.ToShortDateString());
        string passwordText = string.Empty;
        DateTime expdate = Convert.ToDateTime("06/30/2035");
        if (nowdate <= expdate)
        {
            if (!string.IsNullOrEmpty(userNameTextBox.Text.Trim()))
            {
                loginName = userNameTextBox.Text.Trim();
                if (!string.IsNullOrEmpty(passwordTextBox.Text.Trim()))
                {
                    passwordText = passwordTextBox.Text.Trim();

                    aTableLogin = aPanalBll.Login(loginName, passwordText);
                    if (aTableLogin.Rows.Count > 0)
                    {
                        Session["UserId"] = aTableLogin.Rows[0]["UserId"].ToString().Trim();
                        Session["LoginName"] = aTableLogin.Rows[0]["LoginName"].ToString().Trim();
                        Session["UserType"] = aTableLogin.Rows[0]["UserType"].ToString().Trim();
                        Session["EmpMasterCode"] = aTableLogin.Rows[0]["EmpMasterCode"].ToString().Trim();
                        Session["EmpInfoId"] = aTableLogin.Rows[0]["EmpInfoId"].ToString().Trim();

                        Response.Redirect("UI/DashBoard.aspx");
                    }
                    else
                    {
                        msgLabel.Text = "Something Wrong!!!!";
                    }
                }
                else
                {
                    msgLabel.Text = "Input Password Please!!!";
                }
            }
            else
            {
                msgLabel.Text = "Input Use Name Please!!!";
            }
        }
        else
        {
            showMessageBox("Login Failed due to Date Expeired Please contact Creatrix !!!!");
        }
    }

}