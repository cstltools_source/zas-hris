﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_AdvanceSalView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    AdvanceSalDeductionBLL _aAdvanceSalDeductionBLL = new AdvanceSalDeductionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EmpAdvanceSalDeductionLoad();
        }
    }

    private void EmpAdvanceSalDeductionLoad()
    {
        aDataTable = _aAdvanceSalDeductionBLL.LoadAdvanceSalDeduction();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    
    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        EmpAdvanceSalDeductionLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("AdvanceSalEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "AdvanceSalEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(900/2);var Mtop = (screen.height/2)-(700/2);window.open( '" + url + "', null, 'height=700,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string AdvanceSalDeductionId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(AdvanceSalDeductionId);
        }

    }
  
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox) loadGridView.Rows[i].Cells[7].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string AdvanceSalDeductionId = loadGridView.DataKeys[i][0].ToString();
                _aAdvanceSalDeductionBLL.DeleteAdvanceSalDeductionBLL(AdvanceSalDeductionId);
            }

        }
        EmpAdvanceSalDeductionLoad();
    }
}