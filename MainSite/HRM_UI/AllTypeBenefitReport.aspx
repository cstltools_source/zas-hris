﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="AllTypeBenefitReport.aspx.cs" Inherits="HRM_UI_AllTypeBenefitReport" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">All Type Benefit Report </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">All Type Benefit Report</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Select Report </label>
                                        <asp:DropDownList ID="selectReportDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="selectReportDropDownList_SelectedIndexChanged">
                                            <asp:ListItem> Select any one </asp:ListItem>
                                            <asp:ListItem Value="MB">PR Fund Monthly</asp:ListItem>
                                            <asp:ListItem Value="PFYE">PR Fund Yearly Emp Wise</asp:ListItem>
                                            <asp:ListItem Value="PFS">PR Fund Sum</asp:ListItem>
                                            <asp:ListItem Value="PFY">PR Fund Yearly Sum</asp:ListItem>
                                            <asp:ListItem Value="PFEW">PR Fund Employee Wise</asp:ListItem>
                                            <asp:ListItem Value="FB">Fastival Bonus</asp:ListItem>
                                            <asp:ListItem Value="EL">EL Payment </asp:ListItem>
                                            <asp:ListItem Value="FBDL">F.Bonus Disb Letter</asp:ListItem>
                                            <asp:ListItem Value="ELDL">E.Leave Disb Letter</asp:ListItem>
                                            <asp:ListItem Value="FBP">F.Bonus Payslip</asp:ListItem>
                                            <asp:ListItem Value="ELP">E.L. Payslip(All)</asp:ListItem>
                                            <asp:ListItem Value="ELPS">E.L. Payslip(Single)</asp:ListItem>
                                            <asp:ListItem Value="FBS">Festival Bonus Summery</asp:ListItem>
                                            <asp:ListItem Value="ELS">Earn Leave Summery</asp:ListItem>
                                            <asp:ListItem Value="FBPS">F.Bonus Payslip(Single)</asp:ListItem>
                                            <asp:ListItem Value="MA">Mobile Allowance</asp:ListItem>
                                            <asp:ListItem Value="MAUW">Mobile Allowance(Unit Wise)</asp:ListItem>
                                            <asp:ListItem Value="FBSR">F.Bonus Stamp Report</asp:ListItem>
                                            <asp:ListItem Value="ELSR">E.Leave Stamp Report</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div id="year" runat="server">
                                <div class="form-row" runat="server" id="status" visible="False">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Status </label>
                                            <asp:DropDownList ID="empStatusDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                <asp:ListItem>Active</asp:ListItem>
                                                <asp:ListItem>Inactive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Year </label>
                                            <asp:DropDownList ID="yearDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div runat="server" id="divmonth" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Month </label>
                                            <asp:DropDownList ID="monthDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                <asp:ListItem Value=""> Select any one</asp:ListItem>
                                                <asp:ListItem Value="1">January</asp:ListItem>
                                                <asp:ListItem Value="2">February</asp:ListItem>
                                                <asp:ListItem Value="3">March</asp:ListItem>
                                                <asp:ListItem Value="4">April</asp:ListItem>
                                                <asp:ListItem Value="5">May</asp:ListItem>
                                                <asp:ListItem Value="6">June</asp:ListItem>
                                                <asp:ListItem Value="7">July</asp:ListItem>
                                                <asp:ListItem Value="8">August</asp:ListItem>
                                                <asp:ListItem Value="9">September</asp:ListItem>
                                                <asp:ListItem Value="10">October</asp:ListItem>
                                                <asp:ListItem Value="11">November</asp:ListItem>
                                                <asp:ListItem Value="12">December</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="divempwise" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Code </label>
                                            <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divfastival" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Festival Name </label>
                                            <asp:DropDownList ID="fastivalNameDropDownList" runat="server"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="bankcash" runat="server" visible="False">

                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Bank / Cash </label>
                                            <asp:DropDownList ID="bankCashDropDown" runat="server" CssClass="form-control form-control-sm">
                                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                                <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                                <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Company Name </label>
                                            <asp:DropDownList ID="comNameDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Unit Name </label>
                                            <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewRptButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

