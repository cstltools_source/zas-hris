﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_AllTypeBenefitReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
            DropDownList();
        }
    }

    public void DropDownList()
    {
        FastibalBonusBLL aFastibalBonusBll=new FastibalBonusBLL();
        aFastibalBonusBll.LoadFestivalName(fastivalNameDropDownList);
        EmpGeneralInfoBLL aInfoBll=new EmpGeneralInfoBLL();
        aInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }
    private void PopUp(string rptType)
    {
        Session["empcode"] = empCodeTextBox.Text;

        string url = "../Report_UI/AllTypeBenefitReportViewer.aspx?rptType=" + rptType + "&month=" + monthDropDownList.SelectedValue + "&year=" + yearDropDownList.SelectedItem.Text + "&monthname=" + monthDropDownList.SelectedItem.Text+"&fastivalId="+fastivalNameDropDownList.SelectedValue+"&bankcash="+bankCashDropDown.SelectedItem.Text; 
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    private string ParameterGenerator()
    {
        string parameter = "";


        if (selectReportDropDownList.SelectedValue == "FB" || selectReportDropDownList.SelectedValue == "FBDL" || selectReportDropDownList.SelectedValue == "FBP")
        {
            if (!string.IsNullOrEmpty(comNameDropDownList.SelectedValue))
            {
                parameter = parameter + " and tblFestivalBonusProcessData.CompanyInfoId='" + comNameDropDownList.SelectedValue + "' ";
            }
            if (!string.IsNullOrEmpty(unitNameDropDownList.SelectedValue))
            {
                parameter = parameter + " and tblFestivalBonusProcessData.UnitId='" + unitNameDropDownList.SelectedValue + "' ";
            }
        }
        if (selectReportDropDownList.SelectedValue == "EL" || selectReportDropDownList.SelectedValue == "ELDL" || selectReportDropDownList.SelectedValue == "ELP")
        {
            if (!string.IsNullOrEmpty(comNameDropDownList.SelectedValue))
            {
                parameter = parameter + " and tblELPaymentProcessData.CompanyInfoId='" + comNameDropDownList.SelectedValue + "' ";
            }
            if (!string.IsNullOrEmpty(unitNameDropDownList.SelectedValue))
            {
                parameter = parameter + " and tblELPaymentProcessData.UnitId='" + unitNameDropDownList.SelectedValue + "' ";
            }
        }
        if (selectReportDropDownList.SelectedValue == "ELPS")
        {
            parameter = parameter + " and EmpMasterCode='" + empCodeTextBox.Text + "' ";
        }

        if (selectReportDropDownList.SelectedValue == "FBPS" )
        {

            parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode='" + empCodeTextBox.Text+ "' ";
            
        }
        if (selectReportDropDownList.SelectedValue == "MA" || selectReportDropDownList.SelectedValue == "MAUW")
        {
            if (!string.IsNullOrEmpty(yearDropDownList.SelectedValue))
            {
                parameter = parameter + "  YEAR(S.SalaryStartDate)='" + yearDropDownList.SelectedItem.Text + "' ";
            }
            if (!string.IsNullOrEmpty(monthDropDownList.SelectedValue))
            {
                parameter = parameter + " and Month(S.SalaryStartDate)='" + monthDropDownList.SelectedValue + "' ";
            }
            if (bankCashDropDown.SelectedValue != "ALL")
            {
                parameter = parameter + " and S.PayBankorCash ='" + bankCashDropDown.SelectedValue + "' ";
            }
            if (!string.IsNullOrEmpty(comNameDropDownList.SelectedValue))
            {
                parameter = parameter + " and S.CompanyInfoId='" + comNameDropDownList.SelectedValue + "' ";
            }
            if (!string.IsNullOrEmpty(unitNameDropDownList.SelectedValue))
            {
                parameter = parameter + " and S.UnitId='" + unitNameDropDownList.SelectedValue + "' ";
            }
        }
        
        return parameter;
    }

    protected void viewRptButton_Click(object sender, EventArgs e)
    {
        Session["Param"] = ParameterGenerator();
        PopUp(selectReportDropDownList.SelectedValue);
        Session["Status"] = empStatusDropDownList.SelectedItem.Text;
    }
    protected void selectReportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        divfastival.Visible = false;
        bankcash.Visible = false;
        if (selectReportDropDownList.SelectedValue == "MB" || selectReportDropDownList.SelectedValue == "PFS")
        {
            divmonth.Visible = true;    
        }

        else
        {
            divmonth.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue == "PFEW")
        {
            divempwise.Visible = true;
        }
        else
        {
            divempwise.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue == "FB" || selectReportDropDownList.SelectedValue == "FBDL" || selectReportDropDownList.SelectedValue == "FBP" || selectReportDropDownList.SelectedValue == "FBS" || selectReportDropDownList.SelectedValue == "FBSR")
        {
            divempwise.Visible = false;
            divmonth.Visible = false;
            divfastival.Visible = true;
            bankcash.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue == "EL" || selectReportDropDownList.SelectedValue == "ELDL" || selectReportDropDownList.SelectedValue == "ELP" || selectReportDropDownList.SelectedValue == "ELS" ||selectReportDropDownList.SelectedValue == "ELSR")
        {
            divempwise.Visible = false;
            divmonth.Visible = false;
            divfastival.Visible = false;
            bankcash.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue == "ELPS" )
        {
            divempwise.Visible = true;
            divmonth.Visible = false;
            divfastival.Visible = false;
            bankcash.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue=="FBPS")
        {
            divempwise.Visible = true;
            divmonth.Visible = false;
            divfastival.Visible = true;
            bankcash.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue == "MA" || selectReportDropDownList.SelectedValue == "MAUW")
        {
            divempwise.Visible = false;
            divmonth.Visible = true;
            divfastival.Visible = false;
            bankcash.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue == "PFYE" || selectReportDropDownList.SelectedValue == "MB" || selectReportDropDownList.SelectedValue == "PFS" || selectReportDropDownList.SelectedValue == "PFY" || selectReportDropDownList.SelectedValue == "PFEW")
        {
            status.Visible = true;

        }
        else
        {
            status.Visible = false;
        }
    }
    protected void typeReportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        divempwise.Visible = true;
    }

    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        EmpGeneralInfoBLL aInfoBll=new EmpGeneralInfoBLL();
        aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
}