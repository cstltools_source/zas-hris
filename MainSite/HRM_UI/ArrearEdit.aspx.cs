﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ArrearEdit : System.Web.UI.Page
{
    ArrearBLL _aArrearBLL = new ArrearBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ArrearIdHiddenField.Value = Request.QueryString["ID"];
            EmpArrearLoad(ArrearIdHiddenField.Value);
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Arrear aArrear = new Arrear()
            {
                ArrearId = Convert.ToInt32(ArrearIdHiddenField.Value),
                EmpInfoId = Convert.ToInt32(empIdHiddenField.Value),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value),
                UnitId = Convert.ToInt32(unitIdHiddenField.Value),
                DivisionId = Convert.ToInt32(divIdHiddenField.Value),
                DeptId = Convert.ToInt32(deptIdHiddenField.Value),
                SectionId = Convert.ToInt32(secIdHiddenField.Value),
                DesigId = Convert.ToInt32(desigIdHiddenField.Value),
                GradeId = Convert.ToInt32(empGradeIdHiddenField.Value),
                EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value),
                ArearAmount = Convert.ToDecimal(arreatTextBox.Text),
                Purpose = purposeTextBox.Text,
                
                
            };
           
            if (!_aArrearBLL.UpdateDataForEmpSalBenefit(aArrear))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpArrearLoad(string ArrearId)
    {
        Arrear aArrear = new Arrear();
        aArrear = _aArrearBLL.ArrearEditLoad(ArrearId);
        effectDateTexBox.Text = aArrear.EffectiveDate.ToString();
        arreatTextBox.Text = aArrear.ArearAmount.ToString();
        purposeTextBox.Text = aArrear.Purpose;
        comIdHiddenField.Value= aArrear.CompanyInfoId.ToString();
        unitIdHiddenField.Value= aArrear.UnitId.ToString();
        divIdHiddenField.Value= aArrear.DivisionId.ToString();
        deptIdHiddenField.Value= aArrear.DeptId.ToString();
        secIdHiddenField.Value= aArrear.SectionId.ToString();
        desigIdHiddenField.Value= aArrear.DesigId.ToString();
        empGradeIdHiddenField.Value= aArrear.GradeId.ToString();
        empTypeIdHiddenField.Value= aArrear.EmpTypeId.ToString();
        empIdHiddenField.Value = aArrear.EmpInfoId.ToString();
        
        
        GetEmpMasterCode(empIdHiddenField.Value);
        GetNames(comIdHiddenField.Value,unitIdHiddenField.Value,divIdHiddenField.Value,deptIdHiddenField.Value,desigIdHiddenField.Value,secIdHiddenField.Value,empGradeIdHiddenField.Value,empTypeIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aArrearBLL.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                EmpMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }  
    }

    public void GetNames(string comId,string unitId,string divId,string deptId,string desigId,string secId,string gradeId,string emptId)
    {
        
            DataTable aTableCom = new DataTable();
            DataTable aTableUnit = new DataTable();
            DataTable aTableCDiv = new DataTable();
            DataTable aTableDept = new DataTable();
            DataTable aTableDesig = new DataTable();
            DataTable aTableSec = new DataTable();
            DataTable aTableGrade = new DataTable();
            DataTable aTableType = new DataTable();
            aTableCom = _aArrearBLL.LoadCompanyInfo(comId);
            aTableUnit= _aArrearBLL.LoadUnit(unitId);
            aTableCDiv= _aArrearBLL.Loadivision(divId);
            aTableDept = _aArrearBLL.LoadDepartment(deptId);
            aTableDesig = _aArrearBLL.LoadDesignation(desigId);
            aTableSec = _aArrearBLL.LoadSection(secId);
            aTableGrade = _aArrearBLL.LoadGrade(gradeId);
            aTableType = _aArrearBLL.LoadEmpType(emptId);
            

            if (aTableCom.Rows.Count > 0)
            {
                comNameLabel.Text = aTableCom.Rows[0]["CompanyName"].ToString().Trim();
            }
        if (aTableUnit.Rows.Count > 0)
        {
            unitNameLabel.Text = aTableUnit.Rows[0]["UnitName"].ToString().Trim();
        }

        if (aTableCDiv.Rows.Count > 0)
        {
            divNameLabel.Text = aTableCDiv.Rows[0]["DivName"].ToString().Trim();
        }
        if (aTableDept.Rows.Count > 0)
        {
            deptNameLabel.Text = aTableDept.Rows[0]["DeptName"].ToString().Trim();
        }
        if (aTableDesig.Rows.Count > 0)
        {
            desigNameLabel.Text = aTableDesig.Rows[0]["DesigName"].ToString().Trim();
        }
        if (aTableSec.Rows.Count > 0)
        {
            secNameLabel.Text = aTableSec.Rows[0]["SectionName"].ToString().Trim();
        }
        if (aTableGrade.Rows.Count > 0)
        {
            empGradeLabel.Text = aTableGrade.Rows[0]["GradeName"].ToString().Trim();
        }
        if (aTableType.Rows.Count > 0)
        {
            empTypeLabel.Text = aTableType.Rows[0]["EmpType"].ToString().Trim();
        }

    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void EmpMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aArrearBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                empGradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
}