﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_ArrearView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    ArrearBLL _aArrearBLL = new ArrearBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EmpArrearLoad();
        }
    }

    private void EmpArrearLoad()
    {
        aDataTable = _aArrearBLL.LoadArrear();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        _aArrearBLL.CancelDataMarkBLL(loadGridView,aDataTable);
    }
    
    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        EmpArrearLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("ArrearEntry.aspx");
    }
    private void PopUp(string Id)
    {
        //string url = "ArrearEdit.aspx?ID=" + Id;
        //string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        string url = @"ArrearEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        //string str = "";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
        //ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ArrearId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(ArrearId);
        }

    }
  
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox) loadGridView.Rows[i].Cells[7].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string ArrearId = loadGridView.DataKeys[i][0].ToString();
                _aArrearBLL.DeleteArrearBLL(ArrearId);
            }

        }
        EmpArrearLoad();
    }
}