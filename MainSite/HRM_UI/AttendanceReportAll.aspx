﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="AttendanceReportAll.aspx.cs" Inherits="HRM_UI_AttendanceReportAll" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Attendance Report View </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">All Report </a></li>
                            <li class="breadcrumb-item"><a href="#">Attendance Report View</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Attendance From Date :</label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                            <asp:TextBox ID="fromdtTextBox" runat="server"  class="form-control form-control-sm"></asp:TextBox>
                                              <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                                TargetControlID="fromdtTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton1" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Attendance To Date : </label>
                                        <div class="input-group date pull-left" id="daterangepicker11">
                                            <asp:TextBox ID="todtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton2" CssClass="custom"
                                                TargetControlID="todtTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton2" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-2">
                                    <div class="form-group">
                                        <label> Report Type :</label>
                                        <asp:DropDownList ID="selectreportDropDownList" runat="server"
                                            CssClass="form-control form-control-sm" AutoPostBack="True"
                                            OnSelectedIndexChanged="selectreportDropDownList_SelectedIndexChanged">
                                            <asp:ListItem> Select any one</asp:ListItem>
                                            <asp:ListItem Value="All">All Type</asp:ListItem>
                                            <asp:ListItem Value="EAM">EmpAttMonthly</asp:ListItem>
                                            <asp:ListItem Value="EAMD">EmpAttMonthly(Department)</asp:ListItem>
                                            <asp:ListItem Value="P">Present</asp:ListItem>
                                            <asp:ListItem Value="A">Absent</asp:ListItem>
                                            <asp:ListItem Value="L">Late</asp:ListItem>
                                            <asp:ListItem Value="LV">Leave</asp:ListItem>
                                            <asp:ListItem Value="OD">On Duty</asp:ListItem>
                                            <asp:ListItem Value="MA">Manual Attendance</asp:ListItem>
                                            <asp:ListItem Value="EmpAtt">Employee Wise Att</asp:ListItem>
                                            <asp:ListItem Value="DWS">Dept Wise Summary</asp:ListItem>
                                            <asp:ListItem Value="AS">Attendance Sum</asp:ListItem>
                                            <asp:ListItem Value="EJC">Employee Job Card</asp:ListItem>
                                            <asp:ListItem Value="MDS">Department Monthly Report</asp:ListItem>
                                            <asp:ListItem Value="AEJC">All Employee Report</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                 </div>
                              </div>
                            </div>
                         </div>
                         <div class="card">
                           <div class="card-body">
                            <div id="divEmpCode" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Code :</label>
                                            <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divCommon" runat="server" visible="True">
                                
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Company name :</label>
                                            <asp:DropDownList ID="comNameDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm" OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Unit Name :</label>
                                            <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Division :</label>
                                            <asp:DropDownList ID="divisNamDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="divisNamDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Department :</label>
                                            <asp:DropDownList ID="departmentDropDownList" runat="server"
                                                AutoPostBack="True" CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="departmentDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Section :</label>
                                            <asp:DropDownList ID="sectionDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Grade :</label>
                                            <asp:DropDownList ID="empGradeDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="empGradeDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                   <div class="col-2">
                                        <div class="form-group">
                                            <label>Designation Name :</label>
                                            <asp:DropDownList ID="desigDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Shift :</label>
                                            <asp:DropDownList ID="shiftDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employement Type :</label>
                                            <asp:DropDownList ID="employmentTypeDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewReportButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

