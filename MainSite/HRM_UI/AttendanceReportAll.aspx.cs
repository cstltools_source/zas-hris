﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_AttendanceReportAll : System.Web.UI.Page
{
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        } 
    }

    private void PopUp(string fromDate,string toDate, string reportFlag)
    {
        string url = "../Report_UI/AttendanceReportViewer.aspx?FromDate=" + fromDate + "&ToDate=" + toDate + "&ReportFlag=" + reportFlag + "&empcode=" + empCodeTextBox.Text; ;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);

    }

    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
    protected void divisNamDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
    }
    protected void empGradeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDesignationToDropDownBLL(desigDropDownList, empGradeDropDownList.SelectedValue);
    }
    public void DropDownList()
    {
        aInfoBll.LoadShift(shiftDropDownList);
        //aInfoBll.LoadLineToDropDownBLL(lineDropDownList);
        aInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        aInfoBll.LoadDivisionNameToDropDownBLL(divisNamDropDownList);
        aInfoBll.LoadGradeNameToDropDownBLL(empGradeDropDownList);
        aInfoBll.LoadEmpTypeNameToDropDownBLL(employmentTypeDropDownList);
        
    }

    private string  ParameterGenerator()
    {

        string parameter = "";
        parameter = " and E.EmployeeStatus='Active' ";
        if (selectreportDropDownList.SelectedValue == "EmpAtt" || selectreportDropDownList.SelectedValue == "EJC")
        {
            parameter = parameter + " and E.EmpMasterCode='" + empCodeTextBox.Text + "' ";

            return parameter;
        }
        if(!string.IsNullOrEmpty(comNameDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.CompanyInfoId='" + comNameDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(unitNameDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.UnitId='" + unitNameDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(divisNamDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.DivisionId='" + divisNamDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(departmentDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.DepId='" + departmentDropDownList.SelectedValue + "' ";
        }
        //if (!string.IsNullOrEmpty(lineDropDownList.SelectedValue))
        //{
        //    parameter = parameter + " and E.LineId='" + lineDropDownList.SelectedValue + "' ";
        //}
        if (!string.IsNullOrEmpty(sectionDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.SectionId='" + sectionDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(desigDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.DesigId='" + desigDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(employmentTypeDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.EmpTypeId='" + employmentTypeDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(empGradeDropDownList.SelectedValue))
        {
            parameter = parameter + " and E.EmpGradeId='" + empGradeDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(shiftDropDownList.SelectedValue))
        {
            parameter = parameter + " and A.ShiftId='" + shiftDropDownList.SelectedValue + "' ";
        }
        parameter = parameter + " and  E.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" +
                    HttpContext.Current.Session["UserId"].ToString() + "')";
        return parameter;
    }
    protected void viewReportButton_Click(object sender, EventArgs e)
    {
        AttendanceReportBLL attendanceReport=new AttendanceReportBLL();
        Session["AttenDanceReportParameter"] = ParameterGenerator();
        
        PopUp(fromdtTextBox.Text, todtTextBox.Text, selectreportDropDownList.SelectedValue);
        
    }
    protected void selectreportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (selectreportDropDownList.SelectedValue == "EmpAtt" || selectreportDropDownList.SelectedValue == "EJC")
        {
            divCommon.Visible = false;
            divEmpCode.Visible = true;
        }
        else
        {
            divCommon.Visible = true;
            divEmpCode.Visible = false;
        }
        if (selectreportDropDownList.SelectedValue=="EAM")
        {
            divCommon.Visible = false;
            divEmpCode.Visible = false;  
        }
    }
}