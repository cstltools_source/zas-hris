﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;


public partial class HRM_UI_AutoAttendenceAdjustment : System.Web.UI.Page
{
    AutoAttendanceAdjustmentDal adjustmentDal = new AutoAttendanceAdjustmentDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetStatus();           
        }
    }

    private void SetStatus()
    {
        bonusAppOnRadioButtonList.SelectedIndex = 0;
        empCodeTextBox.ReadOnly = true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (fromtDtTextBox.Text != "" && toDateTextBox.Text != "")
        {
            if (bonusAppOnRadioButtonList.SelectedIndex == 1)
            {
                if (empCodeTextBox.Text != "")
                {
                    ProcessAttendance();
                }
                else
                {
                    ShowMessageBox("Please select employee code !!!");
                }
            }
            else
            {
                ProcessAttendance();
            }
           
        }
        else
        {
            ShowMessageBox("Please select date range !!!");
        }
        
    }

    protected void ShowMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private void ProcessAttendance()
    {

        var aTable = new DataTable();
        int id = 0;

        DateTime fromDate = Convert.ToDateTime(fromtDtTextBox.Text.Trim());
        DateTime toDate = Convert.ToDateTime(toDateTextBox.Text.Trim());


        string pram = " WHERE IsActive = 1";

        if (bonusAppOnRadioButtonList.SelectedIndex == 1)
        {
            pram = pram + " AND EmpMasterCode IN ('" + empCodeTextBox.Text.Trim() + "')";
        }

        aTable = adjustmentDal.LoadEmployeeInfo(pram);

        if (aTable.Rows.Count > 0)
        {
            for (int i = 0; i < aTable.Rows.Count; i++)
            {
                string empCode = "";

                foreach (DateTime day in EachDay(fromDate, toDate))
                {
                    empCode = aTable.Rows[i]["EmpInfoId"].ToString();
                    id = adjustmentDal.SaveAdjustmentInfo(day, empCode);
                }
            }
            
        }

        if (id > 0)
        {
            cancelButton_OnClick(null, null);
            ShowMessageBox("Process completed successfully !!");
        }

    }

    public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
    {
        for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
            yield return day;
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        SetStatus();
        empCodeTextBox.Text = "";
        fromtDtTextBox.Text = "";
        toDateTextBox.Text = "";
    }

    protected void bonusAppOnRadioButtonList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        empCodeTextBox.ReadOnly = bonusAppOnRadioButtonList.SelectedIndex != 1;
    }
}