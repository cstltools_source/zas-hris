﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_CompanyInfoEdit : System.Web.UI.Page
{
    CompanyInfoBLL aCompanyInfoBll = new CompanyInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            companyInfoIdHiddenField.Value = Request.QueryString["ID"];
            CompanyInfoLoad(companyInfoIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (companyNameTextBox.Text == "")
        {
            showMessageBox("Please Input Company Name !!");
            return false;
        }
        if (addressNameTextBox.Text == "")
        {
            showMessageBox("Please Input Address !!");
            return false;
        }
        if (contactNoTextBox.Text == "")
        {
            showMessageBox("Please Input contact Number !!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            CompanyInfo aCompanyInfo = new CompanyInfo()
            {
                CompanyInfoId = Convert.ToInt32(companyInfoIdHiddenField.Value),
                CompanyName = companyNameTextBox.Text,
                Address = addressNameTextBox.Text,
                ContactNo = contactNoTextBox.Text,
                FaxNo = faxNoTextBox.Text,
                Remarks = remarksTextBox.Text

            };
            CompanyInfoBLL aCompanyInfoBll = new CompanyInfoBLL();

            if (!aCompanyInfoBll.UpdateDataForCompanyInfo(aCompanyInfo))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }

        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void CompanyInfoLoad(string comapnyInfoId)
    {
        CompanyInfo aCompanyInfo = new CompanyInfo();
        aCompanyInfo = aCompanyInfoBll.CompanyInfoEditLoad(comapnyInfoId);
        companyNameTextBox.Text = aCompanyInfo.CompanyName;
        addressNameTextBox.Text = aCompanyInfo.Address;
        contactNoTextBox.Text = aCompanyInfo.ContactNo;
        faxNoTextBox.Text = aCompanyInfo.FaxNo;
        remarksTextBox.Text = aCompanyInfo.Remarks;

    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}