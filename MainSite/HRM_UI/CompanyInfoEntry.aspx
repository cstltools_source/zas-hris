﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="CompanyInfoEntry.aspx.cs" Inherits="HRM_UI_CompanyInfoEntry" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        fieldset.for-panel {
            background-color: #fcfcfc;
            border: 1px solid #999;
            padding: 15px 10px;
            background-color: white;
            margin-bottom: 12px;
        }

            fieldset.for-panel legend {
                background-color: #fafafa;
                border: 1px solid #ddd;
                border-radius: 1px;
                font-size: 12px;
                font-weight: bold;
                line-height: 10px;
                margin: inherit;
                padding: 7px;
                width: auto;
                margin-bottom: 0;
                color: black;
            }

            .col-form-label {
                font-size: 1.1em !important;
                font-weight: bold !important;
                vertical-align: baseline !important;
            }

            .form-group {
                margin-bottom: -5px !important;
            }

            .buttons {
                margin: 0 auto;
            }

           
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Company Information Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="CompanyListImageButton_Click" />
                        <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Company Information Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <fieldset class="for-panel">
                                <legend>Company Info</legend>
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right col-form-label"> Name of the company: </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="companynameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                
                                 <div class="form-group row">
                                    <label class="col-sm-2 text-right col-form-label">Contact No: </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="contactTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right col-form-label">FAX No: </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="faxNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-sm-2 text-right col-form-label">Address: </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="companyAddressTextBox" runat="server" class="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="form-group row mt-3">
                                    <label class="col-sm-2 text-right col-form-label">Remarks: </label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="remarksTextBox" runat="server" class="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="form-row buttons mt-10">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-4">
                                        <br />
                                        <br />
                                        <br />
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click1" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                          
                                    </div>
                                    </div>
                                    <div class="col-sm-4"></div>
                                    
                                </div>

                            </fieldset>

                            
                            <br />
                            <br />
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

