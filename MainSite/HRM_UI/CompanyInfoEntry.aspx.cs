﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_CompanyInfoEntry : System.Web.UI.Page
{
    CompanyInfoBLL aCompanyInfoBll = new CompanyInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        companynameTextBox.Text = string.Empty;
        companyAddressTextBox.Text = string.Empty;
        contactTextBox.Text = string.Empty;
        faxNoTextBox.Text = string.Empty;
        remarksTextBox.Text = string.Empty;        
    }
    private bool Validation()
    {
        if (companynameTextBox.Text == "")
        {
            showMessageBox("Please Input Company Name !!");
            return false;
        }
        if (companyAddressTextBox.Text == "")
        {
            showMessageBox("Please Input Address !!");
            return false;
        }
        if (contactTextBox.Text == "")
        {
            showMessageBox("Please Input contact Number !!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click1(object sender, EventArgs e)
    {
        if (Validation())
        {
            CompanyInfo aCompanyInfo = new CompanyInfo()
            {
                CompanyName = companynameTextBox.Text,
                Address = companyAddressTextBox.Text,
                ContactNo = contactTextBox.Text,
                FaxNo = faxNoTextBox.Text,
                Remarks = remarksTextBox.Text,
            };
            if (aCompanyInfoBll.SaveCompanyInfoData(aCompanyInfo))
            {
                showMessageBox("Data Save Successfully Company Code  is :  " + aCompanyInfo.CompanyCode + " And CompanyName is :" + aCompanyInfo.CompanyName);
                Clear();
            }
            else
            {
                showMessageBox("Company Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void CompanyListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("CompanyInfoView.aspx");
    }
   
    protected void rptImageButton_Click(object sender, EventArgs eventArgs)
    {
        string url = "../Report_UI/CompanyInfoReport.aspx";
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}