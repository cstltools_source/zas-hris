﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Data.OleDb;
using Library.DAL.HRM_DAL;

public partial class HRM_UI_DailyAttendanceProcess : System.Web.UI.Page
{
    AttendanceProcessDAL attendanceProcessDal = new AttendanceProcessDAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fromdtTextBox.Text = System.DateTime.Today.AddDays(-1).ToString("dd-MMM-yyyy");
            todtTextBox.Text = System.DateTime.Today.ToString("dd-MMM-yyyy");
        }
    }


    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void viewButton_Click(object sender, EventArgs e)
    {
        System.Threading.Thread.Sleep(1000);

        if (RadioButtonList1.SelectedValue == "AP")
        {
            AttendanceProcess();
        }

        //if (RadioButtonList1.SelectedValue == "DT")
        //{
        //    DataTransfar();
        //    showMessageBox(" Data Transfar is Successfully, Please Start Attendance Process");
        //}
      
    }

    private DataTable AccessDataCollection()
    {

        //string conString =
        // @"Provider=Microsoft.JET.OLEDB.4.0;"
        //+ @"data source=C:\Program Files (x86)\AccessControl\Data\ATT2000.mdb;Persist Security Info=False";
        //string conString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Program Files (x86)\AccessControl\Data\ATT2000.mdb;Persist Security Info=False";
        string conString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=F:\Server\Ahad PC\Backup\attBackup.mdb;Persist Security Info=False";
        //string conString = @"Provider=MSOLAP.4;Persist Security Info=False;Data Source=C:\Program Files (x86)\AccessControl\Data\ATT2000.mdb";


        // create an open the connection          
        //OleDbConnection conn = new OleDbConnection(conString);
        OleDbConnection conn = new OleDbConnection(ConfigurationManager.ConnectionStrings["AccessConnectionString"].ConnectionString);
        conn.Open();

        // create the DataSet
        DataSet ds = new DataSet();

        // create the adapter and fill the DataSet
        // string query = @"Select * from data_card";
        string query = @"SELECT CHECKINOUT.CHECKTIME, USERINFO.Badgenumber From CHECKINOUT INNER JOIN USERINFO ON CHECKINOUT.USERID = USERINFO.USERID";
        // string query = @"SELECT CHECKINOUT.*, USERINFO.* From CHECKINOUT INNER JOIN USERINFO ON CHECKINOUT.USERID = USERINFO.USERID";


        DateTime fromDateTime = Convert.ToDateTime(Convert.ToDateTime(fromdtTextBox.Text.Trim()).ToString("dd-MMM-yyyy"));
        DateTime toDateTime = Convert.ToDateTime(Convert.ToDateTime(todtTextBox.Text.Trim()).ToString("dd-MMM-yyyy"));

        //string query = @"Select * from data_card where CHECKTIME between '28-May-2014' and '29-May-2014'";
        OleDbDataAdapter adapter =
           new OleDbDataAdapter(query, conn);
        adapter.Fill(ds);

        DataTable dtDataAll = new DataTable();
        dtDataAll = ds.Tables[0];
        //int count = dtDataAll.Rows.Count;
        DataRow[] rows = dtDataAll.Select("CHECKTIME >='" + fromDateTime.ToString() + "' AND CHECKTIME <='" + toDateTime.ToString() + "'");

        DataTable dtFilterData = new DataTable();

        dtFilterData.Columns.Add("CHECKTIME");
        dtFilterData.Columns.Add("Badgenumber");
        foreach (DataRow dataRow in rows)
        {
            dtFilterData.ImportRow(dataRow);
        }
        //int count1 = dtFilterData.Rows.Count;

        // close the connection
        conn.Close();
        return dtFilterData;
    }

    private void DataTransfar()
    {
        if (Validation() == true)
        {
            DataTable aTableAccessData = new DataTable();
            aTableAccessData = AccessDataCollection();

            foreach (DataRow row in aTableAccessData.Rows)
            {
                DataInsertInSql(row);

            }
        }
    }
    private void DataInsertInSql(DataRow accessRow)
    {
        //string SQLConnection = @"Data Source=DESKTOP;"
        //   + "Initial Catalog=Northwind;connection timeout=1200;"
        //   + "Integrated Security=false;"
        //   + "User ID=jeff;Password=password";


        //
        //string t_card = accessRow["t_card"].ToString(); 
        //string d_card = accessRow["d_card"].ToString();

        //d_card = d_card.Substring(4, 2) + "/" + d_card.Substring(6, 2) + "/" + d_card.Substring(0, 4);
        //t_card = t_card.Substring(0, 2) + ":" + t_card.Substring(2, 2) + ":" + t_card.Substring(4, 2);


        //string attDate = d_card + " " + t_card;

        string Date = accessRow["CHECKTIME"].ToString();

        DateTime CHECKTIME = Convert.ToDateTime(Date);

        string USERID = accessRow["Badgenumber"].ToString();


        //string SQLConnection = @"Data Source=SERVER;Initial Catalog=CSTLHRM;Integrated Security=false;User ID=sa;Password=123";
        string SQLConnection = @"Data Source=192.168.1.5;Initial Catalog=CSTLHRM;Integrated Security=false;User ID=cstladmin;Password=CSTL**10";


        // create an open the connection          
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SolutionConnectionStringHRDB"].ConnectionString);
        conn.Open();

        // create the DataSet
        DataSet ds = new DataSet();

        // create the adapter and fill the DataSet
        SqlDataAdapter adapter =
           new SqlDataAdapter("Select * from tblProximityRecord where ProximityID='" + USERID + "' and PRTime='" + CHECKTIME + "'", conn);
        adapter.Fill(ds);

        if (ds.Tables[0].Rows.Count == 0)
        {
            DateTime PRDate = Convert.ToDateTime(Convert.ToDateTime(CHECKTIME).ToString("dd-MMM-yyyy"));

            string insertCommand = @"insert into tblProximityRecord (ProximityID,PRDate,PRTime,AttnUp)	values ('" + USERID + "','" + PRDate + "','" + CHECKTIME + "','true')";

            SqlCommand myCommand = new SqlCommand(insertCommand, conn);
            myCommand.ExecuteNonQuery();
        }

        // close the connection
        conn.Close();

    }
    private void AttendanceProcess()
    {
        int count = 0;
        if (Validation() == true)
        {
            if (selectReportDropDownList.SelectedValue == "A")
            {
                //DataTable aTableEmp = attendanceProcessDal.LoadEmployee(fromdtTextBox.Text,todtTextBox.Text);
                DataTable aTableEmp = attendanceProcessDal.LoadEmployee();

                foreach (DataRow row in aTableEmp.Rows)
                {
                    try
                    {
                        count = attendanceProcessDal.AttendanceProcess(Convert.ToDateTime(fromdtTextBox.Text),
                                                Convert.ToDateTime(todtTextBox.Text), row["EmpMasterCode"].ToString(), "HRDB");
                    }
                    catch (Exception)
                    {
                        
                        
                    }
                    
                }
            }
            else
            {
                count = attendanceProcessDal.AttendanceProcess(Convert.ToDateTime(fromdtTextBox.Text),
                    Convert.ToDateTime(todtTextBox.Text), empCodeTextBox.Text, "HRDB");
            }
            showMessageBox(count.ToString() + " Data is Processed");
        }
    }

    private bool Validation()
    {
        if (fromdtTextBox.Text == "")
        {
            showMessageBox("Please Input From Date !!");
            return false;
        }

        if (todtTextBox.Text == "")
        {
            showMessageBox("Please Input From Date !!");
            return false;
        }
        if (RadioButtonList1.SelectedValue == "AP")
        {
            if (selectReportDropDownList.SelectedValue == "S")
            {
                if (empCodeTextBox.Text == "")
                {
                    showMessageBox("Please Input EmpCode !!");
                    return false;
                }
            }
        }

        return true;
    }
   
    protected void selectReportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (selectReportDropDownList.SelectedValue=="A")
        {
            divempcode.Visible = false;
            empCodeTextBox.Text = "";
        }
        if (selectReportDropDownList.SelectedValue=="S")
        {
            divempcode.Visible = true;
        }
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedValue=="AP")
        {
            divAttPro.Visible = true;
        }

        if (RadioButtonList1.SelectedValue == "DT")
        {
            divAttPro.Visible = false;
        }
    }
}