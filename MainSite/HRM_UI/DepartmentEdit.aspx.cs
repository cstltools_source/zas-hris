﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_DepartmentEdit : System.Web.UI.Page
{
    DepartmentBLL aDepartmentBll = new DepartmentBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
            deptIdHiddenField.Value = Request.QueryString["ID"];
            DepartmentLoad(deptIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void LoadDropDown()
    {
       aDepartmentBll.LoadDivisionName(divisionDropDownList);
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (departmentNameTextBox.Text != "" )
        { 
            Department aDepartment = new Department()
            {
                DepartmentId = Convert.ToInt32(deptIdHiddenField.Value),
                DepartmentName = departmentNameTextBox.Text, 
                DeptShortName = DeptShortNameTextBox.Text,
                DivisionId = Convert.ToInt32(divisionDropDownList.SelectedValue),
            };
            DepartmentBLL aDepartmentBll = new DepartmentBLL();

            if (!aDepartmentBll.UpdateDataForDepartment(aDepartment))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully !!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void DepartmentLoad(string departmentId)
    {
        Department aDepartment = new Department();
        aDepartment = aDepartmentBll.DepartmentEditLoad(departmentId);
        departmentNameTextBox.Text = aDepartment.DepartmentName;
        DeptShortNameTextBox.Text = aDepartment.DeptShortName;
        divisionDropDownList.SelectedValue = aDepartment.DivisionId.ToString();
    }


    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}