﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_DepartmentEntry : System.Web.UI.Page
{
    DepartmentBLL aDepartmentBll = new DepartmentBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DivisionName();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    public void DivisionName()
    {
        aDepartmentBll.LoadDivisionName(divisionDropDownList);
    }
    private void Clear()
    {
       DepartmentNameTextBox.Text = string.Empty;
       DeptShortNameTextBox.Text = string.Empty;
       divisionDropDownList.SelectedValue = null;
    }
    private bool Validation()
    {
        if (DepartmentNameTextBox.Text == "")
        {
            showMessageBox("Please Input Department Name!!");
            return false;
        }
        
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Department aDepartment = new Department()
                 {
                    DepartmentName = DepartmentNameTextBox.Text,
                    DeptShortName = DeptShortNameTextBox.Text,
                    DivisionId = Convert.ToInt32(divisionDropDownList.SelectedValue)
                 };
          
            if (aDepartmentBll.SaveDataForDepartment(aDepartment))
            {
                showMessageBox("Data Save Successfully Department Code is :" + aDepartment.DeaprtmentCode + " And Department Name is : " + aDepartment.DepartmentName);
                Clear();
            }
            else
            {
                showMessageBox("Department Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("DepartmentView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}