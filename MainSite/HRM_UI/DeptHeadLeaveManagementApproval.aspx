﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="DeptHeadLeaveManagementApproval.aspx.cs" Inherits="HRM_UI_DeptHeadLeaveManagementApproval" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       
    
    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }
    </style>

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Leave Management Approval</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentNewImageButton_Click" />
                        <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="deptReloadImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Leave Management Approval</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Take action </label>
                                        <asp:RadioButtonList ID="actionRadioButtonList" runat="server" CssClass="custom-control custom-radio"></asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-3">
                                    <label style="color: white;">Submith </label>
                                    <br />
                                    <asp:Button ID="subButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="btnSubmit0_Click" Text="Submit Action" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 380px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark" DataKeyNames="LeaveAvailId,EmpInfoId,DayQty"
                                    OnRowCommand="loadGridView_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                    <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                    <asp:BoundField DataField="DeptName" HeaderText="Department" />
                                    <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                    <asp:BoundField DataField="SectionName" HeaderText="Section" />
                                    <asp:BoundField DataField="AvailLeaveQty" HeaderText="Avail Leave Qty" />
                                    <asp:BoundField DataField="LeaveName" HeaderText="Leave Name" />
                                    <asp:BoundField DataField="LeaveReason" HeaderText="Leave Reason" />
                                    <asp:TemplateField HeaderText="From Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="fromDateTextBox" runat="server" CssClass="form-control form-control-sm" 
                                                Text='<%# Eval("FromDate")%>'></asp:TextBox>
                                            <asp:CalendarExtender ID="fromDate_CalendarExtender" runat="server" 
                                                Format="dd-MMM-yyyy" PopupButtonID="imgFrDate" 
                                                TargetControlID="fromDateTextBox" />
                                            <asp:ImageButton ID="imgFrDate" runat="server" 
                                                AlternateText="Click to show calendar" 
                                                ImageUrl="~/Assets/img/calendar.jpg" TabIndex="4" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="toDateTextBox" runat="server" CssClass="form-control form-control-sm" 
                                                ontextchanged="toDateTextBox_TextChanged" Text='<%# Eval("ToDate")%>' AutoPostBack="True"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                Format="dd-MMM-yyyy" PopupButtonID="imgToDate" 
                                                TargetControlID="toDateTextBox" />
                                            <asp:ImageButton ID="imgToDate" runat="server" 
                                                AlternateText="Click to show calendar" 
                                                ImageUrl="~/Assets/img/calendar.jpg" TabIndex="4" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                                    OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%" class="TableWorkArea">
                    <tr>
                        <td colspan="6" class="TableHeading">
                            Leave Management Approval</td>
                    </tr>
                   <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                    </tr>
                     <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                            Take Action :</td>
                        <td width="20%" class="TDRight">
                            <asp:RadioButtonList ID="actionRadioButtonList" runat="server" 
                                CssClass="inline-rb">
                            </asp:RadioButtonList>
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            <asp:Button ID="btnSubmit0" runat="server" onclick="btnSubmit0_Click" 
                                Text="Submit Action" />
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft" colspan="6">
                            <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" 
                                CssClass="gridview" DataKeyNames="LeaveAvailId,EmpInfoId,DayQty" 
                                onrowcommand="loadGridView_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                    <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                    <asp:BoundField DataField="DeptName" HeaderText="Department" />
                                    <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                    <asp:BoundField DataField="SectionName" HeaderText="Section" />
                                    <asp:BoundField DataField="AvailLeaveQty" HeaderText="Avail Leave Qty" />
                                    <asp:BoundField DataField="LeaveName" HeaderText="Leave Name" />
                                    <asp:BoundField DataField="LeaveReason" HeaderText="Leave Reason" />
                                    <asp:TemplateField HeaderText="From Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="fromDateTextBox" runat="server" CssClass="TextBoxCalander" 
                                                Text='<%# Eval("FromDate")%>'></asp:TextBox>
                                            <asp:CalendarExtender ID="fromDate_CalendarExtender" runat="server" 
                                                Format="dd-MMM-yyyy" PopupButtonID="imgFrDate" 
                                                TargetControlID="fromDateTextBox" />
                                            <asp:ImageButton ID="imgFrDate" runat="server" 
                                                AlternateText="Click to show calendar" 
                                                ImageUrl="~/Images/Calendar_scheduleHS.png" TabIndex="4" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="To Date">
                                        <ItemTemplate>
                                            <asp:TextBox ID="toDateTextBox" runat="server" CssClass="TextBoxCalander" 
                                                ontextchanged="toDateTextBox_TextChanged" Text='<%# Eval("ToDate")%>' AutoPostBack="True"></asp:TextBox>
                                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                Format="dd-MMM-yyyy" PopupButtonID="imgToDate" 
                                                TargetControlID="toDateTextBox" />
                                            <asp:ImageButton ID="imgToDate" runat="server" 
                                                AlternateText="Click to show calendar" 
                                                ImageUrl="~/Images/Calendar_scheduleHS.png" TabIndex="4" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" 
                                                oncheckedchanged="chkSelectAll_CheckedChanged" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft" >
                            &nbsp;
                        </td>
                         <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>--%>

</asp:Content>

