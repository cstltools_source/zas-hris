﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_DeptHeadLeaveManagementApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    LeaveAvailBLL aLeaveAvailBLL = new LeaveAvailBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DeptHeadLeaveLoad();
        }
    }

    public void DeptHeadLeaveLoad()
    {
        string empinfoid = Session["EmpMasterCode"].ToString();
        DataTable dt = aLeaveAvailBLL.DeptHead(empinfoid);
        if(dt.Rows.Count > 0)
        {
            LateApproveLoad(dt.Rows[0]["DeptId"].ToString());
        }
    }
    public void LateApproveLoad(string deptid)
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        LeaveAvailLoadByCondition(filename, userName,deptid);
        aLeaveAvailBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void LeaveAvailLoadByCondition(string pageName, string user,string deptid)
    {
        string ActionStatus = aLeaveAvailBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aLeaveAvailBLL.DeptHeadLoadLeaveApproveViewForApproval(ActionStatus,deptid);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
   
    private void PopUp(string Id)
    {
        string url = "LateAttnApproveEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string LateApproveId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(LateApproveId);
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

       
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[10].FindControl("chkSelect");
                if (ChkBoxHeader.Checked == true)
                {
                    ChkBoxRows.Checked = true;
                }
                else
                {
                    ChkBoxRows.Checked = false;
                }
            }
      
       
       
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        SubmitApproval();
        DeptHeadLeaveLoad();
    }


    private void SubmitApproval()
    {
        List<LeaveAvail> leaveAvailList = new List<LeaveAvail>();
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox) loadGridView.Rows[i].Cells[10].FindControl("chkSelect");
            if (cb.Checked==true)
            {
                TextBox fromDateTextBox = (TextBox)loadGridView.Rows[i].Cells[8].FindControl("fromDateTextBox");
                TextBox toDateTextBox = (TextBox)loadGridView.Rows[i].Cells[9].FindControl("toDateTextBox");
                LeaveAvail aLeave = new LeaveAvail();
                {
                    aLeave.LeaveAvailId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString());
                    aLeave.EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString());
                    aLeave.FromDate = Convert.ToDateTime(fromDateTextBox.Text);
                    aLeave.ToDate = Convert.ToDateTime(toDateTextBox.Text);
                    aLeave.AvailLeaveQty = loadGridView.Rows[i].Cells[5].Text;
                    aLeave.LeaveName = loadGridView.Rows[i].Cells[6].Text;
                    aLeave.ActionStatus = actionRadioButtonList.SelectedItem.Text;
                    aLeave.Status = "Accepted";
                    aLeave.ApprovedBy = Session["LoginName"].ToString();
                    aLeave.ApprovedDate = System.DateTime.Today;

                };
                aLeaveAvailBLL.ApprovalUpdateBLL(aLeave);
                leaveAvailList.Add(aLeave);
            }
        }

        if (leaveAvailList.Count()>0)
        {

            aLeaveAvailBLL.LeaveUpdateIntoAttRecord(leaveAvailList);

        }
    }
    protected void toDateTextBox_TextChanged(object sender, EventArgs e)
    {
        TextBox TextBox = (TextBox)sender;
        GridViewRow currentRow = (GridViewRow)TextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        TextBox fromDateTextBox = (TextBox)loadGridView.Rows[rowindex].Cells[8].FindControl("fromDateTextBox");
        TextBox toDateTextBox = (TextBox)loadGridView.Rows[rowindex].Cells[9].FindControl("toDateTextBox");

        DateTime fromDate = new DateTime();
        DateTime toDate = new DateTime();
        fromDate = Convert.ToDateTime(fromDateTextBox.Text);
        toDate = Convert.ToDateTime(toDateTextBox.Text);
        TimeSpan span = toDate.Subtract(fromDate);
        string day=Convert.ToString(((int)span.TotalDays) + 1);
        if (Convert.ToInt32(loadGridView.DataKeys[rowindex][2].ToString()) < Convert.ToInt32(day))
        {
            showMessageBox("Quantity must be smaller then Total Leave Quantity");
        }
        if (Convert.ToInt32(loadGridView.DataKeys[rowindex][2].ToString()) >= Convert.ToInt32(day))
        {
            loadGridView.Rows[rowindex].Cells[5].Text = day;
        }        

    }
}