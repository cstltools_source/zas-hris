﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="DeptHeadSetupEntry.aspx.cs" Inherits="HRM_UI_DeptHeadSetupEntry" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Department Head Setup Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Department Head Setup Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-2">
                                    <label>Effective Date </label>
                                    <div class="input-group date pull-left" id="daterangepicker1">
                                        <asp:TextBox ID="effectDateTexBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender" runat="server"
                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                            TargetControlID="effectDateTexBox" />
                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                            <span>
                                                <asp:ImageButton ID="ImageButton1" runat="server"
                                                    AlternateText="Click to show calendar"
                                                    ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Master Code </label>
                                        <asp:TextBox ID="EmpMasterCodeTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-1">
                                    <div class="form-group">
                                        <label style="color: white"> Search </label> <br />
                                        <asp:Button ID="searchButton" Text="Search" CssClass="btn btn-sm btn-block btn-info" runat="server" OnClick="searchButton_Click" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Name </label> <br />
                                        <asp:Label ID="empNameTexBox" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="EmpInfoIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Company Name </label> <br />
                                        <asp:Label ID="comNameLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="comIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Unit Name </label> <br />
                                        <asp:Label ID="unitNameLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="unitIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Division Name </label> <br />
                                        <asp:Label ID="divNameLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="divIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department Name </label> <br />
                                        <asp:Label ID="deptNameLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="deptIdHiddenField" runat="server" />
                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Section Name </label> <br />
                                        <asp:Label ID="secNameLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="secIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Designation Name </label> <br />
                                        <asp:Label ID="desigNameLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="desigIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Grade Name </label> <br />
                                        <asp:Label ID="empGradeLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="GradeIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Type Name </label> <br />
                                        <asp:Label ID="empTypeLabel" runat="server" class="custom-control-label" Text=""></asp:Label>
                                        <asp:HiddenField ID="empTypeIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Purpose </label>
                                        <asp:TextBox ID="purposeTextBox" runat="server" CssClass="form-control form-control-sm" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                             <br/>
                            <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

