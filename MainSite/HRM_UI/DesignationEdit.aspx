﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DesignationEdit.aspx.cs" Inherits="HRM_UI_DesignationEdit" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form2" runat="server">

        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
            <div class="page-heading">
                <div class="page-heading__container">
                    <div class="icon"><span class="li-register"></span></div>
                    <span></span>
                    <h1 class="title" style="font-size: 18px; padding-top: 9px;">Department Edit </h1>
                </div>
                <div class="page-heading__container float-right d-none d-sm-block">
                    <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                    <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Department Edit</a></li>

                        </ol>
                    </nav>
            </div>
            <!-- //END PAGE HEADING -->
             <br/>
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Designation Name </label>
                                    <asp:TextBox ID="designatioNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    <asp:HiddenField ID="desigIdHiddenField" runat="server" />
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Holiday Duty Bill </label>
                                    <asp:TextBox ID="hdutybillTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                          <div class="col-3">
                                <div class="form-group">
                                    <label>Night Bill </label>
                                    <asp:TextBox ID="nightbillTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Over Time Rate </label>
                                    <asp:TextBox ID="otRateTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                           <div class="col-3">
                                <div class="form-group">
                                    <label>Tiffin Allowance </label>
                                    <asp:TextBox ID="tiffinTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <asp:Button ID="updateButton" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="updateButton_Click" />
                                    <asp:Button ID="cancelButton" Text="Close"  CssClass="btn btn-sm warning" runat="server" OnClick="closeButton_Click" BackColor="#FF9900" />
                                    <asp:Button ID="deleteButton" Text="Delete" CssClass="btn btn-primary" runat="server" OnClick="deleteButton_Click" />
                                    
                                    <asp:ModalPopupExtender ID="pnlModal_ModalPopupExtender" runat="server"
                                        BackgroundCssClass="modal fade" CancelControlID="" DropShadow="true"
                                        Enabled="True" OkControlID="" PopupControlID="pnlModal"
                                        TargetControlID="deleteButton">
                                    </asp:ModalPopupExtender>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <asp:Panel ID="pnlModal" runat="server" CssClass="modalPopup" Style="display: none;">           
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Confirm Delete</h5>
                            </div>
                            <div class="modal-body"> Are you Sure? You want to delete this data? </div>
                            <div class="modal-footer">
                                <asp:Button ID="yesButton" Text="Yes" CssClass="btn btn-secondary" runat="server" OnClick="yesButton_Click"/>                         
                                 <asp:Button ID="noButton" Text="No" CssClass="btn btn-primary" runat="server"/>
                            </div>
                        </div>
                    </div>
            </asp:Panel>

        </div>
    </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>