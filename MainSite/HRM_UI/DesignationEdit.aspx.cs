﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_DesignationEdit : System.Web.UI.Page
{
    DesignationBLL aDesignationBll = new DesignationBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            desigIdHiddenField.Value = Request.QueryString["ID"];
            DesigntionLoad(desigIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (designatioNameTextBox.Text != "" )
        {
            Designation aDesignation = new Designation()
            {
                DesignationId = Convert.ToInt32(desigIdHiddenField.Value),
                DesignationName = designatioNameTextBox.Text,
                HDutyBill = Convert.ToDecimal(hdutybillTextBox.Text),
                NightBill = Convert.ToDecimal(nightbillTextBox.Text),
                OTRate = Convert.ToDecimal(otRateTextBox.Text),
                TiffinAllow = Convert.ToDecimal(tiffinTextBox.Text),
            };
            
            if (aDesignationBll.UpdateDataForDesignation(aDesignation))
            {
                showMessageBox("Data Update Successfully !!! Please Reload");
            }
            else
            {
                showMessageBox("Data Not Update !!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void DesigntionLoad(string designationId)
    {
        Designation aDesignation = new Designation();
        aDesignation = aDesignationBll.DesignationEditLoad(designationId);
        designatioNameTextBox.Text = aDesignation.DesignationName;
        hdutybillTextBox.Text = aDesignation.HDutyBill.ToString();
        otRateTextBox.Text = aDesignation.OTRate.ToString();
        nightbillTextBox.Text = aDesignation.NightBill.ToString();
        tiffinTextBox.Text = aDesignation.TiffinAllow.ToString();
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        if (desigIdHiddenField.Value!="")
        {
            if (aDesignationBll.DeleteDataForDesignation(desigIdHiddenField.Value))
            {
                showMessageBox("Data Deleted Successfully !!!");
            }
            else
            {
                showMessageBox("Data Not Deleted !!!");
            }
        }
        
    }

    protected void deleteButton_Click(object sender, EventArgs e)
    {

    }
}