﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_DesignationEntry : System.Web.UI.Page
{
    DesignationBLL aDesignationBll = new DesignationBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetDefultInfo();
        }
    }

    private void SetDefultInfo()
    {
        hdutybillTextBox.Text = "0";
        otRateTextBox.Text = "0";
        tiffinTextBox.Text = "0";
        nightbillTextBox.Text = "0";
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        designationTextBox.Text = string.Empty;
        SetDefultInfo();
        
    }

    private bool Validation()
    {
        if (designationTextBox.Text == "")
        {
            showMessageBox("Please Input Designation Name!!");
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Designation aDesignation = new Designation()
            {
                DesignationName = designationTextBox.Text,
                HDutyBill = Convert.ToDecimal(hdutybillTextBox.Text),
                NightBill = Convert.ToDecimal(nightbillTextBox.Text),
                OTRate = Convert.ToDecimal(otRateTextBox.Text),
                TiffinAllow = Convert.ToDecimal(tiffinTextBox.Text),
            };
            if (aDesignationBll.SaveDataForDesignation(aDesignation))
            {
                showMessageBox("Data Save Successfully Designation Code is :" + aDesignation.DesignaitonCode + " And Designation Name is : " + aDesignation.DesignationName);
                Clear();
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("DesignationView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}