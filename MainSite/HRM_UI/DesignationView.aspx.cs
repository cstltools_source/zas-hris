﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_DesignationView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    DesignationBLL aDesignationBll = new DesignationBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DesignationLoad();
        }

    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void DesignationLoad()
    {
        aDataTable = aDesignationBll.LoadDesignationView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    protected void desigNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("DesignationEntry.aspx");
    }
    protected void desigReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        DesignationLoad();
    }

    private void PopUp(string Id)
    {
        string url = "DesignationEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    

    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string designationId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(designationId);

           
        }
        if (e.CommandName == "RemoveData")
        {
            int Deleteindex = Convert.ToInt32(e.CommandArgument);
            string DeletedesignationId = loadGridView.DataKeys[Deleteindex][0].ToString();
            if (aDesignationBll.DeleteDataForDesignation(DeletedesignationId))
            {
                DesignationLoad();
                showMessageBox("Data Deleted Successfully !!!");
            }
            else
            {
                showMessageBox("Data Not Deleted !!!");
            }
        }
    }
  
}