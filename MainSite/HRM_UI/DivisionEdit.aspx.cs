﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_DivisionEdit : System.Web.UI.Page
{
    DivisionBLL aDivisionBll = new DivisionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divisionIdHiddenField.Value = Request.QueryString["ID"];
            DivisionLoad(divisionIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (divisionNameTextBox.Text == "")
        {
            showMessageBox("Please Input Division Name!!");
            return false;
        }
        if (DivisionShortNameTexBox.Text == "")
        {
            showMessageBox("Please Input  Division Short Name!!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        { 
            Division aDivision = new Division()
            {
                DivisionId = Convert.ToInt32(divisionIdHiddenField.Value),
                DivName = divisionNameTextBox.Text,
                DivShortName = DivisionShortNameTexBox.Text
            };
           
            if (!aDivisionBll.UpdateaDataforDivision(aDivision))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void DivisionLoad(string divisionId)
    {
        Division aDivision = new Division();
        aDivision = aDivisionBll.DivisionEditLoad(divisionId);
        divisionNameTextBox.Text = aDivision.DivName;
        DivisionShortNameTexBox.Text = aDivision.DivShortName;
    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}