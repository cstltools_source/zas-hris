﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_DepartmentEntry : System.Web.UI.Page
{
    DivisionBLL aDivisionBll = new DivisionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       DivisionNameTextBox.Text = string.Empty;
       DivisionShortNameTexBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (DivisionNameTextBox.Text == "")
        {
            showMessageBox("Please Input Division Name!!");
            return false;
        }
        if (DivisionShortNameTexBox.Text == "")
        {
            showMessageBox("Please Input  Division Short Name!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Division aDivision = new Division()
            {
                DivName = DivisionNameTextBox.Text,
                DivShortName = DivisionShortNameTexBox.Text
            };
            if (aDivisionBll.SaveDataForDivision(aDivision))
            {
                showMessageBox("Data Save Successfully Division Code is :" + aDivision.DivCode + " And Division Name is : " + aDivision.DivName);
                Clear();
            }
            else
            {
                showMessageBox("Division Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("DivisionView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}