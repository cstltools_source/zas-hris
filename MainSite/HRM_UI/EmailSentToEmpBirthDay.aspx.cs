﻿using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_EmailSentToLateEmp : System.Web.UI.Page
{
    ProbationBLL aProbationBll = new ProbationBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
            //DropDownList();
        }      
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    public void LoadData()
    {
        DataTable dt = aProbationBll.LoadEmpBirthData(DateTime.Now.Day.ToString(), DateTime.Now.Month.ToString());
        loadGridView.DataSource = dt;
        loadGridView.DataBind();
    }
    private void Clear()
    {
        
        loadGridView.DataSource = string.Empty;
        loadGridView.DataBind();
    }
    private bool Validation()
    {
        //if (DateTextBox.Text == "")
        //{
        //    showMessageBox("Please Input DateTime !!");
        //    return false;
        //}
        //if (divisionDropDownList.SelectedValue == "")
        //{
        //    //showMessageBox("Please Select Division !!");
        //    return false;
        //}
        //if (departmentDropDownList.SelectedValue == "")
        //{
        //    //showMessageBox("Please Select Department !!");
        //    return false;
        //}
        //if (sectionDropDownList.SelectedValue == "")
        //{
        //    //showMessageBox("Please Select Section !!");
        //    return false;
        //}
        return true;
    }
    public void SendMail()
    {
        for (int j = 0; j < loadGridView.Rows.Count; j++)
        {
            CheckBox cb = (CheckBox) loadGridView.Rows[j].Cells[0].FindControl("chkSelect");
            if (cb.Checked == true)
            {
                // Gmail Address from where you send the mail
                var fromAddress = "aptechdesignsltd@gmail.com";
                // any address where the email will be sending
                  var toAddress = loadGridView.Rows[j].Cells[4].Text;
                //Password of your gmail address
                const string fromPassword = "adomain@#";
                // Passing the values and make a email formate to display
                string subject = "Happy BirthDay";
                string body = "May Almighty Allah  continue to bless & may you see many more. Happy Birthday!";
               
                // smtp settings
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                // Passing values to smtp object
                smtp.Send(fromAddress, "nazrul31.islam@gmail.com", subject, body);
            }
        }
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            LoadData();

        }
        
    }
    protected void mailButton_Click(object sender, EventArgs e)
    {
        try
        {
            SendMail();
            showMessageBox("Your Mail is Sent Successfully");
            Clear();
        }
        catch (Exception) { }
    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[5].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }
    
}