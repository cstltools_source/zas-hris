﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="EmailSentToLateEmp.aspx.cs" Inherits="HRM_UI_EmailSentToLateEmp" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Email Sent To Late Employee </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="addNewButton" Text="View Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="jobViewImageButton_Click" />--%>
                        <%-- <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="gradeReloadImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Email Sent To Late Employee</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>From Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                            <asp:TextBox ID="DateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                                TargetControlID="DateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton1" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-2">
                                    <div class="form-group">
                                        <label>Division </label>
                                        <asp:DropDownList ID="divisionDropDownList" runat="server"
                                            class="form-control form-control-sm" AutoPostBack="True" OnSelectedIndexChanged="divisionDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department </label>
                                        <asp:DropDownList ID="departmentDropDownList" runat="server"
                                            class="form-control form-control-sm" AutoPostBack="True"
                                            OnSelectedIndexChanged="departmentDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                               

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Section </label>
                                        <asp:DropDownList ID="sectionDropDownList" runat="server" class="form-control form-control-sm">
                                        </asp:DropDownList>
                                    </div>
                                </div>


                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Send Mail" CssClass="btn btn-sm btn-info" runat="server" OnClick="mailButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Search" CssClass="btn btn-sm warning" runat="server" OnClick="searchButton_Click" BackColor="#FF9900" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 380px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="EmployeeCode" HeaderText="Employee Code" />
                                        <asp:BoundField DataField="EmployeeName" HeaderText="Employee Name" />
                                        <asp:BoundField DataField="Email" HeaderText="Email" />
                                        <asp:BoundField DataField="ShiftStart" HeaderText="Shift Start" />
                                        <asp:BoundField DataField="InTime" HeaderText="In Time" />
                                        <asp:BoundField DataField="LateTime" HeaderText="LateTime" />
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                                    OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

