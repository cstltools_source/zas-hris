﻿using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_EmailSentToLateEmp : System.Web.UI.Page
{
    EmailSentToLateEmpBLL aEmailSentToLateEmpBll = new EmailSentToLateEmpBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        }      
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    public void DropDownList()
    {
        aEmailSentToLateEmpBll.LoadDivisionNameToDropDownBLL(divisionDropDownList);
    }
    
    private void Clear()
    {
        DateTextBox.Text = string.Empty;
        divisionDropDownList.SelectedValue = string.Empty;
        departmentDropDownList.SelectedValue = string.Empty;
        sectionDropDownList.SelectedValue = string.Empty;
        loadGridView.DataSource = string.Empty;
        loadGridView.DataBind();
    }
    private bool Validation()
    {
        if (DateTextBox.Text == "")
        {
            showMessageBox("Please Input DateTime !!");
            return false;
        }
        if (divisionDropDownList.SelectedValue == "")
        {
            //showMessageBox("Please Select Division !!");
            return false;
        }
        if (departmentDropDownList.SelectedValue == "")
        {
            //showMessageBox("Please Select Department !!");
            return false;
        }
        if (sectionDropDownList.SelectedValue == "")
        {
            //showMessageBox("Please Select Section !!");
            return false;
        }
        return true;
    }
    public void SendMail()
    {
        for (int j = 0; j < loadGridView.Rows.Count; j++)
        {
            CheckBox cb = (CheckBox) loadGridView.Rows[j].Cells[0].FindControl("chkSelect");
            if (cb.Checked == true)
            {
                // Gmail Address from where you send the mail
                var fromAddress = "aptechdesignsltd@gmail.com";
                // any address where the email will be sending
                  var toAddress = loadGridView.Rows[j].Cells[3].Text;
                //Password of your gmail address
                const string fromPassword = "adomain@#";
                // Passing the values and make a email formate to display
                string subject = "Late Attandance";
                string body = "You have been Late on " + DateTextBox.Text + "\n";
                body += "Shift Start: " + loadGridView.Rows[j].Cells[3].Text +"\n";
                body += "In Time: " + loadGridView.Rows[j].Cells[4].Text + "\n";
                body += "Total Late: " + loadGridView.Rows[j].Cells[5].Text +"min"+ "\n";
                // smtp settings
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                // Passing values to smtp object
                smtp.Send(fromAddress, toAddress, subject, body);
            }
        }
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            DataTable aDataTable1 = new DataTable();
            aDataTable1 = aEmailSentToLateEmpBll.LoadEmployeeBLL(divisionDropDownList.SelectedValue, departmentDropDownList.SelectedValue, sectionDropDownList.SelectedValue);

            DataTable aDataTable2 = new DataTable();
            aDataTable2 = aEmailSentToLateEmpBll.LoadEmployeeATTBLL(Convert.ToDateTime(DateTextBox.Text));

            var JoinResult = (from p in aDataTable1.AsEnumerable()
                              join t in aDataTable2.AsEnumerable()
                              on p.Field<int>("EmpId") equals t.Field<int>("EmpId")
                              select new
                              {
                                  EmployeeCode = p.Field<string>("EmpMasterCode"),
                                  EmployeeName = p.Field<string>("EmpName"),
                                  Email = p.Field<string>("Email"),
                                  ShiftStart = t.Field<string>("ShiftStart"),
                                  InTime = t.Field<string>("InTime"),
                                  LateTime = t.Field<string>("LateTime")
                              }).ToList();

            loadGridView.DataSource = JoinResult;
            loadGridView.DataBind();
        }
        else
        {
            if (DateTextBox.Text != "")
            {
                DataTable aDataTable3 = new DataTable();
                aDataTable3 = aEmailSentToLateEmpBll.LoadEmployeeATTAllBLL(Convert.ToDateTime(DateTextBox.Text));
                loadGridView.DataSource = aDataTable3;
                loadGridView.DataBind();
            }
            else
            {
                showMessageBox("Please Input DateTime !!");
            }
           
        }
    }
    protected void mailButton_Click(object sender, EventArgs e)
    {
        try
        {
            SendMail();
            showMessageBox("Your Mail is Sent Successfully");
            Clear();
        }
        catch (Exception ex) { }
    }
    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aEmailSentToLateEmpBll.LoadDepartmentToDropDownBLL(departmentDropDownList, divisionDropDownList.SelectedValue);
    }
    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aEmailSentToLateEmpBll.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[6].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }
    
}