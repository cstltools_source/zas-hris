﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_DepartmentEntry : System.Web.UI.Page
{
    EmpAcademyInfoBLL aEmpAcademyInfoBll = new EmpAcademyInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       areaStudayTextBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (areaStudayTextBox.Text == "")
        {
            showMessageBox("Please Input AreaOfStaudy Name!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            AreaOfStaudy areaOfStaudy = new AreaOfStaudy()
            {
                AreaofStudy = areaStudayTextBox.Text,
            };
            if (aEmpAcademyInfoBll.SaveDataForAcademicInfo(areaOfStaudy))
            {
                showMessageBox("Data Save Successfully AreaOfStaudy ");
                Clear();
            }
            else
            {
                showMessageBox("AreaOfStaudy Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("DivisionView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}