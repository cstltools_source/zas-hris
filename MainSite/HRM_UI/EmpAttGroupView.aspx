﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="EmpAttGroupView.aspx.cs" Inherits="HRM_UI_EmpAttGroupView" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;"> Manual Attandance Group Arrange </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="addNewButton" Text="View Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="jobViewImageButton_Click" />--%>
                        <%-- <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="gradeReloadImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Manual Attandance Group Arrange</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Group </label>
                                        <asp:DropDownList ID="groupDropDownList" runat="server" CssClass="form-control form-control-sm"
                                            AutoPostBack="True"
                                            OnSelectedIndexChanged="divisionDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Division Name </label>
                                        <asp:DropDownList ID="divisionDropDownList" runat="server" CssClass="form-control form-control-sm"
                                            AutoPostBack="True"
                                            OnSelectedIndexChanged="divisionDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department Name </label>
                                        <asp:DropDownList ID="deptDropDownList" runat="server" CssClass="form-control form-control-sm"
                                            AutoPostBack="True"
                                            OnSelectedIndexChanged="deptDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12">
                                    <div id="gridContainer1" style="height: 350px; overflow: auto; width: auto;">
                                        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered text-center thead-dark" DataKeyNames="EmpInfoId">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SL">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                                <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                                <asp:BoundField DataField="DeptName" HeaderText="Department" />
                                                <asp:TemplateField HeaderText="Delete">
                                                    <HeaderTemplate>
                                                        <asp:ImageButton ID="deleteImageButton" runat="server"
                                                           ImageUrl="~/Assets/delete-icon.png" OnClick="yesButton_Click" OnClientClick="return confirm(' Do you want to Delete data  ?');" />
                                                        <%--<asp:ModalPopupExtender ID="pnlModal_ModalPopupExtender" runat="server"
                                                            BackgroundCssClass="modalBackground" CancelControlID="" DropShadow="true"
                                                            DynamicServicePath="" Enabled="True" OkControlID="" PopupControlID="pnlModal"
                                                            TargetControlID="deleteImageButton">
                                                        </asp:ModalPopupExtender>
                                                        <asp:Panel ID="pnlModal" runat="server" CssClass="modalPopup"
                                                            Style="display: none;">
                                                            <div id="PopupHeader" class="popup_Titlebar">
                                                                <div class="TitlebarLeft">
                                                                    Confirm Message
                                                                </div>
                                                                <div class="TitlebarRight">
                                                                    <asp:ImageButton ID="ImageButton1" runat="server"
                                                                        ImageUrl="../css/Images/close.jpg" />
                                                                </div>
                                                            </div>
                                                            <div align="center" class="popup_Body">
                                                                <div class="mainLeft ">
                                                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/css/Images/question.png"
                                                                        Width="30px" />
                                                                </div>
                                                                <div class="mainRight">
                                                                    <p align="center">
                                                                        Are you want to Delete data ?
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div align="center" class="popup_Buttons">
                                                                <div class="right_button">
                                                                    <asp:Button ID="yesButton" runat="server" BackColor="#1E90FF" Height="30px"
                                                                        OnClick="yesButton_Click" Text="Yes" Width="60px" />
                                                                </div>
                                                                <div class="left_button">
                                                                    <asp:Button ID="noButton" runat="server" BackColor="red" Height="30px"
                                                                        Text="No" Width="60px" />
                                                                </div>
                                                            </div>
                                                        </asp:Panel>--%>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkDelete" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

