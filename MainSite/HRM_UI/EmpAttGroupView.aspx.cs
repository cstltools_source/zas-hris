﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_EmpAttGroupView : System.Web.UI.Page
{
    ManualAttEmpGroupArrangeBLL aGroupBll = new ManualAttEmpGroupArrangeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
        }
    }
    private void LoadDropDown()
    {
        aGroupBll.LoadGroup(groupDropDownList);
        aGroupBll.LoadDivisionName(divisionDropDownList);
    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");


        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }
    private void DepartmentWiseLoad()
    {
        DataTable aDataTable = new DataTable();
        aDataTable = aGroupBll.LoadGroupEmp(deptDropDownList.SelectedValue,groupDropDownList.SelectedValue);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aGroupBll.LoadDepartmentName(deptDropDownList, divisionDropDownList.SelectedValue);
    }
    protected void deptDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        DepartmentWiseLoad();
    }

    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string Id = loadGridView.DataKeys[i][0].ToString();
                Group aGroup=new Group();
                aGroup.GroupId = Convert.ToInt32(groupDropDownList.SelectedValue);
                aGroup.EmpId = Convert.ToInt32(Id);
                aGroupBll.DeleteGroup(aGroup);
            }
            
        }
        DepartmentWiseLoad();

    }

    protected void companyInfoNewImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("ManualAttEmpGroupArrange.aspx");
    }
}