﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_EmpEduInstituteEntry : System.Web.UI.Page
{
    EmpAcademyInfoBLL aEmpAcademyInfoBll = new EmpAcademyInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       instituteNameTextBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (instituteNameTextBox.Text == "")
        {
            showMessageBox("Please Input Division Name!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmpEduInstitute aEduInstitute = new EmpEduInstitute()
            {
                EduInstituteName = instituteNameTextBox.Text,
            };
            if (aEmpAcademyInfoBll.SaveEmpEduInstitute(aEduInstitute))
            {
                showMessageBox("Data Save Successfully");
                Clear();
            }
            else
            {
                showMessageBox("Institute Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("DivisionView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}