﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_EmpGenInfView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    EmpGeneralInfoBLL aGeneralInfoBll = new EmpGeneralInfoBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //EmpGenerInfoLoald();
            DropDownlist();
        }
    }
    public void DropDownlist()
    {
        aGeneralInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        aGeneralInfoBll.LoadDivisionNameToDropDownBLL(divisionDropDownList);   
    }
    private void EmpGenerInfoLoald()
    {
        aDataTable = aGeneralInfoBll.LoadEmpGeneralInfo();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string EmpInfoId = loadGridView.DataKeys[rowindex][0].ToString();
            Response.Redirect("EmployeeEdit.aspx?id="+EmpInfoId);
        }
    }
    private void PopUp(string Id)
    {
        string url = "EmpGeneralEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void companyInfoNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("EmpGeneralInfo.aspx");
    }
    protected void companyinfoReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        if (comNameDropDownList.SelectedValue != "")
        {
            aDataTable = aGeneralInfoBll.LoadEmpGeneralInfoDeptWise(deptDropDownList.SelectedValue, comNameDropDownList.SelectedValue);
            loadGridView.DataSource = aDataTable;
            loadGridView.DataBind();
        }
        else
        {
            showMessageBox("Please Select a company !!");
        }
        aGeneralInfoBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }
    protected void deptDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (comNameDropDownList.SelectedValue != "")
        {
            aDataTable = aGeneralInfoBll.LoadEmpGeneralInfoDeptWise(deptDropDownList.SelectedValue,comNameDropDownList.SelectedValue);
            loadGridView.DataSource = aDataTable;
            loadGridView.DataBind();
        }
        else
        {
            showMessageBox("Please Select a company !!");
        }
     
        aGeneralInfoBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aGeneralInfoBll.LoadDepartmentToDropDownBLL(deptDropDownList,divisionDropDownList.SelectedValue);
    }
    protected void insertButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmpImageInsert.aspx");
    }


    protected void comNameDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        deptDropDownList_SelectedIndexChanged(null, null);
    }
}