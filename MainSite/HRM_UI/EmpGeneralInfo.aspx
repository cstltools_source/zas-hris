﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="EmpGeneralInfo.aspx.cs" Inherits="HRM_UI_EmpGeneralInfo" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        .datetextbox {
            width: 100%;
            border: 1px solid #ced4da;
            border-color: #d7dde3;
            background: #fff;
            height: 30px !important;
        }

        .radioButtonList label {
            display: inline;
        }

        .btn-addToList {
            background-color: #F57F17;
            color: white;
        }

        .buttonFont {
            font-size: 13px;
            font-weight: bold;
        }
    </style>

    <script>
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(InIEvent);
        </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content" id="content">

        <!-- PAGE HEADING -->
        <div class="page-heading">
            <div class="page-heading__container">
                <div class="icon"><span class="li-register"></span></div>
                <span></span>
                <h1 class="title" style="font-size: 18px; padding-top: 9px;">Employee Information Entry</h1>
            </div>
            <div class="page-heading__container float-right d-none d-sm-block">
                <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="empViewImageButton_Click" />
                <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
            </div>
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">New Employee Related </a></li>
                    <li class="breadcrumb-item"><a href="#">Employee Information Entry</a></li>

                </ol>
            </nav>
        </div>
        <!-- //END PAGE HEADING -->

        <div class="container-fluid">
            <div class="card">
                <div class="card-body">

                    <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                    <%--<h4 id="rw-fw-default">Default example</h4>
                            <p class="subtitle margin-bottom-20">Basic horizontal example of form wizard plugin.</p>--%>

                    <div id="wizard1" class="wizard">
                        <ul class="steps_4">
                            <li><a href="#step-1">
                                <label class="stepNumber">1</label><span class="stepDesc">Step 1<br>
                                    <small>General Information </small></span></a></li>
                            <li><a href="#step-2">
                                <label class="stepNumber">2</label><span class="stepDesc">Step 2<br>
                                    <small>Employment Information</small></span></a></li>
                            <li><a href="#step-3">
                                <label class="stepNumber">3</label><span class="stepDesc">Step 3<br>
                                    <small>Education Information</small></span></a></li>
                            <li><a href="#step-4">
                                <label class="stepNumber">4</label><span class="stepDesc">Step 4<br>
                                    <small>Weekly Holiday</small></span></a></li>
                        </ul>


                        <div id="step-1">
                            <h5 class="StepTitle" style="background-color: powderblue">General Information</h5>
                            <hr />
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Is Same Data </label>
                                                <br />
                                                <asp:CheckBox ID="issameCheckBox" runat="server" />
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <span style="color: red;">*</span>
                                                <div class="input-group date pull-left" id="daterangepicker11">
                                                    <asp:TextBox ID="entryDateTextBox" runat="server" CssClass="form-control form-control-sm" CausesValidation="true" Enabled="False"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton2" CssClass="custom"
                                                        TargetControlID="entryDateTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton2" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Employee Name </label>
                                                <span style="color: red;">*</span>
                                                <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Short Name </label>
                                                <%--<span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="shortNameTextBox" runat="server"
                                                    CssClass="form-control form-control-sm" OnTextChanged="shortNameTextBox_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Father Name </label>
                                                <%--<span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="fatherNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Mother Name </label>
                                                <%--<span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="motherNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <%--Row Number 2--%>

                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Date of birth </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <br />
                                                <asp:DropDownList ID="dayDropDownList" runat="server" Width="45px" CssClass="datetextbox">
                                                    <asp:ListItem Selected="True" Value="DD">DD</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="monthDropDownList" runat="server" Width="47px" CssClass="datetextbox">
                                                    <asp:ListItem Selected="True" Value="MM">MM</asp:ListItem>
                                                    <asp:ListItem Value="01">Jan</asp:ListItem>
                                                    <asp:ListItem Value="02">Feb</asp:ListItem>
                                                    <asp:ListItem Value="03">Mar</asp:ListItem>
                                                    <asp:ListItem Value="04">Apr</asp:ListItem>
                                                    <asp:ListItem Value="05">May</asp:ListItem>
                                                    <asp:ListItem Value="06">Jun</asp:ListItem>
                                                    <asp:ListItem Value="07">Jul</asp:ListItem>
                                                    <asp:ListItem Value="08">Aug</asp:ListItem>
                                                    <asp:ListItem Value="09">Sep</asp:ListItem>
                                                    <asp:ListItem Value="10">Oct</asp:ListItem>
                                                    <asp:ListItem Value="11">Nov</asp:ListItem>
                                                    <asp:ListItem Value="12">Dec</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="yearDropDownList" runat="server" AutoPostBack="True"
                                                    OnSelectedIndexChanged="yearDropDownList_SelectedIndexChanged" CssClass="datetextbox"
                                                    Width="65px">
                                                    <asp:ListItem Selected="True" Value="YYYY">YYYY</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Age </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:TextBox ID="ageTextBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Place of Birth </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="placeOfBirthTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Gender </label>
                                                &nbsp; <span style="color: red;"></span>
                                                <asp:DropDownList ID="genderDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem>Select any one</asp:ListItem>
                                                    <asp:ListItem>Male</asp:ListItem>
                                                    <asp:ListItem>Female</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Religion </label>
                                                &nbsp; <span style="color: red;"></span>
                                                <asp:DropDownList ID="religionDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem Value="1">Select any one</asp:ListItem>
                                                    <asp:ListItem>Islam</asp:ListItem>
                                                    <asp:ListItem>Hindu</asp:ListItem>
                                                    <asp:ListItem>Budhha</asp:ListItem>
                                                    <asp:ListItem>Chirstan</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Nationality </label>
                                                &nbsp; <span style="color: red;"></span>
                                                <asp:DropDownList ID="nationalityDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem Value="">Select any one</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Bangladeshi</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Indian</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Indonesian</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Malaysian</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Nepali</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Nepali</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Pakistani</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">Spanish</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">SriLankan</asp:ListItem>
                                                    <asp:ListItem Value="Bangladeshi">SaudiArabian</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>


                                    <%--Row Number 3--%>

                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Marital Status  </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:DropDownList ID="maritalStatusDropDownList" runat="server"
                                                    CssClass="form-control form-control-sm">
                                                    <asp:ListItem>Select any one</asp:ListItem>
                                                    <asp:ListItem>Married</asp:ListItem>
                                                    <asp:ListItem>Unmarried</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Blood Group </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:DropDownList ID="bloodGroupDropDownList" runat="server"
                                                    CssClass="form-control form-control-sm">
                                                    <asp:ListItem>Select any one</asp:ListItem>
                                                    <asp:ListItem>A+</asp:ListItem>
                                                    <asp:ListItem>A-</asp:ListItem>
                                                    <asp:ListItem>AB+</asp:ListItem>
                                                    <asp:ListItem>AB-</asp:ListItem>
                                                    <asp:ListItem>B+</asp:ListItem>
                                                    <asp:ListItem>B-</asp:ListItem>
                                                    <asp:ListItem>O+</asp:ListItem>
                                                    <asp:ListItem>O-</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Medical Information </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:DropDownList ID="medicalDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem Value="1">Select any one</asp:ListItem>
                                                    <asp:ListItem>Good</asp:ListItem>
                                                    <asp:ListItem>Fair</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Mobile No  </label>
                                                <%--&nbsp; <span style="color: red;"></span>--%>
                                                <asp:TextBox ID="mobileNoTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Phone No </label>
                                                <asp:TextBox ID="phoneNoTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>E-mail  </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="emailTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ControlToValidate="emailTextBox"
                                                    ErrorMessage="Please Give a Valid Email Address" Font-Bold="True"
                                                    ForeColor="#FF3300"
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                    </div>


                                    <%--Row Number 4--%>

                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Present Address </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="prtAddressTextBox" runat="server" CssClass="form-control form-control-sm"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Permanent Address </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="permAddressTextBox" runat="server" CssClass="form-control form-control-sm"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>National Id No </label>
                                                <%--&nbsp; <span style="color: red;">*</span>--%>
                                                <asp:TextBox ID="nationalIdNoTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Employee Category  </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="catNameDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Spouse Name  </label>
                                                <asp:TextBox ID="sposNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Spouse Date of Birth </label>
                                                <div class="input-group date pull-left" id="daterangepicker1">
                                                    <asp:TextBox ID="sposeBirthDtTextBox" runat="server" CssClass="form-control form-control-sm" CausesValidation="true" Enabled="False"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                                        TargetControlID="sposeBirthDtTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton1" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <%--Row Number 5--%>

                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Reference Name </label>
                                                <asp:TextBox ID="referanceNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Reference Address </label>
                                                <asp:TextBox ID="referanceAddressTextBox" runat="server" CssClass="form-control form-control-sm"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Reference Cell No </label>
                                                <asp:TextBox ID="refranceCellNoTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Emergency Contact Person  </label>
                                                <asp:TextBox ID="EmergencyContactPersonTextBox" runat="server"
                                                    CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Emergency Contact Number  </label>
                                                <asp:TextBox ID="EmergencyContactNumberTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div id="step-2">
                            <h5 class="StepTitle" style="background-color: powderblue">Employment Information</h5>
                            <hr />
                            <br />
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <%--Row number 1--%>
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Company Name  </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="comNameDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Unit Name </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Division Name </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="divisNamDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="divisNamDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Department  </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="departmentDropDownList" runat="server"
                                                    CssClass="form-control form-control-sm" AutoPostBack="True"
                                                    OnSelectedIndexChanged="departmentDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Section </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="sectionDropDownList" runat="server" CssClass="form-control form-control-sm"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Emp Grade Name </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="empGradeDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="empGradeDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>


                                    <%--Row number 2--%>
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <br />                                              
                                                <asp:CheckBox ID="isSalesForce" Text="Is Sales personal" runat="server" AutoPostBack="True" OnCheckedChanged="isSalesForce_CheckedChanged" />
                                            </div>
                                        </div>
                                                                               
                                        <div class="col-2">
                                            <div class="form-group">
                                                 <label> Sales Personal Type  </label>
                                                <asp:DropDownList ID="ddlSalesForceType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesForceType_OnSelectedIndexChanged"
                                                    CssClass="form-control form-control-sm">
                                                    <asp:ListItem Value="">Select any one</asp:ListItem>
                                                    <asp:ListItem Value="RSM">RSM</asp:ListItem>
                                                    <asp:ListItem Value="ASM">ASM</asp:ListItem>
                                                    <asp:ListItem Value="MIO">MIO</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        
                                         <div class="col-3">
                                        <div class="form-group">
                                            <label>
                                                Region Name: 
                                            </label>
                                            <asp:DropDownList ID="regionDropDownList" runat="server" CssClass="form-control form-control-sm"
                                                AutoPostBack="True" OnTextChanged="regionDropDownList_OnTextChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>
                                                Area Name: 
                                            </label>
                                            <asp:DropDownList ID="areaDropDownList" runat="server" CssClass="form-control form-control-sm"
                                                AutoPostBack="True" OnSelectedIndexChanged="areaDropDownList_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>
                                                Territory Name: 
                                            </label>
                                            <asp:DropDownList ID="territoryDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                        
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Designation  </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="desigDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Salary Scale Name </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="salScaleNameDropDownList" runat="server"
                                                    AutoPostBack="True" CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Shift </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="shiftDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <%--                                                <div class="col-2">
                                                    <div class="form-group">
                                                        <label>Department  </label>
                                                        &nbsp; <span style="color: red;">*</span>
                                                        <asp:DropDownList ID="DropDownList4" runat="server"
                                                            CssClass="form-control form-control-sm" AutoPostBack="True"
                                                            OnSelectedIndexChanged="departmentDropDownList_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>--%>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Card No (<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>) </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:TextBox ID="cardNoTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Joining Date </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <div class="input-group date pull-left" id="daterangepicker1121">
                                                    <asp:TextBox ID="joiningDateTextBox" runat="server" CssClass="form-control form-control-sm" CausesValidation="true" OnTextChanged="joiningDateTextBox_TextChanged" AutoPostBack="True" Enabled="False"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom"
                                                        TargetControlID="joiningDateTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton3" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <%--Row number 4--%>
                                    <div class="form-row">
                                        
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Probation Period To </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <div class="input-group date pull-left" id="daterangepicker112">
                                                    <asp:TextBox ID="probitionDateTextBox" runat="server" CssClass="form-control form-control-sm" CausesValidation="true" Enabled="False"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton4" CssClass="custom"
                                                        TargetControlID="probitionDateTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton4" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Confirmation Date </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <div class="input-group date pull-left" id="daterangepicker1122">
                                                    <asp:TextBox ID="ConfirmationdateTextBox" runat="server" CssClass="form-control form-control-sm" CausesValidation="true" Enabled="False"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender5" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton5" CssClass="custom"
                                                        TargetControlID="ConfirmationdateTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton5" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Shift Employee </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="shiftEmployeeDropDownList" runat="server"
                                                    CssClass="form-control form-control-sm">
                                                    <asp:ListItem></asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Emploment Type  </label>
                                                <span style="color: red;">*</span>
                                                <asp:DropDownList ID="employmentTypeDropDownList" runat="server"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Over Time Allow  </label>
                                                <span style="color: red;">*</span>
                                                <asp:DropDownList ID="otAllowDropDownList" runat="server"
                                                    CssClass="form-control form-control-sm">
                                                    <asp:ListItem>Select any one</asp:ListItem>
                                                    <asp:ListItem>Yes</asp:ListItem>
                                                    <asp:ListItem>No</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>TIN No  </label>
                                                <asp:TextBox ID="tinNoTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        
                                    </div>

                                    <%--Row number 4--%>

                                    <div class="form-row">
                                        
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Pay Type  </label>
                                                &nbsp; <span style="color: red;">*</span>
                                                <asp:DropDownList ID="payTypeDropDownList" runat="server" CssClass="form-control form-control-sm"
                                                    AutoPostBack="True"
                                                    OnSelectedIndexChanged="payTypeDropDownList_SelectedIndexChanged">
                                                    <asp:ListItem>Select any one</asp:ListItem>
                                                    <asp:ListItem Value="C">Cash</asp:ListItem>
                                                    <asp:ListItem Value="Bk">Bank</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Bank Name  </label>
                                                <asp:DropDownList ID="bankNameDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Bank Account No </label>
                                                <asp:TextBox ID="bankAccNoTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Remarks </label>
                                                <asp:TextBox ID="remarksTextBox" runat="server" CssClass="form-control form-control-sm"
                                                    TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                        
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Job Location </label>
                                                <asp:DropDownList ID="ddlJobLocation" runat="server" CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>


                        <div id="step-3">
                            <h5 class="StepTitle" style="background-color: powderblue"> Education Information </h5>
                            <hr />
                            <br />

                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <div class="form-row">
                                        <div class="col-2">
                                            <label>Is Education  </label>
                                            <asp:CheckBox ID="iseducationCheckBox" runat="server" />
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Board/University Name  </label>
                                                <asp:DropDownList ID="boardDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Exam/Degree Title </label>
                                                <asp:DropDownList ID="examDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Area Of Stady </label>
                                                <asp:DropDownList ID="areaStudyDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Qualification Attend  </label>
                                                <asp:DropDownList ID="qualificationiDropDownList" runat="server"
                                                    AutoPostBack="True" CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Year of Passing </label>
                                                <asp:DropDownList ID="passYearDropDownList" CssClass="form-control form-control-sm" runat="server">
                                                    <asp:ListItem Value="1">Select any one </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Result Type </label>
                                                <asp:DropDownList ID="resultTypeDropDownList" runat="server" CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="resultTypeDropDownList_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">Select any one</asp:ListItem>
                                                    <asp:ListItem Value="Division">Division</asp:ListItem>
                                                    <asp:ListItem Value="Class">Class</asp:ListItem>
                                                    <asp:ListItem Value="CGPA">CGPA</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Result </label>
                                                <asp:TextBox ID="cgpaTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <%--<div class="col-6"></div>
                                                <div class="col-5"></div>--%>
                                        <div class="col-1">
                                            <div class="form-group">
                                                <asp:Button ID="addButton" Text="Add to list" CssClass="btn btn-sm block btn-addToList buttonFont" runat="server" OnClick="addtolistButton_Click" />
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div id="gridContainer1" style="height: auto; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                                        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                                            CssClass="table table-bordered text-center thead-dark" DataKeyNames="EduInstituteId,ExamId,QualificationId,StudyId">
                                                            <Columns>
                                                                <asp:BoundField DataField="Institute" HeaderText="Board &amp; University"
                                                                    HtmlEncode="False" />
                                                                <asp:BoundField DataField="Exam" HeaderText="Exam/Degree Title"
                                                                    HtmlEncode="False" />
                                                                <asp:BoundField DataField="PassingYear" HeaderText="Year Of Passing"
                                                                    HtmlEncode="False" />
                                                                <asp:BoundField DataField="Qualification" HeaderText="Qualification Attend"
                                                                    HtmlEncode="False" />
                                                                <asp:BoundField DataField="AreaStudy" HeaderText="Area Of Study"
                                                                    HtmlEncode="False" />
                                                                <asp:BoundField DataField="Result" HeaderText="Result" HtmlEncode="False" />
                                                                <asp:BoundField DataField="ResultType" HeaderText="Type" HtmlEncode="False" />

                                                                <asp:TemplateField HeaderText="Delete">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="deleteImageButton" runat="server"
                                                                            ImageUrl="~/Assets/img/delete.png" OnClick="deleteImageButton_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>

                        <div id="step-4">
                            <h5 class="StepTitle" style="background-color: powderblue">Weekly Holiday</h5>
                            <hr />
                            <br />

                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <%-- Holiday setup--%>
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Day Quantity </label>
                                                <span style="color: red;">*</span>
                                                <asp:RadioButtonList ID="dayqRadioButtonList" runat="server"
                                                    AutoPostBack="True" RepeatDirection="Horizontal" Width="120px"
                                                    OnSelectedIndexChanged="dayqRadioButtonList_SelectedIndexChanged">
                                                    <asp:ListItem> &nbsp; One </asp:ListItem>
                                                    <asp:ListItem> &nbsp; Two </asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>

                                        <div class="col-2" id="divfday" runat="server" visible="False">
                                            <div class="form-group">
                                                <label>First Day </label>
                                                <asp:DropDownList ID="fdayDropDownList" runat="server" CssClass="form-control form-control-sm"
                                                    AutoPostBack="True"
                                                    OnSelectedIndexChanged="fdayDropDownList_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                    <asp:ListItem>Saturday</asp:ListItem>
                                                    <asp:ListItem>Sunday</asp:ListItem>
                                                    <asp:ListItem>Monday</asp:ListItem>
                                                    <asp:ListItem>Tuesday</asp:ListItem>
                                                    <asp:ListItem>Wednesday</asp:ListItem>
                                                    <asp:ListItem>Thursday</asp:ListItem>
                                                    <asp:ListItem>Friday</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2" id="divsday" runat="server" visible="False">
                                            <div class="form-group">
                                                <label>Second Day </label>
                                                <asp:DropDownList ID="sdayDropDownList" runat="server" CssClass="form-control form-control-sm"
                                                    AutoPostBack="True"
                                                    OnSelectedIndexChanged="sdayDropDownList_SelectedIndexChanged">
                                                    <asp:ListItem Selected="True"></asp:ListItem>
                                                    <asp:ListItem>Saturday</asp:ListItem>
                                                    <asp:ListItem>Sunday</asp:ListItem>
                                                    <asp:ListItem>Monday</asp:ListItem>
                                                    <asp:ListItem>Tuesday</asp:ListItem>
                                                    <asp:ListItem>Wednesday</asp:ListItem>
                                                    <asp:ListItem>Thursday</asp:ListItem>
                                                    <asp:ListItem>Friday</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <%-- Job Experience setup--%>

                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <asp:CheckBox ID="jobCheckBox" runat="server" AutoPostBack="True"
                                                    OnCheckedChanged="jobCheckBox_CheckedChanged" />
                                                <br />
                                                <h5 class="StepTitle" style="background-color: powderblue">Give Job Experience</h5>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="divjob" runat="server" visible="False">
                                        <div class="form-row">
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Company Name </label>
                                                    <asp:TextBox ID="comNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Designation  </label>
                                                    <asp:TextBox ID="desigTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Department </label>
                                                    <asp:TextBox ID="deptTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>From Date </label>
                                                    <div class="input-group date pull-left" id="daterangepicker">
                                                        <asp:TextBox ID="fromDateTextBox" runat="server" CssClass="form-control form-control-sm" CausesValidation="true" Enabled="False"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender6" runat="server"
                                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton" CssClass="custom" PopupPosition="TopLeft"
                                                            TargetControlID="fromDateTextBox" />
                                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                            <span>
                                                                <asp:ImageButton ID="ImageButton" runat="server"
                                                                    AlternateText="Click to show calendar"
                                                                    ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>To Date  </label>
                                                    <div class="input-group date pull-left" id="daterangepicker1">
                                                        <asp:TextBox ID="toDateTextBox" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True"
                                                            OnTextChanged="toDateTextBox_TextChanged" CausesValidation="true" Enabled="False"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender" runat="server"
                                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton7" CssClass="custom" PopupPosition="TopLeft"
                                                            TargetControlID="toDateTextBox" />
                                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                            <span>
                                                                <asp:ImageButton ID="ImageButton7" runat="server"
                                                                    AlternateText="Click to show calendar"
                                                                    ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Duration </label>
                                                    <asp:TextBox ID="jobdurationTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <%--<div class="col-6"></div>
                                                    <div class="col-5"></div>--%>
                                            <div class="col-1">
                                                <div class="form-group">
                                                    <asp:Button ID="Button2" Text="Add to list" CssClass="btn btn-sm btn-addToList buttonFont" runat="server" OnClick="jobExAddtolistButton_Click" />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="jobloadGridView1" style="height: auto; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                                            <asp:GridView ID="jobloadGridView" runat="server" AutoGenerateColumns="False"
                                                                CssClass="table table-bordered text-center thead-dark">
                                                                <Columns>

                                                                    <asp:BoundField DataField="CompanyName" HeaderText="Company Name"
                                                                        HtmlEncode="False" />
                                                                    <asp:BoundField DataField="Designation" HeaderText="Designation"
                                                                        HtmlEncode="False" />
                                                                    <asp:BoundField DataField="Department" HeaderText="Department"
                                                                        HtmlEncode="False" />
                                                                    <asp:BoundField DataField="FromDate" HeaderText="From Date"
                                                                        HtmlEncode="False" />
                                                                    <asp:BoundField DataField="ToDate" HeaderText="To Date" HtmlEncode="False" />
                                                                    <asp:BoundField DataField="Duration" HeaderText="Duration" HtmlEncode="False" />
                                                                    <asp:TemplateField HeaderText="Delete">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="deleteJobImageButton" runat="server"
                                                                                ImageUrl="~/Assets/img/delete.png" OnClick="deleteJobImageButton_Click" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <%-- Training setup--%>

                                    <div class="form-row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <asp:CheckBox ID="trainingCheckBox" runat="server" AutoPostBack="True"
                                                    OnCheckedChanged="trainingCheckBox_CheckedChanged" />
                                                <br />
                                                <h5 class="StepTitle" style="background-color: powderblue">Give Training</h5>

                                            </div>
                                        </div>
                                    </div>

                                    <div id="divtraing" runat="server" visible="False">
                                        <div class="form-row">
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Training Name </label>
                                                    <asp:TextBox ID="trainingNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Institute Name  </label>
                                                    <asp:TextBox ID="instNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Subject </label>
                                                    <asp:TextBox ID="subjectTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Result </label>
                                                    <asp:TextBox ID="resultTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Country </label>
                                                    <asp:DropDownList ID="countryDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>From Date </label>
                                                    <div class="input-group date pull-left" id="daterangepicker11222">
                                                        <asp:TextBox ID="fromtrainDateTextBox" runat="server" CssClass="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender8" runat="server"
                                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton8" CssClass="custom"
                                                            TargetControlID="fromtrainDateTextBox" />
                                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                            <span>
                                                                <asp:ImageButton ID="ImageButton8" runat="server"
                                                                    AlternateText="Click to show calendar"
                                                                    ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>To Date  </label>
                                                    <div class="input-group date pull-left" id="daterangepicker1121826">
                                                        <asp:TextBox ID="totrainDateTextBox" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True"
                                                            OnTextChanged="totrainDateTextBox_TextChanged" CausesValidation="true"></asp:TextBox>
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender9" runat="server"
                                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton9" CssClass="custom" Enabled="True"
                                                            TargetControlID="totrainDateTextBox" />
                                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                            <span>
                                                                <asp:ImageButton ID="ImageButton9" runat="server"
                                                                    AlternateText="Click to show calendar"
                                                                    ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="form-group">
                                                    <label>Duration </label>
                                                    <asp:TextBox ID="durationTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <%--<div class="col-6"></div>
                                                    <div class="col-5"></div>--%>
                                            <div class="col-1">
                                                <div class="form-group">
                                                    <asp:Button ID="traningAddtolistButton" Text="Add to list" CssClass="btn btn-sm btn-addToList buttonFont" runat="server" OnClick="traningAddtolistButton_Click" />
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div id="trainingloadGridView1" style="height: auto; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                                            <asp:GridView ID="trainingloadGridView" runat="server" AutoGenerateColumns="False"
                                                                CssClass="table table-bordered text-center thead-dark">
                                                                <Columns>

                                                                    <asp:BoundField DataField="TrainingName" HeaderText="Traning Name"
                                                                        HtmlEncode="False" />
                                                                    <asp:BoundField DataField="InstituteName" HeaderText="InstituteName"
                                                                        HtmlEncode="False" />
                                                                    <asp:BoundField DataField="Subject" HeaderText="Subject" HtmlEncode="False" />
                                                                    <asp:BoundField DataField="Duration" HeaderText="Duration" HtmlEncode="False" />
                                                                    <asp:BoundField DataField="Result" HeaderText="Result" HtmlEncode="False" />
                                                                    <asp:BoundField DataField="FromDate" HeaderText="From Date"
                                                                        HtmlEncode="False" />
                                                                    <asp:BoundField DataField="ToDate" HeaderText="To Date" HtmlEncode="False" />
                                                                    <asp:BoundField DataField="Country" HeaderText="Country" HtmlEncode="False" />

                                                                    <asp:TemplateField HeaderText="Delete">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="deleteJobImageButton" runat="server"
                                                                                ImageUrl="~/Assets/img/delete.png" OnClick="deleteTrainingImageButton_Click" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="col-4">
                                            <div class="form-group">

                                                <asp:CheckBox ID="userCheckBox" runat="server" AutoPostBack="True" OnCheckedChanged="userCheckBox_CheckedChanged" />
                                                <label style="color: red; font-size: 12px; font-weight: bold;">Do You want to Create User For This Employee ? </label>

                                            </div>
                                        </div>
                                        <div class="col-3" id="userdiv" runat="server" visible="False">
                                            <div class="form-group">
                                                <label>Login Name </label>
                                                <asp:TextBox ID="loginnameTextBox" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <div class="form-row">
                        <div class="col-6">
                            <div class="form-group">
                                <asp:Button ID="submitButton" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="yesButton_Click" />
                                <%--<asp:ModalPopupExtender ID="submitButton_ModalPopupExtender" runat="server"
                                            BackgroundCssClass="modalBackground" CancelControlID="" DropShadow="true" Enabled="True" OkControlID="" PopupControlID="pnlModal"
                                            TargetControlID="submitButton">
                                        </asp:ModalPopupExtender>--%>
                                <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FF9900" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <%--    <asp:Panel ID="pnlModal" runat="server" CssClass="modalPopup" Style="display: none;">
                        <div class="modal fade" id="borderedExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content modal-content--bordered">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel"> Confirm Message </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    </div>
                                    <div class="modal-body"> Are you want to Insert Employee Image ? </div>
                                    <div class="modal-footer">
                                        <asp:Button ID="noButton" runat="server" Text="No" CssClass="btn btn-secondary" OnClick="noButton_Click" />
                                        <asp:Button ID="yesButton" runat="server" Text="Yes" CssClass="btn btn-primary" OnClick="yesButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>--%>
        </div>
    </div>
</asp:Content>

