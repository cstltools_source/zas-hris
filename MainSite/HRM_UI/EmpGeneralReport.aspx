﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="EmpGeneralReport.aspx.cs" Inherits="HRM_UI_EmpGeneralReport" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Employee Information Report</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Information Report</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label> Company </label>
                                        <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control form-control-sm"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            
                            

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Type </label>
                                        <asp:DropDownList ID="selectReportDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="selectReportDropDownList_SelectedIndexChanged">
                                            <asp:ListItem> Select from list </asp:ListItem>
                                            <asp:ListItem Value="S">Single Employee</asp:ListItem>
                                            <asp:ListItem Value="A">All Employee</asp:ListItem>
                                            <asp:ListItem Value="Acive">All Active Employee</asp:ListItem>
                                            <asp:ListItem Value="Inactive">All Inactive Employee</asp:ListItem>
                                            <asp:ListItem Value="Posted">All Posted Employee</asp:ListItem>
                                            <%--<asp:ListItem Value="SE">Shift Employee</asp:ListItem>
                                            <asp:ListItem Value="DW">Department Wise</asp:ListItem>
                                            <asp:ListItem Value="SW">Section Wise</asp:ListItem>
                                            <asp:ListItem Value="UW">Unit Wise</asp:ListItem>--%>
                                            <asp:ListItem Value="JDW">Joining Date wise</asp:ListItem>
                                            <asp:ListItem Value="TN">Employee TIN Report</asp:ListItem>
                                            <asp:ListItem Value="BA">Employee Bank Account Report</asp:ListItem>
                                            <asp:ListItem Value="EJR">Employee Job Left Report</asp:ListItem>
                                            <asp:ListItem Value="EPR">Employee Probation Report</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row" runat="server" visible="False" id="unit">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Unit </label>
                                            <asp:DropDownList ID="unitDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                </div>

                            <div id="oid" runat="server">
                                <div id="divshift" runat="server" visible="False">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Shift </label>
                                                <asp:DropDownList ID="shiftDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="dateid" runat="server" visible="False">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>From Date </label>
                                                <div class="input-group date pull-left" id="daterangepicker113">
                                                    <asp:TextBox ID="fromdtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom" PopupPosition="TopLeft"
                                                        TargetControlID="fromdtTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton3" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>To Date </label>
                                                <div class="input-group date pull-left" id="daterangepicker12">
                                                    <asp:TextBox ID="todtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton4" CssClass="custom" PopupPosition="TopLeft"
                                                        TargetControlID="todtTextBox" />
                                                    <%-- <div class="input-group-addon" style="border: 1px solid #cccccc">--%>
                                                    <span>
                                                        <asp:ImageButton ID="ImageButton4" runat="server"
                                                            AlternateText="Click to show calendar"
                                                            ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                    </span>
                                                    <%--</div>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="divdept" runat="server" visible="False">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Division </label>
                                                <asp:DropDownList ID="divisionDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="divisionDropDownList_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Department </label>
                                                <asp:DropDownList ID="deptDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div id="divsec" runat="server" visible="False">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Division </label>
                                                <asp:DropDownList ID="divDropDownList1" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="divDropDownList1_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Department </label>
                                                <asp:DropDownList ID="deptDropDownList2" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm" OnSelectedIndexChanged="deptDropDownList2_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Section </label>
                                                <asp:DropDownList ID="sectionDropDownList1" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div id="divempcode" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Code </label>
                                            <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

