﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Library.BLL.HRM_BLL;

public partial class HRM_UI_EmpGeneralReport : System.Web.UI.Page
{
    EmpGeneralInfoBLL aGeneralInfoBll = new EmpGeneralInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            aGeneralInfoBll.LoadShift(shiftDropDownList);
            aGeneralInfoBll.LoadUnitNameAll(unitDropDownList);
            aGeneralInfoBll.LoadDivisionNameToDropDownBLL(divisionDropDownList);
            aGeneralInfoBll.LoadDivisionNameToDropDownBLL(divDropDownList1);
            aGeneralInfoBll.LoadCompanyNameToDropDownBLL(ddlCompany);
        }
    }
    protected void viewButton_Click(object sender, EventArgs e)
    {

        if (ddlCompany.SelectedValue != "")
        {
            Session["EmpGeneralReportParameter"] = ParameterGenerator();
            PopUp(empCodeTextBox.Text, selectReportDropDownList.SelectedValue);
        }
        else
        {
            ddlCompany.Focus();
        }

        
    }
    private string ParameterGenerator()
    {
        string parameter = "";

        if (selectReportDropDownList.SelectedValue == "A")
        {
            parameter = " WHERE tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "'  ";

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "BA")
        {
            parameter = " WHERE E.PayType='Bank' AND E.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' ";

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "S")
        {
            parameter = " WHERE tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' AND tblEmpGeneralInfo.EmpMasterCode='" + empCodeTextBox.Text + "'  ";

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "SE")
        {
            parameter = " WHERE tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' AND  tblEmpGeneralInfo.ShiftId='" + shiftDropDownList.SelectedValue + "'  ";

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "DW")
        {
            parameter = " WHERE tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' AND  tblEmpGeneralInfo.DepId='" + deptDropDownList.SelectedValue + "' AND tblEmpGeneralInfo.EmployeeStatus='Active' ";

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "SW")
        {
            parameter = " WHERE tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' AND  tblEmpGeneralInfo.DepId='" + deptDropDownList2.SelectedValue + "' AND tblEmpGeneralInfo.SectionId='" + sectionDropDownList1.SelectedValue + "' AND tblEmpGeneralInfo.EmployeeStatus='Active' ORDER BY CONVERT(int , dbo.udf_GetNumeric(tblEmpGeneralInfo.EmpMasterCode) )  ASC";

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "JDW")
        {
            parameter = " AND tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "'";

            if (fromdtTextBox.Text != "" && todtTextBox.Text != "")
            {
                parameter = parameter + " AND  tblEmpGeneralInfo.JoiningDate BETWEEN '" + fromdtTextBox.Text + "' AND '" + todtTextBox.Text + "'  ";
            }

            return parameter;

        }

        if (selectReportDropDownList.SelectedValue == "Inactive" || selectReportDropDownList.SelectedValue == "Acive")
        {
            parameter = " AND tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "'";

            if (fromdtTextBox.Text != "" && todtTextBox.Text != "")
            {
                parameter = parameter + " AND  tblEmpGeneralInfo.JoiningDate BETWEEN '" + fromdtTextBox.Text + "' AND '" + todtTextBox.Text + "'  ";
            }

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "EJR")
        {
            //parameter = " WHERE tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' AND  EffectiveDate BETWEEN '" + fromdtTextBox.Text + "' AND '" + todtTextBox.Text + "'  ";

            parameter = " AND tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "'";

            if (fromdtTextBox.Text != "" && todtTextBox.Text != "")
            {
                parameter = parameter + " AND  EffectiveDate BETWEEN '" + fromdtTextBox.Text + "' AND '" + todtTextBox.Text + "'  ";
            }

            return parameter;

        }
        if (selectReportDropDownList.SelectedValue == "EPR")
        {
            //parameter = " WHERE tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' AND  ProbationPeriodTo BETWEEN '" + fromdtTextBox.Text + "' AND '" + todtTextBox.Text + "'  ";


            if (unitDropDownList.SelectedValue != "")
            {
                parameter = " AND EGI.UnitId = '" + Convert.ToInt32(unitDropDownList.SelectedValue) + "'";
            }
            

            if (fromdtTextBox.Text != "" && todtTextBox.Text != "")
            {
                parameter = parameter + " AND ProbationPeriodTo BETWEEN '" + fromdtTextBox.Text + "' AND '" + todtTextBox.Text + "'  ";
            }

            if (fromdtTextBox.Text == "" && todtTextBox.Text != "")
            {
                parameter = parameter + " AND ProbationPeriodTo < '" + todtTextBox.Text + "'";
            }

            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "UW")
        {
            parameter = " tblEmpGeneralInfo.CompanyInfoId = '" + Convert.ToInt32(ddlCompany.SelectedValue) + "' AND  WHERE tblEmpGeneralInfo.UnitId='" + unitDropDownList.SelectedValue + "'  ";

            return parameter;
        }
        parameter = parameter + " AND tblEmpGeneralInfo.UnitId IN (SELECT UnitId FROM tblUserUnit WHERE UserId='" +
                    HttpContext.Current.Session["UserId"].ToString() + "') ORDER BY  CONVERT(int , dbo.udf_GetNumeric(tblEmpGeneralInfo.EmpMasterCode) )  ASC";
        return parameter;
    }

    private void PopUp(string empcode, string reportFlag)
    {
        string url = "../Report_UI/EmpGeneralReportViewer.aspx?EmpCode=" + empcode + "&ReportFlag=" + reportFlag;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=1200,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void selectReportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        if (selectReportDropDownList.SelectedValue == "Posted")
        {
            divempcode.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue=="A")
        {
            divempcode.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue=="S")
        {
            divempcode.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue=="SE")
        {
            divshift.Visible = true;
        }
        else
        {
            divshift.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue=="DW")
        {
            divdept.Visible = true;
        }
        else
        {
            divdept.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue == "SW")
        {
            divsec.Visible = true;
        }
        else
        {
            divsec.Visible = false;
        }
        if(selectReportDropDownList.SelectedValue=="JDW")
        {
            dateid.Visible = true;
            oid.Visible = false;
        }
        else
        {
            dateid.Visible = false;
            oid.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue == "Acive")
        {
            divempcode.Visible = false;
            dateid.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue == "Inactive")
        {
            divempcode.Visible = false;
            dateid.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue == "EJR" || selectReportDropDownList.SelectedValue == "EPR")
        {
            //divempcode.Visible = false;
            dateid.Visible = true;
        }


        if (selectReportDropDownList.SelectedValue == "EPR")
        {
            unit.Visible = true;
        }
        else
        {
            unit.Visible = false; 
        }

        if (selectReportDropDownList.SelectedValue == "UW")
        {
            unit.Visible = true;
        }

    }
    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aGeneralInfoBll.LoadDepartmentToDropDownBLL(deptDropDownList,divisionDropDownList.SelectedValue);
    }

    protected void divDropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        aGeneralInfoBll.LoadDepartmentToDropDownBLL(deptDropDownList2, divDropDownList1.SelectedValue);
    }

    protected void deptDropDownList2_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        aGeneralInfoBll.LoadSectionNameToDropDownBLL(sectionDropDownList1, deptDropDownList2.SelectedValue);
    }
}