﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_EmpGroupArrange : System.Web.UI.Page
{
    GroupBLL aGroupBll=new GroupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
        }
        
    }
    private void LoadDropDown()
    {
        EmpGeneralInfoBLL aEmpGeneralInfoBll=new EmpGeneralInfoBLL();
        aEmpGeneralInfoBll.LoadUnitNameAllByUser(unitDropDownList);
        //aGroupBll.LoadGroup(groupDropDownList);
        aGroupBll.LoadDivisionName(divisionDropDownList);
    }
    //protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "Remove")
    //    {
    //        int deletingIndex = Convert.ToInt32(e.CommandArgument);
    //        List<Group> agroupList = new List<Group>();
    //        Group aGroup;

    //        int empId = Convert.ToInt32(loadGridView.DataKeys[deletingIndex][0].ToString());
    //        if (loadGridView.Rows.Count > 0)
    //        {
    //            for (int i = 0; i < loadGridView.Rows.Count; i++)
    //            {
    //                if (empId != Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()))
    //                {
    //                    aGroup = new Group()
    //                    {
    //                        EmpId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
    //                        EmpCode = loadGridView.Rows[i].Cells[0].Text,
    //                        EmpName =(loadGridView.Rows[i].Cells[1].Text),
    //                        DeptName = (loadGridView.Rows[i].Cells[2].Text)
    //                    };
    //                    agroupList.Add(aGroup);
    //                }
    //            }
    //        }


    //        loadGridView.DataSource = null;
    //        loadGridView.DataBind();
    //        loadGridView.DataSource = agroupList;
    //        loadGridView.DataBind();
    //    }
    //}
    //protected void addButton_Click(object sender, EventArgs e)
    //{
    //    List<Group> aGroupList = new List<Group>();
    //    Group aGroup;

    //    bool checkData;

    //    if (loadGridView.Rows.Count > 0)
    //    {
    //        for (int i = 0; i < loadGridView.Rows.Count; i++)
    //        {
    //            aGroup = new Group()
    //            {
    //                EmpId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
    //                EmpCode = loadGridView.Rows[i].Cells[0].Text,
    //                EmpName = (loadGridView.Rows[i].Cells[1].Text.Trim()),
    //                DeptName = (loadGridView.Rows[i].Cells[2].Text.Trim()),
    //            };
    //            aGroupList.Add(aGroup);

    //        }
    //    }
    //    int empid = Convert.ToInt32(empidHiddenField.Value);

    //    checkData = Convert.ToBoolean((from a in aGroupList
    //                                   where a.EmpId == empid
    //                                   select a).Count());

    //    if (!checkData)
    //    {
    //        aGroup = new Group()
    //        {
    //            EmpId = Convert.ToInt32(empidHiddenField.Value),
    //            EmpCode = empCodeTextBox.Text,
    //            EmpName = empnameLabel.Text,
    //            DeptName = DeptnameLabel.Text
    //        };

    //        aGroupList.Add(aGroup);


    //        loadGridView.DataSource = null;
    //        loadGridView.DataBind();
    //        loadGridView.DataSource = aGroupList;
    //        loadGridView.DataBind();
    //        empCodeTextBox.Text = string.Empty;
    //        empidHiddenField.Value = string.Empty;
    //        empnameLabel.Text = string.Empty;
    //        DeptnameLabel.Text = string.Empty;


    //    }
    //    else
    //    {
    //        //msgLabel.Text = "All Ready Added!!";
    //        showMessageBox("All Ready Added!!");
    //    }
    //}
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    //protected void empCodeTextBox_TextChanged(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(empCodeTextBox.Text.Trim()))
    //    {
    //        DataTable aTable = new DataTable();
    //        aTable = aGroupBll.LoadEmpInfo(empCodeTextBox.Text);

    //        if (aTable.Rows.Count > 0)
    //        {
    //            empidHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
    //            empnameLabel.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
    //            DeptnameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
    //        }
    //        else
    //        {
    //            showMessageBox("Data not Found");
    //        }
    //    }
    //    else
    //    {
    //        showMessageBox("Please Input Employee Code");
    //    }
    //}
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");


        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }



    }
    private void DepartmentWiseLoad()
    {
        DataTable aDataTable = new DataTable();
        aDataTable = aGroupBll.LoadDepartmentWiseEmp(deptDropDownList.SelectedValue);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    private void SectionWiseLoad()
    {
        DataTable aDataTable = new DataTable();
        aDataTable = aGroupBll.LoadSectionWiseEmp(secDropDownList.SelectedValue);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    private void Clear()
    {
        loadGridView.DataSource = null;
        loadGridView.DataBind();
    }
    //protected void submitButton_Click(object sender, EventArgs e)
    //{
        
    //}
    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aGroupBll.LoadDepartmentName(deptDropDownList,divisionDropDownList.SelectedValue);
    }
    protected void deptDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
     //DepartmentWiseLoad();   
        EmpGeneralInfoBLL aEmpGeneralInfoBll=new EmpGeneralInfoBLL();
        aEmpGeneralInfoBll.LoadSectionNameToDropDownBLL(secDropDownList,deptDropDownList.SelectedValue);
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        List<Group> aGroupList = new List<Group>();
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkSelect");
            if (ChkBoxRows.Checked == true)
            {
                Group aGroup = new Group();
                aGroup.EmpId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString());
                aGroup.GroupId = Convert.ToInt32(groupDropDownList.SelectedValue);
                aGroupList.Add(aGroup);
            }

        }
        if (aGroupBll.SaveDataForGroup(aGroupList))
        {
            showMessageBox("Data Save successfully");
            Clear();
        }
    }

    protected void unitDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        ShiftWiseGroupBLL  aShiftWiseGroupBll=new ShiftWiseGroupBLL();
        aShiftWiseGroupBll.LoadGroupName(groupDropDownList,unitDropDownList.SelectedValue);
    }

    protected void secDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        SectionWiseLoad();
    }

    protected void processButton_Click(object sender, EventArgs e)
    {
        SectionWiseLoad();
    }

    protected void OnClick(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        GridViewRow item = (GridViewRow)btn.Parent.Parent;
        int row = item.RowIndex;
        aGroupBll.DelGroupEmpInfo(loadGridView.DataKeys[row][0].ToString());
        showMessageBox("Employee Removed From Group ");
        SectionWiseLoad();

    }
}