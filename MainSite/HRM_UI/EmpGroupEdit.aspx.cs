﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_EmpGroupEdit : System.Web.UI.Page
{
    EmpGroupBLL aEmpGroupBll = new EmpGroupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
            groupIdHiddenField.Value = Request.QueryString["ID"];
            EmpGroupLoad(groupIdHiddenField.Value);
        }
    }

    public void LoadDropDownList()
    {
        EmpGeneralInfoBLL aEmpGeneralInfoBll = new EmpGeneralInfoBLL();
        aEmpGeneralInfoBll.LoadUnitNameAllByUser(unitDropDownList);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {

        if (empGroupTexBox.Text == "")
        {
            showMessageBox("Please Input  Employee Group!!");
            return false;
        }
        if (unitDropDownList.SelectedIndex == 0)
        {
            showMessageBox("Please Input  Company Unit!!");
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        { 
            EmpGroup aEmpGroup = new EmpGroup()
            {
                GroupId = Convert.ToInt32(groupIdHiddenField.Value),
                GroupName = empGroupTexBox.Text,
                UnitId = Convert.ToInt32(unitDropDownList.SelectedValue),
            };
           
            if (!aEmpGroupBll.UpdateaDataforEmpGroup(aEmpGroup))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpGroupLoad(string EmpGroupId)
    {
        EmpGroup aEmpGroup = new EmpGroup();
        aEmpGroup = aEmpGroupBll.EmpGroupEditLoad(EmpGroupId);
        empGroupTexBox.Text = aEmpGroup.GroupName;
        unitDropDownList.SelectedValue = aEmpGroup.UnitId.ToString();
    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}