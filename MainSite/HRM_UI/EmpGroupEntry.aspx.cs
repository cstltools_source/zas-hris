﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_EmpGroupEntry : System.Web.UI.Page
{
    EmpGroupBLL aEmpGroupBll = new EmpGroupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDownList();
        }
    }

    public void LoadDropDownList()
    {
        EmpGeneralInfoBLL aEmpGeneralInfoBll=new EmpGeneralInfoBLL();
        aEmpGeneralInfoBll.LoadUnitNameAllByUser(unitDropDownList);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       empGroupTexBox.Text = string.Empty;
        unitDropDownList.SelectedIndex = 0;
    }
    private bool Validation()
    {
        
        if (empGroupTexBox.Text == "")
        {
            showMessageBox("Please Input  Employee Group!!");
            return false;
        }
        if (unitDropDownList.SelectedIndex== 0)
        {
            showMessageBox("Please Input  Company Unit!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmpGroup aEmpGroup = new EmpGroup()
            {
                GroupName = empGroupTexBox.Text,
                UnitId = Convert.ToInt32(unitDropDownList.SelectedValue),
            };
            if (aEmpGroupBll.SaveDataForEmpGroup(aEmpGroup))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox("EmpGroup Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("EmpGroupView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}