﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmpImage.aspx.cs" Inherits="HRM_UI_EmpImage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
             <script type="text/javascript">
                 function previewFile() {
                     var preview = document.querySelector('#<%=picImage.ClientID %>');
                 var file = document.querySelector('#<%=picFileUpload.ClientID %>').files[0];
                 var reader = new FileReader();

                 reader.onloadend = function () {
                     preview.src = reader.result;
                 }

                 if (file) {
                     reader.readAsDataURL(file);
                 } else {
                     preview.src = "";
                 }
             }
    </script>
    <script type="text/javascript">
        function previewFileemp() {
            var preview = document.querySelector('#<%=emppicImage.ClientID %>');
            var file = document.querySelector('#<%=empFileUpload.ClientID %>').files[0];
            var reader = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
            }

            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
           <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;"> Employee Image </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="CompanyListImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Image</a></li>

                        </ol>
                    </nav>
                </div>
            <!-- //END PAGE HEADING -->
             <br/>
                    <br/>
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">

                        <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Employee Image </label>
                                        <asp:Image ID="emppicImage" runat="server" Height="160px" Width="140px" />
                        
                                    </div>
                                </div>
                                
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>&nbsp; </label>
                                        
                                        <asp:FileUpload ID="picFileUpload" onchange="previewFile()"  runat="server"  
                                         name="file" BorderStyle="Groove" Font-Bold="True" Font-Size="Medium" 
                                         ForeColor="#0033CC"/>
                                    </div>
                                </div>
                                
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Signature Image </label>
                                        <asp:Image ID="picImage" runat="server" Height="36px" Width="159px" />
                        
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <asp:FileUpload ID="empFileUpload" onchange="previewFileemp()" runat="server" 
                                        BorderStyle="Groove" Font-Bold="True" Font-Italic="False" Font-Size="Medium" 
                                        ForeColor="#0033CC" />
                        
                                    </div>
                                </div>
                            </div>

                         
                        <div class="form-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <asp:Button ID="updateButton" Text="Save " CssClass="btn btn-sm btn-info" runat="server" OnClick="saveButton_Click" />
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>
