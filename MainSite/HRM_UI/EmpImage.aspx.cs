﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_EmpImage : System.Web.UI.Page
{
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();
    EmpImageBLL aEmpImageBll = new EmpImageBLL();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public bool SaveImg()
    {
        string employeeId = Request.QueryString["empId"];
        //if (!aInfoBll.HasEmpId(employeeId))
        //{
        if (empFileUpload.HasFile)
        {
            if (empFileUpload.PostedFile.ContentType == "image/jpeg")
            {
                if (empFileUpload.PostedFile.ContentLength < 102400)
                {
                    int empId = Convert.ToInt32(Request.QueryString["empId"]);
                    int length = empFileUpload.PostedFile.ContentLength;
                    int siglength = picFileUpload.PostedFile.ContentLength;
                    byte[] imagebyt = new byte[length];
                    byte[] sigimagebyt = new byte[siglength];
                    HttpPostedFile img = empFileUpload.PostedFile;
                    HttpPostedFile sigimg = picFileUpload.PostedFile;
                    img.InputStream.Read(imagebyt, 0, length);
                    sigimg.InputStream.Read(sigimagebyt, 0, siglength);

                    aEmpImageBll.SaveEmpImage(imagebyt, sigimagebyt, empId);
                }

                else
                {
                    showMessageBox("select file");
                }
            }
        }
        else
        {
            return false;
        }
        return true;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    public void Close()
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void saveButton_Click(object sender, EventArgs e)
    {
        if (SaveImg())
        {
            showMessageBox("Image Saved");
            Close();
        }
    }
}