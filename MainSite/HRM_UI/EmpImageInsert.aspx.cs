﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.HRM_BLL;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_EmpImageInsert : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void PopUp(int empId)
    {
        string url = "../HRM_UI/EmpImage.aspx?empId=" + empId;
        string fullURL = "window.open('" + url + "', '_blank', 'height=500,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    public void clear()
    {
        empCodeTextBox.Text = "";
        empNameTexBox.Text = "";
    }
  
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empCodeTextBox.Text.Trim()))
        {
            ArrearBLL arrear = new ArrearBLL();
            DataTable aTable = new DataTable();
            aTable = arrear.LoadEmpInfo(empCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void insertButton_Click(object sender, EventArgs e)
    {
        PopUp(Convert.ToInt32(EmpInfoIdHiddenField.Value));
        clear();
    }
}