﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_EmpInformationApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    EmpGeneralInfoBLL aGeneralInfoBll = new EmpGeneralInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
            EmpLoad(Parameter());

        }
    }

    public void DropDownList()
    {
        aGeneralInfoBll.LoadUnitNameAllByUser(unitDropDownList);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void EmpLoad(string param)
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        aGeneralInfoBll.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);
        aDataTable = aGeneralInfoBll.LoadEmpGeneralInfoDeptWiseForApproval(param);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    
    
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ArrearId = loadGridView.DataKeys[rowindex][0].ToString();
           
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");
        
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[10].FindControl("chkSelect");
                if (ChkBoxHeader.Checked == true)
                {
                    ChkBoxRows.Checked = true;
                }
                else
                {
                    ChkBoxRows.Checked = false;
                }
            } 
    }
    public bool Validation()
    {
        int c = 0, i;
        for (i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)loadGridView.Rows[i].FindControl("chkSelect");
            if (cb.Checked != true)
            {
                c++;

            }
        }
        if (c == i)
        {
            showMessageBox("Choose check box ");
            return false;
        }
        return true;
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {

        if (Validation())
        {
            //try
            //{
                SubmitApproval();
            //}
            //catch (Exception)
            //{
            //    showMessageBox("Pick Any data ");
            //}
        }
        
        
    }
    
    private void SubmitApproval()
    {

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            try
            {
                CheckBox cb = (CheckBox) loadGridView.Rows[i].Cells[10].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    string joiningDate = loadGridView.Rows[i].Cells[8].Text.Trim();

                    string EmpInfoId = loadGridView.DataKeys[i][0].ToString();

                    string AppUser = Session["LoginName"].ToString();
                    DateTime appDate = System.DateTime.Today;

                    aGeneralInfoBll.EmpApprovalBLL(EmpInfoId, joiningDate, AppUser, appDate,
                        actionRadioButtonList.SelectedItem.Text);
                }
            }
            catch (Exception)
            {
                
                
            }
            
            
        }
        showMessageBox("Your Operation Successfully Done");
        EmpLoad(Parameter());
            
    }

    protected void unitDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //EmpLoad(Parameter());
    }

    public string Parameter()
    {
        string param = "";
        if (unitDropDownList.SelectedIndex>0)
        {
            param = param + " AND tblEmpGeneralInfo.UnitId='" + unitDropDownList.SelectedValue + "'";
        }
        return param;
    }
    protected void Button1_OnClick(object sender, EventArgs e)
    {
        EmpLoad(Parameter());
    }
}