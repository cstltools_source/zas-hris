﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="EmpProbation.aspx.cs" Inherits="HRM_UI_EmpProbation" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }

        .table th {
            padding: 8px !important;
            border-top: 1px solid #d7dde3;
            vertical-align: middle;
        }
    </style>

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Employee Probation </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%-- <asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="addImageButton_Click" />--%>
                        <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="ReloadImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Probation</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Unit Name </label>
                                        <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Master Code </label>
                                        <asp:TextBox ID="EmpMasterCodeTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label> From Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker12">
                                            <asp:TextBox ID="fromdateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton2" CssClass="custom"
                                                TargetControlID="fromdateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton2" runat="server"
                                                        AlternateText="Click to show calendar"
                                                        ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-2">
                                    <div class="form-group">
                                        <label> To Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker121">
                                            <asp:TextBox ID="todateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom"
                                                TargetControlID="todateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton3" runat="server"
                                                        AlternateText="Click to show calendar"
                                                        ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-1">
                                    <div class="form-group">
                                        <label style="color: white">Search </label>
                                        <br />
                                        <asp:Button ID="searchButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="searchButton_Click" Text="Search" />
                                    </div>
                                </div>

                            </div>
                            <hr />
                            <label class="highlight label-font-size font-weight-bold" style="color: #00997B">Employee on probation </label>
                            <hr />
                            <div class="form-row">
                                <div id="gridContainer1" style="height: 400px; overflow: auto; width: 100%;">
                                    <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                        CssClass="table table-bordered text-center thead-dark" DataKeyNames="EmpInfoId"
                                        OnRowCommand="loadGridView_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="SL">
                                                <ItemTemplate>
                                                    <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="UnitName" HeaderText="Unit Name" />
                                            <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Id" />
                                            <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                            <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                            <asp:BoundField DataField="DeptName" HeaderText="Department" />
                                            <asp:BoundField DataField="JoiningDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Joining Date" />
                                            <asp:BoundField DataField="ProbationPeriodTo" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Probation" />

                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:Button ID="confirmButton" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="confirmButton_Click"
                                                        Text="Confirm >>" />
                                                    <asp:ModalPopupExtender ID="submitButton_ModalPopupExtender" runat="server"
                                                        BackgroundCssClass="modalBackground" CancelControlID="" DropShadow="true"
                                                        Enabled="True" OkControlID="" PopupControlID="pnlModal"
                                                        TargetControlID="confirmButton">
                                                    </asp:ModalPopupExtender>



                                                    <asp:Panel ID="pnlModal" runat="server" CssClass="modal-dialog modal-lg" Width="500" Style="display: none;">

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Confirm Message</h5>
                                                                <asp:ImageButton ID="ImageButton1" CssClass="close" runat="server" ImageUrl="../Assets/delete-icon.png" />

                                                            </div>
                                                            <div class="modal-body">

                                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Assets/img/question.png" />
                                                                <p style="padding: 6px; font-size: 16px;">Are you want to Confirm Employee ?</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <asp:Button ID="yesButton" CssClass="btn btn-sm btn-secondary" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Yes" runat="server" Text="Yes" OnClick="yesButton_Click" />
                                                                <asp:Button ID="noButton" CssClass="btn btn-sm btn-primary" CommandArgument="<%# Container.DataItemIndex %>" CommandName="No" runat="server" Text="No" OnClick="noButton_Click" />

                                                            </div>
                                                        </div>


                                                    </asp:Panel>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <hr />
                            <br />
                            <br />
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

