﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_EmpProbation : System.Web.UI.Page
{
    ProbationBLL aProbationBLL = new ProbationBLL();
    FoodChargeDeactivationDal aDal = new FoodChargeDeactivationDal();
    ProbationBLL aProbationBll = new ProbationBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdownList();
            searchButton_Click(null, null);
        }
    }

    private void LoadDropdownList()
    {
        aDal.LoadUnitName(unitNameDropDownList);
    }
    protected void confirmButton_Click(object sender, EventArgs e)
    {
        
    }

    private string GetParameter()
    {
        string param = "";

        if (EmpMasterCodeTextBox.Text != "")
        {
            param = param + "  AND tblEmpGeneralInfo.EmpMasterCode = '" + EmpMasterCodeTextBox.Text.Trim() + "' ";
        }

        if (unitNameDropDownList.SelectedValue != "")
        {
            param = param + " AND tblEmpGeneralInfo.UnitId = '" + unitNameDropDownList.SelectedValue.Trim() + "'";
        }

        if (fromdateTextBox.Text != "" && todateTextBox.Text != "")
        {
            param = param + " AND ProbationPeriodTo BETWEEN '" + Convert.ToDateTime(fromdateTextBox.Text.Trim()) + "' AND '" + Convert.ToDateTime(todateTextBox.Text.Trim()) + "'";
        }

        return param;
    }

    private void PopUp(string Id)
    {
        string url = "ProbationConfirm.aspx?ID=" + Id;
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    public void LoadData()
    {
        
        //DataTable dt = aProbationBll.LoadEmployee(dateTextBox.Text);
        //loadGridView.DataSource = dt;
        //loadGridView.DataBind();
    }
    protected void dateTextBox_TextChanged(object sender, EventArgs e)
    {
        LoadData();
    }
    protected void ReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        searchButton_Click(null, null);
    }

    protected void yesButton_Click(object sender, EventArgs e)
    {
        //Button Button = (Button)sender;
        //GridViewRow currentRow = (GridViewRow)Button.Parent.Parent;
        //int rowindex = 0;
        //rowindex = currentRow.RowIndex;

        //DataTable dt = aProbationBLL.LoadEmployeeInfo(loadGridView.DataKeys[rowindex][0].ToString());

        //    Probation aProbation = new Probation()
        //    {
        //        EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[rowindex][0].ToString()),
                
        //    };


        //    aProbation.ConfirmationDate = Convert.ToDateTime(dt.Rows[0]["ConfirmationDate"].ToString());
        //        aProbation.ProbationPeriodTo = Convert.ToDateTime(dt.Rows[0]["ProbationPeriodTo"].ToString());
        //        aProbation.Action = "Confirmed";
            
            
        //    if (aProbationBLL.SaveDataForProbation(aProbation))
        //    {

        //        EmpGeneralInfo aEmpGeneralInfo = new EmpGeneralInfo();
        //        aEmpGeneralInfo.EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[rowindex][0].ToString());
        //        aEmpGeneralInfo.ConfirmationDate = Convert.ToDateTime(dt.Rows[0]["ConfirmationDate"].ToString());
        //        if (aProbationBLL.UpdateConfirm(aEmpGeneralInfo))
        //        {
        //            showMessageBox("Saved Data For Employee Job Confirmation!!!");
        //            LoadData();
        //        }
                
                
        //    }
        
    }
    protected void showMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void noButton_Click(object sender, EventArgs e)
    {
        //Button Button = (Button)sender;
        //GridViewRow currentRow = (GridViewRow)Button.Parent.Parent;
        //int rowindex = 0;
        //rowindex = currentRow.RowIndex;
        //string empid = (loadGridView.DataKeys[rowindex][0].ToString());

        //PopUp(empid);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Yes")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            DataTable dt = aProbationBLL.LoadEmployeeInfo(loadGridView.DataKeys[rowindex][0].ToString());

            Probation aProbation = new Probation()
            {
                EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[rowindex][0].ToString()),

            };


            aProbation.ConfirmationDate = Convert.ToDateTime(dt.Rows[0]["ConfirmationDate"].ToString());
            aProbation.ProbationPeriodTo = Convert.ToDateTime(dt.Rows[0]["ProbationPeriodTo"].ToString());
            aProbation.Action = "Confirmed";


            if (aProbationBLL.SaveDataForProbation(aProbation))
            {

                EmpGeneralInfo aEmpGeneralInfo = new EmpGeneralInfo();
                aEmpGeneralInfo.EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[rowindex][0].ToString());
                aEmpGeneralInfo.ConfirmationDate = Convert.ToDateTime(dt.Rows[0]["ConfirmationDate"].ToString());
                if (aProbationBLL.UpdateConfirm(aEmpGeneralInfo))
                {
                    showMessageBox("Employee confirm successfully !!!");
                    LoadData();
                }


            }
        }
        if (e.CommandName == "No")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            
            string empid = (loadGridView.DataKeys[rowindex][0].ToString());

            //PopUp(empid);
        }
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        DataTable aTable = aProbationBll.LoadEmployee(GetParameter());

        if (aTable.Rows.Count > 0)
        {
            loadGridView.DataSource = aTable;
            loadGridView.DataBind();
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
        }
    }
}