﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_EmpQualificationEntry : System.Web.UI.Page
{
    EmpAcademyInfoBLL aAcademyInfoBll = new EmpAcademyInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       qualificationTextBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (qualificationTextBox.Text == "")
        {
            showMessageBox("Please Input Qualification Name!!");
            return false;
        }
        if (qualificationTextBox.Text == "")
        {
            showMessageBox("Please Input  Qualification Short Name!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmpQualification aQualification = new EmpQualification()
            {
                Qualification = qualificationTextBox.Text,
            };
            if (aAcademyInfoBll.SaveDataForEmpQualification(aQualification))
            {
                showMessageBox("Data Save Successfully");
                Clear();
            }
            else
            {
                showMessageBox("Qualification Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("QualificationView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}