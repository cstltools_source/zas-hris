﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_EmpSalaryGenerator : System.Web.UI.Page
{
    EmpSalaryGeneratorBLL aEmpSalaryGeneratorBll=new EmpSalaryGeneratorBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void empTypeRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (empTypeRadioButtonList.SelectedValue == "Single Employee")
        {
            empcodeLabel.Visible = true;
            empCodeTextBox.Visible = true;
        }
        if (empTypeRadioButtonList.SelectedValue == "All Employee")
        {
            empcodeLabel.Visible = false;
            empCodeTextBox.Visible = false;
        }
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (empTypeRadioButtonList.SelectedValue == "Single Employee")
        {
            if (aEmpSalaryGeneratorBll.SalaryProcessSingleEmp(empIdHiddenField.Value,Convert.ToDateTime(fromdateTextBox.Text),Convert.ToDateTime(todateTextBox.Text),salaryIdDropDownList.SelectedValue,empCodeTextBox.Text))
            {
                showMessageBox("Employee salary has been generated");
                empCodeTextBox.Text = string.Empty;
                fromdateTextBox.Text = string.Empty;
                todateTextBox.Text = string.Empty;
            }
            
        }
        if (empTypeRadioButtonList.SelectedValue == "All Employee")
        {
            if (aEmpSalaryGeneratorBll.SalaryProcess(Convert.ToDateTime(fromdateTextBox.Text),Convert.ToDateTime(todateTextBox.Text),salaryIdDropDownList.SelectedValue))
            {
                showMessageBox("All employee salary has been generated");
                fromdateTextBox.Text = string.Empty;
                todateTextBox.Text = string.Empty;
            }
            
        }
    }
    protected void empCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = aEmpSalaryGeneratorBll.LoadEmpInfo(empCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
}