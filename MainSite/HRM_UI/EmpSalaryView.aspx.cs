﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_EmpGenInfView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    EmpSalaryBLL aEmpSalaryBll = new EmpSalaryBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EmpSalaryInfoLoad();
            
        }
    }
    private void EmpSalaryInfoLoad()
    {
        aDataTable = aEmpSalaryBll.LoadEmpGeneralInfoDeptWise();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string EmpInfoId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(EmpInfoId);
        }
    }
    private void PopUp(string Id)
    {
        string url = "EmpSalaryDetailsView.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void companyInfoNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("GerEmpSalaryEntry.aspx");
    }
    
    
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[10].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string empidId = loadGridView.DataKeys[i][0].ToString();
                aEmpSalaryBll.DeleteData(empidId);
            }
            EmpSalaryInfoLoad();
        }

    }
}