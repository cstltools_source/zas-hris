﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="EmployeSalaryHold.aspx.cs" Inherits="HRM_UI_EmployeSalaryHold" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }
    </style>

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;"> Employee Salary Hold </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block"></div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Group Arrange</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Company Unit </label>
                                        <asp:DropDownList ID="unitDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="unitDropDownList_OnSelectedIndexChanged" class="form-control form-control-sm" ></asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Division </label>
                                        <asp:DropDownList ID="divisionDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="divisionDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department </label>
                                        <asp:DropDownList ID="deptDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="deptDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Section </label>
                                        <asp:DropDownList ID="secDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="secDropDownList_OnSelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                               
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">

                            <div class="form-row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div id="gridContainer1" style="height: 320px; overflow: auto; width: auto;">
                                            <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                                CssClass="table table-bordered text-center thead-dark" DataKeyNames="EmpInfoId">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SL">
                                                        <ItemTemplate>
                                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                                    <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                                    <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                                    <asp:BoundField DataField="DeptName" HeaderText="Department" />
                                                    <asp:BoundField DataField="SectionName" HeaderText="Section" />
                                                    
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True"
                                                                OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" runat="server" CssClass="btn btn-sm btn-info" Text="Submit" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

