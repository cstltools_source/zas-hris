﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;

public partial class HRM_UI_EmployeSalaryHold : System.Web.UI.Page
{
    EmpSalaryHoldDal aHoldDal = new EmpSalaryHoldDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
            LoadEmployeeInformation();
        }
        
    }

    private void LoadDropDown()
    {
        aHoldDal.LoadUnitName(unitDropDownList);
        aHoldDal.LoadDivisionName(divisionDropDownList);
    }

    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        deptDropDownList.Items.Clear();
        secDropDownList.Items.Clear();

        if (divisionDropDownList.SelectedValue != "")
        {
            aHoldDal.LoadDepartmentName(deptDropDownList, divisionDropDownList.SelectedValue);
        }
        else
        {
            deptDropDownList.Items.Clear();
            secDropDownList.Items.Clear();
        }

        LoadEmployeeInformation();
    }

    protected void deptDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {

        secDropDownList.Items.Clear();

        if (deptDropDownList.SelectedValue != "")
        {
            aHoldDal.LoadSectionName(secDropDownList, deptDropDownList.SelectedValue);
        }
        else
        {
            secDropDownList.Items.Clear();
        }

        LoadEmployeeInformation();
    }

    protected void secDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEmployeeInformation();
    }

    private void LoadEmployeeInformation()
    {
        DataTable aTable = aHoldDal.GetEmployeeList(GenerateParameter());

        if (aTable.Rows.Count > 0)
        {
            loadGridView.DataSource = aTable;
            loadGridView.DataBind();

            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkSelect");

                if (Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()) == aTable.Rows[i].Field<Int32>("EmpInfoId"))
                {
                    ChkBoxRows.Checked = aTable.Rows[i].Field<Boolean>("IsSalary");
                }
            }
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
        }
    }


    private string GenerateParameter()
    {
        string pram = "";


        if (unitDropDownList.SelectedValue != "")
        {
            pram = pram + " AND EGI.UnitId = " + unitDropDownList.SelectedValue;
        }
        
        if (divisionDropDownList.SelectedValue != "")
        {
            pram = pram + " AND EGI.DivisionId = " + divisionDropDownList.SelectedValue;
        }

        if (deptDropDownList.SelectedValue != "")
        {
            pram = pram + " AND EGI.DepId = " + deptDropDownList.SelectedValue;
        }

        if (secDropDownList.SelectedValue != "")
        {
            pram = pram + " AND EGI.SectionId = " + secDropDownList.SelectedValue;
        }

        
        return pram;
    }

    protected void unitDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEmployeeInformation();
    }

    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");


        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkSelect");
            
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {

        bool status = false;
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox) loadGridView.Rows[i].Cells[3].FindControl("chkSelect");
            status = aHoldDal.UpdateSalaryHold(loadGridView.DataKeys[i][0].ToString(), ChkBoxRows.Checked);
            
        }

        if (status)
        {
            showMessageBox("Information Save Successfully !!!");
            LoadEmployeeInformation();
        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }

    private void Clear()
    {
        LoadDropDown();
        deptDropDownList.Items.Clear();
        secDropDownList.Items.Clear();
    }
}