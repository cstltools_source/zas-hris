﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_EmployeeEditAfterApproval : System.Web.UI.Page
{
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
           
        }
    }

    private void SetDefaultData()
    {
        salScaleNameDropDownList.SelectedValue = 4.ToString(CultureInfo.InvariantCulture);
        shiftDropDownList.SelectedValue = 1.ToString(CultureInfo.InvariantCulture);
        shiftEmployeeDropDownList.SelectedIndex = 2;
        otAllowDropDownList.SelectedIndex = 2;
    }
    private void Clear()
    {
        dayDropDownList.SelectedIndex = 0;
        monthDropDownList.SelectedIndex = 0;
        yearDropDownList.SelectedIndex = 0;
        empNameTextBox.Text = string.Empty;
        empidTextBox.Text = string.Empty;
        nationalityDropDownList.SelectedValue = null;
        medicalDropDownList.SelectedIndex = 0;
        probitionDateTextBox.Text = string.Empty;
        shiftEmployeeDropDownList.SelectedValue = null;
        shortNameTextBox.Text = string.Empty;
        fatherNameTextBox.Text = string.Empty;
        otAllowDropDownList.SelectedIndex = 0;
        motherNameTextBox.Text = string.Empty;
        religionDropDownList.Text = "----Select----";
        nationalityDropDownList.SelectedIndex = 0;
        placeOfBirthTextBox.Text = string.Empty;
        bloodGroupDropDownList.Text = "----Select----";
        genderDropDownList.Text = "----Select----";
        maritalStatusDropDownList.SelectedValue = null;
        permAddressTextBox.Text = string.Empty;
        prtAddressTextBox.Text = string.Empty;
        emailTextBox.Text = string.Empty;
        phoneNoTextBox.Text = string.Empty;
        mobileNoTextBox.Text = string.Empty;
        nationalIdNoTextBox.Text = string.Empty;
        sposNameTextBox.Text = string.Empty;
        sposeBirthDtTextBox.Text = string.Empty;
        referanceNameTextBox.Text = string.Empty;
        referanceAddressTextBox.Text = string.Empty;
        refranceCellNoTextBox.Text = string.Empty;
        joiningDateTextBox.Text = string.Empty;
        employmentTypeDropDownList.Text = string.Empty;
        empGradeDropDownList.SelectedValue = null;
        salScaleNameDropDownList.SelectedValue = null;
        payTypeDropDownList.SelectedValue = null;
        otAllowDropDownList.SelectedValue = null;
        shiftEmployeeDropDownList.SelectedIndex = 0;
        ConfirmationdateTextBox.Text = string.Empty;
        comNameDropDownList.SelectedValue = null;
        unitNameDropDownList.SelectedValue = null;
        divisNamDropDownList.SelectedValue = null;
        departmentDropDownList.SelectedValue = null;
        desigDropDownList.SelectedValue = null;
        sectionDropDownList.SelectedValue = null;
        shiftDropDownList.SelectedValue = null;
        passYearDropDownList.SelectedValue = null;
        cgpaTextBox.Text = string.Empty;
        entryDateTextBox.Text = string.Empty;
        bloodGroupDropDownList.SelectedValue = null;
        maritalStatusDropDownList.SelectedValue = null;
        ageTextBox.Text = string.Empty;
        genderDropDownList.SelectedValue = null;
        bankNameDropDownList.SelectedValue = null;
        bankAccNoTextBox.Text = string.Empty;
        remarksTextBox.Text = string.Empty;
        catNameDropDownList.SelectedValue = null;
        cardNoTextBox.Text = string.Empty;
        nationalityDropDownList.SelectedIndex = 0;
        religionDropDownList.SelectedIndex = 0;
        EmergencyContactNumberTextBox.Text = string.Empty;
        EmergencyContactPersonTextBox.Text = string.Empty;
        loadGridView.DataSource = null;
        loadGridView.DataBind();
    }
    protected void yearDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        AgeNew();
    }
    public void AgeNew()
    {
        string birthdt = yearDropDownList.SelectedItem.Text + "-" + monthDropDownList.SelectedValue + "-" +
                         dayDropDownList.SelectedItem.Text;

        try
        {
            DateTime dob = Convert.ToDateTime(birthdt);
            DateTime PresentYear = DateTime.Now;
            TimeSpan ts = PresentYear - dob;
            DateTime age = new DateTime(PresentYear.Subtract(dob).Ticks);

            ageTextBox.Text = (age.Year - 1) + " years " + age.Month.ToString() + " months " + age.Day.ToString() + " days ";
        }
        catch (Exception)
        {

            showMessageBox("Give A valid Date of birth !!");
        }
    }

    private void LoadDateBirthYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 80; i <= DateTime.Now.Year; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));
    }
    private void LoadDay()
    {
        int i = 1;
        for (i = 1; i <= 31; i++)
        {
            dayDropDownList.Items.Add(Convert.ToString(i));

        }
    }
    private void LoadMonth()
    {
        int i = 1;
        for (i = 1; i <= 12; i++)
            monthDropDownList.Items.Add(Convert.ToString(i));
    }

    public void GridChekc()
    {
        if (jobloadGridView.Rows.Count <= 0)
        {
            jobCheckBox.Checked = false;
            divjob.Visible = false;
        }
        if (trainingloadGridView.Rows.Count <= 0)
        {
            trainingCheckBox.Checked = false;
            divtraing.Visible = false;
        }
        if (jobloadGridView.Rows.Count > 0)
        {
            jobCheckBox.Checked = true;
            divjob.Visible = true;
        }
        if (trainingloadGridView.Rows.Count > 0)
        {
            trainingCheckBox.Checked = true;
            divtraing.Visible = true;
        }
    }
    private void LoadPassYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 50; i <= DateTime.Now.Year; i++)
            passYearDropDownList.Items.Add(Convert.ToString(i));
    }
    public void GridLoad()
    {
        DataTable dtedu = aInfoBll.LoadEucation(empIdHiddenField.Value);
        DataTable dtjob = aInfoBll.LoadJobExp(empIdHiddenField.Value);
        DataTable dttrain = aInfoBll.LoadTraining(empIdHiddenField.Value);
        loadGridView.DataSource = dtedu;
        loadGridView.DataBind();

        jobloadGridView.DataSource = dtjob;
        jobloadGridView.DataBind();

        trainingloadGridView.DataSource = dttrain;
        trainingloadGridView.DataBind();
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    public void DropDownList()
    {
        aInfoBll.LoadEmpCategoryBLL(catNameDropDownList);
        aInfoBll.LoadCountry(countryDropDownList);
        aInfoBll.LoadShift(shiftDropDownList);
        aInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        aInfoBll.LoadBankName(bankNameDropDownList);
        aInfoBll.LoadDivisionNameToDropDownBLL(divisNamDropDownList);
        aInfoBll.LoadGradeNameToDropDownBLL(empGradeDropDownList);
        aInfoBll.LoadSalaryScaleNameToDropDownBLL(salScaleNameDropDownList);
        aInfoBll.LoadEmpTypeNameToDropDownBLL(employmentTypeDropDownList);
        aInfoBll.LoadJobLocation(ddlJobLocation);
        //aInfoBll.LoadLineToDropDownBLL(lineDropDownList);
    }

    private bool Validation()
    {
        if (entryDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date!!");
            entryDateTextBox.Focus();
            SetFocus(entryDateTextBox);
            return false;
        }
        //DataTable dtdata = aInfoBll.CheckCardNo(Label1.Text + cardNoTextBox.Text);
        //if (dtdata.Rows.Count > 0)
        //{
        //    showMessageBox("Card No already Exist!!");
        //    cardNoTextBox.Focus();
        //    SetFocus(cardNoTextBox);
        //    return false;
        //}
        //if (empNameTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Employee Name!!");
        //    SetFocus(empNameTextBox);
        //    return false;
        //}
        //if (shortNameTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Employee Short Name!!");
        //    SetFocus(shortNameTextBox);
        //    return false;
        //}
        //if (fatherNameTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Father Name!!");
        //    SetFocus(fatherNameTextBox);
        //    return false;
        //}
        //if (motherNameTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Mother Name!!");
        //    SetFocus(motherNameTextBox);
        //    return false;
        //}
        if (dayDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Date of Birth!!");
            SetFocus(dayDropDownList);
            dayDropDownList.Focus();
            return false;
        }
        if (monthDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Date of Birth!!");
            SetFocus(monthDropDownList);
            monthDropDownList.Focus();
            return false;
        }
        if (yearDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Date of Birth!!");
            SetFocus(yearDropDownList);
            yearDropDownList.Focus();
            return false;
        }
        //if (nationalityDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Input nationality!!");
        //    SetFocus(nationalityDropDownList);
        //    nationalityDropDownList.Focus();
        //    return false;
        //}
        //if (religionDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Input Religion!!");
        //    SetFocus(religionDropDownList);
        //    religionDropDownList.Focus();
        //    return false;
        //}
        //if (placeOfBirthTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Birth place!!");
        //    SetFocus(placeOfBirthTextBox);
        //    return false;
        //}
        //if (prtAddressTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Present Address!!");
        //          SetFocus(prtAddressTextBox);
        //    return false;
        //}
        //if (permAddressTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Permanent Address!!");
        //      SetFocus(permAddressTextBox);
        //    return false;
        //}
        //if (nationalIdNoTextBox.Text == "")
        //{
        //    showMessageBox("Please Input National Id!!");
        //      SetFocus(nationalIdNoTextBox);
        //    return false;
        //}
        //if (cardNoTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Punch CardNo!!");
        //    SetFocus(cardNoTextBox);
        //    return false;
        //}
        if (catNameDropDownList.Text == "")
        {
            showMessageBox("Please Input Categoty!!");
            SetFocus(catNameDropDownList);
            catNameDropDownList.Focus();
            return false;
        }
        if (ageTextBox.Text == "")
        {
            showMessageBox("Please Input Age!!");
            SetFocus(ageTextBox);
            ageTextBox.Focus();
            return false;
        }
        //if (emailTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Email!!");
        //     SetFocus(emailTextBox);
        //    return false;
        //}
        //if (bloodGroupDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Input Blood Group!!");
        //     SetFocus(bloodGroupDropDownList);
        //    return false;
        //}
        //if (maritalStatusDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Input Marital Status!!");
        //     SetFocus(maritalStatusDropDownList);
        //    return false;
        //}
        //if (medicalDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Input Medical Informaton!!");
        //     SetFocus(medicalDropDownList);
        //    return false;
        //}
        //if (genderDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Input Gender!!");
        //     SetFocus(genderDropDownList);
        //    genderDropDownList.Focus();
        //    return false;
        //}
        //if (mobileNoTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Mobile No!!");
        //     SetFocus(mobileNoTextBox);
        //    return false;
        //}
        /////////////////////////////////////////////////////
        if (comNameDropDownList.Text == "")
        {
            showMessageBox("Please Input Company Name!!");
            SetFocus(comNameDropDownList);
            comNameDropDownList.Focus();
            return false;
        }
        if (unitNameDropDownList.Text == "")
        {
            showMessageBox("Please Input Unit Name!!");
            SetFocus(unitNameDropDownList);
            unitNameDropDownList.Focus();
            return false;
        }
        if (divisNamDropDownList.Text == "")
        {
            showMessageBox("Please Input Division Name!!");
            SetFocus(divisNamDropDownList);
            divisNamDropDownList.Focus();
            return false;
        }
        if (departmentDropDownList.Text == "")
        {
            showMessageBox("Please Input Department!!");
            SetFocus(departmentDropDownList);
            departmentDropDownList.Focus();
            return false;
        }
        if (sectionDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Section!!");
            SetFocus(sectionDropDownList);
            sectionDropDownList.Focus();
            return false;
        }
        if (empGradeDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Emp Grade Name!!");
            SetFocus(empGradeDropDownList);
            empGradeDropDownList.Focus();
            return false;
        }
        if (desigDropDownList.Text == "")
        {
            showMessageBox("Please Input Designation!!");
            SetFocus(desigDropDownList);
            desigDropDownList.Focus();
            return false;
        }
        if (salScaleNameDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Salary Scale Name!!");
            SetFocus(salScaleNameDropDownList);
            salScaleNameDropDownList.Focus();
            return false;
        }
        if (shiftDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Shift!!");
            SetFocus(shiftDropDownList);
            shiftDropDownList.Focus();
            return false;
        }

        if (payTypeDropDownList.SelectedItem.Text == "Bank")
        {
            if (bankNameDropDownList.SelectedValue == "")
            {
                showMessageBox("Please Select a Bank!!");
                SetFocus(bankNameDropDownList);
                bankNameDropDownList.Focus();
                return false;
            }

            if (bankAccNoTextBox.Text == "")
            {
                showMessageBox("Please insert Bank acount No!!");
                SetFocus(bankNameDropDownList);
                bankNameDropDownList.Focus();
                return false;
            }

        }

        /////////////////////////////////////////////
        if (joiningDateTextBox.Text == "")
        {
            showMessageBox("Please Input joining Date!!");
            SetFocus(joiningDateTextBox);
            joiningDateTextBox.Focus();
            return false;
        }
        //if (probitionDateTextBox.Text == "")
        //{
        //    showMessageBox("Please Input Probition Date!!");
        //    SetFocus(probitionDateTextBox);
        //    return false;
        //}
        if (shiftEmployeeDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Shift Employee!!");
            SetFocus(shiftEmployeeDropDownList);
            shiftEmployeeDropDownList.Focus();
            return false;
        }
        if (ConfirmationdateTextBox.Text == "")
        {
            showMessageBox("Please Input Confirmation date!!");
            SetFocus(ConfirmationdateTextBox);
            ConfirmationdateTextBox.Focus();
            return false;
        }
        if (employmentTypeDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Employment Type!!");
            SetFocus(employmentTypeDropDownList);
            employmentTypeDropDownList.Focus();
            return false;
        }
        if (otAllowDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Over Time!!");
            SetFocus(otAllowDropDownList);
            otAllowDropDownList.Focus();
            return false;
        }
        if (payTypeDropDownList.SelectedIndex == 0)
        {
            showMessageBox("Please Input Pay Type!!");
            SetFocus(payTypeDropDownList);
            payTypeDropDownList.Focus();
            return false;
        }
        ///////////////////////////////////////////
        //if (maritalStatusDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Input Marital Status !!");
        //    SetFocus(maritalStatusDropDownList);
        //    return false;
        //}

        if (EntryDate() == false)
        {
            showMessageBox("Please give a valid Entry Date !!!");
            return false;
        }
        if (JoiningDate() == false)
        {
            showMessageBox("Please give a valid Joining Date !!!");
            return false;
        }

        //if (sposNameTextBox.Text!="")
        //{
        //    if (sposeBirthDtTextBox.Text == "")
        //    {
        //        showMessageBox("Please give Spose Date of Birth !!!");
        //        return false;
        //    }
        //}

        //if (Probitiondate() == false)
        //{
        //    showMessageBox("Please give a valid Probition Date !!!");
        //    return false;
        //}

        //if (ConfirmationDate() == false)
        //{
        //    showMessageBox("Please give a valid Extention Probition Period !!!");
        //    return false;
        //}
        if (employmentTypeDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Choose Employee Type !!!");
            employmentTypeDropDownList.Focus();
            return false;
        }
        if (cardNoTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Employee Card No !!!");
            cardNoTextBox.Focus();
            return false;
        }
        if (catNameDropDownList.Text == string.Empty)
        {
            showMessageBox("Please Input Employee Category Name !!!");
            catNameDropDownList.Focus();
            return false;
        }
        //if (loadGridView.Rows.Count <= 0)
        //{
        //    showMessageBox("Please Input Educational Information!!!");
        //    return false;
        //}

        if (fdayDropDownList.SelectedValue == "" && fdayDropDownList.SelectedIndex == 1)
        {
            showMessageBox("Please select atleast 1 day !!!");
            fdayDropDownList.Focus();
            return false;
        }
        //if (tinNoTextBox.Text== "")
        //{
        //    showMessageBox("Please TIN No !!!");
        //    return false;
        //}
        //fdayDropDownList
        if (Name() == false)
        {
            showMessageBox("Weekly Holiday Must not Be Same");
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation() == true)
            {

                string birthdt = yearDropDownList.SelectedItem.Text + "-" + monthDropDownList.SelectedValue + "-" +
                             dayDropDownList.SelectedItem.Text;
                EmpGeneralInfo employeeInformation = new EmpGeneralInfo();

                empIdHiddenField.Value = HiddenFieldempid.Value;
                employeeInformation.EmpInfoId = Convert.ToInt32(HiddenFieldempid.Value);
                employeeInformation.EntryDate = Convert.ToDateTime(entryDateTextBox.Text);
                employeeInformation.EmpName = empNameTextBox.Text;
                employeeInformation.ShortName = shortNameTextBox.Text;
                employeeInformation.FatherName = fatherNameTextBox.Text;
                employeeInformation.MotherName = motherNameTextBox.Text;
                employeeInformation.Religion = religionDropDownList.Text;
                employeeInformation.Nationality = nationalityDropDownList.SelectedItem.Text;
                employeeInformation.DateOfBirth = Convert.ToDateTime(birthdt);
                employeeInformation.PlaceOfBirth = placeOfBirthTextBox.Text;

                if (bloodGroupDropDownList.SelectedIndex > 0)
                {
                    employeeInformation.BloodGroup = bloodGroupDropDownList.Text;
                }
                
                employeeInformation.Gender = genderDropDownList.Text;
                employeeInformation.AddressPresent = prtAddressTextBox.Text;
                employeeInformation.AddressPermanent = permAddressTextBox.Text;
                employeeInformation.MedicalInformation = medicalDropDownList.Text;
                employeeInformation.PhoneNo = phoneNoTextBox.Text;
                employeeInformation.CellNumber = mobileNoTextBox.Text;
                if (emailTextBox.Text!=string.Empty)
                {
                    employeeInformation.Email = emailTextBox.Text;    
                }
                else
                {
                    employeeInformation.Email = " ";
                }
                
                employeeInformation.MaritalStatus = maritalStatusDropDownList.Text;
                employeeInformation.NationalIdNo = nationalIdNoTextBox.Text;
                employeeInformation.SpouseName = sposNameTextBox.Text;
                employeeInformation.SpouseDateOfBirth = sposeBirthDtTextBox.Text;
                employeeInformation.RefName = referanceNameTextBox.Text;
                employeeInformation.RefAddress = referanceAddressTextBox.Text;
                employeeInformation.RefCellNo = refranceCellNoTextBox.Text;
                employeeInformation.CompanyInfoId = Convert.ToInt32(comNameDropDownList.SelectedValue);
                employeeInformation.UnitId = Convert.ToInt32(unitNameDropDownList.SelectedValue);
                employeeInformation.DivisionId = Convert.ToInt32(divisNamDropDownList.SelectedValue);
                employeeInformation.DepId = Convert.ToInt32(departmentDropDownList.SelectedValue);
                employeeInformation.SectionId = Convert.ToInt32(sectionDropDownList.SelectedValue);
                employeeInformation.EmpGradeId = Convert.ToInt32(empGradeDropDownList.SelectedValue);
                employeeInformation.SalScaleId = Convert.ToInt32(salScaleNameDropDownList.SelectedValue);
                employeeInformation.DesigId = Convert.ToInt32(desigDropDownList.SelectedValue);
                employeeInformation.EmpTypeId = Convert.ToInt32(employmentTypeDropDownList.SelectedValue);
                employeeInformation.ShiftId = Convert.ToInt32(shiftDropDownList.SelectedValue);
                employeeInformation.JoiningDate = Convert.ToDateTime(joiningDateTextBox.Text);
                employeeInformation.PayType = payTypeDropDownList.SelectedItem.Text;
                employeeInformation.EmployeeStatus = "Active";
                employeeInformation.ProbationPeriodTo = Convert.ToDateTime((probitionDateTextBox.Text));
                employeeInformation.ConfirmationDate = Convert.ToDateTime((ConfirmationdateTextBox.Text));
                employeeInformation.Remarks = remarksTextBox.Text;
                employeeInformation.ActionStatus = "Posted";
                employeeInformation.Age = ageTextBox.Text;
                employeeInformation.OTAllow = otAllowDropDownList.SelectedItem.Text;
                employeeInformation.CardNo = cardNoTextBox.Text.Trim();

                employeeInformation.JobLocationId = ddlJobLocation.SelectedValue != "" ? Convert.ToInt32(ddlJobLocation.SelectedValue) : Convert.ToInt32(0);

                if (catNameDropDownList.SelectedValue != "")
                {
                    employeeInformation.EmpCategoryId = Convert.ToInt32(catNameDropDownList.SelectedValue);
                }
                else
                {
                    employeeInformation.EmpCategoryId = Convert.ToInt32("0");
                }
                employeeInformation.EmergencycontactNumber = EmergencyContactNumberTextBox.Text;
                employeeInformation.EmergencycontactPerson = EmergencyContactPersonTextBox.Text;
                if (bankNameDropDownList.SelectedValue == "")
                {
                    employeeInformation.BankId = Convert.ToInt32("0");
                }
                else
                {
                    employeeInformation.BankId = Convert.ToInt32(bankNameDropDownList.SelectedValue);
                }
                if (bankAccNoTextBox.Text == string.Empty)
                {
                    bankAccNoTextBox.Text = "";
                }
                else
                {
                    employeeInformation.BankAccNo = bankAccNoTextBox.Text;
                }
                employeeInformation.TINNo = tinNoTextBox.Text;
                employeeInformation.ShiftEmployee = shiftEmployeeDropDownList.SelectedItem.Text;
                EmpGeneralInfoBLL aEmpGeneralInfoBll = new EmpGeneralInfoBLL();

                if (!aEmpGeneralInfoBll.UpdateDataForEmpGeneralInfo(employeeInformation))
                {
                    showMessageBox("Data Not Update!!!");
                }
                else
                {
                    empIdHiddenField.Value = HiddenFieldempid.Value;
                    WeeklyHoliday aWeeklyHoliday = new WeeklyHoliday();
                    aWeeklyHoliday.EmpId = Convert.ToInt32(empIdHiddenField.Value);
                    if (dayqRadioButtonList.Items[0].Selected == true)
                    {
                        aWeeklyHoliday.FirstHolidayName = fdayDropDownList.SelectedItem.Text;
                        aWeeklyHoliday.SecondHolidayName = "NONE";
                        aWeeklyHoliday.DayQty = "1";
                    }
                    if (dayqRadioButtonList.Items[0].Selected == true && dayqRadioButtonList.Items[1].Selected == true)
                    {
                        aWeeklyHoliday.FirstHolidayName = fdayDropDownList.SelectedItem.Text;
                        aWeeklyHoliday.SecondHolidayName = sdayDropDownList.SelectedItem.Text;
                        aWeeklyHoliday.DayQty = "2";
                    }
                    aInfoBll.UpdateWeeklyHoliday(aWeeklyHoliday);

                    List<EmpEducationInfo> aEducationInfoList = new List<EmpEducationInfo>();

                    for (int i = 0; i < loadGridView.Rows.Count; i++)
                    {
                        EmpEducationInfo aEmpEducationInfo = new EmpEducationInfo();
                        aEmpEducationInfo.BoardUniverName = loadGridView.Rows[i].Cells[0].Text;
                        aEmpEducationInfo.Exam = loadGridView.Rows[i].Cells[1].Text;
                        aEmpEducationInfo.PassYear = loadGridView.Rows[i].Cells[2].Text;
                        aEmpEducationInfo.Qualification = loadGridView.Rows[i].Cells[3].Text;
                        aEmpEducationInfo.AreaStudy = loadGridView.Rows[i].Cells[4].Text;
                        aEmpEducationInfo.Result = loadGridView.Rows[i].Cells[5].Text;
                        aEmpEducationInfo.ResultType = loadGridView.Rows[i].Cells[6].Text;
                        aEmpEducationInfo.EduInstituteId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString());
                        aEmpEducationInfo.ExamId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString());
                        aEmpEducationInfo.QualificationId = Convert.ToInt32(loadGridView.DataKeys[i][2].ToString());
                        aEmpEducationInfo.StudyId = Convert.ToInt32(loadGridView.DataKeys[i][3].ToString());
                        aEmpEducationInfo.EmpInfoId = (int) employeeInformation.EmpInfoId; ;
                        aEducationInfoList.Add(aEmpEducationInfo);

                    }

                    List<EmpTrainingInfo> aTrainingInfoList = new List<EmpTrainingInfo>();

                    for (int i = 0; i < trainingloadGridView.Rows.Count; i++)
                    {
                        EmpTrainingInfo aTrainingInfo = new EmpTrainingInfo();
                        aTrainingInfo.TrainingName = trainingloadGridView.Rows[i].Cells[0].Text;
                        aTrainingInfo.InstituteName = trainingloadGridView.Rows[i].Cells[1].Text;
                        aTrainingInfo.Subject = trainingloadGridView.Rows[i].Cells[2].Text;
                        aTrainingInfo.Duration = trainingloadGridView.Rows[i].Cells[3].Text;
                        aTrainingInfo.Result = trainingloadGridView.Rows[i].Cells[4].Text;
                        aTrainingInfo.FromDate = Convert.ToDateTime(trainingloadGridView.Rows[i].Cells[5].Text);
                        aTrainingInfo.ToDate = Convert.ToDateTime(trainingloadGridView.Rows[i].Cells[6].Text);
                        aTrainingInfo.Country = trainingloadGridView.Rows[i].Cells[7].Text;
                        aTrainingInfo.EmpInfoId =  (int)employeeInformation.EmpInfoId; ;
                        aTrainingInfoList.Add(aTrainingInfo);
                    }

                    List<JobExperiancInfo> aJobExperiancInfoList = new List<JobExperiancInfo>();
                    for (int i = 0; i < jobloadGridView.Rows.Count; i++)
                    {
                        JobExperiancInfo aJobExperiancInfo = new JobExperiancInfo();
                        aJobExperiancInfo.CompanyName = jobloadGridView.Rows[i].Cells[0].Text;
                        aJobExperiancInfo.Designation = jobloadGridView.Rows[i].Cells[1].Text;
                        aJobExperiancInfo.Department = jobloadGridView.Rows[i].Cells[2].Text;
                        aJobExperiancInfo.FromDate = Convert.ToDateTime(jobloadGridView.Rows[i].Cells[3].Text);
                        aJobExperiancInfo.ToDate = Convert.ToDateTime(jobloadGridView.Rows[i].Cells[4].Text);
                        aJobExperiancInfo.Duration = jobloadGridView.Rows[i].Cells[5].Text;
                        aJobExperiancInfo.EmpInfoId =  (int)employeeInformation.EmpInfoId;
                        aJobExperiancInfoList.Add(aJobExperiancInfo);

                    }
                    EmpEducationInfo aEmpEducationInfos = new EmpEducationInfo();
                    aEmpEducationInfos.EmpInfoId = Convert.ToInt32(empIdHiddenField.Value);
                    if (iseducationCheckBox.Checked)
                    {
                        if (aInfoBll.DeleteEmpEducationInfo(aEmpEducationInfos))
                        {

                        }
                        if (aInfoBll.SaveEmpEducationInfo(aEducationInfoList))
                        {

                        }    
                    }
                    
                    if (jobCheckBox.Checked)
                    {
                        JobExperiancInfo aExperiancInfo = new JobExperiancInfo();
                        aExperiancInfo.EmpInfoId = Convert.ToInt32(empIdHiddenField.Value);
                        if (aInfoBll.DeleteEmpJobExperianceInfo(aExperiancInfo))
                        {

                        }
                        if (aInfoBll.SaveEmpJobExperianceInfo(aJobExperiancInfoList))
                        {
                            if (trainingCheckBox.Checked == false)
                            {

                                showMessageBox("Data Update Successfully!!!");
                            }

                        }

                    }
                    if (trainingCheckBox.Checked)
                    {
                        EmpTrainingInfo aTrainingInfo = new EmpTrainingInfo();
                        aTrainingInfo.EmpInfoId = Convert.ToInt32(empIdHiddenField.Value);
                        if (aInfoBll.DeleteEmpTrainingeInfo(aTrainingInfo))
                        {

                        }
                        if (aInfoBll.SaveEmpTreningInfo(aTrainingInfoList))
                        {
                            showMessageBox("Data Update Successfully!!!");
                        }
                    }

                    if (trainingCheckBox.Checked == false && jobCheckBox.Checked == false)
                    {
                        showMessageBox("Data Update Successfully!!!");
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                   "alert",
                   "alert('Data Updated Successfully...');window.location ='EmployeeEditAfterApproval.aspx';",
                   true);
                Clear();
            }
            else
            {
                showMessageBox("Please input data in all Textbox");
            }
            
        }
        catch (Exception)
        {
            
            showMessageBox("Please Input Valid Data!!");
        }
        
    }

    private void EmpGeneralInfoLoad(string employeeId)
    {
        EmpGeneralInfo aInfo = new EmpGeneralInfo();
        aInfo = aInfoBll.EmpGeneralInfoEditLoad(employeeId);
        try
        {
            comNameDropDownList.SelectedValue = aInfo.CompanyInfoId.ToString();
        }
        catch (Exception ex)
        {
        }
        
        try
        {
            aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
        }
        catch (Exception ex)
        {
        }
        try
        {
            unitNameDropDownList.SelectedValue = aInfo.UnitId.ToString();
        }
        catch (Exception ex)
        {
        }

        try
        {
            ddlJobLocation.SelectedValue = aInfo.JobLocationId.ToString();
        }
        catch (Exception ex)
        {
        }

        try
        {
            divisNamDropDownList.SelectedValue = aInfo.DivisionId.ToString();
        }
        catch (Exception ex)
        {
        }
        
        
        try
        {
            aInfoBll.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
        }
        catch (Exception ex)
        {
        }
        try
        {
            departmentDropDownList.SelectedValue = aInfo.DepId.ToString();
        }
        catch (Exception ex)
        {
        }
        try
        {
            aInfoBll.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
        }
        catch (Exception ex)
        {
        }
        
        try
        {
            sectionDropDownList.SelectedValue = aInfo.SectionId.ToString();
        }
        catch (Exception ex)
        {
        }
        
        try
        {
            empGradeDropDownList.SelectedValue = aInfo.EmpGradeId.ToString();
        }
        catch (Exception ex)
        {
        } try
        {
            aInfoBll.LoadDesignationToDropDownBLL(desigDropDownList, empGradeDropDownList.SelectedValue);
        }
        catch (Exception ex)
        {
        }
        
        try
        {
            salScaleNameDropDownList.SelectedValue = aInfo.SalScaleId.ToString();
        }
        catch (Exception ex)
        {
        }
        bankAccNoTextBox.Text = aInfo.BankAccNo;
        
        try
        {
            bankNameDropDownList.SelectedValue = aInfo.BankId.ToString();
        }
        catch (Exception ex)
        {
        }
        
        try
        {
            desigDropDownList.SelectedValue = aInfo.DesigId.ToString();
        }
        catch (Exception ex)
        {
        }

        try
        {
            shiftDropDownList.SelectedValue = aInfo.ShiftId.ToString();
        }
        catch (Exception ex)
        {
        }
        
        try
        {
            employmentTypeDropDownList.SelectedValue = aInfo.EmpTypeId.ToString();
        }
        catch (Exception ex)
        {
        }
        referanceNameTextBox.Text = aInfo.RefName;
        referanceAddressTextBox.Text = aInfo.RefAddress;
        refranceCellNoTextBox.Text = aInfo.RefCellNo;
        empNameTextBox.Text = aInfo.EmpName;
        shortNameTextBox.Text = aInfo.ShortName;
        fatherNameTextBox.Text = aInfo.FatherName;
        motherNameTextBox.Text = aInfo.MotherName;
        religionDropDownList.Text = aInfo.Religion;
        nationalityDropDownList.Text = aInfo.Nationality;
        placeOfBirthTextBox.Text = aInfo.PlaceOfBirth;
        bloodGroupDropDownList.Text = aInfo.BloodGroup;
        genderDropDownList.Text = aInfo.Gender;
        prtAddressTextBox.Text = aInfo.AddressPresent;
        permAddressTextBox.Text = aInfo.AddressPermanent;
        medicalDropDownList.Text = aInfo.MedicalInformation;
        phoneNoTextBox.Text = aInfo.PhoneNo;
        mobileNoTextBox.Text = aInfo.CellNumber;
        emailTextBox.Text = aInfo.Email;
        maritalStatusDropDownList.Text = aInfo.MaritalStatus;
        nationalIdNoTextBox.Text = aInfo.NationalIdNo;
        sposNameTextBox.Text = aInfo.SpouseName;
        try
        {
            sposeBirthDtTextBox.Text = Convert.ToDateTime(aInfo.SpouseDateOfBirth).ToString("dd-MMM-yyyy");
        }
        catch (Exception ex)
        {
        }

        try
        {
            joiningDateTextBox.Text = string.IsNullOrEmpty(aInfo.JoiningDate.ToString()) ? String.Empty : aInfo.JoiningDate.Value.ToString("dd-MMM-yyyy");
        }
        catch (Exception ex)
        {
        }
        try
        {
            employmentTypeDropDownList.Text = aInfo.EmpTypeId.ToString();
        }
        catch (Exception ex)
        {
        }
        try
        {
            payTypeDropDownList.SelectedValue = aInfo.PayType;
        }
        catch (Exception ex)
        {
        }
        
        
        //empStatusDropDownList.Text = aInfo.EmployeeStatus;
        for (int i = 0; i < payTypeDropDownList.Items.Count; i++)
        {
            if (payTypeDropDownList.Items[i].Text==aInfo.PayType)
            {
                payTypeDropDownList.SelectedIndex = i;
            }
        }

        probitionDateTextBox.Text = string.IsNullOrEmpty(aInfo.ProbationPeriodTo.ToString()) ? String.Empty : aInfo.ProbationPeriodTo.Value.ToString("dd-MMM-yyyy");
        shiftEmployeeDropDownList.SelectedItem.Text = aInfo.ShiftEmployee.ToString();
        ConfirmationdateTextBox.Text = string.IsNullOrEmpty(aInfo.ConfirmationDate.ToString()) ? String.Empty : aInfo.ConfirmationDate.Value.ToString("dd-MMM-yyyy");
        empNameTextBox.Text = aInfo.EmpName;
        remarksTextBox.Text = aInfo.Remarks;
        ageTextBox.Text = aInfo.Age;
        entryDateTextBox.Text = aInfo.EntryDate.ToString("dd-MMM-yyyy");
        otAllowDropDownList.SelectedItem.Text = aInfo.OTAllow;
        nationalityDropDownList.SelectedItem.Text = aInfo.Nationality;
        cardNoTextBox.Text = aInfo.CardNo;
        catNameDropDownList.SelectedValue = aInfo.EmpCategoryId.ToString();
        EmergencyContactNumberTextBox.Text = aInfo.EmergencycontactNumber;
        EmergencyContactPersonTextBox.Text = aInfo.EmergencycontactPerson;
        tinNoTextBox.Text = aInfo.TINNo;

        if (aInfo.DateOfBirth != null)
        {
            Date(Convert.ToDateTime(aInfo.DateOfBirth));
        }
        


    }

    public void Date(DateTime date)
    {
        string datetime = date.ToString("dd-MM-yyyy");

        if (datetime.Contains("-"))
        {
            string[] dates = datetime.Split('-');
            dayDropDownList.SelectedItem.Text = dates[0];
            monthDropDownList.SelectedValue = dates[1];
            yearDropDownList.SelectedItem.Text = dates[2];
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }

    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
    }

    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
    protected void divisNamDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
    }
    protected void empGradeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDesignationToDropDownBLL(desigDropDownList, empGradeDropDownList.SelectedValue);
    }


    public bool EntryDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(entryDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool SpouseDateofBirth()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(sposeBirthDtTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool JoiningDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(joiningDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool Probitiondate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(probitionDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool ConfirmationDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(ConfirmationdateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    private bool EducationValidation()
    {

        if (boardDropDownList.SelectedValue == null)
        {
            showMessageBox("Please Input Board or University Name !!");
            return false;
        }

        if (passYearDropDownList.SelectedValue == "1")
        {
            showMessageBox("Please Input Year of Passing !!");
            return false;
        }
        if (areaStudyDropDownList.SelectedValue == null)
        {
            showMessageBox("Please Input Area of Study !!");
            return false;
        }
        if (examDropDownList.SelectedValue == null)
        {
            showMessageBox("Please Input Exam of Degree Title !!");
            return false;
        }

        if (qualificationiDropDownList.SelectedValue == null)
        {
            showMessageBox("Please Input Qualification !!");
            return false;
        }

        if (cgpaTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Result !!");
            return false;
        }
        if (resultTypeDropDownList.SelectedValue == "1")
        {
            showMessageBox("Please Input Result Type !!");
            return false;
        }

        return true;
    }

    private bool JobExpValidation()
    {

        if (comNameTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Company Info !!");
            return false;
        }

        if (desigTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Designation !!");
            return false;
        }
        if (fromDateTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input From Date !!");
            return false;
        }
        if (toDateTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input To Date !!");
            return false;
        }

        if (jobdurationTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Duration !!");
            return false;
        }

        return true;
    }

    private bool TrainingValidation()
    {

        if (trainingNameTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Training Name !!");
            return false;
        }

        if (subjectTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Subject Name !!");
            return false;
        }
        if (resultTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input From Date !!");
            return false;
        }
        if (instNameTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Institution Name !!");
            return false;
        }

        if (fromtrainDateTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input From Date !!");
            return false;
        }

        if (totrainDateTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input To Date !!");
            return false;
        }

        if (durationTextBox.Text == string.Empty)
        {
            showMessageBox("Please Input Duration !!");
            return false;
        }

        return true;
    }
    public void ClearEdu()
    {
        boardDropDownList.SelectedValue = null;
        passYearDropDownList.SelectedValue = "1";
        areaStudyDropDownList.SelectedValue = null;
        examDropDownList.SelectedValue = null;
        qualificationiDropDownList.SelectedValue = null;
        cgpaTextBox.Text = string.Empty;
        resultTypeDropDownList.SelectedValue = "1";
    }
    public void CLearJob()
    {
        comNameTextBox.Text = string.Empty;
        desigTextBox.Text = string.Empty;
        deptTextBox.Text = string.Empty;
        fromDateTextBox.Text = string.Empty;
        toDateTextBox.Text = string.Empty;
        jobdurationTextBox.Text = string.Empty;
    }
    public void ClearTraining()
    {
        trainingNameTextBox.Text = string.Empty;
        instNameTextBox.Text = string.Empty;
        subjectTextBox.Text = string.Empty;
        resultTextBox.Text = string.Empty;
        durationTextBox.Text = string.Empty;
        fromtrainDateTextBox.Text = string.Empty;
        totrainDateTextBox.Text = string.Empty;
    }

    protected void addtolistButton_Click(object sender, EventArgs e)
    {
        if (EducationValidation())
        {


            string institute = boardDropDownList.SelectedItem.Text;
            string yearpassing = passYearDropDownList.SelectedItem.Text;
            string qualification = qualificationiDropDownList.SelectedItem.Text;
            string areaofstudy = areaStudyDropDownList.SelectedItem.Text;
            string result = cgpaTextBox.Text;
            string type = resultTypeDropDownList.SelectedItem.Text;
            string exam = examDropDownList.SelectedItem.Text;
            string EduInstituteId = boardDropDownList.SelectedValue;
            string ExamId = examDropDownList.SelectedValue;
            string QualificationId = qualificationiDropDownList.SelectedValue;
            string StudyId = areaStudyDropDownList.SelectedValue;

            DataTable aDataTable = new DataTable();

            aDataTable.Columns.Add("Institute");
            aDataTable.Columns.Add("Exam");
            aDataTable.Columns.Add("PassingYear");
            aDataTable.Columns.Add("Qualification");
            aDataTable.Columns.Add("AreaStudy");
            aDataTable.Columns.Add("Result");
            aDataTable.Columns.Add("ResultType");
            aDataTable.Columns.Add("EduInstituteId");
            aDataTable.Columns.Add("ExamId");
            aDataTable.Columns.Add("QualificationId");
            aDataTable.Columns.Add("StudyId");

            DataRow dataRow;

            if (loadGridView.Rows.Count > 0)
            {
                for (int i = 0; i < loadGridView.Rows.Count; i++)
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["Institute"] = loadGridView.Rows[i].Cells[0].Text;
                    dataRow["Exam"] = loadGridView.Rows[i].Cells[1].Text;
                    dataRow["PassingYear"] = loadGridView.Rows[i].Cells[2].Text;
                    dataRow["Qualification"] = loadGridView.Rows[i].Cells[3].Text;
                    dataRow["AreaStudy"] = loadGridView.Rows[i].Cells[4].Text;
                    dataRow["Result"] = loadGridView.Rows[i].Cells[5].Text;
                    dataRow["ResultType"] = loadGridView.Rows[i].Cells[6].Text;
                    dataRow["EduInstituteId"] = loadGridView.DataKeys[i][0].ToString();
                    dataRow["ExamId"] = loadGridView.DataKeys[i][1].ToString();
                    dataRow["QualificationId"] = loadGridView.DataKeys[i][2].ToString();
                    dataRow["StudyId"] = loadGridView.DataKeys[i][3].ToString();

                    aDataTable.Rows.Add(dataRow);
                }

                dataRow = aDataTable.NewRow();
                dataRow["Institute"] = institute;
                dataRow["Exam"] = exam;
                dataRow["PassingYear"] = yearpassing;
                dataRow["Qualification"] = qualification;
                dataRow["AreaStudy"] = areaofstudy;
                dataRow["Result"] = result;
                dataRow["ResultType"] = type;
                dataRow["EduInstituteId"] = EduInstituteId;
                dataRow["ExamId"] = ExamId;
                dataRow["QualificationId"] = QualificationId;
                dataRow["StudyId"] = StudyId;

                aDataTable.Rows.Add(dataRow);
                loadGridView.DataSource = aDataTable;
                loadGridView.DataBind();
                ClearEdu();

            }
            else
            {
                dataRow = aDataTable.NewRow();
                dataRow["Institute"] = institute;
                dataRow["Exam"] = exam;
                dataRow["PassingYear"] = yearpassing;
                dataRow["Qualification"] = qualification;
                dataRow["AreaStudy"] = areaofstudy;
                dataRow["Result"] = result;
                dataRow["ResultType"] = type;
                dataRow["EduInstituteId"] = EduInstituteId;
                dataRow["ExamId"] = ExamId;
                dataRow["QualificationId"] = QualificationId;
                dataRow["StudyId"] = StudyId;

                aDataTable.Rows.Add(dataRow);
                loadGridView.DataSource = aDataTable;
                loadGridView.DataBind();
                ClearEdu();
            }
        }
    }
    public void DropdownListEducational()
    {
        aInfoBll.LoadAreaStudy(areaStudyDropDownList);
        aInfoBll.LoadBoardName(boardDropDownList);
        aInfoBll.LoadExam(examDropDownList);
        aInfoBll.LoadQualificationName(qualificationiDropDownList);
    }
    protected void deleteImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ItemCodeTextBox = (ImageButton)sender;
        GridViewRow currentRow = (GridViewRow)ItemCodeTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("Institute");
        aDataTable.Columns.Add("Exam");
        aDataTable.Columns.Add("PassingYear");
        aDataTable.Columns.Add("Qualification");
        aDataTable.Columns.Add("AreaStudy");
        aDataTable.Columns.Add("Result");
        aDataTable.Columns.Add("ResultType");
        aDataTable.Columns.Add("EduInstituteId");
        aDataTable.Columns.Add("ExamId");
        aDataTable.Columns.Add("QualificationId");
        aDataTable.Columns.Add("StudyId");

        DataRow dataRow;

        if (loadGridView.Rows.Count > 0)
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                if (i != rowindex)
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["Institute"] = loadGridView.Rows[i].Cells[0].Text;
                    dataRow["Exam"] = loadGridView.Rows[i].Cells[1].Text;
                    dataRow["PassingYear"] = loadGridView.Rows[i].Cells[2].Text;
                    dataRow["Qualification"] = loadGridView.Rows[i].Cells[3].Text;
                    dataRow["AreaStudy"] = loadGridView.Rows[i].Cells[4].Text;
                    dataRow["Result"] = loadGridView.Rows[i].Cells[5].Text;
                    dataRow["ResultType"] = loadGridView.Rows[i].Cells[6].Text;
                    dataRow["EduInstituteId"] = loadGridView.DataKeys[i][0].ToString();
                    dataRow["ExamId"] = loadGridView.DataKeys[i][1].ToString();
                    dataRow["QualificationId"] = loadGridView.DataKeys[i][2].ToString();
                    dataRow["StudyId"] = loadGridView.DataKeys[i][3].ToString();

                    aDataTable.Rows.Add(dataRow);

                }
            }
        }

        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void jobExAddtolistButton_Click(object sender, EventArgs e)
    {
        if (JobExpValidation())
        {


            string companyName = comNameTextBox.Text;
            string Designation = desigTextBox.Text;
            string Department = deptTextBox.Text;
            string FromDate = fromDateTextBox.Text;
            string ToDate = toDateTextBox.Text;
            string duration = jobdurationTextBox.Text;

            DataTable aDataTable = new DataTable();

            aDataTable.Columns.Add("CompanyName");
            aDataTable.Columns.Add("Designation");
            aDataTable.Columns.Add("Department");
            aDataTable.Columns.Add("FromDate");
            aDataTable.Columns.Add("ToDate");
            aDataTable.Columns.Add("Duration");

            DataRow dataRow;

            if (jobloadGridView.Rows.Count > 0)
            {
                for (int i = 0; i < jobloadGridView.Rows.Count; i++)
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["CompanyName"] = jobloadGridView.Rows[i].Cells[0].Text;
                    dataRow["Designation"] = jobloadGridView.Rows[i].Cells[1].Text;
                    dataRow["Department"] = jobloadGridView.Rows[i].Cells[2].Text;
                    dataRow["FromDate"] = jobloadGridView.Rows[i].Cells[3].Text;
                    dataRow["ToDate"] = jobloadGridView.Rows[i].Cells[4].Text;
                    dataRow["Duration"] = jobloadGridView.Rows[i].Cells[5].Text;

                    aDataTable.Rows.Add(dataRow);
                }

                dataRow = aDataTable.NewRow();
                dataRow["CompanyName"] = companyName;
                dataRow["Designation"] = Designation;
                dataRow["Department"] = Department;
                dataRow["FromDate"] = FromDate;
                dataRow["ToDate"] = ToDate;
                dataRow["Duration"] = duration;

                aDataTable.Rows.Add(dataRow);
                jobloadGridView.DataSource = aDataTable;
                jobloadGridView.DataBind();
                CLearJob();
            }
            else
            {
                dataRow = aDataTable.NewRow();
                dataRow["CompanyName"] = companyName;
                dataRow["Designation"] = Designation;
                dataRow["Department"] = Department;
                dataRow["FromDate"] = FromDate;
                dataRow["ToDate"] = ToDate;
                dataRow["Duration"] = duration;

                aDataTable.Rows.Add(dataRow);
                jobloadGridView.DataSource = aDataTable;
                jobloadGridView.DataBind();
                CLearJob();
            }
        }
    }

    protected void deleteJobImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ItemCodeTextBox = (ImageButton)sender;
        GridViewRow currentRow = (GridViewRow)ItemCodeTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("CompanyName");
        aDataTable.Columns.Add("Designation");
        aDataTable.Columns.Add("PassingYear");
        aDataTable.Columns.Add("Department");
        aDataTable.Columns.Add("FromDate");
        aDataTable.Columns.Add("ToDate");
        aDataTable.Columns.Add("Duration");

        DataRow dataRow;

        if (jobloadGridView.Rows.Count > 0)
        {
            for (int i = 0; i < jobloadGridView.Rows.Count; i++)
            {
                if (i != rowindex)
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["CompanyName"] = jobloadGridView.Rows[i].Cells[0].Text;
                    dataRow["Designation"] = jobloadGridView.Rows[i].Cells[1].Text;
                    dataRow["Department"] = jobloadGridView.Rows[i].Cells[2].Text;
                    dataRow["FromDate"] = jobloadGridView.Rows[i].Cells[3].Text;
                    dataRow["ToDate"] = jobloadGridView.Rows[i].Cells[4].Text;
                    dataRow["Duration"] = jobloadGridView.Rows[i].Cells[5].Text;

                    aDataTable.Rows.Add(dataRow);

                }
            }
        }

        jobloadGridView.DataSource = aDataTable;
        jobloadGridView.DataBind();
    }

    protected void traningAddtolistButton_Click(object sender, EventArgs e)
    {
        if (TrainingValidation())
        {
            string TrainingName = trainingNameTextBox.Text;
            string InstituteName = instNameTextBox.Text;
            string Subject = subjectTextBox.Text;
            string Duration = durationTextBox.Text;
            string Result = resultTextBox.Text;
            string fromdt = fromtrainDateTextBox.Text;
            string todate = totrainDateTextBox.Text;
            string country = countryDropDownList.SelectedItem.Text;

            DataTable aDataTable = new DataTable();

            aDataTable.Columns.Add("TrainingName");
            aDataTable.Columns.Add("InstituteName");
            aDataTable.Columns.Add("Subject");
            aDataTable.Columns.Add("Duration");
            aDataTable.Columns.Add("Result");
            aDataTable.Columns.Add("FromDate");
            aDataTable.Columns.Add("ToDate");
            aDataTable.Columns.Add("Country");

            DataRow dataRow;

            if (trainingloadGridView.Rows.Count > 0)
            {
                for (int i = 0; i < trainingloadGridView.Rows.Count; i++)
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["TrainingName"] = trainingloadGridView.Rows[i].Cells[0].Text;
                    dataRow["InstituteName"] = trainingloadGridView.Rows[i].Cells[1].Text;
                    dataRow["Subject"] = trainingloadGridView.Rows[i].Cells[2].Text;
                    dataRow["Duration"] = trainingloadGridView.Rows[i].Cells[3].Text;
                    dataRow["Result"] = trainingloadGridView.Rows[i].Cells[4].Text;
                    dataRow["FromDate"] = trainingloadGridView.Rows[i].Cells[5].Text;
                    dataRow["ToDate"] = trainingloadGridView.Rows[i].Cells[6].Text;
                    dataRow["Country"] = trainingloadGridView.Rows[i].Cells[7].Text;

                    aDataTable.Rows.Add(dataRow);
                }

                dataRow = aDataTable.NewRow();
                dataRow["TrainingName"] = TrainingName;
                dataRow["InstituteName"] = InstituteName;
                dataRow["Subject"] = Subject;
                dataRow["Duration"] = Duration;
                dataRow["Result"] = Result;
                dataRow["FromDate"] = fromdt;
                dataRow["ToDate"] = todate;
                dataRow["Country"] = country;

                aDataTable.Rows.Add(dataRow);
                trainingloadGridView.DataSource = aDataTable;
                trainingloadGridView.DataBind();
                ClearTraining();
            }
            else
            {
                dataRow = aDataTable.NewRow();
                dataRow["TrainingName"] = TrainingName;
                dataRow["InstituteName"] = InstituteName;
                dataRow["Subject"] = Subject;
                dataRow["Duration"] = Duration;
                dataRow["Result"] = Result;
                dataRow["FromDate"] = fromdt;
                dataRow["ToDate"] = todate;
                dataRow["Country"] = country;

                aDataTable.Rows.Add(dataRow);
                trainingloadGridView.DataSource = aDataTable;
                trainingloadGridView.DataBind();
                ClearTraining();
            }
        }
    }

    protected void deleteTrainingImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ItemCodeTextBox = (ImageButton)sender;
        GridViewRow currentRow = (GridViewRow)ItemCodeTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("TrainingName");
        aDataTable.Columns.Add("InstituteName");
        aDataTable.Columns.Add("Subject");
        aDataTable.Columns.Add("Duration");
        aDataTable.Columns.Add("Result");
        aDataTable.Columns.Add("FromDate");
        aDataTable.Columns.Add("ToDate");
        aDataTable.Columns.Add("Country");

        DataRow dataRow;

        if (trainingloadGridView.Rows.Count > 0)
        {
            for (int i = 0; i < trainingloadGridView.Rows.Count; i++)
            {
                if (i != rowindex)
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["TrainingName"] = trainingloadGridView.Rows[i].Cells[0].Text;
                    dataRow["InstituteName"] = trainingloadGridView.Rows[i].Cells[1].Text;
                    dataRow["Subject"] = trainingloadGridView.Rows[i].Cells[2].Text;
                    dataRow["Duration"] = trainingloadGridView.Rows[i].Cells[3].Text;
                    dataRow["Result"] = trainingloadGridView.Rows[i].Cells[4].Text;
                    dataRow["FromDate"] = trainingloadGridView.Rows[i].Cells[5].Text;
                    dataRow["ToDate"] = trainingloadGridView.Rows[i].Cells[6].Text;
                    dataRow["Country"] = trainingloadGridView.Rows[i].Cells[7].Text;

                    aDataTable.Rows.Add(dataRow);

                }
            }
        }

        trainingloadGridView.DataSource = aDataTable;
        trainingloadGridView.DataBind();
    }
    public void JobDuration()
    {
        if (fromDateTextBox.Text != "")
        {

            DateTime fromdt = Convert.ToDateTime(fromDateTextBox.Text);
            DateTime todt = Convert.ToDateTime(toDateTextBox.Text);

            DateTime duration = new DateTime(todt.Subtract(fromdt).Ticks);

            jobdurationTextBox.Text = (duration.Year - 1) + " years " + duration.Month.ToString() + " months " + duration.Day.ToString() + " days ";

        }
        else
        {
            showMessageBox("Please input From Date of Job Experience");
        }
    }

    public void TrainingDuration()
    {
        if (fromtrainDateTextBox.Text != "")
        {

            DateTime fromdt = Convert.ToDateTime(fromtrainDateTextBox.Text);
            DateTime todt = Convert.ToDateTime(totrainDateTextBox.Text);

            DateTime duration = new DateTime(todt.Subtract(fromdt).Ticks);

            durationTextBox.Text = (duration.Year - 1) + " years " + duration.Month.ToString() + " months " + duration.Day.ToString() + " days ";

        }
        else
        {
            showMessageBox("Please input From Date of Training");
        }
    }

    protected void toDateTextBox_TextChanged(object sender, EventArgs e)
    {
        JobDuration();
    }
    protected void totrainDateTextBox_TextChanged(object sender, EventArgs e)
    {
        TrainingDuration();
    }

    protected void jobCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (jobCheckBox.Checked)
        {
            divjob.Visible = true;
        }
        else
        {
            divjob.Visible = false;
        }
    }
    protected void trainingCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (trainingCheckBox.Checked)
        {
            divtraing.Visible = true;
        }
        else
        {
            divtraing.Visible = false;
        }
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {

        LoadPassYear();
        DropdownListEducational();
        DropDownList();
        DataTable dt = new DataTable();
        dt = aInfoBll.searchID(empidTextBox.Text);
        if (dt.Rows.Count > 0)
        {
            empIdHiddenField.Value = dt.Rows[0]["EmpInfoId"].ToString();
            HiddenFieldempid.Value = dt.Rows[0]["EmpInfoId"].ToString();
            EmpGeneralInfoLoad(empIdHiddenField.Value);
            LoadPassYear();
            WeeklyHoliday(empIdHiddenField.Value);
            LoadDateBirthYear();
            LoadDay();
            GridLoad();
            GridChekc();
            empIdHiddenField.Value = dt.Rows[0]["EmpInfoId"].ToString();
        }
        else
        {
            showMessageBox("No Data Found!!");
            Clear();
        }
        empIdHiddenField.Value = HiddenFieldempid.Value;
        SetDefaultData();
    }

    public void WeeklyHoliday(string empinfoId)
    {
        DataTable dt = aInfoBll.LoadWeeklyHoliday(empinfoId);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["DayQty"].ToString() == "1")
            {
                dayqRadioButtonList.SelectedIndex = 0;
            }
            if (dt.Rows[0]["DayQty"].ToString() == "2")
            {
                dayqRadioButtonList.SelectedIndex = 1;
            }

            if (dt.Rows[0]["FirstHolidayName"].ToString() != "NONE")
            {
                for (int i = 0; i < fdayDropDownList.Items.Count; i++)
                {
                    if (dt.Rows[0]["FirstHolidayName"].ToString() == fdayDropDownList.Items[i].Text)
                    {
                        fdayDropDownList.SelectedIndex = i;
                    }
                }
            }
            if (dt.Rows[0]["SecondHolidayName"].ToString() != "NONE")
            {
                for (int i = 0; i < sdayDropDownList.Items.Count; i++)
                {
                    if (dt.Rows[0]["SecondHolidayName"].ToString() == sdayDropDownList.Items[i].Text)
                    {
                        sdayDropDownList.SelectedIndex = i;
                    }
                }
            }
        }
        if (dayqRadioButtonList.Items[0].Selected == true)
        {
            divsday.Visible = false;
            divfday.Visible = true;
        }
        if (dayqRadioButtonList.Items[1].Selected == true)
        {
            divsday.Visible = true;
            divfday.Visible = true;
        }
    }
    protected void dayqRadioButtonList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dayqRadioButtonList.Items[0].Selected == true)
        {
            divsday.Visible = false;
            divfday.Visible = true;
        }
        if (dayqRadioButtonList.Items[1].Selected == true)
        {
            divsday.Visible = true;
            divfday.Visible = true;
        }
    }
    protected void fdayDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Name() == false)
        {
            showMessageBox("Weekly Holiday Must not Be Same");
            sdayDropDownList.SelectedValue = null;
        }
    }
    protected void sdayDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Name() == false)
        {
            showMessageBox("Weekly Holiday Must not Be Same");
            sdayDropDownList.SelectedValue = null;
        }
    }
    public bool Name()
    {
        if (fdayDropDownList.SelectedItem.Text == sdayDropDownList.SelectedItem.Text)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    protected void empidTextBox_TextChanged(object sender, EventArgs e)
    {

    }
    protected void joiningDateTextBox_TextChanged(object sender, EventArgs e)
    {
        probitionDateTextBox.Text = Convert.ToDateTime(joiningDateTextBox.Text).AddMonths(6).ToString("dd-MMM-yyyy");
        ConfirmationdateTextBox.Text = Convert.ToDateTime(probitionDateTextBox.Text).AddDays(1).ToString("dd-MMM-yyyy");
    }

    protected void empViewImageButton_Click(object sender, EventArgs e)
    {
        
    }

    protected void txtSearch_Click(object sender, EventArgs e)
    {
        
    }
}