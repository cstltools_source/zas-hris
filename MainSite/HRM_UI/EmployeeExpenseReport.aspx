﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="EmployeeExpenseReport.aspx.cs" Inherits="HRM_UI_EmployeeExpenseReport" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Employee Expense Report </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx"> All Reports </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Expense Report</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">

                            <div class="form-row">

                                <div class="col-2">
                                        <div class="form-group">
                                            <label> Company name </label>
                                            <asp:DropDownList ID="comNameDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm" OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Unit Name </label>
                                            <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                        </div>
                                    </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Master Code </label>
                                        
                                        <asp:TextBox ID="EmpMasterCodeTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Expense Year </label>
                                        
                                        <asp:DropDownList ID="yearDropDownList" runat="server" CssClass="form-control form-control-sm">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Expense Month </label>
                                        
                                        <asp:DropDownList ID="monthDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            <asp:ListItem Value=""> Select any one</asp:ListItem>
                                            <asp:ListItem Value="1">January</asp:ListItem>
                                            <asp:ListItem Value="2">February</asp:ListItem>
                                            <asp:ListItem Value="3">March</asp:ListItem>
                                            <asp:ListItem Value="4">April</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">June</asp:ListItem>
                                            <asp:ListItem Value="7">July</asp:ListItem>
                                            <asp:ListItem Value="8">August</asp:ListItem>
                                            <asp:ListItem Value="9">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                </div>
                                <div class="form-row">
                                 <div class="col-1">
                                    <div class="form-group">
                                        <label style="color: white">Search </label>
                                        <br />
                                        <asp:Button ID="searchButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="searchButton_Click" Text="View Report" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                </div>
                <asp:HiddenField ID="JobLeftIdHiddenField" runat="server" />
                <asp:HiddenField ID="empIdHiddenField" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

