﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_EmployeeExpenseReport : System.Web.UI.Page
{
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();
    ExpenseEntryDal aEntryDal = new ExpenseEntryDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        } 
    }

    public void DropDownList()
    {
        aInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        aEntryDal.LoadFinancialYear(yearDropDownList);       
    }

    
    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        PopUpCustomerPaymentReport();
    }

    private void PopUpCustomerPaymentReport()
    {
        Session["PRAM"] = "";
        Session["PRAM"] = GetPrameterList();

        const string url = "../Report_UI/ExpenseReportViewer.aspx";
        const string fullURL = "var Mleft = (screen.width/2)-(950/2);var Mtop = (screen.height/2)-(700/2);window.open( '" + url + "', null, 'height=700,width=950,status=yes,toolbar=no,addressbar=no, scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    private String GetPrameterList()
    {
        string parameter = "";

        if (unitNameDropDownList.SelectedValue != "")
        {
            parameter = parameter + "AND EG.UnitId = '" + unitNameDropDownList.SelectedValue + "'";
        }

        if (yearDropDownList.SelectedValue != "")
        {
            parameter = parameter + "AND ED.ExpenseYear = '" + yearDropDownList.SelectedValue + "'";
        }


        if (EmpMasterCodeTextBox.Text != "")
        {
            parameter = parameter + "AND EG.EmpMasterCode = '" + EmpMasterCodeTextBox.Text + "'";
        }

        if (monthDropDownList.SelectedValue != "")
        {
            parameter = parameter + "AND ExpenseMonth = '" + monthDropDownList.SelectedItem.Text.Trim() + "'";
        }

        //if (fromDateTextBox.Text != "" && toDateTextBox.Text != "")
        //{
        //    parameter = parameter + "AND I.InvoiceDate BETWEEN '" + Convert.ToDateTime(fromDateTextBox.Text.Trim()) + "' AND ' " + Convert.ToDateTime(toDateTextBox.Text.Trim()) + "'";
        //}


        return parameter;

    }
}