﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_EmployeeTypeEdit : System.Web.UI.Page
{
    EmployeeTypeBLL aEmployeeTypeBll = new EmployeeTypeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            empTypeIdHiddenField.Value = Request.QueryString["ID"];
            EmployeeTypeLoad(empTypeIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (empTypeTextBox.Text == "")
        {
            showMessageBox("Please Input EmployeeType !!!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmployeeType aEmployeeType = new EmployeeType()
            {
                EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value),
                EmpType = empTypeTextBox.Text,
            };

            if (!aEmployeeTypeBll.UpdateEmployeeType(aEmployeeType))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmployeeTypeLoad(string EmpTypeId)
    {
        EmployeeType aEmployeeType = new EmployeeType();
        aEmployeeType = aEmployeeTypeBll.EmployeeTypeEditLoad(EmpTypeId);
        empTypeTextBox.Text = aEmployeeType.EmpType;
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}