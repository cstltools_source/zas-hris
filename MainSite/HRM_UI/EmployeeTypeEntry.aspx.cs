﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_EmployeeTypeEntry : System.Web.UI.Page
{
    EmployeeTypeBLL aEmployeeTypeBll = new EmployeeTypeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       empTypeNameTextBox.Text = string.Empty;       
    }
    private bool Validation()
    {
        if (empTypeNameTextBox.Text == "")
        {
            showMessageBox("Please Input EmployeeType !!!");
            return false;
        }
        
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmployeeType aEmployeeType = new EmployeeType()
                 {
                    EmpType = empTypeNameTextBox.Text
                 };
          
            if (aEmployeeTypeBll.SaveDataForEmployeeType(aEmployeeType))
            {
                showMessageBox("Data Save Successfully  :" );
                Clear();
            }
            else
            {
                showMessageBox("EmployeeType Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("EmployeeTypeView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}