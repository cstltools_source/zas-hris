﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;
using DAO.HRM_Entities;

public partial class HRM_UI_ExpenseDetailsEntry : System.Web.UI.Page
{
    ExpenseEntryDal aEntryDal = new ExpenseEntryDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
            
        }
    }

    public void DropDownList()
    {
        aEntryDal.LoadFinancialYear(financialDropDownList);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {

        if (financialDropDownList.SelectedValue != "")
        {
            string pram = "";

            if (empCodeTextBox.Text != "")
            {
                pram = pram + " AND EMP.EmpMasterCode = '" + empCodeTextBox.Text.Trim() + "'";
            }
            DataTable dtempinfo = aEntryDal.GetEmployeeInformation(financialDropDownList.SelectedValue.Trim(), pram);

            loadGridView.DataSource = dtempinfo;
            loadGridView.DataBind();
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();

            ShowMessageBox("No information found !!");
        }

       
    }

    protected void confirmButton_Click(object sender, EventArgs e)
    {
        var button = (Button)sender;
        var currentRow = (GridViewRow)button.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        var dataKey = loadGridView.DataKeys[rowindex];
        
        if (dataKey != null)
        {

            ExpenseDetailsDao aDao = new ExpenseDetailsDao();

            aDao.FyrCode = Convert.ToInt32(financialDropDownList.SelectedValue.Trim());
            aDao.EmpInfoId = Convert.ToInt32(Convert.ToInt32(dataKey["EmpInfoId"].ToString()));
            aDao.January = Convert.ToInt32(Convert.ToInt32(dataKey["EmpInfoId"].ToString()));


            if (((TextBox)loadGridView.Rows[rowindex].Cells[4].FindControl("janTextBox")).Text != string.Empty)
            {
                aDao.January =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[4].FindControl("janTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[5].FindControl("febTextBox")).Text != string.Empty)
            {
                aDao.February =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[5].FindControl("febTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[15].FindControl("decTextBox")).Text != string.Empty)
            {
                aDao.December =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[15].FindControl("decTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[14].FindControl("novTextBox")).Text != string.Empty)
            {
                aDao.November =
               Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[14].FindControl("novTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[13].FindControl("octTextBox")).Text != string.Empty)
            {
                aDao.October =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[13].FindControl("octTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[12].FindControl("sepTextBox")).Text != string.Empty)
            {
                aDao.September =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[12].FindControl("sepTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[11].FindControl("augTextBox")).Text != string.Empty)
            {
                aDao.August =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[11].FindControl("augTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[10].FindControl("julTextBox")).Text != string.Empty)
            {
                aDao.July =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[10].FindControl("julTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[9].FindControl("junTextBox")).Text != string.Empty)
            {
                aDao.June =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[9].FindControl("junTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[8].FindControl("mayTextBox")).Text != string.Empty)
            {
                aDao.May =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[8].FindControl("mayTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[7].FindControl("aprTextBox")).Text != string.Empty)
            {
                aDao.April =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[7].FindControl("aprTextBox")).Text);
            }
            if (((TextBox)loadGridView.Rows[rowindex].Cells[6].FindControl("marTextBox")).Text != string.Empty)
            {
                aDao.March =
                Convert.ToDecimal(((TextBox)loadGridView.Rows[rowindex].Cells[6].FindControl("marTextBox")).Text);
            }


            aEntryDal.DeleteExistiongData(aDao);
        
            if (aEntryDal.SaveExpenseDetails(aDao, Session["LoginName"].ToString(),
                Convert.ToDateTime(DateTime.Today.ToShortDateString())))
            {
                searchButton_Click(null, null);
                ShowMessageBox("Successfully done!!!");
            }
            

        }
    }

    protected void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void financialDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }
}
