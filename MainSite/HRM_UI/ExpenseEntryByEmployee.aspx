﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="ExpenseEntryByEmployee.aspx.cs" Inherits="HRM_UI_ExpenseEntryByEmployee" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        fieldset.for-panel {
            background-color: #fcfcfc;
            border: 1px solid #999;
            padding: 10px 10px;
            background-color: white;
            margin-bottom: 12px;
        }

            fieldset.for-panel legend {
                background-color: #fafafa;
                border: 1px solid #ddd;
                border-radius: 1px;
                font-size: 12px;
                font-weight: bold;
                line-height: 10px;
                margin: inherit;
                padding: 7px;
                width: auto;
                margin-bottom: 0;
                color: black;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Expense entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Monthly Tasks </a></li>
                            <li class="breadcrumb-item"><a href="#">Expense entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">

                            <div class="form-row">
                                

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Master Code </label> <span style="color: red;">*</span>
                                        <asp:TextBox ID="EmpMasterCodeTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-1">
                                    <div class="form-group">
                                        <label style="color: white">Search </label>
                                        <br />
                                        <asp:Button ID="searchButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="searchButton_Click" Text="Search" />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Name </label> <span style="color: red;">*</span>
                                        <asp:TextBox ID="empNameTexBox" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                        <asp:HiddenField ID="EmpInfoIdHiddenField" runat="server" />
                                        <asp:HiddenField ID="joiningdtHiddenField" runat="server" />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Expense Year </label> <span style="color: red;">*</span>
                                            <asp:DropDownList ID="yearDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Expense Month </label> <span style="color: red;">*</span>
                                                <asp:DropDownList ID="monthDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem Value=""> Select any one</asp:ListItem>
                                                    <asp:ListItem Value="1">January</asp:ListItem>
                                                    <asp:ListItem Value="2">February</asp:ListItem>
                                                    <asp:ListItem Value="3">March</asp:ListItem>
                                                    <asp:ListItem Value="4">April</asp:ListItem>
                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                    <asp:ListItem Value="6">June</asp:ListItem>
                                                    <asp:ListItem Value="7">July</asp:ListItem>
                                                    <asp:ListItem Value="8">August</asp:ListItem>
                                                    <asp:ListItem Value="9">September</asp:ListItem>
                                                    <asp:ListItem Value="10">October</asp:ListItem>
                                                    <asp:ListItem Value="11">November</asp:ListItem>
                                                    <asp:ListItem Value="12">December</asp:ListItem>
                                                </asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Expense Amount </label> <span style="color: red;">*</span>
                                        <asp:TextBox ID="expenseAmountTextbox" runat="server" class="form-control form-control-sm"></asp:TextBox>  
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Company Name </label>
                                    <asp:Label ID="comNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="comIdHiddenField" runat="server" />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Unit Name </label>
                                    <asp:Label ID="unitNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="unitIdHiddenField" runat="server" />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Division Name </label>
                                    <asp:Label ID="divNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="divIdHiddenField" runat="server" />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Department Name </label>
                                    <asp:Label ID="deptNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="deptIdHiddenField" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Section Name </label>
                                    <asp:Label ID="secNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="secIdHiddenField" runat="server" />
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Designation Name </label>
                                    <asp:Label ID="desigNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="desigIdHiddenField" runat="server" />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Grade </label>
                                    <asp:Label ID="empGradeLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="empGradeIdHiddenField" runat="server" />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Type </label>
                                    <asp:Label ID="empTypeLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="empTypeIdHiddenField" runat="server" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="form-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                    <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
               <asp:HiddenField ID="JobLeftIdHiddenField" runat="server" />
                <asp:HiddenField ID="empIdHiddenField" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

