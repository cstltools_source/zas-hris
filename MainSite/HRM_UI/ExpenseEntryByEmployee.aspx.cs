﻿                            using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;
using DAO.HRM_Entities;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ExpenseEntryByEmployee : System.Web.UI.Page
{
    ExpenseEntryDal aDal = new ExpenseEntryDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
        }
    }

    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }

    protected void ShowMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            var aTable = new DataTable();

            aTable = aDal.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                empGradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
                joiningdtHiddenField.Value = aTable.Rows[0]["JoiningDate"].ToString().Trim();
            }
            else
            {
                ShowMessageBox("Data not Found");
            }
        }
        else
        {
            ShowMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }

    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        monthDropDownList.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        empGradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        empGradeLabel.Text = string.Empty;
        empTypeLabel.Text = string.Empty;
        expenseAmountTextbox.Text = "";

    }

    protected void departmentListImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("ExpenseView.aspx");
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            ExpenseEntryDao aJobLeft = new ExpenseEntryDao()
            {
                EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value),

                Month = monthDropDownList.SelectedItem.Text,
                Year = yearDropDownList.SelectedItem.Text,
                CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value),
                UnitId = Convert.ToInt32(unitIdHiddenField.Value),
                DivisionId = Convert.ToInt32(divIdHiddenField.Value),
                DeptId = Convert.ToInt32(deptIdHiddenField.Value),
                SectionId = Convert.ToInt32(secIdHiddenField.Value),
                DesigId = Convert.ToInt32(desigIdHiddenField.Value),
                GradeId = Convert.ToInt32(empGradeIdHiddenField.Value),
                EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value),
                EntryBy = Session["LoginName"].ToString(),
                EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                ActionStatus = "Posted",
                ExpenseAmount = Convert.ToDecimal(expenseAmountTextbox.Text)

            };

            if (aDal.SaveJobleft(aJobLeft))
            {
                ShowMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                ShowMessageBox("Job Left Information already exist");
            }
        }
        else
        {
            ShowMessageBox("Please input data in all Textbox");
        }

    }

    private bool Validation()
    {
        
        if (EmpMasterCodeTextBox.Text == "")
        {
            ShowMessageBox("Please Input Employee Code !!");
            return false;
        }

        if (empNameTexBox.Text == "")
        {
            ShowMessageBox("Please Input Employee Name !!");
            return false;
        }

        if (monthDropDownList.SelectedValue == "")
        {
            ShowMessageBox("Please select month !!");
            return false;
        }

        if (yearDropDownList.SelectedItem.Text == "")
        {
            ShowMessageBox("Please select year !!!");
            return false;
        }

        if (expenseAmountTextbox.Text == "")
        {
            ShowMessageBox("Please select expense amount !!!");
            return false;
        }

        if (!aDal.CheckDuplicate(EmpMasterCodeTextBox.Text, monthDropDownList.SelectedItem.Text, yearDropDownList.SelectedItem.Text))
        {
            ShowMessageBox("Expense alrady exist !!!");
            return false;
        }

        return true;
    }
}