﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection.Emit;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;

public partial class HRM_UI_ExpenseView : System.Web.UI.Page
{
    ExpenseEntryDal aDal = new ExpenseEntryDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
        }
    }

    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }

    protected void ShowMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        DataTable aTable = aDal.GetExpenseInfo(GetParameter());

        if (aTable.Rows.Count > 0)
        {
            loadGridView.DataSource = aTable;
            loadGridView.DataBind();
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
        }
    }

    private string GetParameter()
    {
        string param = "";

        if (EmpMasterCodeTextBox.Text != "")
        {
            param = param + "  AND EG.EmpMasterCode = '" + EmpMasterCodeTextBox.Text.Trim() + "' ";
        }

        if (monthDropDownList.SelectedIndex > 0)
        {
            param = param + " AND EP.Month = '" + monthDropDownList.SelectedItem.Text.Trim() + "'";
        }

        if (yearDropDownList.SelectedIndex > 0)
        {
            param = param + " AND Year = '" + yearDropDownList.SelectedItem.Text + "'";
        }

        

        return param;
    }

    protected void yesButton_Click(object sender, ImageClickEventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string empidId = loadGridView.DataKeys[i][0].ToString();
                aDal.DeleteData(empidId);
            }

           
        }

        searchButton_Click(null, null);
    }

    protected void departmentListImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("ExpenseEntryByEmployee.aspx");
    }
}