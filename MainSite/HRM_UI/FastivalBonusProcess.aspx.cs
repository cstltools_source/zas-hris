﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_FastivalBonusProcess : System.Web.UI.Page
{
    FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
            LoadYear();
        }
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }
    public void Clear()
    {
        festivalNameDropDownList.SelectedIndex = 0;
        yearDropDownList.SelectedIndex = 0;
        empCategoryDropDownList.SelectedIndex = 0;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    public void DropDownList()
    {
        aFastibalBonusBll.LoadFestivalName(festivalNameDropDownList);
        aFastibalBonusBll.LoadEmpCateGory(empCategoryDropDownList);
    }
    protected void procesButton_Click(object sender, EventArgs e)
    {
        DataTable dt = aFastibalBonusBll.GetFestivalBonusData(festivalNameDropDownList.SelectedValue,
            yearDropDownList.SelectedItem.Text, empCategoryDropDownList.SelectedValue);
        if (dt.Rows.Count>0 )
        {
            int save = aFastibalBonusBll.FestivalProcess(Convert.ToInt32(festivalNameDropDownList.SelectedValue),yearDropDownList.SelectedItem.Text,Convert.ToInt32(empCategoryDropDownList.SelectedValue),Session["LoginName"].ToString(),"HRDB");
            Clear();
            showMessageBox("Data Process Successfully !!!");
        }
    }
}