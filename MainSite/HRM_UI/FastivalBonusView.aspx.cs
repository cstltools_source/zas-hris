﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_FastivalBonusView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FastivalBonusView();
        }
    }

    private void FastivalBonusView()
    {
        aDataTable = aFastibalBonusBll.LoadFastibalBonusView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string FastivalBonusId = loadGridView.DataKeys[rowindex][0].ToString();
            Session["ID"] = FastivalBonusId;
            Response.Redirect("FestivalBonus.aspx");
            //PopUp(companyInfoId);
        }
    }

    //private void PopUp(string Id)
    //{
    //    string url = "FestivalBonus.aspx?ID=" + Id;
    //    string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=600,width=700,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
    //    ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    //}

    protected void companyInfoNewImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("FestivalBonus.aspx");
    }
    protected void companyinfoReloadImageButton_Click(object sender, EventArgs e)
    {
        FastivalBonusView();
    }
}