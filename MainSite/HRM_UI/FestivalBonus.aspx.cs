﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_FestivalBonus : Page
{
    FastibalBonusBLL aFastibalBonusBll=new FastibalBonusBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadGrid();
            DropDownList();
            LoadYear();
        }
        if (Session["ID"] !=null)
        {
            LoadEditData();
            Session["ID"] = null;
        }
    }

    public void LoadEditData()
    {
        string ID = Session["ID"].ToString();
        Session["MainId"] = ID;
        DataTable dtloadData = aFastibalBonusBll.LoadFastivalBonusEditData(ID);
        if (dtloadData.Rows.Count>0)
        {
            festivalNameDropDownList.SelectedValue = dtloadData.Rows[0]["FestivalId"].ToString();
            empCategoryDropDownList.SelectedValue = dtloadData.Rows[0]["EmpCategoryId"].ToString();
            paymentDtTextBox.Text = Convert.ToDateTime(dtloadData.Rows[0]["PaymentDate"].ToString()).ToString("dd-MMM-yyyy");
            bonusAppOnRadioButtonList.SelectedValue = dtloadData.Rows[0]["BonusApplicableOn"].ToString();
            serviceLMDateTextBox.Text = Convert.ToDateTime(dtloadData.Rows[0]["ServiceLenghMarginDate"].ToString()).ToString("dd-MMM-yyyy");

            for (int i = 0; i < yearDropDownList.Items.Count; i++)
            {
                if (yearDropDownList.Items[i].Text == dtloadData.Rows[0]["FestivalYear"].ToString())
                {
                    yearDropDownList.SelectedIndex = i;
                }
            }
        }
        loadGridView.DataSource = dtloadData;
        loadGridView.DataBind();

    }
    public void DropDownList()
    {
        aFastibalBonusBll.LoadFestivalName(festivalNameDropDownList);
        aFastibalBonusBll.LoadEmpCateGory(empCategoryDropDownList);
    }
    public void Clear()
    {
        LoadGrid();
        festivalNameDropDownList.SelectedIndex = 0;
        empCategoryDropDownList.SelectedIndex = 0;
        yearDropDownList.SelectedIndex = 0;
        paymentDtTextBox.Text = string.Empty;
        bonusAppOnRadioButtonList.Items[0].Selected = false;
        bonusAppOnRadioButtonList.Items[1].Selected = false;
        serviceLMDateTextBox.Text = string.Empty;

    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (festivalNameDropDownList.SelectedValue == null)
        {
            showMessageBox("Please Input Festival Date !!");
            return false;
        }

        if (empCategoryDropDownList.SelectedValue==null)
        {
            showMessageBox("Please Input Emplyee Category !!!");
            return false;
        }
        if (paymentDtTextBox.Text =="")
        {
            showMessageBox("Please Input Payment Date !!!");
            return false;
        }
        if (serviceLMDateTextBox.Text == "")
        {
            showMessageBox("Please Input Service Length Date !!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            if (Session["MainId"] == null)
            {
                FastibalBonusMastar aFastibalBonusMastar = new FastibalBonusMastar()
                {
                    FestivalYear = Convert.ToInt32(yearDropDownList.SelectedItem.Text),
                    EmpCategoryId = Convert.ToInt32(empCategoryDropDownList.SelectedValue),
                    PaymentDate = Convert.ToDateTime(paymentDtTextBox.Text),
                    FestivalId = Convert.ToInt32(festivalNameDropDownList.SelectedValue),
                    BonusApplicableOn = bonusAppOnRadioButtonList.SelectedItem.Text,
                    Status = "Active",
                    AddedBy = Session["LoginName"].ToString(),
                    AddedDate = Convert.ToDateTime(DateTime.Now.ToString()),
                    ServiceLenghMarginDate = Convert.ToDateTime(serviceLMDateTextBox.Text),
                };

                List<FastibalBonusDetail> aFastibalBonusDetailList = new List<FastibalBonusDetail>();

                for (int i = 0; i < loadGridView.Rows.Count; i++)
                {
                    FastibalBonusDetail aFastibalBonusDetail = new FastibalBonusDetail();
                    aFastibalBonusDetail.FromMonth = Convert.ToInt32(((TextBox)loadGridView.Rows[i].Cells[1].FindControl("fromMonthTextBox")).Text
                        .Trim());
                    aFastibalBonusDetail.ToMonth = Convert.ToInt32(((TextBox)loadGridView.Rows[i].Cells[2].FindControl("toMonthTextBox")).Text
                        .Trim());
                    aFastibalBonusDetail.Percentige = Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[3].FindControl("percentageTextBox")).Text
                        .Trim());

                    aFastibalBonusDetailList.Add(aFastibalBonusDetail);

                }

                if (aFastibalBonusBll.SaveDataForFastibalBonus(aFastibalBonusMastar, aFastibalBonusDetailList))
                {
                    showMessageBox("Data Save Successfully");
                    Clear();
                    Session["MainId"] = null;
                }
                else
                {
                    showMessageBox("Festival Bonus Already Exist");
                }
            }
            else
            {
                FastibalBonusMastar aFastibalBonusMastar = new FastibalBonusMastar()
                {
                    FastivalBonusId = Convert.ToInt32(Session["MainId"].ToString()),
                    FestivalYear = Convert.ToInt32(yearDropDownList.SelectedItem.Text),
                    EmpCategoryId = Convert.ToInt32(empCategoryDropDownList.SelectedValue),
                    PaymentDate = Convert.ToDateTime(paymentDtTextBox.Text),
                    FestivalId = Convert.ToInt32(festivalNameDropDownList.SelectedValue),
                    BonusApplicableOn = bonusAppOnRadioButtonList.SelectedItem.Text,
                    Status = "Active",
                    AddedBy = Session["LoginName"].ToString(),
                    AddedDate = Convert.ToDateTime(DateTime.Now.ToString()),
                    UpdateBy = Session["LoginName"].ToString(),
                    UpdateDate = Convert.ToDateTime(DateTime.Now.ToString()),
                    ServiceLenghMarginDate = Convert.ToDateTime(serviceLMDateTextBox.Text),
                };

                List<FastibalBonusDetail> aFastibalBonusDetailList = new List<FastibalBonusDetail>();

                for (int i = 0; i < loadGridView.Rows.Count; i++)
                {
                    FastibalBonusDetail aFastibalBonusDetail = new FastibalBonusDetail();
                    aFastibalBonusDetail.FastivalBonusDetailsId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString());
                    aFastibalBonusDetail.FastivalBonusId = Convert.ToInt32(Session["MainId"].ToString());
                    aFastibalBonusDetail.FromMonth = Convert.ToInt32(((TextBox)loadGridView.Rows[i].Cells[1].FindControl("fromMonthTextBox")).Text
                        .Trim());
                    aFastibalBonusDetail.ToMonth = Convert.ToInt32(((TextBox)loadGridView.Rows[i].Cells[2].FindControl("toMonthTextBox")).Text
                        .Trim());
                    aFastibalBonusDetail.Percentige = Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[3].FindControl("percentageTextBox")).Text
                        .Trim());

                    if (aFastibalBonusDetail.FastivalBonusDetailsId != 0)
                    {
                        if (aFastibalBonusBll.UpdateFastibalBonusDetail(aFastibalBonusDetail))
                        {

                        }
                    }
                    else
                    {
                        if (aFastibalBonusBll.SaveFastibalBonusDetail(aFastibalBonusDetail))
                        {
                            
                        }
                    }
                    
                    
                    
                    //aFastibalBonusDetailList.Add(aFastibalBonusDetail);

                }
                if (Session["DeleteID"] != null)
                {
                    string[] DeleteID = Session["DeleteID"].ToString().Split(',');
                    foreach (var ID in DeleteID)
                    {
                        if (aFastibalBonusBll.DeleteFastibalBonusDetail(ID))
                        {

                        }
                    }
                }
                if (aFastibalBonusBll.UpdateFastibalBonusMastar(aFastibalBonusMastar))
                {
                    
                }
                Clear();
                showMessageBox("Data Update Successfully");
                Session["MainId"] = null;
                if (Session["DeleteID"] !=null)
                {
                    Session["DeleteID"] = null;
                }
            }

        }
    }
    protected void addImageButton_Click(object sender, ImageClickEventArgs e)
    {
        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("Percentige");
        aDataTable.Columns.Add("FromMonth");
        aDataTable.Columns.Add("ToMonth");
        aDataTable.Columns.Add("FastivalBonusDetailsId");
        

        DataRow dataRow = null;

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            dataRow = aDataTable.NewRow();
            dataRow["FastivalBonusDetailsId"] = loadGridView.DataKeys[i][0].ToString();
            dataRow["FromMonth"] = ((TextBox)loadGridView.Rows[i].Cells[1].FindControl("fromMonthTextBox")).Text
                .Trim();
            dataRow["ToMonth"] = ((TextBox)loadGridView.Rows[i].Cells[2].FindControl("toMonthTextBox")).Text
                .Trim();
            dataRow["Percentige"] = ((TextBox)loadGridView.Rows[i].Cells[3].FindControl("percentageTextBox")).Text
                .Trim();
            aDataTable.Rows.Add(dataRow);

        }

        dataRow = aDataTable.NewRow();

        dataRow["Percentige"] = "";
        dataRow["FromMonth"] = "";
        dataRow["ToMonth"] = "";
        dataRow["FastivalBonusDetailsId"] = "0";


        aDataTable.Rows.Add(dataRow);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        //for (int i = 0; i < aDataTable.Rows.Count; i++)
        //{
        //    ((DropDownList) loadGridView.Rows[i].Cells[1].FindControl("fromMonthDropDownList")).SelectedValue =
        //        aDataTable.Rows[i][1].ToString();
        //    ((DropDownList)loadGridView.Rows[i].Cells[2].FindControl("toMonthDropDownList")).SelectedValue =
        //        aDataTable.Rows[i][2].ToString();

        //}

       
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }
    protected void deleteImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ImageButton = (ImageButton)sender;
        GridViewRow currentRow = (GridViewRow)ImageButton.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("Percentige");
        aDataTable.Columns.Add("FromMonth");
        aDataTable.Columns.Add("ToMonth");
        aDataTable.Columns.Add("FastivalBonusDetailsId");

        DataRow dataRow = null;

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            if (i != rowindex)
            {
                dataRow = aDataTable.NewRow();
                dataRow["FastivalBonusDetailsId"] = loadGridView.DataKeys[i][0].ToString();
                dataRow["FromMonth"] = ((TextBox)loadGridView.Rows[i].Cells[1].FindControl("fromMonthTextBox")).Text
               .Trim();
                dataRow["ToMonth"] = ((TextBox)loadGridView.Rows[i].Cells[2].FindControl("toMonthTextBox")).Text
                    .Trim();
                dataRow["Percentige"] = ((TextBox)loadGridView.Rows[i].Cells[3].FindControl("percentageTextBox")).Text
                    .Trim();
                aDataTable.Rows.Add(dataRow);
            }
            else
            {
                if (Session["MainId"] != null)
                {
                    string deleteId = "";
                    if (Session["DeleteID"] !=null)
                    {
                        deleteId = Session["DeleteID"].ToString();
                    }

                    Session["DeleteID"] = loadGridView.DataKeys[i][0].ToString() + "," + deleteId;
                }

                
            }

        }

        //dataRow = aDataTable.NewRow();

        //dataRow["Percentage"] = "";
        //dataRow["FromMonth"] = "1";
        //dataRow["ToMonth"] = "1";


        //aDataTable.Rows.Add(dataRow);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        //for (int i = 0; i < aDataTable.Rows.Count; i++)
        //{
        //    ((DropDownList)loadGridView.Rows[i].Cells[1].FindControl("fromMonthDropDownList")).SelectedValue =
        //        aDataTable.Rows[i][1].ToString();
        //    ((DropDownList)loadGridView.Rows[i].Cells[2].FindControl("toMonthDropDownList")).SelectedValue =
        //        aDataTable.Rows[i][2].ToString();

        //}

       
    }

    public void LoadGrid()
    {
        DataTable aDataTable=new DataTable();
        aDataTable.Columns.Add("Percentige");
        aDataTable.Columns.Add("FromMonth");
        aDataTable.Columns.Add("ToMonth");
        aDataTable.Columns.Add("FastivalBonusDetailsId");

        DataRow dataRow = null;

        dataRow = aDataTable.NewRow();

        dataRow["Percentige"] = "";

        dataRow["FromMonth"] = "";
        dataRow["ToMonth"] = "";
        dataRow["FastivalBonusDetailsId"] = "0";


        aDataTable.Rows.Add(dataRow);

        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("FastivalBonusView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}