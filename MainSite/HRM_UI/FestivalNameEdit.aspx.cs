﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_FestivalNameEdit : System.Web.UI.Page
{
    FastibalNameBLL aFastibalNameBll = new FastibalNameBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fastivalIdHiddenField.Value = Request.QueryString["ID"];
            FastivalLoad(fastivalIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (festivalNameTextBox.Text == "")
        {
            showMessageBox("Please Input Festival Name!!");
            return false;
        }
        
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        { 
            FastibalName aFastibalName = new FastibalName()
            {
                FestivalId = Convert.ToInt32(fastivalIdHiddenField.Value),
                FestivalName = festivalNameTextBox.Text,
            };
           
            if (!aFastibalNameBll.UpdateFastibalName(aFastibalName))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void FastivalLoad(string fastivalId)
    {
        FastibalName aFastibalName = new FastibalName();
        aFastibalName = aFastibalNameBll.FastibalNameEditLoad(fastivalId);
        festivalNameTextBox.Text = aFastibalName.FestivalName;
    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}