﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_FestivalNameEntry : System.Web.UI.Page
{
    FastibalNameBLL aFastibalNameBll = new FastibalNameBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       festivalNameTextBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (festivalNameTextBox.Text == "")
        {
            showMessageBox("Please Input Festival Name!!");
            return false;
        }
        
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            FastibalName aFastibalName = new FastibalName()
            {
                FestivalName = festivalNameTextBox.Text,
                AddedBy = Session["LoginName"].ToString(),
                AddedDate = Convert.ToDateTime(DateTime.Now.ToString()),
                IsActive = true
            };
            if (aFastibalNameBll.SaveFastibalName(aFastibalName))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox("Festival Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("FestivalNameView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}