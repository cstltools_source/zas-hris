﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_FinalSattlementEntry : System.Web.UI.Page
{
    FinalSattlementBLL _aFinalSattlementBLL = new FinalSattlementBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        GradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        empGradeLabel.Text = string.Empty;
        empTypeLabel.Text = string.Empty;
        grossTextBox.Text = string.Empty;
        jobleftDtTexBox.Text = string.Empty;
        fromDtTexBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        return true;
    }


    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {

            DataTable dtAllEmp = new DataTable();
            dtAllEmp = _aFinalSattlementBLL.GetAllActiveEmployee(EmpMasterCodeTextBox.Text.Trim());
            if (dtAllEmp.Rows.Count > 0)
            {
                foreach (DataRow row in dtAllEmp.Rows)
                {
                    _aFinalSattlementBLL.SalaryProcessMain(row, Convert.ToDateTime(Convert.ToDateTime(fromDtTexBox.Text).ToString("dd-MMM-yyyy")), Convert.ToDateTime(Convert.ToDateTime(jobleftDtTexBox.Text).ToString("dd-MMM-yyyy")), Convert.ToDecimal(pfTextBox.Text));
                    _aFinalSattlementBLL.SalaryProcessCom(row, Convert.ToDateTime(Convert.ToDateTime(fromDtTexBox.Text).ToString("dd-MMM-yyyy")), Convert.ToDateTime(Convert.ToDateTime(jobleftDtTexBox.Text).ToString("dd-MMM-yyyy")), Convert.ToDecimal(pfTextBox.Text));
                }
                Clear();
                showMessageBox("Data Saved Successfully !!!");
            }

        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    public string FromDate(string date)
    {
        string[] joiningdt = date.Split('-');
        date = "01" + joiningdt[1] + joiningdt[2];
        return date;
    }
    protected void departmentListImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("FinalSattlementView.aspx");
    }

    public string Month(string month)
    {
        string monthname = "";
        if (month == "1")
        {
            monthname = "Jan";
        }
        if (month == "2")
        {
            monthname = "Feb";
        }
        if (month == "3")
        {
            monthname = "Mar";
        }
        if (month == "4")
        {
            monthname = "Apr";
        }
        if (month == "5")
        {
            monthname = "May";
        }
        if (month == "6")
        {
            monthname = "Jun";
        }
        if (month == "7")
        {
            monthname = "Jul";
        }
        if (month == "8")
        {
            monthname = "Aug";
        }
        if (month == "9")
        {
            monthname = "Sep";
        }
        if (month == "10")
        {
            monthname = "Oct";
        }
        if (month == "11")
        {
            monthname = "Nov";
        }
        if (month == "12")
        {
            monthname = "Dec";
        }
        return monthname;
    }
    public void GrossCalculate(decimal gross)
    {
        decimal totalgross = 0;
        decimal perdaygross = 0;
        decimal monthdays = 0;
        decimal workingday = 0;
        decimal paygross = 0;
        totalgross = gross;
        DateTime jobleftdate = Convert.ToDateTime(jobleftDtTexBox.Text);
        string startdate = "1-" + Month(jobleftdate.Month.ToString()) + "-" + jobleftdate.Year.ToString();
        DateTime mainstartdate = Convert.ToDateTime(startdate);
        workingday = (decimal)(jobleftdate - mainstartdate).TotalDays;
        monthdays = DateTime.DaysInMonth(jobleftdate.Year, jobleftdate.Month);
        perdaygross = totalgross / monthdays;
        paygross = perdaygross * workingday;
        grossTextBox.Text = paygross.ToString("F");
    }

    public decimal PrFund(string empinfoId)
    {
        decimal prfund = 0;

        DataTable dtgetworkdays = _aFinalSattlementBLL.GetWorkDays(empinfoId);
        DataTable dtpffund = _aFinalSattlementBLL.GetPrFundAmount(empinfoId);
        if (dtgetworkdays.Rows.Count > 0)
        {
            string[] getyear = dtgetworkdays.Rows[0]["WorkYear"].ToString().Split('y');

            int year = Convert.ToInt32(getyear[0]);

            if (year >= 3)
            {
                prfund = Convert.ToDecimal(dtpffund.Rows[0]["PrFundTotal"].ToString());
            }
            else
            {
                prfund = Convert.ToDecimal(dtpffund.Rows[0]["PrFund"].ToString());
            }

        }
        return prfund;

    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aFinalSattlementBLL.GetAllActiveEmployee(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                GradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
                jobleftDtTexBox.Text = Convert.ToDateTime(aTable.Rows[0]["EffectiveDate"].ToString().Trim()).ToString("dd-MMM-yyyy");
                grossTextBox.Text = aTable.Rows[0]["Amount"].ToString().Trim();
                //GrossCalculate(Convert.ToDecimal(grossTextBox.Text));
                pfTextBox.Text = PrFund(EmpInfoIdHiddenField.Value).ToString();
                //fromDtTexBox.Text = FromDate(jobleftDtTexBox.Text);
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}