﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_FinancialYearEdit : System.Web.UI.Page
{
    FinancialYearBLL afinancialYearBll = new FinancialYearBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
            CompanyName();
            financialYearIdHiddenField.Value = Request.QueryString["ID"];
            LoadFinancialYear(financialYearIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    public void CompanyName()
    {
        afinancialYearBll.CompanyNameLoad(companyNameDropDownList);
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            FinancialYearEntry aFinancialYearEntry = new FinancialYearEntry()
            {
                FyrCode = Convert.ToInt32(financialYearIdHiddenField.Value),
                StartDate = Convert.ToDateTime(yearStartDtTextBox.Text),
                EndDate = Convert.ToDateTime(yearEndDtTextBox.Text),
                CompanyId = Convert.ToInt32(companyNameDropDownList.SelectedValue),
            };
            FinancialYearBLL aFinancialYearBll = new FinancialYearBLL();

            if (!aFinancialYearBll.UpdateDataForFinancialYear(aFinancialYearEntry))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private bool Validation()
    {
        if (yearStartDtTextBox.Text == "")
        {
            showMessageBox("Please Input FinancialYear!!");
            return false;
        }
        if (yearEndDtTextBox.Text == "")
        {
            showMessageBox("Please Input FinancialYear!!");
            return false;
        }

        return true;
    }

    private void LoadFinancialYear(string financialYearId)
    {
        FinancialYearEntry aFinancialYearEntry = new FinancialYearEntry();
        aFinancialYearEntry = afinancialYearBll.FinancialYearEditLoad(financialYearId);
        yearStartDtTextBox.Text = aFinancialYearEntry.StartDate.ToString("dd-MMM-yyyy");
        yearEndDtTextBox.Text = aFinancialYearEntry.EndDate.ToString("dd-MMM-yyyy");
        companyNameDropDownList.SelectedValue = aFinancialYearEntry.CompanyId.ToString();
        for (int i = 0; i < yearDropDownList.Items.Count; i++)
        {
            if (yearDropDownList.Items[i].Text == aFinancialYearEntry.FiscalYearId)
            {
                yearDropDownList.SelectedIndex = i;
            }
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }

}