﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_FinancialYearEntry : System.Web.UI.Page
{
    FinancialYearBLL aFinancialYearBll = new FinancialYearBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CompanyName();
            LoadYear();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    private void Clear()
    {
        yearStartDtTextBox.Text = string.Empty;
        yearEndDtTextBox.Text = string.Empty;
        companyNameDropDownList.SelectedValue = null;
    }

    public void CompanyName()
    {
        aFinancialYearBll.CompanyNameLoad(companyNameDropDownList);
    }
    private bool Validation()
    {
        if (yearStartDtTextBox.Text == "")
        {
            showMessageBox("Please Input FinancialYear!!");
            return false;
        }
        if (yearEndDtTextBox.Text == "")
        {
            showMessageBox("Please Input FinancialYear!!");
            return false;
        }

        return true;
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }
    protected void submitButton_Click1(object sender, EventArgs e)
    {
        if (Validation()==true )
        {
            FinancialYearEntry aFinancialYearEntry = new FinancialYearEntry()
            {
                StartDate = Convert.ToDateTime(yearStartDtTextBox.Text),
                EndDate = Convert.ToDateTime(yearEndDtTextBox.Text),
                CompanyId = Convert.ToInt32(companyNameDropDownList.SelectedValue), 
                FiscalYearId = yearDropDownList.SelectedItem.Text
            };

            FinancialYearBLL aFinancialYearBll = new FinancialYearBLL();
            if (aFinancialYearBll.SaveFinancialYear(aFinancialYearEntry))
            {
                showMessageBox("Data Save succesfully");
                Clear();
            }   
        }
    }

    protected void regionViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("FinancialYearView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}