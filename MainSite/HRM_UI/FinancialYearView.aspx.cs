﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class Accounts_UI_FinancialYearView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    FinancialYearBLL afinancialYearBll = new FinancialYearBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadFinancialYear();
        }
    }
    private void LoadFinancialYear()
    {
        aDataTable = afinancialYearBll.LoadFinancialYear();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    protected void ReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        LoadFinancialYear();
    }

    protected void AddImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("FinancialYearEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "FinancialYearEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(1000/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string FinancialYearId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(FinancialYearId);
        }
    }
}