﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;

public partial class HRM_UI_FoodChargeDeactivation : System.Web.UI.Page
{
    FoodChargeDeactivationDal  aDal = new FoodChargeDeactivationDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdownList();
            searchButton_Click(null, null);
        }
    }

    private void LoadDropdownList()
    {
        aDal.LoadUnitName(unitNameDropDownList);
    }

    private string GetParameter()
    {
        string param = "";

        if (EmpMasterCodeTextBox.Text != "")
        {
            param = param + "  AND EG.EmpMasterCode = '" + EmpMasterCodeTextBox.Text.Trim() + "' ";
        }

        if (unitNameDropDownList.SelectedValue != "")
        {
            param = param + " AND EG.UnitId = '" + unitNameDropDownList.SelectedValue.Trim() + "'";
        }

        return param;
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        DataTable aTable = aDal.GetActiveFoodChargeList(GetParameter());

        if (aTable.Rows.Count > 0)
        {
            loadGridView.DataSource = aTable;
            loadGridView.DataBind();
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
        }
    }

    protected void confirmButton_Click(object sender, EventArgs e)
    {
        var button = (Button)sender;
        var currentRow = (GridViewRow)button.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        var dataKey = loadGridView.DataKeys[rowindex];
        if (dataKey != null)
        {
            int foodChargeId = Convert.ToInt32(dataKey["FoodId"].ToString());

            if (aDal.DeactivateFoodCharge(foodChargeId, Session["LoginName"].ToString(),
                Convert.ToDateTime(DateTime.Today.ToShortDateString())))
            {
                searchButton_Click(null,null);
                ShowMessageBox("Deactivated successfully !!!");
            }
        }
    }

    protected void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void ReloadImageButton_Click(object sender, EventArgs e)
    {
        searchButton_Click(null, null);
    }
}