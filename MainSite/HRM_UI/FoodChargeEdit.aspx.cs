﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_FoodChargeEdit : System.Web.UI.Page
{
    FoodChargeBLL aFoodChargeBll = new FoodChargeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            foodChargeIdHiddenField.Value = Request.QueryString["ID"];
            FoodChargeLoad(foodChargeIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (foodChargeTexBox.Text == "")
        {
            showMessageBox("Please Input Food Charge!!");
            return false;
        }
        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input  Effect Date!!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            FoodCharge aFoodCharge = new FoodCharge()
            {
                FoodId = Convert.ToInt32(foodChargeIdHiddenField.Value),
                FoodCAmount = Convert.ToDecimal(foodChargeTexBox.Text),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value),
                ActionStatus = "Posted"
            };
           
            if (!aFoodChargeBll.UpdateaDataforaFoodCharge(aFoodCharge))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void FoodChargeLoad(string FoodId)
    {
        FoodCharge aFoodCharge = new FoodCharge();
        aFoodCharge = aFoodChargeBll.FoodChargeEditLoad(FoodId);
        foodChargeTexBox.Text = aFoodCharge.FoodCAmount.ToString();
        effectDateTexBox.Text = aFoodCharge.EffectiveDate.ToString();
        EmpInfoIdHiddenField.Value = aFoodCharge.EmpInfoId.ToString();
        GetEmpCode(EmpInfoIdHiddenField.Value);
    }

    public void GetEmpCode(string empid)
    {
        if (!string.IsNullOrEmpty(empid))
        {
            DataTable aTable = new DataTable();
            aTable = aFoodChargeBll.LoadEmpInfoCode(empid);

            if (aTable.Rows.Count > 0)
            {
                empCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }  
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    
}