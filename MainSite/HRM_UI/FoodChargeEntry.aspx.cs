﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_FoodChargeEntry : System.Web.UI.Page
{
    FoodChargeBLL aFoodChargeBll = new FoodChargeBLL();
    
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       empCodeTextBox.Text = string.Empty;
       empNameTexBox.Text = string.Empty;
       foodChargeTexBox.Text = string.Empty;
       effectDateTexBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (foodChargeTexBox.Text == "")
        {
            showMessageBox("Please Input Food Charge Amount !!");
            return false;
        }
        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            FoodCharge aFoodCharge = new FoodCharge()
            {
                FoodCAmount = Convert.ToDecimal(foodChargeTexBox.Text),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value),
                EntryUser = Session["LoginName"].ToString(),
                EntryDate = Convert.ToDateTime(Convert.ToDateTime(DateTime.Today.ToShortDateString()).ToString("dd-MMM-yyyy")),
                ActionStatus = "Posted",
                IsActive = true,
            };
            if (aFoodChargeBll.SaveDataForDivision(aFoodCharge))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox(" Food Charge already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("FoodChargeView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = aFoodChargeBll.LoadEmpInfo(empCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}