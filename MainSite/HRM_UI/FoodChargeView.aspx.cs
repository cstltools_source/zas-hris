﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_FoodChargeView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    FoodChargeBLL aFoodChargeBll = new FoodChargeBLL();
    FoodChargeDeactivationDal aDal = new FoodChargeDeactivationDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropdownList();
            searchButton_Click(null,null);
        }
    }

    private void LoadDropdownList()
    {
        aDal.LoadUnitName(unitNameDropDownList);
    }

    private void FoodChargeLoad()
    {
        //aDataTable = aFoodChargeBll.LoadDataforaFoodChargeView();
        //loadGridView.DataSource = aDataTable;
        //loadGridView.DataBind();
        //aFoodChargeBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }

    private string GetParameter()
    {
        string param = "";

        if (EmpMasterCodeTextBox.Text != "")
        {
            param = param + "  AND tblEmpGeneralInfo.EmpMasterCode = '" + EmpMasterCodeTextBox.Text.Trim() + "' ";
        }

        if (unitNameDropDownList.SelectedValue != "")
        {
            param = param + " AND tblEmpGeneralInfo.UnitId = '" + unitNameDropDownList.SelectedValue.Trim() + "'";
        }

        return param;
    }
    
    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        FoodChargeLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("FoodChargeEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "FoodChargeEdit.aspx?ID=" + Id;
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string foodId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(foodId);
        }

    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[5].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string ODId = loadGridView.DataKeys[i][0].ToString();
                aFoodChargeBll.DeleteDataBLL(ODId);
            }
            FoodChargeLoad();
        }

    }

    protected void searchButton_Click(object sender, EventArgs e)
    {

        //aDataTable = aFoodChargeBll.LoadDataforaFoodChargeView();
        //loadGridView.DataSource = aDataTable;
        //loadGridView.DataBind();
        

        DataTable aTable = aFoodChargeBll.LoadDataforaFoodChargeView(GetParameter());

        if (aTable.Rows.Count > 0)
        {
            loadGridView.DataSource = aTable;
            loadGridView.DataBind();
        }
        else
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
        }

        aFoodChargeBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }
}