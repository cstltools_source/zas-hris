﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_FundEdit : System.Web.UI.Page
{
    FundBll _aFundBll = new FundBll();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            fundIdHiddenField.Value = Request.QueryString["ID"];
            EmpFundLoad(fundIdHiddenField.Value);
        }
    }

    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }

        if (fundTexBox.Text == "")
        {
            showMessageBox("Please Input Fund Amount !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }

        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Fund aFund = new Fund();
            aFund.FundId = Convert.ToInt32(fundIdHiddenField.Value);
            aFund.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aFund.ActiveDate = Convert.ToDateTime(effectDateTexBox.Text);
            aFund.Balance = Convert.ToDecimal(fundTexBox.Text);

            if (_aFundBll.UpdateDataForfund(aFund))
            {
                showMessageBox("Data Update Successfully ");

            }
            else
            {
                showMessageBox(" Data Not Update !!!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpFundLoad(string fundId)
    {
        Fund aFund = new Fund();
        aFund = _aFundBll.fundEditLoad(fundId);
        effectDateTexBox.Text = aFund.ActiveDate.ToString("dd-MMM-yyyy");
        fundTexBox.Text = aFund.Balance.ToString();
        EmpInfoIdHiddenField.Value = aFund.EmpInfoId.ToString();
        desigIdHiddenField.Value = aFund.DesigId.ToString();
        GetEmpMasterCode(EmpInfoIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aFundBll.LoadEmpInfo(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                departNameTexBox.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                desigNameTexBox.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                sectionNameTexBox.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
            }
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}