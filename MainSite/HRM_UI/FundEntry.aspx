﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="FundEntry.aspx.cs" Inherits="HRM_UI_FundEntry" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Fund Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="gradeImageButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="gradeImageButton_Click" />
                     </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">General Task </a></li>
                            <li class="breadcrumb-item"><a href="#">Fund Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                             <div class="form-row">
                                 <div class="col-2">
                                    <div class="form-group">
                                        <label>Active Date : </label> 
                                        <asp:TextBox ID="activeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    <asp:ImageButton ID="sanimgDate" runat="server" AlternateText="Click to show calendar" 
                                        ImageUrl="~/Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                    <asp:CalendarExtender ID="activedate" runat="server" Format="dd-MMM-yyyy" 
                                        PopupButtonID="sanimgDate" CssClass="custom" PopupPosition="TopLeft" TargetControlID="activeTextBox" />
                                      </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Master Code : </label>
                                         <asp:TextBox ID="empMasterCodeTextBox" runat="server" AutoPostBack="True" 
                                       CssClass="form-control form-control-sm" ontextchanged="empMasterCodeTextBox_TextChanged" ></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-1">
                                    <div class="form-group">
                                         <label style="color: white">Search </label>
                                        <br />
                                        <asp:Button ID="txtSearch" runat="server" CssClass="btn btn-sm btn-info" Text="Search" onclick="txtSearch_Click" />
                                      </div>
                                 </div>
                                 <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name :</label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" ReadOnly="True" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                              </div>
                             </div>
                           </div>
                           <div class="card">
                            <div class="card-body">
                               <div class="form-row">
                                 <div class="col-2">
                                    <div class="form-group">
                                        <label>Designation : </label>
                                        <asp:TextBox ID="designationTextBox" runat="server" ReadOnly="True" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="col-2">
                                    <div class="form-group">
                                        <label>Section : </label>
                                        <asp:TextBox ID="sectionTextBox" runat="server" ReadOnly="True" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="col-2">
                                    <div class="form-group">
                                        <label>Department : </label>
                                        <asp:TextBox ID="departmentTextBox" runat="server" ReadOnly="True" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-2">
                                    <div class="form-group">
                                        <label>Division : </label> 
                                        <asp:TextBox ID="divisionTextBox" runat="server" ReadOnly="True" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Fund Head : </label> 
                                        <asp:DropDownList ID="fundDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm" >
                                        <asp:ListItem></asp:ListItem>
                                        <asp:ListItem Value="PF">Provident Fund</asp:ListItem>
                                        <asp:ListItem>Gratuity</asp:ListItem>
                                    </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Balance Amount : </label> 
                                        <asp:TextBox ID="amountTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="fnpTextBox" runat="server" 
                                        FilterType="Custom, Numbers" TargetControlID="amountTextBox" ValidChars="." />
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <asp:Button ID="addToListButton" runat="server" CssClass="btn btn-sm btn-info" onclick="addToListButton_Click" Text="Add To List" />
                                    </div>
                                </div>
                               </div>
                               <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                       <div id="gridContainer1" style="height: auto; overflow: auto; width: auto;">
                                        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" 
                                        CssClass="table table-bordered text-center thead-dark">
                                        <Columns>
                                            <asp:BoundField DataField="FundName" HeaderText="Fund Name" />
                                            <asp:BoundField DataField="Balance" HeaderText="Balance" />
                                            <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                            <asp:ImageButton ID="deleteJobImageButton" runat="server" OnClick="deleteImageButton_Click"
                                              ImageUrl="~/Assets/delete-icon.png" OnClientClick="return confirm(' Do you want to Delete data  ?');" />
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    </div>
                                </div>
                             </div>
                           </div>
                            <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                   <asp:HiddenField ID="desigHiddenField" runat="server" />
                  <asp:HiddenField ID="divisionHiddenField" runat="server" />
                <asp:HiddenField ID="companyHiddenField" runat="server" />
               <asp:HiddenField ID="sectionHiddenField" runat="server" />
              <asp:HiddenField ID="deptHiddenField" runat="server" />
             <asp:HiddenField ID="empIdHiddenField" runat="server" />
            <asp:HiddenField ID="unitHiddenField" runat="server" />
          </ContentTemplate>
       </asp:UpdatePanel>
    </div>

</asp:Content>

