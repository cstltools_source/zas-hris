﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_FundEntry : System.Web.UI.Page
{
    FundBll aFundBll = new FundBll();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private void Clear()
    {
        empMasterCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        designationTextBox.Text = string.Empty;
        desigHiddenField.Value = string.Empty;
        deptHiddenField.Value = string.Empty;
        departmentTextBox.Text = string.Empty;
        fundDropDownList.SelectedIndex = 0;
        activeTextBox.Text = string.Empty;
        amountTextBox.Text = string.Empty;
        loadGridView.DataSource = null;
        loadGridView.DataBind();
        sectionTextBox.Text = string.Empty;
        divisionTextBox.Text = string.Empty;


    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (empNameTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Name!!");
            return false;
        }


        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            List<Fund> aFundList = new List<Fund>();

            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                Fund aFund = new Fund();
                aFund.EmpInfoId = Convert.ToInt32(empIdHiddenField.Value);
                aFund.FundName = loadGridView.Rows[i].Cells[0].Text;
                aFund.Balance = Convert.ToDecimal(loadGridView.Rows[i].Cells[1].Text);
                aFund.ActiveDate = Convert.ToDateTime(activeTextBox.Text);
                aFund.DeptId = Convert.ToInt32(deptHiddenField.Value);
                aFund.DesigId = Convert.ToInt32(desigHiddenField.Value);
                aFund.CompanyInfoId = Convert.ToInt32(companyHiddenField.Value);
                aFund.UnitId = Convert.ToInt32(unitHiddenField.Value);
                aFund.DivisionId = Convert.ToInt32(divisionHiddenField.Value);
                aFund.SectionId = Convert.ToInt32(sectionHiddenField.Value);

                aFund.Status = "Active";
                aFund.EntryUser = Session["LoginName"].ToString();
                aFund.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                aFundList.Add(aFund);
            }


            if (aFundBll.SaveDataForFund(aFundList))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    protected void gradeImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("FundView.aspx");
    }
    protected void deleteImageButton_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton ItemCodeTextBox = (ImageButton)sender;
        GridViewRow currentRow = (GridViewRow)ItemCodeTextBox.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("FundName");
        aDataTable.Columns.Add("Balance");

        DataRow dataRow;

        if (loadGridView.Rows.Count > 0)
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                if (i != rowindex)
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["FundName"] = loadGridView.Rows[i].Cells[0].Text;
                    dataRow["Balance"] = loadGridView.Rows[i].Cells[1].Text;
                    aDataTable.Rows.Add(dataRow);

                }
            }
        }

        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    public bool CheckFundHead(string fund)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            if (loadGridView.Rows[i].Cells[0].Text == fund)
            {
                return false;
            }
        }
        return true;
    }

    public bool CheckHasData(string fund)
    {
        DataTable dtcheckdata = aFundBll.CheckFund(empIdHiddenField.Value, fund);
        if (dtcheckdata.Rows.Count > 0)
        {
            return false;
        }
        return true;
    }
    protected void addToListButton_Click(object sender, EventArgs e)
    {
        string fund = fundDropDownList.SelectedItem.Text;
        string balance = amountTextBox.Text;

        if (CheckHasData(fundDropDownList.SelectedItem.Text))
        {


            if (CheckFundHead(fund))
            {
                DataTable aDataTable = new DataTable();

                aDataTable.Columns.Add("FundName");
                aDataTable.Columns.Add("Balance");

                DataRow dataRow;

                if (loadGridView.Rows.Count > 0)
                {
                    for (int i = 0; i < loadGridView.Rows.Count; i++)
                    {
                        dataRow = aDataTable.NewRow();
                        dataRow["FundName"] = loadGridView.Rows[i].Cells[0].Text;
                        dataRow["Balance"] = loadGridView.Rows[i].Cells[1].Text;

                        aDataTable.Rows.Add(dataRow);
                    }

                    dataRow = aDataTable.NewRow();
                    dataRow["FundName"] = fund;
                    dataRow["Balance"] = balance;

                    aDataTable.Rows.Add(dataRow);
                    loadGridView.DataSource = aDataTable;
                    loadGridView.DataBind();
                    fundDropDownList.SelectedIndex = 0;
                    amountTextBox.Text = "";

                }
                else
                {
                    dataRow = aDataTable.NewRow();
                    dataRow["FundName"] = fund;
                    dataRow["Balance"] = balance;

                    aDataTable.Rows.Add(dataRow);
                    loadGridView.DataSource = aDataTable;
                    loadGridView.DataBind();
                    fundDropDownList.SelectedIndex = 0;
                    amountTextBox.Text = "";

                }
            }
            else
            {
                showMessageBox("Fund Name Already Exist");
            }
        }
        else
        {
            showMessageBox(fundDropDownList.SelectedItem.Text + " Already Exist !!");
        }
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {

    }
    private void GetEmployeeInfo(string empCode)
    {
        DataTable aDataTableEmp = new DataTable();
        if (!string.IsNullOrEmpty(empCode))
        {
            aDataTableEmp = aFundBll.EmpInformationBll(empCode);
            if (aDataTableEmp.Rows.Count > 0)
            {
                empNameTextBox.Text = aDataTableEmp.Rows[0]["EmpName"].ToString();
                empIdHiddenField.Value = aDataTableEmp.Rows[0]["EmpInfoId"].ToString();
                desigHiddenField.Value = aDataTableEmp.Rows[0]["DepId"].ToString();
                deptHiddenField.Value = aDataTableEmp.Rows[0]["DesigId"].ToString();
                designationTextBox.Text = aDataTableEmp.Rows[0]["DesigName"].ToString();
                departmentTextBox.Text = aDataTableEmp.Rows[0]["DeptName"].ToString();
                companyHiddenField.Value = aDataTableEmp.Rows[0]["CompanyInfoId"].ToString();
                unitHiddenField.Value = aDataTableEmp.Rows[0]["UnitId"].ToString();
                sectionHiddenField.Value = aDataTableEmp.Rows[0]["SectionId"].ToString();
                divisionHiddenField.Value = aDataTableEmp.Rows[0]["DivisionId"].ToString();
                sectionTextBox.Text = aDataTableEmp.Rows[0]["SectionName"].ToString();
                divisionTextBox.Text = aDataTableEmp.Rows[0]["DivName"].ToString();
            }
            else
            {
                showMessageBox("Employee Information Not Found or Employee is not Eligible for PF!!");
            }
        }
    }

    protected void txtSearch_Click(object sender, EventArgs e)
    {
        string empCode = empMasterCodeTextBox.Text.Trim();
        GetEmployeeInfo(empCode);
    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}