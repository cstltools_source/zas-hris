﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="GeneralDutyEdit.aspx.cs" Inherits="HRM_UI_GeneralDutyEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form2" runat="server">

        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
            <div class="page-heading">
                <div class="page-heading__container">
                    <div class="icon"><span class="li-register"></span></div>
                    <span></span>
                    <h1 class="title" style="font-size: 18px; padding-top: 9px;">Grneral Duty Edit </h1>
                </div>
                <div class="page-heading__container float-right d-none d-sm-block">
                    <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                    <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Grneral Duty Edit</a></li>

                        </ol>
                    </nav>
            </div>
            <!-- //END PAGE HEADING -->
             <br/>
                    <br/>
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>General Duty Date </label>
                                    <div class="input-group date pull-left" id="daterangepicker1">
                                        <asp:TextBox ID="gDDateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom" PopupPosition="TopLeft"
                                            TargetControlID="gDDateTextBox" />
                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                            <span>
                                                <asp:ImageButton ID="ImageButton1" runat="server"
                                                    AlternateText="Click to show calendar"
                                                     ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <div class="col-3">
                                <div class="form-group">
                                    <label>Employee Master Code </label>
                                    <asp:TextBox ID="empMasterCodeTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    <asp:HiddenField ID="gDutyIdHiddenField" runat="server" />
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Employee Name </label>
                                    <asp:Label ID="empNameTextBox" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    <asp:HiddenField ID="empInfoIdHiddenField" runat="server" />
                                </div>
                            </div>
                           <div class="col-3">
                                <div class="form-group">
                                    <label>Duty Location </label>
                                    <asp:TextBox ID="dutyLocTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Purpose </label>
                                    <asp:TextBox ID="purposTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                           <div class="col-3">
                                <div class="form-group">
                                    <label>State Status </label>
                                    <asp:TextBox ID="statusTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="form-group">
                                    <label>State Remarks </label>
                                    <asp:TextBox ID="remarksTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <asp:Button ID="updateButton" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="updateButton_Click" />
                                    <asp:Button ID="cancelButton" Text="Close"  CssClass="btn btn-sm warning" runat="server" OnClick="closeButton_Click" BackColor="#FF9900" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>
