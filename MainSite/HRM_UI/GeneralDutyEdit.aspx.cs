﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_GeneralDutyEdit : System.Web.UI.Page
{
    GeneralDutyBLL aGeneralDutyBll = new GeneralDutyBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            gDutyIdHiddenField.Value = Request.QueryString["ID"];
            GeneralDutyLoad(gDutyIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (gDDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (dutyLocTextBox.Text == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (purposTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
       
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            GeneralDuty aGeneralDuty = new GeneralDuty()
            {
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                GDutyId = Convert.ToInt32(gDutyIdHiddenField.Value),
                GDDate = Convert.ToDateTime(gDDateTextBox.Text),
                DutyLocation = dutyLocTextBox.Text,
                Purpose = purposTextBox.Text,
                //ActionStatus = statusTextBox.Text,
                ActionRemarks = remarksTextBox.Text,
                
            };
            if (!aGeneralDutyBll.UpdateDataForGeneralDuty(aGeneralDuty))
            {
                showMessageBox("Data did not Update !!!!");
                
            }
            else
            {
                showMessageBox("Data update successfully , Please reload !!!");
            }

        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    private void GeneralDutyLoad(string gdutyId)
    {
        GeneralDuty aGeneralDuty = new GeneralDuty();
        aGeneralDuty = aGeneralDutyBll.GeneralDutyEditLoad(gdutyId);
        gDDateTextBox.Text = aGeneralDuty.GDDate.ToString("dd-MMM-yyyy");
        empInfoIdHiddenField.Value = aGeneralDuty.EmpInfoId.ToString();
        dutyLocTextBox.Text = aGeneralDuty.DutyLocation.ToString();
        purposTextBox.Text = aGeneralDuty.Purpose.ToString();
        statusTextBox.Text = aGeneralDuty.ActionStatus.ToString();
        remarksTextBox.Text = aGeneralDuty.ActionRemarks.ToString();
        GetEmpMasterCode(empInfoIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string empid)
    {
        if (!string.IsNullOrEmpty(empid))
        {
            DataTable aTable = new DataTable();
            aTable = aGeneralDutyBll.LoadEmpInfoCode(empid);

            if (aTable.Rows.Count > 0)
            {
                empMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    
}