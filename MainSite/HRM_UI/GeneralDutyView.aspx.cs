﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_GeneralDutyView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    GeneralDutyBLL aGeneralDutyBll = new GeneralDutyBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GeneralDutyLoad();
        }
    }

    private void GeneralDutyLoad()
    {
        aDataTable = aGeneralDutyBll.LoadGeneralDutyView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        aGeneralDutyBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }
    
    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        GeneralDutyLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("GeneralDutyEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "GeneralDutyEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(750/2);window.open( '" + url + "', null, 'height=600,width=850,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string gdutyId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(gdutyId);
        }

    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[8].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string GDutyId = loadGridView.DataKeys[i][0].ToString();
                aGeneralDutyBll.DeleteDataBLL(GDutyId);
            }
            GeneralDutyLoad();
        }

    }
}