﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="GerEmpSalaryEntry.aspx.cs" Inherits="HRM_UI_GerEmpSalaryEntry" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">New Employee Salary Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="viewListImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">New Employee Related </a></li>
                            <li class="breadcrumb-item"><a href="#">New Employee Salary Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-3">
                                    <label>Employee Code </label>
                                    <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server"
                                        DelimiterCharacters="" EnableCaching="true"
                                        Enabled="True" MinimumPrefixLength="1" CompletionSetCount="10"
                                        ServiceMethod="GetEmployee" ServicePath="HRMWebService.asmx" TargetControlID="empCodeTextBox"
                                        UseContextKey="True"
                                        CompletionListCssClass="autocomplete_completionListElement"
                                        CompletionListItemCssClass="autocomplete_listItem"
                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                        ShowOnlyCurrentWordInCompletionListItem="true">
                                    </asp:AutoCompleteExtender>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label style="color: white">Search </label> <br />
                                        <asp:Button ID="searchButton" CssClass="btn btn-sm btn-info" runat="server" OnClick="searchButton_Click" Text="Search" />
                                    </div>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name </label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" OnTextChanged="EmpNameTextBoxTextChanged"></asp:TextBox>
                                        <asp:HiddenField ID="hdEmpInfoId" runat="server" />
                                        <asp:HiddenField ID="hdEmpCategoryId" runat="server" />
                                    </div>
                                </div>
                                
                                
                                <div class="col-1">
                                 </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Designation </label> <br />
                                        <asp:Label ID="designationLabel" CssClass="custom-control-label" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDesignationId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department </label> <br />
                                        <asp:Label ID="departmentLabel" CssClass="custom-control-label" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDepartmentId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Section </label> <br />
                                        <asp:Label ID="sectionLabel" CssClass="custom-control-label" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdSectionId" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Grade </label> <br />
                                        <asp:Label ID="empGradeLabel" CssClass="custom-control-label" runat="server"></asp:Label>
                                         <asp:HiddenField ID="hdGradeId" runat="server" />
                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Active Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                            <asp:TextBox ID="dateTextBox" runat="server" Enabled="False" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                                TargetControlID="dateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton1" runat="server"
                                                    AlternateText="Click to show calendar"
                                                    ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Salary Rule </label>
                                        <asp:DropDownList ID="salaryRuleDropDownList" runat="server" CssClass="form-control form-control-sm"></asp:DropDownList>
                                        <asp:HiddenField ID="hdSalScaleId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Gross Salary </label>
                                        <asp:TextBox ID="grossAmountTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="fnpTextBox" runat="server"
                                            TargetControlID="grossAmountTextBox"
                                            FilterType="Custom, Numbers"
                                            ValidChars="." />
                                    </div>
                                </div>
                                
                                 <div class="col-3">
                                    <div class="form-group">
                                        <label style="color: white">Gross Salary </label> <br />
                                        <asp:Button ID="calculateButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="calculateButton_Click" Text="Calculate" />
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: auto; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                <asp:GridView ID="employeeSalaryGridView" runat="server"
                                    AutoGenerateColumns="False"
                                    BorderStyle="None" BorderWidth="1px" CellPadding="4" CssClass="table table-bordered text-center"
                                    DataKeyNames="SalaryHeadId"
                                    GridLines="Horizontal" HorizontalAlign="Center"
                                    OnRowCommand="employeeSalaryGridView_RowCommand">
                                    <Columns>
                                        <asp:BoundField DataField="SalaryHead" HeaderText="Salary Head">
                                            <FooterStyle Font-Size="Large" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Amount" HeaderText="Amount">
                                            <FooterStyle Font-Bold="True" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Remove">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="removeLinkButton" runat="server"
                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"
                                                    Font-Underline="True">Remove</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle HorizontalAlign="Center" />
                                    <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                    <HeaderStyle  Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                    <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                    <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                    <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                    <SortedDescendingHeaderStyle BackColor="#242121" />
                                </asp:GridView>
                            </div>
                           <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    
    <asp:UpdateProgress ID="progress" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/Assets/progress-bar-opt.gif"
                    Height="80" Width="80" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

