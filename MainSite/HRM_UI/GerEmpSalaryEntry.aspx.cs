﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_GerEmpSalaryEntry : System.Web.UI.Page
{
    EmpSalaryBLL aEmpSalaryBll=new EmpSalaryBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();   
        }
    }

    private void LoadDropDown()
    {
        aEmpSalaryBll.LoadSalaryRuleToDropDownBLL(salaryRuleDropDownList, hdSalScaleId.Value.ToString());
    }

    protected void employeeSalaryGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            int deletingIndex = Convert.ToInt32(e.CommandArgument);
            List<EmployeeSalary> aEmployeeSalaryList = new List<EmployeeSalary>();
            EmployeeSalary aEmployeeSalary;

            int SalaryheadId = Convert.ToInt32(employeeSalaryGridView.DataKeys[deletingIndex][0].ToString());
            if (employeeSalaryGridView.Rows.Count > 0)
            {
                for (int i = 0; i < employeeSalaryGridView.Rows.Count; i++)
                {
                    if (SalaryheadId != Convert.ToInt32(employeeSalaryGridView.DataKeys[i][0].ToString()))
                    {
                        aEmployeeSalary = new EmployeeSalary()
                        {
                            SalaryHeadId = Convert.ToInt32(employeeSalaryGridView.DataKeys[i][0].ToString()),
                            SalaryHead = employeeSalaryGridView.Rows[i].Cells[0].Text.ToString(),
                            Amount = Convert.ToDecimal(employeeSalaryGridView.Rows[i].Cells[1].Text.Trim())
                        };
                        aEmployeeSalaryList.Add(aEmployeeSalary);
                    }
                }
            }
           
            
            employeeSalaryGridView.DataSource = null;
            employeeSalaryGridView.DataBind();
            employeeSalaryGridView.DataSource = aEmployeeSalaryList;
            employeeSalaryGridView.DataBind();
        }
    }
    
    private bool CheckValidation()
    {
        if (!aEmpSalaryBll.CheckValidation(grossAmountTextBox.Text.Trim()))
        {
            showMessageBox("Input Amount!!");
            return false;
        }
        else
        {
            if (!aEmpSalaryBll.CheckNumaric(grossAmountTextBox.Text.Trim()))
            {
                showMessageBox("Input Numaric Value!!");
                return false;
            }
        }

        if (!aEmpSalaryBll.CheckValidation(salaryRuleDropDownList.SelectedValue))
        {
            showMessageBox("Select Salary Head!!");
            return false;
        }
        if (!aEmpSalaryBll.CheckValidation(empCodeTextBox.Text.Trim()))
        {
            showMessageBox("Input Employee!!");
            return false;
        }


        if (employeeSalaryGridView.Rows.Count==0)
        {
            showMessageBox("Generate Calculation!!");
            return false;
        }
        return true;
    }
    private bool ValidationForCalculation()
    {
        if (!aEmpSalaryBll.CheckValidation(grossAmountTextBox.Text.Trim()))
        {
            showMessageBox("Input Amount!!");
            return false;
        }
        else
        {
            if (!aEmpSalaryBll.CheckNumaric(grossAmountTextBox.Text.Trim()))
            {
                showMessageBox("Input Numaric Value!!");
                return false;
            }
        }



        if (!aEmpSalaryBll.CheckValidation(salaryRuleDropDownList.SelectedValue))
        {
            showMessageBox("Select Salary Head!!");
            return false;
        }
        return true;
    }
    protected void calculateButton_Click(object sender, EventArgs e)
    {

        if (ValidationForCalculation()==true)
        {
            DataTable dtSalaryCalculation = aEmpSalaryBll.Calculate(salaryRuleDropDownList.SelectedValue,
          Convert.ToDecimal(grossAmountTextBox.Text));
            employeeSalaryGridView.DataSource = dtSalaryCalculation;
            employeeSalaryGridView.DataBind();
        }
      
        
    }


    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (hdEmpInfoId.Value!="")
        {
            if (CheckValidation()==true)
            {
                SaveData();
            }
            
        }
        else
        {
            showMessageBox("Search Employee First !!");
        }   
    }

    private  void ClearAll()
    {
       dateTextBox.Text = string.Empty;
       empNameTextBox.Text = string.Empty;
       designationLabel.Text = string.Empty;
       departmentLabel.Text = string.Empty;
       sectionLabel.Text = string.Empty;
       hdEmpInfoId.Value = string.Empty;
       hdDepartmentId.Value = string.Empty;
       hdDesignationId.Value = string.Empty;
       hdSectionId.Value = string.Empty;
       hdGradeId.Value = string.Empty;
       empGradeLabel.Text = string.Empty;
       hdEmpCategoryId.Value = string.Empty;
       employeeSalaryGridView.DataSource = null;
       employeeSalaryGridView.DataBind();
       empCodeTextBox.Text = string.Empty;
       salaryRuleDropDownList.DataSource = null;
       salaryRuleDropDownList.DataBind();

       grossAmountTextBox.Text = string.Empty;
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    private void SaveData()
    {
        List<EmployeeSalary> aEmployeeSalaryList = new List<EmployeeSalary>();
        EmployeeSalary aEmployeeSalary;
        
        if (employeeSalaryGridView.Rows.Count>0)
        {
            for (int i = 0; i < employeeSalaryGridView.Rows.Count; i++)
            {
                aEmployeeSalary = new EmployeeSalary()
                {
                    EmpInfoId = Convert.ToInt32(hdEmpInfoId.Value),
                    SalaryHeadId = Convert.ToInt32(employeeSalaryGridView.DataKeys[i][0].ToString()),
                    SalaryHead = (employeeSalaryGridView.Rows[i].Cells[0].Text),
                    Amount = Convert.ToDecimal(employeeSalaryGridView.Rows[i].Cells[1].Text),
                    ActiveDate = Convert.ToDateTime(dateTextBox.Text.Trim()),
                    ActionStatus = "Posted",
                    EntryUser = Session["LoginName"].ToString(),
                    EntryDate =Convert.ToDateTime(DateTime.Today.ToShortDateString()),
                    IsActive = true,
                };
                aEmployeeSalaryList.Add(aEmployeeSalary);
            }

            if (aEmpSalaryBll.StartingSalaryCheck(hdEmpInfoId.Value))
            {
                if (aEmpSalaryBll.SalarySave(aEmployeeSalaryList))
                {
                    ClearAll();
                    showMessageBox("Employee Salary Save SuccessFully!!");
                }
                else
                {
                    showMessageBox("Employee Salary Already Posted for this year!!");
                }
            }
            else
            {
                showMessageBox("Starting Salary Already Inserted!!");
            }
        }
    }
    
    protected void empMastCodeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        string empCode = empCodeTextBox.Text.Trim();
        GetEmployeeInfo(empCode);
    }

    private void GetEmployeeInfo(string empCode)
    {
        DataTable aDataTableEmp = new DataTable();
        if (!string.IsNullOrEmpty(empCode))
        {
            aDataTableEmp = aEmpSalaryBll.EmpInformationBll(empCode);
            if (aDataTableEmp.Rows.Count > 0)
            {
                empNameTextBox.Text = aDataTableEmp.Rows[0]["EmpName"].ToString();
                designationLabel.Text = aDataTableEmp.Rows[0]["DesigName"].ToString();
                departmentLabel.Text = aDataTableEmp.Rows[0]["DeptName"].ToString();
                sectionLabel.Text = aDataTableEmp.Rows[0]["SectionName"].ToString();
                hdEmpInfoId.Value = aDataTableEmp.Rows[0]["EmpInfoId"].ToString();
                hdDepartmentId.Value = aDataTableEmp.Rows[0]["DepId"].ToString();
                hdDesignationId.Value = aDataTableEmp.Rows[0]["DesigId"].ToString();
                hdSectionId.Value = aDataTableEmp.Rows[0]["SectionId"].ToString();
                hdGradeId.Value = aDataTableEmp.Rows[0]["GradeId"].ToString();
                empGradeLabel.Text = aDataTableEmp.Rows[0]["GradeType"].ToString();
                hdSalScaleId.Value = aDataTableEmp.Rows[0]["SalScaleId"].ToString();
                LoadDropDown();
            }
            else
            {
                ClearAll();
                showMessageBox("Employee Information Not Found or Salary Already Inserted!!");
            }
        }
    }

    protected void EmpNameTextBoxTextChanged(object sender, EventArgs e)
    {
    }
    protected void viewListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("EmpSalaryView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        ClearAll();
    }
}