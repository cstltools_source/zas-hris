﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_GradeEdit : System.Web.UI.Page
{
    EmpGradeBLL aEmpGradeBll = new EmpGradeBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            gradeIdHiddenField.Value = Request.QueryString["ID"];
            GradeInfoLoad(gradeIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (gradeNameTextBox.Text != "" && gradeTypeTextBox.Text !="")
        {
            EmployeeGrade aEmployeeGrade = new EmployeeGrade()
            {
                GradeId = Convert.ToInt32(gradeIdHiddenField.Value),
                GradeName = gradeNameTextBox.Text,
                GradeType = gradeTypeTextBox.Text,
            };
            EmpGradeBLL aEmpGradeBll = new EmpGradeBLL();

            if (!aEmpGradeBll.UpdateDataForGrade(aEmployeeGrade))
            {
                showMessageBox("Data Not Update!!!");
                
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
                
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void GradeInfoLoad(string gradeId)
    {
        EmployeeGrade aEmployeeGrade = new EmployeeGrade();
        aEmployeeGrade = aEmpGradeBll.GradeEditLoad(gradeId);
        gradeNameTextBox.Text = aEmployeeGrade.GradeName;
        gradeTypeTextBox.Text = aEmployeeGrade.GradeType;
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);

    }
}