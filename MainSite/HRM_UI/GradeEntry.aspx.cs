﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_GradeEntry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private void Clear()
    {
        gradeNameTextBox.Text = string.Empty;
        gradeTypeTextBox.Text = string.Empty;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (gradeNameTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Name!!");
            return false;
        }
        if (gradeTypeTextBox.Text == "")
        {
            showMessageBox("Please Input  Employee Short Name!!");
            return false;
        }
        
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmployeeGrade aEmployeeGrade = new EmployeeGrade()
            {
                GradeName = gradeNameTextBox.Text,
                GradeType = gradeTypeTextBox.Text,   
            };
           
            EmpGradeBLL aEmpGradeBll = new EmpGradeBLL();
            
            if (aEmpGradeBll.SaveDataForDepartment(aEmployeeGrade))
            {
                showMessageBox("Data Save Successfully Employee Grade Code is :    " + aEmployeeGrade.GradeCode + " And Employee Grade Name is :  " + aEmployeeGrade.GradeName);
                Clear();
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    
    protected void gradeImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("GradeView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}