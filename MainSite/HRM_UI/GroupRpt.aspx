﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="GroupRpt.aspx.cs" Inherits="HRM_UI_GroupRpt" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Group Report</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Group Report</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row" runat="server" Visible="False">
                                <div class="col-2">
                                    <div class="form-group" >
                                        <label>Company Name  </label>
                                        &nbsp; <span style="color: red;">*</span>
                                        <asp:DropDownList ID="comNameDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Unit Name </label>
                                        &nbsp; <span style="color: red;">*</span>
                                        <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="unitNameDropDownList_OnSelectedIndexChanged"
                                            CssClass="form-control form-control-sm">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Group </label>
                                        <asp:DropDownList ID="groupDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Attendance From Date :</label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                            <asp:TextBox ID="fromdtTextBox" runat="server"  class="form-control form-control-sm"></asp:TextBox>
                                              <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                                TargetControlID="fromdtTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton1" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Attendance To Date : </label>
                                        <div class="input-group date pull-left" id="daterangepicker11">
                                            <asp:TextBox ID="todtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton2" CssClass="custom"
                                                TargetControlID="todtTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton2" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

