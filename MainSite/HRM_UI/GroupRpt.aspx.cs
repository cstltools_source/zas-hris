﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_GroupRpt : System.Web.UI.Page
{
    GroupBLL aGroupBll = new GroupBLL();
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
        }
    }
    private void LoadDropDown()
    {
        //aGroupBll.LoadGroup(groupDropDownList);
        //aInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        EmpGeneralInfoBLL aEmpGeneralInfoBll = new EmpGeneralInfoBLL();
        aEmpGeneralInfoBll.LoadUnitNameAllByUser(unitNameDropDownList);
    }
    protected void viewButton_Click(object sender, EventArgs e)
    {
        if (groupDropDownList.SelectedIndex==0)
        {
            PopUp("0");
        }
        else
        {
            PopUp(groupDropDownList.SelectedValue);
        }
        
    }
    private void PopUp(string groupId)
    {
        string url = "../Report_UI/EmpGroupReportViewer.aspx?groupId=" + groupId+"&fromdt="+fromdtTextBox.Text+"&todt="+todtTextBox.Text;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
        //DataTable dtdata = aInfoBll.LoadCompany(comNameDropDownList.SelectedValue);
        //if (dtdata.Rows.Count > 0)
        //{
        //    Label1.Text = dtdata.Rows[0]["ComShortName"].ToString();
        //}
    }

    protected void unitNameDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (unitNameDropDownList.SelectedValue != "")
        {
            aGroupBll.LoadGroupByUnit(groupDropDownList, unitNameDropDownList.SelectedValue);
        }
    }
}