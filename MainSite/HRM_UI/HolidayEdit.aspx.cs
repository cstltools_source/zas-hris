﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_HolidayEdit : System.Web.UI.Page
{
    HolidayInfoBLL aHolidayInfoBll = new HolidayInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UnitName();
            CompanyName();
            holidayIdHiddenField.Value = Request.QueryString["ID"];
            HolidayLoad(holidayIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (holidayDateNameTextBox.Text == "")
        {
            showMessageBox("Please Input Holiday Name!!");
            return false;
        }
        if (detailsNameTextBox.Text == "")
        {
            showMessageBox("Please Input Holiday Details !!");
            return false;
        }
        if (companyNameDropDownList.Text == "")
        {
            showMessageBox("Please Input Company Name!!");
            return false;
        }
        return true;
    }

    private void HolidayLoad(string holidayId)
    {
        HolidayInfo aHolidayInfo = new HolidayInfo();
        aHolidayInfo = aHolidayInfoBll.HolidayEditLoad(holidayId);
        holidayDateNameTextBox.Text = aHolidayInfo.HolidayfromDate.ToString("dd-MMM-yyyy");
        detailsNameTextBox.Text = aHolidayInfo.Details;
        companyNameDropDownList.SelectedValue = Convert.ToString(aHolidayInfo.CompanyInfoId);
        companyUnitNameDropDownList.SelectedValue = Convert.ToString(aHolidayInfo.UnitId);
    }

    public void UnitName()
    {
        HolidayInfoBLL aHolidayInfoBll = new HolidayInfoBLL();
        aHolidayInfoBll.LoadUnitName(companyUnitNameDropDownList);
    }

    public void CompanyName()
    {
        HolidayInfoBLL aHolidayInfoBll = new HolidayInfoBLL();
        aHolidayInfoBll.LoadCompanyName(companyNameDropDownList);
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void updateButton_Click1(object sender, EventArgs e)
    {
        if (Validation())
        {
            HolidayInfo aHolidayInfo = new HolidayInfo()
            {
                HolidayId = Convert.ToInt32(holidayIdHiddenField.Value),
                HolidayfromDate = Convert.ToDateTime(holidayDateNameTextBox.Text),
                Details = detailsNameTextBox.Text,
                CompanyInfoId = Convert.ToInt32(companyNameDropDownList.SelectedValue),
                UnitId = Convert.ToInt32(companyUnitNameDropDownList.SelectedValue)
            };
            HolidayInfoBLL aHolidayInfoBll = new HolidayInfoBLL();

            if (!aHolidayInfoBll.UpdateDataForHoliday(aHolidayInfo))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }

        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
}