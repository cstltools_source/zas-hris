﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_HolidayEntry : System.Web.UI.Page
{
    HolidayInfoBLL aHolidayInfoBll = new HolidayInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UnitName();
            CompanyName();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        holidayfromDateTextBox.Text = string.Empty;
        holidaytoDateTextBox.Text = string.Empty;
        detailsTextBox.Text = string.Empty;
    }

    public bool HolidayDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(holidayfromDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool HolidayDate2()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(holidaytoDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (holidayfromDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
          if (holidaytoDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (detailsTextBox.Text == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (companyNameDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
        if (companyUnitDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Date !!");
            return false;
        }
        if (HolidayDate() == false)
        {
            showMessageBox("Please give a valid Holiday Date !!!");
            return false;
        }
        return true;
    }

    public void UnitName()
    {
        aHolidayInfoBll.LoadUnitName(companyUnitDropDownList);
    }

    public void CompanyName()
    {
        aHolidayInfoBll.LoadCompanyName(companyNameDropDownList);
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (HolidayDate() && HolidayDate2())
        {
            if (Validation() == true)
            {
                HolidayInfo aHolidayInfo = new HolidayInfo()
                {
                   HolidayfromDate = Convert.ToDateTime(holidayfromDateTextBox.Text),
                   HolidaytoDate = Convert.ToDateTime(holidaytoDateTextBox.Text),
                   Details = detailsTextBox.Text,
                   UnitId = Convert.ToInt32(companyUnitDropDownList.SelectedValue),
                   CompanyInfoId = Convert.ToInt32(companyNameDropDownList.SelectedValue),
                   IsActive = true,
                };
                if (aHolidayInfoBll.SaveHolidayInfoData(aHolidayInfo))
                {
                    showMessageBox("Data Save Successfully ");
                    Clear();
                    UnitName();
                    CompanyName();
                }
            }
            else
            {
                showMessageBox("Please input data in all Textbox");
            }    
        }
        else
        {
            showMessageBox("Please Input valid Date");
        }
    }
    protected void viewListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("HolidayView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}