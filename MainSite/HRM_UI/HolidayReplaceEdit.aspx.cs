﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_HolidayReplaceEdit : System.Web.UI.Page
{
    HolidayReplaceBLL aHolidayReplaceBll = new HolidayReplaceBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            whRIdHiddenField.Value = Request.QueryString["ID"];
            HolidayReplaceLoad(whRIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void hWorkTextBox_TextChanged(object sender, EventArgs e)
    {
        holidayNameTextBox.Text = aHolidayReplaceBll.DayName(hWorkTextBox.Text);
        if (!aHolidayReplaceBll.HasWeeklyHoliday(holidayNameTextBox.Text, empInfoIdHiddenField.Value))
        {
            hWorkTextBox.Text = "";
            holidayNameTextBox.Text = "";
            showMessageBox("Weekly Holyday is not assign for this day");
        }
    }
    protected void alterDtTextBox_TextChanged(object sender, EventArgs e)
    {
        alterDayNameTextBox.Text = aHolidayReplaceBll.DayName(alterDtTextBox.Text);
    }
    public bool HolidayReplaceDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(hWorkTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool AlterReplaceDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(alterDtTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (hWorkTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }

        if (HolidayReplaceDate() == false)
        {
            showMessageBox("Please give a valid HolidayReplace Date !!!");
            return false;
        }
        if (AlterReplaceDate() == false)
        {
            showMessageBox("Please give a valid Alternate Date !!!");
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            HolidayReplace aHolidayReplace = new HolidayReplace()
            {
                WHRId = Convert.ToInt32(whRIdHiddenField.Value),
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                EmpMasterCode = empMasterCodeTextBox.Text,
                EmpName = empNameTextBox.Text,
                WeekHolidaydate = Convert.ToDateTime(hWorkTextBox.Text),
                WeekHolidayDayName = holidayNameTextBox.Text,
                AlternativeDate = Convert.ToDateTime(alterDtTextBox.Text),
                AlternativeDayName = alterDayNameTextBox.Text,


            };
            if (!aHolidayReplaceBll.UpdateDataForHoliday(aHolidayReplace))
            {
                showMessageBox("Data did not Update !!!!");

            }
            else
            {
                showMessageBox("Data update successfully , Please reload !!!");
            }

        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    private void HolidayReplaceLoad(string WHRI)
    {
        HolidayReplace aHolidayReplace = new HolidayReplace();
        aHolidayReplace = aHolidayReplaceBll.HolidayEditLoad(WHRI);
        empMasterCodeTextBox.Text = aHolidayReplace.EmpMasterCode;
        empNameTextBox.Text = aHolidayReplace.EmpName;
        hWorkTextBox.Text = aHolidayReplace.WeekHolidaydate.ToString("dd-MMM-yyyy");
        holidayNameTextBox.Text = aHolidayReplace.WeekHolidayDayName;
        alterDtTextBox.Text = aHolidayReplace.AlternativeDate.ToString("dd-MMM-yyyy");
        alterDayNameTextBox.Text = aHolidayReplace.AlternativeDayName;
        empInfoIdHiddenField.Value = aHolidayReplace.EmpInfoId.ToString();
    }


    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {

    }
    
}