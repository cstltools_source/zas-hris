﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="HolidayReplaceEntry.aspx.cs" Inherits="HRM_UI_HolidayReplaceEntry" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Holiday Replace Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                         <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="jobViewImageButton_Click" />
                     </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Holiday Replace Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                              <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Code : </label>
                                         <asp:TextBox ID="empMasterCodeTextBox" runat="server" AutoPostBack="True" 
                                         CssClass="form-control form-control-sm" ontextchanged="empMasterCodeTextBox_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name : </label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox>
                                      </div>
                                   </div>
                                   <div class="col-3">
                                    <div class="form-group">
                                        <label> Weekly Holiday Date : </label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                            <asp:TextBox ID="hWorkTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom" PopupPosition="TopLeft"
                                                TargetControlID="hWorkTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton1" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                        <%--<div class="input-group date pull-left" id="daterangepicker1">
                                           <asp:TextBox ID="" runat="server" 
                                            CssClass="form-control form-control-sm" AutoPostBack="True" 
                                            ontextchanged="hWorkTextBox_TextChanged"></asp:TextBox>
                                        <asp:CalendarExtender ID="hWorkTextBox0_CalendarExtender" runat="server" 
                                            Format="dd-MMM-yyyy" PopupButtonID="imgOnDDate" 
                                            TargetControlID="hWorkTextBox" />
                                        <asp:ImageButton ID="imgOnDDate" runat="server" 
                                            AlternateText="Click to show calendar" 
                                            ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                        </div>--%>
                                    </div>
                                </div>
                             </div>
                              <div class="form-row">
                               <div class="col-3">
                                    <div class="form-group">
                                        <label>weekly Holiday Name :</label>
                                        <asp:TextBox ID="holidayNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Alternative Date : </label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                            <asp:TextBox ID="alterDtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton" CssClass="custom" PopupPosition="TopLeft"
                                                TargetControlID="alterDtTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                       <%-- <div class="input-group date pull-left" id="daterangepicker12">
                                            <asp:TextBox ID="" runat="server" CssClass="form-control form-control-sm" 
                                                AutoPostBack="True" ontextchanged="alterDtTextBox_TextChanged"></asp:TextBox>
                                            <asp:ImageButton ID="imgalter" runat="server" 
                                                AlternateText="Click to show calendar" 
                                                ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                             <asp:CalendarExtender ID="alterDtTextBox_CalendarExtender" runat="server" 
                                                Format="dd-MMM-yyyy" PopupButtonID="imgalter" 
                                                TargetControlID="alterDtTextBox" />
                                            </div>--%>
                                        </div>
                                    </div>
                                 <div class="col-3">
                                    <div class="form-group">
                                        <label>Alternative Day Name : </label>
                                        <asp:TextBox ID="alterDayNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                             </div>
                             <br/>
                            <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             <asp:HiddenField ID="empInfoIdHiddenField" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

