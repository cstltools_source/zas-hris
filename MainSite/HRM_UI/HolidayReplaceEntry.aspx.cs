﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_HolidayReplaceEntry : System.Web.UI.Page
{
    HolidayReplaceBLL aHolidayReplaceBll = new HolidayReplaceBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        hWorkTextBox.Text = string.Empty;
        empMasterCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
    }
    public bool HolidayReplaceDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(hWorkTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool AlterReplaceDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(alterDtTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (hWorkTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }

        if (HolidayReplaceDate() == false)
        {
            showMessageBox("Please give a valid HolidayReplace Date !!!");
            return false;
        }
        if (AlterReplaceDate() == false)
        {
            showMessageBox("Please give a valid Alternate Date !!!");
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            HolidayReplace aHolidayReplace = new HolidayReplace()
            {
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                EmpMasterCode = empMasterCodeTextBox.Text,
                EmpName = empNameTextBox.Text,
                WeekHolidaydate = Convert.ToDateTime(hWorkTextBox.Text),
                WeekHolidayDayName = holidayNameTextBox.Text,
                AlternativeDate = Convert.ToDateTime(alterDtTextBox.Text),
                AlternativeDayName = alterDayNameTextBox.Text,
                ActionStatus = "Posted",
                EntryUser = Session["LoginName"].ToString(),
                EntryDate = System.DateTime.Today,
                IsActive = true,
            };

            if (aHolidayReplaceBll.SaveHolidayReplaceData(aHolidayReplace))
            {
                showMessageBox("Data Save Successfully");
                Clear();
            }

        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    protected void jobViewImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("HolidayReplaceView.aspx");
    }

    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = aHolidayReplaceBll.LoadEmpInfo(empMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void hWorkTextBox_TextChanged(object sender, EventArgs e)
    {
        holidayNameTextBox.Text = aHolidayReplaceBll.DayName(hWorkTextBox.Text);
        if (!aHolidayReplaceBll.HasWeeklyHoliday(holidayNameTextBox.Text, empInfoIdHiddenField.Value))
        {
            hWorkTextBox.Text = "";
            holidayNameTextBox.Text = "";
            showMessageBox("Weekly Holyday is not assign for this day");
        }
    }
    protected void alterDtTextBox_TextChanged(object sender, EventArgs e)
    {
        alterDayNameTextBox.Text = aHolidayReplaceBll.DayName(alterDtTextBox.Text);
    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}