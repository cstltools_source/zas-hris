﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HolidayWorkEdit.aspx.cs" Inherits="HRM_UI_HolidayWorkEdit" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
           <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;"> Holiday Work Edit </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="CompanyListImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Daily Task </a></li>
                            <li class="breadcrumb-item"><a href="#">Holiday Work Edit</a></li>

                        </ol>
                    </nav>
                </div>
            <!-- //END PAGE HEADING -->
             <br/>
                    <br/>
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">

                        <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Holiday Work Date </label>
                                        <asp:TextBox ID="OnDateTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:CalendarExtender ID="OnDTextBox_CalendarExtender" runat="server" 
                                            Format="dd-MMM-yyyy" PopupButtonID="imgOnDDate" CssClass="custom" PopupPosition="TopLeft"
                                            TargetControlID="OnDateTextBox" />
                                        <asp:ImageButton ID="imgOnDDate" runat="server" 
                                            AlternateText="Click to show calendar" 
                                            ImageUrl="~/Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Employee Code </label>
                                        <asp:TextBox ID="empMasterCodeTextBox" runat="server" AutoPostBack="True" 
                                        CssClass="form-control form-control-sm" ontextchanged="empMasterCodeTextBox_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-4">
                                    <div class="form-group">
                                        <label>Employee Name </label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Duty Location </label>
                                        <asp:TextBox ID="dutyLocTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-4">
                                    <div class="form-group">
                                        <label>Purpose </label>
                                        <asp:TextBox ID="purposTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-4">
                                    <div class="form-group">
                                        <label>Remarks </label>
                                        <asp:TextBox ID="remarksTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            

                        <div class="form-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <asp:Button ID="updateButton" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="updateButton_Click" />
                                    <asp:Button ID="cancelButton" Text="Close"  CssClass="btn btn-sm warning" runat="server" OnClick="closeButton_Click" BackColor="#FF9900" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="empInfoIdHiddenField" runat="server" />
        <asp:HiddenField ID="ondutyIdHiddenField" runat="server" />
    </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>

