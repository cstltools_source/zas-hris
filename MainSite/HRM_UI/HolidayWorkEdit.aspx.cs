﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_HolidayWorkEdit : System.Web.UI.Page
{
    HolidayWorkBLL aDutyBll = new HolidayWorkBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ondutyIdHiddenField.Value = Request.QueryString["ID"];
            HolidayWorkLoad(ondutyIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (OnDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (dutyLocTextBox.Text == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (purposTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }

        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            HolidayWork aHolidayWork = new HolidayWork()
            {
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                HolidayWorkId = Convert.ToInt32(ondutyIdHiddenField.Value),
                HworkDate = Convert.ToDateTime(OnDateTextBox.Text),
                DutyLocation = dutyLocTextBox.Text,
                Purpose = purposTextBox.Text,
                ActionRemarks = remarksTextBox.Text

            };
            if (!aDutyBll.UpdateDataForHolidayWork(aHolidayWork))
            {
                showMessageBox("Data did not Update !!!!");

            }
            else
            {
                showMessageBox("Data update successfully , Please reload !!!");
            }

        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    private void HolidayWorkLoad(string HolidayWorkId)
    {
        HolidayWork aHolidayWork = new HolidayWork();
        aHolidayWork = aDutyBll.HolidayWorkEditLoad(HolidayWorkId);
        OnDateTextBox.Text = aHolidayWork.HworkDate.ToString();
        empInfoIdHiddenField.Value = aHolidayWork.EmpInfoId.ToString();
        dutyLocTextBox.Text = aHolidayWork.DutyLocation.ToString();
        purposTextBox.Text = aHolidayWork.Purpose.ToString();
        // statusTextBox.Text = aHolidayWork.ActionStatus.ToString();
        remarksTextBox.Text = aHolidayWork.ActionRemarks.ToString();
        GetEmpMasterCode(empInfoIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string empid)
    {
        if (!string.IsNullOrEmpty(empid))
        {
            DataTable aTable = new DataTable();
            aTable = aDutyBll.LoadEmpInfoCode(empid);

            if (aTable.Rows.Count > 0)
            {
                empMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {

    }
}