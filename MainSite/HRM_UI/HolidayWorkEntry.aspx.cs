﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_HolidayWorkEntry : System.Web.UI.Page
{
    HolidayWorkBLL aDutyBll = new HolidayWorkBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        hWorkTextBox.Text = string.Empty;
        empMasterCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        dutyLocTextBox.Text = string.Empty;
        purposTextBox.Text = string.Empty;
        remarksTextBox.Text = string.Empty;
     
    }
    public bool HolidayWorkDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(hWorkTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (hWorkTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (dutyLocTextBox.Text == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (purposTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
        if (HolidayWorkDate() == false)
        {
            showMessageBox("Please give a valid HolidayWork Date !!!");
            return false;
        }
        return true;
    }
    
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
       {
            HolidayWork aHolidayWork = new HolidayWork()
            {
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                HworkDate = Convert.ToDateTime(hWorkTextBox.Text),
                DutyLocation = dutyLocTextBox.Text,
                Purpose = purposTextBox.Text,
                ActionStatus = "Posted",
                ActionRemarks = remarksTextBox.Text,
                EntryUser = Session["LoginName"].ToString(),
                EntryDate = System.DateTime.Today,
                IsActive = true,
            };
            HolidayInfo aHolidayInfo=new HolidayInfo();
           aHolidayInfo.HolidayfromDate = Convert.ToDateTime(hWorkTextBox.Text);
           if (!aDutyBll.HasHolidayWork(aHolidayWork))
           {
               if (aDutyBll.SaveDataForHolidayWork(aHolidayWork, aHolidayInfo))
               {
                   showMessageBox("Data Save Successfully");
                   Clear();
               }
               else
               {
                   showMessageBox("There is no National or Weekly Holiday");
               }    
           }
           else
           {
               showMessageBox("Holiday Work Already Posted !!!");
           }
           
        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    protected void jobViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("HolidayWorkView.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = aDutyBll.LoadEmpInfo(empMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}