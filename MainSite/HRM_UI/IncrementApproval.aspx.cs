﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;
using System.Collections.Generic;

public partial class HRM_UI_IncrementApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    SalIncrementBLL aIncrementBll = new SalIncrementBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IncrementLoad();
        }
    }
    public void IncrementLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        IncrementLoadByCondition(filename, userName);
        aIncrementBll.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void IncrementLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aIncrementBll.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aIncrementBll.LoadIncrementViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void PopUp(string Id)
    {
        string url = "IncrementsEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string IncrementId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(IncrementId);
        }
    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[9].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }

    }

    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                SubmitApproval();
                IncrementLoad();
            }
        }
        catch (Exception)
        {
            
            showMessageBox("Choose Proper Data");
        }
        
    }
    public bool Validation()
    {
        int c = 0, i;
        for (i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[9].FindControl("chkSelect");
            if (cb.Checked != true)
            {
                c++;

            }
        }
        if (c == i)
        {
            showMessageBox("Choose check box ");
            return false;
        }
        return true;
    }

    private void SubmitApproval()
    {
        if (actionRadioButtonList.SelectedValue == null)
        {
            showMessageBox("Choose One of the Operations ");
        }
        else
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {

                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[9].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    Increment aIncrementMaster = new Increment()
                    {
                        IncrementId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        ApprovedBy = Session["LoginName"].ToString(),
                        ApprovedDate = System.DateTime.Today,
                    };
                    if (aIncrementMaster.ActionStatus == "Accepted")
                    {
                        if (aIncrementBll.ApprovalUpdateBLL(aIncrementMaster))
                        {
                            DataTable actdt = aIncrementBll.LoadActdt((loadGridView.DataKeys[i][0].ToString()));
                            DateTime activedt = Convert.ToDateTime(actdt.Rows[0]["ActiveDate"].ToString());
                            DateTime inactivedt = activedt.AddDays(-1);

                            List<EmployeeSalary> aEmployeeSalaryList = new List<EmployeeSalary>();
                            EmployeeSalary aEmployeeSalary;


                            for (int h = 0; h < actdt.Rows.Count; h++)
                            {
                                aEmployeeSalary = new EmployeeSalary();

                                aEmployeeSalary.EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString());
                                aEmployeeSalary.InactiveDate = inactivedt;
                                aEmployeeSalary.SalaryHeadId = Convert.ToInt32(actdt.Rows[h]["SalaryHeadId"]);
                                aEmployeeSalary.SalaryHead = Convert.ToString(actdt.Rows[h]["SalHeadName"]);
                                aEmployeeSalary.Amount = Convert.ToDecimal(actdt.Rows[h]["Amount"]);
                                aEmployeeSalary.ActiveDate = activedt;
                                aEmployeeSalary.ActionStatus = "Accepted";
                                aEmployeeSalary.EntryUser = Session["LoginName"].ToString();
                                aEmployeeSalary.EntryDate = Convert.ToDateTime(DateTime.Today.ToShortDateString());
                                aEmployeeSalary.ApprovedUser = Session["LoginName"].ToString();
                                aEmployeeSalary.ApprovedDate = Convert.ToDateTime(DateTime.Today.ToShortDateString());
                                aEmployeeSalary.IsActive = true;
                                aEmployeeSalaryList.Add(aEmployeeSalary);
                            }

                            if (aEmployeeSalaryList.Count > 0)
                            {
                                if (
                                    aIncrementBll.UpdateIncrementDetailsBLL(aEmployeeSalaryList,
                                        loadGridView.DataKeys[i][1].ToString()) == true)
                                {
                                    if (aIncrementBll.InsertIncrementDetailsBLL(aEmployeeSalaryList) == true)
                                    {
                                       showMessageBox("Data Accepted Successfully !!");
                                    }
                                    
                                }

                            }
                        }
                    }
                    else
                    {
                        if (aIncrementBll.ApprovalUpdateBLL(aIncrementMaster))
                        {
                        }
                        showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + "  Successfully");
                    }

                }
            }
        }


    }
}