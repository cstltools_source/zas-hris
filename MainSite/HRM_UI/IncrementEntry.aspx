﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="IncrementEntry.aspx.cs" Inherits="HRM_UI_IncrementEntry" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Employee Salary Increment Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="viewListImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Salary Increment Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-row">
                                     <div class="col-2">
                                        <div class="form-group">
                                            <label>Active Date </label>
                                            <div class="input-group date pull-left" id="daterangepicker12">
                                                <asp:TextBox ID="dateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                    Format="dd-MMM-yyyy" PopupButtonID="ImageButton2" CssClass="custom"
                                                    TargetControlID="dateTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                    <span>
                                                        <asp:ImageButton ID="ImageButton2" runat="server"
                                                            AlternateText="Click to show calendar"
                                                             ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Code </label>
                                            <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-1">
                                        <div class="form-group">
                                            <label style="color: white">Search </label>
                                            <br />
                                            <asp:Button ID="searchButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="searchButton_Click" Text="Search" />
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Employee Name </label>
                                            <asp:TextBox ID="empNameTextBox" CssClass="form-control form-control-sm" ReadOnly="True" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hdEmpInfoId" runat="server" />
                                        </div>
                                    </div>
                                </div>
                           
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                             <div class="form-row">
                               <div class="col-2">
                                    <div class="form-group">
                                        <label>Designation Name </label>
                                        <asp:Label ID="designationLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDesignationId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Section Name </label>
                                        <asp:Label ID="sectionLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdSectionId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department Name </label>
                                        <asp:Label ID="departmentLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDepartmentId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Division Name </label>
                                        <asp:Label ID="divisionLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDivisionID" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Grade </label>
                                        <asp:Label ID="empGradeLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdGradeId" runat="server" />
                                        <asp:HiddenField ID="hdCompanyID" runat="server" />
                                        <asp:HiddenField ID="hdUnitID" runat="server" />
                                        <asp:HiddenField ID="hdSalScaleId" runat="server" />
                                        <asp:HiddenField ID="hdEmpCategoryId" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Gross Amount </label>
                                        <asp:Label ID="grossAmountTextBox" CssClass="form-control form-control-sm" runat="server" Font-Bold="True"></asp:Label>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Salary Rule </label>
                                        <asp:DropDownList ID="salaryRuleDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Increment Amount </label>
                                        <asp:TextBox ID="incrementAmountTextBox" runat="server"
                                            CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Remarks </label>
                                        <asp:TextBox ID="remarksTextBox" runat="server"
                                            CssClass="form-control form-control-sm" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="calculateButton" CssClass="btn btn-sm btn-info" runat="server"
                                            OnClick="calculateButton_Click" Text="Calculate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12">
                                    <div id="gridContainer1" style="height: auto; overflow: auto; width: auto;">
                                        <asp:GridView ID="GVSalaryIncrement" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered text-center " DataKeyNames="SalaryHeadId">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SL">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SalaryHead" HeaderText="Salary Head">
<%--                                                    <FooterStyle BorderColor="#CCCCCC" Font-Size="Large" ForeColor="#000099" />--%>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Amount" HeaderText="Amount">
                                                    <FooterStyle Font-Bold="True" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Remove">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="removeLinkButton" runat="server"
                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"
                                                            Font-Underline="True">Remove</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--<EmptyDataRowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                            <SortedDescendingHeaderStyle BackColor="#242121" />--%>
                                            <HeaderStyle BackColor="#CCFFFF" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="Button2" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

