﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_IncrementView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    SalIncrementBLL aIncrementBll = new SalIncrementBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            IncrementLoad();
        }
    }
    private void IncrementLoad()
    {
        aDataTable = aIncrementBll.LoadEmpIncrementView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        aIncrementBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }
    protected void reloadLinkButton_Click(object sender, EventArgs eventArgs)
    {
        IncrementLoad();
    }
    protected void addImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("IncrementEntry.aspx");
    }

    private void PopUp(string Id)
    {
        string url = "IncrementsEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    protected void ShowMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);

            if (loadGridView.Rows[rowindex].Cells[10].Text != "Accepted")
            {
                string incId = loadGridView.DataKeys[rowindex][0].ToString();
                PopUp(incId);
            }
            else
            {
                ShowMessageBox("Accepted data is not editable !!");
            }


        }
    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[5].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                if (loadGridView.Rows[i].Cells[10].Text != "Accepted")
                {

                    string TransferId = loadGridView.DataKeys[i][0].ToString();
                    aIncrementBll.DeleteDataBLL(TransferId);
                }
                else
                {
                    ShowMessageBox("Accepted data cann't delete !!");
                }
            }
            

            IncrementLoad();
        }

    }
}