﻿<%@ Page Title="" Language="C#"  CodeFile="IncrementsEdit.aspx.cs" Inherits="HRM_UI_IncrementEntry" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form2" runat="server">

        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
            <div class="page-heading">
                <div class="page-heading__container">
                    <div class="icon"><span class="li-register"></span></div>
                    <span></span>
                    <h1 class="title" style="font-size: 18px; padding-top: 9px;">Employee Salary Increment Edit </h1>
                </div>
                <div class="page-heading__container float-right d-none d-sm-block">
                
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Monthly Tasks </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Salary Increment Edit</a></li>
                        </ol>
                    </nav>
            </div>
            <!-- //END PAGE HEADING -->
             <br/>
                    <br/>
            <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Active Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker12">
                                            <asp:TextBox ID="dateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton2" CssClass="custom"
                                                TargetControlID="dateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton2" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Code </label>
                                        <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-1">
                                    <div class="form-group">
                                        <label style="color: white">Search </label>
                                        <br />
                                        <asp:Button ID="searchButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="searchButton_Click" Text="Search" />
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name </label>
                                        <asp:TextBox ID="empNameTextBox" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                        <asp:HiddenField ID="hdEmpInfoId" runat="server" />   
                                        <asp:HiddenField ID="incrementHiddenField" runat="server" />
                                    </div>
                                </div>
                                 
                            </div>
                             <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Designation Name </label>
                                        <asp:Label ID="designationLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDesignationId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Section Name </label>
                                        <asp:Label ID="sectionLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdSectionId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Department Name </label>
                                        <asp:Label ID="departmentLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDepartmentId" runat="server" />
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Division Name </label>
                                        <asp:Label ID="divisionLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdDivisionID" runat="server" />
                                    </div>
                                </div>
                              </div>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Grade </label>
                                        <asp:Label ID="empGradeLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdGradeId" runat="server" />
                                        <asp:HiddenField ID="hdCompanyID" runat="server" />
                                        <asp:HiddenField ID="hdUnitID" runat="server" />
                                        <asp:HiddenField ID="hdSalScaleId" runat="server" />
                                        <asp:HiddenField ID="hdEmpCategoryId" runat="server" />
                                    </div>
                                </div>
                                 <div class="col-2">
                                    <div class="form-group">
                                        <label>Salary Rule </label>
                                        <asp:DropDownList ID="salaryRuleDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                               <div class="col-2">
                                    <div class="form-group">
                                        <label>Gross Amount </label>
                                        <asp:Label ID="grossAmountTextBox" CssClass="form-control form-control-sm" runat="server" Font-Bold="True"></asp:Label>
                                    </div>
                                </div>
                                
                               <div class="col-2">
                                    <div class="form-group">
                                        <label>Increment Amount </label>
                                        <asp:TextBox ID="incrementAmountTextBox" runat="server"
                                            CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-4">
                                    <div class="form-group">
                                        <label>Remarks </label>
                                        <asp:TextBox ID="remarksTextBox" runat="server"
                                            CssClass="form-control form-control-sm" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="calculateButton" CssClass="btn btn-sm btn-info" runat="server"
                                            OnClick="calculateButton_Click" Text="Calculate" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12">
                                    <div id="gridContainer1" style="height: auto; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                        <asp:GridView ID="GVSalaryIncrement" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered text-center thead-dark" DataKeyNames="SalaryHeadId">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SL">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="SalaryHead" HeaderText="Salary Head">
                                                    <FooterStyle BorderColor="#CCCCCC" Font-Size="Large" ForeColor="#000099" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Amount" HeaderText="Amount">
                                                    <FooterStyle Font-Bold="True" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Remove">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="removeLinkButton" runat="server"
                                                            CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove"
                                                            Font-Underline="True">Remove</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <%--<EmptyDataRowStyle HorizontalAlign="Center" />
                                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                            <SortedDescendingCellStyle BackColor="#E5E5E5" />--%>
                                            <SortedDescendingHeaderStyle BackColor="#242121" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <%--<asp:Button ID="Button2" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>





<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>EdEdit</title>
    <link href="../css/custom.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" href="../css/style.css" type="text/css">
    <link rel="stylesheet" href="../css/colors/blue.css" id="colors" type="text/css">
    <link href="../css/GV.css" rel="stylesheet" type="text/css" />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/Styles.css" rel="stylesheet" type="text/css" />
    </head>
<body>
    <form id="form1" runat="server">
    <div>
     <asp:ScriptManager ID="ScriptManager1" runat="server">
      </asp:ScriptManager>
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%" class="TableWorkArea">
                    <tr>
                        <td colspan="6" class="TableHeading">
                           Employee Salary Increment Entry</td>
                    </tr>
                     <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft" >
                            &nbsp;</td>
                         <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            Employee Master Code</td>
                        <td width="13%" class="TDLeft">
                            <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="TextBox" ></asp:TextBox>
                            <asp:AutoCompleteExtender ID="empCodeTextBox_AutoCompleteExtender" runat="server"
                                        DelimiterCharacters="" EnableCaching="true"
                                        Enabled="True" MinimumPrefixLength="1" CompletionSetCount="10"
                                        ServiceMethod="GetEmployee" ServicePath="HRMWebService.asmx"  TargetControlID="empCodeTextBox" 
                                        UseContextKey="True"
                                        CompletionListCssClass="autocomplete_completionListElement" 
                                        CompletionListItemCssClass="autocomplete_listItem" 
                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                        ShowOnlyCurrentWordInCompletionListItem="true">
                                    </asp:AutoCompleteExtender>
                        </td>
                        <td width="20%" class="TDRight">
                            <asp:Button ID="searchButton" runat="server" onclick="searchButton_Click" 
                                Text="Search" />
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                            Employee Name</td>
                        <td width="13%" class="TDLeft">
                            <asp:Label ID="empNameTextBox" runat="server" Text=""></asp:Label>
                          </td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                            Designation</td>
                        <td width="13%" class="TDLeft">
                            <asp:Label ID="designationLabel" runat="server"></asp:Label>
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                     <tr>
                         <td class="TDLeft" width="13%">
                             &nbsp;</td>
                         <td class="TDRight" width="20%">
                             Section</td>
                         <td class="TDLeft" width="13%">
                             <asp:Label ID="sectionLabel" runat="server"></asp:Label>
                         </td>
                         <td class="TDRight" width="20%">
                             &nbsp;</td>
                         <td class="TDLeft" width="13%">
                             &nbsp;</td>
                         <td class="TDRight" width="20%">
                             &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            Department</td>
                        <td class="TDLeft" width="13%">
                            <asp:Label ID="departmentLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                     <tr>
                         <td class="TDLeft" width="13%">
                             &nbsp;</td>
                         <td class="TDRight" width="20%">
                             Division</td>
                         <td class="TDLeft" width="13%">
                             <asp:Label ID="divisionLabel" runat="server"></asp:Label>
                         </td>
                         <td class="TDRight" width="20%">
                             &nbsp;</td>
                         <td class="TDLeft" width="13%">
                             &nbsp;</td>
                         <td class="TDRight" width="20%">
                             &nbsp;</td>
                    </tr>
                     <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            Employee Grade</td>
                        <td width="13%" class="TDLeft" >
                            <asp:Label ID="empGradeLabel" runat="server"></asp:Label>
                        </td>
                         <td width="20%" class="TDRight">
                             &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            Current Gross Salary</td>
                        <td class="TDLeft" width="13%">
                            <asp:Label ID="grossAmountTextBox" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                            Active Date</td>
                        <td width="13%" class="TDLeft">
                            <asp:TextBox ID="dateTextBox" runat="server" CssClass="TextBoxCalander"></asp:TextBox>
                            <asp:ImageButton runat="server" AlternateText="Click to show calendar" ImageUrl="~/Images/Calendar_scheduleHS.png"
                             TabIndex="4" ID="imgDate"></asp:ImageButton>
                            <asp:CalendarExtender ID="activedate" runat="server" Format="dd-MMM-yyyy" TargetControlID="dateTextBox"
                             PopupButtonID="imgDate"/>
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    
                   
                     <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            Salary Rule</td>
                        <td class="TDLeft" width="13%">
                            <asp:DropDownList ID="salaryRuleDropDownList" runat="server" 
                                CssClass="DropDown">
                            </asp:DropDownList>
                         </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">

                         </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                        </td>
                        <td class="TDRight" width="20%">
                            </td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            Increment Amount</td>
                        <td class="TDLeft" width="13%">
                            <asp:TextBox ID="incrementAmountTextBox" runat="server" 
                                CssClass="TextBoxCalander"></asp:TextBox>
                        </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            Remarks</td>
                        <td class="TDLeft" width="13%">
                            <asp:TextBox ID="remarksTextBox" runat="server" 
                                CssClass="TextBoxCalander" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:Button ID="calculateButton" runat="server" 
                                onclick="calculateButton_Click" Text="Calculate" />
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%" colspan="4">
                            <asp:GridView ID="GVSalaryIncrement" runat="server" 
                                AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" 
                                BorderStyle="None" BorderWidth="1px" CellPadding="4" CssClass="gridview" 
                                DataKeyNames="SalaryHeadId" ForeColor="Black" 
                                GridLines="Horizontal" HorizontalAlign="Center" 
                                onrowcommand="employeeSalaryGridView_RowCommand">
                                <Columns>
                                    <asp:BoundField DataField="SalaryHead" HeaderText="Salary Head" >
                                    <FooterStyle BorderColor="#CCCCCC" Font-Size="Large" ForeColor="#000099" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" >
                                    <FooterStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Remove">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="removeLinkButton" runat="server" 
                                                CommandArgument="<%# Container.DataItemIndex %>" CommandName="Remove" 
                                                Font-Underline="True">Remove</asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataRowStyle HorizontalAlign="Center" />
                                <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                                <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                                <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                                <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                                <SortedDescendingHeaderStyle BackColor="#242121" />
                            </asp:GridView>
                            
                        </td>
                        
                        
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="incrementHiddenField" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:Button ID="submitButton" runat="server" onclick="submitButton_Click" 
                                Text="Submit" />
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            <asp:HiddenField ID="hdEmpInfoId" runat="server" />
                        </td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="hdDepartmentId" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            <asp:HiddenField ID="hdEmpCategoryId" runat="server" />
                        </td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="hdCompanyID" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            <asp:HiddenField ID="hdDivisionID" runat="server" />
                        </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            <asp:HiddenField ID="hdDesignationId" runat="server" />
                        </td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="hdSectionId" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            <asp:HiddenField ID="hdGradeId" runat="server" />
                        </td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="hdSalScaleId" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            <asp:HiddenField ID="hdUnitID" runat="server" />
                        </td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft" >
                            &nbsp;
                        </td>
                         <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
  </form>
</body>
</html>--%>
