﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Adapters;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_IncrementEntry : System.Web.UI.Page
{
    SalIncrementBLL aSalIncrementBll = new SalIncrementBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            incrementHiddenField.Value = Request.QueryString["ID"];
            IncrementEditLoad(incrementHiddenField.Value);
        }
    }

    private void LoadDropDown()
    {
        aSalIncrementBll.LoadSalaryRuleToDropDownBLL(salaryRuleDropDownList, hdSalScaleId.Value.ToString());
    }

    protected void employeeSalaryGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Remove")
        {
            int deletingIndex = Convert.ToInt32(e.CommandArgument);
            List<EmployeeSalary> aEmployeeSalaryList = new List<EmployeeSalary>();
            EmployeeSalary aEmployeeSalary;

            int SalaryheadId = Convert.ToInt32(GVSalaryIncrement.DataKeys[deletingIndex][0].ToString());
            if (GVSalaryIncrement.Rows.Count > 0)
            {
                for (int i = 0; i < GVSalaryIncrement.Rows.Count; i++)
                {
                    if (SalaryheadId != Convert.ToInt32(GVSalaryIncrement.DataKeys[i][0].ToString()))
                    {
                        aEmployeeSalary = new EmployeeSalary()
                        {
                            SalaryHeadId = Convert.ToInt32(GVSalaryIncrement.DataKeys[i][0].ToString()),
                            SalaryHead = GVSalaryIncrement.Rows[i].Cells[0].Text.ToString(),
                            Amount = Convert.ToDecimal(GVSalaryIncrement.Rows[i].Cells[1].Text.Trim())
                        };
                        aEmployeeSalaryList.Add(aEmployeeSalary);
                    }
                }
            }


            GVSalaryIncrement.DataSource = null;
            GVSalaryIncrement.DataBind();
            GVSalaryIncrement.DataSource = aEmployeeSalaryList;
            GVSalaryIncrement.DataBind();
        }
    }

    private bool CheckValidation()
    {
        if (!aSalIncrementBll.CheckValidation(grossAmountTextBox.Text.Trim()))
        {
            showMessageBox("Input Amount!!");
            return false;
        }
        else
        {
            if (!aSalIncrementBll.CheckNumaric(grossAmountTextBox.Text.Trim()))
            {
                showMessageBox("Input Numaric Value!!");
                return false;
            }
        }
        //if (aSalIncrementBll.SameMonthIncrementCheck(hdEmpInfoId.Value, Convert.ToDateTime(dateTextBox.Text).Month.ToString()).Rows.Count > 0)
        //{
        //    showMessageBox("Increment Already Exist");
        //    return false;
        //}

        if (!aSalIncrementBll.CheckValidation(salaryRuleDropDownList.SelectedValue))
        {
            showMessageBox("Select Salary Head!!");
            return false;
        }
        if (!aSalIncrementBll.CheckValidation(empCodeTextBox.Text.Trim()))
        {
            showMessageBox("Input Employee!!");
            return false;
        }


        if (GVSalaryIncrement.Rows.Count == 0)
        {
            showMessageBox("Generate Calculation!!");
            return false;
        }
        return true;
    }
    private bool ValidationForCalculation()
    {
        if (!aSalIncrementBll.CheckValidation(grossAmountTextBox.Text.Trim()))
        {
            showMessageBox("Input Amount!!");
            return false;
        }
        else
        {
            if (!aSalIncrementBll.CheckNumaric(grossAmountTextBox.Text.Trim()))
            {
                showMessageBox("Input Numaric Value!!");
                return false;
            }
        }



        if (!aSalIncrementBll.CheckValidation(salaryRuleDropDownList.SelectedValue))
        {
            showMessageBox("Select Salary Head!!");
            return false;
        }
        return true;
    }
    protected void calculateButton_Click(object sender, EventArgs e)
    {
        Decimal totalamount = Convert.ToDecimal(grossAmountTextBox.Text) +
                              Convert.ToDecimal(incrementAmountTextBox.Text);

        if (ValidationForCalculation() == true)
        {
            DataTable dtSalaryCalculation = aSalIncrementBll.Calculate(salaryRuleDropDownList.SelectedValue,
            Convert.ToDecimal(totalamount));
            GVSalaryIncrement.DataSource = dtSalaryCalculation;
            GVSalaryIncrement.DataBind();
        }
    }
    private void ClearAll()
    {
        dateTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        designationLabel.Text = string.Empty;
        departmentLabel.Text = string.Empty;
        sectionLabel.Text = string.Empty;
        hdEmpInfoId.Value = string.Empty;
        hdDepartmentId.Value = string.Empty;
        hdDesignationId.Value = string.Empty;
        hdSectionId.Value = string.Empty;
        hdGradeId.Value = string.Empty;
        empGradeLabel.Text = string.Empty;
        hdEmpCategoryId.Value = string.Empty;
        GVSalaryIncrement.DataSource = null;
        GVSalaryIncrement.DataBind();
        empCodeTextBox.Text = string.Empty;
        salaryRuleDropDownList.DataSource = null;
        salaryRuleDropDownList.DataBind();
        divisionLabel.Text = string.Empty;
        incrementAmountTextBox.Text = string.Empty;
        remarksTextBox.Text = string.Empty;
        grossAmountTextBox.Text = string.Empty;
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool SaveIncrementDetail(int IncrementId)
    {
        List<IncrementDetail> aDetails = new List<IncrementDetail>();
        IncrementDetail aIncrementDetail;

        for (int i = 0; i < GVSalaryIncrement.Rows.Count; i++)
        {
            aIncrementDetail = new IncrementDetail();

            aIncrementDetail.IncrementId = Convert.ToInt32(incrementHiddenField.Value);
            aIncrementDetail.SalaryHeadId = Convert.ToInt32(GVSalaryIncrement.DataKeys[i][0].ToString());
            aIncrementDetail.SalHeadName = (GVSalaryIncrement.Rows[i].Cells[1].Text);
            aIncrementDetail.Amount = Convert.ToDecimal(GVSalaryIncrement.Rows[i].Cells[2].Text);
            aIncrementDetail.ActiveDate = Convert.ToDateTime(dateTextBox.Text.Trim());
            aIncrementDetail.IsActive = true;

            aDetails.Add(aIncrementDetail);

        }
        return aSalIncrementBll.SaveIncrementDetailBLL(aDetails);
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        int IncrementId = 0;
        if (hdEmpInfoId.Value != "")
        {
            if (CheckValidation() == true)
            {
                if (SaveData())
                {
                    if (SaveIncrementDetail(IncrementId))
                    {

                    }
                }
            }
        }
        else
        {
            showMessageBox("Search Employee First !!");
        }
    }
    private bool SaveData()
    {
        Increment aIncrement = new Increment()
        {
            IncrementId = Convert.ToInt32(incrementHiddenField.Value),
            EmpInfoId = Convert.ToInt32(hdEmpInfoId.Value),
            CompanyInfoId = Convert.ToInt32(hdCompanyID.Value),
            UnitId = Convert.ToInt32(hdUnitID.Value),
            SectionId = Convert.ToInt32(hdSectionId.Value),
            DeptId = Convert.ToInt32(hdDepartmentId.Value),
            DivisionId = Convert.ToInt32(hdDivisionID.Value),
            DesigId = Convert.ToInt32(hdDesignationId.Value),
            SalScaleId = Convert.ToInt32(hdSalScaleId.Value),
            IncrementAmount = Convert.ToDecimal(incrementAmountTextBox.Text),
            PreviousGrossSalary = Convert.ToDecimal(grossAmountTextBox.Text),
            IncrementRemarks = remarksTextBox.Text,
            ActiveDate = Convert.ToDateTime(dateTextBox.Text.Trim()),
            ActionStatus = "Posted",
            IsActive = true,
        };
        int EmpIncreId = 0;

        if (aSalIncrementBll.StartingSalaryCheck(hdEmpInfoId.Value))
        {
            if (aSalIncrementBll.UpdateIncrement(aIncrement))
            {
                if (aSalIncrementBll.DeleteIncrementDetail(incrementHiddenField.Value))
                {
                    showMessageBox("Employee Salary Update SuccessFully!!");
                    return true;
                }

            }
            else
            {
                showMessageBox("Employee Increment Already Posted!!");
            }
        }

        return true;
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        string empCode = empCodeTextBox.Text.Trim();
        GetEmployeeInfo(empCode);
    }

    private void GetEmployeeInfo(string empCode)
    {
        DataTable aDataTableEmp = new DataTable();
        if (!string.IsNullOrEmpty(empCode))
        {
            aDataTableEmp = aSalIncrementBll.EmpInformationBll(empCode);
            if (aDataTableEmp.Rows.Count > 0)
            {
                empNameTextBox.Text = aDataTableEmp.Rows[0]["EmpName"].ToString();
                designationLabel.Text = aDataTableEmp.Rows[0]["DesigName"].ToString();
                departmentLabel.Text = aDataTableEmp.Rows[0]["DeptName"].ToString();
                sectionLabel.Text = aDataTableEmp.Rows[0]["SectionName"].ToString();
                hdEmpInfoId.Value = aDataTableEmp.Rows[0]["EmpInfoId"].ToString();
                hdDepartmentId.Value = aDataTableEmp.Rows[0]["DepId"].ToString();
                hdDesignationId.Value = aDataTableEmp.Rows[0]["DesigId"].ToString();
                hdSectionId.Value = aDataTableEmp.Rows[0]["SectionId"].ToString();
                hdGradeId.Value = aDataTableEmp.Rows[0]["GradeId"].ToString();
                empGradeLabel.Text = aDataTableEmp.Rows[0]["GradeType"].ToString();
                hdSalScaleId.Value = aDataTableEmp.Rows[0]["SalScaleId"].ToString();
                hdCompanyID.Value = aDataTableEmp.Rows[0]["CompanyInfoId"].ToString();
                hdUnitID.Value = aDataTableEmp.Rows[0]["UnitId"].ToString();
                hdDivisionID.Value = aDataTableEmp.Rows[0]["DivisionId"].ToString();
                divisionLabel.Text = aDataTableEmp.Rows[0]["DivName"].ToString();
                LoadDropDown();
            }
            else
            {
                ClearAll();
                showMessageBox("Employee Not Found !!");
            }
        }
        DataTable aDataTableSal = new DataTable();
        aDataTableSal = aSalIncrementBll.EmpSalInformationBll(hdEmpInfoId.Value);
        if (aDataTableSal.Rows.Count > 0)
        {
            grossAmountTextBox.Text = aDataTableSal.Rows[0]["Amount"].ToString();
        }
        else
        {
            showMessageBox("Salary is Not Active For this Employee!!");
        }
    }

    protected void viewListImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("IncrementView.aspx");
    }

    public void IncrementEditLoad(string incrementId)
    {
        DataTable dtincrement = aSalIncrementBll.LoadIncrement(incrementId);
        empCodeTextBox.Text = dtincrement.Rows[0]["EmpMasterCode"].ToString();
        GetEmployeeInfo(empCodeTextBox.Text);
        dateTextBox.Text = Convert.ToDateTime(dtincrement.Rows[0]["ActiveDate"].ToString()).ToString("dd-MMM-yyyy");
        remarksTextBox.Text = dtincrement.Rows[0]["Remarks"].ToString();
        incrementAmountTextBox.Text = dtincrement.Rows[0]["IncrementAmount"].ToString();
        GVSalaryIncrement.DataSource = dtincrement;
        GVSalaryIncrement.DataBind();
        salaryRuleDropDownList.SelectedIndex = 1;
    }
}
