﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_JobDescriptionEdit : System.Web.UI.Page
{
    JobDescriptionBLL aJobDescriptionBll = new JobDescriptionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            jobDescIdHiddenField.Value = Request.QueryString["ID"];
            DepartmentNameLoad();
            DesignationNameLoad();
            JobDescriptionLoad(jobDescIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    public void DesignationNameLoad()
    {
        aJobDescriptionBll.LoadDesignationToDropDownBLL(designationDropDownList);
    }
    public void DepartmentNameLoad()
    {
        aJobDescriptionBll.LoadDepartmentToDropDownBLL(departmentDropDownList);
    }
    public bool JoiningDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(joiningDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (empMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (departmentDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (designationDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Designation !!");
            return false;
        }

        if (JoiningDate() == false)
        {
            showMessageBox("Please Input valid Joining Date !!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            JobDescription aJobDescription = new JobDescription()
            {
                JobDscId = Convert.ToInt32(jobDescIdHiddenField.Value),
                EmpMasterCode = empMasterCodeTextBox.Text,
                EmpName=empNameTextBox.Text,
                DesigId = Convert.ToInt32(designationDropDownList.SelectedValue),
                DeptId = Convert.ToInt32(departmentDropDownList.SelectedValue),
                JoiningDate = Convert.ToDateTime(joiningDateTextBox.Text),
                ReportingTo = reportingToTextBox.Text,
                JobObjective = jobObjTextBox.Text,
                DutiesTasks = dutiTaskTextBox.Text,
                ImediateSupport = imediatSupTextBox.Text,
                KeyPerformArea = keyPerAreaTextBox.Text
            };
            JobDescriptionBLL aJobDescriptionBll = new JobDescriptionBLL();

            if (!aJobDescriptionBll.UpdateDataForJobDescription(aJobDescription))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void JobDescriptionLoad(string jobDescId)
    {
        JobDescription aDescription = new JobDescription();
        aDescription = aJobDescriptionBll.JobDescriptionEditLoad(jobDescId);
        empMasterCodeTextBox.Text = aDescription.EmpMasterCode;
        empNameTextBox.Text = aDescription.EmpName;
        joiningDateTextBox.Text = aDescription.JoiningDate.ToString("dd-MMM-yyyy");
        reportingToTextBox.Text = aDescription.ReportingTo;
        jobObjTextBox.Text = aDescription.JobObjective;
        dutiTaskTextBox.Text = aDescription.DutiesTasks;
        imediatSupTextBox.Text = aDescription.ImediateSupport;
        keyPerAreaTextBox.Text = aDescription.KeyPerformArea;
        departmentDropDownList.SelectedValue = aDescription.DeptId.ToString();
        designationDropDownList.SelectedValue = aDescription.DesigId.ToString();
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
       if (empMasterCodeTextBox.Text != string.Empty)
        {
            DataTable aEmpGeneralDataTable = new DataTable();
            EmpGeneralInfoBLL aEmpGeneralInfoBll = new EmpGeneralInfoBLL();
            
            if (aEmpGeneralDataTable.Rows.Count > 0)
            {
                empNameTextBox.Text = aEmpGeneralDataTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("No Data Found");
            }
        }
        else
        {
            showMessageBox("Please Input a Employee Code");
        }
    }
}