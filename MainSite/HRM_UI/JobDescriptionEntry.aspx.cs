﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_JobDescriptionEntry : System.Web.UI.Page
{
    JobDescriptionBLL aJobDescriptionBll = new JobDescriptionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DepartmentNameLoad();
            DesignationNameLoad();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        empMasterCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        departmentDropDownList.SelectedValue = null;
        designationDropDownList.SelectedValue = null;
        joiningDateTextBox.Text = string.Empty;
        reportingToTextBox.Text = string.Empty;
        jobObjTextBox.Text = string.Empty;
        dutiTaskTextBox.Text = string.Empty;
        imediatSupTextBox.Text = string.Empty;
        keyPerAreaTextBox.Text = string.Empty;
       
    }
    public bool JoiningDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(joiningDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    public void DesignationNameLoad()
    {
        aJobDescriptionBll.LoadDesignationToDropDownBLL(designationDropDownList);
    }
    public void DepartmentNameLoad()
    {
        aJobDescriptionBll.LoadDepartmentToDropDownBLL(departmentDropDownList);
    }
    private bool Validation()
    {
        if (empMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (departmentDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (designationDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Designation !!");
            return false;
        }

        if (JoiningDate()==false)
        {
            showMessageBox("Please Input valid Joining Date !!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            JobDescription aDescription = new JobDescription()
            {
                DesigId = Convert.ToInt32(designationDropDownList.SelectedValue),
                DeptId = Convert.ToInt32(departmentDropDownList.SelectedValue),
                EmpMasterCode = empMasterCodeTextBox.Text,
                EmpName = empNameTextBox.Text,
                JoiningDate = Convert.ToDateTime(joiningDateTextBox.Text),
                ReportingTo = reportingToTextBox.Text,
                JobObjective = jobObjTextBox.Text,
                DutiesTasks = dutiTaskTextBox.Text,
                ImediateSupport = imediatSupTextBox.Text,
                KeyPerformArea = keyPerAreaTextBox.Text,
               
            };
            JobDescriptionBLL aJobDescriptionBll = new JobDescriptionBLL();
            if (aJobDescriptionBll.SaveDataForJobDescription(aDescription))
            {
                showMessageBox("Data Save Successfully");
                Clear();
            }   
        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    protected void jobViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("JobDescView.aspx");
    }

    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (empMasterCodeTextBox.Text != string.Empty)
        {
            DataTable aDataTable=new DataTable();
            JobDescriptionBLL aJobDescriptionBll=new JobDescriptionBLL();
            aDataTable = aJobDescriptionBll.LoadEmpInfoCode(empMasterCodeTextBox.Text);
            if (aDataTable.Rows.Count > 0)
            {
                empIdHiddenField.Value = aDataTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aDataTable.Rows[0]["EmpName"].ToString().Trim();
                designationDropDownList.SelectedValue = aDataTable.Rows[0]["DesigId"].ToString().Trim();
                departmentDropDownList.SelectedValue = aDataTable.Rows[0]["DepId"].ToString().Trim();
                joiningDateTextBox.Text = Convert.ToDateTime(aDataTable.Rows[0]["JoiningDate"].ToString()).ToString("dd-MMM-yyyy");
            }
            else
            {
                showMessageBox("No Data Found");
            }
        }
        else
        {
            showMessageBox("Please Input a Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}