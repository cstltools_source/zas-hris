﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_JobLeftApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    JobLeftBLL aJobLeftBLL = new JobLeftBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            JobLeftLoad();
        }
    }
    public void JobLeftLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        JobLeftLoadByCondition(filename, userName);
        aJobLeftBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void JobLeftLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aJobLeftBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aJobLeftBLL.LoadJobLeftViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void PopUp(string Id)
    {
        string url = "JobLeftEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string JobLeftId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(JobLeftId);
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }  
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validation())
            {
                SubmitApproval();
                JobLeftLoad();
            }
        }
        catch (Exception)
        {
            
          showMessageBox("choose data properly ");
        }
       
    }
    public bool Validation()
    {
        int c = 0, i;
        for (i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)loadGridView.Rows[i].FindControl("chkSelect");
            if (cb.Checked != true)
            {
                c++;

            }
        }
        if (c == i)
        {
            showMessageBox("Choose check box ");
            return false;
        }
        return true;
    }
    private void SubmitApproval()
    {
        if (actionRadioButtonList.SelectedValue == null)
        {
            showMessageBox("Choose One of the Operations ");
        }
        else
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    JobLeft aJobLeft = new JobLeft()
                    {
                        JobLeftId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        ApprovedBy = Session["LoginName"].ToString(),
                        ApprovedDate = System.DateTime.Today,
                    };
                    if (aJobLeft.ActionStatus == "Accepted")
                    {


                        if (aJobLeftBLL.ApprovalUpdateBLL(aJobLeft))
                        {
                            EmpGeneralInfo aEmpGeneralInfo = new EmpGeneralInfo();
                            aEmpGeneralInfo.EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString());
                            aEmpGeneralInfo.EmployeeStatus = "Inactive";
                            aJobLeftBLL.PlaceEmpStatus(aEmpGeneralInfo);
                            showMessageBox("Your Approval Successfully Done !!!");
                        }
                    }
                    else
                    {
                        if (aJobLeftBLL.ApprovalUpdateBLL(aJobLeft))
                        {
                            showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + " Successfully");
                        }
                    }
                }
            }
        }
         
    }
}