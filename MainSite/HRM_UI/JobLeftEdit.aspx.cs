﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_JobLeftEdit : System.Web.UI.Page
{
    JobLeftBLL _aJobLeftBll = new JobLeftBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            JobLeftIdHiddenField.Value = Request.QueryString["ID"];
            EmpSalBenefitLoad(JobLeftIdHiddenField.Value);
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
   
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            JobLeft aJobLeft = new JobLeft()
            {
                JobLeftId = Convert.ToInt32(JobLeftIdHiddenField.Value),
                EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                Status = "Inactive",
                CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value),
                UnitId = Convert.ToInt32(unitIdHiddenField.Value),
                DivisionId = Convert.ToInt32(divIdHiddenField.Value),
                DeptId = Convert.ToInt32(deptIdHiddenField.Value),
                SectionId = Convert.ToInt32(secIdHiddenField.Value),
                DesigId = Convert.ToInt32(desigIdHiddenField.Value),
                GradeId = Convert.ToInt32(empGradeIdHiddenField.Value),
                EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value),
                
                ActionStatus = "JobLeft"
                
            };
           
            if (!_aJobLeftBll.UpdateDataForEmpSalBenefit(aJobLeft))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpSalBenefitLoad(string JobLeftId)
    {
        JobLeft aJobLeft = new JobLeft();
        aJobLeft = _aJobLeftBll.EmpSalBenefitEditLoad(JobLeftId);
        effectDateTexBox.Text = aJobLeft.EffectiveDate.ToString("dd-MMM-yyyy");
        comIdHiddenField.Value= aJobLeft.CompanyInfoId.ToString();
        unitIdHiddenField.Value= aJobLeft.UnitId.ToString();
        divIdHiddenField.Value= aJobLeft.DivisionId.ToString();
        deptIdHiddenField.Value= aJobLeft.DeptId.ToString();
        secIdHiddenField.Value= aJobLeft.SectionId.ToString();
        desigIdHiddenField.Value= aJobLeft.DesigId.ToString();
        empGradeIdHiddenField.Value= aJobLeft.GradeId.ToString();
        empTypeIdHiddenField.Value= aJobLeft.EmpTypeId.ToString();
        EmpInfoIdHiddenField.Value = aJobLeft.EmpInfoId.ToString();
        
        
        GetEmpMasterCode(EmpInfoIdHiddenField.Value);
        GetNames(comIdHiddenField.Value,unitIdHiddenField.Value,divIdHiddenField.Value,deptIdHiddenField.Value,desigIdHiddenField.Value,secIdHiddenField.Value,empGradeIdHiddenField.Value,empTypeIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aJobLeftBll.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                EmpMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }  
    }

    public void GetNames(string comId,string unitId,string divId,string deptId,string desigId,string secId,string gradeId,string emptId)
    {
        
            DataTable aTableCom = new DataTable();
            DataTable aTableUnit = new DataTable();
            DataTable aTableCDiv = new DataTable();
            DataTable aTableDept = new DataTable();
            DataTable aTableDesig = new DataTable();
            DataTable aTableSec = new DataTable();
            DataTable aTableGrade = new DataTable();
            DataTable aTableType = new DataTable();
            aTableCom = _aJobLeftBll.LoadCompanyInfo(comId);
            aTableUnit= _aJobLeftBll.LoadUnit(unitId);
            aTableCDiv= _aJobLeftBll.Loadivision(divId);
            aTableDept = _aJobLeftBll.LoadDepartment(deptId);
            aTableDesig = _aJobLeftBll.LoadDesignation(desigId);
            aTableSec = _aJobLeftBll.LoadSection(secId);
            aTableGrade = _aJobLeftBll.LoadGrade(gradeId);
            aTableType = _aJobLeftBll.LoadEmpType(emptId);
            

            if (aTableCom.Rows.Count > 0)
            {
                comNameLabel.Text = aTableCom.Rows[0]["CompanyName"].ToString().Trim();
            }
        if (aTableUnit.Rows.Count > 0)
        {
            unitNameLabel.Text = aTableUnit.Rows[0]["UnitName"].ToString().Trim();
        }

        if (aTableCDiv.Rows.Count > 0)
        {
            divNameLabel.Text = aTableCDiv.Rows[0]["DivName"].ToString().Trim();
        }
        if (aTableDept.Rows.Count > 0)
        {
            deptNameLabel.Text = aTableDept.Rows[0]["DeptName"].ToString().Trim();
        }
        if (aTableDesig.Rows.Count > 0)
        {
            desigNameLabel.Text = aTableDesig.Rows[0]["DesigName"].ToString().Trim();
        }
        if (aTableSec.Rows.Count > 0)
        {
            secNameLabel.Text = aTableSec.Rows[0]["SectionName"].ToString().Trim();
        }
        if (aTableGrade.Rows.Count > 0)
        {
            empGradeLabel.Text = aTableGrade.Rows[0]["GradeName"].ToString().Trim();
        }
        if (aTableType.Rows.Count > 0)
        {
            empTypeLabel.Text = aTableType.Rows[0]["EmpType"].ToString().Trim();
        }

    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aJobLeftBll.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                empGradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
                joiningdtHiddenField.Value = aTable.Rows[0]["JoiningDate"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
}