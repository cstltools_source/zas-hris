﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.HRM_BLL;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_JobLocationView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    JobLocalionBll aDivisionBll = new JobLocalionBll();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DivisionLoad();
        }
    }

    private void DivisionLoad()
    {
        aDataTable = aDivisionBll.LoadDataforDivisionView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }


    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        DivisionLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("JobLocationEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "JobLocationEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string DivisionId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(DivisionId);
        }

    }

    protected void loadGridView_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        // {
        //    if(e.Row.RowIndex == 0)
        //    e.Row.Style.Add("height","120px");
        // }
    }
}