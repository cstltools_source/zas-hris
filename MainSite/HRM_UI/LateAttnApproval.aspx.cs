﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LateAttendanceApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    LateApproveBLL aLateApproveBLL = new LateApproveBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LateApproveLoad();
        }
    }
    public void LateApproveLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        LateLoadByCondition(filename, userName);
        aLateApproveBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void LateLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aLateApproveBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aLateApproveBLL.LoadLateApproveViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    private void PopUp(string Id)
    {
        string url = "LateAttnApproveEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string LateApproveId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(LateApproveId);
        }

    }

    private bool Validation()
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox) loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
            if (ChkBoxRows.Checked == true)
            {
                return true;
               
            }
        }
        showMessageBox("Select Atleast One!!");
        return false;
    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[8].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        if (Validation()==true)
        {
            try
            {
                SubmitApproval();
                LateApproveLoad();
            }
            catch (Exception)
            {

                showMessageBox("choose data properly ");
            }
            
        }
        
    }
    
    private void SubmitApproval()
    {
        if (actionRadioButtonList.SelectedValue == null)
        {
            showMessageBox("Choose One of the Operations ");
        }
        else
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[8].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    LateApprove aLateApprove = new LateApprove()
                    {
                        LateApproveId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        ApprovedBy = Session["LoginName"].ToString(),
                        ApprovedDate = System.DateTime.Today,
                    };
                    


                        if (aLateApproveBLL.ApprovalUpdateBLL(aLateApprove))
                        {
                            if (aLateApprove.ActionStatus == "Accepted")
                            {
                            string empid = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString()).ToString();
                            DateTime date = Convert.ToDateTime(loadGridView.DataKeys[i][2].ToString());
                            string status = "P";
                            aLateApproveBLL.PlaceLate(empid, date, status);
                           

                            }
                    }
                    else
                    {
                       
                    }

                }
            }
            showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + " Successfully");
        }
         
    }
}