﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LateAttnApproveEdit.aspx.cs" Inherits="HRM_UI_LateAttnApproveEdit" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form2" runat="server">

        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
            <div class="page-heading">
                <div class="page-heading__container">
                    <div class="icon"><span class="li-register"></span></div>
                    <span></span>
                    <h1 class="title" style="font-size: 18px; padding-top: 9px;">Late Attendance Approve Edit </h1>
                </div>
                <div class="page-heading__container float-right d-none d-sm-block">
                   
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Late Attendance Approve Edit</a></li>

                        </ol>
                    </nav>
            </div>
            <!-- //END PAGE HEADING -->
                 <br/>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                             <div class="form-row">
                               <div class="col-2">
                                <div class="form-group">
                                    <label>Effective Date: </label>
                                    <asp:TextBox ID="effectDateTexBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                     <asp:ImageButton runat="server" AlternateText="Click to show calendar" 
                                     ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" ID="imgDate"></asp:ImageButton>
                                    <asp:CalendarExtender ID="effectDate" runat="server" Format="dd-MMM-yyyy" TargetControlID="effectDateTexBox"
                                     PopupButtonID="imgDate" CssClass="custom" PopupPosition="TopLeft"> </asp:CalendarExtender>
                                   </div>
                               </div>
                                 <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Code :</label>
                                        <asp:TextBox ID="EmpMasterCodeTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                             <div class="col-3">
                                <div class="form-group">
                                    <label>Employee Name :</label>
                                    <asp:TextBox ID="empNameTexBox" CssClass="form-control form-control-sm" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="EmpInfoIdHiddenField" runat="server" />
                                    <asp:HiddenField ID="joiningdtHiddenField" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="card">
                      <div class="card-body">
                          <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                           <div class="form-row">
                               <div class="col-2">
                                    <div class="form-group">
                                        <label>Company Name :</label>
                                        <asp:Label ID="comNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Unit Name :</label>
                                        <asp:Label ID="unitNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Division Name :</label>
                                        <asp:Label ID="divNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField3" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department Name :</label>
                                        <asp:Label ID="deptNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField4" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Section Name :</label>
                                        <asp:Label ID="secNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField5" runat="server" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Designation Name :</label>
                                        <asp:Label ID="desigNameLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField6" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label> Employee Grade :</label>
                                        <asp:Label ID="empGradeLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField7" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Type :</label>
                                        <asp:Label ID="empTypeLabel" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                        <asp:HiddenField ID="HiddenField8" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Late Date :</label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                             <asp:TextBox ID="lateDateTexBox" runat="server" CssClass="TextBoxCalander" 
                                                AutoPostBack="True" ontextchanged="lateDateTexBox_TextChanged"></asp:TextBox>
                                                 <asp:CalendarExtender ID="lateDateTexBox_CalendarExtender" runat="server" 
                                                    Format="dd-MMM-yyyy" PopupButtonID="imglDate" CssClass="custom" PopupPosition="TopLeft"
                                                      TargetControlID="lateDateTexBox"> </asp:CalendarExtender>
                                                    <asp:ImageButton ID="imglDate" runat="server" 
                                                  AlternateText="Click to show calendar" 
                                                 ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <div class="form-row">
                            <div class="col-2">
                                <div class="form-group">
                                    <label>  Shift In time : </label>
                                    <asp:Label ID="shiftIntimeTextBox" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                </div>
                              </div>
                              <div class="col-2">
                                    <div class="form-group">
                                        <label> Attendanc In time : </label>
                                         <asp:Label ID="attIntimeTextBox" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                    </div>
                                </div>
                             <div class="col-2">
                                <div class="form-group">
                                    <label> Late In time : </label>
                                    <asp:Label ID="lateIntimeTextBox" CssClass="form-control form-control-sm" runat="server"></asp:Label>
                                </div>
                              </div>
                              <div class="col-2">
                                    <div class="form-group">
                                        <label> Late Reason : </label>
                                          <asp:TextBox ID="lateResionTextBox" runat="server" CssClass="form-control form-control-sm" 
                                           TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                             </div>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                     <asp:Button ID="Button1" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="updateButton_Click" />
                                    <asp:Button ID="cancelButton" Text="Close" CssClass="btn btn-sm warning" runat="server" OnClick="closeButton_Click" BackColor="#FF9900" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             <asp:HiddenField ID="LateApproveIdHiddenField" runat="server" />
            <asp:HiddenField ID="empIdHiddenField" runat="server" />
            <asp:HiddenField ID="comIdHiddenField" runat="server" />
            <asp:HiddenField ID="unitIdHiddenField" runat="server" />
            <asp:HiddenField ID="divIdHiddenField" runat="server" />
            <asp:HiddenField ID="deptIdHiddenField" runat="server" />
            <asp:HiddenField ID="secIdHiddenField" runat="server" />
            <asp:HiddenField ID="desigIdHiddenField" runat="server" />
            <asp:HiddenField ID="empGradeIdHiddenField" runat="server" />
            <asp:HiddenField ID="empTypeIdHiddenField" runat="server" />
        
         </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>
