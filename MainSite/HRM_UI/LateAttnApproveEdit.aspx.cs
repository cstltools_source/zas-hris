﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LateAttnApproveEdit : System.Web.UI.Page
{
    LateApproveBLL _aLateApproveBLL = new LateApproveBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LateApproveIdHiddenField.Value = Request.QueryString["ID"];
            EmpLateApproveLoad(LateApproveIdHiddenField.Value);
        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }

        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            LateApprove aLateApprove = new LateApprove()
            {
                LateApproveId = Convert.ToInt32(LateApproveIdHiddenField.Value),
                EmpInfoId = Convert.ToInt32(empIdHiddenField.Value),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value),
                UnitId = Convert.ToInt32(unitIdHiddenField.Value),
                DivisionId = Convert.ToInt32(divIdHiddenField.Value),
                DeptId = Convert.ToInt32(deptIdHiddenField.Value),
                SectionId = Convert.ToInt32(secIdHiddenField.Value),
                DesigId = Convert.ToInt32(desigIdHiddenField.Value),
                GradeId = Convert.ToInt32(empGradeIdHiddenField.Value),
                EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value),
                ShiftIntime = Convert.ToDateTime((shiftIntimeTextBox.Text)).TimeOfDay,
                AttIntime = Convert.ToDateTime((attIntimeTextBox.Text)).TimeOfDay,
                LateIntime = Convert.ToDateTime((lateIntimeTextBox.Text)).TimeOfDay,
                LateReason = lateResionTextBox.Text,
                LateDate = Convert.ToDateTime(lateDateTexBox.Text),


            };

            if (!_aLateApproveBLL.UpdateDataForEmpSalBenefit(aLateApprove))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpLateApproveLoad(string LateApproveId)
    {
        LateApprove aLateApprove = new LateApprove();
        aLateApprove = _aLateApproveBLL.LateApproveEditLoad(LateApproveId);
        effectDateTexBox.Text = aLateApprove.EffectiveDate.ToString("dd-MMM-yyyy");
        shiftIntimeTextBox.Text = aLateApprove.ShiftIntime.ToString();
        attIntimeTextBox.Text = aLateApprove.AttIntime.ToString();
        lateDateTexBox.Text = aLateApprove.LateDate.ToString("dd-MMM-yyyy");
        lateIntimeTextBox.Text = aLateApprove.LateIntime.ToString();
        lateResionTextBox.Text = aLateApprove.LateReason;
        comIdHiddenField.Value = aLateApprove.CompanyInfoId.ToString();
        unitIdHiddenField.Value = aLateApprove.UnitId.ToString();
        divIdHiddenField.Value = aLateApprove.DivisionId.ToString();
        deptIdHiddenField.Value = aLateApprove.DeptId.ToString();
        secIdHiddenField.Value = aLateApprove.SectionId.ToString();
        desigIdHiddenField.Value = aLateApprove.DesigId.ToString();
        empGradeIdHiddenField.Value = aLateApprove.GradeId.ToString();
        empTypeIdHiddenField.Value = aLateApprove.EmpTypeId.ToString();
        empIdHiddenField.Value = aLateApprove.EmpInfoId.ToString();


        GetEmpMasterCode(empIdHiddenField.Value);
        GetNames(comIdHiddenField.Value, unitIdHiddenField.Value, divIdHiddenField.Value, deptIdHiddenField.Value, desigIdHiddenField.Value, secIdHiddenField.Value, empGradeIdHiddenField.Value, empTypeIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aLateApproveBLL.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                EmpMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    public void GetNames(string comId, string unitId, string divId, string deptId, string desigId, string secId, string gradeId, string emptId)
    {

        DataTable aTableCom = new DataTable();
        DataTable aTableUnit = new DataTable();
        DataTable aTableCDiv = new DataTable();
        DataTable aTableDept = new DataTable();
        DataTable aTableDesig = new DataTable();
        DataTable aTableSec = new DataTable();
        DataTable aTableGrade = new DataTable();
        DataTable aTableType = new DataTable();
        aTableCom = _aLateApproveBLL.LoadCompanyInfo(comId);
        aTableUnit = _aLateApproveBLL.LoadUnit(unitId);
        aTableCDiv = _aLateApproveBLL.Loadivision(divId);
        aTableDept = _aLateApproveBLL.LoadDepartment(deptId);
        aTableDesig = _aLateApproveBLL.LoadDesignation(desigId);
        aTableSec = _aLateApproveBLL.LoadSection(secId);
        aTableGrade = _aLateApproveBLL.LoadGrade(gradeId);
        aTableType = _aLateApproveBLL.LoadEmpType(emptId);


        if (aTableCom.Rows.Count > 0)
        {
            comNameLabel.Text = aTableCom.Rows[0]["CompanyName"].ToString().Trim();
        }
        if (aTableUnit.Rows.Count > 0)
        {
            unitNameLabel.Text = aTableUnit.Rows[0]["UnitName"].ToString().Trim();
        }

        if (aTableCDiv.Rows.Count > 0)
        {
            divNameLabel.Text = aTableCDiv.Rows[0]["DivName"].ToString().Trim();
        }
        if (aTableDept.Rows.Count > 0)
        {
            deptNameLabel.Text = aTableDept.Rows[0]["DeptName"].ToString().Trim();
        }
        if (aTableDesig.Rows.Count > 0)
        {
            desigNameLabel.Text = aTableDesig.Rows[0]["DesigName"].ToString().Trim();
        }
        if (aTableSec.Rows.Count > 0)
        {
            secNameLabel.Text = aTableSec.Rows[0]["SectionName"].ToString().Trim();
        }
        if (aTableGrade.Rows.Count > 0)
        {
            empGradeLabel.Text = aTableGrade.Rows[0]["GradeName"].ToString().Trim();
        }
        if (aTableType.Rows.Count > 0)
        {
            empTypeLabel.Text = aTableType.Rows[0]["EmpType"].ToString().Trim();
        }

    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void EmpMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aLateApproveBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                empGradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void lateDateTexBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lateDateTexBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aLateApproveBLL.LoadLateTime(Convert.ToDateTime(lateDateTexBox.Text), empIdHiddenField.Value);

            if (aTable.Rows.Count > 0)
            {

                shiftIntimeTextBox.Text = aTable.Rows[0]["ShiftStart"].ToString().Trim();
                attIntimeTextBox.Text = aTable.Rows[0]["InTime"].ToString().Trim();
                lateIntimeTextBox.Text = aTable.Rows[0]["InTime"].ToString().Trim();

            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {

    }
}