﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LateAttnApproveEntry : System.Web.UI.Page
{
    LateApproveBLL _aLateApproveBll = new LateApproveBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        effectDateTexBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        GradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        empGradeLabel.Text = string.Empty;
        empTypeLabel.Text = string.Empty;
        shiftIntimeTextBox.Text = string.Empty;
        attIntimeTextBox.Text = string.Empty;
        lateIntimeTextBox.Text = string.Empty;
        lateResionTextBox.Text = string.Empty;
        lateDateTexBox.Text = string.Empty;

    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }

        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }

        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (_aLateApproveBll.VarifyLate(lateDateTexBox.Text, EmpInfoIdHiddenField.Value))
        {
            if (Validation() == true)
            {
                LateApprove aLateApprove = new LateApprove();
                aLateApprove.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
                aLateApprove.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
                aLateApprove.CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value);
                aLateApprove.UnitId = Convert.ToInt32(unitIdHiddenField.Value);
                aLateApprove.DivisionId = Convert.ToInt32(divIdHiddenField.Value);
                aLateApprove.DeptId = Convert.ToInt32(deptIdHiddenField.Value);
                aLateApprove.SectionId = Convert.ToInt32(secIdHiddenField.Value);
                aLateApprove.DesigId = Convert.ToInt32(desigIdHiddenField.Value);
                aLateApprove.GradeId = Convert.ToInt32(GradeIdHiddenField.Value);
                aLateApprove.EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value);
                aLateApprove.EntryUser = Session["LoginName"].ToString();
                aLateApprove.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                aLateApprove.ActionStatus = "Posted";
                aLateApprove.ShiftIntime = Convert.ToDateTime((shiftIntimeTextBox.Text)).TimeOfDay;
                aLateApprove.AttIntime = Convert.ToDateTime((attIntimeTextBox.Text)).TimeOfDay;
                aLateApprove.LateIntime = Convert.ToDateTime((lateIntimeTextBox.Text)).TimeOfDay;
                aLateApprove.LateReason = lateResionTextBox.Text;
                aLateApprove.LateDate = Convert.ToDateTime(lateDateTexBox.Text);
                aLateApprove.IsActive = true;


                if (_aLateApproveBll.SaveDataForLateApprove(aLateApprove))
                {
                    showMessageBox("Data Save Successfully ");
                    Clear();
                }
                else
                {
                    showMessageBox(" This Employee data already exist");
                }
            }
            else
            {
                showMessageBox("Please input data in all Textbox");
            }
        }
        else
        {
            showMessageBox("This Employee is not Late !!!");
            Clear();
        }

    }
    protected void departmentListImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("LateAttnApproveView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aLateApproveBll.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                GradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void lateDateTexBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lateDateTexBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aLateApproveBll.LoadLateTime(Convert.ToDateTime(lateDateTexBox.Text), EmpInfoIdHiddenField.Value);

            if (aTable.Rows.Count > 0)
            {

                shiftIntimeTextBox.Text = aTable.Rows[0]["ShiftStart"].ToString().Trim();
                attIntimeTextBox.Text = aTable.Rows[0]["InTime"].ToString().Trim();
                lateIntimeTextBox.Text = aTable.Rows[0]["InTime"].ToString().Trim();

            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}