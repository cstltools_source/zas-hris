﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="LeaveApplicationReport.aspx.cs" Inherits="HRM_UI_LeaveApplicationReport" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Leave Application Report</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block"> </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Daily Tasks- </a></li>
                            <li class="breadcrumb-item"><a href="#">Leave Application Report</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-body">
                                <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                                <div class="form-row">
                                      <div class="col-3">
                                            <div class="form-group">
                                                <label>From Date </label>
                                                <div class="input-group date pull-left" id="daterangepicker113">
                                                    <asp:TextBox ID="fromdateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom"
                                                        TargetControlID="fromdateTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton3" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                 ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>To Date </label>
                                                <div class="input-group date pull-left" id="daterangepicker12">
                                                    <asp:TextBox ID="todateTextBox" runat="server" class="form-control form-control-sm" AutoPostBack="True" CausesValidation="true" OnTextChanged="todateTextBox_TextChanged" ></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton4" CssClass="custom"
                                                        TargetControlID="todateTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton4" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                 ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                           <div class="card-body"> 
                            <div id="gridContainer1" style="height: 360px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                 <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" 
                                        CssClass="table table-bordered text-center thead-dark" DataKeyNames="LeaveAvailId,EmpInfoId" >
                                        <Columns>
                                            <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                            <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                            <asp:BoundField DataField="DeptName" HeaderText="Department" />
                                            <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                            <asp:BoundField DataField="LeaveName" HeaderText="Leave Name" />
                                            <asp:BoundField DataField="AvailLeaveQty" HeaderText="Day" />
                                            <asp:TemplateField HeaderText="View Report">
                                                <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="../Assets/Rptview (2).jpg" OnClick="LinkButton1_Click"></asp:ImageButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    </div>
                                </div>

                               <br />
                             <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <%--<asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewButton_Click" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>


