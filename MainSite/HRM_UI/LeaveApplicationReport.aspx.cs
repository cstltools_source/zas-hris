﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_LeaveApplicationReport : System.Web.UI.Page
{
    LeaveAvailBLL availBll = new LeaveAvailBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadGridView();
        }
    }

    public void LoadGridView()
    {
        DataTable dt = availBll.LoadLeaveAvailView(fromdateTextBox.Text, todateTextBox.Text);
        loadGridView.DataSource = dt;
        loadGridView.DataBind();
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        ImageButton ImageButton = (ImageButton)sender;

        GridViewRow currentRow = (GridViewRow)ImageButton.Parent.Parent;
        int rowindex = 0;
        rowindex = currentRow.RowIndex;

        int id = Convert.ToInt32(loadGridView.DataKeys[rowindex][0].ToString());
        Session["ID"] = id;
        Session["ReportParameter"] = " AND dbo.tblEmpGeneralInfo.EmpMasterCode='" + loadGridView.Rows[rowindex].Cells[0].Text + "' ";

        PopUp(Convert.ToDateTime(fromdateTextBox.Text).Year.ToString(), "", "SN");
    }
    private void PopUp(string year, string empcode, string reportFlag)
    {
        string url = "../Report_UI/LeaveReportViewer.aspx?year=" + year + "&empcode=" + empcode + "&ReportFlag=" + reportFlag;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void todateTextBox_TextChanged(object sender, EventArgs e)
    {
        LoadGridView();
    }
}