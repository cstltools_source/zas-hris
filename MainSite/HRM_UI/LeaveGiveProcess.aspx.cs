﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAL.HRM_DAL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LeaveGiveProcess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    protected void generteButton_Click(object sender, EventArgs e)
    {
        LeaveProcessDAL aLeaveProcessDal = new LeaveProcessDAL();
        int count = 0;
        count = aLeaveProcessDal.LeaveProcess("HRDB");
        showMessageBox(count.ToString() + " Data Processed");
    }
}