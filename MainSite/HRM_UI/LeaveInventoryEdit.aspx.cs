﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LeaveInventoryEdit : System.Web.UI.Page
{
    private LeaveInventoryBLL aInventoryBll = new LeaveInventoryBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LeaveNameLoad();
            CurrentYearLoad();
            leaveInventoryIdHiddenField.Value = Request.QueryString["ID"];
            LeaveInventoryLoad(leaveInventoryIdHiddenField.Value);
        }
    }
    private void CurrentYearLoad()
    {
        int i = DateTime.Now.Year;
        for (i = i - 1; i <= DateTime.Now.Year + 3; i++)
            leaveYearDropDownList.Items.Add(Convert.ToString(i));
        
        //leaveYearTextBox.Text = today.Trim();
    }
   
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    public void LeaveNameLoad()
    {
        aInventoryBll.LoadLeaveName(leaveNameDropDownList);
    }
    private bool Validation()
    {
        if (leaveNameDropDownList.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        //if (leaveYearDropDownList.SelectedIndex == 0)
        //{
        //    showMessageBox("Please Select Leave Year Name !!");
        //    return false;
        //}
        if (dayQtyTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
        if (employMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Select Date !!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            LeaveInventory aLeaveInventory = new LeaveInventory()
            {
                LeaveInventoryId = Convert.ToInt32(leaveInventoryIdHiddenField.Value),
                LeaveId = Convert.ToInt32(leaveNameDropDownList.SelectedValue),
                LeaveName = leaveNameDropDownList.SelectedItem.Text,
                LeaveYear = leaveYearDropDownList.SelectedItem.Text,
                DayQty = dayQtyTextBox.Text,
                EmpMasterCode = employMasterCodeTextBox.Text,
                EmpName = empNameTextBox.Text,
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value)
            };
           
            if (!aInventoryBll.UpdateDataForLeaveInventory(aLeaveInventory))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void LeaveInventoryLoad(string leaveId)
    {
        LeaveInventory aInventory = new LeaveInventory();
        aInventory = aInventoryBll.LeaveInventoryEditLoad(leaveId);
        employMasterCodeTextBox.Text = aInventory.EmpMasterCode;
        empNameTextBox.Text = aInventory.EmpName;
        leaveNameDropDownList.SelectedValue = aInventory.LeaveId.ToString();
        dayQtyTextBox.Text = aInventory.DayQty.ToString();
        leaveYearDropDownList.SelectedItem.Text = aInventory.LeaveYear.ToString();
        empInfoIdHiddenField.Value = aInventory.EmpInfoId.ToString();
    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void employMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (employMasterCodeTextBox.Text != string.Empty)
        {
            DataTable aEmpGeneralDataTable = new DataTable();
            LeaveInventoryBLL aLeaveInventoryBll = new LeaveInventoryBLL();
            aEmpGeneralDataTable = aLeaveInventoryBll.LoadEmpInfoCode(employMasterCodeTextBox.Text.Trim());

            if (aEmpGeneralDataTable.Rows.Count > 0)
            {
                empNameTextBox.Text = aEmpGeneralDataTable.Rows[0]["EmpName"].ToString().Trim();
                empInfoIdHiddenField.Value = aEmpGeneralDataTable.Rows[0]["EmpInfoId"].ToString().Trim();
            }
            else
            {
                showMessageBox("No Data Found");
            }
        }
        else
        {
            showMessageBox("Please Input a Employee Code");
        }
    }
}