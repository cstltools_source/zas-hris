﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LeaveInventoryEntry : System.Web.UI.Page
{
    private LeaveInventoryBLL aInventoryBll = new LeaveInventoryBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LeaveNameLoad();
            LoadYear();
            }
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 1; i <= DateTime.Now.Year + 3; i++)
            leaveYearDropDownList.Items.Add(Convert.ToString(i));
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private void Clear()
    {
        leaveNameDropDownList.SelectedValue = null;
        dayQtyTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        employMasterCodeTextBox.Text = string.Empty;
    }

    public void LeaveNameLoad()
    {
        aInventoryBll.LoadLeaveName(leaveNameDropDownList);
    }
    private bool Validation()
    {
        if (leaveNameDropDownList.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (leaveYearDropDownList.Text == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (dayQtyTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
        if (employMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Select Date !!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            LeaveInventory aLeaveInventory = new LeaveInventory()
            {
                LeaveId = Convert.ToInt32(leaveNameDropDownList.SelectedValue),
                LeaveName = leaveNameDropDownList.SelectedItem.Text,
                LeaveYear = leaveYearDropDownList.SelectedItem.Text,
                DayQty = dayQtyTextBox.Text,
                YearDayQty = Convert.ToDecimal(dayQtyTextBox.Text),
                EmpMasterCode = employMasterCodeTextBox.Text,
                EmpName = empNameTextBox.Text,
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                EntryDate = DateTime.Now,
                EntryBy = Session["LoginName"].ToString(),
                IsActive = true,
            };
            LeaveInventoryBLL aLeaveInventoryBll = new LeaveInventoryBLL();
            if (aLeaveInventoryBll.SaveDataForLeave(aLeaveInventory))
            {
                showMessageBox("Data Save Successfully For Employee Code is :" + aLeaveInventory.EmpMasterCode);
                Clear();    
            }
            else
            {
                showMessageBox("Leave Already Inserted");
            }  
        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }
    
    protected void jobViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("LeaveInventoryView.aspx");
    }

    private void CurrentYearLoad()
    {
        string today = System.DateTime.Today.Year.ToString();
        leaveYearDropDownList.SelectedItem.Text = today.Trim();
    }
   
    protected void employMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (employMasterCodeTextBox.Text != string.Empty)
        {
            DataTable aEmpGeneralDataTable = new DataTable();
            LeaveInventoryBLL aLeaveInventoryBll=new LeaveInventoryBLL();
            aEmpGeneralDataTable = aLeaveInventoryBll.LoadEmpInfoCode(employMasterCodeTextBox.Text.Trim());
            
            if (aEmpGeneralDataTable.Rows.Count > 0)
            {
                empNameTextBox.Text = aEmpGeneralDataTable.Rows[0]["EmpName"].ToString().Trim();
                empInfoIdHiddenField.Value = aEmpGeneralDataTable.Rows[0]["EmpInfoId"].ToString().Trim();
            }
            else
            {
                showMessageBox("No Data Found");
            }
        }
        else
        {
            showMessageBox("Please Input a Employee Code");
        }
    }
    
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("LeaveManagementEntry.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}