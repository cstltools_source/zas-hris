﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="LeaveInventoryView.aspx.cs" Inherits="HRM_UI_LeaveInventoryView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }
    </style>

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Leave Inventory View </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="addImageButton_Click" />
                        <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="reloadLinkButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Daily Tasks </a></li>
                            <li class="breadcrumb-item"><a href="#">Leave Inventory View</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 380px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark" DataKeyNames="LeaveInventoryId"
                                    OnRowCommand="loadGridView_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                        <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                        <asp:BoundField DataField="LeaveName" HeaderText="Leave Name" />
                                        <asp:BoundField DataField="DayQty" HeaderText="Leave Day quantity" />
                                        <asp:BoundField DataField="LeaveYear" HeaderText="Leave Year" />
                                        <%--<asp:BoundField DataField="EntryBy" HeaderText="Entry By" />
                                        <asp:BoundField DataField="EntryDate" DataFormatString="{0:dd-mmm-yyyy}" HeaderText="Entry Date" />--%>

                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="editImageButton" runat="server"
                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditData"
                                                    ImageUrl="~/Assets/img/rsz_edit.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    

    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%" class="TableWorkArea">
                    <tr>
                        <td colspan="6" class="TableHeading">
                            Leave Inventory View</td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp; Add New :</td>
                        <td width="20%" class="TDRight">
                            <asp:ImageButton ID="addImageButton" runat="server" ImageUrl="~/images/Add.png" 
                                onclick="addImageButton_Click" />
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                            Reload :</td>
                        <td width="20%" class="TDRight">
                            <asp:ImageButton ID="reloadLinkButton" runat="server" 
                                ImageUrl="~/images/refresh.png" onclick="reloadLinkButton_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                            <td class="TDLeft" colspan="6">
                                <div id ="container2" style ="height:350px;overflow:auto; " class="divborder">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" 
                                    CssClass="gridview" DataKeyNames="LeaveInventoryId" 
                                    onrowcommand="loadGridView_RowCommand">
                                    <Columns>
                                        <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                        <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                        <asp:BoundField DataField="LeaveName" HeaderText="Leave Name" />
                                        <asp:BoundField DataField="DayQty" HeaderText="Leave Day quantity" />
                                        <asp:BoundField DataField="LeaveYear" HeaderText="Leave Year" />
                                        <asp:BoundField DataField="EntryBy" HeaderText="Entry By" />
                                        <asp:BoundField DataField="EntryDate" DataFormatString="{0:dd-mmm-yyyy}" 
                                            HeaderText="Entry Date" />
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="editImageButton0" runat="server" 
                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditData" 
                                                    ImageUrl="~/images/edit.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                    </Columns>
                                    
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="TDLeft" width="13%">
                                &nbsp;</td>
                            <td class="TDRight" width="20%">
                                &nbsp;</td>
                            <td class="TDLeft" width="13%">
                                &nbsp;</td>
                            <td class="TDRight">
                                &nbsp;</td>
                            <td class="TDLeft" width="13%">
                                &nbsp;</td>
                            <td class="TDRight" width="20%">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="TDLeft" width="13%">
                                &nbsp;
                            </td>
                            <td class="TDRight" width="20%">
                                &nbsp;
                            </td>
                            <td class="TDLeft" width="13%">
                                &nbsp;
                            </td>
                            <td class="TDRight">
                                &nbsp;
                            </td>
                            <td class="TDLeft" width="13%">
                                &nbsp;
                            </td>
                            <td class="TDRight" width="20%">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="TDLeft" width="13%">
                                &nbsp;
                            </td>
                            <td class="TDRight" width="20%">
                                &nbsp;
                            </td>
                            <td class="TDLeft" width="13%">
                                &nbsp;
                            </td>
                            <td class="TDRight">
                                &nbsp;
                            </td>
                            <td class="TDLeft" width="13%">
                                &nbsp;
                            </td>
                            <td class="TDRight" width="20%">
                                &nbsp;
                            </td>
                        </tr>
                        </table>
                        </div>
            </caption>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>

