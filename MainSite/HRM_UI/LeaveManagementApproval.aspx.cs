﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LeaveManagementApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    LeaveAvailBLL aLeaveAvailBLL = new LeaveAvailBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LeaveAvailLoad();
        }
    }

    public void LeaveAvailLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        LeaveAvailLoadByCondition(filename, userName);
        aLeaveAvailBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void LeaveAvailLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aLeaveAvailBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aLeaveAvailBLL.LoadLeaveApproveViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    private void PopUp(string Id)
    {
        string url = "LateAttnApproveEdit.aspx?ID=" + Id;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=700,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string LateApproveId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(LateApproveId);
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");


        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }



    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            try
            {
                SubmitApproval();
                LeaveAvailLoad();
            }
            catch (Exception)
            {

                showMessageBox("choose data properly ");
            }
            
        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
            if (ChkBoxRows.Checked == true)
            {
                return true;
                break;
            }
        }
        showMessageBox("Select Atleast One!!");
        return false;
    }
    private void SubmitApproval()
    {
        List<LeaveAvail> leaveAvailList = new List<LeaveAvail>();
        

        if (actionRadioButtonList.SelectedValue != null)
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    LeaveAvail aLeave = new LeaveAvail()
                    {
                        LeaveAvailId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString()),
                        LeaveName = loadGridView.Rows[i].Cells[4].Text,
                        FromDate = Convert.ToDateTime(loadGridView.Rows[i].Cells[5].Text),
                        ToDate = Convert.ToDateTime(loadGridView.Rows[i].Cells[6].Text),
                        AvailLeaveQty = loadGridView.Rows[i].Cells[7].Text,
                        Status = "Accepted",
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        ApprovedBy = Session["LoginName"].ToString(),
                        ApprovedDate = System.DateTime.Today
                    };

                    aLeaveAvailBLL.ApprovalUpdateBLL(aLeave);
                    leaveAvailList.Add(aLeave);
                }
            }

            if (actionRadioButtonList.SelectedItem.Text == "Accepted")
            {


                if (leaveAvailList.Count() > 0)
                {
                    aLeaveAvailBLL.LeaveUpdateIntoAttRecord(leaveAvailList);

                    
                    for (int i = 0; i < loadGridView.Rows.Count; i++)
                    {
                        if (loadGridView.Rows[i].Cells[4].Text != "LWP" && loadGridView.Rows[i].Cells[4].Text != "Others(LV)")
                        {
                            CheckBox cb = (CheckBox) loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
                            if (cb.Checked == true)
                            {
                                DataTable dt = aLeaveAvailBLL.GetInventory(loadGridView.DataKeys[i][2].ToString());

                                LeaveInventory aLeaveInventory = new LeaveInventory();
                                aLeaveInventory.LeaveInventoryId =
                                    Convert.ToInt32(Convert.ToInt32(loadGridView.DataKeys[i][2].ToString()));
                                aLeaveInventory.DayQty =
                                    (Convert.ToDecimal(dt.Rows[0]["DayQty"].ToString()) -
                                     Convert.ToDecimal(loadGridView.Rows[i].Cells[7].Text)).ToString();

                                aLeaveAvailBLL.UpdateLeaveInventory(aLeaveInventory);
                            }
                        }
                    }
                    showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + " Successfully!!");
                    
                    

                }
            }
            else
            {
                showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + " Successfully!!");
            }
        }
        else
        {
            showMessageBox("Choose One of the Operations ");
        }
    }
}