﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_LeaveManagementEdit : System.Web.UI.Page
{
    LeaveAvailBLL availBll = new LeaveAvailBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LeaveNameLoad();
            leaveAvailIdHiddenField.Value = Request.QueryString["ID"];
            LeaveLoad(leaveAvailIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (leaveNameDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Leave Name !!");
            return false;
        }
        if (empMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Select Employee Code !!");
            return false;
        }
        if (fromDateTextBox.Text == "")
        {
            showMessageBox("Please Select Date !!");
            return false;
        }
        if (toDateTextBox.Text == "")
        {
            showMessageBox("Please Select Date !!");
            return false;
        }
        if (availQtyTextBox.Text == "")
        {
            showMessageBox("Please input Leave Quantity !!");
            return false;
        }
        if (leaveReasonTextBox.Text == "")
        {
            showMessageBox("Please input Leave Reason !!");
            return false;
        }
        if (FromDate() == false)
        {
            showMessageBox("Please give a valid From Date !!!");
            return false;
        }
        if (ToDate() == false)
        {
            showMessageBox("Please give a valid To Date !!!");
            return false;
        }
        return true;
    }
    public bool FromDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(fromDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool ToDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(toDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public void LeaveNameLoad()
    {
        availBll.LoadLeaveName(leaveNameDropDownList);
    }

    protected void updateButton_Click1(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            LeaveAvail avail = new LeaveAvail()
            {
                LeaveAvailId = Convert.ToInt32(leaveAvailIdHiddenField.Value),
                LeaveInventoryId = Convert.ToInt32(inventoryIdHiddenField.Value),
                EmpMasterCode = empMasterCodeTextBox.Text,
                EmpName = empNameTextBox.Text,
                FromDate = Convert.ToDateTime(fromDateTextBox.Text),
                ToDate = Convert.ToDateTime(toDateTextBox.Text),
                AvailLeaveQty = availQtyTextBox.Text,
                LeaveReason = leaveReasonTextBox.Text,
                LeaveName = leaveNameDropDownList.SelectedItem.Text,
                EmpInfoId = Convert.ToInt32(hdEmpInfoId.Value),
            };
            LeaveAvailBLL availBll = new LeaveAvailBLL();

            if (!availBll.UpdateDataForLeaveAvail(avail))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);

    }

    private void LeaveLoad(string leaveId)
    {
        LeaveAvail avail = new LeaveAvail();
        avail = availBll.LeaveAvailEditLoad(leaveId);
        empMasterCodeTextBox.Text = avail.EmpMasterCode;
        empNameTextBox.Text = avail.EmpName;
        leaveNameDropDownList.SelectedValue = avail.LeaveInventoryId.ToString();
        fromDateTextBox.Text = avail.FromDate.ToString("dd-MMM-yyyy");
        toDateTextBox.Text = avail.ToDate.ToString("dd-MMM-yyyy");
        availQtyTextBox.Text = avail.AvailLeaveQty;
        leaveReasonTextBox.Text = avail.LeaveReason;
        hdEmpInfoId.Value = avail.EmpInfoId.ToString();
        LeaveInvId();
    }

    public void LeaveInvId()
    {
        if (leaveNameDropDownList.SelectedValue != "")
        {
            DataTable aDataTableLeave = new DataTable();
            string year = System.DateTime.Today.Year.ToString();
            aDataTableLeave = availBll.LeaveQtyCheck(leaveNameDropDownList.SelectedValue, hdEmpInfoId.Value, year);
            if (aDataTableLeave.Rows.Count > 0)
            {
                MessageQtyLabel.Text = aDataTableLeave.Rows[0]["DayQty"].ToString();
                inventoryIdHiddenField.Value = aDataTableLeave.Rows[0]["LeaveInventoryId"].ToString();
            }
            else
            {
                MessageQtyLabel.Text = "No Data";
            }
        }
        else
        {
            MessageQtyLabel.Text = "";
        }
    }
    protected void empMasterCodeTextBox_TextChanged1(object sender, EventArgs e)
    {
        if (empMasterCodeTextBox.Text != string.Empty)
        {
            List<LeaveInventory> aLeaveInventories = new List<LeaveInventory>();
            LeaveInventoryBLL aLeaveInventoryBll = new LeaveInventoryBLL();
            aLeaveInventories = aLeaveInventoryBll.GetEmployeeNameAndDayQty(empMasterCodeTextBox.Text.Trim());
            if (aLeaveInventories.Count > 0)
            {
                foreach (LeaveInventory aLeaveInventory in aLeaveInventories)
                {
                    empNameTextBox.Text = aLeaveInventory.EmpName;
                    hdEmpInfoId.Value = aLeaveInventory.EmpInfoId.ToString();
                }
                string year = System.DateTime.Today.Year.ToString();
                availBll.LoadLeaveName(leaveNameDropDownList, hdEmpInfoId.Value, year);
            }
            else
            {
                showMessageBox("No Data Found");
            }
        }
        else
        {
            showMessageBox("Please Input a Employee Code");
        }
    }
    protected void calculateButton_Click(object sender, EventArgs e)
    {
        if (fromDateTextBox.Text.Trim() != "" && toDateTextBox.Text.Trim() != "")
        {
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();
            fromDate = Convert.ToDateTime(fromDateTextBox.Text);
            toDate = Convert.ToDateTime(toDateTextBox.Text);
            TimeSpan span = toDate.Subtract(fromDate);
            availQtyTextBox.Text = Convert.ToString(((int)span.TotalDays) + 1);
        }
    }

    protected void leaveNameDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (leaveNameDropDownList.SelectedItem.Text != "LWP")
        {

            if (leaveNameDropDownList.SelectedValue != "")
            {
                DataTable aDataTableLeave = new DataTable();
                string year = Convert.ToDateTime(fromDateTextBox.Text).Year.ToString();
                aDataTableLeave = availBll.LeaveQtyCheck(leaveNameDropDownList.SelectedValue, hdEmpInfoId.Value, year);
                if (aDataTableLeave.Rows.Count > 0)
                {
                    MessageQtyLabel.Text = aDataTableLeave.Rows[0]["DayQty"].ToString();
                    inventoryIdHiddenField.Value = aDataTableLeave.Rows[0]["LeaveInventoryId"].ToString();
                }
                else
                {
                    MessageQtyLabel.Text = "No Data";
                }
            }
            else
            {
                MessageQtyLabel.Text = "";
            }
        }
    }
}