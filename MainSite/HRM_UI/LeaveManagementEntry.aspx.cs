﻿ using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
 using Library.BLL.HRM_BLL;
 using Library.DAO.HRM_Entities;


public partial class HRM_UI_LeaveManagementEntry : System.Web.UI.Page
{
    private LeaveAvailBLL availBll = new LeaveAvailBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //LeaveNameLoad();
        }
    }

    private void Clear()
    {
        empMasterCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        fromDateTextBox.Text = string.Empty;
        toDateTextBox.Text = string.Empty;
        availQtyTextBox.Text = string.Empty;
        leaveNameDropDownList.SelectedValue = null;
        leaveReasonTextBox.Text = string.Empty;
        hdEmpInfoId.Value = string.Empty;
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (leaveNameDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Leave Name !!");
            return false;
        }
        if (empMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Select Employee Code !!");
            return false;
        }
        if (fromDateTextBox.Text == "")
        {
            showMessageBox("Please Select Date !!");
            return false;
        }
        if (toDateTextBox.Text == "")
        {
            showMessageBox("Please Select Date !!");
            return false;
        }
        if (availQtyTextBox.Text == "")
        {
            showMessageBox("Please input Leave Quantity !!");
            return false;
        }
        if (leaveReasonTextBox.Text == "")
        {
            showMessageBox("Please input Leave Reason !!");
            return false;
        }
        if (FromDate() == false)
        {
            showMessageBox("Please give a valid From Date !!!");
            return false;
        }
        if (ToDate() == false)
        {
            showMessageBox("Please give a valid To Date !!!");
            return false;
        }
        return true;
    }
    public bool FromDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(fromDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool ToDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(toDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            if (MessageQtyLabel.Text != "No Data")
            {
                if (leaveNameDropDownList.SelectedItem.Text != "LWP" && leaveNameDropDownList.SelectedItem.Text != "Others(LV)")
                {
                    if (Convert.ToInt32(availQtyTextBox.Text.Trim()) <= Convert.ToInt32(MessageQtyLabel.Text.Trim()))
                    {
                        LeaveAvail avail = new LeaveAvail()
                        {
                            LeaveName = leaveNameDropDownList.SelectedItem.Text,
                            LeaveInventoryId = Convert.ToInt32(inventoryIdHiddenField.Value),
                            EmpMasterCode = empMasterCodeTextBox.Text,
                            EmpInfoId = Convert.ToInt32(hdEmpInfoId.Value),
                            EmpName = empNameTextBox.Text,
                            FromDate = Convert.ToDateTime(fromDateTextBox.Text),
                            ToDate = Convert.ToDateTime(toDateTextBox.Text),
                            AvailLeaveQty = availQtyTextBox.Text,
                            LeaveReason = leaveReasonTextBox.Text,
                            EntryBy = Session["LoginName"].ToString(),
                            EntryDate = System.DateTime.Today,
                            IsActive = true,
                            ActionStatus = "Posted",
                        };
                        LeaveInventory aLeaveInventory = new LeaveInventory();
                        aLeaveInventory.LeaveInventoryId = Convert.ToInt32(inventoryIdHiddenField.Value);
                        aLeaveInventory.DayQty = (Convert.ToDecimal(MessageQtyLabel.Text) - Convert.ToDecimal(availQtyTextBox.Text)).ToString();
                        LeaveAvailBLL availBll = new LeaveAvailBLL();
                        if (availBll.SaveDateForLeaveAvail(avail))
                        {
                            //if (availBll.UpdateLeaveInventory(aLeaveInventory))
                            {
                                showMessageBox("Data Save Successfully");
                                Session["ID"] = avail.LeaveAvailId;
                                Session["ReportParameter"] = " AND dbo.tblEmpGeneralInfo.EmpMasterCode='" + empMasterCodeTextBox.Text + "' ";
                                PopUp(Convert.ToDateTime(fromDateTextBox.Text).Year.ToString(), "", "SN");
                                Clear();
                            }

                        }
                        else
                        {
                            showMessageBox("Leave Already Exist For This Date");
                        }
                    }
                    else
                    {
                        showMessageBox("Leave quantity must be within the range");
                    }
                }
                else
                {

                    LeaveAvail avail = new LeaveAvail()
                    {
                        LeaveName = leaveNameDropDownList.SelectedItem.Text,
                        LeaveInventoryId = Convert.ToInt32("0"),
                        EmpMasterCode = empMasterCodeTextBox.Text,
                        EmpInfoId = Convert.ToInt32(hdEmpInfoId.Value),
                        EmpName = empNameTextBox.Text,
                        FromDate = Convert.ToDateTime(fromDateTextBox.Text),
                        ToDate = Convert.ToDateTime(toDateTextBox.Text),
                        AvailLeaveQty = availQtyTextBox.Text,
                        LeaveReason = leaveReasonTextBox.Text,
                        EntryBy = Session["LoginName"].ToString(),
                        EntryDate = System.DateTime.Today,
                        IsActive = true,
                        ActionStatus = "Posted",
                    };
                    //LeaveInventory aLeaveInventory = new LeaveInventory();
                    //aLeaveInventory.LeaveInventoryId = Convert.ToInt32(inventoryIdHiddenField.Value);
                    //aLeaveInventory.DayQty = (Convert.ToDecimal(MessageQtyLabel.Text) - Convert.ToDecimal(availQtyTextBox.Text)).ToString();
                    LeaveAvailBLL availBll = new LeaveAvailBLL();
                    if (availBll.SaveDateForLeaveAvail(avail))
                    {
                        //if (availBll.UpdateLeaveInventory(aLeaveInventory))
                        //{
                        showMessageBox("Data Save Successfully");
                        Session["ID"] = avail.LeaveAvailId;
                        Session["ReportParameter"] = " AND dbo.tblEmpGeneralInfo.EmpMasterCode='" + empMasterCodeTextBox.Text + "' ";
                        PopUp(Convert.ToDateTime(fromDateTextBox.Text).Year.ToString(), "", "SN");
                        Clear();
                        //}

                    }
                    else
                    {
                        showMessageBox("Leave Already Exist For This Date !!!");
                    }

                }

            }
        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }
    private void PopUp(string year, string empcode, string reportFlag)
    {
        string url = "../Report_UI/LeaveReportViewer.aspx?year=" + year + "&empcode=" + empcode + "&ReportFlag=" + reportFlag;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        try
        {

        
            if (empMasterCodeTextBox.Text != string.Empty)
            {
                List<LeaveInventory> aLeaveInventories = new List<LeaveInventory>();
                LeaveInventoryBLL aLeaveInventoryBll = new LeaveInventoryBLL();
                aLeaveInventories = aLeaveInventoryBll.GetEmployeeNameAndDayQty(empMasterCodeTextBox.Text.Trim());
                if (aLeaveInventories.Count > 0)
                {
                    foreach (LeaveInventory aLeaveInventory in aLeaveInventories)
                    {
                        empNameTextBox.Text = aLeaveInventory.EmpName;
                        hdEmpInfoId.Value = aLeaveInventory.EmpInfoId.ToString();
                    
                    }
                    string year = Convert.ToDateTime(fromDateTextBox.Text).Year.ToString();
                    availBll.LoadLeaveName(leaveNameDropDownList, hdEmpInfoId.Value, year);
                }
                else
                {
                    showMessageBox("No Data Found");
                }
            }
            else
            {
                showMessageBox("Please Input a Employee Code");
            }
        }
        catch (Exception)
        {

            
        }
        leaveNameDropDownList.Items.Add("LWP");
        leaveNameDropDownList.Items.Add("Others(LV)");
    }
   
    protected void calculateButton_Click(object sender, EventArgs e)
    {
        if (fromDateTextBox.Text.Trim()!="" && toDateTextBox.Text.Trim()!="")
        {
            DateTime fromDate = new DateTime();
            DateTime toDate = new DateTime();
            fromDate = Convert.ToDateTime(fromDateTextBox.Text);
            toDate = Convert.ToDateTime(toDateTextBox.Text);
            TimeSpan span = toDate.Subtract(fromDate);
            availQtyTextBox.Text = Convert.ToString(((int)span.TotalDays)+1);

            LeaveInventoryBLL aLeaveInventoryBll=new LeaveInventoryBLL();
            int govholiday = Convert.ToInt32(aLeaveInventoryBll.CheckGovtHoliday(fromDate.ToString("dd-MMM-yyyy"), toDate.ToString("dd-MMM-yyyy"), hdEmpInfoId.Value).Rows[0][0].ToString());
            int weekholiday  = Convert.ToInt32(aLeaveInventoryBll.CheckWeeklyHolidayDays(fromDate.ToString("dd-MMM-yyyy"), toDate.ToString("dd-MMM-yyyy"), hdEmpInfoId.Value).Rows[0][0].ToString());
            availQtyTextBox.Text = (Convert.ToInt32(availQtyTextBox.Text) - govholiday - weekholiday).ToString();
        }
    }
    protected void ViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("LeaveManagementView.aspx");
    }
    protected void leaveNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (leaveNameDropDownList.SelectedItem.Text != "LWP" && leaveNameDropDownList.SelectedItem.Text != "Others(LV)")
        {

            if (leaveNameDropDownList.SelectedValue != "")
            {
                DataTable aDataTableLeave = new DataTable();
                string year = Convert.ToDateTime(fromDateTextBox.Text).Year.ToString();
                aDataTableLeave = availBll.LeaveQtyCheck(leaveNameDropDownList.SelectedValue, hdEmpInfoId.Value, year);
                if (aDataTableLeave.Rows.Count > 0)
                {
                    MessageQtyLabel.Text = aDataTableLeave.Rows[0]["DayQty"].ToString();
                    inventoryIdHiddenField.Value = aDataTableLeave.Rows[0]["LeaveInventoryId"].ToString();
                }
                else
                {
                    MessageQtyLabel.Text = "No Data";
                }
            }
            else
            {
                MessageQtyLabel.Text = "";
            }
        }
        else
        {
            MessageQtyLabel.Text = "";
            inventoryIdHiddenField.Value = null;
        }


    }
    protected void addLeaveLinkButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("LeaveInventoryEntry.aspx");
    }
    protected void ImageButton1_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("LeaveApplicationReport.aspx");
        ;
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}