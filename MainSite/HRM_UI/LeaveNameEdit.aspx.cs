﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LeaveEdit : System.Web.UI.Page
{
    LeaveBLL aLeaveBll = new LeaveBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            leaveIdHiddenField.Value = Request.QueryString["ID"];
            LeaveLoad(leaveIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (leaveNameTextBox.Text != "" )
        {
            Leave aLeave = new Leave()
            {
                LeaveId = Convert.ToInt32(leaveIdHiddenField.Value),
                LeaveName = leaveNameTextBox.Text,
            };
            
            if (!aLeaveBll.UpdateDataForLeave(aLeave))
            {
                showMessageBox("Data Update Successfully !!! Please Reload");
            }
            else
            {
                showMessageBox("Data Not Update !!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void LeaveLoad(string LeaveId)
    {
        Leave aLeave = new Leave();
        aLeave = aLeaveBll.LeaveEditLoad(LeaveId);
        leaveNameTextBox.Text = aLeave.LeaveName;
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}