﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LeaveEntry : System.Web.UI.Page
{
    LeaveBLL aLeaveBll = new LeaveBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        leaveNameTextBox.Text = string.Empty;
    }

    private bool Validation()
    {
        if (leaveNameTextBox.Text == "")
        {
            showMessageBox("Please Input Leae Name!!");
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Leave aLeave = new Leave()
            {
                LeaveName = leaveNameTextBox.Text,
            };
            if (aLeaveBll.SaveDataForLeave(aLeave))
            {
                showMessageBox("Data Save Successfully Leave Code is :" + aLeave.LeaveCode + " And Leave Name is : " + aLeave.LeaveName);
                Clear();
            }
            else
            {
                showMessageBox("Leave Name Already Exists!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("LeaveNameView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}