﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_LeaveReport : System.Web.UI.Page
{
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
            LoadUnit();
        }
    }

    private void LoadUnit()
    {
        aInfoBll.LoadUnitNameToDropDownBLL(ddlUnit, 1.ToString());
    }

    protected void showReportButton_Click(object sender, EventArgs e)
    {
        Session["ReportParameter"] = ParameterGenerator();
        PopUp(yearDropDownList.SelectedItem.Text, EmpMasterCodeTextBox.Text, typeDropDownList.SelectedValue);
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 1; i <= DateTime.Now.Year +3; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));
    }
    private string ParameterGenerator()
    {
        string parameter = "";

        if (typeDropDownList.SelectedValue == "S")
        {
            parameter = " AND dbo.tblEmpGeneralInfo.EmpMasterCode='" + EmpMasterCodeTextBox.Text + "' ";

           
            return parameter;
        }

        if (ddlUnit.SelectedValue != "")
        {
            parameter = " AND tblEmpGeneralInfo.UnitId ='" + ddlUnit.SelectedValue + "' ";
        }

        return parameter;
    }
    
    private void PopUp(string year, string empcode,string reportFlag)
    {
        string url = "../Report_UI/LeaveReportViewer.aspx?year=" + year + "&empcode=" + empcode + "&ReportFlag=" + reportFlag;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=1000,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    protected void typeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (typeDropDownList.SelectedValue=="S")
        {
            divemp.Visible = true;
        }
        if (typeDropDownList.SelectedValue=="A")
        {
            divemp.Visible = false;
        }
    }
}