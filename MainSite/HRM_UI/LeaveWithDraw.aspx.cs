﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LeaveWithDraw : System.Web.UI.Page
{
    LeaveInventoryBLL aLeaveInventoryBll = new LeaveInventoryBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
            currentYear();
            LoadYear();
        }
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 1; i <= DateTime.Now.Year + 3; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));
    }
    public void DropDownList()
    {
        aLeaveInventoryBll.LoadLeaveName(leaveDropDownList);
    }

    public void Clear()
    {
        empCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        leaveDropDownList.SelectedIndex = 0;
        yearDropDownList.SelectedIndex = 0;
        loadGridView.DataSource = null;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void empCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (empCodeTextBox.Text != string.Empty)
        {
            DataTable aEmpGeneralDataTable = new DataTable();
            LeaveInventoryBLL aLeaveInventoryBll = new LeaveInventoryBLL();
            aEmpGeneralDataTable = aLeaveInventoryBll.LoadEmpInfoCode(empCodeTextBox.Text.Trim());

            if (aEmpGeneralDataTable.Rows.Count > 0)
            {
                empNameTextBox.Text = aEmpGeneralDataTable.Rows[0]["EmpName"].ToString().Trim();
                empInfoIdHiddenField.Value = aEmpGeneralDataTable.Rows[0]["EmpInfoId"].ToString().Trim();
            }
            else
            {
                showMessageBox("No Data Found");
            }
        }
        else
        {
            showMessageBox("Please Input a Employee Code");
        }
    }
    public void currentYear()
    {
        for (int i = 0; i < yearDropDownList.Items.Count; i++)
        {
            if (yearDropDownList.Items[i].Text.Trim() == DateTime.Now.Year.ToString())
            {
                yearDropDownList.SelectedIndex = i;
            }
        }
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        string empinfoId = empInfoIdHiddenField.Value;
        DataTable dtleave = aLeaveInventoryBll.LoadLeave(yearDropDownList.SelectedItem.Text,empinfoId,leaveDropDownList.SelectedItem.Text);
        if (dtleave.Rows.Count > 0)
        {
            loadGridView.DataSource = dtleave;
            loadGridView.DataBind();
        }
        else
        {
            showMessageBox("Data Not Found !!");
        }
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        int day = 0;
        
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[2].FindControl("giveCheckBox");

            if (ChkBoxRows.Checked)
            {
                day++;

                DateTime startdt = Convert.ToDateTime(loadGridView.Rows[i].Cells[0].Text);
                DateTime todt = Convert.ToDateTime(loadGridView.Rows[i].Cells[1].Text);

                while (startdt<=todt)
                {
                    Attendence attendence = new Attendence();
                    attendence.ATTDate = Convert.ToDateTime(startdt);
                    attendence.EmpId = Convert.ToInt32(empInfoIdHiddenField.Value);
                    aLeaveInventoryBll.DeleteLeave(attendence);

                    startdt = startdt.AddDays(1);
                }
                aLeaveInventoryBll.DeleteLeaveAvail(loadGridView.DataKeys[i][0].ToString());

                decimal totalqty= 0;
                decimal mainqty = 0;
                decimal remainQty = 0;

                mainqty = Convert.ToDecimal(aLeaveInventoryBll.LeaveInv(empInfoIdHiddenField.Value, yearDropDownList.SelectedItem.Text,
                    leaveDropDownList.SelectedItem.Text).Rows[0]["DayQty"].ToString());
                remainQty = Convert.ToDecimal(loadGridView.DataKeys[i][2].ToString());

                totalqty = mainqty + remainQty;

                aLeaveInventoryBll.UpdateLeave(loadGridView.DataKeys[i][1].ToString(), totalqty.ToString());

                showMessageBox("Leave Withdraw successfully");
                Clear();

            }
               
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}