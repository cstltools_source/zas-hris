﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LineEdit : System.Web.UI.Page
{
    LineBLL aLineBLL = new LineBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lineIdHiddenField.Value = Request.QueryString["ID"];
            LineLoad(lineIdHiddenField.Value);
        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

  
    protected void updateButton_Click(object sender, EventArgs e)
    {

        if (lineNameTextBox.Text != "" )
        {
            
            Line aLine = new Line()
            {
                LineId = Convert.ToInt32(lineIdHiddenField.Value),
                LineName = lineNameTextBox.Text,
                
            };
            LineBLL aLineBLL = new LineBLL();

            if (!aLineBLL.UpdateDataForLine(aLine))
            {
                showMessageBox("Data did'nt Update !!!!! ");
            }
            else
            {
                showMessageBox("Data Update Successfully !! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all text box");
        }
    }
    private void LineLoad(string Lineid)
    {
         Line aLine = new Line();
         aLine = aLineBLL.LineEditLoad(Lineid);
         
         lineNameTextBox.Text = aLine.LineName;
         
    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
        //this.ClientScript.RegisterClientScriptBlock(this.GetType(), "Close", "window.close()", true);
    }
}