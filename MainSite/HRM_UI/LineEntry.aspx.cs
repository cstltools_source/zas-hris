﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_LineEntry : System.Web.UI.Page
{
    LineBLL aLineBLL = new LineBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        } 
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        lineNameTextBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (lineNameTextBox.Text == "")
        {
            showMessageBox("Please Input Line Name!!");
            return false;
        }
        
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            
            Line aLine = new Line()
            {
                LineName = lineNameTextBox.Text
                
            };
            if (aLineBLL.SaveDataForLine(aLine))
            {
                showMessageBox("Data Save Successfully and  Line Name is :   " + aLine.LineName);
                Clear();
                
            }
            else
            {
                showMessageBox("Line Name already exist");
            }
            
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void viewListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("LineView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}