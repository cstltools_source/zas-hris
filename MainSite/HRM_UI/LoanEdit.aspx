﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoanEdit.aspx.cs" Inherits="HRM_UI_LoanEdit" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
    </head>
    <body>
    <form id="form2" runat="server">

        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
            <div class="page-heading">
                <div class="page-heading__container">
                    <div class="icon"><span class="li-register"></span></div>
                    <span></span>
                    <h1 class="title" style="font-size: 18px; padding-top: 9px;">Loan Edit </h1>
                </div>
                <div class="page-heading__container float-right d-none d-sm-block">
                   
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">General Tasks </a></li>
                            <li class="breadcrumb-item"><a href="#">Loan Edit</a></li>

                        </ol>
                    </nav>
            </div>
            <!-- //END PAGE HEADING -->
             <br/>
                    <br/>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Code : </label>
                                        <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm" 
                                        ontextchanged="empCodeTextBox_TextChanged" AutoPostBack="True"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="empCodeTextBox_AutoCompleteExtender" 
                                        runat="server" CompletionListCssClass="autocomplete_completionListElement" 
                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem" 
                                        CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="10" 
                                        DelimiterCharacters="" EnableCaching="true" Enabled="True" 
                                        MinimumPrefixLength="1" ServiceMethod="GetEmployee" 
                                        ServicePath="HRMWebService.asmx" ShowOnlyCurrentWordInCompletionListItem="true" 
                                        TargetControlID="empCodeTextBox" UseContextKey="True">
                                    </asp:AutoCompleteExtender>
                                    </div>
                                </div>
                               <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name : </label>
                                         <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox>
                                    </div>
                                </div>
                              <div class="col-2">
                                <div class="form-group">
                                    <label>Employee Designation : </label>
                                    <asp:TextBox ID="designationTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                  </div>
                            </div>
                           <div class="col-2">
                                <div class="form-group">
                                    <label>Department Name: </label>
                                    <asp:TextBox ID="departmentTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                            
                            <div class="col-2">
                                <div class="form-group">
                                    <label>Loan Type: </label>
                                   <asp:DropDownList ID="loanTypeDropDownList" runat="server" CssClass="form-control form-control-sm">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem>Salary Advance</asp:ListItem>
                                    <asp:ListItem>Provident Fund</asp:ListItem>
                                </asp:DropDownList>
                              </div>
                          </div>
                         </div>
                        <div class="form-row">
                           <div class="col-2">
                                <div class="form-group">
                                    <label>Loan Amount : </label>
                                    <asp:TextBox ID="loanAmountTextBox" runat="server" AutoPostBack="True" 
                                    CssClass="form-control form-control-sm"></asp:TextBox>
                                 </div>
                            </div>
                         <div class="col-2">
                                <div class="form-group">
                                    <label>Total Installment : </label>
                                   <asp:TextBox ID="totalinstallmentTextBox" runat="server" AutoPostBack="True" 
                                  CssClass="form-control form-control-sm" ontextchanged="installmentTextBox1_TextChanged"></asp:TextBox>
                                 </div>
                            </div>
                         <div class="col-2">
                                <div class="form-group">
                                    <label>Installment Amount: </label>
                                    <asp:TextBox ID="installmentAmountTextBox" runat="server" 
                                     CssClass="form-control form-control-sm" AutoPostBack="True"></asp:TextBox>
                               </div>
                           </div>
                           
                           <div class="col-2">
                                <div class="form-group">
                                    <label>Sanction Date: </label>
                                   <asp:TextBox ID="sanctionTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    <asp:CalendarExtender ID="sanctionTextBox1_CalendarExtender" runat="server" 
                                        Format="dd-MMM-yyyy" PopupButtonID="sanimgDate" CssClass="custom" PopupPosition="TopLeft"
                                        TargetControlID="sanctionTextBox" />
                                    <asp:ImageButton ID="sanimgDate" runat="server" 
                                        AlternateText="Click to show calendar" 
                                        ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                 </div>
                            </div>
                         <div class="col-2">
                                <div class="form-group">
                                    <label>Deduction Stat Date: </label>
                                   <asp:TextBox ID="deductDtTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                <asp:CalendarExtender ID="deductDtTextBox_CalendarExtender" runat="server" 
                                    Format="dd-MMM-yyyy" PopupButtonID="deducimgDate" CssClass="custom" PopupPosition="TopLeft"
                                    TargetControlID="deductDtTextBox" />
                                <asp:ImageButton ID="deducimgDate" runat="server" 
                                    AlternateText="Click to show calendar" 
                                     ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                 </div>
                            </div>
                         </div>
                      </div>
                       <div class="form-row">
                          <div class="col-1">
                                <div class="form-group">
                                   <asp:Button ID="generateButton" Text="Generate" CssClass="btn-info" runat="server" OnClick="generateButton_Click" />
                                  </div>
                            </div>
                        </div>
                       </div>
                    <div class="card">
                         <div class="card-body">
                           <div class="form-row">
                            <div class="col-12">
                                <div class="form-group">
                                     <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered text-center thead-dark"> <Columns>
                                     <asp:BoundField DataField="InstallDate" HeaderText="Installment Date" 
                                        DataFormatString="{0:dd-MMM-yyyy}" />
                                    <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                </Columns>
                            </asp:GridView>
                           </div>
                        </div>
                      </div>
                        <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="submitButton" Text="Submit" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 <asp:HiddenField ID="empIdHiddenField" runat="server" />
                <asp:HiddenField ID="desigHiddenField" runat="server" />
                <asp:HiddenField ID="deptHiddenField" runat="server" />
                <asp:HiddenField ID="loanHiddenField" runat="server" />
               

        </div>
       </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>