﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="LoanEntry.aspx.cs" Inherits="HRM_UI_LoanEntry" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Loan Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="gradeImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">General Tasks </a></li>
                            <li class="breadcrumb-item"><a href="#">Loan Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Code : </label>
                                        <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"
                                            OnTextChanged="empCodeTextBox_TextChanged" AutoPostBack="True"></asp:TextBox>
                                        <asp:AutoCompleteExtender ID="empCodeTextBox_AutoCompleteExtender"
                                            runat="server" CompletionListCssClass="autocomplete_completionListElement"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                            CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="10"
                                            DelimiterCharacters="" EnableCaching="true" Enabled="True"
                                            MinimumPrefixLength="1" ServiceMethod="GetEmployee"
                                            ServicePath="HRMWebService.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                            TargetControlID="empCodeTextBox" UseContextKey="True">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name : </label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                            AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Designation : </label>
                                        <asp:TextBox ID="designationTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Department Name: </label>
                                        <asp:TextBox ID="departmentTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Loan Type: </label>
                                        <asp:DropDownList ID="loanTypeDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            <asp:ListItem> Select from list </asp:ListItem>
                                            <asp:ListItem>Salary Advance</asp:ListItem>
                                            <asp:ListItem>Provident Fund</asp:ListItem>
                                            <asp:ListItem>Bike Loan</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Loan Amount : </label>
                                        <asp:TextBox ID="loanAmountTextBox" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Total Installment : </label>
                                        <asp:TextBox ID="totalinstallmentTextBox" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm" OnTextChanged="installmentTextBox1_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Installment Amount: </label>
                                        <asp:TextBox ID="installmentAmountTextBox" runat="server"
                                            CssClass="form-control form-control-sm" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Sanction Date: </label>
                                        <asp:TextBox ID="sanctionTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:CalendarExtender ID="sanctionTextBox1_CalendarExtender" runat="server"
                                            Format="dd-MMM-yyyy" PopupButtonID="sanimgDate" CssClass="custom" PopupPosition="TopLeft"
                                            TargetControlID="sanctionTextBox" />
                                        <asp:ImageButton ID="sanimgDate" runat="server"
                                            AlternateText="Click to show calendar"
                                            ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Deduction Stat Date: </label>
                                        <asp:TextBox ID="deductDtTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:CalendarExtender ID="deductDtTextBox_CalendarExtender" runat="server"
                                            Format="dd-MMM-yyyy" PopupButtonID="deducimgDate" CssClass="custom" PopupPosition="TopLeft"
                                            TargetControlID="deductDtTextBox" />
                                        <asp:ImageButton ID="deducimgDate" runat="server"
                                            AlternateText="Click to show calendar"
                                            ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-1">
                                <div class="form-group">
                                    <asp:Button ID="generateButton" Text="Generate" CssClass="btn-info" runat="server" OnClick="generateButton_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered text-center thead-dark">
                                            <Columns>
                                                <asp:BoundField DataField="InstallDate" HeaderText="Installment Date"
                                                    DataFormatString="{0:dd-MMM-yyyy}" />
                                                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="submitButton" Text="Submit" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="empIdHiddenField" runat="server" />
                <asp:HiddenField ID="desigHiddenField" runat="server" />
                <asp:HiddenField ID="deptHiddenField" runat="server" />

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

