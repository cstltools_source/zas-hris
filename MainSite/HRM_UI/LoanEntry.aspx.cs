﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_LoanEntry : System.Web.UI.Page
{
    LoanBll aLoanBll = new LoanBll();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private void Clear()
    {
        empCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        designationTextBox.Text = string.Empty;
        desigHiddenField.Value = string.Empty;
        deptHiddenField.Value = string.Empty;
        departmentTextBox.Text = string.Empty;
        sanctionTextBox.Text = string.Empty;
        loanAmountTextBox.Text = string.Empty;
        totalinstallmentTextBox.Text = string.Empty;
        installmentAmountTextBox.Text = string.Empty;
        deductDtTextBox.Text = string.Empty;
        loadGridView.DataSource = null;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (empNameTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Name!!");
            return false;
        }
        if (deductDtTextBox.Text == "")
        {
            showMessageBox("Please Input Deduction Start Date Name!!");
            return false;
        }
        if (sanctionTextBox.Text == "")
        {
            showMessageBox("Please Input Sanction Start Date Name!!");
            return false;
        }
        if (installmentAmountTextBox.Text == "")
        {
            showMessageBox("Please Input Installment Amount!!");
            return false;
        }
        if (loanAmountTextBox.Text == "")
        {
            showMessageBox("Please Input Loan Amount!!");
            return false;
        }
        if (totalinstallmentTextBox.Text == "")
        {
            showMessageBox("Please Input Total Installment Number!!");
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            if (loadGridView.Rows.Count > 0)
            {


                List<LoanDetail> aLoanDetailList = new List<LoanDetail>();
                LoanMaster aLoanMaster = new LoanMaster()
                {
                    EmpInfoId = Convert.ToInt32(empIdHiddenField.Value),
                    DesigId = Convert.ToInt32(desigHiddenField.Value),
                    DeptId = Convert.ToInt32(deptHiddenField.Value),
                    LoanAmount = Convert.ToDecimal(loanAmountTextBox.Text),
                    TotalInstallment = Convert.ToInt32(totalinstallmentTextBox.Text),
                    SanctionDate = Convert.ToDateTime(sanctionTextBox.Text),
                    DeductionStartDate = Convert.ToDateTime(deductDtTextBox.Text),
                    InstallAmount = Convert.ToDecimal(installmentAmountTextBox.Text),
                    Status = "Posted",
                    EntryUser = Session["LoginName"].ToString(),
                    EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                    LoanType = loanTypeDropDownList.SelectedItem.Text,
                    IsActive = true

                };


                for (int i = 0; i < loadGridView.Rows.Count; i++)
                {
                    LoanDetail aLoanDetail = new LoanDetail();
                    aLoanDetail.InstallmentDate = Convert.ToDateTime(loadGridView.Rows[i].Cells[0].Text);
                    aLoanDetail.Amount = Convert.ToDecimal(loadGridView.Rows[i].Cells[1].Text);
                    aLoanDetailList.Add(aLoanDetail);
                }

                if (aLoanBll.SaveDataForLoan(aLoanMaster, aLoanDetailList))
                {
                    showMessageBox("Data Save Successfully ");
                    Clear();
                }
                else
                {
                    showMessageBox("Loan Already Exist");
                }
            }
            else
            {
                showMessageBox("Generate Loan !!!!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    protected void gradeImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("LoanView.aspx");
    }
    protected void empCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        string empCode = empCodeTextBox.Text.Trim();
        GetEmployeeInfo(empCode);
    }
    private void GetEmployeeInfo(string empCode)
    {
        DataTable aDataTableEmp = new DataTable();
        if (!string.IsNullOrEmpty(empCode))
        {
            aDataTableEmp = aLoanBll.EmpInformationBll(empCode);
            if (aDataTableEmp.Rows.Count > 0)
            {
                empNameTextBox.Text = aDataTableEmp.Rows[0]["EmpName"].ToString();
                empIdHiddenField.Value = aDataTableEmp.Rows[0]["EmpInfoId"].ToString();
                desigHiddenField.Value = aDataTableEmp.Rows[0]["DepId"].ToString();
                deptHiddenField.Value = aDataTableEmp.Rows[0]["DesigId"].ToString();
                designationTextBox.Text = aDataTableEmp.Rows[0]["DesigName"].ToString();
                departmentTextBox.Text = aDataTableEmp.Rows[0]["DeptName"].ToString();

            }
            else
            {

                showMessageBox("Employee Information Not Found!!");
            }
        }
    }

    protected void installmentTextBox1_TextChanged(object sender, EventArgs e)
    {
        try
        {
            decimal loanAmount = string.IsNullOrEmpty(loanAmountTextBox.Text) ? 0 : Convert.ToDecimal(loanAmountTextBox.Text);
            decimal totalInstall = string.IsNullOrEmpty(totalinstallmentTextBox.Text) ? 0 : Convert.ToDecimal(totalinstallmentTextBox.Text);
            decimal total = 0;
            total = loanAmount / totalInstall;

            installmentAmountTextBox.Text = total.ToString("F");
        }
        catch (Exception)
        {

            showMessageBox("Input Real Values !!!!");
        }


    }
    protected void generateButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            DataTable aDataTable = new DataTable();
            aDataTable.Columns.Add("InstallDate");
            aDataTable.Columns.Add("Amount");
            DataRow dataRow;


            for (int i = 0; i < Convert.ToInt32(totalinstallmentTextBox.Text); i++)
            {
                DateTime maindate = new DateTime();
                DateTime adddate;

                maindate = Convert.ToDateTime(deductDtTextBox.Text);
                adddate = maindate.AddMonths(i);
                decimal balance = Convert.ToDecimal(Convert.ToDecimal(installmentAmountTextBox.Text).ToString("F"));

                dataRow = aDataTable.NewRow();
                dataRow["InstallDate"] = adddate.ToString("dd-MMM-yyyy");
                dataRow["Amount"] = balance;

                aDataTable.Rows.Add(dataRow);


            }

            loadGridView.DataSource = aDataTable;
            loadGridView.DataBind();
        }
    }
}