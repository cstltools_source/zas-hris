﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="LoanReroute.aspx.cs" Inherits="HRM_UI_LoanReroute" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Loan Re Route </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                       <%-- <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="jobViewImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Loan Re Route</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Master Code </label>
                                        <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"
                                            OnTextChanged="empCodeTextBox_TextChanged"
                                            AutoPostBack="True"></asp:TextBox>
                                        <asp:AutoCompleteExtender ID="empCodeTextBox_AutoCompleteExtender"
                                            runat="server" CompletionListCssClass="autocomplete_completionListElement"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                            CompletionListItemCssClass="autocomplete_listItem" CompletionSetCount="10"
                                            DelimiterCharacters="" EnableCaching="true" Enabled="True"
                                            MinimumPrefixLength="1" ServiceMethod="GetEmployee"
                                            ServicePath="HRMWebService.asmx" ShowOnlyCurrentWordInCompletionListItem="true"
                                            TargetControlID="empCodeTextBox"
                                            UseContextKey="True">
                                        </asp:AutoCompleteExtender>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Name </label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"></asp:TextBox>
                                        <asp:HiddenField ID="empIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Loan Amount </label>
                                        <asp:TextBox ID="loanAmountTextBox" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:HiddenField ID="loanIdHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Loan Amount Paid </label>
                                        <asp:TextBox ID="loanAmountPaidTextBox" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:HiddenField ID="dateHiddenField" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Loan Amount Remain </label>
                                        <asp:TextBox ID="loanAmountRemainTextBox" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Total Installment </label>
                                        <asp:TextBox ID="totalInstallmentTextBox" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm" OnTextChanged="totalInstallmentTextBox_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                            <div class="form-row">
                                

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>New Installment </label>
                                        <asp:TextBox ID="newInstallmentamountTextBox" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label>
                                                Start Date
                                            </label>
                                            <div class="input-group date pull-left" id="daterangepicker1">
                                                <asp:TextBox ID="startDtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                    Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                                    TargetControlID="startDtTextBox" />
                                                <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                    <span>
                                                        <asp:ImageButton ID="ImageButton1" runat="server"
                                                            AlternateText="Click to show calendar"
                                                             ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <asp:Button ID="generateButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="generateButton_Click" Text="Generate" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 200px; overflow: auto; width: auto;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="InstallDate" HeaderText="Installment Date"
                                            DataFormatString="{0:dd-MMM-yyyy}" />
                                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-3">
                                    <label>Adjustment Amount </label>
                                     <asp:TextBox ID="adjustamountTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                </div>
                            </div>
                            <br />
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="Button1_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

