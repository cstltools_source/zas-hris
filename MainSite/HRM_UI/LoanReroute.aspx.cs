﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_LoanReroute : System.Web.UI.Page
{
    LoanBll aLoanBll = new LoanBll();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void empCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        string empCode = empCodeTextBox.Text.Trim();
        GetEmployeeInfo(empCode);
        DataTable dtloaninfo = aLoanBll.LoanInform(empIdHiddenField.Value);
        if (dtloaninfo.Rows.Count>0)
        {
            loanIdHiddenField.Value = dtloaninfo.Rows[0]["LoanId"].ToString();
            loanAmountTextBox.Text = dtloaninfo.Rows[0]["LoanAmount"].ToString();
            dateHiddenField.Value = dtloaninfo.Rows[0]["DeductionStartDate"].ToString();
            DataTable dtpaidamount = aLoanBll.PaidLoanInform(empIdHiddenField.Value, dtloaninfo.Rows[0]["DeductionStartDate"].ToString());
            if (dtpaidamount.Rows.Count>0)
            {
                loanAmountPaidTextBox.Text = dtpaidamount.Rows[0][0].ToString();
                loanAmountRemainTextBox.Text = (Convert.ToDecimal(loanAmountTextBox.Text) -
                                                Convert.ToDecimal(loanAmountPaidTextBox.Text)).ToString();
            }
        }
    }

    
    private void GetEmployeeInfo(string empCode)
    {
        DataTable aDataTableEmp = new DataTable();
        if (!string.IsNullOrEmpty(empCode))
        {
            aDataTableEmp = aLoanBll.EmpInformationBll(empCode);
            if (aDataTableEmp.Rows.Count > 0)
            {
                empNameTextBox.Text = aDataTableEmp.Rows[0]["EmpName"].ToString();
                empIdHiddenField.Value = aDataTableEmp.Rows[0]["EmpInfoId"].ToString();
                

            }
            else
            {

                showMessageBox("Employee Information Not Found!!");
            }
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void generateButton_Click(object sender, EventArgs e)
    {
        DataTable aDataTable = new DataTable();
        aDataTable.Columns.Add("InstallDate");
        aDataTable.Columns.Add("Amount");
        DataRow dataRow;


        for (int i = 0; i < Convert.ToInt32(totalInstallmentTextBox.Text); i++)
        {
            DateTime maindate = new DateTime();
            DateTime adddate;

            maindate = Convert.ToDateTime(startDtTextBox.Text);
            adddate = maindate.AddMonths(i);
            decimal balance = Convert.ToDecimal(Convert.ToDecimal(newInstallmentamountTextBox.Text).ToString("F"));

            dataRow = aDataTable.NewRow();
            dataRow["InstallDate"] = adddate.ToString("dd-MMM-yyyy");
            dataRow["Amount"] = balance;

            aDataTable.Rows.Add(dataRow);


        }

        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    public void Clear()
    {
        empNameTextBox.Text = string.Empty;
        empCodeTextBox.Text = string.Empty;
        loanAmountPaidTextBox.Text = string.Empty;
        loanAmountTextBox.Text = string.Empty;
        loanAmountRemainTextBox.Text = string.Empty;
        newInstallmentamountTextBox.Text = string.Empty;
        totalInstallmentTextBox.Text = string.Empty;
        startDtTextBox.Text = string.Empty;
        loadGridView.DataSource = null;
        loadGridView.DataBind();
        adjustamountTextBox.Text = string.Empty;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        List<LoanDetail> aLoanDetailList = new List<LoanDetail>();
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            LoanDetail aLoanDetail = new LoanDetail();
            aLoanDetail.LoanId = Convert.ToInt32(loanIdHiddenField.Value);
            aLoanDetail.InstallmentDate = Convert.ToDateTime(loadGridView.Rows[i].Cells[1].Text);
            aLoanDetail.Amount = Convert.ToDecimal(loadGridView.Rows[i].Cells[2].Text);
            if (i==loadGridView.Rows.Count-1)
            {
                aLoanDetail.Amount = aLoanDetail.Amount + (string.IsNullOrEmpty(adjustamountTextBox.Text)?0:Convert.ToDecimal(adjustamountTextBox.Text));
            }
            aLoanDetailList.Add(aLoanDetail);
        }
        aLoanBll.DeleteNewData(loanIdHiddenField.Value, dateHiddenField.Value, empIdHiddenField.Value);
        if (aLoanBll.SaveDataForLoanDetail(aLoanDetailList))
        {
            showMessageBox("Data Save Successfully ");
            Clear();
        }
    }
    protected void totalInstallmentTextBox_TextChanged(object sender, EventArgs e)
    {
        try
        {
            decimal loanAmount = string.IsNullOrEmpty(loanAmountRemainTextBox.Text) ? 0 : Convert.ToDecimal(loanAmountRemainTextBox.Text);
            decimal totalInstall = string.IsNullOrEmpty(totalInstallmentTextBox.Text) ? 0 : Convert.ToDecimal(totalInstallmentTextBox.Text);
            decimal total = 0;
            total = loanAmount / totalInstall;

            newInstallmentamountTextBox.Text = total.ToString("F");
        }
        catch (Exception)
        {

            showMessageBox("Input Real Values !!!!");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}