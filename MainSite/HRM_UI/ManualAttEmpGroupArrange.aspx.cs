﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ManualAttEmpGroupArrange : System.Web.UI.Page
{
    ManualAttEmpGroupArrangeBLL aGroupBll = new ManualAttEmpGroupArrangeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
        }
    }
    private void LoadDropDown()
    {
        aGroupBll.LoadGroup(groupDropDownList);
        aGroupBll.LoadDivisionName(divisionDropDownList);
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");


        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
    }
    private void DepartmentWiseLoad()
    {
        DataTable aDataTable = new DataTable();
        aDataTable = aGroupBll.LoadDepartmentWiseEmp(deptDropDownList.SelectedValue);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    private void Clear()
    {
        loadGridView.DataSource = null;
        loadGridView.DataBind();
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        List<Group> aGroupList=new List<Group>();
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[3].FindControl("chkSelect");
            if (ChkBoxRows.Checked==true)
            {
                Group aGroup = new Group();
                aGroup.EmpId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString());
                aGroup.GroupId = Convert.ToInt32(groupDropDownList.SelectedValue);
                aGroupList.Add(aGroup);    
            }
            
        }
        if (aGroupBll.SaveDataForGroup(aGroupList))
        {
            showMessageBox("Data Save successfully");
            Clear();
        }
    }
    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aGroupBll.LoadDepartmentName(deptDropDownList,divisionDropDownList.SelectedValue);
    }
    protected void deptDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
     DepartmentWiseLoad();   
    }
    protected void jobViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("EmpAttGroupView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}