﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="ManualAttGroupWiseEntry.aspx.cs" Inherits="HRM_UI_ManualAttGroupWiseEntry" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Manual Attandance Group Wise</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                       <%-- <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="CompanyListImageButton_Click" />
                        <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Manual Attandance Group Wise</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Group</label>
                                        <asp:DropDownList ID="groupDropDownList" runat="server" CssClass="form-control form-control-sm">
                                           </asp:DropDownList>
                                    </div>
                                </div>
                                
                                
                            </div>
                            <div class="form-row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>From Date</label>
                                        <asp:TextBox ID="fromDtTextBox" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" 
                                        ></asp:TextBox> <asp:CalendarExtender ID="hWorkTextBox0_CalendarExtender" runat="server" 
                                        Format="dd-MMM-yyyy" PopupButtonID="imgOnDDate" CssClass="custom" PopupPosition="TopLeft"
                                             TargetControlID="fromDtTextBox" />
                                    <asp:ImageButton ID="imgOnDDate" runat="server" AlternateText="Click to show calendar" 
                                        ImageUrl="~/Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <asp:TextBox ID="toDtTextBox" runat="server" CssClass="form-control form-control-sm"
                                        AutoPostBack="True" ></asp:TextBox>
                                    <asp:ImageButton ID="imgalter" runat="server" AlternateText="Click to show calendar" 
                                        ImageUrl="~/Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                    <asp:CalendarExtender ID="toDtTextBox_CalendarExtender" runat="server" 
                                        Format="dd-MMM-yyyy" PopupButtonID="imgalter" CssClass="custom" PopupPosition="TopLeft"
                                         TargetControlID="toDtTextBox" />
                                    </div>
                                </div>
                                
                            </div>
                            <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

