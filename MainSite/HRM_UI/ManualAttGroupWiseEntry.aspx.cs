﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ManualAttGroupWiseEntry : System.Web.UI.Page
{
    ManualAttGroupWiseEntryBLL aShiftWiseGroupBll = new ManualAttGroupWiseEntryBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        fromDtTextBox.Text = string.Empty;
        toDtTextBox.Text = string.Empty;
        groupDropDownList.SelectedValue = null;
    }
    private void LoadDropDown()
    {
        aShiftWiseGroupBll.LoadGroup(groupDropDownList);
    }

    public bool FromDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(fromDtTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool ToDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(toDtTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (fromDtTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }

        if (FromDate() == false)
        {
            showMessageBox("Please give a valid From Date !!!");
            return false;
        }
        if (ToDate() == false)
        {
            showMessageBox("Please give a valid To Date !!!");
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {

            ShiftWiseGroup aShiftWiseGroup = new ShiftWiseGroup()
            {
                GroupId = Convert.ToInt32(groupDropDownList.SelectedValue),
                FromDate = Convert.ToDateTime(fromDtTextBox.Text),
                ToDate = Convert.ToDateTime(toDtTextBox.Text),
                Status = "Posted",
                EntryUser = Session["LoginName"].ToString(),
                EntryDate = System.DateTime.Today,
                IsActive = true,
            };
            DataTable dtDate = aShiftWiseGroupBll.LoadDate(groupDropDownList.SelectedValue, fromDtTextBox.Text,
                                                           toDtTextBox.Text);
            if (dtDate.Rows.Count <= 0)
            {
                if (aShiftWiseGroupBll.SaveShiftWiseGroupData(aShiftWiseGroup))
                {
                    DataTable dtEmp = new DataTable();
                    dtEmp = aShiftWiseGroupBll.LoadEmpID(Convert.ToInt32(groupDropDownList.SelectedValue));
                    foreach (DataRow dtRow in dtEmp.Rows)
                    {
                        ManualAttendence attendence = new ManualAttendence()
                        {
                            EmpInfoId = Convert.ToInt32(dtRow["EmpId"].ToString()),
                            FromDate = Convert.ToDateTime(fromDtTextBox.Text),
                            ToDate = Convert.ToDateTime(toDtTextBox.Text),
                            ShiftId = 1,
                            ShiftInTime = "09:00:00",
                            ShiftOutTime = "19:30:00",
                            EntryReason = "Group Wise Manual Attandance",
                            ATTStatus = "P",
                            EntryBy = Session["LoginName"].ToString(),
                            EntryDate = System.DateTime.Today,
                            ActionStatus = "Posted",
                            DayName = "NO",
                            IsActive = true,
                            OverTimeDuration = "00:00:00"
                        };
                        if (aShiftWiseGroupBll.SaveDataForManualAttendence(attendence))
                        {
                        }
                    }
                    Clear();
                    showMessageBox("Data Save Successfully");
                }
            }
            else
            {
                showMessageBox("Attandance Already Given Between this Date");
            }
        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    protected void jobViewImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("ShiftWiseGroupView.aspx");
    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}