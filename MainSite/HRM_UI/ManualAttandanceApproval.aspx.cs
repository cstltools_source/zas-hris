﻿using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ManualAttandanceApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    ManualAttendenceBLL aManualAttendanceApproveBLL = new ManualAttendenceBLL();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ManualAttendanceApproveLoad();
        }
    }
    public void ManualAttendanceApproveLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        ManualAttendanceLoadByCondition(filename, userName);
        aManualAttendanceApproveBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void ManualAttendanceLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aManualAttendanceApproveBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aManualAttendanceApproveBLL.LoadManualAttendanceViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    private void PopUp(string Id)
    {
        string url = "ManualAttendanceAttnApproveEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ManualAttendanceApproveId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(ManualAttendanceApproveId);
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }  
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            try
            {
                SubmitApproval();
                ManualAttendanceApproveLoad();
            }
            catch (Exception)
            {

                showMessageBox("choose data properly ");
            }
            
        }
    }
    private bool Validation()
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
            if (ChkBoxRows.Checked == true)
            {
                return true;
               
            }
        }
        showMessageBox("Select Atleast One!!");
        return false;
    }
    public string DutyDuration(string ottime)
    {
        if (!string.IsNullOrEmpty(ottime))
        {
            if (ottime.Contains(':'.ToString()))
            {
                string[] hour = ottime.Split(':');
                int outhour = Convert.ToInt32(hour[0]);
                int othour = 8;
                int total;
                total = outhour + othour;
                string mainouttime = total.ToString() + ":" + hour[1] + ":" + hour[2];
                ottime = mainouttime;
            }

        }
        return ottime;
    }
    public string DutyDurationHour(string ottime)
    {
        if (!string.IsNullOrEmpty(ottime))
        {
            if (ottime.Contains(':'.ToString()))
            {
                string[] hour = ottime.Split(':');
                int outhour = Convert.ToInt32(hour[0]);
                int othour = 8;
                int total;
                total = outhour + othour;
                ottime = total.ToString();
            }

        }
        return ottime;
    }
    public int OTDurationHour(string ottime)
    {
        if (!string.IsNullOrEmpty(ottime))
        {
            if (ottime.Contains(':'.ToString()))
            {
                string[] hour = ottime.Split(':');
                
                ottime = hour[0].ToString();
            }

        }
        return Convert.ToInt32(ottime);
    }
    public string ComOutTimeHour(string outtime,string shifttime)
    {
        string comtime = null;
        string[] outtime1 = outtime.Split(':');
        string[] shifttime1 = shifttime.Split(':');

            
        int total;
        total = Convert.ToInt32(outtime1[0]) - Convert.ToInt32(shifttime1[0]);

        if (total>2)
        {
            comtime = (Convert.ToInt32(shifttime1[0]) + 2) + ":" + "00:00";
        }
        else
        {
            comtime = outtime;
        }

        return comtime;
    }
    private void SubmitApproval()
    {
        if (actionRadioButtonList.SelectedValue == null)
        {
            showMessageBox("Choose One of the Operations ");
        }
        else
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {

                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    ManualAttendence aManualAttendanceApprove = new ManualAttendence()
                    {
                        MAttendenceId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        Approveby = Session["LoginName"].ToString(),
                        ApproveDate = System.DateTime.Today,
                    };

                    if (aManualAttendanceApproveBLL.ApprovalUpdateBLL(aManualAttendanceApprove))
                    {
                        if (aManualAttendanceApprove.ActionStatus == "Accepted")
                        {
                            DataTable dt = new DataTable();
                       
                            dt = aManualAttendanceApproveBLL.LoadManualAttData(aManualAttendanceApprove.MAttendenceId);
                          
                            if (dt.Rows.Count > 0)
                            {
                                Attendence attendence = new Attendence();
                                attendence.EmpId = Convert.ToInt32(dt.Rows[0]["EmpInfoId"].ToString());
                                attendence.Date = (dt.Rows[0]["ATTDate"].ToString());
                                attendence.ATTDate = Convert.ToDateTime((dt.Rows[0]["ATTDate"].ToString()));
                                attendence.DayName = dt.Rows[0]["DayName"].ToString();
                                attendence.ShiftId = dt.Rows[0]["ShiftId"].ToString();
                                attendence.ShiftStart = Convert.ToDateTime(dt.Rows[0]["ShiftInTime"].ToString()).TimeOfDay;
                                attendence.ShiftEnd = Convert.ToDateTime(dt.Rows[0]["ShiftOutTime"].ToString()).TimeOfDay;
                                if (dt.Rows[0]["ATTStatus"].ToString()=="A")
                                {
                                    attendence.InTime = Convert.ToDateTime("00:00:00").TimeOfDay;
                                    attendence.OutTime = Convert.ToDateTime("00:00:00").TimeOfDay;
                                }
                                else
                                {
                                    attendence.InTime = Convert.ToDateTime(dt.Rows[0]["InTime"].ToString()).TimeOfDay;
                                    attendence.OutTime = Convert.ToDateTime(dt.Rows[0]["OutTime"].ToString()).TimeOfDay;
                                }
                               
                                attendence.OTDuration =
                                    Convert.ToDateTime(dt.Rows[0]["OverTimeDuration"].ToString()).TimeOfDay;

                                if (Convert.ToDateTime(dt.Rows[0]["OverTimeDuration"].ToString()).TimeOfDay > Convert.ToDateTime("02:00:00").TimeOfDay)
                                {
                                    attendence.ComOTDuration = Convert.ToDateTime("02:00:00").TimeOfDay;
                                    attendence.ComOutTime = Convert.ToDateTime(dt.Rows[0]["ShiftOutTime"].ToString()).TimeOfDay + Convert.ToDateTime("02:00:00").TimeOfDay;
                                }
                                else
                                {
                                    attendence.ComOTDuration =
                                         Convert.ToDateTime(dt.Rows[0]["OverTimeDuration"].ToString()).TimeOfDay;
                                    attendence.ComOutTime =
                                        attendence.OutTime;
                                }

                                attendence.DutyDuration = attendence.OutTime - attendence.InTime;
                                attendence.ComDutyDuration = attendence.ComOutTime - attendence.InTime;
                                attendence.ATTStatus = dt.Rows[0]["ATTStatus"].ToString();

                                if (attendence.ATTStatus == "P")
                                {
                                    attendence.Remarks = "Present,ME";
                                }
                                if (attendence.ATTStatus == "A")
                                {
                                    attendence.Remarks = "Absent,ME";
                                }

                                if (attendence.ATTStatus == "L")
                                {
                                    attendence.Remarks = "Late,ME";
                                }

                                DataTable dtAttendanceRecord = aManualAttendanceApproveBLL.LoadAttendanceRecordData(attendence.EmpId, attendence.Date);

                                if (dtAttendanceRecord.Rows.Count > 0)
                                {
                                    string sts = dtAttendanceRecord.Rows[0]["ATTStatus"].ToString();
                                    if (sts == "LV")
                                    {
                                        attendence.ATTStatus = "LV";
                                        attendence.Remarks = dtAttendanceRecord.Rows[0]["Remarks"].ToString();
                                    }
                                    if (sts == "GV")
                                    {
                                        attendence.ATTStatus = "GV";
                                        attendence.Remarks = dtAttendanceRecord.Rows[0]["Remarks"].ToString();
                                    }
                                    if (sts == "WH")
                                    {
                                        attendence.ATTStatus = "WH";
                                        attendence.Remarks = dtAttendanceRecord.Rows[0]["Remarks"].ToString();
                                    }
                                }

                                aManualAttendanceApproveBLL.DeleteData(attendence);

                                if (aManualAttendanceApproveBLL.ApprovalUpdateBLL(attendence))
                                {
                                    showMessageBox("Your Approval Successfully Done !!!");
                                }
                            }
                        }
                        else
                        {
                           
                        }
                    }
                }
            }

            showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + " Successfully");
        }
        
    }
}