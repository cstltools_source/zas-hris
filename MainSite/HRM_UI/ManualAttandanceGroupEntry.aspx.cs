﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ManualAttandanceGroupEntry : System.Web.UI.Page
{
    ManualGroupBLL aEmpGroupBll = new ManualGroupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        empGroupTexBox.Text = string.Empty;

    }
    private bool Validation()
    {

        if (empGroupTexBox.Text == "")
        {
            showMessageBox("Please Input  Employee Group!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmpGroup aEmpGroup = new EmpGroup()
            {
                GroupName = empGroupTexBox.Text,

            };
            if (aEmpGroupBll.SaveDataForEmpGroup(aEmpGroup))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox("EmpGroup Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("EmpGroupView.aspx");
    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}