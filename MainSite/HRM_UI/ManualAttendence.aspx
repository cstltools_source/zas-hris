﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="ManualAttendence.aspx.cs" Inherits="HRM_UI_ManualAttendence" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="cc1" Namespace="MKB.TimePicker" Assembly="TimePicker, Version=1.0.0.0, Culture=neutral, PublicKeyToken=d25e9f59e49c4d2f" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Manual Attendance</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Manual Attendance Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Employee Code</label>
                                        <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm" 
                                         ontextchanged="empCodeTextBox_TextChanged" AutoPostBack="True"></asp:TextBox>
                                         <asp:AutoCompleteExtender ID="empCodeTextBox_AutoCompleteExtender" runat="server"
                                         DelimiterCharacters="" EnableCaching="true"
                                        Enabled="True" MinimumPrefixLength="1" CompletionSetCount="10"
                                        ServiceMethod="GetEmployee" ServicePath="HRMWebService.asmx"  TargetControlID="empCodeTextBox" 
                                        UseContextKey="True"
                                        CompletionListCssClass="autocomplete_completionListElement" 
                                        CompletionListItemCssClass="autocomplete_listItem" 
                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                        ShowOnlyCurrentWordInCompletionListItem="true"
                                        >
                                    </asp:AutoCompleteExtender>
                                   </div>
                               </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name</label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-2">
                                    <div class="form-group">
                                        <label>Attendance From Date</label>
                                        <asp:TextBox ID="attDateTextBox" runat="server" CssClass="form-control form-control-sm" 
                                        AutoPostBack="True" ontextchanged="attDateTextBox_TextChanged"></asp:TextBox>
                                        <asp:ImageButton ID="imgDate" runat="server" 
                                        AlternateText="Click to show calendar" 
                                        ImageUrl="~/Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                        <asp:CalendarExtender ID="attDate" runat="server" Format="dd-MMM-yyyy" TargetControlID="attDateTextBox"
                                       PopupButtonID="imgDate" CssClass="custom">
                                   </asp:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Attendance To Date</label>
                                        <asp:TextBox ID="attToDateTextBox" runat="server" AutoPostBack="True" 
                                         CssClass="form-control form-control-sm" ontextchanged="attDateTextBox_TextChanged"></asp:TextBox>
                                         <asp:CalendarExtender ID="attToDateTextBox_CalendarExtender" runat="server" 
                                         Format="dd-MMM-yyyy" PopupButtonID="imgtDate" TargetControlID="attToDateTextBox" CssClass="custom">
                                         </asp:CalendarExtender>
                                         <asp:ImageButton ID="imgtDate" runat="server" 
                                         AlternateText="Click to show calendar" 
                                         ImageUrl="~/Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                    </div>
                                 </div>
                              </div>
                          
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Attendance Status</label>
                                        <asp:DropDownList ID="attStatDropDownList" runat="server" AutoPostBack="True" 
                                            CssClass="form-control form-control-sm">
                                            <asp:ListItem Value="1">Select any one</asp:ListItem>
                                            <asp:ListItem Value="P">Present</asp:ListItem>
                                            <asp:ListItem Value="A">Absent</asp:ListItem>
                                            <asp:ListItem Value="L">Late</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Shift</label>
                                        <asp:DropDownList ID="shiftDropDownList" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" 
                                             onselectedindexchanged="shiftDropDownList_SelectedIndexChanged">
                                         </asp:DropDownList>
                                    </div>
                                </div>
                                </div>
                               <div id="shiftin" runat="server" >
                               <div class="form-row">
                              <div class="col-2">
                                    <div class="form-group">
                                        <label>Shift In Time</label>
                                        <asp:TextBox ID="shiftInTimeTextBox" runat="server" CssClass="form-control form-control-sm" 
                                        ontextchanged="empCodeTextBox_TextChanged" BackColor="#99FFCC" 
                                        ReadOnly="True"></asp:TextBox>
                                    </div>
                                </div>
                               <div class="col-2">
                                    <div class="form-group">
                                        <label>Shift Out Time</label>
                                        <asp:TextBox ID="shiftOutTimeTextBox" runat="server" CssClass="form-control form-control-sm" 
                                        BackColor="#99FFCC" ReadOnly="True" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                           </div>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>In Time</label>
                                        <cc1:TimeSelector ID="InTimeSelector" runat="server" AllowSecondEditing="true" 
                                            DisplayButtons="False" SelectedTimeFormat="TwentyFour" Font-Bold="True" 
                                            MinuteIncrement="5" BackColor="#66FF33" BorderColor="#660033">
                                        </cc1:TimeSelector>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Out Time</label>
                                        <cc1:TimeSelector ID="OutTimeSelector" runat="server" AllowSecondEditing="true" 
                                        DisplayButtons="False" SelectedTimeFormat="TwentyFour" Font-Bold="True" 
                                        MinuteIncrement="5" BackColor="#66FF33" BorderColor="#660033">
                                        </cc1:TimeSelector>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="form-row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Entry Reason</label>
                                        <asp:TextBox ID="txtReason" runat="server" CssClass="form-control form-control-sm" ></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row" runat="server" Visible="false">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>OT Give</label>
                                        <asp:CheckBox ID="otgiveCheckBox" runat="server" AutoPostBack="True" 
                                         oncheckedchanged="otgiveCheckBox_CheckedChanged" />
                                    </div>
                                </div>
                                <div class="col-2" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                         <asp:Button ID="calculateButton" runat="server" onclick="calculateButton_Click" 
                                         Text="Calculate OT" BackColor="#660066" Font-Bold="True" ForeColor="White" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row" id="otid" runat="server" Visible="False">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Hour :</label>
                                        <asp:TextBox ID="otTextBox" runat="server" CssClass="form-control form-control-sm" 
                                      AutoPostBack="True" ontextchanged="otTextBox_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Min :</label>
                                        <asp:TextBox ID="otMinTextBox" runat="server" AutoPostBack="True" 
                                        CssClass="form-control form-control-sm" ontextchanged="otTextBox_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Morning OT</label>
                                        <asp:CheckBox ID="morningOtCheckBox" runat="server" AutoPostBack="True" />
                                    </div>
                                </div>
                            </div>
                          <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="empidHiddenField" runat="server" />    
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>




</asp:Content>

