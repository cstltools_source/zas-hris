﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ManualAttendence : System.Web.UI.Page
{
    private ManualAttendenceBLL attendenceBll = new ManualAttendenceBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                Data();
                SetTime("00:00:00", "00:00:00");
            }
            catch (Exception)
            {


            }
            //DropDownList();

        }
    }
    private void Clear()
    {
        otgiveCheckBox.Checked = false;
        empCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        attDateTextBox.Text = string.Empty;
        attToDateTextBox.Text = string.Empty;
        attStatDropDownList.SelectedValue = null;
        shiftDropDownList.SelectedValue = null;
        shiftInTimeTextBox.Text = string.Empty;
        shiftOutTimeTextBox.Text = string.Empty;
        txtReason.Text = string.Empty;
        empidHiddenField.Value = null;
        otTextBox.Text = string.Empty;
        otid.Visible = false;
        otMinTextBox.Text = "00";
        otTextBox.Text = "00";

    }
    public void Data()
    {
        otMinTextBox.Text = "00";
        otTextBox.Text = "00";
    }
    private bool Validation()
    {
        if (empCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (attDateTextBox.Text == "")
        {
            showMessageBox("Please Input Attendence Date !!");
            return false;
        }
        if (attToDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (shiftDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Shift !!");
            return false;
        }
        if (attFromDate() == false)
        {
            showMessageBox("Please give a valid Attendance From Date !!!");
            return false;
        }
        if (attToDate() == false)
        {
            showMessageBox("Please give a valid Attendance To Date !!!");
            return false;
        }
        //DateTime dtStart = Convert.ToDateTime(attDateTextBox.Text);
        //DateTime dtEnd = Convert.ToDateTime(attToDateTextBox.Text);
        //while (dtStart <= dtEnd)
        //{
        //    if (dtStart.DayOfWeek.ToString()=="Thursday" && shiftDropDownList.SelectedValue !="4")
        //    {
        //        showMessageBox("Please Input Valid Date as per System Requirement");
        //        return false;

        //    }
        //    if (dtStart.DayOfWeek.ToString() != "Thursday" && shiftDropDownList.SelectedValue == "4")
        //    {
        //        showMessageBox("Please Input Valid Date as per System Requirement");
        //        return false;

        //    }
        //    dtStart = dtStart.AddDays(1);
        //}
        return true;
    }
    public bool attFromDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(attDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool attToDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(attToDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    public void DropDownList()
    {
        DataTable dtShiftChk = attendenceBll.ChkShiftData(empidHiddenField.Value, attDateTextBox.Text);
        if (dtShiftChk.Rows.Count <= 0)
        {
            attendenceBll.LoadShiftName(shiftDropDownList, empidHiddenField.Value);
            shiftDropDownList.SelectedValue = 1.ToString(CultureInfo.InvariantCulture);
            shiftDropDownList_SelectedIndexChanged(null,null);
        }
        else
        {
            attendenceBll.LoadShiftNameGroupWise(shiftDropDownList, empidHiddenField.Value, attDateTextBox.Text);
        }
    }
    public string OTHour(string hour)
    {
        string othour = null;
        if (hour.Contains("."))
        {
            string[] split = hour.Split('.');

            //if (hour.Length == 1)
            //{
            if (split[1].Length == 2)
            {
                othour = split[0] + ":" + split[1] + ":00";
            }
            //if (split[1].Length == 1)
            //{
            //    othour = "0" + hour + ":" + "0" + split[1] + ":00";
            //}

            //}
            //if (hour.Length == 2)
            //{
            //    if (split[1].Length == 2)
            //    {
            //        othour = hour + ":" + split[1] + ":00";
            //    }
            //    if (split[1].Length == 1)
            //    {
            //        othour = hour + ":" + "0" + split[1] + ":00";
            //    }

            //}
        }
        else
        {

            if (hour.Length == 1)
            {
                othour = "0" + hour + ":00:00";

            }
            if (hour.Length == 2)
            {

                othour = hour + ":00:00";

            }
        }

        return othour;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        DateTime intime = DateTime.Parse(string.Format("{0}:{1}:{2}", InTimeSelector.Hour, InTimeSelector.Minute, InTimeSelector.Second));
        DateTime outtime = DateTime.Parse(string.Format("{0}:{1}:{2}", OutTimeSelector.Hour, OutTimeSelector.Minute, OutTimeSelector.Second));
        if (Validation())
        {

            ManualAttendence attendence = new ManualAttendence()
            {
                EmpInfoId = Convert.ToInt32(empidHiddenField.Value),
                ATTDate = Convert.ToDateTime(attDateTextBox.Text),
                ShiftId = Convert.ToInt32(shiftDropDownList.SelectedValue),
                EntryReason = txtReason.Text,
                ATTStatus = attStatDropDownList.SelectedValue,
                EntryBy = Session["LoginName"].ToString(),
                EntryDate = System.DateTime.Today,
                DayName = DateTime.Now.DayOfWeek.ToString(),
                OverTimeDuration = OTHour(otTextBox.Text),
                ToDate = Convert.ToDateTime(attToDateTextBox.Text),
                //StateStatus = "Entered",
                ActionStatus = "Posted"

            };

            attendence.ShiftInTime = intime.ToString("HH:mm:ss");
            attendence.ShiftOutTime = outtime.ToString("HH:mm:ss");

            if (otgiveCheckBox.Checked)
            {
                attendence.OverTimeDuration = (otTextBox.Text) + ":" + otMinTextBox.Text + ":" + "00";

            }
            else
            {
                attendence.OverTimeDuration = "00:00:00";
            }

            ManualAttendenceBLL attendenceBll = new ManualAttendenceBLL();
            if (attendenceBll.SaveDataForManualAttendence(attendence))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(),
                   "alert",
                   "alert('Data Saved Successfully...');window.location ='ManualAttendence.aspx';",
                   true);
                Clear();
                SetTime("00:00:00", "00:00:00");
            }
        }

    }

    protected void empCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = attendenceBll.Empcode(empCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empidHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();

            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    public void SetTime(string intime, string outtime)
    {
        try
        {
            string[] intimedivide = intime.Split(':');
            string[] outtimedivide = outtime.Split(':');

            InTimeSelector.Hour = Convert.ToInt32(intimedivide[0]);
            InTimeSelector.Minute = Convert.ToInt32(intimedivide[1]);
            InTimeSelector.Second = Convert.ToInt32(intimedivide[2]);

            OutTimeSelector.Hour = Convert.ToInt32(outtimedivide[0]);
            OutTimeSelector.Minute = Convert.ToInt32(outtimedivide[1]);
            OutTimeSelector.Second = Convert.ToInt32(outtimedivide[2]);
        }
        catch (Exception)
        {


        }

    }

    protected void shiftDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(shiftDropDownList.SelectedValue.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = attendenceBll.Shift(shiftDropDownList.SelectedValue);

            if (aTable.Rows.Count > 0)
            {
                shiftInTimeTextBox.Text = aTable.Rows[0]["ShiftInTime"].ToString().Trim();
                shiftOutTimeTextBox.Text = aTable.Rows[0]["ShiftOutTime"].ToString().Trim();
                if (shiftDropDownList.SelectedValue == "6")
                {
                    shiftin.Visible = false;
                }
                else
                {
                    shiftin.Visible = true;
                }
                SetTime(shiftInTimeTextBox.Text, shiftOutTimeTextBox.Text);
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Shift");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManualAttendenceView.aspx");

    }
    protected void attDateTextBox_TextChanged(object sender, EventArgs e)
    {
        DropDownList();
    }


    protected void otgiveCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (otgiveCheckBox.Checked == true)
        {
            otid.Visible = true;
        }
        else
        {
            otid.Visible = false;
        }
    }

    protected void otTextBox_TextChanged(object sender, EventArgs e)
    {
        string outTime = shiftOutTimeTextBox.Text;
        if (!string.IsNullOrEmpty(outTime))
        {
            if (outTime.Contains(':'.ToString()))
            {
                string mainouttime = null;
                string[] hour = outTime.Split(':');
                decimal outhour = Convert.ToInt32(hour[0]);
                decimal othour = Convert.ToDecimal(otTextBox.Text);
                decimal total;
                total = outhour + othour;
                if (total > 23)
                {
                    showMessageBox("Out Time Must be within 23 hour!!");
                }
                else
                {
                    if (otTextBox.Text.Contains("."))
                    {
                        string[] maintotal = new string[] { };
                        string[] split;
                        if (total.ToString().Contains("."))
                        {
                            maintotal = total.ToString().Split('.');
                            split = otTextBox.Text.Split('.');
                            mainouttime = maintotal[0] + ":" + (Convert.ToInt32(split[1]) + Convert.ToInt32(hour[1])) + ":" + hour[2];
                        }

                        split = otTextBox.Text.Split('.');
                        mainouttime = maintotal[0] + ":" + (Convert.ToInt32(split[1]) + Convert.ToInt32(hour[1])) + ":" + hour[2];
                    }
                    else
                    {
                        mainouttime = total.ToString() + ":" + hour[1] + ":" + hour[2];
                    }

                    shiftOutTimeTextBox.Text = mainouttime;
                }

            }

        }
    }
    protected void calculateButton_Click(object sender, EventArgs e)
    {
        TimeSpan intime = TimeSpan.Parse(string.Format("{0}:{1}:{2}", InTimeSelector.Hour, InTimeSelector.Minute, InTimeSelector.Second));
        TimeSpan outtime = TimeSpan.Parse(string.Format("{0}:{1}:{2}", OutTimeSelector.Hour, OutTimeSelector.Minute, OutTimeSelector.Second));
        TimeSpan shiftin = Convert.ToDateTime(shiftInTimeTextBox.Text).TimeOfDay;
        TimeSpan shiftout = Convert.ToDateTime(shiftOutTimeTextBox.Text).TimeOfDay;

        TimeSpan firstot = new TimeSpan();
        TimeSpan lastot = new TimeSpan();
        TimeSpan finalot = new TimeSpan();


        firstot = shiftin.Subtract(intime);
        lastot = outtime.Subtract(shiftout);

        finalot = lastot.Add(firstot);

        string[] time = finalot.ToString().Split(':');
        otTextBox.Text = time[0];
        otMinTextBox.Text = time[1];
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}