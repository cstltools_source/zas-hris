﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="ManualAttendenceCreation.aspx.cs" Inherits="HRM_UI_ManualAttendenceCreation" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Manual Attendance Group Creation </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Manual Attendance Group Creation</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>


                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Code </label>
                                         <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm" 
                                         ontextchanged="empCodeTextBox_TextChanged" AutoPostBack="True"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name</label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Attendance Date </label>
                                        <asp:TextBox ID="attDateTextBox" runat="server" CssClass="form-control form-control-sm" 
                                        AutoPostBack="True"></asp:TextBox>
                                    <asp:ImageButton ID="imgDate" runat="server" 
                                        AlternateText="Click to show calendar" 
                                        ImageUrl="~/Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                        <asp:CalendarExtender ID="attDate" runat="server" Format="dd-MMM-yyyy" TargetControlID="attDateTextBox"
                                             PopupButtonID="imgDate">
                                   </asp:CalendarExtender>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="empidHiddenField" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

