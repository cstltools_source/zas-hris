﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ManualAttendenceCreation : System.Web.UI.Page
{
    private ManualAttendenceGroupBLL attendenceBll = new ManualAttendenceGroupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //DropDownList();
        }
    }
    private void Clear()
    {

        empCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        attDateTextBox.Text = string.Empty;
        empidHiddenField.Value = null;

    }
    private bool Validation()
    {
        if (empCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (attDateTextBox.Text == "")
        {
            showMessageBox("Please Input Attendence !!");
            return false;
        }


        return true;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {

            ManualAttendenceGroup attendence = new ManualAttendenceGroup()
            {
                EmpInfoId = Convert.ToInt32(empidHiddenField.Value),
                ManualStartDate = Convert.ToDateTime(attDateTextBox.Text),
                IsActive = true,
            };

            ManualAttendenceGroupBLL attendenceBll = new ManualAttendenceGroupBLL();
            if (attendenceBll.SaveDataForManualAttendenceGroup(attendence))
            {
                showMessageBox("Data saved Successfully");
                Clear();
            }
        }

    }

    protected void empCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = attendenceBll.Empcode(empCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empidHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();

            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void departmentListImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManualAttendenceGroupCreationView.aspx");

    }
    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}