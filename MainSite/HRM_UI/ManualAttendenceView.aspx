﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="ManualAttendenceView.aspx.cs" Inherits="HRM_UI_ManualAttendenceView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Manual Attendance View</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="companyInfoNewImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Manual Attendance View</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div id ="container2" style ="height:400px;overflow:auto; " >
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False" 
                                    CssClass="table table-bordered text-center thead-dark" DataKeyNames="MAttendenceId" 
                                   PageIndex="1">
                                    <Columns>
                                        <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                        <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                        <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                        <asp:BoundField DataField="DeptName" HeaderText="Department" />
                                        <asp:BoundField DataField="ATTDate" HeaderText="ATTDate " />
                                        <asp:BoundField DataField="ShiftInTime" HeaderText="InTime" />
                                      
                                         
                                        <asp:BoundField DataField="ShiftOutTime" HeaderText="OutTime" />
                                       <asp:BoundField DataField="OverTimeDuration" HeaderText="Over Time" />
                                        <asp:TemplateField HeaderText="Delete">
                                        <HeaderTemplate>
                                            <asp:ImageButton ID="deleteImageButton" runat="server" 
                                                ImageUrl="~/Assets/img/delete.png" OnClientClick=" return confirn('Confirm Delete ?'); " OnClick="yesButton_Click" />
                                           
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                   

                                    </Columns>
                                </asp:GridView>
                            </div>
                                    </div>
                                </div>
                                

                            </div>

                            
                             <br/>
                            <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

