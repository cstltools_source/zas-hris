﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_ManualAttendenceView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();

    private ManualAttendenceBLL attendenceBll = new ManualAttendenceBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EmpLoad();

        }
    }
    private void EmpLoad()
    {
        aDataTable = attendenceBll.LoadView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }


    protected void companyInfoNewImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("ManualAttendence.aspx");
    }


    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[5].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string Id = loadGridView.DataKeys[i][0].ToString();
                attendenceBll.DeleteData(Id);
            }
           
        }

          EmpLoad();
    }
}