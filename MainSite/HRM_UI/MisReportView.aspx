﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="MisReportView.aspx.cs" Inherits="HRM_UI_MisReportView" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .style1 {
            VERTICAL-ALIGN: TOP;
            TEXT-ALIGN: right;
            FONT-SIZE: 9pt;
            COLOR: #000000;
            FONT-FAMILY: Century Gothic;
            TEXT-DECORATION: NONE;
            BACKGROUND: #F2F2F2; /*C0C0C0*/
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">MIS Report</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">MIS Report</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Type </label>
                                        <asp:DropDownList ID="rptTypeDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="rptTypeDropDownList_SelectedIndexChanged">
                                            <asp:ListItem> Select any one </asp:ListItem>
                                            <asp:ListItem Value="YJL">Yearly Join and Left</asp:ListItem>
                                            <asp:ListItem Value="DWTS">Department Wise Total Salary</asp:ListItem>
                                            <asp:ListItem Value="DWTSC">Department Wise Total Salary Chart</asp:ListItem>
                                            <asp:ListItem Value="YSG">Yearly Salary Graph</asp:ListItem>
                                            <asp:ListItem Value="YAS">Yearly Attendence Statement</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div id="year" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Year </label>
                                            <asp:DropDownList ID="yearDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="fromto" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>From Year </label>
                                            <asp:DropDownList ID="fromyearDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>To Year </label>
                                            <asp:DropDownList ID="toyearDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="oid" runat="server">
                                <div id="divshift" runat="server" visible="False">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Shift </label>
                                                <asp:DropDownList ID="shiftDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="date" runat="server" visible="False">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>From Date </label>
                                                <div class="input-group date pull-left" id="daterangepicker113">
                                                    <asp:TextBox ID="fromdtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom"
                                                        TargetControlID="fromdtTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton3" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                 ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>To Date </label>
                                                <div class="input-group date pull-left" id="daterangepicker12">
                                                    <asp:TextBox ID="todtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton4" CssClass="custom"
                                                        TargetControlID="todtTextBox" />
                                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                        <span>
                                                            <asp:ImageButton ID="ImageButton4" runat="server"
                                                                AlternateText="Click to show calendar"
                                                                 ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br />

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

