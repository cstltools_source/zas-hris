﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HRM_UI_MisReportView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
            LoadFromYear();
            LoadToYear();
        }
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= (Convert.ToInt32(DateTime.Now.Year)); i++)
            yearDropDownList.Items.Add(Convert.ToString(i));
    }

    private void LoadFromYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= (Convert.ToInt32(DateTime.Now.Year)); i++)
            fromyearDropDownList.Items.Add(Convert.ToString(i));
    }
    private void LoadToYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= (Convert.ToInt32(DateTime.Now.Year)); i++)
            toyearDropDownList.Items.Add(Convert.ToString(i));
    }
    protected void rptTypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        year.Visible = false;
        fromto.Visible = false;
        date.Visible = false;
        if (rptTypeDropDownList.SelectedValue=="YJL")
        {
            year.Visible = true;
            fromto.Visible = false;
        }
        if (rptTypeDropDownList.SelectedValue == "DWTS")
        {
            year.Visible = false;
            fromto.Visible = false;
        }
        if (rptTypeDropDownList.SelectedValue == "DWTSC")
        {
            year.Visible = false;
            fromto.Visible = false;
        }
        if (rptTypeDropDownList.SelectedValue == "YSG")
        {
            year.Visible = false;
            fromto.Visible = true;
        }
        if (rptTypeDropDownList.SelectedValue == "YAS")
        {
            date.Visible = true;
        }
    }
    private void PopUp()
    {
        string url = "../Report_UI/MisReportViewer.aspx?rptType=" + rptTypeDropDownList.SelectedValue + "&year=" + yearDropDownList.SelectedValue + "&from=" + fromyearDropDownList.SelectedValue + "&to=" + toyearDropDownList.SelectedValue + "&fromdate=" + fromdtTextBox.Text+ "&todate=" + todtTextBox.Text;

        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void viewButton_Click(object sender, EventArgs e)
    {
        PopUp();
    }
}