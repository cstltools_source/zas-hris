﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_MobileAllowanceEntry : System.Web.UI.Page
{
    MobileAllowanceBLL _aMobileAllowanceBll=new MobileAllowanceBLL(); 
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        effectDateTexBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        GradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        empGradeLabel.Text = string.Empty;
        empTypeLabel.Text = string.Empty;
        mobAllowanceTexBox.Text = string.Empty;
        pre.Visible = false;
        preAmountTypeLabel.Text = string.Empty;
        preDateLabel.Text = string.Empty;

    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        if (mobAllowanceTexBox.Text == "")
        {
            showMessageBox("Please Input Amount Code !!");
            return false;
        }
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            MobileAllowance aMobileAllowance = new MobileAllowance();
            aMobileAllowance.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aMobileAllowance.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
            aMobileAllowance.CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value);
            aMobileAllowance.UnitId = Convert.ToInt32(unitIdHiddenField.Value);
            aMobileAllowance.DivisionId = Convert.ToInt32(divIdHiddenField.Value);
            aMobileAllowance.DeptId = Convert.ToInt32(deptIdHiddenField.Value);
            aMobileAllowance.SectionId = Convert.ToInt32(secIdHiddenField.Value);
            aMobileAllowance.DesigId = Convert.ToInt32(desigIdHiddenField.Value);
            aMobileAllowance.GradeId = Convert.ToInt32(GradeIdHiddenField.Value);
            aMobileAllowance.EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value);
            aMobileAllowance.EntryUser = Session["LoginName"].ToString();
            aMobileAllowance.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            aMobileAllowance.MobAllowanceAmont = Convert.ToDecimal(mobAllowanceTexBox.Text);
            aMobileAllowance.ActionStatus = "Posted";
            aMobileAllowance.IsActive = true;
            
            if (_aMobileAllowanceBll.SaveDataForMobAllowance(aMobileAllowance))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox(" Mobile Allowance already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("MobileAllowanceView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aMobileAllowanceBll.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                preDateLabel.Text = "";
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                GradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();


                DataTable dtMobAllownace = _aMobileAllowanceBll.LoadEmpMobAllowanceInfo(EmpInfoIdHiddenField.Value);
                if (dtMobAllownace.Rows.Count>0)
                {
                    pre.Visible = true;

                    preAmountTypeLabel.Text = dtMobAllownace.Rows[0]["MobAllowanceAmont"].ToString();
                    preDateLabel.Text = Convert.ToDateTime(dtMobAllownace.Rows[0]["EffectiveDate"].ToString()).ToString("dd-MMM-yyyy");
                }
                else
                {
                    pre.Visible = false;
                }



            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void effectDateTexBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(preDateLabel.Text))
        {
            if (Convert.ToDateTime(preDateLabel.Text) >= Convert.ToDateTime(effectDateTexBox.Text))
            {
                effectDateTexBox.Text = "";
                showMessageBox("New Effective Date Must Be Greater Then Previous Effective Date");
            }
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}