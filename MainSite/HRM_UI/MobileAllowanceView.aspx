﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="MobileAllowanceView.aspx.cs" Inherits="HRM_UI_MobileAllowanceView" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }

    </style>

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">MobileAllowance View </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentNewImageButton_Click" />
                        <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="deptReloadImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Monthly Task </a></li>
                            <li class="breadcrumb-item"><a href="#">MobileAllowance View</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 380px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark" DataKeyNames="MobAllowanceId"
                                    OnRowCommand="loadGridView_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                        <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                        <asp:BoundField DataField="DesigName" HeaderText="Designation Name" />
                                        <asp:BoundField DataField="DeptName" HeaderText="Dept Name" />
                                        <asp:BoundField DataField="EffectiveDate" DataFormatString="{0:dd-MMM-yyyy}"
                                            HeaderText="Effective Date" />
                                        <asp:BoundField DataField="MobAllowanceAmont" HeaderText="Amount" />
                                        <asp:BoundField DataField="EntryUser" HeaderText="Entry User" />
                                        <asp:BoundField DataField="EntryDate" DataFormatString="{0:dd-MMM-yyyy}"
                                            HeaderText="Entry Date" />

                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="editImageButton" runat="server"
                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditData"
                                                    ImageUrl="~/Assets/img/rsz_edit.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Delete">
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="deleteImageButton" runat="server"
                                                    ImageUrl="~/Assets/delete-icon.png"  OnClientClick="return confirm('Do you want to delete this data ?'); " OnClick="yesButton_Click"/>
                                                <%--<asp:ModalPopupExtender ID="pnlModal_ModalPopupExtender" runat="server"
                                                    BackgroundCssClass="modalBackground" CancelControlID="noButton" DropShadow="true"
                                                    Enabled="True" OkControlID="yesButton" PopupControlID="pnlModal"
                                                    TargetControlID="deleteImageButton">
                                                </asp:ModalPopupExtender> 
                                                <asp:Panel ID="pnlModal" runat="server" BackColor="gray" Style="display: none;">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Confirm Delete</h5>
                                                            </div>
                                                            <div class="modal-body"> You want to delete this data ? </div>
                                                            <div class="modal-footer">
                                                                <asp:Button ID="yesButton" Text="Yes" CssClass="btn btn-sm btn-info" runat="server" OnClick="yesButton_Click" />
                                                                <asp:Button ID="noButton" Text="No" CssClass="btn btn-sm warning" runat="server" OnClick="noButton_OnClick"  BackColor="#FF9900" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

