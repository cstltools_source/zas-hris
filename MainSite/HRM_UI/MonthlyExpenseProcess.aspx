﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="MonthlyExpenseProcess.aspx.cs" Inherits="HRM_UI_MonthlyExpenseProcess" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Reference Page="~/HRM_UI/AutoAttendenceAdjustment.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style>
        .form-group > label {
            font-size: 14px;
            font-weight: bold;
        }

        fieldset.for-panel {
            background-color: #fcfcfc;
            border: 1px solid #999;
            padding: 10px 10px;
            background-color: white;
            margin-bottom: 12px;
        }

            fieldset.for-panel legend {
                background-color: #fafafa;
                border: 1px solid #ddd;
                border-radius: 1px;
                font-size: 12px;
                font-weight: bold;
                line-height: 10px;
                margin: inherit;
                padding: 7px;
                width: auto;
                margin-bottom: 0;
                color: black;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Monthly Expense Process  </h1>
                    </div>

                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">All Process </a></li>
                            <li class="breadcrumb-item"><a href="#">Monthly Expense Process </a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">

                            <fieldset class="for-panel">
                                <legend>Process Details </legend>

                                <div class="col-md-4">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-sm-5 col-md-5 label-align">
                                                    <span class="lbl">Expense Year: </span>
                                                </div>
                                                <div class="col-sm-7 col-md-7">
                                                    <asp:DropDownList ID="yearDropDownList" runat="server" CssClass="form-control form-control-sm"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <div class="col-sm-5 col-md-5 label-align">
                                                    <span class="lbl">Month: </span>
                                                </div>
                                                <div class="col-sm-7 col-md-7">
                                                    <asp:DropDownList ID="monthDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                        <asp:ListItem Value=""> Select any one</asp:ListItem>
                                                        <asp:ListItem Value="1">January</asp:ListItem>
                                                        <asp:ListItem Value="2">February</asp:ListItem>
                                                        <asp:ListItem Value="3">March</asp:ListItem>
                                                        <asp:ListItem Value="4">April</asp:ListItem>
                                                        <asp:ListItem Value="5">May</asp:ListItem>
                                                        <asp:ListItem Value="6">June</asp:ListItem>
                                                        <asp:ListItem Value="7">July</asp:ListItem>
                                                        <asp:ListItem Value="8">August</asp:ListItem>
                                                        <asp:ListItem Value="9">September</asp:ListItem>
                                                        <asp:ListItem Value="10">October</asp:ListItem>
                                                        <asp:ListItem Value="11">November</asp:ListItem>
                                                        <asp:ListItem Value="12">December</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    
                                </div>
                            </fieldset>


                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Start process" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />

                                    </div>
                                </div>
                            </div>

                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />
                            <br />

                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/Assets/progress-bar-opt.gif"
                    Height="80" Width="80" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

