﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL.HRM_DAL;


public partial class HRM_UI_MonthlyExpenseProcess : System.Web.UI.Page
{
    AutoAttendanceAdjustmentDal adjustmentDal = new AutoAttendanceAdjustmentDal();
    ExpenseEntryDal aEntryDal = new ExpenseEntryDal();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetStatus();           
        }
    }

    private void SetStatus()
    {
        aEntryDal.LoadFinancialYear(yearDropDownList);
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (yearDropDownList.SelectedValue != "")
        {
            if (monthDropDownList.SelectedValue != "")
            {
                aEntryDal.ProcessExpense(yearDropDownList.SelectedValue, monthDropDownList.SelectedItem.Text.Trim());
                ShowMessageBox("Process Done !!!"); 
            }
            else
            {
                ShowMessageBox("You should select month !!!");
            }
           
        }
        else
        {
            ShowMessageBox("You should select expense year !!!");
        }
        
    }

    protected void ShowMessageBox(string message)
    {
        message = message.Replace("'", "\'");
        string sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    
}