﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="MultipleReport.aspx.cs" Inherits="HRM_UI_MultipleReport" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Multilpe Report Page </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="viewListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Multilpe Report Page</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Report Name </label>
                                        <asp:DropDownList ID="selectReportDropDownList" runat="server"
                                            CssClass="form-control form-control-sm">
                                            <asp:ListItem> Select any one </asp:ListItem>
                                            <asp:ListItem Value="C">Company Info</asp:ListItem>
                                            <asp:ListItem Value="U">Unit</asp:ListItem>
                                            <asp:ListItem Value="D">Division</asp:ListItem>
                                            <asp:ListItem Value="DP">Department</asp:ListItem>
                                            <asp:ListItem Value="S">Section</asp:ListItem>
                                            <asp:ListItem Value="ET">Employee Type</asp:ListItem>
                                            <asp:ListItem Value="EU">Employee Login Info</asp:ListItem>
                                            <asp:ListItem Value="EGI">Employee Group Info</asp:ListItem>
                                            <asp:ListItem Value="L">Line</asp:ListItem>
                                            <asp:ListItem Value="DG">Designation</asp:ListItem>
                                            <asp:ListItem Value="SH">Shift</asp:ListItem>
                                            <asp:ListItem Value="G">Grade</asp:ListItem>
                                            <asp:ListItem Value="SG">Salary Grade</asp:ListItem>
                                        </asp:DropDownList>
                                       
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewRptButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

