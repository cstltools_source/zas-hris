﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_OnDutyApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    OnDutyBLL aOnDutyBLL = new OnDutyBLL();
    AttedanceOperationBLL attedanceOperationBll = new AttedanceOperationBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            OnDutyLoad();
        }
    }
    public void OnDutyLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        OnDutyLoadByCondition(filename, userName);
        aOnDutyBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void OnDutyLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aOnDutyBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aOnDutyBLL.LoadOnDutyViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
   
    private void PopUp(string Id)
    {
        string url = "OnDutyEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ondutyId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(ondutyId);
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");
       
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
                if (ChkBoxHeader.Checked == true)
                {
                    ChkBoxRows.Checked = true;
                }
                else
                {
                    ChkBoxRows.Checked = false;
                }
            }
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            SubminApproval();
            OnDutyLoad();
        }
    }

    private bool Validation()
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[4].FindControl("chkSelect");
            if (ChkBoxRows.Checked == true)
            {
                return true;
                break;
            }
        }
        showMessageBox("Select Atleast One!!");
        return false;
    }
    private void SubminApproval()
    {
        if (actionRadioButtonList.SelectedValue == null)
        {
            showMessageBox("Choose One of the Operations ");
        }
        else
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {

                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[8].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    OnDuty aOnDuty = new OnDuty()
                    {
                        OnDutyId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString()),
                        OnDDate = Convert.ToDateTime(loadGridView.Rows[i].Cells[4].Text),
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        ApprovedUser = Session["LoginName"].ToString(),
                        ApprovedDate = System.DateTime.Today,
                    };
                    if (aOnDutyBLL.ApprovalUpdateBLL(aOnDuty))
                    {
                        if (actionRadioButtonList.SelectedItem.Text == "Accepted")
                        {
                            attedanceOperationBll.UpdateOperationBLL(aOnDuty.OnDDate, aOnDuty.EmpInfoId, "OD", "On Duty");
                        }
                    }
                }
            }
          
            showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + " Successfully !!");
        }
    }
}