﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_OnDutyEdit : System.Web.UI.Page
{
    OnDutyBLL aDutyBll = new OnDutyBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ondutyIdHiddenField.Value = Request.QueryString["ID"];
            OnDutyLoad(ondutyIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (OnDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (dutyLocTextBox.Text == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (purposTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
       
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            OnDuty aOnDuty = new OnDuty()
            {
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                OnDutyId = Convert.ToInt32(ondutyIdHiddenField.Value),
                OnDDate = Convert.ToDateTime(OnDateTextBox.Text),
                DutyLocation = dutyLocTextBox.Text,
                Purpose = purposTextBox.Text,
                ActionRemarks = remarksTextBox.Text
                
            };
            if (!aDutyBll.UpdateDataForOnDuty(aOnDuty))
            {
                showMessageBox("Data did not Update !!!!");
                
            }
            else
            {
                showMessageBox("Data update successfully , Please reload !!!");
            }

        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    private void OnDutyLoad(string ondutyId)
    {
        OnDuty aOnDuty = new OnDuty();
        aOnDuty = aDutyBll.OnDutyEditLoad(ondutyId);
        OnDateTextBox.Text = aOnDuty.OnDDate.ToString("dd-MMM-yyyy");
        empInfoIdHiddenField.Value = aOnDuty.EmpInfoId.ToString();
        dutyLocTextBox.Text = aOnDuty.DutyLocation.ToString();
        purposTextBox.Text = aOnDuty.Purpose.ToString();
        remarksTextBox.Text = aOnDuty.ActionRemarks.ToString();
        GetEmpMasterCode(empInfoIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string empid)
    {
        if (!string.IsNullOrEmpty(empid))
        {
            DataTable aTable = new DataTable();
            aTable = aDutyBll.LoadEmpInfoCode(empid);

            if (aTable.Rows.Count > 0)
            {
                empMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {

    }
}