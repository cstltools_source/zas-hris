﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="OnDutyEntry.aspx.cs" Inherits="HRM_UI_OnDutyEntry" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">On Duty Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="jobViewImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Daily Tasks </a></li>
                            <li class="breadcrumb-item"><a href="#">On Duty Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                 <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Master Code </label>
                                        <asp:TextBox ID="empMasterCodeTextBox" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm" OnTextChanged="empMasterCodeTextBox_TextChanged"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-1">
                                    <div class="form-group">
                                        <label style="color: white">Search </label>
                                        <br />
                                        <asp:Button ID="searchButton" runat="server" CssClass="btn btn-sm btn-info" Text="Search" OnClick="searchButton_Click" />
                                    </div>
                                </div>
                                 <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name  </label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" ReadOnly="True" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:HiddenField ID="empInfoIdHiddenField" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>On Duty From Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                            <asp:TextBox ID="OnFrDDateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom" PopupPosition="TopLeft"
                                                TargetControlID="OnFrDDateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton1" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>On Duty To Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker12">
                                            <asp:TextBox ID="OnToDDateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton2" CssClass="custom" PopupPosition="TopLeft"
                                                TargetControlID="OnToDDateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton2" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Duty Location  </label>
                                        <asp:TextBox ID="dutyLocTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Purpose  </label>
                                        <asp:TextBox ID="purposTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>                        
                            </div>
                             <br/>
                             <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

