﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_OnDutyEntry : System.Web.UI.Page
{
    OnDutyBLL aDutyBll = new OnDutyBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        OnFrDDateTextBox.Text = string.Empty;
        OnToDDateTextBox.Text = string.Empty;
        empMasterCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        dutyLocTextBox.Text = string.Empty;
        purposTextBox.Text = string.Empty;
     
    }
    public bool OnDutyDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(OnFrDDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (OnFrDDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (dutyLocTextBox.Text == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        if (purposTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
        if (OnDutyDate() == false)
        {
            showMessageBox("Please give a valid OnDuty Date !!!");
            return false;
        }
        return true;
    }
    
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
       {
            OnDuty aOnDuty = new OnDuty()
            {
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                OnDDate = Convert.ToDateTime(OnFrDDateTextBox.Text),
                OnTDate = Convert.ToDateTime(OnToDDateTextBox.Text),
                DutyLocation = dutyLocTextBox.Text,
                Purpose = purposTextBox.Text,
                ActionStatus = "Posted",
                ActionRemarks = "",
                IsActive = true,
                EntryUser = Session["LoginName"].ToString(),
                EntryDate = System.DateTime.Today,
            };
           if (aDutyBll.SaveDataForOnDuty(aOnDuty))
           {
               showMessageBox("Data Save Successfully");
               Clear();
           }  
        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    protected void jobViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("OnDutyView.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = aDutyBll.LoadEmpInfo(empMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}