﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_OnDutyView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    OnDutyBLL aOnDutyBLL = new OnDutyBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            OnDutyLoad();
        }
    }

    private void OnDutyLoad()
    {
        aDataTable = aOnDutyBLL.LoadOnDutyView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        aOnDutyBLL.CancelDataMarkBLL(loadGridView, aDataTable);
    }


    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        OnDutyLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("OnDutyEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "OnDutyEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ondutyId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(ondutyId);
        }

    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[8].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string OnDutyId = loadGridView.DataKeys[i][0].ToString();
                aOnDutyBLL.DeleteDataBLL(OnDutyId);
            }
         
        }
        OnDutyLoad();
    }
    protected void loadGridView_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}