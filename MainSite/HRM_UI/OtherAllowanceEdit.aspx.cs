﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_OtherAllowanceEdit : System.Web.UI.Page
{
    OtherAllowanceBLL aOtherAllowanceBLL = new OtherAllowanceBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            otherallowanceIdHiddenField.Value = Request.QueryString["ID"];
            OtherAllowanceEditLoad(otherallowanceIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (otherAllowanceTexBox.Text == "")
        {
            showMessageBox("Please Input Food Charge!!");
            return false;
        }
        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input  Effect Date!!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            OtherAllowance aOtherAllowance = new OtherAllowance()
            {
                OtherAllowanceId = Convert.ToInt32(otherallowanceIdHiddenField.Value),
                Amount = Convert.ToDecimal(otherAllowanceTexBox.Text),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value)
            };

            if (!aOtherAllowanceBLL.UpdateaDataforaOtherAllowance(aOtherAllowance))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void OtherAllowanceEditLoad(string OtherAllowanceId)
    {
        OtherAllowance aOtherAllowance = new OtherAllowance();
        aOtherAllowance = aOtherAllowanceBLL.OtherAllowanceEditLoad(OtherAllowanceId);
        otherAllowanceTexBox.Text = aOtherAllowance.Amount.ToString();
        effectDateTexBox.Text = aOtherAllowance.EffectiveDate.ToString("dd-MMM-yyyy");
        EmpInfoIdHiddenField.Value = aOtherAllowance.EmpInfoId.ToString();
        GetEmpCode(EmpInfoIdHiddenField.Value);
    }

    public void GetEmpCode(string empid)
    {
        if (!string.IsNullOrEmpty(empid))
        {
            DataTable aTable = new DataTable();
            aTable = aOtherAllowanceBLL.LoadEmpInfoCode(empid);

            if (aTable.Rows.Count > 0)
            {
                empCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}