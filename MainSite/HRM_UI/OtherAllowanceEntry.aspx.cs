﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_FoodChargeEntry : System.Web.UI.Page
{
    OtherAllowanceBLL aOtherAllowanceBLL = new OtherAllowanceBLL();
    EmpGeneralInfoBLL aEmpGeneralInfoBll=new EmpGeneralInfoBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
            GridView();
        }

    }
    public void GridView()
    {
        DataTable aDataTable = new DataTable();
        aDataTable = aOtherAllowanceBLL.LoadEmpDept(sectionDropDownList.SelectedValue);
        otherAllowanceGridView.DataSource = aDataTable;
        otherAllowanceGridView.DataBind();

    }
    public void DropDownList()
    {
        aEmpGeneralInfoBll.LoadDivisionNameToDropDownBLL(divisionDropDownList);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       
       effectDateTexBox.Text = string.Empty;
        otherAllowanceGridView.DataSource = null;
        otherAllowanceGridView.DataBind();
        divisionDropDownList.SelectedValue = null;
        departmentDropDownList.SelectedValue = null;
        sectionDropDownList.SelectedValue = null;
    }
    private bool Validation()
    {
        
        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        List<OtherAllowance> aOtherAllowanceList=new List<OtherAllowance>();
        for (int i = 0; i < otherAllowanceGridView.Rows.Count; i++)
        {
            if (((TextBox)otherAllowanceGridView.Rows[i].Cells[3].FindControl("amountTextBox")).Text !="")
            {
                OtherAllowance allowance = new OtherAllowance();
                allowance.Amount = Convert.ToDecimal(((TextBox)otherAllowanceGridView.Rows[i].Cells[3].FindControl("amountTextBox")).Text);
                allowance.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
                allowance.EmpInfoId = Convert.ToInt32(otherAllowanceGridView.DataKeys[i][0].ToString());
                allowance.EntryUser = Session["LoginName"].ToString();
                allowance.EntryDate =Convert.ToDateTime(Convert.ToDateTime(DateTime.Today.ToShortDateString()).ToString("dd-MMM-yyyy"));
                allowance.ActionStatus = "Posted";
                aOtherAllowanceList.Add(allowance);
                allowance.IsActive = true;
            }
            
        }
        if (aOtherAllowanceBLL.SaveDataForOtherAllowance(aOtherAllowanceList))
        {
            showMessageBox("Data Save Successfully ");
            Clear();
        }
        else
        {
            showMessageBox("Other ALlowance already exist");
        }
       
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("OtherAllowanceView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        //if (!string.IsNullOrEmpty(empCodeTextBox.Text.Trim()))
        //{
        //    DataTable aTable = new DataTable();
        //    aTable = aOtherAllowanceBLL.LoadEmpInfo(empCodeTextBox.Text);

        //    if (aTable.Rows.Count > 0)
        //    {
        //        EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
        //        empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
        //    }
        //    else
        //    {
        //        showMessageBox("Data not Found");
        //    }
        //}
        //else
        //{
        //    showMessageBox("Please Input Employee Code");
        //}
    }
    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aEmpGeneralInfoBll.LoadSectionNameToDropDownBLL(sectionDropDownList,departmentDropDownList.SelectedValue);
    }
    protected void divisionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aEmpGeneralInfoBll.LoadDepartmentToDropDownBLL(departmentDropDownList,divisionDropDownList.SelectedValue);
    }
    protected void sectionDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridView();
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}