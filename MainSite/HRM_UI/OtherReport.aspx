﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="OtherReport.aspx.cs" Inherits="HRM_UI_MultipleReport" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Other Report</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Other Report</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Report Type </label>
                                        <asp:DropDownList ID="selectReportDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="selectReportDropDownList_SelectedIndexChanged">
                                            <asp:ListItem> Select any one </asp:ListItem>
                                            <asp:ListItem Value="AR">Arrear</asp:ListItem>
                                            <asp:ListItem Value="TF">Employee Transfer</asp:ListItem>
                                            <asp:ListItem Value="TL">Transfer Letter</asp:ListItem>
                                            <asp:ListItem Value="PRO">Employee Promotion</asp:ListItem>
                                            <asp:ListItem Value="PI">Promo Increment Letter</asp:ListItem>
                                            <asp:ListItem Value="ES">Employee Suspend</asp:ListItem>
                                            <asp:ListItem Value="EJ">Employee Job Left</asp:ListItem>
                                            <asp:ListItem Value="SD">Salary Deduction</asp:ListItem>
                                            <asp:ListItem Value="SI">Salary Increment</asp:ListItem>
                                            <asp:ListItem Value="SI">Salary Inc Letter</asp:ListItem>
                                            <asp:ListItem Value="SIA">Salary Inc Letter All</asp:ListItem>
                                            <asp:ListItem Value="LA">Late Attandance </asp:ListItem>
                                            <asp:ListItem Value="ME">Manual Attandance </asp:ListItem>
                                            <asp:ListItem Value="LN">Loan</asp:ListItem>
                                            <asp:ListItem Value="RA">Reappointment</asp:ListItem>
                                            <asp:ListItem Value="HD">Holiday</asp:ListItem>
                                            <asp:ListItem Value="PI">Promotional Increment</asp:ListItem>
                                            <asp:ListItem Value="LND">Loan Detail</asp:ListItem>
                                            <asp:ListItem Value="LNS">Loan (Single)</asp:ListItem>
                                            <asp:ListItem Value="TY">Income Tax Yearly</asp:ListItem>
                                            <asp:ListItem Value="LMW">Loan Month Wise</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div id="divemp" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Select Status Type </label>
                                            <asp:DropDownList ID="typeReportDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="typeReportDropDownList_SelectedIndexChanged">
                                                <asp:ListItem></asp:ListItem>
                                                <asp:ListItem Value="A">Accepted</asp:ListItem>
                                                <asp:ListItem Value="P">Posted</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divempwise" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Wise </label>
                                            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" CssClass="custom-control custom-checkbox" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divEcode" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Code </label>
                                            <asp:TextBox ID="empcodeTextBox" runat="server" CssClass="form-control form-control-sm"
                                                AutoPostBack="True" OnTextChanged="empcodeTextBox_TextChanged"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="date" runat="server">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>From Date </label>
                                            <div class="input-group date pull-left" id="daterangepicker113">
                                                <asp:TextBox ID="fromdtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                    Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom" PopupPosition="TopLeft"
                                                    TargetControlID="fromdtTextBox" />
                                                <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                    <span>
                                                        <asp:ImageButton ID="ImageButton3" runat="server"
                                                            AlternateText="Click to show calendar"
                                                             ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <div class="input-group date pull-left" id="daterangepicker12">
                                                <asp:TextBox ID="todtTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                    Format="dd-MMM-yyyy" PopupButtonID="ImageButton4" CssClass="custom" PopupPosition="TopLeft"
                                                    TargetControlID="todtTextBox" />
                                                <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                    <span>
                                                        <asp:ImageButton ID="ImageButton4" runat="server"
                                                            AlternateText="Click to show calendar"
                                                             ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="loan" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Loan </label>
                                            <asp:DropDownList ID="loanDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div id="year" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Loan </label>
                                            <asp:DropDownList ID="yearDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewRptButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

