﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_MultipleReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
        }
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }
    private string ParameterGenerator()
    {
        string parameter = "";

        if (selectReportDropDownList.SelectedValue == "ME")
        {
            parameter = " WHERE tblManualAttendence.IsActive=1 ";

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblManualAttendence.ATTDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblManualAttendence.ATTDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblManualAttendence.ATTDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }

        if (selectReportDropDownList.SelectedValue == "HD")
        {
            parameter = " WHERE tblHolidayInformation.IsActive=1 ";

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblHolidayInformation.HolidayDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblHolidayInformation.HolidayDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblHolidayInformation.HolidayDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "EJ")
        {
            parameter = " WHERE tblJobleft.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblJobleft.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblJobleft.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblJobleft.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblJobleft.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblJobleft.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "RA")
        {
            parameter = " WHERE tblReApointment.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblReApointment.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblReApointment.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblReApointment.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblReApointment.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblReApointment.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }

        if (selectReportDropDownList.SelectedValue == "ES")
        {
            parameter = " WHERE tblSuspend.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblSuspend.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblSuspend.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblSuspend.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblSuspend.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblSuspend.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "SI")
        {
            parameter = " WHERE tblIncrement.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblIncrement.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblIncrement.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblIncrement.ActiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblIncrement.ActiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblIncrement.ActiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "LN")
        {
            parameter = " WHERE tblLoanMaster.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblLoanMaster.Status in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblLoanMaster.Status in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblLoanMaster.SanctionDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblLoanMaster.SanctionDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblLoanMaster.SanctionDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "SD")
        {
            parameter = " WHERE tblSalaryDeduction.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblSalaryDeduction.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblSalaryDeduction.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblSalaryDeduction.SDEffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblSalaryDeduction.SDEffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblSalaryDeduction.SDEffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "LA")
        {
            parameter = " WHERE tblLateApprove.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblLateApprove.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblLateApprove.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblLateApprove.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblLateApprove.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblLateApprove.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "AR")
        {
            parameter = " WHERE tblArrear.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblArrear.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblArrear.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblArrear.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblArrear.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblArrear.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }

        if (selectReportDropDownList.SelectedValue == "TF")
        {
            parameter = " WHERE tblTransfer.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblTransfer.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblTransfer.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblTransfer.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblTransfer.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblTransfer.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "PRO")
        {
            parameter = " WHERE tblPromotion.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblPromotion.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblPromotion.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblPromotion.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblPromotion.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblPromotion.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "MA" || selectReportDropDownList.SelectedValue == "MAUW")
        {
            parameter = " WHERE tblMobileAllowance.IsActive=1 ";

            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and tblMobileAllowance.ActionStatus in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and tblMobileAllowance.ActionStatus in ('Posted','Verified') ";
            }

            if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            {
                parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            }

            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + " and tblMobileAllowance.EffectiveDate = '" + fromdtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(todtTextBox.Text))
            {
                if (string.IsNullOrEmpty(fromdtTextBox.Text))
                {
                    parameter = parameter + " and tblMobileAllowance.EffectiveDate = '" + todtTextBox.Text + "' ";
                }
            }
            if (!string.IsNullOrEmpty(fromdtTextBox.Text))
            {
                if (!string.IsNullOrEmpty(todtTextBox.Text))
                {
                    parameter = parameter + "  and tblMobileAllowance.EffectiveDate between  '" + fromdtTextBox.Text + "' and  '" + todtTextBox.Text + "' ";
                }
            }
            return parameter;
        }

        if (selectReportDropDownList.SelectedValue=="LND")
        {
            //if (typeReportDropDownList.SelectedValue == "A")
            //{
            //    parameter = parameter + " and Status in ('Accepted') ";
            //}
            //if (typeReportDropDownList.SelectedValue == "P")
            //{
            //    parameter = parameter + " and Status in ('Posted','Verified') ";
            //}

            //if (!string.IsNullOrEmpty(empcodeTextBox.Text))
            //{
            //    parameter = parameter + " and tblEmpGeneralInfo.EmpMasterCode = '" + empcodeTextBox.Text + "' ";
            //}
            return parameter;
        }
        if (selectReportDropDownList.SelectedValue == "LNS")
        {
            if (typeReportDropDownList.SelectedValue == "A")
            {
                parameter = parameter + " and Status in ('Accepted') ";
            }
            if (typeReportDropDownList.SelectedValue == "P")
            {
                parameter = parameter + " and Status in ('Posted','Verified') ";
            }

            Session["LoanId"] = loanDropDownList.SelectedValue;

            parameter = parameter + " AND tblLoanMaster.LoanId='" + loanDropDownList.SelectedValue + "'";

            return parameter;
        }
        //if (selectReportDropDownList.SelectedValue == "U")
        //{
        //    parameter = "Where tblEmpGeneralInfo.IsActive=1";
        //    return parameter;
        //}

        return parameter;
    }

    private void PopUp(string rptType)
    {
        string url = "../Report_UI/OtherReportViewer.aspx?rptType=" + rptType + "&empcode=" + empcodeTextBox.Text + "&status=" + CheckBox1.Checked + "&fromdt=" + fromdtTextBox.Text + "&todt=" + todtTextBox.Text + "&actionstatus="+typeReportDropDownList.SelectedItem.Text+"&year="+yearDropDownList.SelectedItem.Text; 
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void viewRptButton_Click(object sender, EventArgs e)
    {
        Session["ReportParameter"] = ParameterGenerator();
        
        PopUp(selectReportDropDownList.SelectedValue);
    }
    protected void selectReportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        divemp.Visible = true;
        divEcode.Visible = true;
        divempwise.Visible = true;
        date.Visible = true;
        empcodeTextBox.Text = "";
        loan.Visible = false;
        year.Visible = false;
        if (selectReportDropDownList.SelectedValue=="LND")
        {
            divEcode.Visible = false;
            divempwise.Visible = false;
            date.Visible = false;
            divemp.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue == "LMW")
        {
            divEcode.Visible = false;
            divempwise.Visible = false;
            date.Visible = false;
            divemp.Visible = false;
            year.Visible = true;
        }
        if (selectReportDropDownList.SelectedValue=="TY")
        {
            divEcode.Visible = false;
            divempwise.Visible = false;
            date.Visible = true;
            divemp.Visible = false;
        }
        if (selectReportDropDownList.SelectedValue == "LNS")
        {
            loan.Visible = true;
        }
    }
    protected void typeReportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        divempwise.Visible = true;
    }
    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBox1.Checked == true)
        {
            divEcode.Visible = true;
        }
        if (CheckBox1.Checked == false)
        {
            divEcode.Visible = false;
            empcodeTextBox.Text = string.Empty;
        }
    }
    protected void empcodeTextBox_TextChanged(object sender, EventArgs e)
    {
        OtherReportBLL aOtherReportBll=new OtherReportBLL();
        aOtherReportBll.LoadLoan(loanDropDownList,empcodeTextBox.Text);
    }
}