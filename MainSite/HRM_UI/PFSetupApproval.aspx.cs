﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_PFSetupApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    PFSetupBLL aPFSetupBLL = new PFSetupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PFSetupLoad();
        }
    }
    public void PFSetupLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        PFSetupLoadByCondition(filename, userName);
        aPFSetupBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void PFSetupLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aPFSetupBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aPFSetupBLL.LoadPFSetupViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    private void PopUp(string Id)
    {
        string url = @"PFSetupEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        //string str = "";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string PFSetupId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(PFSetupId);
        }

    }
    public bool Validation()
    {
        int c = 0, i;
        for (i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)loadGridView.Rows[i].FindControl("chkSelect");
            if (cb.Checked != true)
            {
                c++;

            }
        }
        if (c == i)
        {
            showMessageBox("Choose check box ");
            return false;
        }
        return true;
    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        } 
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            try
            {
                SubminApproval();
                PFSetupLoad();
            }
            catch (Exception)
            {
                showMessageBox("choose data properly ");
            }
        }
        
    }


    private void SubminApproval()
    {
        

        if (actionRadioButtonList.SelectedValue == null)
        {
            showMessageBox("Choose One of the Operations ");
        }
        else
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    PFSetup aPFSetup = new PFSetup()
                    {
                        PFSId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        ApprovedBy = Session["LoginName"].ToString(),
                        ApprovedDate = System.DateTime.Today,
                    };
                    if (actionRadioButtonList.SelectedValue != null)
                    {
                        aPFSetupBLL.ApprovalUpdateBLL(aPFSetup);
                        aPFSetupBLL.UpdatePFEligibilityStatus(loadGridView.DataKeys[i][1].ToString());
                    }

                }
            }
            showMessageBox("Data "+actionRadioButtonList.SelectedItem.Text+"  Successfully !!");
        }

    }
}