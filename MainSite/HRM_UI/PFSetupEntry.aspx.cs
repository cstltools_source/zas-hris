﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_PFSetupEntry : System.Web.UI.Page
{
    PFSetupBLL _aPFSetupBLL=new PFSetupBLL(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        pfActiveTextBox.Text = string.Empty;
        pfStartDtTextBox.Text = string.Empty;
        empBalanceTextBox.Text = string.Empty;
        companyBalanceTextBox.Text = string.Empty;
        interBalanceTextBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        GradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        empGradeLabel.Text = string.Empty;
        empTypeLabel.Text = string.Empty;
        empBalanceTextBox.Text = string.Empty;
        companyBalanceTextBox.Text = string.Empty;
        prevCheckBox.Checked = false;
        prevb.Visible = false;
    }
    private bool Validation()
    {

        if (pfActiveTextBox.Text == "")
        {
            showMessageBox("Please Input PF Active Date !!");
            return false;
        }
        
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (PFActiveDate() == false)
        {
            showMessageBox("Please give a valid PF Active Date !!!");
            return false;
        }
        
        return true;
    }
    public bool PFActiveDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(pfActiveTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            PFSetup aPFSetup = new PFSetup();
            aPFSetup.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aPFSetup.PFActiveDate = Convert.ToDateTime(pfActiveTextBox.Text);
            aPFSetup.CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value);
            aPFSetup.UnitId = Convert.ToInt32(unitIdHiddenField.Value);
            aPFSetup.DivisionId = Convert.ToInt32(divIdHiddenField.Value);
            aPFSetup.DeptId = Convert.ToInt32(deptIdHiddenField.Value);
            aPFSetup.SectionId = Convert.ToInt32(secIdHiddenField.Value);
            aPFSetup.DesigId = Convert.ToInt32(desigIdHiddenField.Value);
            aPFSetup.GradeId = Convert.ToInt32(GradeIdHiddenField.Value);
            aPFSetup.EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value);
            aPFSetup.EntryUser = Session["LoginName"].ToString();
            aPFSetup.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            //aPFSetup.JoiningDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            aPFSetup.ActionStatus = "Posted";
            aPFSetup.PreBalance = prevCheckBox.Checked;
            if (prevCheckBox.Checked)
            {
                if (!string.IsNullOrWhiteSpace(empBalanceTextBox.Text))
                {
                    aPFSetup.EmpBalance = Convert.ToDecimal(empBalanceTextBox.Text);    
                }
                if (!string.IsNullOrWhiteSpace(companyBalanceTextBox.Text))
                {
                    aPFSetup.CompanyBalance = Convert.ToDecimal(companyBalanceTextBox.Text);
                }
                if (!string.IsNullOrWhiteSpace(interBalanceTextBox.Text))
                {
                    aPFSetup.InterBalance = Convert.ToDecimal(interBalanceTextBox.Text);    
                }
                if (!string.IsNullOrWhiteSpace(pfStartDtTextBox.Text))
                {
                    aPFSetup.PFStartDate = Convert.ToDateTime(pfStartDtTextBox.Text);
                }
                
            }
            
            aPFSetup.IsActive = true;
            
            if (_aPFSetupBLL.SaveDataForPFSetup(aPFSetup))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox(" PFSetup already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("PFSetupView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aPFSetupBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                JoiningDate.Text = Convert.ToDateTime(aTable.Rows[0]["JoiningDate"].ToString().Trim()).ToString("dd-MMM-yyyy");
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                GradeIdHiddenField.Value = aTable.Rows[0]["EmpGradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void prevCheckBox_CheckedChanged(object sender, EventArgs e)
    {
        if (prevCheckBox.Checked)
        {
            prevb.Visible = true;    
        }
        else
        {
            prevb.Visible = false;
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}