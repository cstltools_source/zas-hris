﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_PFStartStopEntry : System.Web.UI.Page
{
    PFStartStopBLL _aPFStartStopBLL=new PFStartStopBLL(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        pfActionTextBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        GradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        empGradeLabel.Text = string.Empty;
        empTypeLabel.Text = string.Empty;
        currentStatusLabel.Text = string.Empty;
        prevDateLabel.Text = string.Empty;
        actionDropDownList.SelectedValue =null;

    }
    private bool Validation()
    {

        if (pfActionTextBox.Text == "")
        {
            showMessageBox("Please Input PF Active Date !!");
            return false;
        }
        
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (PFActiveDate() == false)
        {
            showMessageBox("Please give a valid PF Active Date !!!");
            return false;
        }
        
        return true;
    }
    public bool PFActiveDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(pfActionTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            PFStartStop aPFStartStop = new PFStartStop();
            aPFStartStop.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aPFStartStop.EntryUser = Session["LoginName"].ToString();
            aPFStartStop.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            aPFStartStop.ActionStatus = "Posted";
            aPFStartStop.IsActive = true;
            aPFStartStop.ActiveInactiveDate = Convert.ToDateTime(prevDateLabel.Text);
            aPFStartStop.Action = currentStatusLabel.Text;
            aPFStartStop.ActionDate = Convert.ToDateTime(pfActionTextBox.Text);
            aPFStartStop.ActionStatus = actionDropDownList.SelectedItem.Text;

            if (_aPFStartStopBLL.SaveDataForPFStartStop(aPFStartStop))
            {

                bool status;
                if (actionDropDownList.SelectedItem.Text=="Active")
                {
                    status = true;
                }
                else
                {
                    status = false;
                }

                if (_aPFStartStopBLL.UpdatePFSetup(status,EmpInfoIdHiddenField.Value))
                {
                    
                }

                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox(" PFStartStop already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("PFStartStopView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aPFStartStopBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                GradeIdHiddenField.Value = aTable.Rows[0]["EmpGradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();

                DataTable dt = _aPFStartStopBLL.LoadPFSetup(EmpInfoIdHiddenField.Value);

                if (dt.Rows.Count > 0)
                {
                    prevDateLabel.Text = Convert.ToDateTime(dt.Rows[0]["PFActiveDate"].ToString()).ToString("dd-MMM-yyyy");
                    if (Convert.ToBoolean(dt.Rows[0]["IsActive"].ToString()))
                    {
                        currentStatusLabel.Text = "Active";
                    }
                    else
                    {
                        currentStatusLabel.Text = "Inactive";
                    }

                }
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}