﻿using System;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_PromotionApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    PromotionBLL aPromotionBLL = new PromotionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PromotionLoad();
        }
    }
    private void PromotionLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        PromotionLoadByCondition(filename, userName);
        aPromotionBLL.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void PromotionLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aPromotionBLL.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aPromotionBLL.LoadPromotionViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    private void PopUp(string Id)
    {
        string url = "PromotionEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=700,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string PromotionId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(PromotionId);
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[9].FindControl("chkSelect");
            if (ChkBoxHeader.Checked == true)
            {
                ChkBoxRows.Checked = true;
            }
            else
            {
                ChkBoxRows.Checked = false;
            }
        }
      
    }
    public bool Validation()
    {
        int c = 0, i;
        for (i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)loadGridView.Rows[i].FindControl("chkSelect");
            if (cb.Checked != true)
            {
                c++;

            }
        }
        if (c == i)
        {
            showMessageBox("Choose check box ");
            return false;
        }
        return true;
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            try
            {
                SubmitApproval();
                PromotionLoad();
            }
            catch (Exception)
            {
                showMessageBox("choose data properly ");
            }
        }
        
    }


    private void SubmitApproval()
    {

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {

            CheckBox cb = (CheckBox) loadGridView.Rows[i].Cells[9].FindControl("chkSelect");
            if (cb.Checked==true)
            {
                Promotion aPromotion = new Promotion()
                {
                    PromotionId =  Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                    ActionStatus = actionRadioButtonList.SelectedItem.Text,
                    ApprovedBy = Session["LoginName"].ToString(),
                    ApprovedDate = System.DateTime.Today,
                };
                if (aPromotionBLL.ApprovalUpdateBLL(aPromotion))
                {
                    if (aPromotion.ActionStatus != "Cancel")
                    {
                        EmpGeneralInfo aEmpGeneralInfo = new EmpGeneralInfo();
                        aEmpGeneralInfo.EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString());
                        aEmpGeneralInfo.DesigId = Convert.ToInt32(loadGridView.DataKeys[i][2].ToString());
                        aEmpGeneralInfo.SalScaleId = Convert.ToInt32(loadGridView.DataKeys[i][3].ToString());
                        aEmpGeneralInfo.EmpGradeId = Convert.ToInt32(loadGridView.DataKeys[i][4].ToString());

                        aPromotionBLL.PlaceEmpStatus(aEmpGeneralInfo);

                        showMessageBox("Data Accepted Successfully !!");
                    }
                    else
                    {
                        showMessageBox("Data "+actionRadioButtonList.SelectedItem.Text+" Successfully");
                    }
                }
                
            }
        } 
    }
}