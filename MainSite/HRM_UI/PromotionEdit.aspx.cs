﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_PromotionEdit : System.Web.UI.Page
{
    PromotionBLL _aPromotionBLL = new PromotionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
            promotionIdHiddenField.Value = Request.QueryString["ID"];
            EmpPromotionLoad(promotionIdHiddenField.Value);
        }
    }
    public void DropDownList()
    {
         _aPromotionBLL.LoadGradeNameToDropDownBLL(empgradeDownList);
        _aPromotionBLL.LoadDesignationToDropDownBLL(desigDropDownList);
        _aPromotionBLL.LoadSalGradeNameToDropDownBLL(salscaleDropDownList);

    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }

        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }

        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Promotion aPromotion = new Promotion();
            aPromotion.PromotionId = Convert.ToInt32(promotionIdHiddenField.Value);
            aPromotion.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aPromotion.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
            aPromotion.DesigId = Convert.ToInt32(desigIdHiddenField.Value);
            aPromotion.NewDesigId = Convert.ToInt32(desigDropDownList.SelectedValue);
            aPromotion.SalScaleId = Convert.ToInt32(salscaleIdHiddenField.Value);
            aPromotion.NewSalScaleId = Convert.ToInt32(salscaleDropDownList.SelectedValue);
            aPromotion.NewGradeId = Convert.ToInt32(empgradeDownList.SelectedValue);
            aPromotion.DivisionId = Convert.ToInt32(divIdHiddenField.Value);


            if (_aPromotionBLL.UpdateDataForPromotion(aPromotion))
            {
                showMessageBox("Data Update Successfully ");
                
            }
            else
            {
                showMessageBox(" Data Not Update !!!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpPromotionLoad(string PromotionId)
    {
        Promotion aPromotion = new Promotion();
        aPromotion = _aPromotionBLL.PromotionEditLoad(PromotionId);
        effectDateTexBox.Text = aPromotion.EffectiveDate.ToString("dd-MMM-yyyy");
        EmpInfoIdHiddenField.Value = aPromotion.EmpInfoId.ToString();
        desigIdHiddenField.Value= aPromotion.DesigId.ToString();
        salscaleIdHiddenField.Value= aPromotion.SalScaleId.ToString();
        gradeIdHiddenField.Value = aPromotion.GradeId.ToString();
        desigDropDownList.SelectedValue= aPromotion.NewDesigId.ToString();
        salscaleDropDownList.SelectedValue= aPromotion.NewSalScaleId.ToString();
        empgradeDownList.SelectedValue = aPromotion.NewGradeId.ToString();
        GetEmpMasterCode(EmpInfoIdHiddenField.Value);
        GetEmpInfo();
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aPromotionBLL.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                EmpMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }  
    }

    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        GetEmpInfo();
    }

    public void GetEmpInfo()
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aPromotionBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                salscaleLabel.Text = aTable.Rows[0]["SalScaleName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                departNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                sectionNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                gradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                salscaleIdHiddenField.Value = aTable.Rows[0]["SalScaleId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                divitionNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
}