﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_PromotionEntry : System.Web.UI.Page
{
    PromotionBLL _aPromotionBLL=new PromotionBLL(); 
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        effectDateTexBox.Text = string.Empty;
        desigIdHiddenField.Value = null;
        salscaleLabel.Text = string.Empty;
        salscaleDropDownList.SelectedValue = null;
        desigNameLabel.Text = string.Empty;
        desigDropDownList.SelectedValue = null;
        gradeDropDownList.SelectedValue = null;
        departmentNameLabel1.Text = string.Empty;
        sectionNameLabel.Text = string.Empty;
        divitionNameLabel.Text = string.Empty;
        gradeLabel.Text = string.Empty;
        
    }
    //protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    //_aPromotionBLL.LoadDesignationToDropDownBLL(desigDropDownList,empgradeDownList.SelectedValue);
    //}
    
    
    public void DropDownList()
    {
        _aPromotionBLL.LoadGradeNameToDropDownBLL(gradeDropDownList);
        _aPromotionBLL.LoadDesignationToDropDownBLL(desigDropDownList);
        _aPromotionBLL.LoadSalGradeNameToDropDownBLL(salscaleDropDownList);
        
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Promotion aPromotion = new Promotion();
            aPromotion.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aPromotion.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
            aPromotion.EntryUser = Session["LoginName"].ToString();
            aPromotion.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            aPromotion.ActionStatus = "Posted";
            aPromotion.DesigId = Convert.ToInt32(desigIdHiddenField.Value);
            aPromotion.NewDesigId = Convert.ToInt32(desigDropDownList.SelectedValue);
            aPromotion.SalScaleId = Convert.ToInt32(salscaleIdHiddenField.Value);
            aPromotion.NewSalScaleId = Convert.ToInt32(salscaleDropDownList.SelectedValue);
            aPromotion.GradeId = Convert.ToInt32(gradeIdHiddenField.Value);
            aPromotion.NewGradeId = Convert.ToInt32(gradeDropDownList.SelectedValue);
            aPromotion.IsActive = true;
            aPromotion.CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value);
            aPromotion.UnitId = Convert.ToInt32(unitIdHiddenField.Value);
            aPromotion.DivisionId = Convert.ToInt32(divIdHiddenField.Value);
            aPromotion.DeptId = Convert.ToInt32(deptIdHiddenField.Value);
            aPromotion.SectionId = Convert.ToInt32(secIdHiddenField.Value);
            aPromotion.Remarks = "Only Promotion";
            
            if (_aPromotionBLL.SaveDataForPromotion(aPromotion))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox(" Promotion already Posted !!!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("PromotionView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aPromotionBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                salscaleLabel.Text = aTable.Rows[0]["SalScaleName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                divitionNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                departmentNameLabel1.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                sectionNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                gradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                salscaleIdHiddenField.Value = aTable.Rows[0]["SalScaleId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                gradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();

                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}