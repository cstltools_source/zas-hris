﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_TransferView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
  PromotionBLL aBll = new PromotionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EmpTransferLoad();
        }
    }

    private void EmpTransferLoad()
    {
        aDataTable = aBll.LoadPromotion();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        aBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }
    
    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        EmpTransferLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("PromotionEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "PromotionEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(900/2);var Mtop = (screen.height/2)-(1050/2);window.open( '" + url + "', null, 'height=1000,width=1200,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string TransferId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(TransferId);
        }
    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[5].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string TransferId = loadGridView.DataKeys[i][0].ToString();
                aBll.DeleteDataBLL(TransferId);
            }
            EmpTransferLoad();
        }

    }
}