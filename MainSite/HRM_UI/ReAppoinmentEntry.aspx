﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="ReAppoinmentEntry.aspx.cs" Inherits="HRM_UI_ReAppointmentEntry" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       
    
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Reappointment Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Daily Tasks </a></li>
                            <li class="breadcrumb-item"><a href="#">Reappointment Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group">
                                    <label>Effective Date </label>
                                    <div class="input-group date pull-left" id="daterangepicker1">
                                        <asp:TextBox ID="effectDateTexBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom"
                                            TargetControlID="effectDateTexBox" />
                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                            <span>
                                                <asp:ImageButton ID="ImageButton1" runat="server"
                                                    AlternateText="Click to show calendar"
                                                     ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Master Code </label>
                                        <asp:TextBox ID="EmpMasterCodeTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                                
                                <div class="col-1">
                                    <div class="form-group">
                                        <label style="color: white">Search </label> <br />
                                        <asp:Button ID="searchButton" runat="server" CssClass="btn btn-sm btn-info" onclick="searchButton_Click" Text="Search" />
                                    </div>
                                </div>
                              
                                <div class="col-3">
                                    <div class="form-group">
                                        <label> Employee Name </label>
                                        <asp:TextBox ID="empNameTexBox" CssClass="form-control form-control-sm" ReadOnly="True" runat="server"></asp:TextBox>
                                        <asp:HiddenField ID="EmpInfoIdHiddenField" runat="server" />                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label> Company Name </label>
                                         <asp:TextBox ID="comNameTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="comIdHiddenField" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label> Unit Name </label>
                                         <asp:TextBox ID="unitNameTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="unitIdHiddenField" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label> Division Name </label>
                                         <asp:TextBox ID="divNameTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="divIdHiddenField" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label> Department Name </label>
                                         <asp:TextBox ID="deptNameTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="deptIdHiddenField" runat="server" />
                                    </div>
                                </div>
                            </div>
                              <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label> Section Name </label>
                                         <asp:TextBox ID="secNameTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="secIdHiddenField" runat="server" />
                                    </div>
                                </div>
                              <div class="col-3">
                                    <div class="form-group">
                                        <label>Designation Name </label>
                                         <asp:TextBox ID="desigNameTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="desigIdHiddenField" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Grade :</label>
                                         <asp:TextBox ID="empGradeTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="GradeIdHiddenField" runat="server" />
                                    </div>
                                </div>
                                
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Type :</label>
                                         <asp:TextBox ID="empTypeTexBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox> <asp:HiddenField ID="empTypeIdHiddenField" runat="server" />
                                    </div>
                                </div>
                            </div>
                            
                           <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    
    
    
    
    
     <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%" class="TableWorkArea">
                    <tr>
                        <td colspan="6" class="TableHeading">
                           Reappoinment Entry
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp; View List</td>
                        <td width="20%" class="TDRight">
                            <asp:ImageButton ID="departmentListImageButton" runat="server" 
                                ImageUrl="~/images/viewList.png" onclick="departmentListImageButton_Click" />
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            Effective Date</td>
                        <td class="TDRight" width="20%">
                            <asp:TextBox ID="effectDateTexBox" runat="server" CssClass="TextBoxCalander"></asp:TextBox>
                            <asp:ImageButton runat="server" AlternateText="Click to show calendar" ImageUrl="~/Images/Calendar_scheduleHS.png"
                                    TabIndex="4" ID="imgDate"></asp:ImageButton>
                                    <asp:CalendarExtender ID="effectDate" runat="server" Format="dd-MMM-yyyy" TargetControlID="effectDateTexBox"
                                     PopupButtonID="imgDate">
                                   </asp:CalendarExtender></td>
                       
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                        <td width="13%" class="TDLeft">
                            Employee Code</td>
                        <td width="20%" class="TDRight">
                            <asp:TextBox ID="EmpMasterCodeTextBox" runat="server" CssClass="TextBox" 
                                ontextchanged="EmpMasterCodeTextBox_TextChanged"></asp:TextBox>
                        </td>
                        <td width="13%" class="TDLeft">
                            <asp:Button ID="searchButton" runat="server" onclick="searchButton_Click" 
                                Text="Search" />
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            Employee&nbsp; Name</td>
                        <td class="TDRight" width="20%">
                            <asp:TextBox ID="empNameTexBox" runat="server" CssClass="TextBox"></asp:TextBox>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="comIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Company Name</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="comNameLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="unitIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Unit Name</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="unitNameLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="divIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Division Name</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="divNameLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="deptIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Department Name</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="deptNameLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="secIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Section Name</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="secNameLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="desigIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Designation</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="desigNameLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="GradeIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Employee Grade</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="empGradeLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            <asp:HiddenField ID="empTypeIdHiddenField" runat="server" />
                        </td>
                        <td class="TDLeft" width="13%">
                            Employee Type</td>
                        <td class="TDRight" width="20%">
                            <asp:Label ID="empTypeLabel" runat="server"></asp:Label>
                        </td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                       
                        <td class="TDLeft" width="13%">
                            &nbsp;</td>
                        <td class="TDRight" width="20%">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;<asp:HiddenField ID="EmpInfoIdHiddenField" runat="server" />
                        </td>
                        <td width="13%" class="TDLeft">
                            
                            &nbsp;</td>
                        <td width="20%" class="TDRight">
                            <asp:Button ID="submitButton" runat="server" Text="Submit" 
                                onclick="submitButton_Click" />
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>--%>

</asp:Content>

