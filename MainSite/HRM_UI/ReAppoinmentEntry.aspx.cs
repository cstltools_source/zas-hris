﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_ReAppointmentEntry : System.Web.UI.Page
{
    ReAppoinmentBLL _aReAppoinmentBLL=new ReAppoinmentBLL(); 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        effectDateTexBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        GradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameTexBox.Text = string.Empty;
        unitNameTexBox.Text = string.Empty;
        divNameTexBox.Text = string.Empty;
        deptNameTexBox.Text = string.Empty;
        secNameTexBox.Text = string.Empty;
        desigNameTexBox.Text = string.Empty;
        empGradeTexBox.Text = string.Empty;
        empTypeTexBox.Text = string.Empty;
        
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            ReAppointment aReAppointment = new ReAppointment();
            aReAppointment.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aReAppointment.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
            aReAppointment.CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value);
            aReAppointment.UnitId = Convert.ToInt32(unitIdHiddenField.Value);
            aReAppointment.DivisionId = Convert.ToInt32(divIdHiddenField.Value);
            aReAppointment.DeptId = Convert.ToInt32(deptIdHiddenField.Value);
            aReAppointment.SectionId = Convert.ToInt32(secIdHiddenField.Value);
            aReAppointment.DesigId = Convert.ToInt32(desigIdHiddenField.Value);
            aReAppointment.GradeId = Convert.ToInt32(GradeIdHiddenField.Value);
            aReAppointment.EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value);
            aReAppointment.EntryUser = Session["LoginName"].ToString();
            aReAppointment.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            aReAppointment.ActionStatus = "Posted";
            aReAppointment.IsActive = true;  
            
            if (_aReAppoinmentBLL.SaveDataForReAppointment(aReAppointment))
            {
                EmpGeneralInfo aEmpGeneralInfo=new EmpGeneralInfo();
                aEmpGeneralInfo.EmployeeStatus = "Active";
                aEmpGeneralInfo.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
                if (_aReAppoinmentBLL.EmployeeStatus(aEmpGeneralInfo))
                {
                    showMessageBox("Data Save Successfully ");
                    Clear();    
                }
                
            }
            else
            {
                showMessageBox(" Salary Benefit already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("ReAppointmentView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        DataTable aTable = new DataTable();
        aTable = _aReAppoinmentBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

        if (_aReAppoinmentBLL.HasjobLeft(aTable.Rows[0]["EmpInfoId"].ToString().Trim()))
        {
            if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameTexBox.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameTexBox.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameTexBox.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameTexBox.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameTexBox.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameTexBox.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeTexBox.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeTexBox.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                GradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
        }
         else
        {
            showMessageBox("This Employee Never Left the Job!!");
        }
        
    }

    protected void EmpMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {

    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}