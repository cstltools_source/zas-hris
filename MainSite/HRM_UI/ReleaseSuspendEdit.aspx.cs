﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ReleaseSuspendEdit : System.Web.UI.Page
{
    ReleaseSuspendBLL _aReleaseSuspendBLL = new ReleaseSuspendBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ReleaseSuspendIdHiddenField.Value = Request.QueryString["ID"];
            EmpReleaseSuspendLoad(ReleaseSuspendIdHiddenField.Value);
        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }

        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            ReleaseSuspend aReleaseSuspend = new ReleaseSuspend()
            {
                RelsSuspendId = Convert.ToInt32(ReleaseSuspendIdHiddenField.Value),
                EmpInfoId = Convert.ToInt32(empIdHiddenField.Value),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),

                CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value),
                UnitId = Convert.ToInt32(unitIdHiddenField.Value),
                DivisionId = Convert.ToInt32(divIdHiddenField.Value),
                DeptId = Convert.ToInt32(deptIdHiddenField.Value),
                SectionId = Convert.ToInt32(secIdHiddenField.Value),
                DesigId = Convert.ToInt32(desigIdHiddenField.Value),
                GradeId = Convert.ToInt32(empGradeIdHiddenField.Value),
                EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value),
                ActionStatus = "Posted",
                EntryBy = Session["LoginName"].ToString(),
                EntryDate = System.DateTime.Today,

            };

            if (!_aReleaseSuspendBLL.UpdateDataForEmpSalBenefit(aReleaseSuspend))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpReleaseSuspendLoad(string ReleaseSuspendId)
    {
        ReleaseSuspend aReleaseSuspend = new ReleaseSuspend();
        aReleaseSuspend = _aReleaseSuspendBLL.EmpSalBenefitEditLoad(ReleaseSuspendId);
        effectDateTexBox.Text = aReleaseSuspend.EffectiveDate.ToString("dd-MMM-yyyy");
        comIdHiddenField.Value = aReleaseSuspend.CompanyInfoId.ToString();
        unitIdHiddenField.Value = aReleaseSuspend.UnitId.ToString();
        divIdHiddenField.Value = aReleaseSuspend.DivisionId.ToString();
        deptIdHiddenField.Value = aReleaseSuspend.DeptId.ToString();
        secIdHiddenField.Value = aReleaseSuspend.SectionId.ToString();
        desigIdHiddenField.Value = aReleaseSuspend.DesigId.ToString();
        empGradeIdHiddenField.Value = aReleaseSuspend.GradeId.ToString();
        empTypeIdHiddenField.Value = aReleaseSuspend.EmpTypeId.ToString();
        empIdHiddenField.Value = aReleaseSuspend.EmpInfoId.ToString();


        GetEmpMasterCode(empIdHiddenField.Value);
        GetNames(comIdHiddenField.Value, unitIdHiddenField.Value, divIdHiddenField.Value, deptIdHiddenField.Value, desigIdHiddenField.Value, secIdHiddenField.Value, empGradeIdHiddenField.Value, empTypeIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aReleaseSuspendBLL.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                EmpMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    public void GetNames(string comId, string unitId, string divId, string deptId, string desigId, string secId, string gradeId, string emptId)
    {

        DataTable aTableCom = new DataTable();
        DataTable aTableUnit = new DataTable();
        DataTable aTableCDiv = new DataTable();
        DataTable aTableDept = new DataTable();
        DataTable aTableDesig = new DataTable();
        DataTable aTableSec = new DataTable();
        DataTable aTableGrade = new DataTable();
        DataTable aTableType = new DataTable();
        aTableCom = _aReleaseSuspendBLL.LoadCompanyInfo(comId);
        aTableUnit = _aReleaseSuspendBLL.LoadUnit(unitId);
        aTableCDiv = _aReleaseSuspendBLL.Loadivision(divId);
        aTableDept = _aReleaseSuspendBLL.LoadDepartment(deptId);
        aTableDesig = _aReleaseSuspendBLL.LoadDesignation(desigId);
        aTableSec = _aReleaseSuspendBLL.LoadSection(secId);
        aTableGrade = _aReleaseSuspendBLL.LoadGrade(gradeId);
        aTableType = _aReleaseSuspendBLL.LoadEmpType(emptId);


        if (aTableCom.Rows.Count > 0)
        {
            comNameLabel.Text = aTableCom.Rows[0]["CompanyName"].ToString().Trim();
        }
        if (aTableUnit.Rows.Count > 0)
        {
            unitNameLabel.Text = aTableUnit.Rows[0]["UnitName"].ToString().Trim();
        }

        if (aTableCDiv.Rows.Count > 0)
        {
            divNameLabel.Text = aTableCDiv.Rows[0]["DivName"].ToString().Trim();
        }
        if (aTableDept.Rows.Count > 0)
        {
            deptNameLabel.Text = aTableDept.Rows[0]["DeptName"].ToString().Trim();
        }
        if (aTableDesig.Rows.Count > 0)
        {
            desigNameLabel.Text = aTableDesig.Rows[0]["DesigName"].ToString().Trim();
        }
        if (aTableSec.Rows.Count > 0)
        {
            secNameLabel.Text = aTableSec.Rows[0]["SectionName"].ToString().Trim();
        }
        if (aTableGrade.Rows.Count > 0)
        {
            empGradeLabel.Text = aTableGrade.Rows[0]["GradeName"].ToString().Trim();
        }
        if (aTableType.Rows.Count > 0)
        {
            empTypeLabel.Text = aTableType.Rows[0]["EmpType"].ToString().Trim();
        }

    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}