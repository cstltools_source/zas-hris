﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_ReleaseSuspendEntry : System.Web.UI.Page
{
    ReleaseSuspendBLL _aReleaseSuspendBLL=new ReleaseSuspendBLL(); 
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        effectDateTexBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        empGradeIdHiddenField.Value = null;
        empTypeIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        empGradeLabel.Text = string.Empty;
        empTypeLabel.Text = string.Empty;
        
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (Convert.ToDateTime(effectDateTexBox.Text) < Convert.ToDateTime(susDateLabel.Text))
        {
            showMessageBox("Please change Wrong effect date !!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            ReleaseSuspend aReleaseSuspend = new ReleaseSuspend();
            aReleaseSuspend.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aReleaseSuspend.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
            aReleaseSuspend.Status = "Inactive";
            aReleaseSuspend.CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value);
            aReleaseSuspend.UnitId = Convert.ToInt32(unitIdHiddenField.Value);
            aReleaseSuspend.DivisionId = Convert.ToInt32(divIdHiddenField.Value);
            aReleaseSuspend.DeptId = Convert.ToInt32(deptIdHiddenField.Value);
            aReleaseSuspend.SectionId = Convert.ToInt32(secIdHiddenField.Value);
            aReleaseSuspend.DesigId = Convert.ToInt32(desigIdHiddenField.Value);
            aReleaseSuspend.GradeId = Convert.ToInt32(empGradeIdHiddenField.Value);
            aReleaseSuspend.EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value);
            aReleaseSuspend.EntryBy = Session["LoginName"].ToString();
            aReleaseSuspend.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            aReleaseSuspend.ActionStatus = "Posted";
            aReleaseSuspend.IsActive = true;
            aReleaseSuspend.SuspendId = Convert.ToInt32(susIdHiddenField.Value);
            aReleaseSuspend.SuspendDate = Convert.ToDateTime(susDateLabel.Text);
            
            if (_aReleaseSuspendBLL.SaveDataForReleaseSuspend(aReleaseSuspend))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox(" Suspand Release already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("ReleaseSuspendView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aReleaseSuspendBLL.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                empGradeLabel.Text = aTable.Rows[0]["GradeName"].ToString().Trim();
                empTypeLabel.Text = aTable.Rows[0]["EmpType"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
                empGradeIdHiddenField.Value = aTable.Rows[0]["GradeId"].ToString().Trim();
                empTypeIdHiddenField.Value = aTable.Rows[0]["EmpTypeId"].ToString().Trim();
                susIdHiddenField.Value = aTable.Rows[0]["SuspendId"].ToString().Trim();
                susDateLabel.Text = Convert.ToDateTime(aTable.Rows[0]["EffectiveDate"].ToString().Trim()).ToString("dd-MMM-yyyy");
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}