﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_RestLeaveApproval : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    RestLeaveBLL aRestLeaveBll = new RestLeaveBLL();
    AttedanceOperationBLL attedanceOperationBll = new AttedanceOperationBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            RestLeaveLoad();
        }
    }
    private void RestLeaveLoad()
    {
        string filename = Path.GetFileName(Request.Path);
        string userName = Session["LoginName"].ToString();
        RestLeaveLoadByCondition(filename, userName);
        aRestLeaveBll.LoadApprovalControlBLL(actionRadioButtonList, filename, userName);

    }
    private void RestLeaveLoadByCondition(string pageName, string user)
    {
        string ActionStatus = aRestLeaveBll.LoadForApprovalConditionBLL(pageName, user);
        aDataTable = aRestLeaveBll.LoadRestLeaveViewForApproval(ActionStatus);
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    
    
    private void PopUp(string Id)
    {
        string url = "RestLeaveEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ondutyId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(ondutyId);
        }

    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox ChkBoxHeader = (CheckBox)loadGridView.HeaderRow.FindControl("chkSelectAll");

       
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {
                CheckBox ChkBoxRows = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
                if (ChkBoxHeader.Checked == true)
                {
                    ChkBoxRows.Checked = true;
                }
                else
                {
                    ChkBoxRows.Checked = false;
                }
            }
      
       
       
    }
    public bool Validation()
    {
        int c = 0, i;
        for (i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox cb = (CheckBox)loadGridView.Rows[i].FindControl("chkSelect");
            if (cb.Checked != true)
            {
                c++;

            }
        }
        if (c == i)
        {
            showMessageBox("Choose check box ");
            return false;
        }
        return true;
    }
    protected void btnSubmit0_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            try
            {
                SubminApproval();
                RestLeaveLoad();
            }
            catch (Exception)
            {

                showMessageBox("choose data properly "); 
            }
        }
        
    }


    private void SubminApproval()
    {
        if (actionRadioButtonList.SelectedValue == null)
        {
            showMessageBox("Choose One of the Operations ");
        }
        else
        {
            for (int i = 0; i < loadGridView.Rows.Count; i++)
            {

                CheckBox cb = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkSelect");
                if (cb.Checked == true)
                {
                    RestLeave aRestLeave = new RestLeave()
                    {
                        RestLeaveId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                        EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][1].ToString()),
                        FromDate = Convert.ToDateTime(loadGridView.Rows[i].Cells[3].Text),
                        ToDate = Convert.ToDateTime(loadGridView.Rows[i].Cells[4].Text),
                        ActionStatus = actionRadioButtonList.SelectedItem.Text,
                        ApprovedUser = Session["LoginName"].ToString(),
                        ApprovedDate = System.DateTime.Today,
                    };
                    if (aRestLeaveBll.ApprovalUpdateBLL(aRestLeave))
                    {
                        if (aRestLeave.ActionStatus == "Accepted")
                        {


                            DateTime dtStart = Convert.ToDateTime(loadGridView.Rows[i].Cells[3].Text);
                            DateTime dtEnd = Convert.ToDateTime(loadGridView.Rows[i].Cells[4].Text);
                            while (dtStart <= dtEnd)
                            {

                                attedanceOperationBll.UpdateOperationBLL(dtStart, aRestLeave.EmpInfoId, "LV", "Rest Leave");
                                dtStart = dtStart.AddDays(1);
                            }
                            showMessageBox("Data Accepted Successfully !!");
                        }
                        else
                        {
                            showMessageBox("Data " + actionRadioButtonList.SelectedItem.Text + " Successfully");
                        }
                    }
                }


            }
        }
         

    }
}