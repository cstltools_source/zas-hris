﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_RestLeaveEdit : System.Web.UI.Page
{
    RestLeaveBLL aDutyBll = new RestLeaveBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ondutyIdHiddenField.Value = Request.QueryString["ID"];
            OnDutyLoad(ondutyIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (fromDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (toDateTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (purposTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
       
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            RestLeave aOnDuty = new RestLeave()
            {
                EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                RestLeaveId = Convert.ToInt32(ondutyIdHiddenField.Value),
                FromDate = Convert.ToDateTime(fromDateTextBox.Text),
                ToDate = Convert.ToDateTime(toDateTextBox.Text),
                EntryDate = System.DateTime.Today,
                Purpose = purposTextBox.Text,
                ActionRemarks = remarksTextBox.Text
                
            };
            if (!aDutyBll.UpdateDataForOnDuty(aOnDuty))
            {
                showMessageBox("Data did not Update !!!!");
                
            }
            else
            {
                showMessageBox("Data update successfully , Please reload !!!");
            }

        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    private void OnDutyLoad(string ondutyId)
    {
        RestLeave aOnDuty = new RestLeave();
        aOnDuty = aDutyBll.OnDutyEditLoad(ondutyId);
        fromDateTextBox.Text = aOnDuty.FromDate.ToString("dd-MMM-yyyy");
        toDateTextBox.Text = aOnDuty.ToDate.ToString("dd-MMM-yyyy");
        empInfoIdHiddenField.Value = aOnDuty.EmpInfoId.ToString();
        purposTextBox.Text = aOnDuty.Purpose.ToString();
        remarksTextBox.Text = aOnDuty.ActionRemarks.ToString();
        GetEmpMasterCode(empInfoIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string empid)
    {
        if (!string.IsNullOrEmpty(empid))
        {
            DataTable aTable = new DataTable();
            aTable = aDutyBll.LoadEmpInfoCode(empid);

            if (aTable.Rows.Count > 0)
            {
                empMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {

    }
}