﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="RestLeaveEntry.aspx.cs" Inherits="HRM_UI_RestLeaveEntry" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Rest Leave Entry</h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="jobViewImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Attendence Operation </a></li>
                            <li class="breadcrumb-item"><a href="#">Rest Leave Entry </a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-2">
                                        <div class="form-group">
                                            <label>Type </label>
                                            <asp:DropDownList ID="typeDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="typeDropDownList_SelectedIndexChanged">
                                                <asp:ListItem> Select any one </asp:ListItem>
                                                <asp:ListItem Value="SE">Single Employee</asp:ListItem>
                                                <asp:ListItem Value="GWE">Group Wise Employee</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                            </div>
                            

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>From Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker113">
                                            <asp:TextBox ID="fromDateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom" PopupPosition="TopLeft"
                                                TargetControlID="fromDateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton3" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>To Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker12">
                                            <asp:TextBox ID="toDateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                Format="dd-MMM-yyyy" PopupButtonID="ImageButton4" CssClass="custom"
                                                TargetControlID="toDateTextBox" />
                                            <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                <span>
                                                    <asp:ImageButton ID="ImageButton4" runat="server"
                                                        AlternateText="Click to show calendar"
                                                         ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-2">
                                        <div class="form-group">
                                            <label>Purpose </label>
                                            <asp:TextBox ID="purposTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                            </div>
                            <div id="divgroup" runat="server" visible="True">
                                <div class="form-row">
                                    
                                   <div class="col-2">
                                        <div class="form-group">
                                            <label>Group</label>
                                            <asp:DropDownList ID="groupDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div id="divemp" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Code </label>
                                            <asp:TextBox ID="empMasterCodeTextBox" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True" OnTextChanged="empMasterCodeTextBox_TextChanged"></asp:TextBox>
                                            <asp:HiddenField ID="empInfoIdHiddenField" runat="server" />
                                        </div>
                                    </div>
                                
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Name </label>
                                            <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                            <br />
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

