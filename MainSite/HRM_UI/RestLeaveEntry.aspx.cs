﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_RestLeaveEntry : System.Web.UI.Page
{
    RestLeaveBLL aDutyBll = new RestLeaveBLL();
    private GroupBLL aGroupBll = new GroupBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           LoadDropDown();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        empMasterCodeTextBox.Text = string.Empty;
        empNameTextBox.Text = string.Empty;
        purposTextBox.Text = string.Empty;
        fromDateTextBox.Text = string.Empty;
        toDateTextBox.Text = string.Empty;
     
    }
    private void LoadDropDown()
    {
        aGroupBll.LoadGroup(groupDropDownList);
        
    }
    public bool RestLeaveFromDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(fromDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool RestLeavetoDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(toDateTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
     
        if (purposTextBox.Text == "")
        {
            showMessageBox("Please Select Purpose !!");
            return false;
        }
        if (RestLeaveFromDate() == false)
        {
            showMessageBox("Please give a valid RestLeave Date !!!");
            return false;
        }
        if (RestLeavetoDate() == false)
        {
            showMessageBox("Please give a valid RestLeave Date !!!");
            return false;
        }
        return true;
    }
    
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
       {
           if (typeDropDownList.SelectedValue=="SE")
           {
               RestLeave aRestLeave = new RestLeave()
               {
                   EmpInfoId = Convert.ToInt32(empInfoIdHiddenField.Value),
                   Purpose = purposTextBox.Text,
                   ActionStatus = "Posted",
                   ActionRemarks = "",
                   EntryUser = Session["LoginName"].ToString(),
                   EntryDate = System.DateTime.Today,
                   FromDate = Convert.ToDateTime(fromDateTextBox.Text),
                   ToDate = Convert.ToDateTime(toDateTextBox.Text),
                   IsActive = true,
               };
               if (aDutyBll.SaveDataForRestLeave(aRestLeave))
               {
                   showMessageBox("Data Save Successfully");
                   Clear();
               }   
           }
           if (typeDropDownList.SelectedValue=="GWE")
           {
               DataTable dtGroupWiseEmp = aDutyBll.LoadGroupWiseEmp(groupDropDownList.SelectedValue);
               foreach (DataRow dtGroupEmp in dtGroupWiseEmp.Rows)
               {
                   RestLeave aRestLeave = new RestLeave();

                   aRestLeave.EmpInfoId = Convert.ToInt32(dtGroupEmp["EmpInfoId"].ToString());
                   aRestLeave.Purpose = purposTextBox.Text;
                   aRestLeave.ActionStatus = "Posted";
                   aRestLeave.ActionRemarks = "";
                   aRestLeave.EntryUser = Session["LoginName"].ToString();
                   aRestLeave.EntryDate = System.DateTime.Today;
                   aRestLeave.FromDate = Convert.ToDateTime(fromDateTextBox.Text);
                   aRestLeave.ToDate = Convert.ToDateTime(toDateTextBox.Text);
                   aRestLeave.IsActive = true;
                   if (aDutyBll.SaveDataForRestLeave(aRestLeave))
                   {
                       
                   }       
               }
               showMessageBox("Data Save Successfully");
               Clear();               
           }        
           
        }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    protected void jobViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("RestLeaveView.aspx");
    }

    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = aDutyBll.LoadEmpInfo(empMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                empInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void typeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (typeDropDownList.SelectedValue=="SE")
        {
            divemp.Visible = true;
            divgroup.Visible = false;
        }
        if (typeDropDownList.SelectedValue=="GWE")
        {
            divemp.Visible = false;
            divgroup.Visible = true;
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
       Clear();
    }
}