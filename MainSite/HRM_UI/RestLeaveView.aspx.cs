﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_RestLeaveView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    RestLeaveBLL aRestLeaveBll = new RestLeaveBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadRestLeave();
        }
    }

    private void LoadRestLeave()
    {
        aDataTable = aRestLeaveBll.LoadRestLeaveView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        aRestLeaveBll.CancelDataMarkBLL(loadGridView, aDataTable);
    }
    
    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        LoadRestLeave();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("RestLeaveEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "RestLeaveEdit.aspx?ID=" + Id;
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=800,width=900,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ondutyId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(ondutyId);
        }

    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[7].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string OnDutyId = loadGridView.DataKeys[i][0].ToString();
                aRestLeaveBll.DeleteDataBLL(OnDutyId);
            }
            LoadRestLeave();
        }
    } 
}