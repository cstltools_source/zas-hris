﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_RptHeaderEntry : System.Web.UI.Page
{
    RptHeadingBLL aHeadingBll=new RptHeadingBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        rptHeaderTextBox.Text = string.Empty;
        rptAddressTextBox.Text = string.Empty;
        rptTelTextBox.Text = string.Empty;
        rptFaxTextBox.Text = string.Empty;
        rptEmailTextBox.Text = string.Empty;
        rptMessageTextBox.Text = string.Empty;
    }
    private bool Validation()
    {
        if (rptHeaderTextBox.Text == "")
        {
            showMessageBox("Please Input Rpt Header !!");
            return false;
        }
        if (rptAddressTextBox.Text == "")
        {
            showMessageBox("Please Input Rpt Address !!");
            return false;
        }
        if (rptTelTextBox.Text == "")
        {
            showMessageBox("Please Input Rpt Telephone!!");
            return false;
        }
        if (rptFaxTextBox.Text == "")
        {
            showMessageBox("Please Input Rpt Fax !!");
            return false;
        }
        if (rptEmailTextBox.Text == "")
        {
            showMessageBox("Please Input Rpt Email !!");
            return false;
        }
        if (rptMessageTextBox.Text == "")
        {
            showMessageBox("Please Input Rpt Message !!");
            return false;
        }
        return true;
    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            int rptId = 0;
            RptHeader aHeader = new RptHeader()
                                    {
                                        RptHeading = rptHeaderTextBox.Text,
                                        RptAddress = rptAddressTextBox.Text,
                                        RptEmail = rptEmailTextBox.Text,
                                        RptFax = rptFaxTextBox.Text,
                                        RptMessage = rptMessageTextBox.Text,
                                        RptTel = rptTelTextBox.Text
                                    };
            if (aHeadingBll.SaveRptHeaderData(aHeader, out rptId))
            {
                showMessageBox("Data Saved Success fully");
                PopUp(rptId);
                Clear();
            }
        }
    }
    private void PopUp(int rptId)
    {
        string url = "../HRM_UI/RptImage.aspx?rptId=" + rptId;
        string fullURL = "window.open('" + url + "', '_blank', 'height=300,width=700,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    //protected void noButton_OnClick(object sender, EventArgs e)
    //{
    //    if (Validation())
    //    {
    //        int rptId = 0;
    //        RptHeader aHeader = new RptHeader()
    //        {
    //            RptHeading = rptHeaderTextBox.Text,
    //            RptAddress = rptAddressTextBox.Text,
    //            RptEmail = rptEmailTextBox.Text,
    //            RptFax = rptFaxTextBox.Text,
    //            RptMessage = rptMessageTextBox.Text,
    //            RptTel = rptTelTextBox.Text
    //        };
    //        if (aHeadingBll.SaveRptHeaderData(aHeader, out rptId))
    //        {
    //            showMessageBox("Data Saved Success fully");
    //            Clear();
    //        }
    //    }
    //}

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}