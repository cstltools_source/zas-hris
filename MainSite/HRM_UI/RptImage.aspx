﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RptImage.aspx.cs" Inherits="HRM_UI_RptImage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
         <script type="text/javascript">
             function previewFile() {
                 var preview = document.querySelector('#<%=picImage.ClientID %>');
                 var file = document.querySelector('#<%=picFileUpload.ClientID %>').files[0];
                 var reader = new FileReader();

                 reader.onloadend = function () {
                     preview.src = reader.result;
                 }

                 if (file) {
                     reader.readAsDataURL(file);
                 } else {
                     preview.src = "";
                 }
             }
    </script>
    <script src="../scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.8.19.custom.min.js" type="text/javascript"></script>
    
    <style type="text/css">
        .style1
        {
            height: 23px;
        }
    </style>
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <script type="text/javascript">
            function previewFile() {
                var preview = document.querySelector('#<%=picImage.ClientID %>');
                var file = document.querySelector('#<%=picFileUpload.ClientID %>').files[0];
                var reader = new FileReader();

                reader.onloadend = function() {
                    preview.src = reader.result;
                };

                if (file) {
                    reader.readAsDataURL(file);
                } else {
                    preview.src = "";
                }
            }
        </script>

    <table width="100%" class="TableWorkArea" bgcolor="#F5F5F5">
                    <tr>
                        <td colspan="6" class="TableHeading">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td width="13%" class="style1">
                            &nbsp;</td>
                        <td width="20%" class="style1">
                            </td>
                        <td width="13%" class="style1">
                             &nbsp;</td>
                        <td width="20%" class="TDRight" rowspan="5">
                            &nbsp;
                            &nbsp;
                            <asp:Image ID="picImage" runat="server" Height="99px" Width="103px" />
                        </td>
                        <td width="13%" class="style1">
                        </td>
                        <td width="20%" class="style1">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="20%" class="TDRight" rowspan="4">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                             <asp:FileUpload ID="picFileUpload" onchange="previewFile()"  runat="server"  name="file"/>
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;</td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                            </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft" >
                            &nbsp;
                            <asp:Button ID="saveButton" runat="server" onclick="saveButton_Click" 
                                Text="Save" />
                        </td>
                         <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                        <td width="13%" class="TDLeft">
                            &nbsp;
                        </td>
                        <td width="20%" class="TDRight">
                            &nbsp;
                        </td>
                    </tr>
                </table>
    </div>
    </form>
</body>
</html>
