﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_RptImage : System.Web.UI.Page
{
    RptImageBLL aRptImageBll=new RptImageBLL();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    public bool SaveImg()
    {
        
        if (picFileUpload.HasFile)
        {
            //if (picFileUpload.PostedFile.ContentType == "image/jpeg")
            {
                if (picFileUpload.PostedFile.ContentLength < 102400)
                {
                    string rptId = Request.QueryString["rptId"];
                    int length = picFileUpload.PostedFile.ContentLength;
                    byte[] imagebyt = new byte[length];
                    HttpPostedFile img = picFileUpload.PostedFile;
                    img.InputStream.Read(imagebyt, 0, length);
                    aRptImageBll.SaveEmpImage(imagebyt, Convert.ToInt32(rptId));
                    //using (SqlConnection sqlConnection = new SqlConnection(@"Data Source=NAZRUL-PC\SQLServer2008R2;Initial Catalog=EnergisHRM;Integrated Security=false; User Id=sa; password=123"))
                    //{
                    //    using (SqlCommand cmd = new SqlCommand())
                    //    {
                    //        cmd.CommandText = @"insert into dbo.tblRptImage (RptImage,RptId)values(@RptImage,@RptId)";
                    //        cmd.Parameters.Add("@RptImage", SqlDbType.Image).Value = imagebyt;
                    //        cmd.Parameters.Add("@RptId", SqlDbType.Int).Value = rptId;
                    //        cmd.Connection = sqlConnection;
                    //        sqlConnection.Open();

                    //        try
                    //        {
                    //            cmd.ExecuteNonQuery();
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            showMessageBox(ex.ToString());
                    //        }
                    //    }
                    //}
                }

                else
                {
                    showMessageBox("select file");
                }
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    public void Close()
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    protected void saveButton_Click(object sender, EventArgs e)
    {

        if (SaveImg())
        {
            showMessageBox("Image Saved Success fully");
            Close();
        }
    }
}