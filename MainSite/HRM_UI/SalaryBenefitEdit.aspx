﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalaryBenefitEdit.aspx.cs" Inherits="HRM_UI_SalaryBenefitEdit" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
                <div class="page-heading">
        <div class="page-heading__container">
            <div class="icon"><span class="li-register"></span></div>
            <span></span>
            <h1 class="title" style="font-size: 18px; padding-top: 9px;"> Salary Benefit Edit </h1>
        </div>
        <div class="page-heading__container float-right d-none d-sm-block">
        </div>
        <nav aria-label="breadcrumb" role="navigation">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Monthly Task </a></li>
                <li class="breadcrumb-item"><a href="#">Salary Benefit Edit</a></li>

            </ol>
        </nav>
    </div>
        <br/>
        <br/>
       <div class="container-fluid">
           <div class="card">
                <div class="card-body">
                    <div class="form-row">
                        <div class="col-3">
                            <div class="form-group">
                                <label>Effective Date </label>
                                <div class="input-group date pull-left" id="daterangepicker1">
                                    <asp:TextBox ID="effectDateTexBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                        Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom" PopupPosition="TopLeft"
                                        TargetControlID="effectDateTexBox" />
                                    <div class="input-group-addon" style="border: 1px solid #cccccc">
                                        <span>
                                            <asp:ImageButton ID="ImageButton1" runat="server"
                                                AlternateText="Click to show calendar"
                                                    ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label>Employee Master Code </label>
                                <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label>Employee Name </label>
                                <asp:TextBox ID="empNameTexBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label>Company Name </label>
                                <asp:DropDownList ID="comNameDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                onselectedindexchanged="comNameDropDownList_SelectedIndexChanged" 
                                AutoPostBack="True"> </asp:DropDownList>
                          </div>
                      </div>
                        </div>
                        <div class="form-row">
                        <div class="col-3">
                            <div class="form-group">
                                <label>Unit Name </label>
                                <asp:DropDownList ID="unitNameDropDownList" runat="server" CssClass="form-control form-control-sm" AutoPostBack="True">
                                    </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label>Division Name </label>
                                <asp:DropDownList ID="divNameDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                onselectedindexchanged="divNameDropDownList_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label>Department Name </label>
                                    <asp:DropDownList ID="deptNameDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                onselectedindexchanged="deptNameDropDownList_SelectedIndexChanged" AutoPostBack="True">
                            </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label>Section Name </label>
                                <asp:DropDownList ID="secNameDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                    AutoPostBack="True"> </asp:DropDownList>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-3">
                            <div class="form-group">
                                <label>Designation  </label>
                                    <asp:DropDownList ID="desNameDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                        AutoPostBack="True"> </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label>Employee Grade </label>
                                <asp:DropDownList ID="empGradeDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                    AutoPostBack="True"> </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="form-group">
                                <label>Salary Grade </label>
                                    <asp:DropDownList ID="salGradeDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                    AutoPostBack="True"> </asp:DropDownList>
                            </div>
                        </div>
                                
                            <div class="col-3">
                            <div class="form-group">
                                <label> Employee Type </label>
                                    <asp:DropDownList ID="empTypeDropDownList" runat="server" CssClass="form-control form-control-sm" 
                                    AutoPostBack="True"> </asp:DropDownList>
                            </div>
                        </div>
                        </div>
                    <div class="form-row">
                        <div class="col-3">
                            <div class="form-group">
                                <label>Benefit Amount </label>
                                <asp:TextBox ID="salBenefitTexBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                            </div>
                        </div>
                                
                        <div class="col-3">
                            <div class="form-group">
                                <label>Benefit Type </label>
                                <asp:TextBox ID="benefitTypeTexBox" runat="server" CssClass="form-control form-control-sm" ></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="col-6">
                            <div class="form-group">
                                <asp:Button ID="Button" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="updateButton_Click" />
                                <asp:Button ID="closelButton" Text="Close" CssClass="btn btn-sm warning" runat="server" OnClick="closelButton_OnClick" BackColor="#FF9900" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <asp:HiddenField ID="benefitIdHiddenField" runat="server" />
        <asp:HiddenField ID="empIdHiddenField" runat="server" />
        <asp:HiddenField ID="SalScaleIdHiddenField" runat="server" />
</form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>
