﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_SalaryBenefitEdit : System.Web.UI.Page
{
    SalaryBenefitBLL aSalaryBenefitBLL = new SalaryBenefitBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();
            benefitIdHiddenField.Value = Request.QueryString["ID"];
            EmpSalBenefitLoad(benefitIdHiddenField.Value);
        }
    }

    private void LoadDropDown()
    {
        aSalaryBenefitBLL.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        aSalaryBenefitBLL.LoadDivisionNameToDropDownBLL(divNameDropDownList);
        aSalaryBenefitBLL.LoadGradeNameToDropDownBLL(empGradeDropDownList);
        aSalaryBenefitBLL.LoadSalGradeNameToDropDownBLL(salGradeDropDownList);
        aSalaryBenefitBLL.LoadEmpTypeNameToDropDownBLL(empTypeDropDownList);
        aSalaryBenefitBLL.LoadDesignationToDropDownBLL(desNameDropDownList);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (salBenefitTexBox.Text == "")
        {
            showMessageBox("Please Input Salary Benefit Amount !!");
            return false;
        }
        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (benefitTypeTexBox.Text == "")
        {
            showMessageBox("Please Input Salary Benefit Type !!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            EmpSalBenefit aEmpSalBenefit = new EmpSalBenefit()
            {
                BenefitId = Convert.ToInt32(benefitIdHiddenField.Value),
                BenefitAmount = Convert.ToDecimal(salBenefitTexBox.Text),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                BenefitType = benefitTypeTexBox.Text,
                CompanyInfoId = Convert.ToInt32(comNameDropDownList.SelectedValue),
                UnitId = Convert.ToInt32(unitNameDropDownList.SelectedValue),
                DivisionId = Convert.ToInt32(divNameDropDownList.SelectedValue),
                DeptId = Convert.ToInt32(deptNameDropDownList.SelectedValue),
                SectionId = Convert.ToInt32(secNameDropDownList.SelectedValue),
                DesigId = Convert.ToInt32(desNameDropDownList.SelectedValue),
                SalScaleId = Convert.ToInt32(salGradeDropDownList.SelectedValue),
                GradeId = Convert.ToInt32(empGradeDropDownList.SelectedValue),
                EmpTypeId = Convert.ToInt32(empTypeDropDownList.SelectedValue),

                EmpInfoId = Convert.ToInt32(empIdHiddenField.Value),

            };

            if (!aSalaryBenefitBLL.UpdateDataForEmpSalBenefit(aEmpSalBenefit))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpSalBenefitLoad(string benefitId)
    {
        EmpSalBenefit aEmpSalBenefit = new EmpSalBenefit();
        aEmpSalBenefit = aSalaryBenefitBLL.EmpSalBenefitEditLoad(benefitId);
        effectDateTexBox.Text = aEmpSalBenefit.EffectiveDate.ToString("dd-MMM-yyyy");
        comNameDropDownList.SelectedValue = aEmpSalBenefit.CompanyInfoId.ToString();
        aSalaryBenefitBLL.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
        unitNameDropDownList.SelectedValue = aEmpSalBenefit.UnitId.ToString();
        divNameDropDownList.SelectedValue = aEmpSalBenefit.DivisionId.ToString();
        aSalaryBenefitBLL.LoadDepartmentToDropDownBLL(deptNameDropDownList, divNameDropDownList.SelectedValue);
        deptNameDropDownList.SelectedValue = aEmpSalBenefit.DeptId.ToString();
        aSalaryBenefitBLL.LoadSectionNameToDropDownBLL(secNameDropDownList, deptNameDropDownList.SelectedValue);
        secNameDropDownList.SelectedValue = aEmpSalBenefit.SectionId.ToString();
        desNameDropDownList.SelectedValue = aEmpSalBenefit.DesigId.ToString();
        empGradeDropDownList.SelectedValue = aEmpSalBenefit.GradeId.ToString();
        salGradeDropDownList.SelectedValue = aEmpSalBenefit.SalScaleId.ToString();
        empTypeDropDownList.SelectedValue = aEmpSalBenefit.EmpTypeId.ToString();
        empIdHiddenField.Value = aEmpSalBenefit.EmpInfoId.ToString();
        salBenefitTexBox.Text = aEmpSalBenefit.BenefitAmount.ToString();
        benefitTypeTexBox.Text = aEmpSalBenefit.BenefitType;


        GetEmpCode(empIdHiddenField.Value);
    }

    public void GetEmpCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = aSalaryBenefitBLL.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                empCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aSalaryBenefitBLL.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
    protected void deptNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aSalaryBenefitBLL.LoadSectionNameToDropDownBLL(secNameDropDownList, deptNameDropDownList.SelectedValue);
    }
    protected void divNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aSalaryBenefitBLL.LoadDepartmentToDropDownBLL(deptNameDropDownList, divNameDropDownList.SelectedValue);
    }
    
    protected void closelButton_OnClick(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }

    
}