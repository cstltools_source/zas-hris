﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="SalaryBenefitView.aspx.cs" Inherits="HRM_UI_SalaryBenefitView" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Employee Salary Benefit View </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentNewImageButton_Click" />
                        <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="deptReloadImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Salary Benefit View</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 380px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark" DataKeyNames="BenefitId"
                                    OnRowCommand="loadGridView_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                        <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                        <asp:BoundField DataField="DeptName" HeaderText="Dept Name" />
                                        <asp:BoundField DataField="BenefitType" HeaderText="Benefit Type" />
                                        <asp:BoundField DataField="BenefitAmount" HeaderText="Benefit Amount" />
                                        <asp:BoundField DataField="EffectiveDate" DataFormatString="{0:dd-MMM-yyyy}"
                                            HeaderText="Effective Date" />

                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="editImageButton" runat="server"
                                                    CommandArgument="<%# Container.DataItemIndex %>" CommandName="EditData"
                                                    ImageUrl="~/Assets/img/rsz_edit.png" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Delete">
                                            <HeaderTemplate>
                                                <asp:ImageButton ID="deleteImageButton" runat="server"
                                                    ImageUrl="~/Assets/delete-icon.png" OnClick="yesButton_Click" OnClientClick="return confirm('Do you want to Delete data ?');" />
                                              <%--  <asp:ModalPopupExtender ID="pnlModal_ModalPopupExtender" runat="server"
                                                    BackgroundCssClass="modalBackground" CancelControlID="" DropShadow="true"
                                                    Enabled="True" OkControlID="" PopupControlID="pnlModal"
                                                    TargetControlID="deleteImageButton">
                                                </asp:ModalPopupExtender>
                                                <asp:Panel ID="pnlModal" runat="server" CssClass="modalPopup" Style="display: none;">
                                                    <div class="popup_Titlebar" id="PopupHeader">
                                                        <div class="TitlebarLeft">Confirm Message</div>
                                                        <div class="TitlebarRight">
                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../Assets/delete-icon.png" />
                                                        </div>
                                                    </div>
                                                    <div class="popup_Body" align="center">
                                                        <div class="mainLeft ">
                                                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Assets/img/question.png" Width="30px" />
                                                        </div>
                                                        <div class="mainRight">
                                                            <p align="center">Are you want to Delete data ?</p>
                                                        </div>
                                                    </div>
                                                    <div class="popup_Buttons" align="center">
                                                        <div class="right_button">
                                                            <asp:Button ID="yesButton" runat="server" Text="Yes" BackColor="#1E90FF"
                                                                Width="60px" Height="30px" OnClick="yesButton_Click" />
                                                        </div>
                                                        <div class="left_button">
                                                            <asp:Button ID="noButton" runat="server" Text="No" BackColor="red" Width="60px" Height="30px" />
                                                        </div>
                                                    </div>
                                                </asp:Panel>--%>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

