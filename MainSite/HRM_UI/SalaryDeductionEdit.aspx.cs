﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_SalaryDeductionEdit : System.Web.UI.Page
{
    SalaryDeductionBLL _aSalaryDeductionBLL = new SalaryDeductionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SDIdHiddenField.Value = Request.QueryString["ID"];
            EmpSalaryDeductionLoad(SDIdHiddenField.Value);
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            SalaryDeduction aSalaryDeduction = new SalaryDeduction
            {
                SDId = Convert.ToInt32(SDIdHiddenField.Value),
                EmpId = Convert.ToInt32(empIdHiddenField.Value),
                SDEffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value),
                UnitId = Convert.ToInt32(unitIdHiddenField.Value),
                DivisionId = Convert.ToInt32(divIdHiddenField.Value),
                DeptId = Convert.ToInt32(deptIdHiddenField.Value),
                SectionId = Convert.ToInt32(secIdHiddenField.Value),
                DesigId = Convert.ToInt32(desigIdHiddenField.Value),
                GradeId = Convert.ToInt32(empGradeIdHiddenField.Value),
                EmpTypeId = Convert.ToInt32(empTypeIdHiddenField.Value),
                EntryUser = Session["LoginName"].ToString(),
                EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                SDAmount = Convert.ToDecimal(sDAmountTexBox.Text),
                SDReason = sDReasonTexBox.Text,

            };
           
            if (!_aSalaryDeductionBLL.UpdateaDataforSalaryDeduction(aSalaryDeduction))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpSalaryDeductionLoad(string SalaryDeductionId)
    {
        SalaryDeduction aSalaryDeduction = new SalaryDeduction();
        aSalaryDeduction = _aSalaryDeductionBLL.SalaryDeductionEditLoad(SalaryDeductionId);
        effectDateTexBox.Text = aSalaryDeduction.SDEffectiveDate.ToString();
        comIdHiddenField.Value= aSalaryDeduction.CompanyInfoId.ToString();
        unitIdHiddenField.Value= aSalaryDeduction.UnitId.ToString();
        divIdHiddenField.Value= aSalaryDeduction.DivisionId.ToString();
        deptIdHiddenField.Value= aSalaryDeduction.DeptId.ToString();
        secIdHiddenField.Value= aSalaryDeduction.SectionId.ToString();
        desigIdHiddenField.Value= aSalaryDeduction.DesigId.ToString();
        empGradeIdHiddenField.Value= aSalaryDeduction.GradeId.ToString();
        empTypeIdHiddenField.Value= aSalaryDeduction.EmpTypeId.ToString();
        empIdHiddenField.Value = aSalaryDeduction.EmpId.ToString();
        sDAmountTexBox.Text = aSalaryDeduction.SDAmount.ToString();
        sDReasonTexBox.Text = aSalaryDeduction.SDReason;

        GetEmpMasterCode(empIdHiddenField.Value);
        GetNames(comIdHiddenField.Value,unitIdHiddenField.Value,divIdHiddenField.Value,deptIdHiddenField.Value,desigIdHiddenField.Value,secIdHiddenField.Value,empGradeIdHiddenField.Value,empTypeIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aSalaryDeductionBLL.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                EmpMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }  
    }

    public void GetNames(string comId,string unitId,string divId,string deptId,string desigId,string secId,string gradeId,string emptId)
    {
        
            DataTable aTableCom = new DataTable();
            DataTable aTableUnit = new DataTable();
            DataTable aTableCDiv = new DataTable();
            DataTable aTableDept = new DataTable();
            DataTable aTableDesig = new DataTable();
            DataTable aTableSec = new DataTable();
            DataTable aTableGrade = new DataTable();
            DataTable aTableType = new DataTable();
            aTableCom = _aSalaryDeductionBLL.LoadCompanyInfo(comId);
            aTableUnit= _aSalaryDeductionBLL.LoadUnit(unitId);
            aTableCDiv= _aSalaryDeductionBLL.Loadivision(divId);
            aTableDept = _aSalaryDeductionBLL.LoadDepartment(deptId);
            aTableDesig = _aSalaryDeductionBLL.LoadDesignation(desigId);
            aTableSec = _aSalaryDeductionBLL.LoadSection(secId);
            aTableGrade = _aSalaryDeductionBLL.LoadGrade(gradeId);
            aTableType = _aSalaryDeductionBLL.LoadEmpType(emptId);
            

            if (aTableCom.Rows.Count > 0)
            {
                comNameLabel.Text = aTableCom.Rows[0]["CompanyName"].ToString().Trim();
            }
        if (aTableUnit.Rows.Count > 0)
        {
            unitNameLabel.Text = aTableUnit.Rows[0]["UnitName"].ToString().Trim();
        }

        if (aTableCDiv.Rows.Count > 0)
        {
            divNameLabel.Text = aTableCDiv.Rows[0]["DivName"].ToString().Trim();
        }
        if (aTableDept.Rows.Count > 0)
        {
            deptNameLabel.Text = aTableDept.Rows[0]["DeptName"].ToString().Trim();
        }
        if (aTableDesig.Rows.Count > 0)
        {
            desigNameLabel.Text = aTableDesig.Rows[0]["DesigName"].ToString().Trim();
        }
        if (aTableSec.Rows.Count > 0)
        {
            secNameLabel.Text = aTableSec.Rows[0]["SectionName"].ToString().Trim();
        }
        if (aTableGrade.Rows.Count > 0)
        {
            empGradeLabel.Text = aTableGrade.Rows[0]["GradeName"].ToString().Trim();
        }
        if (aTableType.Rows.Count > 0)
        {
            empTypeLabel.Text = aTableType.Rows[0]["EmpType"].ToString().Trim();
        }

    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}