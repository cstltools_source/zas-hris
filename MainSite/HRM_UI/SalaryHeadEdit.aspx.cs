﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_SalaryHeadEdit : System.Web.UI.Page
{
    SalaryHeadBLL aSalaryHeadBll = new SalaryHeadBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SalaryHeadType();
            salaryHeadIdHiddenField.Value = Request.QueryString["ID"];
            SalaryHeadLoad(salaryHeadIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    public void SalaryHeadType()
    {
        aSalaryHeadBll.LoadSalaryHeadType(salHeadTypeDropDownList);
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (salaryHeadNameTextBox.Text != "")
        {
            SalaryHead aSalaryHead = new SalaryHead()
            {
                SalaryHeadId = Convert.ToInt32(salaryHeadIdHiddenField.Value),
                SalaryHeadName = salaryHeadNameTextBox.Text,
                SHeadTypeId = Convert.ToInt32(salHeadTypeDropDownList.SelectedValue),
                Remarks = remarksTextBox.Text
            };
            SalaryHeadBLL aSalaryHeadBll = new SalaryHeadBLL();

            if (!aSalaryHeadBll.UpdateDataForSalaryHead(aSalaryHead))
            {
               showMessageBox("Data Not Update!!!");   
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");  
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void SalaryHeadLoad(string salaryHeadId)
    {
        SalaryHead aSalaryHead = new SalaryHead();
        aSalaryHead = aSalaryHeadBll.SalaryHeadEditLoad(salaryHeadId);
        salaryHeadNameTextBox.Text = aSalaryHead.SalaryHeadName;
        salHeadTypeDropDownList.SelectedValue = aSalaryHead.SHeadTypeId.ToString();
        remarksTextBox.Text = aSalaryHead.Remarks;
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}