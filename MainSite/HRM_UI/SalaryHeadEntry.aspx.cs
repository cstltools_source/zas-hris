﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_SalaryHeadEntry : System.Web.UI.Page
{
    SalaryHeadBLL aSalaryHeadBll = new SalaryHeadBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SalaryHeadType();
        }
    }
   
    private void Clear()
    {
        SalaryNameTextBox.Text = string.Empty;
        salHeadTypeDropDownList.SelectedValue = null;
        remarksTextBox.Text = string.Empty;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    public void SalaryHeadType()
    {
        aSalaryHeadBll.LoadSalaryHeadType(salHeadTypeDropDownList);
    }
    private bool Validation()
    {
        if (SalaryNameTextBox.Text == "")
        {
            showMessageBox("Please Input Salary Head Name !!");
            return false;
        }
        if (salHeadTypeDropDownList.Text == "")
        {
            showMessageBox("Please Input Salary Head Type !!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            SalaryHead aSalaryHead = new SalaryHead()
            {
                SalaryHeadName = SalaryNameTextBox.Text,
                SHeadTypeId = Convert.ToInt32(salHeadTypeDropDownList.SelectedValue),
                Remarks = remarksTextBox.Text,
                EntryBy = Session["LoginName"].ToString(),
                EntryDate = Convert.ToDateTime(Convert.ToDateTime(DateTime.Today.ToShortDateString()).ToString("dd-MMM-yyyy"))
            };
            if (aSalaryHeadBll.SaveForSalaryHead(aSalaryHead))
            {
                showMessageBox("Data Save Successfully EmployeeGrade Code is :    " + aSalaryHead.SalaryHeadCode + "    And  SalaryHeadName is :   " + aSalaryHead.SalaryHeadName);
                Clear();
            }
            else
            {
                showMessageBox("Salary Head Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void salaryHeadImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("SalaryHeadView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}