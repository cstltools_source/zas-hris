﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_EmployeeTypeEdit : System.Web.UI.Page
{
    SalaryHeadTypeBLL aSalaryHeadTypeBll = new SalaryHeadTypeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SHeadTypeIdHiddenField.Value = Request.QueryString["ID"];
            EmployeeTypeLoad(SHeadTypeIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (salHeadTypeTextBox.Text == "")
        {
            showMessageBox("Please Input Salary Head Type !!!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            SalaryHeadType aSalaryHeadType = new SalaryHeadType()
            {
                SHeadTypeId = Convert.ToInt32(SHeadTypeIdHiddenField.Value),
                SHeadType = salHeadTypeTextBox.Text,
            };

            if (!aSalaryHeadTypeBll.UpdateSalaryHeadType(aSalaryHeadType))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmployeeTypeLoad(string SHeadTypeId)
    {
        SalaryHeadType aSalaryHeadType = new SalaryHeadType();
        aSalaryHeadType = aSalaryHeadTypeBll.SalaryHeadTypeEditLoad(SHeadTypeId);
        salHeadTypeTextBox.Text = aSalaryHeadType.SHeadType;
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}