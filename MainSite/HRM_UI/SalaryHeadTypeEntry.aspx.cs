﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_SalaryHeadTypeEntry : System.Web.UI.Page
{
    SalaryHeadTypeBLL aSalaryHeadTypeBll = new SalaryHeadTypeBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
       salHeadTypeTextBox.Text = string.Empty;       
    }
    private bool Validation()
    {
        if (salHeadTypeTextBox.Text == "")
        {
            showMessageBox("Please Input EmployeeType !!!");
            return false;
        }
        
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            SalaryHeadType aSalaryHeadType = new SalaryHeadType()
                 {
                    SHeadType = salHeadTypeTextBox.Text
                 };
          
            if (aSalaryHeadTypeBll.SaveDataForSalaryHeadType(aSalaryHeadType))
            {
                showMessageBox("Data Save Successfully  :" );
                Clear();
            }
            else
            {
                showMessageBox("Salary Head Type Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("SalaryHeadTypeView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}