﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_SalaryHeadView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    SalaryHeadBLL aSalaryHeadBll = new SalaryHeadBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SalaryHeadLoad();
        }

    }

    private void SalaryHeadLoad()
    {
        aDataTable = aSalaryHeadBll.LoadSalaryHeadView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    private void PopUp(string Id)
    {
        string url = "SalaryHeadEdit.aspx?ID=" + Id;
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    protected void salaryHeadNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("SalaryHeadEntry.aspx");

    }
    protected void salaryHeadReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        SalaryHeadLoad();
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string departmentId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(departmentId);
        }

    }
}