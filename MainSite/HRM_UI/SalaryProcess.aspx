﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="SalaryProcess.aspx.cs" Inherits="HRM_UI_SalaryProcess" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Salary process </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">All Process </a></li>
                            <li class="breadcrumb-item"><a href="#">Salary process</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Year </label>
                                        <asp:DropDownList ID="yearDropDownList" runat="server" CssClass="form-control form-control-sm"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Month </label>
                                        <asp:DropDownList ID="monthDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            <asp:ListItem Value="1">January</asp:ListItem>
                                            <asp:ListItem Value="2">February</asp:ListItem>
                                            <asp:ListItem Value="3">March</asp:ListItem>
                                            <asp:ListItem Value="4">April</asp:ListItem>
                                            <asp:ListItem Value="5">May</asp:ListItem>
                                            <asp:ListItem Value="6">June</asp:ListItem>
                                            <asp:ListItem Value="7">July</asp:ListItem>
                                            <asp:ListItem Value="8">August</asp:ListItem>
                                            <asp:ListItem Value="9">September</asp:ListItem>
                                            <asp:ListItem Value="10">October</asp:ListItem>
                                            <asp:ListItem Value="11">November</asp:ListItem>
                                            <asp:ListItem Value="12">December</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Bank/Cash </label>
                                        <asp:DropDownList ID="bankCashDropDown" runat="server" CssClass="form-control form-control-sm">
                                            <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                            <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                            <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Salary Type </label>
                                        <asp:DropDownList ID="typeDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm" OnSelectedIndexChanged="typeDropDownList_SelectedIndexChanged">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem Value="SE">Single Employee</asp:ListItem>
                                            <asp:ListItem Value="AE">All Employee </asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Salary Id</label>
                                        <asp:DropDownList ID="salaryIdDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                            <asp:ListItem Value="1">SID-001</asp:ListItem>
                                            <asp:ListItem Value="2">SID-002</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2" id="divEmpCode" runat="server" visible="False">
                                    <div class="form-group">
                                        <label>Employee Code </label>
                                        <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                            <div id="divCommon" runat="server" visible="True">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Company Name </label>
                                            <asp:DropDownList ID="comNameDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Unit Name </label>
                                            <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Division </label>
                                            <asp:DropDownList ID="divisNamDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="divisNamDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Department </label>
                                            <asp:DropDownList ID="departmentDropDownList" runat="server"
                                                AutoPostBack="True" CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="departmentDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Section </label>
                                            <asp:DropDownList ID="sectionDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                   </div>
                                  <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Grade </label>
                                            <asp:DropDownList ID="empGradeDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="empGradeDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                   <div class="col-2">
                                        <div class="form-group">
                                            <label>Designation </label>
                                            <asp:DropDownList ID="desigDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <%--<div class="col-2">
                                        <div class="form-group">
                                            <label>Line </label>
                                            <asp:DropDownList ID="lineDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>--%>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Type </label>
                                            <asp:DropDownList ID="employmentTypeDropDownList" runat="server"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Submit Process" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewButton_Click" />
                                        <%--<asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <div class="divWaiting">
                <asp:Image ID="imgWait" CssClass="position-set" runat="server" ImageAlign="Middle" ImageUrl="~/Assets/progress-bar-opt.gif"
                    Height="80" Width="80" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

</asp:Content>

