﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAL.HRM_DAL;

public partial class HRM_UI_SalaryProcess : System.Web.UI.Page
{
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();
    AttendanceProcessDAL attendanceProcessDal = new AttendanceProcessDAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
            DropDownList();
        } 
    }

    private void PopUp(string reportFlag)
    {
        string url = "../Report_UI/EmpSalaryReportViewer.aspx?ReportFlag=" + reportFlag + "&BankCash=" + bankCashDropDown.SelectedValue + "&mId=" + monthDropDownList.SelectedValue + "&mn=" + monthDropDownList.SelectedItem.Text + "&empcode=" + empCodeTextBox.Text;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    private bool Validation()
    {
        if (string.IsNullOrEmpty(yearDropDownList.SelectedValue))
        {
            showMessageBox("Select Year");
            return false;
        }
        if (string.IsNullOrEmpty(monthDropDownList.SelectedValue))
        {
            showMessageBox("Select Month");
            return false;
        }
        if (string.IsNullOrEmpty(salaryIdDropDownList.SelectedItem.Text))
        {
            showMessageBox("Select Salary Id");
            return false;
        }  
        return true;
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
    }

    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
    protected void divisNamDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
    }
    protected void empGradeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDesignationToDropDownBLL(desigDropDownList, empGradeDropDownList.SelectedValue);
    }
    public void DropDownList()
    {
        //aInfoBll.LoadLineToDropDownBLL(lineDropDownList);
        aInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        aInfoBll.LoadDivisionNameToDropDownBLL(divisNamDropDownList);
        aInfoBll.LoadGradeNameToDropDownBLL(empGradeDropDownList);
        aInfoBll.LoadEmpTypeNameToDropDownBLL(employmentTypeDropDownList);
        
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }

    private string FirstDayOfMonth(string year, string month)
    {
        return "01-" + month + "-" + year;

    }
    private string LastDayOfMonth(string year, string month)
    {
        return Convert.ToDateTime("01-" + month + "-" + year).AddMonths(1).AddDays(-1).ToString("dd-MMM-yyyy");

    }
    private string  ParameterGenerator()
    {
        string parameter = "";



        parameter = "  S.EmployeeStatus='Active' and S.JoiningDate<='" + LastDayOfMonth(yearDropDownList.SelectedItem.Text, monthDropDownList.SelectedItem.Text) + "' ";
            //if (!string.IsNullOrEmpty(salaryIdDropDownList.SelectedItem.Text))
            //{
            //    parameter = parameter + " and S.SalaryId='" + salaryIdDropDownList.SelectedItem.Text + "' ";
            //}    
      
        //if (!string.IsNullOrEmpty(yearDropDownList.SelectedValue))
        //{
        //    parameter = parameter + " and YEAR(S.SalaryStartDate)='" + yearDropDownList.SelectedItem.Text + "' ";
        //}
        //if (!string.IsNullOrEmpty(monthDropDownList.SelectedValue))
        //{
        //    parameter = parameter + " and Month(S.SalaryStartDate)='" + monthDropDownList.SelectedValue + "' ";
        //}
        if (bankCashDropDown.SelectedValue!="ALL")
        {
            parameter = parameter + " and S.PayType ='" + bankCashDropDown.SelectedValue + "' ";
        }
        if (typeDropDownList.SelectedValue=="SE")
        {
            parameter = parameter + " and S.EmpMasterCode='" + empCodeTextBox.Text + "' ";
            return parameter;
        }

        if(!string.IsNullOrEmpty(comNameDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.CompanyInfoId='" + comNameDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(unitNameDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.UnitId='" + unitNameDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(divisNamDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.DivisionId='" + divisNamDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(departmentDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.DepId='" + departmentDropDownList.SelectedValue + "' ";
        }
        
        if (!string.IsNullOrEmpty(sectionDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.SectionId='" + sectionDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(desigDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.DesigId='" + desigDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(employmentTypeDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.EmpTypeId='" + employmentTypeDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(empGradeDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.EmpGradeId='" + empGradeDropDownList.SelectedValue + "' ";
        }

        
        return parameter;
    }

    protected void viewReportButton_Click(object sender, EventArgs e)
    {
        
        
    }
    protected void typeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (typeDropDownList.SelectedValue == "SE")
        {
            divCommon.Visible = false;
            divEmpCode.Visible = true;
        }
        else
        {
            divCommon.Visible = true;
            divEmpCode.Visible = false;
        }
    }

   
    protected void viewButton_Click(object sender, EventArgs e)
    {
        int count = 0;
        if (Validation() == true)
        {

            DataTable dtlockstatus = attendanceProcessDal.LockStatus(yearDropDownList.SelectedItem.Text,
                monthDropDownList.SelectedValue);

            if (dtlockstatus.Rows.Count < 1)
            {
                DataTable dt = attendanceProcessDal.LoadEmployeeForSalary(ParameterGenerator());
                foreach (DataRow datarow in dt.Rows)
                {
                    string salaryId = salaryIdDropDownList.SelectedItem.Text;
                    string fromDate = FirstDayOfMonth(yearDropDownList.SelectedItem.Text, monthDropDownList.SelectedItem.Text);
                    string toDate = LastDayOfMonth(yearDropDownList.SelectedItem.Text, monthDropDownList.SelectedItem.Text);
                    if (attendanceProcessDal.SalaryProcess(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), datarow["EmpInfoId"].ToString(), salaryId, "HRDB", Session["LoginName"].ToString()) > 0)
                    {
                        count++;
                    }

                }

                if (count > 0)
                {
                    showMessageBox(count.ToString() + " Data is Processed !!!");
                }
                else
                {
                    showMessageBox("Process Not Done!!");
                }
            }
            else
            {
                showMessageBox("Salary have been locked for this Month");
            }

            
        }
    }
}