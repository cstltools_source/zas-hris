﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="SalaryReportAll.aspx.cs" Inherits="HRM_UI_SalaryReportAll" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;"> Employee Salary Report View </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Employee Salary Report View </a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Select Report </label>
                                        <asp:DropDownList ID="selectreportDropDownList" runat="server" AutoPostBack="True"
                                            CssClass="form-control form-control-sm"
                                            OnSelectedIndexChanged="selectreportDropDownList_SelectedIndexChanged">
                                            <asp:ListItem> Select any one </asp:ListItem>
<%--                                            <asp:ListItem Value="ESA">Employee Salary (Actual)</asp:ListItem>
                                            <asp:ListItem Value="SESAY">Emp wise Yearly Salary Actual</asp:ListItem>--%>
                                            <asp:ListItem Value="UWA">Unit Wise Actual</asp:ListItem>
<%--                                         <asp:ListItem Value="UWS">Unit Wise Summry</asp:ListItem>
                                            <asp:ListItem Value="USS">Unit Salary Stamp</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="DWA">Department Wise (Actual)</asp:ListItem>--%>
                                            <asp:ListItem Value="SWA">Employee Salary Statement </asp:ListItem>
                                            <asp:ListItem Value="SDB">Wages Disbursement </asp:ListItem>
                                           <%-- <asp:ListItem Value="DWSum">Department Wise  (Sumary)</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="ESACom">Employee Salary (Compliance)</asp:ListItem>
                                            <asp:ListItem Value="DWC">Department Wise (Compliance)</asp:ListItem>
                                            <asp:ListItem Value="SWC">Section Wise (Compliance)</asp:ListItem>--%>
                                           <%-- <asp:ListItem Value="PS">Pay Slip</asp:ListItem>
                                            <asp:ListItem Value="PSS">Pay Slip Single</asp:ListItem>
                                            <asp:ListItem Value="Ar">Arrear</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="MA">Mobile Allowance</asp:ListItem>
                                            <asp:ListItem Value="MAUW">Mobile Allowance(Unit wise)</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="SD">Salary Deduction</asp:ListItem>--%>
                                            <%-- <asp:ListItem Value="ASD">Advance Salary Deduction</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="EO">Extra Overtime</asp:ListItem>
                                                 <asp:ListItem Value="OD">Other Deduction</asp:ListItem>
                                            <asp:ListItem Value="FSA">Final Sattlement All</asp:ListItem>--%>
                                            <asp:ListItem Value="FSS">Final Sattlement Single</asp:ListItem>
                                           
                                            <asp:ListItem Value="IT">Income Tax</asp:ListItem>
                                           <%-- <asp:ListItem Value="ITUW">Income Tax(Unit Wise)</asp:ListItem>
                                            <asp:ListItem Value="STIN">Salary with TIN</asp:ListItem>
                                            <asp:ListItem Value="SNID">Salary with NID</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div id="dateid" runat="server">
                                <div id="oid" runat="server" visible="True">
                                    <div class="form-row">
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Employee Status </label>
                                                <asp:DropDownList ID="statusDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem>Active</asp:ListItem>
                                                    <asp:ListItem>Inactive</asp:ListItem>
                                                    <asp:ListItem>All</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2" id="yeardrop" runat="server">
                                            <div class="form-group">
                                                <label>Year </label>
                                                <asp:DropDownList ID="yearDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2" id="month" runat="server">
                                            <div class="form-group">
                                                <label>Month </label>
                                                <asp:DropDownList ID="monthDropDownList" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem Value=""> Select any one</asp:ListItem>
                                                    <asp:ListItem Value="1">January</asp:ListItem>
                                                    <asp:ListItem Value="2">February</asp:ListItem>
                                                    <asp:ListItem Value="3">March</asp:ListItem>
                                                    <asp:ListItem Value="4">April</asp:ListItem>
                                                    <asp:ListItem Value="5">May</asp:ListItem>
                                                    <asp:ListItem Value="6">June</asp:ListItem>
                                                    <asp:ListItem Value="7">July</asp:ListItem>
                                                    <asp:ListItem Value="8">August</asp:ListItem>
                                                    <asp:ListItem Value="9">September</asp:ListItem>
                                                    <asp:ListItem Value="10">October</asp:ListItem>
                                                    <asp:ListItem Value="11">November</asp:ListItem>
                                                    <asp:ListItem Value="12">December</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Bank / Cash </label>
                                                <asp:DropDownList ID="bankCashDropDown" runat="server" CssClass="form-control form-control-sm">
                                                    <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                                    <asp:ListItem Value="Bank">Bank</asp:ListItem>
                                                    <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>



                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Report Type </label>
                                                <asp:DropDownList ID="typeDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm" OnSelectedIndexChanged="typeDropDownList_SelectedIndexChanged">
                                                    <asp:ListItem> Select any one </asp:ListItem>
                                                    <asp:ListItem Value="SE">Single Employee</asp:ListItem>
                                                    <asp:ListItem Value="AE">All Employee </asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-2">
                                            <div class="form-group">
                                                <label>Salary Id </label>
                                                <asp:DropDownList ID="salaryIdDropDownList" runat="server" AutoPostBack="True"
                                                    CssClass="form-control form-control-sm"
                                                    OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                                    <asp:ListItem Value="1">SID-001</asp:ListItem>
                                                    <asp:ListItem Value="2">SID-002</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                    </div>
                            </div>

                            <div id="daterange" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>From Date </label>
                                            <div class="input-group date pull-left" id="daterangepicker113">
                                                <asp:TextBox ID="fromDateTexBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                                    Format="dd-MMM-yyyy" PopupButtonID="ImageButton3" CssClass="custom"
                                                    TargetControlID="fromDateTexBox" />
                                                <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                    <span>
                                                        <asp:ImageButton ID="ImageButton3" runat="server"
                                                            AlternateText="Click to show calendar"
                                                             ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>To Date </label>
                                            <div class="input-group date pull-left" id="daterangepicker12">
                                                <asp:TextBox ID="toDateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" runat="server"
                                                    Format="dd-MMM-yyyy" PopupButtonID="ImageButton4" CssClass="custom"
                                                    TargetControlID="toDateTextBox" />
                                                <div class="input-group-addon" style="border: 1px solid #cccccc">
                                                    <span>
                                                        <asp:ImageButton ID="ImageButton4" runat="server"
                                                            AlternateText="Click to show calendar"
                                                             ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divEmpCode" runat="server" visible="False">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Code </label>
                                            <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divCommon" runat="server" visible="True">
                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Company name </label>
                                            <asp:DropDownList ID="comNameDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm" OnSelectedIndexChanged="comNameDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Unit Name </label>
                                            <asp:DropDownList ID="unitNameDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Division </label>
                                            <asp:DropDownList ID="divisNamDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="divisNamDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Department </label>
                                            <asp:DropDownList ID="departmentDropDownList" runat="server"
                                                AutoPostBack="True" CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="departmentDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Section </label>
                                            <asp:DropDownList ID="sectionDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employee Grade </label>
                                            <asp:DropDownList ID="empGradeDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm"
                                                OnSelectedIndexChanged="empGradeDropDownList_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Designation Name </label>
                                            <asp:DropDownList ID="desigDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Employement Type </label>
                                            <asp:DropDownList ID="employmentTypeDropDownList" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <div class="col-2">
                                        <div class="form-group">
                                            <label>Bank Name </label>
                                            <asp:DropDownList ID="ddlBank" runat="server" AutoPostBack="True"
                                                CssClass="form-control form-control-sm">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br />
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="processButton" Text="View Report" CssClass="btn btn-sm btn-info" runat="server" OnClick="viewButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

