﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_SalaryReportAll : System.Web.UI.Page
{
    EmpGeneralInfoBLL aInfoBll = new EmpGeneralInfoBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadYear();
            DropDownList();
        } 
    }

    private void PopUp(string reportFlag)
    {
        string url = "../Report_UI/EmpSalaryReportViewer.aspx?ReportFlag=" + reportFlag + "&BankCash=" + bankCashDropDown.SelectedValue + "&mId=" + monthDropDownList.SelectedValue + "&mn=" + monthDropDownList.SelectedItem.Text + "&empcode=" + empCodeTextBox.Text;
        string fullURL = "window.open('" + url + "', '_blank', 'height=600,width=900,status=yes,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,titlebar=no' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }

    private bool Validation()
    {
        if (string.IsNullOrEmpty(typeDropDownList.SelectedValue))
        {
            showMessageBox("Select Report Type");
            return false;
        }
        if (string.IsNullOrEmpty(selectreportDropDownList.SelectedValue))
        {
            showMessageBox("Select Report");
            return false;
        }
        
        return true;
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
    }

    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
    protected void divisNamDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
    }
    protected void empGradeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        aInfoBll.LoadDesignationToDropDownBLL(desigDropDownList, empGradeDropDownList.SelectedValue);
    }
    public void DropDownList()
    {
        //aInfoBll.LoadLineToDropDownBLL(lineDropDownList);
        aInfoBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        aInfoBll.LoadDivisionNameToDropDownBLL(divisNamDropDownList);
        aInfoBll.LoadGradeNameToDropDownBLL(empGradeDropDownList);
        aInfoBll.LoadEmpTypeNameToDropDownBLL(employmentTypeDropDownList);
        aInfoBll.LoadBankName(ddlBank);
        
    }
    private void LoadYear()
    {
        int i = DateTime.Now.Year;
        for (i = i - 5; i <= DateTime.Now.Year + 5; i++)
            yearDropDownList.Items.Add(Convert.ToString(i));

        yearDropDownList.SelectedItem.Text = DateTime.Now.Year.ToString();
    }
    private string  ParameterGenerator()
    {
        string parameter = "";


        if (selectreportDropDownList.SelectedValue == "FSA" || selectreportDropDownList.SelectedValue == "FSS")
        {
            parameter = "  E.EmployeeStatus='"+statusDropDownList.SelectedItem.Text+"' ";
            
        }
        else
        {
            if (statusDropDownList.SelectedItem.Text=="All")
            {
                parameter = "  E.EmployeeStatus in ('Active','Inactive') ";
            }
            else
            {
                parameter = "  E.EmployeeStatus='" + statusDropDownList.SelectedItem.Text + "' ";
            }
            if (!string.IsNullOrEmpty(salaryIdDropDownList.SelectedItem.Text))
            {
                parameter = parameter + " and S.SalaryId='" + salaryIdDropDownList.SelectedItem.Text + "' ";
            }    
        }
        if (selectreportDropDownList.SelectedValue != "SESAY")
        {
            if (!string.IsNullOrEmpty(yearDropDownList.SelectedValue))
            {
                parameter = parameter + " and YEAR(S.SalaryStartDate)='" + yearDropDownList.SelectedItem.Text + "' ";
            }
        }
        else
        {
            parameter = parameter + " and (S.SalaryStartDate) between '"+fromDateTexBox.Text+"' AND '"+toDateTextBox.Text+"' ";
        }
        if (!string.IsNullOrEmpty(monthDropDownList.SelectedValue))
        {
            parameter = parameter + " and Month(S.SalaryStartDate)='" + monthDropDownList.SelectedValue + "' ";
        }
        if (bankCashDropDown.SelectedValue!="ALL")
        {
            parameter = parameter + " and S.PayBankorCash ='" + bankCashDropDown.SelectedValue + "' ";
        }
        if (typeDropDownList.SelectedValue=="SE")
        {
            parameter = parameter + " and S.EmpMasterCode='" + empCodeTextBox.Text + "' ";
            return parameter;
        }

        if(!string.IsNullOrEmpty(comNameDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.CompanyInfoId='" + comNameDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(unitNameDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.UnitId='" + unitNameDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(divisNamDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.DivisionId='" + divisNamDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(departmentDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.DepId='" + departmentDropDownList.SelectedValue + "' ";
        }
        
        if (!string.IsNullOrEmpty(sectionDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.SectionId='" + sectionDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(desigDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.DesigId='" + desigDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(employmentTypeDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.EmpTypeId='" + employmentTypeDropDownList.SelectedValue + "' ";
        }
        if (!string.IsNullOrEmpty(empGradeDropDownList.SelectedValue))
        {
            parameter = parameter + " and S.EmpGradeId='" + empGradeDropDownList.SelectedValue + "' ";
        }

        if (selectreportDropDownList.SelectedValue == "IT" || selectreportDropDownList.SelectedValue == "ITUW")
        {
            parameter = parameter + " AND Tax<>'0' ";
        }
        if (selectreportDropDownList.SelectedValue == "MA" || selectreportDropDownList.SelectedValue == "MAUW")
        {
            parameter = parameter + " AND MobileAllowance<>'0' ";
        }
        if (selectreportDropDownList.SelectedValue == "OD" )
        {
            parameter = parameter + " AND OtherDeduction<>'0' ";
        }

        if (selectreportDropDownList.SelectedValue == "SDB")
        {
            parameter = parameter + "   AND E.BankAccNo<>''  ";

            if (ddlBank.SelectedValue != "")
            {
                parameter = parameter + " AND E.BankId = " + ddlBank.SelectedValue;
            }
        }

        parameter = parameter + " AND E.IsSalary = 0 ORDER BY  CONVERT(int , dbo.udf_GetNumeric(E.EmpMasterCode) )  ASC ";
        return parameter;
    }

    protected void viewReportButton_Click(object sender, EventArgs e)
    {
        
        
    }
    protected void typeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (typeDropDownList.SelectedValue == "SE")
        {
            divCommon.Visible = false;
            divEmpCode.Visible = true;
        }
        else
        {
            divCommon.Visible = true;
            divEmpCode.Visible = false;
        }
    }

    protected void selectreportDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        month.Visible = true;
        daterange.Visible = false;
        if (selectreportDropDownList.SelectedValue == "SESAY")
        {
            month.Visible = false;
            yeardrop.Visible = false;
            daterange.Visible = true;
        }
    }
    protected void viewButton_Click(object sender, EventArgs e)
    {
        
        if (Validation() == true)
        {
            Session["ReportParameter"] = ParameterGenerator();
            PopUp(selectreportDropDownList.SelectedValue);
        }
    }
}