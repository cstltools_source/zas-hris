﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SalaryScaleEdit.aspx.cs" Inherits="HRM_UI_GradeEdit" %>

<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form2" runat="server">

        <asp:ScriptManager ID="ScriptManager2" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
            <div class="page-heading">
                <div class="page-heading__container">
                    <div class="icon"><span class="li-register"></span></div>
                    <span></span>
                    <h1 class="title" style="font-size: 18px; padding-top: 9px;">Salary Scale Edit </h1>
                </div>
                <div class="page-heading__container float-right d-none d-sm-block">
                    <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                    <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Salary Scale Edit</a></li>

                        </ol>
                    </nav>
            </div>
            <!-- //END PAGE HEADING -->
             <br/>
                    <br/>
            <div class="container-fluid">
                <div class="card">
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Salary Grade Name </label>
                                    <asp:TextBox ID="salGradeNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    <asp:HiddenField ID="SalGradeIdHiddenField" runat="server" />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Basic Salary </label>
                                    <asp:TextBox ID="basicSalaryTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="fnpTextBox" runat="server"
                                        TargetControlID="basicSalaryTextBox"
                                        FilterType="Custom, Numbers"
                                        ValidChars="." />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>House Rent </label>
                                    <asp:TextBox ID="houseRentTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="fhouseRentTextBox" runat="server"
                                        TargetControlID="houseRentTextBox"
                                        FilterType="Custom, Numbers"
                                        ValidChars="." />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Madicle </label>
                                    <asp:TextBox ID="medicleTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                        TargetControlID="medicleTextBox"
                                        FilterType="Custom, Numbers"
                                        ValidChars="." />
                                </div>
                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label>Increment Rate </label>
                                    <asp:TextBox ID="incrementRateTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="fincrementRateTextBox" runat="server"
                                        TargetControlID="incrementRateTextBox"
                                        FilterType="Custom, Numbers"
                                        ValidChars="." />
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group">
                                    <label>Conveyance </label>
                                    <asp:TextBox ID="conveyanceTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="fconveyanceTextBox" runat="server"
                                        TargetControlID="conveyanceTextBox"
                                        FilterType="Custom, Numbers"
                                        ValidChars="." />
                                </div>
                            </div>


                            <div class="col-3">
                                <div class="form-group">
                                    <label>Food allowance </label>
                                    <asp:TextBox ID="foodallowanceTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    <asp:FilteredTextBoxExtender ID="ffoodallowanceTextBox" runat="server"
                                        TargetControlID="foodallowanceTextBox"
                                        FilterType="Custom, Numbers"
                                        ValidChars="." />
                                </div>
                            </div>


                            <div class="col-3">
                                <div class="form-group">
                                    <label>Active Date </label>
                                    <div class="input-group date pull-left" id="daterangepicker1">
                                        <asp:TextBox ID="actDateTextBox" runat="server" class="form-control form-control-sm" CausesValidation="true"></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender" runat="server"
                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton1" CssClass="custom" PopupPosition="TopLeft"
                                            TargetControlID="actDateTextBox" />
                                        <div class="input-group-addon" style="border: 1px solid #cccccc">
                                            <span>
                                                <asp:ImageButton ID="ImageButton1" runat="server"
                                                    AlternateText="Click to show calendar"
                                                     ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-3">
                                <label>Salary Grade Name </label>
                                <asp:TextBox ID="statusTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-6">
                                <div class="form-group">
                                    <asp:Button ID="updateButton" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="updateButton_Click" />
                                    <asp:Button ID="cancelButton" Text="Close"  CssClass="btn btn-sm warning" runat="server" OnClick="closeButton_Click" BackColor="#FF9900" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>
