﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_GradeEdit : System.Web.UI.Page
{
    SalaryScaleBll aSalaryScaleBll = new SalaryScaleBll();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SalGradeIdHiddenField.Value = Request.QueryString["ID"];
            SalaryScleLoad(SalGradeIdHiddenField.Value);            
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        SalaryScale aSalaryScale = new SalaryScale()
          {
              SalScaleId = Convert.ToInt32(SalGradeIdHiddenField.Value),
              SalScaleName = salGradeNameTextBox.Text,
              HouseRent = Convert.ToDecimal(houseRentTextBox.Text),
              Medical = Convert.ToDecimal(medicleTextBox.Text),
              BasicSalary = Convert.ToDecimal(basicSalaryTextBox.Text),
              IncrementRate = Convert.ToDecimal(incrementRateTextBox.Text),
              Foodallowance = Convert.ToDecimal(foodallowanceTextBox.Text),
              //ProvidentFund = provFundTextBox.Text,
              Conveyance = Convert.ToDecimal(conveyanceTextBox.Text),
              //OtRatePerHour = Convert.ToDecimal(otPerHourTextBox.Text),
              ActiveDate = Convert.ToDateTime(actDateTextBox.Text),
              ActionStatus = statusTextBox.Text
          };
        SalaryScaleBll salaryScaleBll = new SalaryScaleBll();
        if (!salaryScaleBll.UpdateDataForSalaryScale(aSalaryScale))
        {
            showMessageBox("Data Not Update!!!");
        }
        else
        {
            showMessageBox("Data Update Successfully!!! Please Reload");
        }         
    }

    private void SalaryScleLoad(string gradeId)
    {
        SalaryScale aSalaryScale = new SalaryScale();
        aSalaryScale = aSalaryScaleBll.SalaryScaleEditLoad(gradeId);
        salGradeNameTextBox.Text = aSalaryScale.SalScaleName;
        houseRentTextBox.Text = aSalaryScale.HouseRent.ToString();
        medicleTextBox.Text = aSalaryScale.Medical.ToString();
        basicSalaryTextBox.Text = aSalaryScale.BasicSalary.ToString();
        incrementRateTextBox.Text = aSalaryScale.IncrementRate.ToString();
        //provFundTextBox.Text = aSalaryScale.ProvidentFund;
        conveyanceTextBox.Text = aSalaryScale.Conveyance.ToString();
        foodallowanceTextBox.Text = aSalaryScale.Foodallowance.ToString();
        //otPerHourTextBox.Text = aSalaryScale.OtRatePerHour.ToString();
        actDateTextBox.Text = aSalaryScale.ActiveDate.ToString();
        statusTextBox.Text = aSalaryScale.ActionStatus;   
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}