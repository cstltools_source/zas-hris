﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="SalaryScaleEntry.aspx.cs" Inherits="HRM_UI_GradeEntry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Salary Scale Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="gradeImageButton_Click" />
                        <%--<asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Salary Scale Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Salary Grade Name </label>
                                        <asp:TextBox ID="salGradeNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Basic Salary </label>
                                        <asp:TextBox ID="basicSalaryTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="fnpTextBox" runat="server"
                                            TargetControlID="basicSalaryTextBox"
                                            FilterType="Custom, Numbers"
                                            ValidChars="." />
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>House Rent </label>
                                        <asp:TextBox ID="houseRentTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="fhouseRentTextBox" runat="server"
                                            TargetControlID="houseRentTextBox"
                                            FilterType="Custom, Numbers"
                                            ValidChars="." />
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Madicle </label>
                                        <asp:TextBox ID="medicleTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                            TargetControlID="medicleTextBox"
                                            FilterType="Custom, Numbers"
                                            ValidChars="." />
                                    </div>
                                </div>

                            </div>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Increment Rate </label>
                                        <asp:TextBox ID="incrementRateTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="fincrementRateTextBox" runat="server"
                                            TargetControlID="incrementRateTextBox"
                                            FilterType="Custom, Numbers"
                                            ValidChars="." />
                                    </div>
                                </div>

                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Conveyance </label>
                                        <asp:TextBox ID="conveyanceTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="fconveyanceTextBox" runat="server"
                                            TargetControlID="conveyanceTextBox"
                                            FilterType="Custom, Numbers"
                                            ValidChars="." />
                                    </div>
                                </div>


                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Food allowance </label>
                                        <asp:TextBox ID="foodallowanceTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:FilteredTextBoxExtender ID="ffoodallowanceTextBox" runat="server"
                                            TargetControlID="foodallowanceTextBox"
                                            FilterType="Custom, Numbers"
                                            ValidChars="." />
                                    </div>
                                </div>


                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Active Date </label>
                                        <div class="input-group date pull-left" id="daterangepicker1">
                                             <asp:TextBox ID="actDateTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                        <asp:CalendarExtender ID="actDtTextBox_CalendarExtender" runat="server" 
                                            Format="dd-MMM-yyyy" PopupButtonID="ImageButton" CssClass="custom" PopupPosition="TopLeft"
                                            TargetControlID="actDateTextBox">
                                        </asp:CalendarExtender>
                                        <asp:ImageButton ID="ImageButton" runat="server" 
                                            AlternateText="Click to show calendar" 
                                            ImageUrl="../Assets/Calendar_scheduleHS.png" TabIndex="4" />
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                           <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

