﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_GradeEntry : System.Web.UI.Page
{
    SalaryScaleBll aSalaryScaleBll=new SalaryScaleBll();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
        }
    }
    
    private void Clear()
    {
        salGradeNameTextBox.Text = string.Empty;
        houseRentTextBox.Text = string.Empty;
        medicleTextBox.Text = string.Empty;
        basicSalaryTextBox.Text = string.Empty;
        incrementRateTextBox.Text = string.Empty;
        //provFundTextBox.Text = string.Empty;
        conveyanceTextBox.Text = string.Empty;
        foodallowanceTextBox.Text = string.Empty;
        actDateTextBox.Text = string.Empty;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (salGradeNameTextBox.Text == "")
        {
            showMessageBox("Please Input Grade Name !!");
            return false;
        }
        if (houseRentTextBox.Text == "")
        {
            showMessageBox("Please Input  House Rent!!");
            return false;
        }
        if (medicleTextBox.Text == "")
        {
            showMessageBox("Please Input  Medical Amount !!");
            return false;
        }

        if (basicSalaryTextBox.Text == "")
        {
            showMessageBox("Please Input  Basic Salary !!");
            return false;
        }

        if (incrementRateTextBox.Text == "")
        {
            showMessageBox("Please Input  Increment Rate !!");
            return false;
        }

        if (conveyanceTextBox.Text == "")
        {
            showMessageBox("Please Input  Conveyancee Fund !!");
            return false;
        }

        if (actDateTextBox.Text == "")
        {
            showMessageBox("Please Input  Active Date !!");
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            SalaryScale aSalaryScale = new SalaryScale()
            {
                SalScaleName = salGradeNameTextBox.Text,
                BasicSalary = Convert.ToDecimal(basicSalaryTextBox.Text),
                HouseRent = Convert.ToDecimal(houseRentTextBox.Text),
                Medical = Convert.ToDecimal(medicleTextBox.Text),
                IncrementRate = Convert.ToDecimal(incrementRateTextBox.Text),
                //ProvidentFund = provFundTextBox.Text,
                Conveyance = Convert.ToDecimal(conveyanceTextBox.Text),
                Foodallowance = Convert.ToDecimal(foodallowanceTextBox.Text),
                ActiveDate = Convert.ToDateTime(actDateTextBox.Text),
                ActionStatus = "Posted",
                EntryBy = Session["LoginName"].ToString(),
                EntryDate = Convert.ToDateTime(Convert.ToDateTime(DateTime.Today.ToShortDateString()).ToString("dd-MMM-yyyy")),
                ApprovedDate = Convert.ToDateTime(Convert.ToDateTime(DateTime.Today.ToShortDateString()).ToString("dd-MMM-yyyy")),
                ApprovedBy = Session["LoginName"].ToString(),
                IsActive = true,
            };
            //if (otPerHourTextBox.Text=="")
            //{
            //    aSalaryScale.Foodallowance = Convert.ToDecimal("0.00");
            //}
            //else
            //{
            //    aSalaryScale.Foodallowance = Convert.ToDecimal(otPerHourTextBox.Text);
            //}

            SalaryScaleBll aSalaryScaleBll = new SalaryScaleBll();
            if (aSalaryScaleBll.SaveDataForSalaryScale(aSalaryScale))
            {
                showMessageBox("Data save successfully");
                Clear();
            }  
        }       
    }
    
    protected void gradeImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("SalaryScaleView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}