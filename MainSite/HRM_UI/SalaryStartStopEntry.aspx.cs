﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_SalaryStartStopEntry : System.Web.UI.Page
{
    SalaryStartStopBLL aStartStopBll=new SalaryStartStopBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        }
    }

    public void DropDownList()
    {
        aStartStopBll.LoadFinancialYear(financialDropDownList);
    }
    protected void monthDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = aStartStopBll.LoadSalaryStop(financialDropDownList.SelectedValue,
            financialDropDownList.SelectedItem.Text, monthDropDownList.SelectedValue);
        if (dt.Rows.Count>0)
        {
            statusLabel.Text = dt.Rows[0]["Status"].ToString();
        }
        else
        {
            statusLabel.Text = "Unlocked";
        }

        if (statusLabel.Text=="Locked")
        {
            lockButton.Visible = false;
            unlockButton.Visible = true;
        }
        else
        {
            lockButton.Visible = true;
            unlockButton.Visible = false;
        }
    }

    public void Clear()
    {
        financialDropDownList.SelectedIndex = 0;
        monthDropDownList.SelectedIndex = 0;
        lockButton.Visible = false;
        unlockButton.Visible = false;
        statusLabel.Text = "";
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void lockButton_Click(object sender, EventArgs e)
    {
        if (statusLabel.Text=="Unlocked")
        {
            SalaryStartStop aSalaryStartStop=new SalaryStartStop();
            aSalaryStartStop.FyrId = Convert.ToInt32(financialDropDownList.SelectedValue);
            aSalaryStartStop.Month = monthDropDownList.SelectedValue;
            aSalaryStartStop.Year = financialDropDownList.SelectedItem.Text;
            aSalaryStartStop.LockedBy = Session["LoginName"].ToString();
            aSalaryStartStop.LockedDate = DateTime.Now;
            aSalaryStartStop.Status = "Locked";

            if (aStartStopBll.SaveDataForSalaryStartStop(aSalaryStartStop))
            {
                showMessageBox("Salary Locked Successfully !!!");
                Clear();
            }
        }
    }
    protected void unlockButton_Click(object sender, EventArgs e)
    {
        if (statusLabel.Text=="Locked")
        {
            SalaryStartStop aSalaryStartStop = new SalaryStartStop();
            aSalaryStartStop.FyrId = Convert.ToInt32(financialDropDownList.SelectedValue);
            aSalaryStartStop.Month = monthDropDownList.SelectedValue;
            aSalaryStartStop.Year = financialDropDownList.SelectedItem.Text;
            aSalaryStartStop.UnlockedBy = Session["LoginName"].ToString();
            aSalaryStartStop.UnlockedDate = DateTime.Now;
            aSalaryStartStop.Status = "Unlocked";

            if (aStartStopBll.UpdateSalaryStartStop(aSalaryStartStop))
            {
                Clear();
                showMessageBox("Salary Unlocked Successfully !!");
            }
        }
    }
}