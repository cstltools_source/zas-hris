﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_SectionEdit : System.Web.UI.Page
{
    SectionBLL aSectionBll = new SectionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DepartmentNameLoad();
            sectionIdHiddenField.Value = Request.QueryString["ID"];

            SectionLoad(sectionIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (sectionNameTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        if (departmentNameDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Department Name !!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Section aSection = new Section()
            {
                SectionId = Convert.ToInt32(sectionIdHiddenField.Value),
                SectionName = sectionNameTextBox.Text,
                DepartmentId = Convert.ToInt32(departmentNameDropDownList.SelectedValue)
            };
            SectionBLL aSectionBll = new SectionBLL();

            if (!aSectionBll.UpdateDataForSection(aSection))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    private void SectionLoad(string sectionid)
    {
        Section aSection = new Section();
        Department aDepartment = new Department();
        aSection = aSectionBll.SectionEditLoad(sectionid);
        sectionNameTextBox.Text = aSection.SectionName;
        departmentNameDropDownList.SelectedValue = Convert.ToString(aSection.DepartmentId);
    }
    private void DepartmentNameLoad()
    {
        aSectionBll.LoadDepartment(departmentNameDropDownList);
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}