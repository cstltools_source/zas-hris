﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_SectionEntry : System.Web.UI.Page
{
    SectionBLL aSectionBll = new SectionBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DepartmentNameLoad();
        } 
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        sectionNameTextBox.Text = string.Empty;
    }
    public void DepartmentNameLoad()
    {
        aSectionBll.LoadDepartment(departmentNameDropDownList);
    }
    private bool Validation()
    {
        if (sectionNameTextBox.Text == "")
        {
            showMessageBox("Please Input Section Name!!");
            return false;
        }
        if (departmentNameDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Select Department Name!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Section aSection = new Section()
            {
                SectionName = sectionNameTextBox.Text,
                DepartmentId = Convert.ToInt32(departmentNameDropDownList.SelectedValue)
            };
            if (aSectionBll.SaveDataForSection(aSection))
            {
                showMessageBox("Data Save Successfully Section Code is :  " + aSection.SectionCode + "    And Section Name is :   " + aSection.SectionName);
                Clear();
                DepartmentNameLoad();
            }
            else
            {
                showMessageBox("Section Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void viewListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("SectionView.aspx");
    }
    protected void sectionImageButton_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Report_UI/SectionReport.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}