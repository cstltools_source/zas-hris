﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_ShiftEdit : System.Web.UI.Page
{
    ShiftBLL aShiftBll = new ShiftBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           sheftIdHiddenField.Value = Request.QueryString["ID"];
           ShiftLoad(sheftIdHiddenField.Value);
        }
    }

    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (shiftNameTextBox.Text == "")
        {
            showMessageBox("Please Input Shift Name!!");
            return false;
        }
        if (shiftInTimeTextBox.Text == "")
        {
            showMessageBox("Please Input Shift In Time !!");
            return false;
        }
        if (shiftOutTimeTextBox.Text == "")
        {
            showMessageBox("Please Input Shift Out Time!!");
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        { 
            Shift aShift = new Shift()
            {
                ShiftId = Convert.ToInt32(sheftIdHiddenField.Value),
                ShiftName = shiftNameTextBox.Text,
                ShiftInTime = Convert.ToDateTime(shiftInTimeTextBox.Text).TimeOfDay,
                ShiftOutTime = Convert.ToDateTime(shiftOutTimeTextBox.Text).TimeOfDay,
                ConsideredInMin = Convert.ToInt32(consInMainTextBox.Text),
                BreakEnd = Convert.ToDateTime(breakEndTextBox.Text).TimeOfDay,
                BreakStart = Convert.ToDateTime(breakStartTextBox.Text).TimeOfDay,
            };
            ShiftBLL aShiftBll = new ShiftBLL();

            if (!aShiftBll.UpdateDataForShift(aShift))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void ShiftLoad(string ShiftId)
    {
        Shift aShift = new Shift();
        aShift = aShiftBll.ShiftEditLoad(ShiftId);
        shiftNameTextBox.Text = aShift.ShiftName;
        shiftInTimeTextBox.Text = aShift.ShiftInTime.ToString();
        shiftOutTimeTextBox.Text = aShift.ShiftOutTime.ToString();
        consInMainTextBox.Text = aShift.ConsideredInMin.ToString();
        breakEndTextBox.Text = aShift.BreakEnd.ToString();
        breakStartTextBox.Text = aShift.BreakStart.ToString();
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}