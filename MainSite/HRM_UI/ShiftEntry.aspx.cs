﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ShiftEntry : System.Web.UI.Page
{
    ShiftBLL aShiftBll = new ShiftBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {}        
    }
    public bool ShiftInTime()
    {
        try
        {
            TimeSpan aTimeSpan = new TimeSpan();
            aTimeSpan = Convert.ToDateTime(shiftInTimeTextBox.Text).TimeOfDay;
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool ShiftOutTime()
    {
        try
        {
            TimeSpan aTimeSpan = new TimeSpan();
            aTimeSpan = Convert.ToDateTime(shiftOutTimeTextBox.Text).TimeOfDay;
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool BreakStart()
    {
        try
        {
            TimeSpan aTimeSpan = new TimeSpan();
            aTimeSpan = Convert.ToDateTime(breakStartTextBox.Text).TimeOfDay;
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool BreakEnd()
    {
        try
        {
            TimeSpan aTimeSpan = new TimeSpan();
            aTimeSpan = Convert.ToDateTime(breakEndTextBox.Text).TimeOfDay;
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private void Clear()
    {
        shiftNameTextBox.Text = string.Empty;
        shiftInTimeTextBox.Text = string.Empty;
        shiftOutTimeTextBox.Text = string.Empty;
        consInMainTextBox.Text = string.Empty;
        breakStartTextBox.Text = string.Empty;
        breakEndTextBox.Text = string.Empty;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {
        if (shiftNameTextBox.Text == "")
        {
            showMessageBox("Please Input Shift Name!!");
            return false;
        }
        if (shiftInTimeTextBox.Text == "")
        {
            showMessageBox("Please Input Shift In Time !!");
            return false;
        }
        if (shiftOutTimeTextBox.Text == "")
        {
            showMessageBox("Please Input Shift Out Time !!");
            return false;
        }
        if (ShiftInTime() == false)
        {
            showMessageBox("Please give a valid Shift InTime.Example:- 2:00:00 !!!");
            return false;
        }
        if (ShiftOutTime() == false)
        {
            showMessageBox("Please give a valid Shift OutTime.Example:- 2:00:00 !!!");
            return false;
        }
        if (BreakStart() == false)
        {
            showMessageBox("Please give a valid Break Start.Example:- 2:00:00 !!!");
            return false;
        }
        if (BreakEnd() == false)
        {
            showMessageBox("Please give a valid Break End.Example:- 2:00:00 !!!");
            return false;
        }
        return true;
    }
    
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            Shift aShift = new Shift()
                                     {
                                         ShiftName = shiftNameTextBox.Text,
                                         ShiftInTime = Convert.ToDateTime(shiftInTimeTextBox.Text).TimeOfDay,
                                         ShiftOutTime = Convert.ToDateTime(shiftOutTimeTextBox.Text).TimeOfDay,
                                         ConsideredInMin = Convert.ToInt32(consInMainTextBox.Text),
                                         BreakEnd = Convert.ToDateTime(breakEndTextBox.Text).TimeOfDay,
                                         BreakStart = Convert.ToDateTime(breakStartTextBox.Text).TimeOfDay,
                                     };


            ShiftBLL aShiftBll = new ShiftBLL();
            if (aShiftBll.SaveDataForShift(aShift))
            {
                showMessageBox("Data Save Successfully");
                Clear();
            }

            else
            {
                showMessageBox("Shift Name Already Exist");
            }   
        }
    }
    protected void viewListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("ShiftView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}