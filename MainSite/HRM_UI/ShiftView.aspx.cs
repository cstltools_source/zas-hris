﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;


public partial class HRM_UI_ShiftView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    ShiftBLL aShiftBll = new ShiftBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShiftLoad();
        }
    }

    private void ShiftLoad()
    {
        aDataTable = aShiftBll.LoadShiftView();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }
    
    protected void ReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        ShiftLoad();
    }
    protected void NewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("ShiftEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "ShiftEdit.aspx?ID=" + Id;
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string ShiftId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(ShiftId);
        }

    }
    protected void loadGridView_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}