﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_ShiftWiseGroupEntry : System.Web.UI.Page
{
    ShiftWiseGroupBLL aShiftWiseGroupBll = new ShiftWiseGroupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadDropDown();   
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        fromDtTextBox.Text = string.Empty;
        toDtTextBox.Text = string.Empty;
        groupDropDownList.SelectedValue = null;
        shiftInTextBox.Text = string.Empty;
        shiftOutTextBox.Text = string.Empty;
        shiftDropDownList.SelectedValue = null;
    }
    private void LoadDropDown()
    {
        EmpGeneralInfoBLL aGeneralInfoBll=new EmpGeneralInfoBLL();
        aGeneralInfoBll.LoadUnitNameAllByUser(unitDropDownList);
        //aShiftWiseGroupBll.LoadGroup(groupDropDownList);
        aShiftWiseGroupBll.LoadShift(shiftDropDownList);
        
    }
    
    public bool FromDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(fromDtTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    public bool ToDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(toDtTextBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }
    private bool Validation()
    {
        if (fromDtTextBox.Text == "")
        {
            showMessageBox("Please Input Date !!");
            return false;
        }
        
        if (FromDate() == false)
        {
            showMessageBox("Please give a valid From Date !!!");
            return false;
        }
        if (ToDate() == false)
        {
            showMessageBox("Please give a valid To Date !!!");
            return false;
        }
        return true;
    }
    
    protected void submitButton_Click(object sender, EventArgs e)
    {
       if (Validation() == true)
       {
            ShiftWiseGroup aShiftWiseGroup = new ShiftWiseGroup()
            {
                
                GroupId = Convert.ToInt32(groupDropDownList.SelectedValue),
                FromDate= Convert.ToDateTime(fromDtTextBox.Text),
                ToDate= Convert.ToDateTime(toDtTextBox.Text),
                ShiftId= Convert.ToInt32(shiftDropDownList.SelectedValue),
                ShiftInTime= Convert.ToDateTime(shiftInTextBox.Text).TimeOfDay,
                ShiftOutTime= Convert.ToDateTime(shiftOutTextBox.Text).TimeOfDay,
                Status = "Posted",
                EntryUser = Session["LoginName"].ToString(),
                EntryDate = System.DateTime.Today,
                IsActive = true,
            };
           DataTable dtDate = aShiftWiseGroupBll.LoadDate(groupDropDownList.SelectedValue, fromDtTextBox.Text,
                                                          toDtTextBox.Text);
           if (dtDate.Rows.Count <= 0)
           {
               if (aShiftWiseGroupBll.SaveShiftWiseGroupData(aShiftWiseGroup))
               {
                   showMessageBox("Data Save Successfully");
                   Clear();
               }
           }
           else
           {
               showMessageBox("Group already assign for this Date");
           }

       }
        else
        {
            showMessageBox("Please Input Data In All TextBox!");
        }
    }

    protected void jobViewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("ShiftWiseGroupView.aspx");
    }

    protected void shiftDropDownList_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(shiftDropDownList.SelectedValue))
        {
            DataTable aTable = new DataTable();
            aTable = aShiftWiseGroupBll.LoadShift(shiftDropDownList.SelectedValue);

            if (aTable.Rows.Count > 0)
            {
                shiftInTextBox.Text = aTable.Rows[0]["ShiftInTime"].ToString().Trim();
                shiftOutTextBox.Text = aTable.Rows[0]["ShiftOutTime"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }

    protected void unitDropDownList_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        aShiftWiseGroupBll.LoadGroupName(groupDropDownList,unitDropDownList.SelectedValue);
    }
}