﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="ShiftWiseGroupView.aspx.cs" Inherits="HRM_UI_ShiftWiseGroupView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }
    </style>

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Shift Wise Group View </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="addNewButton_OnClick" />
                        <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="reloadButton_OnClick" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Shift Wise View</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 380px; overflow: auto; width: auto;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark" DataKeyNames="GSAId" OnRowCommand="loadGridView_RowCommand" >
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="GroupName" HeaderText="Group Name" />
                                        <asp:BoundField DataField="ShiftName" HeaderText="Shift Name" />
                                    <asp:BoundField DataField="ShiftInTime" HeaderText="ShiftInTime" />
                                    <asp:BoundField DataField="ShiftOutTime" HeaderText="ShiftOutTime" />
                                    
                                    <asp:BoundField DataField="FromDate" DataFormatString="{0:dd-MMM-yyyy}" 
                                        HeaderText="From Date" />
                                    
                                    <asp:BoundField DataField="ToDate" DataFormatString="{0:dd-MMM-yyyy}" 
                                        HeaderText="To Date" />
                                        
                                        <asp:TemplateField HeaderText="Delete">
                                            
                                            
                                                <ItemTemplate>
                                                <asp:ImageButton ID="deleteImageButton" runat="server"
                                                ImageUrl="~/Assets/delete-icon.png" OnClick="deleteImageButton_OnClick" CommandArgument='<%# Container.DataItemIndex %>' CommandName="Delete" OnClientClick="return confirm(' Do you want to Delete data  ?');" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

