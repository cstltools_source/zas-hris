﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_ShiftWiseGroupView : System.Web.UI.Page
{
    ShiftWiseGroupBLL  aShiftWiseGroupBll=new ShiftWiseGroupBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();
        }
    }

    public void LoadData()
    {
        DataTable dtdata = aShiftWiseGroupBll.LoadShiftGroupView();
        loadGridView.DataSource = dtdata;
        loadGridView.DataBind();
    }
    protected void addNewButton_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("ShiftWiseGroupEntry.aspx");
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            if (aShiftWiseGroupBll.DeleteShiftGroupDAL(loadGridView.DataKeys[rowindex][0].ToString()))
            {
                showMessageBox("Data Deleted !!");
                LoadData();
            }
        }

    }
    protected void reloadButton_OnClick(object sender, EventArgs e)
    {
        LoadData();   
    }

    protected void deleteImageButton_OnClick(object sender, ImageClickEventArgs e)
    {
        
    }
}