﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="TaxDetailsEntry.aspx.cs" Inherits="HRM_UI_TaxDetailsEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        table.tablestyle {
            border-collapse: collapse;
            border: 1px solid #8cacbb;
        }

        table {
            text-align: left;
        }

        .FixedHeader {
            position: absolute;
            font-weight: bold;
        }
    </style>

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Tax Details </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--                        <asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentNewImageButton_Click" />
                        <asp:Button ID="reloadButton" Text="Refresh" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="deptReloadImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Daily Task </a></li>
                            <li class="breadcrumb-item"><a href="#">Tax Details</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Financial Year </label>
                                        <asp:DropDownList ID="financialDropDownList" runat="server" CssClass="form-control form-control-sm"></asp:DropDownList>
                                    </div>
                                </div>
                                
                                <div class="col-2">
                                    <div class="form-group">
                                        <label> Employee Code </label>
                                        <asp:TextBox ID="empCodeTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <label style="color: white;">Submith </label>
                                    <br />
                                    <asp:Button ID="subButton" runat="server" CssClass="btn btn-sm btn-info" OnClick="searchButton_Click" Text="Search" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div id="gridContainer1" style="height: 360px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered text-center thead-dark" DataKeyNames="EmpInfoId">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SL">
                                            <ItemTemplate>
                                                <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="isempCheckBox" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EmpMasterCode" HeaderText="Code" />
                                    <asp:BoundField DataField="EmpName" HeaderText="Emp Name" />
                                    <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                    <asp:TemplateField HeaderText="Jan">
                                        <ItemTemplate>
                                            <asp:TextBox ID="janTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("January")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Feb">
                                        <ItemTemplate>
                                            <asp:TextBox ID="febTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("February")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mar">
                                        <ItemTemplate>
                                            <asp:TextBox ID="marTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("March")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Apr">
                                        <ItemTemplate>
                                            <asp:TextBox ID="aprTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("April")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="May">
                                        <ItemTemplate>
                                            <asp:TextBox ID="mayTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("May")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jun">
                                        <ItemTemplate>
                                            <asp:TextBox ID="junTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("June")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Jul">
                                        <ItemTemplate>
                                            <asp:TextBox ID="julTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("July")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aug">
                                        <ItemTemplate>
                                            <asp:TextBox ID="augTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("August")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sep">
                                        <ItemTemplate>
                                            <asp:TextBox ID="sepTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("September")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Oct">
                                        <ItemTemplate>
                                            <asp:TextBox ID="octTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("October")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nov">
                                        <ItemTemplate>
                                            <asp:TextBox ID="novTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("November")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dec">
                                        <ItemTemplate>
                                            <asp:TextBox ID="decTextBox" runat="server" CssClass="form-control form-control-sm" Text='<%# Eval("December")%>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                             <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

