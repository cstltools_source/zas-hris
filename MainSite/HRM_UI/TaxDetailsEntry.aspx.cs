﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_TaxDetailsEntry : System.Web.UI.Page
{
    TaxBLL aTaxBll=new TaxBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        }
    }

    public void DropDownList()
    {
        aTaxBll.LoadFinancialYear(financialDropDownList);
    }

    protected void searchButton_Click(object sender, EventArgs e)
    {
        string pram = "";

        if (empCodeTextBox.Text != "")
        {
            pram = pram + " AND tbltemp.EmpMasterCode = '" + empCodeTextBox.Text.Trim() + "'";
        }

        DataTable dtempinfo = aTaxBll.GetEmployeeInformation(pram);

        loadGridView.DataSource = dtempinfo;
        loadGridView.DataBind();

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox selectCheckBox = (CheckBox)loadGridView.Rows[i].Cells[0].FindControl("isempCheckBox");

            DataTable dt =
                aTaxBll.GetEmployeeInformation("AND tbltemp.EmpInfoId='" + loadGridView.DataKeys[i][0].ToString() + "'");

            selectCheckBox.Checked = Convert.ToBoolean(dt.Rows[0]["IsCheck"].ToString());

        }
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        List<TaxDetails> aTaxDetailsList = new List<TaxDetails>();

        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox selectCheckBox = (CheckBox)loadGridView.Rows[i].Cells[0].FindControl("isempCheckBox");

            if (selectCheckBox.Checked)
            {

                TaxDetails aTaxDetails = new TaxDetails();

                aTaxDetails.FyrCode = Convert.ToInt32(financialDropDownList.SelectedValue);
                aTaxDetails.EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString());

                if (((TextBox)loadGridView.Rows[i].Cells[4].FindControl("janTextBox")).Text !=string.Empty)
                {
                    aTaxDetails.January =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[4].FindControl("janTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[5].FindControl("febTextBox")).Text !=string.Empty)
                {
                    aTaxDetails.February =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[5].FindControl("febTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[15].FindControl("decTextBox")).Text != string.Empty)
                {
                    aTaxDetails.December =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[15].FindControl("decTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[14].FindControl("novTextBox")).Text != string.Empty)
                {
                    aTaxDetails.November =
                   Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[14].FindControl("novTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[13].FindControl("octTextBox")).Text != string.Empty)
                {
                    aTaxDetails.October =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[13].FindControl("octTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[12].FindControl("sepTextBox")).Text != string.Empty)
                {
                    aTaxDetails.September =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[12].FindControl("sepTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[11].FindControl("augTextBox")).Text != string.Empty)
                {
                    aTaxDetails.August =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[11].FindControl("augTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[10].FindControl("julTextBox")).Text != string.Empty)
                {
                    aTaxDetails.July =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[10].FindControl("julTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[9].FindControl("junTextBox")).Text != string.Empty)
                {
                    aTaxDetails.June =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[9].FindControl("junTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[8].FindControl("mayTextBox")).Text != string.Empty)
                {
                    aTaxDetails.May =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[8].FindControl("mayTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[7].FindControl("aprTextBox")).Text != string.Empty)
                {
                    aTaxDetails.April =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[7].FindControl("aprTextBox")).Text);
                }
                if (((TextBox)loadGridView.Rows[i].Cells[6].FindControl("marTextBox")).Text != string.Empty)
                {
                    aTaxDetails.March =
                    Convert.ToDecimal(((TextBox)loadGridView.Rows[i].Cells[6].FindControl("marTextBox")).Text);
                }

                
                
                
                
                
               
                
                aTaxDetails.IsActive = selectCheckBox.Checked;

                aTaxDetailsList.Add(aTaxDetails);

                aTaxBll.UpdateTaxDetail(aTaxDetails);
            }
        }

        if (aTaxBll.SaveDataForTaxDetails(aTaxDetailsList))
        {
            loadGridView.DataSource = null;
            loadGridView.DataBind();
            financialDropDownList.SelectedValue = null;
            showMessageBox("Data Saved Successfully");
        }
        
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

}