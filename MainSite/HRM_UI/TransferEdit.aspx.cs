﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_TransferEdit : System.Web.UI.Page
{
    TransferBLL _aTransferBLL = new TransferBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TransferIdHiddenField.Value = Request.QueryString["ID"];
            DropDownList();

            EmpTransferLoad(TransferIdHiddenField.Value);

        }
    }
    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _aTransferBLL.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
    }
    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _aTransferBLL.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
    protected void divisNamDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _aTransferBLL.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
    }

    public void DropDownList()
    {
        _aTransferBLL.LoadDivisionNameToDropDownBLL(divisNamDropDownList);
        _aTransferBLL.LoadCompanyNameToDropDownBLL(comNameDropDownList);

    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }

        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Transfer aTransfer = new Transfer()
            {
                TransferId = Convert.ToInt32(TransferIdHiddenField.Value),
                EmpInfoId = Convert.ToInt32(empIdHiddenField.Value),
                EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text),
                NewCompId = Convert.ToInt32(comNameDropDownList.SelectedValue),
                NewUnitId = Convert.ToInt32(unitNameDropDownList.SelectedValue),
                NewDivisionId = Convert.ToInt32(divisNamDropDownList.SelectedValue),
                NewDeptId = Convert.ToInt32(departmentDropDownList.SelectedValue),
                NewSectionId = Convert.ToInt32(sectionDropDownList.SelectedValue),
                ActionStatus = "Transfer"
            };

            if (!_aTransferBLL.UpdateDataForEmpSalBenefit(aTransfer))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void EmpTransferLoad(string TransferId)
    {
        Transfer aTransfer = new Transfer();
        aTransfer = _aTransferBLL.TransferEditLoad(TransferId);
        effectDateTexBox.Text = aTransfer.EffectiveDate.ToString("dd-MMM-yyyy");
        comIdHiddenField.Value = aTransfer.NewCompId.ToString();
        unitIdHiddenField.Value = aTransfer.NewUnitId.ToString();
        divIdHiddenField.Value = aTransfer.NewDivisionId.ToString();
        deptIdHiddenField.Value = aTransfer.NewDeptId.ToString();
        secIdHiddenField.Value = aTransfer.NewSectionId.ToString();
        empIdHiddenField.Value = aTransfer.EmpInfoId.ToString();


        GetEmpMasterCode(empIdHiddenField.Value);
        GetNames(comIdHiddenField.Value, unitIdHiddenField.Value, divIdHiddenField.Value, deptIdHiddenField.Value, secIdHiddenField.Value);
    }

    public void GetEmpMasterCode(string EmpInfoId)
    {
        if (!string.IsNullOrEmpty(EmpInfoId))
        {
            DataTable aTable = new DataTable();
            aTable = _aTransferBLL.LoadEmpInfoCode(EmpInfoId);

            if (aTable.Rows.Count > 0)
            {
                EmpMasterCodeTextBox.Text = aTable.Rows[0]["EmpMasterCode"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
        }
    }

    public void GetNames(string comId, string unitId, string divId, string deptId, string secId)
    {

        DataTable aTableCom = new DataTable();
        DataTable aTableUnit = new DataTable();
        DataTable aTableCDiv = new DataTable();
        DataTable aTableDept = new DataTable();
        DataTable aTableDesig = new DataTable();
        DataTable aTableSec = new DataTable();
        DataTable aTableGrade = new DataTable();
        DataTable aTableType = new DataTable();
        aTableCom = _aTransferBLL.LoadCompanyInfo(comId);
        aTableUnit = _aTransferBLL.LoadUnit(unitId);
        aTableCDiv = _aTransferBLL.Loadivision(divId);
        aTableDept = _aTransferBLL.LoadDepartment(deptId);
        // aTableDesig = _aTransferBLL.LoadDesignation(desigId);
        aTableSec = _aTransferBLL.LoadSection(secId);
        // aTableGrade = _aTransferBLL.LoadGrade(gradeId);
        // aTableType = _aTransferBLL.LoadEmpType(emptId);


        if (aTableCom.Rows.Count > 0)
        {
            comNameDropDownList.SelectedValue = aTableCom.Rows[0]["CompanyInfoId"].ToString().Trim();
            _aTransferBLL.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
        }
        if (aTableUnit.Rows.Count > 0)
        {
            unitNameDropDownList.SelectedValue = aTableUnit.Rows[0]["UnitId"].ToString().Trim();
        }

        if (aTableCDiv.Rows.Count > 0)
        {
            divisNamDropDownList.Text = aTableCDiv.Rows[0]["DivisionId"].ToString().Trim();
            _aTransferBLL.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
        }
        if (aTableDept.Rows.Count > 0)
        {
            departmentDropDownList.SelectedValue = aTableDept.Rows[0]["DeptId"].ToString().Trim();
            _aTransferBLL.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
        }
        //if (aTableDesig.Rows.Count > 0)
        //{
        //    desigNameTextBox.Text = aTableDesig.Rows[0]["DesigName"].ToString().Trim();
        //}
        if (aTableSec.Rows.Count > 0)
        {
            sectionDropDownList.SelectedValue = aTableSec.Rows[0]["SectionId"].ToString().Trim();
        }
        //if (aTableGrade.Rows.Count > 0)
        //{
        //    empGradeTextBox.Text = aTableGrade.Rows[0]["GradeName"].ToString().Trim();
        //}
        //if (aTableType.Rows.Count > 0)
        //{
        //    empTypeLabel.Text = aTableType.Rows[0]["EmpType"].ToString().Trim();
        //}

    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}