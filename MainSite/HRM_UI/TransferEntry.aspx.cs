﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_TransferEntry : System.Web.UI.Page
{
    TransferBLL _aTransferBll=new TransferBLL(); 
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        }
    }
    
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        EmpMasterCodeTextBox.Text = string.Empty;
        empNameTexBox.Text = string.Empty;
        effectDateTexBox.Text = string.Empty;
        comIdHiddenField.Value = null;
        unitIdHiddenField.Value = null;
        divIdHiddenField.Value = null;
        deptIdHiddenField.Value = null;
        secIdHiddenField.Value = null;
        desigIdHiddenField.Value = null;
        comNameLabel.Text = string.Empty;
        unitNameLabel.Text = string.Empty;
        divNameLabel.Text = string.Empty;
        deptNameLabel.Text = string.Empty;
        secNameLabel.Text = string.Empty;
        desigNameLabel.Text = string.Empty;
        comNameDropDownList.SelectedValue = null;
        unitNameDropDownList.SelectedValue = null;
        divisNamDropDownList.SelectedValue = null;
        departmentDropDownList.SelectedValue = null;
        sectionDropDownList.SelectedValue = null;
        
    }
    protected void departmentDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _aTransferBll.LoadSectionNameToDropDownBLL(sectionDropDownList, departmentDropDownList.SelectedValue);
    }
    protected void comNameDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _aTransferBll.LoadUnitNameToDropDownBLL(unitNameDropDownList, comNameDropDownList.SelectedValue);
    }
    protected void divisNamDropDownList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _aTransferBll.LoadDepartmentToDropDownBLL(departmentDropDownList, divisNamDropDownList.SelectedValue);
    }
    
    public void DropDownList()
    {
        _aTransferBll.LoadDivisionNameToDropDownBLL(divisNamDropDownList);
        _aTransferBll.LoadCompanyNameToDropDownBLL(comNameDropDownList);
        
    }
    private bool Validation()
    {

        if (effectDateTexBox.Text == "")
        {
            showMessageBox("Please Input Effect Date !!");
            return false;
        }
        
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input Employee Code !!");
            return false;
        }
        if (empNameTexBox.Text == "")
        {
            showMessageBox("Please Input Employee Name !!");
            return false;
        }
        if (EffectDate() == false)
        {
            showMessageBox("Please give a valid Effect Date !!!");
            return false;
        }
        
        return true;
    }
    public bool EffectDate()
    {
        try
        {
            DateTime aDateTime = new DateTime();
            aDateTime = Convert.ToDateTime(effectDateTexBox.Text);
        }
        catch (Exception)
        {
            return false;
        }
        return true;
    }

    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            Transfer aTransfer = new Transfer();
            aTransfer.EmpInfoId = Convert.ToInt32(EmpInfoIdHiddenField.Value);
            aTransfer.EffectiveDate = Convert.ToDateTime(effectDateTexBox.Text);
            aTransfer.CompanyInfoId = Convert.ToInt32(comIdHiddenField.Value);
            aTransfer.UnitId = Convert.ToInt32(unitIdHiddenField.Value);
            aTransfer.DivisionId = Convert.ToInt32(divIdHiddenField.Value);
            aTransfer.DeptId = Convert.ToInt32(deptIdHiddenField.Value);
            aTransfer.SectionId = Convert.ToInt32(secIdHiddenField.Value);
            aTransfer.EntryUser = Session["LoginName"].ToString();
            aTransfer.EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            aTransfer.ActionStatus = "Posted";
            aTransfer.NewUnitId = Convert.ToInt32(unitNameDropDownList.SelectedValue);
            aTransfer.NewDivisionId = Convert.ToInt32(divisNamDropDownList.SelectedValue);
            aTransfer.NewDeptId = Convert.ToInt32(departmentDropDownList.SelectedValue);
            aTransfer.NewSectionId = Convert.ToInt32(sectionDropDownList.SelectedValue);
            aTransfer.NewCompId = Convert.ToInt32(comNameDropDownList.SelectedValue);
            aTransfer.IsActive = true;
            
            
            if (_aTransferBll.SaveDataForTransfer(aTransfer))
            {
                showMessageBox("Data Save Successfully ");
                Clear();
            }
            else
            {
                showMessageBox(" Transfer already Posted !!!!");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void departmentListImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("TransferView.aspx");
    }
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(EmpMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = _aTransferBll.LoadEmpInfo(EmpMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                EmpInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTexBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
                comNameLabel.Text = aTable.Rows[0]["CompanyName"].ToString().Trim();
                unitNameLabel.Text = aTable.Rows[0]["UnitName"].ToString().Trim();
                divNameLabel.Text = aTable.Rows[0]["DivName"].ToString().Trim();
                deptNameLabel.Text = aTable.Rows[0]["DeptName"].ToString().Trim();
                secNameLabel.Text = aTable.Rows[0]["SectionName"].ToString().Trim();
                desigNameLabel.Text = aTable.Rows[0]["DesigName"].ToString().Trim();
                comIdHiddenField.Value = aTable.Rows[0]["CompanyInfoId"].ToString().Trim();
                unitIdHiddenField.Value = aTable.Rows[0]["UnitId"].ToString().Trim();
                divIdHiddenField.Value = aTable.Rows[0]["DivisionId"].ToString().Trim();
                deptIdHiddenField.Value = aTable.Rows[0]["DeptId"].ToString().Trim();
                secIdHiddenField.Value = aTable.Rows[0]["SectionId"].ToString().Trim();
                desigIdHiddenField.Value = aTable.Rows[0]["DesigId"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();
    }
}