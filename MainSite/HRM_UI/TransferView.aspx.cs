﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;

public partial class HRM_UI_TransferView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    TransferBLL _aTransferBLL = new TransferBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            EmpTransferLoad();
        }
    }

    private void EmpTransferLoad()
    {
        aDataTable = _aTransferBLL.LoadTransfer();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
        _aTransferBLL.CancelDataMarkBLL(loadGridView, aDataTable);
    }
    
    protected void deptReloadImageButton_Click(object sender, EventArgs eventArgs)
    {
        EmpTransferLoad();
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("TransferEntry.aspx");
    }
    private void PopUp(string Id)
    {
        string url = "TransferEdit.aspx?ID=" + Id;
        string fullURL = "var Mleft = (screen.width/2)-(700/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url+"', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string TransferId = loadGridView.DataKeys[rowindex][0].ToString();
            PopUp(TransferId);
        }
    }
    protected void yesButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            CheckBox checkBox = (CheckBox)loadGridView.Rows[i].Cells[5].FindControl("chkDelete");

            if (checkBox.Checked)
            {
                string TransferId = loadGridView.DataKeys[i][0].ToString();
                _aTransferBLL.DeleteDataBLL(TransferId);
            }
            EmpTransferLoad();
        }

    }
}