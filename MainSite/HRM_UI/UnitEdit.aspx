﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnitEdit.aspx.cs" Inherits="HRM_UI_UnitEdit" %>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3" />
</head>
<body>
    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <div class="content" id="content">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <!-- PAGE HEADING -->
                    <div class="page-heading">
                        <div class="page-heading__container">
                            <div class="icon"><span class="li-register"></span></div>
                            <span></span>
                            <h1 class="title" style="font-size: 18px; padding-top: 9px;">Company Unit Edit </h1>
                        </div>
                        <div class="page-heading__container float-right d-none d-sm-block">
                            <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="viewListImageButton_Click" />--%>
                            <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Company Unit Edit</a></li>

                        </ol>
                    </nav>
                    </div>
                    <!-- //END PAGE HEADING -->
                    <br/>
                    <br/>
                   <div class="container-fluid">
                        <div class="card">
                            <div class="card-body">
                                <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                                <div class="form-row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Unit Name </label>
                                            <asp:TextBox ID="unitNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            <asp:HiddenField ID="unitIdHiddenField" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Address </label>
                                            <asp:TextBox ID="unitaddressTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                   <div class="col-3">
                                        <div class="form-group">
                                            <label>Phone Number </label>
                                            <asp:TextBox ID="phoneNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Mobile Number </label>
                                            <asp:TextBox ID="unitMobileNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Fax Number </label>
                                            <asp:TextBox ID="faxNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label>Company </label>
                                            <asp:DropDownList ID="companyNameDropDownList" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <asp:Button ID="Button1" Text="Update" CssClass="btn btn-sm btn-info" runat="server" OnClick="updateButton_Click" />
                                            <asp:Button ID="cancelButton" Text="Close"  CssClass="btn btn-sm warning" runat="server" OnClick="closeButton_Click" BackColor="#FF9900" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>

    <!-- IMPORTANT SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- END IMPORTANT SCRIPTS -->
    <!-- THIS PAGE SCRIPTS ONLY -->
    <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
    <!-- //END THIS PAGE SCRIPTS ONLY -->
    <!-- TEMPLATE SCRIPTS -->
    <script type="text/javascript" src="../Assets/js/app.js"></script>
    <script type="text/javascript" src="../Assets/js/plugins.js"></script>
    <script type="text/javascript" src="../Assets/js/demo.js"></script>
    <script type="text/javascript" src="../Assets/js/settings.js"></script>
    <!-- END TEMPLATE SCRIPTS -->
    <!-- THIS PAGE DEMO -->
    <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
    <!-- //THIS PAGE DEMO -->
</body>

</html>