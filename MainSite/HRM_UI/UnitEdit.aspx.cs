﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_UnitEdit : System.Web.UI.Page
{
    CompanyUnitBLL aCompanyUnitBll = new CompanyUnitBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            unitIdHiddenField.Value = Request.QueryString["ID"];
            CompanyName();
            CompanyUnitLoad(unitIdHiddenField.Value);
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (unitNameTextBox.Text == "")
        {
            showMessageBox("Please Input Unit Name!!");
            return false;
        }
        if (unitaddressTextBox.Text == "")
        {
            showMessageBox("Please Input Unit Address !!");
            return false;
        }
        if (companyNameDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Company Name!!");
            return false;
        }
        return true;
    }

    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation())
        {
            CompanyUnit aCompanyUnit = new CompanyUnit()
            {
                UnitId = Convert.ToInt32(unitIdHiddenField.Value),
                CompanyInfoId = Convert.ToInt32(companyNameDropDownList.SelectedValue),
                UnitName = unitNameTextBox.Text,
                UnitAddress = unitaddressTextBox.Text,
                PhoneNo = phoneNoTextBox.Text,
                MobileNo = unitMobileNoTextBox.Text,
                FaxNo = faxNoTextBox.Text,
               
            };
            CompanyUnitBLL aCompanyUnitBll = new CompanyUnitBLL();

            if (!aCompanyUnitBll.UpdateDataForCompanyUnit(aCompanyUnit))
            {
                showMessageBox("Data Not Update!!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void CompanyUnitLoad(string unitId)
    {
        CompanyUnit aCompanyUnit = new CompanyUnit();
        
        aCompanyUnit = aCompanyUnitBll.CompanyUnitEditLoad(unitId);
        unitNameTextBox.Text = aCompanyUnit.UnitName;
        unitaddressTextBox.Text = aCompanyUnit.UnitAddress;
        unitMobileNoTextBox.Text = aCompanyUnit.MobileNo;
        phoneNoTextBox.Text = aCompanyUnit.PhoneNo;
        faxNoTextBox.Text = aCompanyUnit.FaxNo;
        companyNameDropDownList.SelectedValue = Convert.ToString(aCompanyUnit.CompanyInfoId);
    }

    public void CompanyName()
    {
        CompanyUnitBLL aUnitBll = new CompanyUnitBLL();
        aUnitBll.LoadCompayName(companyNameDropDownList);
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}