﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="UnitEntry.aspx.cs" Inherits="HRM_UI_UnitEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        
   <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-user"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Company Unit Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="viewListImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup</a></li>
                            <li class="breadcrumb-item"><a href="#">Company Unit Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                                    <div class="form-row">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Company Unite Name :</label>
                                                <asp:TextBox ID="unitnameTextBox" runat="server" class="form-control form-control-sm" ></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Unite Address :</label>
                                                <asp:TextBox ID="addressTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Phone No :</label>
                                                <asp:TextBox ID="phoneNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                       </div>
                                       <div class="form-row">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Mobile No :</label>
                                                <asp:TextBox ID="mobileNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Fax No </label> 
                                                <asp:TextBox ID="faxNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Company Name :</label>
                                                <asp:DropDownList ID="companyCodeDropDownList" runat="server" AutoPostBack="True" 
                                                   CssClass="form-control form-control-sm"> </asp:DropDownList>
                                            </div>
                                        </div>
                                       </div>
                                        <div class="form-row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <asp:Button ID="submitButton" Text="Save" CssClass="btn btn-sm btn-info btn-sm" runat="server" OnClick="submitButton_Click" />
                                                <%--<asp:Button ID="Button2" Text="Cancel" CssClass="btn btn-warning btn-sm" runat="server" OnClick="cancelButton_OnClick" />--%>
                                            </div>
                                        </div>
                                    </div>
                                     <asp:HiddenField ID="hiddenField" runat="server" />
                            
                                </div>
                            </div>
                         
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
 
</asp:Content>

