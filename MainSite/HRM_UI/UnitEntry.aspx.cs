﻿using System;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_UnitEntry : System.Web.UI.Page
{
    CompanyUnitBLL aUnitBll = new CompanyUnitBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CompanyName();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void Clear()
    {
        unitnameTextBox.Text = string.Empty;
        addressTextBox.Text = string.Empty;
        phoneNoTextBox.Text = string.Empty;
        mobileNoTextBox.Text = string.Empty;
        faxNoTextBox.Text = string.Empty;
    }

    public void CompanyName()
    {
        aUnitBll.LoadCompayName(companyCodeDropDownList);
    }
    private bool Validation()
    {
        if (unitnameTextBox.Text == "")
        {
            showMessageBox("Please Input Department Name!!");
            return false;
        }
        if (addressTextBox.Text == "")
        {
            showMessageBox("Please Input Department Name!!");
            return false;
        }
        if (phoneNoTextBox.Text == "")
        {
            showMessageBox("Please Input Department Name!!");
            return false;
        }
        if (companyCodeDropDownList.SelectedValue == "")
        {
            showMessageBox("Please Input Department Name!!");
            return false;
        }
        return true;
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            CompanyUnit aCompanyUnit = new CompanyUnit()
            {
                UnitName = unitnameTextBox.Text,
                UnitAddress = addressTextBox.Text,
                PhoneNo = phoneNoTextBox.Text,
                MobileNo = mobileNoTextBox.Text,
                FaxNo = mobileNoTextBox.Text,
                CompanyInfoId = Convert.ToInt32(companyCodeDropDownList.SelectedValue)

            };
            if (aUnitBll.SaveCompanyCompanyUnit(aCompanyUnit))
            {
                showMessageBox("Data Save Successfully Company Unit Code is :  " + aCompanyUnit.UnitCode + "    And Company Unit Name is :   " + aCompanyUnit.UnitName);
                Clear();
                CompanyName();
            }
            else
            {
                showMessageBox("Company Unit Name already exist");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void viewListImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("UnitView.aspx");
    }

}