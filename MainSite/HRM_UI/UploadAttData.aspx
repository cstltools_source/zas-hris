﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="UploadAttData.aspx.cs" Inherits="HRM_UI_UploadAttData" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=16.1.0.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
     <div class="content" id="content">
        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>--%>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Upload Attendance Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <%--<asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentListImageButton_Click" />--%>
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup :</a></li>
                            <li class="breadcrumb-item"><a href="#">Upload Attendance Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Upload File :</label>
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label></label>
                                        <asp:Button ID="Button2" Text="Show" CssClass="btn btn-sm btn-info" runat="server" OnClick="Button2_OnClick" BackColor="#009900" Font-Bold="True" />
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        
                                    </div>
                                </div> 
                                <div class="col-2">
                                    <div class="form-group">
                                        
                                    </div>
                                </div>
                                <div class="col-2">
                                    <div class="form-group">
                                        <asp:Button ID="Button3" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="Button3_OnClick" />
                                        
                                    </div>
                                </div>
                               
                            </div>
                            <div class="form-row">
                                
                            </div>
                            <div class="form-row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered text-center thead-dark"  >
                                            <Columns>
                                                <asp:TemplateField HeaderText="SL">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="ATTDate" DataFormatString="{0:dd-MMM-yyyy}" HeaderText="Date" />
                                                <asp:BoundField DataField="Time" HeaderText="Time" />
                                                <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                                
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>

                                
                            </div>
                            
                        </div>
                    </div>

                </div>
            <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </div>
</asp:Content>

