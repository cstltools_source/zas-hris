﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.HRM_DAL;

public partial class HRM_UI_UploadAttData : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button3_OnClick(object sender, EventArgs e)
    {
        foreach (GridViewRow row in loadGridView.Rows)
        {
            AttendanceProcessDAL attendanceProcessDal=new AttendanceProcessDAL();
            try
            {
                DateTime prtime = Convert.ToDateTime(row.Cells[1].Text + " " + row.Cells[2].Text);
                if (attendanceProcessDal.CheckEmployee(row.Cells[3].Text, prtime).Rows.Count == 0)
                {
                    attendanceProcessDal.SaveProximityData(row.Cells[3].Text, row.Cells[1].Text, prtime);
                    
                }
                else
                {
                    attendanceProcessDal.DeleteProximityData(row.Cells[3].Text, Convert.ToDateTime(row.Cells[1].Text));
                    attendanceProcessDal.SaveProximityData(row.Cells[3].Text, row.Cells[1].Text, prtime);
                }
            }
            catch (Exception)
            {
                
                
            }
            

        }
        showMessageBox("Data Processed");
        loadGridView.DataSource = null;
        loadGridView.DataBind();

    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    

    public void GetData()
    {
        DataTable dtdata = new DataTable();
        dtdata.Columns.Add("ATTDate");
        dtdata.Columns.Add("Time");
        dtdata.Columns.Add("EmpMasterCode");

        DataRow dataRow = null;




        FileUpload fu = FileUpload1;
        if (fu.HasFile)
        {
            StreamReader reader = new StreamReader(fu.FileContent);
            do
            {
                string textLine = reader.ReadLine();
                string[] dividetext = textLine.Split(' ');
                string date =dividetext[0].Substring(2, 2) + "-" + dividetext[0].Substring(4, 2) + "-20" + dividetext[0].Substring(0, 2);
                DateTime maindate = Convert.ToDateTime(date);
                string time =dividetext[1].Substring(0, 2)+":"+dividetext[1].Substring(2, 2) + ":" + dividetext[1].Substring(4, 2);
                TimeSpan maintime = Convert.ToDateTime(time).TimeOfDay;
                
                try
                {
                    string mastercode = dividetext[2];
                    if (!string.IsNullOrEmpty(mastercode) && mastercode != "" && mastercode != " ")
                    {


                        dataRow = dtdata.NewRow();

                        dataRow["ATTDate"] = maindate.ToString("dd-MMM-yyyy");
                        dataRow["Time"] = maintime;
                        dataRow["EmpMasterCode"] = mastercode;

                        dtdata.Rows.Add(dataRow);
                    }
                }
                catch (Exception)
                {
                    
                    
                }

            } while (reader.Peek() != -1);
            reader.Close();
        }

        loadGridView.DataSource = dtdata;
        loadGridView.DataBind();
    }


    protected void Button2_OnClick(object sender, EventArgs e)
    {
        //populateListBox();
        GetData();
    }
}