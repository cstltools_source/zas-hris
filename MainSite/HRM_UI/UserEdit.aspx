﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserEdit.aspx.cs" Inherits="HRM_UI_UserEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Edit</title>
    <link rel="stylesheet" href="../Assets/css/styles2c70.css?v=1.0.3"/>
</head>
<body>
    <form id="form1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server">
     </asp:ScriptManager>
      <asp:HiddenField ID="userIdHiddenField" runat="server" />
          <div class="page-heading">
                  <div class="page-heading__container">
                      <div class="icon"><span class="li-user"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">User Information Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">User Administration</a></li>
                            <li class="breadcrumb-item"><a href="#">User Information Entry</a></li>

                        </ol>
                    </nav>
                </div>
                 <br/>
                    <br/>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                                    <div class="form-row">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Employee Master Code </label>
                                                <asp:TextBox ID="empMasterCodeTextBox" runat="server" class="form-control form-control-sm" ></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Employee Name </label>
                                                <asp:TextBox ID="empNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Employee Type </label>
                                                <asp:DropDownList ID="empTypeDropDownList" runat="server"
                                                    AutoPostBack="True" class="form-control form-control-sm">
                                                    <asp:ListItem Selected="True">Select any one</asp:ListItem>
                                                    <asp:ListItem>Admin</asp:ListItem>
                                                    <asp:ListItem>Employee</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Login Name </label>
                                                <asp:TextBox ID="logingNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Passward </label>
                                                <asp:TextBox ID="passwordNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>User Status </label>
                                                <asp:DropDownList ID="userStatusNameDropDownList" runat="server"
                                                    AutoPostBack="True" class="form-control form-control-sm" >
                                                    <asp:ListItem Selected="True">Select any one</asp:ListItem>
                                                    <asp:ListItem>Active</asp:ListItem>
                                                    <asp:ListItem>Inactive</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Email Adress</label>
                                                <asp:TextBox ID="emailNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Contact Number </label>
                                                <asp:TextBox ID="contactNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                         <div class="col-3">
                                            <div class="form-group">
                                                <label>User Unit </label>
                                                <asp:CheckBoxList ID="unitCheckBoxList" runat="server"></asp:CheckBoxList>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <asp:Button ID="submitButton" Text="Update" CssClass="btn btn-sm btn-info btn-sm" runat="server" OnClick="updateButton_Click1" />
                                                <asp:Button ID="Button2" Text="Close" CssClass="btn btn-warning btn-sm" runat="server" OnClick="CloseButton_OnClick" />
                                            </div>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="hiddenField" runat="server" />
                            
                                </div>
                            </div>
                         
                        </div>
                   
    </form>
    
     <!-- IMPORTANT SCRIPTS -->
        <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="../Assets/js/vendors/jquery/jquery-migrate.min.js"></script>
        <script type="text/javascript" src="../Assets/js/vendors/bootstrap/bootstrap.bundle.min.js"></script>
        <script type="text/javascript" src="../Assets/js/vendors/mcustomscrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
        <!-- END IMPORTANT SCRIPTS -->
        <!-- THIS PAGE SCRIPTS ONLY -->
        <script type="text/javascript" src="../Assets/js/vendors/moment/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="../Assets/js/vendors/echarts/echarts.min.js"></script>
        <script type="text/javascript" src="../Assets/js/vendors/select2/select2.min.js"></script>
        <script type="text/javascript" src="../Assets/js/vendors/daterangepicker/daterangepicker.js"></script>
        <script type="text/javascript" src="../Assets/js/vendors/raty/jquery.raty.js"></script>
        <!-- //END THIS PAGE SCRIPTS ONLY -->
        <!-- TEMPLATE SCRIPTS -->
        <script type="text/javascript" src="../Assets/js/app.js"></script>
        <script type="text/javascript" src="../Assets/js/plugins.js"></script>
        <script type="text/javascript" src="../Assets/js/demo.js"></script>
        <script type="text/javascript" src="../Assets/js/settings.js"></script>
        <!-- END TEMPLATE SCRIPTS -->
        <!-- THIS PAGE DEMO -->
        <script type="text/javascript" src="../Assets/js/demo_dashboard.js"></script>
        <!-- //THIS PAGE DEMO -->
</body>
</html>
