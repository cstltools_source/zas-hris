﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAO.UA_DAO;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_UserEdit : System.Web.UI.Page
{
    UserBLL aUserBll = new UserBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UnitCheckBox();
            userIdHiddenField.Value = Request.QueryString["ID"];

            UserLoad(userIdHiddenField.Value);
        }
    }
    public void UnitCheckBox()
    {
        DataTable dtunitdata = aUserBll.LoadUserUnit();
        unitCheckBoxList.DataValueField = "UnitId";
        unitCheckBoxList.DataTextField = "UnitName";
        unitCheckBoxList.DataSource = dtunitdata;
        unitCheckBoxList.DataBind();
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (empNameTextBox.Text == "")
        {
            showMessageBox("Please Input User Name!!");
            return false;
        }
        if (logingNameTextBox.Text == "")
        {
            showMessageBox("Please Input Loging Name!!");
            return false;
        }

        if (passwordNameTextBox.Text == "")
        {
            showMessageBox("Please Input Password!!");
            return false;
        }
        if (contactNoTextBox.Text == "")
        {
            showMessageBox("Please Input Contact Number!!");
            return false;
        }
        return true;
    }

    protected void updateButton_Click1(object sender, EventArgs e)
    {
        if (Validation() == true)
        {

            UserInformation aUserInformation = new UserInformation()
            {
                UserId = Convert.ToInt32(userIdHiddenField.Value),
                UserName = empNameTextBox.Text,
                UserType = empTypeDropDownList.Text,
                LoginName = logingNameTextBox.Text,
                Password = passwordNameTextBox.Text,
                UserStatus = userStatusNameDropDownList.Text,
                Email = emailNameTextBox.Text,
                ContactNo = contactNoTextBox.Text,
                EmpMasterCode = empMasterCodeTextBox.Text
            };
            UserBLL aBll = new UserBLL();

            if (!aBll.UpdateDataForUser(aUserInformation))
            {
                showMessageBox("Data Not Update!!!");

            }
            else
            {
                aUserBll.DeleteUserUnitInfo(userIdHiddenField.Value);
                for (int i = 0; i < unitCheckBoxList.Items.Count; i++)
                {
                    if (unitCheckBoxList.Items[i].Selected)
                    {
                        UserUnitDAO aUserUnitDao = new UserUnitDAO()
                        {
                            UnitId = Convert.ToInt32(unitCheckBoxList.Items[i].Value),
                            UserId = aUserInformation.UserId,
                        };
                        aUserBll.SaveUserUnit(aUserUnitDao);
                    }
                }
                showMessageBox("Data Update Successfully!!! Please Reload");
            }

        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void UserLoad(string userId)
    {
        UserInformation aInformation = new UserInformation();
        aInformation = aUserBll.UserEditLoad(userId);
        DataTable dtuserunit = aUserBll.LoadUserUnitById(userId);
        if (dtuserunit.Rows.Count>0)
        {
            for (int i = 0; i < unitCheckBoxList.Items.Count; i++)
            {
                for (int j = 0; j < dtuserunit.Rows.Count; j++)
                {
                    if (unitCheckBoxList.Items[i].Value==dtuserunit.Rows[j]["UnitId"].ToString())
                    {
                        unitCheckBoxList.Items[i].Selected = true;
                    }
                }
            }
        }
        empNameTextBox.Text = aInformation.UserName;
        empTypeDropDownList.SelectedValue = aInformation.UserType;
        logingNameTextBox.Text = aInformation.LoginName;
        passwordNameTextBox.Text = aInformation.Password;
        userStatusNameDropDownList.SelectedValue = aInformation.UserStatus;
        emailNameTextBox.Text = aInformation.Email;
        contactNoTextBox.Text = aInformation.ContactNo;
        empMasterCodeTextBox.Text = aInformation.EmpMasterCode;
    }
 
    protected void CloseButton_OnClick(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}