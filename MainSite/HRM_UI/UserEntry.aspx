﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="UserEntry.aspx.cs" Inherits="HRM_UI_UserEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-user"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">User Information Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="viewListImageButton_Click" />
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">User Administration</a></li>
                            <li class="breadcrumb-item"><a href="#">User Information Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                              <div class="form-row">
                                 <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Code : </label>
                                         <asp:TextBox ID="empMasterCodeTextBox" runat="server" AutoPostBack="True" 
                                         CssClass="form-control form-control-sm" ontextchanged="empMasterCodeTextBox_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name : </label>
                                        <asp:TextBox ID="empNameTextBox" runat="server" CssClass="form-control form-control-sm" ReadOnly="True"
                                         AutoPostBack="True" ></asp:TextBox>
                                      </div>
                                   </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Employee Type </label>
                                                <asp:DropDownList ID="empTypeDropDownList" runat="server"
                                                    AutoPostBack="True" class="form-control form-control-sm">
                                                    <asp:ListItem Selected="True">Select any one</asp:ListItem>
                                                    <asp:ListItem>Admin</asp:ListItem>
                                                    <asp:ListItem>Employee</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Login Name </label>
                                                <asp:TextBox ID="logingNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                      </div>
                                     <div class="form-row">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Passward </label>
                                                <asp:TextBox ID="passwordNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>User Status </label>
                                                <asp:DropDownList ID="userStatusNameDropDownList" runat="server"
                                                    AutoPostBack="True" class="form-control form-control-sm" >
                                                    <asp:ListItem Selected="True">Select any one</asp:ListItem>
                                                    <asp:ListItem>Active</asp:ListItem>
                                                    <asp:ListItem>Inactive</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Email Adress</label>
                                                <asp:TextBox ID="emailNameTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>Contact Number </label>
                                                <asp:TextBox ID="contactNoTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                            </div>
                                        </div>
                                       </div>
                                    <div class="form-row">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label>User Unit </label>
                                                <asp:CheckBoxList ID="unitCheckBoxList" runat="server"></asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                     <br/>
                                     <div class="form-row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <asp:Button ID="submitButton" Text="Save" CssClass="btn btn-sm btn-info btn-sm" runat="server" OnClick="submitButton_Click" />
                                                <asp:Button ID="Button2" Text="Cancel" CssClass="btn btn-warning btn-sm" runat="server" OnClick="cancelButton_OnClick" />
                                            </div>
                                        </div>
                                    </div>
                                     <asp:HiddenField ID="hiddenField" runat="server" />
                                
                                </div>
                            </div>
                         
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

