﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAO.UA_DAO;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_UserEntry : System.Web.UI.Page
{
    UserBLL aUserBll = new UserBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            logingNameTextBox.Text = string.Empty;
            passwordNameTextBox.Text = string.Empty;
            UnitCheckBox();
        }
    }

    private void Clear()
    {
        empNameTextBox.Text = string.Empty;
        userStatusNameDropDownList.SelectedValue = string.Empty;
        logingNameTextBox.Text = string.Empty;
        passwordNameTextBox.Text = string.Empty;
        userStatusNameDropDownList.Text = string.Empty;
        emailNameTextBox.Text = string.Empty;
        contactNoTextBox.Text = string.Empty;
        empMasterCodeTextBox.Text = string.Empty;

    }

    public void UnitCheckBox()
    {
        DataTable dtunitdata = aUserBll.LoadUserUnit();
        unitCheckBoxList.DataValueField = "UnitId";
        unitCheckBoxList.DataTextField = "UnitName";
        unitCheckBoxList.DataSource = dtunitdata;
        unitCheckBoxList.DataBind();
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    private bool Validation()
    {
        if (empNameTextBox.Text == "")
        {
            showMessageBox("Please Input User Name!!");
            return false;
        }
        if (logingNameTextBox.Text == "")
        {
            showMessageBox("Please Input Loging Name!!");
            return false;
        }

        if (passwordNameTextBox.Text == "")
        {
            showMessageBox("Please Input Password!!");
            return false;
        }
        if (contactNoTextBox.Text == "")
        {
            showMessageBox("Please Input Contact Number!!");
            return false;
        }
        return true;
    }
    protected void empMasterCodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(empMasterCodeTextBox.Text.Trim()))
        {
            DataTable aTable = new DataTable();
            aTable = aUserBll.LoadEmpInfo(empMasterCodeTextBox.Text);

            if (aTable.Rows.Count > 0)
            {
                //empInfoIdHiddenField.Value = aTable.Rows[0]["EmpInfoId"].ToString().Trim();
                empNameTextBox.Text = aTable.Rows[0]["EmpName"].ToString().Trim();
            }
            else
            {
                showMessageBox("Data not Found");
            }
        }
        else
        {
            showMessageBox("Please Input Employee Code");
        }
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {
            UserInformation aUserInformation = new UserInformation()
            {
                EmpMasterCode = empMasterCodeTextBox.Text,
                UserName = empNameTextBox.Text,
                ContactNo = contactNoTextBox.Text,
                LoginName = logingNameTextBox.Text,
                Password = passwordNameTextBox.Text,
                UserStatus = userStatusNameDropDownList.SelectedItem.Text,
                Email = emailNameTextBox.Text,
                UserType = empTypeDropDownList.SelectedItem.Text,
                

            };
            UserBLL aBll = new UserBLL();
            if (aBll.SaveDataForUser(aUserInformation))
            {
                for (int i = 0; i < unitCheckBoxList.Items.Count; i++)
                {
                    if (unitCheckBoxList.Items[i].Selected)
                    {
                        UserUnitDAO aUserUnitDao = new UserUnitDAO()
                        {
                            UnitId = Convert.ToInt32(unitCheckBoxList.Items[i].Value),
                            UserId = aUserInformation.UserId,
                        };
                        aUserBll.SaveUserUnit(aUserUnitDao);
                    }
                }
                showMessageBox("User Saved  Successfully ");
                Clear();    
            }
            
            
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }
    protected void viewListImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        Clear();  
    }
}