﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="UserView.aspx.cs" Inherits="HRM_UI_UserView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content" id="content">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
            <ContentTemplate>
        <!-- PAGE HEADING -->
        <div class="page-heading">
            <div class="page-heading__container">
                <div class="icon"><span class="li-user"></span></div>
                <span></span>
                <h1 class="title" style="font-size: 18px; padding-top: 9px;"> User Information View </h1>
            </div>
            <div class="page-heading__container float-right d-none d-sm-block">
                <asp:Button ID="addNewButton" Text="Add New Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="addImageButton_Click" />
                <asp:Button ID="reloadButton" Text="Reload" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="reloadLinkButton_Click" />
            </div>
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">User Administration</a></li>
                    <li class="breadcrumb-item"><a href="#">User Information View</a></li>
                </ol>
            </nav>
        </div>
        <!-- //END PAGE HEADING -->

        <div class="container-fluid">
            <div class="card">
                <div class="card-body">


                    <div  style="height:  430px; overflow: auto; width: auto">
                            <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                 CssClass="table table-bordered text-center thead-dark" DataKeyNames="UserId" 
                                Font-Size="11px" OnRowCommand="loadGridView_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="SL">
                                        <ItemTemplate>
                                            <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                            <asp:HiddenField runat="server" ID="hfUserId" Value='<%#Eval("UserId") %>' />
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Master Code" />
                                    <asp:BoundField DataField="UserName" HeaderText="Employee Name" />
                                    <asp:BoundField DataField="UserType" HeaderText="Employee Type" />
                                    <asp:BoundField DataField="LoginName" HeaderText="Login Name" />
                                    <asp:BoundField DataField="Password" HeaderText="Password" />
                                    <asp:BoundField DataField="ContactNo" HeaderText="Contact No" />
                                    <asp:BoundField DataField="UserStatus" HeaderText="Employee Status" />
                                    <asp:TemplateField HeaderText="Actions">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="editImageButton" runat="server" class="btn btn-white btn-sm  " CommandArgument='<%#Eval("UserId") %>'
                                                data-toggle="modal" data-target="#modal_form" CommandName="EditData" ImageUrl="~/Assets/img/rsz_edit.png" />
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions">
                                        <ItemTemplate>
                                           <asp:ImageButton ID="deleteImageButton" runat="server" class="btn btn-white btn-sm  " CommandArgument='<%#Eval("UserId") %>'
                                                CommandName="DeleteData" OnClientClick="return confirm('Are you sure you want to delete the User Login ?');" ImageUrl="~/Assets/img/delete.png" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <%--<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NextPreviousFirstLast" NextPageText="Next" PreviousPageText="Prev" />--%>
                                <PagerStyle Width="50px" />
                            </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
                  </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>

