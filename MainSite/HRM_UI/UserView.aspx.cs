﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_UserView : System.Web.UI.Page
{
    DataTable aDataTable = new DataTable();
    UserBLL aUserBll = new UserBLL();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            UserLoad();
        }


    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    private void UserLoad()
    {
        aDataTable = aUserBll.LoadUserInfo();
        loadGridView.DataSource = aDataTable;
        loadGridView.DataBind();
    }

    private void PopUp(string Id)
    {
        string url = "UserEdit.aspx?ID=" + Id;
        string fullURL = @"var Mleft = (screen.width/2)-(1000/2);var Mtop = (screen.height/2)-(600/2);window.open( '" + url + "', null, 'height=600,width=1000,status=yes,toolbar=no,scrollbars=yes,menubar=no,location=no,top=\'+Mtop+\', left=\'+Mleft+\'' );";
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", fullURL, true);
    }


    protected void addImageButton_Click(object sender, EventArgs e)
    {
        Response.Redirect("UserEntry.aspx");
    }
    protected void reloadLinkButton_Click(object sender, EventArgs e)
    {
        UserLoad();
    }
    protected void loadGridView_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditData")
        {
            //int rowindex = Convert.ToInt32(e.CommandArgument);
            string userId = e.CommandArgument.ToString();
            PopUp(userId);
        }
        if (e.CommandName == "DeleteData")
        {
            int rowindex = Convert.ToInt32(e.CommandArgument);
            string userId = e.CommandArgument.ToString();
            UserInformation aUserInformation = new UserInformation();
            aUserInformation.UserId = Convert.ToInt32(userId);
            if (aUserBll.DeleteUserInfo(aUserInformation))
            {
                showMessageBox(" User Login Deleted Successfully !!!!");
            }

        }

    }
}