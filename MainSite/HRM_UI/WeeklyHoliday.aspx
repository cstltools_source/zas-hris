﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="WeeklyHoliday.aspx.cs" Inherits="HRM_UI_WeeklyHoliday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Weekly Holiday Entry </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                        <asp:Button ID="detailsViewButton" Text="View Details Information" CssClass="btn btn-sm btn-outline-secondary " runat="server" OnClick="departmentNewImageButton_Click" />
                        <%-- <asp:Button ID="reportViewButton" Text="Report" CssClass="btn btn-sm btn-outline-success" runat="server" OnClick="rptImageButton_Click" />--%>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">New Employee Related </a></li>
                            <li class="breadcrumb-item"><a href="#">Weekly Holiday Entry</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Code </label>
                                        <asp:TextBox ID="empcodeTextBox" runat="server" AutoPostBack="True" CssClass="form-control form-control-sm" OnTextChanged="empcodeTextBox_TextChanged"></asp:TextBox>
                                        <asp:HiddenField ID="empidHiddenField" runat="server" />
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Employee Name </label>
                                        <asp:TextBox ID="empnameTextBox" runat="server" CssClass="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Day Quantity </label>
                                        <asp:CheckBoxList ID="dayqCheckBoxList" runat="server" AutoPostBack="True"
                                            OnSelectedIndexChanged="dayqCheckBoxList_SelectedIndexChanged"
                                            CssClass="custom-control custom-checkbox">
                                            <asp:ListItem Value="1"> &nbsp; 1st Day</asp:ListItem>
                                            <asp:ListItem Value="2"> &nbsp; 2nd Day</asp:ListItem>
                                        </asp:CheckBoxList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Day 1 </label>
                                        <asp:DropDownList ID="fdayDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            <asp:ListItem Selected="True"></asp:ListItem>
                                            <asp:ListItem>Saturday</asp:ListItem>
                                            <asp:ListItem>Sunday</asp:ListItem>
                                            <asp:ListItem>Monday</asp:ListItem>
                                            <asp:ListItem>Tuesday</asp:ListItem>
                                            <asp:ListItem>Wednesday</asp:ListItem>
                                            <asp:ListItem>Thursday</asp:ListItem>
                                            <asp:ListItem>Friday</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row" id="divsday" runat="server" visible="False">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label>Day 2 </label>
                                        <asp:DropDownList ID="sdayDropDownList" runat="server" CssClass="form-control form-control-sm">
                                            <asp:ListItem Selected="True"></asp:ListItem>
                                            <asp:ListItem>Saturday</asp:ListItem>
                                            <asp:ListItem>Sunday</asp:ListItem>
                                            <asp:ListItem>Monday</asp:ListItem>
                                            <asp:ListItem>Tuesday</asp:ListItem>
                                            <asp:ListItem>Wednesday</asp:ListItem>
                                            <asp:ListItem>Thursday</asp:ListItem>
                                            <asp:ListItem>Friday</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <asp:Button ID="Button1" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <asp:Button ID="cancelButton" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

















