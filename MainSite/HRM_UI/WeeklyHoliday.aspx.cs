﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_WeeklyHoliday : System.Web.UI.Page
{
    WeeklyHolidayBLL aWeeklyHolidayBll = new WeeklyHolidayBLL();

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public  void clear()
    {
        dayqCheckBoxList.Items[0].Selected = false;
        dayqCheckBoxList.Items[1].Selected = false;
        empcodeTextBox.Text = string.Empty;
        empnameTextBox.Text = string.Empty;
        fdayDropDownList.SelectedValue = "";
        sdayDropDownList.SelectedValue = "";
    }
    private bool Validation()
    {

        if (empnameTextBox.Text == "")
        {
            showMessageBox("Please Input  Employee Name!!");
            return false;
        }
        if (empcodeTextBox.Text == "")
        {
            showMessageBox("Please Input  Employee Code!!");
            return false;
        }
        if (dayqCheckBoxList.Items[0].Selected == false)
        {
            showMessageBox("Select atlest 1 day!!");
            return false;
        }
        if (fdayDropDownList.SelectedValue == "")
        {
            showMessageBox("Select atlest 1 day!!");
            return false;
        }
        if (dayqCheckBoxList.SelectedValue == "2")
        {
            if (sdayDropDownList.SelectedValue == "")
            {
                showMessageBox("Select 2 day!!");
                return false;
            }
        }
        if (fdayDropDownList.SelectedValue == sdayDropDownList.SelectedValue)
        {
            showMessageBox("Don't Select Same days!!");
            return false;
        }
        return true;
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        if (Validation()==true)
        {
            WeeklyHoliday aWeeklyHoliday = new WeeklyHoliday();
            aWeeklyHoliday.EmpId = Convert.ToInt32(empidHiddenField.Value);
            if (dayqCheckBoxList.Items[0].Selected == true)
            {
                aWeeklyHoliday.FirstHolidayName = fdayDropDownList.SelectedItem.Text;
                aWeeklyHoliday.SecondHolidayName = "NONE";
                aWeeklyHoliday.DayQty = "1";
            }
            if (dayqCheckBoxList.Items[0].Selected == true && dayqCheckBoxList.Items[1].Selected == true)
            {
                aWeeklyHoliday.FirstHolidayName = fdayDropDownList.SelectedItem.Text;
                aWeeklyHoliday.SecondHolidayName = sdayDropDownList.SelectedItem.Text;
                aWeeklyHoliday.DayQty = "2";
            }
            if (aWeeklyHolidayBll.SaveDataForWeeklyHoliday(aWeeklyHoliday))
            {
                showMessageBox("Data saved successfully !!!!");
                clear();
            }
            else
            {
                showMessageBox("Data not saved, You Can Update only !!!");
            }
        }
       
        
    }
    protected void empcodeTextBox_TextChanged(object sender, EventArgs e)
    {
        if (empcodeTextBox.Text != string.Empty)
        {
            DataTable aEmpGeneralDataTable = new DataTable();
            WeeklyHolidayBLL aWeeklyHolidayBll = new WeeklyHolidayBLL();
            aEmpGeneralDataTable = aWeeklyHolidayBll.LoadEmpId(empcodeTextBox.Text.Trim());
            if (aEmpGeneralDataTable.Rows.Count > 0)
            {
                empnameTextBox.Text = aEmpGeneralDataTable.Rows[0]["EmpName"].ToString().Trim();
                empidHiddenField.Value = aEmpGeneralDataTable.Rows[0]["EmpInfoId"].ToString().Trim();
            }
            else
            {
                showMessageBox("No Data Found");
            }
        }
        else
        {
            showMessageBox("Please Input a Employee Code");
        }

    }
    protected void dayqCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dayqCheckBoxList.Items[0].Selected==true)
        {
            divsday.Visible = false;
        }
        if (dayqCheckBoxList.Items[1].Selected == true)
        {
            divsday.Visible = true;
        }
    }
    protected void departmentNewImageButton_Click(object sender, EventArgs eventArgs)
    {
        Response.Redirect("WeeklyHolidayView.aspx");
    }

    protected void cancelButton_OnClick(object sender, EventArgs e)
    {
        clear();
    }
}