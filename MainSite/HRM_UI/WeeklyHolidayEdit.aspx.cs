﻿using System;
using System.Data;
using System.Web.UI;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;


public partial class HRM_UI_WeeklyHolidayEdit : System.Web.UI.Page
{
    WeeklyHolidayBLL aWeeklyHolidayBll = new WeeklyHolidayBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            holidayIdIdHiddenField.Value = Request.QueryString["ID"];
            WeeklyHolidayLoad(holidayIdIdHiddenField.Value);
            Chk();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    public void GetEmpMasterCode(string empid)
    {
        
            DataTable aEmpGeneralDataTable = new DataTable();
            WeeklyHolidayBLL aWeeklyHolidayBll = new WeeklyHolidayBLL();
            aEmpGeneralDataTable = aWeeklyHolidayBll.LoadEmpMasterCode(empid);
            if (aEmpGeneralDataTable.Rows.Count > 0)
            {
                empNameTextBox.Text = aEmpGeneralDataTable.Rows[0]["EmpName"].ToString().Trim();
                EmpMasterCodeTextBox.Text = aEmpGeneralDataTable.Rows[0]["EmpMasterCode"].ToString().Trim();
            }
            else
            {
                showMessageBox("No Data Found");
            }
        
    }

    private bool Validation()
    {
        
        if (empNameTextBox.Text == "")
        {
            showMessageBox("Please Input  Employee Name!!");
            return false;
        }
        if (EmpMasterCodeTextBox.Text == "")
        {
            showMessageBox("Please Input  Employee Code!!");
            return false;
        }

        if (fdayDropDownList.SelectedValue == "")
        {
            showMessageBox("Select atlest 1 day!!");
            return false;
        }
        if (dayqCheckBoxList.SelectedValue=="2")
        {
            if (sdayDropDownList.SelectedValue == "")
            {
                showMessageBox("Select 2 day!!");
                return false;
            }
        }
        if (fdayDropDownList.SelectedValue == sdayDropDownList.SelectedValue)
        {
            showMessageBox("Don't Select Same days!!");
            return false;
        }
        return true;
    }
    protected void updateButton_Click(object sender, EventArgs e)
    {
        if (Validation() == true)
        {

            WeeklyHoliday aWeeklyHoliday = new WeeklyHoliday();
            
            aWeeklyHoliday.WeeklyHolidayId = Convert.ToInt32(holidayIdIdHiddenField.Value);
            aWeeklyHoliday.EmpId = Convert.ToInt32(empIdHiddenField.Value);
            if (dayqCheckBoxList.Items[0].Selected == true)
            {
                aWeeklyHoliday.FirstHolidayName = fdayDropDownList.SelectedItem.Text;
                aWeeklyHoliday.SecondHolidayName = "NONE";
                aWeeklyHoliday.DayQty = "1";
            }
            if (dayqCheckBoxList.Items[0].Selected == true && dayqCheckBoxList.Items[1].Selected == true)
            {
                aWeeklyHoliday.FirstHolidayName = fdayDropDownList.SelectedItem.Text;
                aWeeklyHoliday.SecondHolidayName = sdayDropDownList.SelectedItem.Text;
                aWeeklyHoliday.DayQty = "2";
            }
           
            if (!aWeeklyHolidayBll.UpdateDataForWeeklyHoliday(aWeeklyHoliday))
            {
                showMessageBox("Data Not Update !!!");
            }
            else
            {
                showMessageBox("Data Update Successfully!!! Please Reload");
            }
        }
        else
        {
            showMessageBox("Please input data in all Textbox");
        }
    }

    private void WeeklyHolidayLoad(string WeeklyHolidayId)
    {
        WeeklyHoliday aWeeklyHoliday = new WeeklyHoliday();
        aWeeklyHoliday = aWeeklyHolidayBll.WeeklyHolidayEditLoad(WeeklyHolidayId);
        fdayDropDownList.SelectedItem.Text = aWeeklyHoliday.FirstHolidayName;
        sdayDropDownList.SelectedItem.Text = aWeeklyHoliday.SecondHolidayName;
        empIdHiddenField.Value = aWeeklyHoliday.EmpId.ToString();
        dayqtyHiddenField.Value = aWeeklyHoliday.DayQty;
        GetEmpMasterCode(empIdHiddenField.Value);
    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void dayqCheckBoxList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dayqCheckBoxList.Items[0].Selected == true)
        {
            divsday.Visible = false;
        }
        if (dayqCheckBoxList.Items[1].Selected == true)
        {
            divsday.Visible = true;
        }
        
    }
    public void Chk()
    {
        if (dayqtyHiddenField.Value=="2")
        {
            divsday.Visible = true;
            dayqCheckBoxList.Items[0].Selected = true;
            dayqCheckBoxList.Items[1].Selected = true;
        }
        if (dayqtyHiddenField.Value=="1")
        {
            divsday.Visible = false;
            dayqCheckBoxList.Items[0].Selected = true;
            dayqCheckBoxList.Items[1].Selected = false;
        }
    }
}