﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="YearlyPFBenefit.aspx.cs" Inherits="HRM_UI_YearlyPFBenefit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Yearly Provident Fund Benefit </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                     
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Master Setup </a></li>
                            <li class="breadcrumb-item"><a href="#">Yearly Provident Fund Benefit</a></li>

                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Financial Year </label>
                                        <asp:DropDownList ID="financialYearDropDownList" runat="server"
                                            CssClass="form-control form-control-sm">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-2">
                                    <div class="form-group">
                                        <label>Intrest Parcentage </label>
                                        <asp:TextBox ID="interPerTextBox" runat="server" class="form-control form-control-sm"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-row">
                                <div class="col-2">
                                    <div class="form-group">
                                        <asp:Button ID="submitButton" Text="Save" CssClass="btn btn-sm btn-info" runat="server" OnClick="submitButton_Click" />
                                        <%--<asp:Button ID="cancelButton" Text="Cancel" <asp:Button ID="Button" Text="Cancel" CssClass="btn btn-sm warning" runat="server" OnClick="cancelButton_OnClick" BackColor="#FFCC00" />--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                       
                    <div class="card">
                        <div class="card-body">
                            <asp:Label ID="MessageLabel" runat="server" Text=""></asp:Label>
                            <div class="form-row">
                                <div class="col-12">
                                    <div id="gridContainer1" style="height: 360px; overflow: auto; width: auto; overflow-y: scroll; overflow-x: hidden;">
                                        <asp:GridView ID="loadGridView" runat="server" AutoGenerateColumns="False"
                                            CssClass="table table-bordered text-center thead-dark" DataKeyNames="EmpInfoId">
                                            <Columns>
                                                <asp:TemplateField HeaderText="SL">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LabelSL" Text='<%# Container.DataItemIndex + 1 %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:BoundField DataField="EmpMasterCode" HeaderText="Employee Code" />
                                                <asp:BoundField DataField="EmpName" HeaderText="Employee Name" />
                                                <asp:BoundField DataField="DesigName" HeaderText="Designation" />
                                                <asp:BoundField DataField="ComAmount" HeaderText="Com PF Amount" />
                                                <asp:BoundField DataField="EmpAmount" HeaderText="Emp PF Amount" />
                                                <asp:BoundField DataField="InterPer"
                                                    HeaderText="Interest Percent" />
                                                <asp:BoundField DataField="InterAmount" HeaderText="Interest Amount" />
                                                <asp:BoundField DataField="TotalAmount"
                                                    HeaderText="Total Amount" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

