﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.BLL.HRM_BLL;
using Library.DAO.HRM_Entities;

public partial class HRM_UI_YearlyPFBenefit : System.Web.UI.Page
{
    YearlyPFBenefitBLL aYearlyPfBenefitBll=new YearlyPFBenefitBLL();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DropDownList();
        }
    }
    protected void showMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }

    public void Clear()
    {
        loadGridView.DataSource = null;
        loadGridView.DataBind();
        financialYearDropDownList.SelectedIndex = 0;
        interPerTextBox.Text = string.Empty;
    }
    public void DropDownList()
    {
        aYearlyPfBenefitBll.LoadFinancialYear(financialYearDropDownList);
    }
    protected void calcButton_Click(object sender, EventArgs e)
    {
        decimal mainpercent =string.IsNullOrWhiteSpace(interPerTextBox.Text)?0:Convert.ToDecimal(interPerTextBox.Text);
        decimal percent = mainpercent/100;

        DataTable dtpfsetup = aYearlyPfBenefitBll.LoadPFSetup(financialYearDropDownList.SelectedItem.Text,percent.ToString(),mainpercent.ToString());
        if (dtpfsetup.Rows.Count >0)
        {
            loadGridView.DataSource = dtpfsetup;
            loadGridView.DataBind();
        }
    }
    protected void submitButton_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < loadGridView.Rows.Count; i++)
        {
            YearlyPFBenefit aYearlyPfBenefit = new YearlyPFBenefit()
            {
                EmpInfoId = Convert.ToInt32(loadGridView.DataKeys[i][0].ToString()),
                FianacialYear = Convert.ToInt32(financialYearDropDownList.SelectedValue),
                IntPer = Convert.ToDecimal(interPerTextBox.Text),
                EmpAmount = Convert.ToDecimal(loadGridView.Rows[i].Cells[3].Text),
                CompAmount = Convert.ToDecimal(loadGridView.Rows[i].Cells[4].Text),
                InterAmount = Convert.ToDecimal(loadGridView.Rows[i].Cells[6].Text),
                TotalAmount = Convert.ToDecimal(loadGridView.Rows[i].Cells[7].Text),
                ActionStatus = "Posted",
                EntryDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                EntryUser = Session["LoginName"].ToString(),
                IsActive = true
            };

            if (aYearlyPfBenefitBll.SaveDataForYearlyPFBenefit(aYearlyPfBenefit))
            {
                
            }

        }
        showMessageBox("Data Saved Successfully");
        Clear();
    }
}