﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Library.DAL.PanalCls;


public partial class MasterPages_MainMasterPage : System.Web.UI.MasterPage
{
    private PanalClsDAL aDal = new PanalClsDAL();
    private DataTable aDataTableMenu = new DataTable();
    private DataTable aDataTableSubItem = new DataTable();
    private DataTable aTableSubSubItem = new DataTable();
    private DataTable aTableSubSubChildItem = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null || Session["UserId"].ToString() == "")
        {
            Response.Redirect("../Default.aspx");
        }
        if (!IsPostBack)
        {
            Menu();
            SetUserLogIninfo();
        }
    }

    private void SetUserLogIninfo()
    {
        lblUserName.Text = Convert.ToString(Session["LoginName"]).ToUpper();
        //designationLabel.Text = Convert.ToString(Session["UserType"]);
    }

    private void Menu()
    {
        string clM = "menu";
        string page = @"#";
        string parent = "halflings white home";
        string item = @"Product Info";
        string responsive = @"responsive";
        string clMainMenu = @"halflings white home";
        string navigation = @"navigation";
        string leftC = @"left-corner";
        string rightC = @"right-corner";


        //
        string openable = @"""openable""";
        string icon = @"icon li-home";
        string text = @"""text""";
        string noIcone = @"no-icon";


        aDataTableMenu = Convert.ToInt32(Session["UserId"]) == 1 ? aDal.MainMenu() : aDal.MainMenu(Convert.ToInt32(Session["UserId"]));


        if (aDataTableMenu.Rows.Count > 0)
        {
            ///////////////////////////////Start//////////////////////////////////////////////////////

            string manurHtml = "";



            for (int i = 0; i < aDataTableMenu.Rows.Count; i++)
            {

                aDataTableSubItem = Convert.ToInt32(Session["UserId"]) == 1 ? aDal.SubItem(aDataTableMenu.Rows[i]["SL"].ToString().Trim()) : aDal.SubItem(aDataTableMenu.Rows[i]["SL"].ToString().Trim(), Convert.ToInt32(Session["UserId"]));

                if (aDataTableSubItem.Rows.Count > 0)
                {
                    manurHtml = manurHtml + @"<ul>";

                    for (int j = 0; j < aDataTableSubItem.Rows.Count; j++)
                    {

                        //////////////////////////////////////////////////SubItemBind/////////////////////////////////////////////////////////

                        if (aDataTableSubItem.Rows[j]["URL"].ToString().Trim() != "#")
                        {
                            manurHtml = manurHtml + @" <li ><a href=" + aDataTableSubItem.Rows[j]["URL"].ToString().Trim() + "><span class=" + aDataTableSubItem.Rows[j]["icon"].ToString().Trim() + "></span><span class=" + text + ">" + aDataTableSubItem.Rows[j]["ManuName"].ToString().Trim() + "</span></a>";
                            //manurHtml = manurHtml + @" <li><a href=" + aDataTableSubItem.Rows[j]["URL"].ToString().Trim() + " class=\"no-icon\"><span class=\"text\">" + aDataTableSubItem.Rows[j]["ManuName"].ToString().Trim() + "</span></a>";
                        }
                        else
                        {
                            manurHtml = manurHtml + @" <li class=" + openable + "><a href=" + aDataTableSubItem.Rows[j]["URL"].ToString().Trim() + "><span class=" + aDataTableSubItem.Rows[j]["icon"].ToString().Trim() + "></span><span class=" + text + ">" + aDataTableSubItem.Rows[j]["ManuName"].ToString().Trim() + "</span></a>";
                        }




                        //aTableSubSubItem = aDal.SubSubItem(aDataTableSubItem.Rows[j]["SL"].ToString().Trim());

                        aTableSubSubItem = Convert.ToInt32(Session["UserId"]) == 1 ? aDal.SubSubItem(aDataTableSubItem.Rows[j]["SL"].ToString().Trim()) : aDal.SubSubItem(aDataTableSubItem.Rows[j]["SL"].ToString().Trim(), Convert.ToInt32(Session["UserId"]));


                        if (aTableSubSubItem.Rows.Count > 0)
                        {
                            manurHtml = manurHtml + @"<ul>";
                            for (int k = 0; k < aTableSubSubItem.Rows.Count; k++)
                            {
                                //////////////////////////////////////////////////SubSubItemBind/////////////////////////////////////////////////////////
                                try
                                {
                                    if (aTableSubSubItem.Rows[k]["URL"].ToString() != "#")
                                    {
                                        manurHtml = manurHtml + @" <li ><a href=" + aTableSubSubItem.Rows[k]["URL"].ToString().Trim() + "><span class=" + aTableSubSubItem.Rows[k]["icon"].ToString().Trim() + "></span><span class=" + text + ">" + aTableSubSubItem.Rows[k]["ManuName"].ToString().Trim() + "</span></a>";
                                        //manurHtml = manurHtml + @" <li><a href=" + aTableSubSubItem.Rows[k]["URL"].ToString().Trim() + " class=\"no-icon\"><span class=\"text\">" + aTableSubSubItem.Rows[k]["ManuName"].ToString().Trim() + "</span></a>";
                                    }
                                    else
                                    {
                                        manurHtml = manurHtml + @" <li class=" + openable + "><a href=" + aTableSubSubItem.Rows[k]["URL"].ToString().Trim() + "><span class=" + aTableSubSubItem.Rows[k]["icon"].ToString().Trim() + "></span><span class=" + text + ">" + aTableSubSubItem.Rows[k]["ManuName"].ToString().Trim() + "</span></a>";
                                    }
                                }
                                catch (Exception)
                                {

                                    //  throw;
                                }







                                //aTableSubSubChildItem = aDal.SubSubChildItem(aTableSubSubItem.Rows[k]["SL"].ToString().Trim());

                                aTableSubSubChildItem = Convert.ToInt32(Session["UserId"]) == 1 ? aDal.SubSubChildItem(aTableSubSubItem.Rows[k]["SL"].ToString().Trim()) : aDal.SubSubChildItem(aTableSubSubItem.Rows[k]["SL"].ToString().Trim(), Convert.ToInt32(Session["UserId"]));


                                if (aTableSubSubChildItem.Rows.Count > 0)
                                {
                                    manurHtml = manurHtml + @"<ul>";
                                    for (int l = 0; l < aTableSubSubChildItem.Rows.Count; l++)
                                    {
                                        //////////////////////////////////////////////////SubSubChildItemBind/////////////////////////////////////////////////////////

                                        if (aTableSubSubChildItem.Rows[l]["URL"].ToString() != "#")
                                        {
                                            manurHtml = manurHtml + @" <li ><a href=" + aTableSubSubChildItem.Rows[l]["URL"].ToString().Trim() + "><span class=" + aTableSubSubChildItem.Rows[l]["icon"].ToString().Trim() + "></span><span class=" + text + ">" + aTableSubSubChildItem.Rows[l]["ManuName"].ToString().Trim() + "</span></a></li>";
                                            //manurHtml = manurHtml + @" <li><a href=" + aTableSubSubChildItem.Rows[l]["URL"].ToString().Trim() + " class=\"no-icon\"><span class=\"text\">" + aTableSubSubChildItem.Rows[l]["ManuName"].ToString().Trim() + "</span></a>";
                                        }
                                        else
                                        {
                                            manurHtml = manurHtml + @" <li class=" + openable + "><a href=" + aTableSubSubChildItem.Rows[l]["URL"].ToString().Trim() + "><span class=" + aTableSubSubChildItem.Rows[l]["icon"].ToString().Trim() + "></span><span class=" + text + ">" + aTableSubSubChildItem.Rows[l]["ManuName"].ToString().Trim() + "</span></a></li>";
                                        }

                                        //////////////////////////////////////////////////SubSubChildItemBindEnd/////////////////////////////////////////////////////////
                                    }

                                    manurHtml = manurHtml + @"</ul>";
                                }

                                //////////////////////////////////////////////////SubSubItemBind/////////////////////////////////////////////////////////
                                manurHtml = manurHtml + @"</li>";
                            }

                            manurHtml = manurHtml + @"</ul>";
                        }

                        //////////////////////////////////////////////////SubItemBindEnd/////////////////////////////////////////////////////////
                        manurHtml = manurHtml + @"</li>";
                    }

                    manurHtml = manurHtml + @"</ul>";
                }


                //manurHtml = manurHtml + @"</li>";

                //////////////////////////////////////////////////MainMenuBindEnd/////////////////////////////////////////////////////////
            }

            //manurHtml = manurHtml + @"</ul></nav>";

            //////////////////////////////////////////end////////////////////////////////////////
            navigation_default.InnerHtml = manurHtml;
        }
    }
    public void LoadEmployeeInfo()
    {
        //nameTextBox.Text = Convert.ToString(Session["EmpName"]);
        //surNameTextBox.Text = Convert.ToString(Session["ShortName"]);
        //addressTextBox.Text = Convert.ToString(Session["AddressPermanent"]);
        //emailTextBox.Text = Convert.ToString(Session["Email"]);
        //cellNumberTextBox.Text = Convert.ToString(Session["CellNumber"]);
        //unitTextBox.Text = Convert.ToString(Session["UnitName"]);
        //departmentTextBox.Text = Convert.ToString(Session["DeptName"]);
        //designationTextBox.Text = Convert.ToString(Session["DesigName"]);
    }


    
}

