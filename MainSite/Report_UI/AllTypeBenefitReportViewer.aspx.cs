﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;


public partial class Report_UI_AllTypeBenefitReportViewer : System.Web.UI.Page
{
    OtherReportBLL OtherReportBLL=new OtherReportBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        string rptType = Request.QueryString["rptType"];
        string month = Request.QueryString["month"];
        string monthname = Request.QueryString["monthname"];
        string year = Request.QueryString["year"];
        string fastivalId = Request.QueryString["fastivalId"];
        string bankcash = Request.QueryString["bankcash"];
        string empCode = Session["empcode"].ToString();
        string param = Session["Param"].ToString();

        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = OtherReportBLL.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////
         if (rptType == "MB")
         {
             PFSetupBLL aSetupBll=new PFSetupBLL();
             
            nameLabel.Text = "Monthly Pf Benefit";
            DataTable allDataTable = new DataTable();
            allDataTable = aSetupBll.PFReport(month,year,monthname).Copy();
            allDataTable.TableName = "monthlyPFDataTable";

            mainDS.Tables.Add(allDataTable);
             
            //rptPfMonthly rptPfMonthly = new rptPfMonthly();
            //rptPfMonthly.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptPfMonthly;
             
            rptdoc.Load(ReportPath("rptPfMonthly.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
         if (rptType == "PFS")
         {
             PFSetupBLL aSetupBll = new PFSetupBLL();

             nameLabel.Text = "Monthly Pf Benefit";
             DataTable allDataTable = new DataTable();
             allDataTable = aSetupBll.PFReport(month, year, monthname).Copy();
             allDataTable.TableName = "monthlyPFDataTable";

             mainDS.Tables.Add(allDataTable);
             
             //rptPfMonthlySummary rptPfMonthlySummary = new rptPfMonthlySummary();
             //rptPfMonthlySummary.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptPfMonthlySummary;

             rptdoc.Load(ReportPath("rptPfMonthlySummary.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "PFY")
         {
             PFSetupBLL aSetupBll = new PFSetupBLL();

             nameLabel.Text = "Yearly Pf Benefit";
             DataTable allDataTable = new DataTable();
             allDataTable = aSetupBll.PFReportYearly(year).Copy();
             allDataTable.TableName = "monthlyPFDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptPfYearlySummary rptPfYearlySummary = new rptPfYearlySummary();
             //rptPfYearlySummary.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptPfYearlySummary;

             rptdoc.Load(ReportPath("rptPfYearlySummary.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "PFYE")
         {
             PFSetupBLL aSetupBll = new PFSetupBLL();

             nameLabel.Text = "Yearly Pf Benefit";
             DataTable allDataTable = new DataTable();
             allDataTable = aSetupBll.PFReportYearly(year).Copy();
             allDataTable.TableName = "monthlyPFDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptPfYearly rptPfYearly = new rptPfYearly();
             //rptPfYearly.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptPfYearly;

             rptdoc.Load(ReportPath("rptPfYearly.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "PFEW")
         {
             PFSetupBLL aSetupBll = new PFSetupBLL();

             nameLabel.Text = "Yearly Pf Benefit";
             DataTable allDataTable = new DataTable();
             allDataTable = aSetupBll.PFSingleEmp(year,empCode).Copy();
             allDataTable.TableName = "monthlyPFDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptPfSingleEmp rptPFSingle = new rptPfSingleEmp();
             //rptPFSingle.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptPFSingle;

             rptdoc.Load(ReportPath("rptPfSingleEmp.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "FB")
         {
             FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();

             nameLabel.Text = "Fastival Bonus ";
             DataTable allDataTable = new DataTable();
             if (bankcash=="ALL")
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId,param).Copy();    
             }
             else
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, " AND dbo.tblEmpGeneralInfo.PayType='"+bankcash+"' "+param+"").Copy();
             }
             
             allDataTable.TableName = "fastivalDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptFastval rptFastval = new rptFastval();
             //rptFastval.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptFastval;

             rptdoc.Load(ReportPath("rptFastval.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "MA")
         {
             EmpSalaryReportBLL aSalaryReportBll=new EmpSalaryReportBLL();
             DataTable allDataTable = new DataTable();

             allDataTable = aSalaryReportBll.SalaryReportDetailsBLL(param, bankcash).Copy();
             allDataTable.TableName = "mAllowanceDataTableDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptMobileAllowance arptMobileAllowance = new rptMobileAllowance();
             //arptMobileAllowance.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = arptMobileAllowance;

             rptdoc.Load(ReportPath("rptMobileAllowance.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "MAUW")
         {
             EmpSalaryReportBLL aSalaryReportBll = new EmpSalaryReportBLL();
             DataTable allDataTable = new DataTable();

             allDataTable = aSalaryReportBll.SalaryReportDetailsBLL(param, bankcash).Copy();
             allDataTable.TableName = "mAllowanceDataTableDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptMobileAllowanceUnitwise rptMobileAllowanceUnitwise = new rptMobileAllowanceUnitwise();
             //rptMobileAllowanceUnitwise.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptMobileAllowanceUnitwise;

             rptdoc.Load(ReportPath("rptMobileAllowanceUnitwise.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
        if (rptType == "FBS")
        {

            FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();

             nameLabel.Text = "Fastival Bonus ";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, param).Copy();
             }
             else
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, " AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' " + param + "").Copy();
             }

             allDataTable.TableName = "fastivalDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptFastvalSummery rptFastvalSummery = new rptFastvalSummery();
             //rptFastvalSummery.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptFastvalSummery;

             rptdoc.Load(ReportPath("rptFastvalSummery.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
        if (rptType == "FBSR")
        {
            FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();

            nameLabel.Text = "Fastival Bonus ";
            DataTable allDataTable = new DataTable();
            if (bankcash == "ALL")
            {
                allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, param).Copy();
            }
            else
            {
                allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, " AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' " + param + "").Copy();
            }

            allDataTable.TableName = "fastivalDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptFastvalStamp rptFastvalStamp = new rptFastvalStamp();
            //rptFastvalStamp.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptFastvalStamp;

            rptdoc.Load(ReportPath("rptFastvalStamp.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
         if (rptType == "FBDL")
         {
             FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();

             nameLabel.Text = "Fastival Bonus Disbersment Letter";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, param).Copy();
             }
             else
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, " AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' "+param+"").Copy();
             }

             allDataTable.TableName = "fastivalDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptFestivalDisbursementLetter rptFestivalDisbursementLetter = new rptFestivalDisbursementLetter();
             //rptFestivalDisbursementLetter.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptFestivalDisbursementLetter;

             rptdoc.Load(ReportPath("rptFestivalDisbursementLetter.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "FBP")
         {
             FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();

             nameLabel.Text = "Fastival Bonus Payslip";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, param).Copy();
             }
             else
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, " AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' "+param+"").Copy();
             }

             allDataTable.TableName = "fastivalDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptFBonusPaySlipSingleEmp rptFBonusPaySlipSingleEmp = new rptFBonusPaySlipSingleEmp();
             //rptFBonusPaySlipSingleEmp.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptFBonusPaySlipSingleEmp;

             rptdoc.Load(ReportPath("rptFBonusPaySlipSingleEmp.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "FBPS")
         {
             FastibalBonusBLL aFastibalBonusBll = new FastibalBonusBLL();

             nameLabel.Text = "Fastival Bonus Payslip";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId, param).Copy();
             }
             else
             {
                 allDataTable = aFastibalBonusBll.FestivalBonusReport(year, fastivalId,  param).Copy();
             }

             allDataTable.TableName = "fastivalDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptFBonusPaySlipSingleEmp rptFBonusPaySlipSingleEmp = new rptFBonusPaySlipSingleEmp();
             //rptFBonusPaySlipSingleEmp.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptFBonusPaySlipSingleEmp;

             rptdoc.Load(ReportPath("rptFBonusPaySlipSingleEmp.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "EL")
         {
             ELPaymentBLL aElPaymentBll = new ELPaymentBLL();

             nameLabel.Text = "EL Payment ";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year,  param).Copy();
             }
             else
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, "  AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' "+param+"").Copy();
             }

             allDataTable.TableName = "elPaymentDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptElPayment rptElPayment = new rptElPayment();
             //rptElPayment.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptElPayment;

             rptdoc.Load(ReportPath("rptElPayment.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "ELDL")
         {
             ELPaymentBLL aElPaymentBll = new ELPaymentBLL();

             nameLabel.Text = "EL Payment Disbersment Letter";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, param).Copy();
             }
             else
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, "  AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' "+param+"").Copy();
             }

             allDataTable.TableName = "elPaymentDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptELeavePayDisbursementLetter rptELeavePayDisbursementLetter = new rptELeavePayDisbursementLetter();
             //rptELeavePayDisbursementLetter.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptELeavePayDisbursementLetter;

             rptdoc.Load(ReportPath("rptELeavePayDisbursementLetter.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "ELP" || rptType == "ELPS")
         {
             ELPaymentBLL aElPaymentBll = new ELPaymentBLL();

             nameLabel.Text = "EL Payment Payslip";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, param).Copy();
             }
             else
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, "  AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' "+param+"").Copy();
             }

             allDataTable.TableName = "elPaymentDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptELeavePaySlipSingleEmp rptELeavePaySlipSingleEmp = new rptELeavePaySlipSingleEmp();
             //rptELeavePaySlipSingleEmp.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptELeavePaySlipSingleEmp;

             rptdoc.Load(ReportPath("rptELeavePaySlipSingleEmp.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "ELS")
         {
             ELPaymentBLL aElPaymentBll = new ELPaymentBLL();

             nameLabel.Text = "EL Payment Summery";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, param).Copy();
             }
             else
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, "  AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' " + param + "").Copy();
             }

             allDataTable.TableName = "elPaymentDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptElPaymentSummery rptElPaymentSummery = new rptElPaymentSummery();
             //rptElPaymentSummery.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptElPaymentSummery;

             rptdoc.Load(ReportPath("rptElPaymentSummery.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
         if (rptType == "ELSR")
         {
             ELPaymentBLL aElPaymentBll = new ELPaymentBLL();

             nameLabel.Text = "EL Payment Summery";
             DataTable allDataTable = new DataTable();
             if (bankcash == "ALL")
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, param).Copy();
             }
             else
             {
                 allDataTable = aElPaymentBll.ELPaymentReport(year, "  AND dbo.tblEmpGeneralInfo.PayType='" + bankcash + "' " + param + "").Copy();
             }

             allDataTable.TableName = "elPaymentDataTable";

             mainDS.Tables.Add(allDataTable);

             //rptElPaymentStamp rptElPaymentStamp = new rptElPaymentStamp();
             //rptElPaymentStamp.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptElPaymentStamp;

             rptdoc.Load(ReportPath("rptElPaymentStamp.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();

         }
     
    }
    
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
   
    protected void crvotherrpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvotherrpt.Dispose();
        }
    }
    protected void crvotherrpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvotherrpt.Dispose();
        }
    }

    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}