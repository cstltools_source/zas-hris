﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_AttendanceReportViewer : System.Web.UI.Page
{
    AttendanceReportBLL attendanceReportBll=new AttendanceReportBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
     string fromDate =   Request.QueryString["FromDate"];
     string toDate = Request.QueryString["ToDate"];
     string empcode = Request.QueryString["empcode"];
     string reportFlag = Request.QueryString["ReportFlag"];
     string attenDanceReportParameter = Session["AttenDanceReportParameter"].ToString();

        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = attendanceReportBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////
        if (reportFlag == "MA")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = attendanceReportBll.ManualRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
            allDataTable.TableName = "ManualAttDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptManualAttandance rptAllAttendance = new rptManualAttandance();
            //rptAllAttendance.SetDataSource(mainDS);
            //crvAttendancerpt.ReportSource = rptAllAttendance;

            rptdoc.Load(ReportPath("rptManualAttandance.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvAttendancerpt.ReportSource = rptdoc;
            crvAttendancerpt.DataBind();

        }
        if (reportFlag == "All")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = attendanceReportBll.AllRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
            allDataTable.TableName = "presentDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptAllAttendances rptAllAttendance = new rptAllAttendances();
            //rptAllAttendance.SetDataSource(mainDS);
            //crvAttendancerpt.ReportSource = rptAllAttendance;

            rptdoc.Load(ReportPath("rptAllAttendances.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvAttendancerpt.ReportSource = rptdoc;
            crvAttendancerpt.DataBind();

        }
        if (reportFlag == "EAM")
        {
            DataTable allDataTable = new DataTable();
            DataTable daynameDataTable = new DataTable();
            DataTable totalstatDataTable = new DataTable();
            allDataTable = attendanceReportBll.MonthlyAttendance(fromDate,empcode).Copy();
            daynameDataTable = attendanceReportBll.MonthDayName(fromDate, toDate).Copy();
            totalstatDataTable = attendanceReportBll.TotalStats(fromDate, toDate).Copy();
            allDataTable.TableName = "monthlyAttDataTable";
            daynameDataTable.TableName = "daynmeAttDataTable";
            totalstatDataTable.TableName = "totalstatDataTable";

            mainDS.Tables.Add(allDataTable);
            mainDS.Tables.Add(daynameDataTable);
            mainDS.Tables.Add(totalstatDataTable);

            //rptMonthlyAttendance rptMonthlyAttendance = new rptMonthlyAttendance();
            //rptMonthlyAttendance.SetDataSource(mainDS);
            //crvAttendancerpt.ReportSource = rptMonthlyAttendance;

            rptdoc.Load(ReportPath("rptMonthlyAttendance.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvAttendancerpt.ReportSource = rptdoc;
            crvAttendancerpt.DataBind();

        }
        if (reportFlag == "EAMD")
        {
            DataTable allDataTable = new DataTable();
            DataTable daynameDataTable = new DataTable();
            DataTable totalstatDataTable = new DataTable();
            allDataTable = attendanceReportBll.MonthlyAttendance(fromDate, empcode).Copy();
            daynameDataTable = attendanceReportBll.MonthDayName(fromDate, toDate).Copy();
            totalstatDataTable = attendanceReportBll.TotalStats(fromDate, toDate).Copy();
            allDataTable.TableName = "monthlyAttDataTable";
            daynameDataTable.TableName = "daynmeAttDataTable";
            totalstatDataTable.TableName = "totalstatDataTable";

            mainDS.Tables.Add(allDataTable);
            mainDS.Tables.Add(daynameDataTable);
            mainDS.Tables.Add(totalstatDataTable);

            //rptMonthlyAttendanceDept rptMonthlyAttendanceDept = new rptMonthlyAttendanceDept();
            //rptMonthlyAttendanceDept.SetDataSource(mainDS);
            //crvAttendancerpt.ReportSource = rptMonthlyAttendanceDept;

            rptdoc.Load(ReportPath("rptMonthlyAttendanceDept.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvAttendancerpt.ReportSource = rptdoc;
            crvAttendancerpt.DataBind();

        }
     if (reportFlag=="P")
        {
            DataTable presentDataTable = new DataTable();
            presentDataTable = attendanceReportBll.PresentRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
            presentDataTable.TableName = "presentDataTable";

            mainDS.Tables.Add(presentDataTable);

            //rptPresent aPresent = new rptPresent();
            //aPresent.SetDataSource(mainDS);
            //crvAttendancerpt.ReportSource = aPresent;

            rptdoc.Load(ReportPath("rptPresent.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvAttendancerpt.ReportSource = rptdoc;
            crvAttendancerpt.DataBind();

        }
     if (reportFlag == "A")
     {
         DataTable absentDataTable = new DataTable();
         absentDataTable = attendanceReportBll.AbsentRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
         absentDataTable.TableName = "absentDataTable";

         mainDS.Tables.Add(absentDataTable);

         //rptAbsent absent = new rptAbsent();
         //absent.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = absent;

         rptdoc.Load(ReportPath("rptAbsent.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "L")
     {
         DataTable lateDataTable = new DataTable();
         lateDataTable = attendanceReportBll.LateRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
         lateDataTable.TableName = "lateDataTable";

         mainDS.Tables.Add(lateDataTable);

         //rptLate late = new rptLate();
         //late.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = late;

         rptdoc.Load(ReportPath("rptLate.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "OD")
     {
         DataTable onDutyDataTable = new DataTable();
         onDutyDataTable = attendanceReportBll.OnDutyRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
         onDutyDataTable.TableName = "onDurtDataTable";

         mainDS.Tables.Add(onDutyDataTable);

         //rptOnDuty onDuty = new rptOnDuty();
         //onDuty.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = onDuty;

         rptdoc.Load(ReportPath("rptOnDuty.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "LV")
     {
         DataTable leaveDataTable = new DataTable();
         leaveDataTable = attendanceReportBll.LeaveRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
         leaveDataTable.TableName = "onDurtDataTable";

         mainDS.Tables.Add(leaveDataTable);

         //rptEmpLeave rptEmpLeave = new rptEmpLeave();
         //rptEmpLeave.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = rptEmpLeave;

         rptdoc.Load(ReportPath("rptEmpLeave.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "EmpAtt")
     {
         DataTable EmpAttDataTable = new DataTable();
         EmpAttDataTable = attendanceReportBll.EmployeeAttendanceRecordBLL(fromDate, toDate, attenDanceReportParameter).Copy();
         EmpAttDataTable.TableName = "presentDataTable";

         mainDS.Tables.Add(EmpAttDataTable);

         //rptEmpAtt rptEmpAtt = new rptEmpAtt();
         //rptEmpAtt.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = rptEmpAtt;

         rptdoc.Load(ReportPath("rptEmpAtt.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "EJC")
     {
         DataTable EmpAttDataTable = new DataTable();
         DataTable EmptotalAttDataTable = new DataTable();
         DataTable EmpimageAttDataTable = new DataTable();
         EmpAttDataTable = attendanceReportBll.SingleWEmpAttendance(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), attenDanceReportParameter).Copy();
         EmptotalAttDataTable = attendanceReportBll.Total(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), attenDanceReportParameter).Copy();
         EmpimageAttDataTable = attendanceReportBll.EmpImage(empcode).Copy();
         EmpAttDataTable.TableName = "presentDataTable";
         EmpimageAttDataTable.TableName = "empImageDataTable";
         EmptotalAttDataTable.TableName = "totalDataTable";
         mainDS.Tables.Add(EmpAttDataTable);
         mainDS.Tables.Add(EmptotalAttDataTable);
         mainDS.Tables.Add(EmpimageAttDataTable);

         //rptEmpJobCard rptEmpJobCard = new rptEmpJobCard();
         //rptEmpJobCard.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = rptEmpJobCard;

         rptdoc.Load(ReportPath("rptEmpJobCard.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }

     if (reportFlag == "MDS")
     {
         DataTable mDSumDataTable = new DataTable();
         mDSumDataTable = attendanceReportBll.MonthlyDeptWiseSumAttendance(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), attenDanceReportParameter).Copy();
         mDSumDataTable.TableName = "mDSumDataTable";
         mainDS.Tables.Add(mDSumDataTable);

         //rptAttMonthDepartSum arptAttMonthDepartSum = new rptAttMonthDepartSum();
         //arptAttMonthDepartSum.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = arptAttMonthDepartSum;

         rptdoc.Load(ReportPath("rptAttMonthDepartSum.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "DWS")
     {
         DataTable dwsDataTable = new DataTable();
         dwsDataTable = attendanceReportBll.DeptWiseAttendanceSummaryBLL(fromDate, toDate, attenDanceReportParameter).Copy();
         dwsDataTable.TableName = "dwsDataTable";

         mainDS.Tables.Add(dwsDataTable);

         //rptDeptWiseSummary rptDeptWiseSummary = new rptDeptWiseSummary();
         //rptDeptWiseSummary.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = rptDeptWiseSummary;

         rptdoc.Load(ReportPath("rptDeptWiseSummary.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "AS")
     {
         DataTable attSumDataTable = new DataTable();
         attSumDataTable = attendanceReportBll.MonthlyAttendanceSummery(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), attenDanceReportParameter).Copy();
         attSumDataTable.TableName = "attSumDataTable";

         mainDS.Tables.Add(attSumDataTable);

         //rptAttendanceSum rptAttendanceSum = new rptAttendanceSum();
         //rptAttendanceSum.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = rptAttendanceSum;

         rptdoc.Load(ReportPath("rptAttendanceSum.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();

     }
     if (reportFlag == "AEJC")
     {
         DataTable EmpAttDataTable = new DataTable();
         DataTable EmptotalAttDataTable = new DataTable();
         DataTable EmpimageAttDataTable = new DataTable();
         EmpAttDataTable = attendanceReportBll.SingleWEmpAttendance(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), attenDanceReportParameter).Copy();
         EmptotalAttDataTable = attendanceReportBll.Total(Convert.ToDateTime(fromDate), Convert.ToDateTime(toDate), attenDanceReportParameter).Copy();
         EmpimageAttDataTable = attendanceReportBll.EmpImage(empcode).Copy();
         EmpAttDataTable.TableName = "presentDataTable";
         EmpimageAttDataTable.TableName = "empImageDataTable";
         EmptotalAttDataTable.TableName = "totalDataTable";
         mainDS.Tables.Add(EmpAttDataTable);
         mainDS.Tables.Add(EmptotalAttDataTable);
         mainDS.Tables.Add(EmpimageAttDataTable);

         //rptAllEmpJobCard rptAllEmpJobCard = new rptAllEmpJobCard();
         //rptAllEmpJobCard.SetDataSource(mainDS);
         //crvAttendancerpt.ReportSource = rptAllEmpJobCard;

         rptdoc.Load(ReportPath("rptAllEmpJobCard.rpt"));
         rptdoc.SetDataSource(mainDS);

         crvAttendancerpt.ReportSource = rptdoc;
         crvAttendancerpt.DataBind();
      }

    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    
    protected void crvAttendancerpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvAttendancerpt.Dispose();
        }
    }
    protected void crvAttendancerpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvAttendancerpt.Dispose();
        }
     }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}

