﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;
using Library.CrystalReports.HRM_DataSets;


public partial class Report_UI_CompanyInfoReport : System.Web.UI.Page
{
    CompanyInfoBLL aCompanyInfoBll = new CompanyInfoBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        DataSet mainDS = new DataSet();
        DataTable aDataTableReport = new DataTable();

        aDataTableReport = aCompanyInfoBll.LoadCompanyInfoReport();
        dsCompanyInfo aDsCompanyInfo = new dsCompanyInfo();
        foreach (DataRow row in aDataTableReport.Rows)
        {
            aDsCompanyInfo.aDataTableReport.ImportRow(row);
        }
        //rptCompanyInfo aRptCompanyInfo = new rptCompanyInfo();
        //aRptCompanyInfo.SetDataSource(aDsCompanyInfo);
        //crvCompanyInfoReport.ReportSource = aRptCompanyInfo;

        rptdoc.Load(ReportPath("rptCompanyInfo.rpt"));
        rptdoc.SetDataSource(mainDS);

        crvCompanyInfoReport.ReportSource = rptdoc;
        crvCompanyInfoReport.DataBind();

    }
    protected void crvCompanyInfoReport_Init(object sender, EventArgs e)
    {
        
    }

    protected void crvCompanyInfoReport_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvCompanyInfoReport.Dispose();
        }
    }
    protected void crvCompanyInfoReport_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvCompanyInfoReport.Dispose();
        }
    }

    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}