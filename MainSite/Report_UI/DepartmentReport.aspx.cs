﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;
using Library.CrystalReports.HRM_DataSets;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_DepartmentReport : System.Web.UI.Page
{
    DepartmentBLL aDepartmentBll = new DepartmentBLL();

    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        DataSet mainDS = new DataSet();
        DataTable aDataTableReport = new DataTable();

        aDataTableReport = aDepartmentBll.LoadDepartmentView();
        dsDepartment aDsDepartment = new dsDepartment();
        foreach (DataRow row in aDataTableReport.Rows)
        {
            aDsDepartment.aDataTable.ImportRow(row);
        }
        //rptDepartment aRptDepartment = new rptDepartment();
        //aRptDepartment.SetDataSource(aDsDepartment);
        //crvDepartment.ReportSource = aRptDepartment;

        rptdoc.Load(ReportPath("rptDepartment.rpt"));
        rptdoc.SetDataSource(mainDS);

        crvDepartment.ReportSource = rptdoc;
        crvDepartment.DataBind();

    }
    protected void crvDepartment_Init(object sender, EventArgs e)
    {

    }

    protected void crvDepartment_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvDepartment.Dispose();
        }
    }
    protected void crvDepartment_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvDepartment.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}