﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;
using Library.CrystalReports.HRM_DataSets;
//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_DesignationReport : System.Web.UI.Page
{
    DesignationBLL aDesignationBll = new DesignationBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        DataSet mainDS = new DataSet();
        DataTable aDataTableReport = new DataTable();

        aDataTableReport = aDesignationBll.LoadDesignationView();
        dsDesignation aDsDesignation = new dsDesignation();
        foreach (DataRow row in aDataTableReport.Rows)
        {
            aDsDesignation.aDataTable.ImportRow(row);
        }
        //rptDesignation aRptDesignation = new rptDesignation();
        //aRptDesignation.SetDataSource(aDsDesignation);
        //crvDesignation.ReportSource = aRptDesignation;

        rptdoc.Load(ReportPath("rptDesignation.rpt"));
        rptdoc.SetDataSource(mainDS);

        crvDesignation.ReportSource = rptdoc;
        crvDesignation.DataBind();

    }
    protected void crvDesignation_Init(object sender, EventArgs e)
    {

    }

    protected void crvDesignation_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvDesignation.Dispose();
        }
    }
    protected void crvDesignation_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvDesignation.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}