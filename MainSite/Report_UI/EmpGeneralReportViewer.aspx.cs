﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_EmpGeneralReportViewer : System.Web.UI.Page
{
    EmpGeneralInfoBLL aGeneralInfoBll=new EmpGeneralInfoBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        string empcode = Request.QueryString["EmpCode"];
     
     string reportFlag = Request.QueryString["ReportFlag"];
     Int32 companyId = Convert.ToInt32(Request.QueryString["CompanyId"]);
     string EmpGeneralReportParameter = Session["EmpGeneralReportParameter"].ToString();

        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = aGeneralInfoBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////
        if (reportFlag == "A")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReport(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "Acive")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReportactive(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "Inactive")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReportInactive(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "Posted")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReportPosted(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "SE" || reportFlag=="JDW")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReport(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "DW")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReport(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "SW")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReport(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "UW")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReport(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "EJR")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeReportJobleft(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptAllEmployeeGeneralJobLeft.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "EPR")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.ProbationaryEmployeeList(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "ProbationDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmployeeGeneral rptAllEmployeeGeneral = new rptAllEmployeeGeneral();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptProbationalEmployeeList.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag=="S")
        {
            DataTable singleEmpDataTable = new DataTable();
            DataTable salEmpDataTable = new DataTable();
            DataTable eduEmpDataTable = new DataTable();
            DataTable trainEmpDataTable = new DataTable();
            DataTable jobEmpDataTable = new DataTable();

            singleEmpDataTable = aGeneralInfoBll.EmployeeReport(EmpGeneralReportParameter).Copy();
            salEmpDataTable = aGeneralInfoBll.EmpSalaryInfo(EmpGeneralReportParameter).Copy();
            eduEmpDataTable = aGeneralInfoBll.EmpEduInfo(EmpGeneralReportParameter).Copy();
            trainEmpDataTable = aGeneralInfoBll.EmpTrainInfo(EmpGeneralReportParameter).Copy();
            jobEmpDataTable = aGeneralInfoBll.EmpJobInfo(EmpGeneralReportParameter).Copy();
            singleEmpDataTable.TableName = "mainDataTableReport";
            salEmpDataTable.TableName = "salDataTable";
            eduEmpDataTable.TableName = "eduDataTable";
            trainEmpDataTable.TableName = "trainingDataTable";
            jobEmpDataTable.TableName = "jobDataTable";
            

            mainDS.Tables.Add(singleEmpDataTable);
            mainDS.Tables.Add(salEmpDataTable);
            mainDS.Tables.Add(eduEmpDataTable);
            mainDS.Tables.Add(trainEmpDataTable);
            mainDS.Tables.Add(jobEmpDataTable);


            //rptEmpGeneral rptEmpGeneral = new rptEmpGeneral();
            //rptEmpGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptEmpGeneral;

            rptdoc.Load(ReportPath("rptEmpGeneral.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "TN")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeTinReport(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptEmpTIN rptAllEmployeeGeneral = new rptEmpTIN();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptEmpTIN.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
        if (reportFlag == "BA")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGeneralInfoBll.EmployeeTinReport(EmpGeneralReportParameter).Copy();
            allDataTable.TableName = "mainDataTableReport";

            mainDS.Tables.Add(allDataTable);

            //rptEmpBankAcc rptAllEmployeeGeneral = new rptEmpBankAcc();
            //rptAllEmployeeGeneral.SetDataSource(mainDS);
            //crvempgeneralrpt.ReportSource = rptAllEmployeeGeneral;

            rptdoc.Load(ReportPath("rptEmpBankAcc.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgeneralrpt.ReportSource = rptdoc;
            crvempgeneralrpt.DataBind();

        }
     
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }

    protected void crvempgeneralrpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvempgeneralrpt.Dispose();
        }
    }
    protected void crvempgeneralrpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvempgeneralrpt.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}