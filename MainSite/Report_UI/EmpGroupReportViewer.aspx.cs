﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_EmpGeneralReportViewer : System.Web.UI.Page
{
    EmpGeneralInfoBLL aGeneralInfoBll=new EmpGeneralInfoBLL();
    ReportDocument rptdoc = new ReportDocument();
    GroupBLL aGroupBll=new GroupBLL();
    protected void Page_Init(object sender, EventArgs e)
    {
        string groupId = Request.QueryString["groupId"];
        string fromdt = Request.QueryString["fromdt"];
        string todt = Request.QueryString["todt"];
     
        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = aGeneralInfoBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////

        if (groupId!="0")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGroupBll.LoadGroupRpt(groupId,fromdt,todt).Copy();
            allDataTable.TableName = "groupDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptGroup rptGroup = new rptGroup();
            //rptGroup.SetDataSource(mainDS);
            //crvempgrouprpt.ReportSource = rptGroup;

            rptdoc.Load(ReportPath("rptGroup.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgrouprpt.ReportSource = rptdoc;
            crvempgrouprpt.DataBind();
        }
        else
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aGroupBll.LoadGroupRpt(fromdt,todt).Copy();
            allDataTable.TableName = "groupDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptGroup rptGroup = new rptGroup();
            //rptGroup.SetDataSource(mainDS);
            //crvempgrouprpt.ReportSource = rptGroup;

            rptdoc.Load(ReportPath("rptGroup.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvempgrouprpt.ReportSource = rptdoc;
            crvempgrouprpt.DataBind();
        }
            
        
     
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }

    protected void crvempgrouprpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvempgrouprpt.Dispose();
        }
    }
    protected void crvempgrouprpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvempgrouprpt.Dispose();
        }
    }

    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}