﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_EmpSalaryReportViewer : System.Web.UI.Page
{
    EmpSalaryReportBLL aSalaryReportBll=new EmpSalaryReportBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {

        string reportFlag = Request.QueryString["ReportFlag"];
        string ReportParameter = Session["ReportParameter"].ToString();
        string BankCash = Request.QueryString["BankCash"];
        string monthId = Request.QueryString["mId"];
        string monthName = Request.QueryString["mn"];
        string empcode = Request.QueryString["empcode"];
        string to = Request.QueryString["to"];
        string from = Request.QueryString["from"];
        string year = Request.QueryString["year"];

        DataSet mainDS = new DataSet();
       
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = aSalaryReportBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";
        mainDS.Tables.Add(reportHederDataTable);
        
        if (reportFlag == "ESA")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptSalaryActual rptSalaryActual = new rptSalaryActual();
            //rptSalaryActual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryActual;

            rptdoc.Load(ReportPath("rptSalaryActual.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "SESAY")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);
            //rptSingleYearlySalaryActual rptSalaryActual = new rptSingleYearlySalaryActual();
            //rptSalaryActual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryActual;

            rptdoc.Load(ReportPath("rptSingleYearlySalaryActual.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }

        if (reportFlag == "PS")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);
            //rptPaySlip rptPaySlip = new rptPaySlip();
            //rptPaySlip.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptPaySlip;

            rptdoc.Load(ReportPath("rptPaySlip.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();


        }
        if (reportFlag == "PSS")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);
            //rptPaySlipSingleEmp rptPaySlipSingleEmp = new rptPaySlipSingleEmp();
            //rptPaySlipSingleEmp.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptPaySlipSingleEmp;

            rptdoc.Load(ReportPath("rptPaySlipSingleEmp.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();


        }
        if (reportFlag == "SDB")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter,BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);
            //rptSalaryDisbursementLetter rptSalaryDisbursementLetter = new rptSalaryDisbursementLetter();
            //rptSalaryDisbursementLetter.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryDisbursementLetter;

            rptdoc.Load(ReportPath("rptSalaryDisbursementLetter.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
       
        if (reportFlag == "DWA")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);
            //rptDepartmentSalaryActual actual = new rptDepartmentSalaryActual();
            //actual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = actual;

            rptdoc.Load(ReportPath("rptDepartmentSalaryActual.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "UWA")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);
            //rptUnitSalaryActual actual = new rptUnitSalaryActual();
            //actual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = actual;

            rptdoc.Load(ReportPath("rptSalaryActualUnit.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "USS")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);
            //rptUnitSalaryStamp actual = new rptUnitSalaryStamp();
            //actual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = actual;

            rptdoc.Load(ReportPath("rptUnitSalaryStamp.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "OD")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptOtherDeducUnit actual = new rptOtherDeducUnit();
            //actual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = actual;

            rptdoc.Load(ReportPath("rptOtherDeducUnit.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "ITUW")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "taxDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptTaxUnitWise actual = new rptTaxUnitWise();
            //actual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = actual;

            rptdoc.Load(ReportPath("rptTaxUnitWise.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "IT")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "taxDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptTax rptTax = new rptTax();
            //rptTax.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptTax;

            rptdoc.Load(ReportPath("rptTax.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "SWA")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptSalaryActualSection SectionActual = new rptSalaryActualSection();
            //SectionActual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = SectionActual;

            rptdoc.Load(ReportPath("rptSalaryActualSection.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "DWC")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLLCom(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);


            //rptDepartmentSalaryCom rptDepartmentSalaryCom = new rptDepartmentSalaryCom();
            //rptDepartmentSalaryCom.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptDepartmentSalaryCom;

            rptdoc.Load(ReportPath("rptDepartmentSalaryCom.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "UWS")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptUnitWiseSalarySum rptUnitWiseSalarySum = new rptUnitWiseSalarySum();
            //rptUnitWiseSalarySum.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptUnitWiseSalarySum;

            rptdoc.Load(ReportPath("rptUnitWiseSalarySum.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "STIN")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptSalaryTIN rptSalaryActual = new rptSalaryTIN();
            //rptSalaryActual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryActual;

            rptdoc.Load(ReportPath("rptSalaryTIN.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "SNID")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptSalaryNID rptSalaryActual = new rptSalaryNID();
            //rptSalaryActual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryActual;

            rptdoc.Load(ReportPath("rptSalaryNID.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "SWC")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLLCom(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptSalaryActualSection rptSalaryActualSection = new rptSalaryActualSection();
            //rptSalaryActualSection.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryActualSection;

            rptdoc.Load(ReportPath("rptSalaryActualSection.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "DWSum")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptDeptWiseSalarySum deptWiseSalarySum = new rptDeptWiseSalarySum();
            //deptWiseSalarySum.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = deptWiseSalarySum;

            rptdoc.Load(ReportPath("rptDeptWiseSalarySum.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "FSA")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.FinalSattlementSalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptSalaryActual rptSalaryActual = new rptSalaryActual();
            //rptSalaryActual.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryActual;

            rptdoc.Load(ReportPath("rptSalaryActual.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        
        if (reportFlag == "FSS")
        {
            DataTable salarymainDataTable = new DataTable();
            DataTable mainDataTable = new DataTable();
            DataTable salaryDataTable = new DataTable();

            salarymainDataTable = aSalaryReportBll.FinalSattlementSalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            mainDataTable = aSalaryReportBll.FinalSattlementSalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            salaryDataTable = aSalaryReportBll.EmpSalary(empcode).Copy();
            salarymainDataTable.TableName = "salaryDataTable";
            mainDataTable.TableName = "mainDataTableReport";
            salaryDataTable.TableName = "salDataTable";
            mainDS.Tables.Add(salaryDataTable);
            mainDS.Tables.Add(mainDataTable);
            mainDS.Tables.Add(salarymainDataTable);

            //rptFinalSettlement rptFinalSettlement = new rptFinalSettlement();
            //rptFinalSettlement.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptFinalSettlement;

            rptdoc.Load(ReportPath("rptFinalSettlement.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "ESACom")
        {
            DataTable salaryDataTable = new DataTable();

            salaryDataTable = aSalaryReportBll.SalaryReportDetailsBLLCom(ReportParameter, BankCash).Copy();
            salaryDataTable.TableName = "salaryDataTable";
            mainDS.Tables.Add(salaryDataTable);

            //rptSalaryActualCom salaryActualCom = new rptSalaryActualCom();
            //salaryActualCom.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = salaryActualCom;

            rptdoc.Load(ReportPath("rptSalaryActualCom.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "SD")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aSalaryReportBll.SalaryDeduction().Copy();
            allDataTable.TableName = "SalaryDeductDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptSalaryDeduction rptSalaryDeduction = new rptSalaryDeduction();
            //rptSalaryDeduction.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptSalaryDeduction;

            rptdoc.Load(ReportPath("rptSalaryDeduction.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "Ar")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aSalaryReportBll.ArrearRpt().Copy();
            allDataTable.TableName = "arrearDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptArrear rptArrear = new rptArrear();
            //rptArrear.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptArrear;

            rptdoc.Load(ReportPath("rptArrear.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "MA")
        {
            DataTable allDataTable = new DataTable();
            
            allDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter,BankCash).Copy();
            allDataTable.TableName = "mAllowanceDataTableDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptMobileAllowance arptMobileAllowance = new rptMobileAllowance();
            //arptMobileAllowance.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = arptMobileAllowance;

            rptdoc.Load(ReportPath("rptMobileAllowance.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "MAUW")
        {
            DataTable allDataTable = new DataTable();

            allDataTable = aSalaryReportBll.SalaryReportDetailsBLL(ReportParameter, BankCash).Copy();
            allDataTable.TableName = "mAllowanceDataTableDataTable";

            mainDS.Tables.Add(allDataTable);

           // rptMobileAllowanceUnitwise rptMobileAllowanceUnitwise = new rptMobileAllowanceUnitwise();
           // rptMobileAllowanceUnitwise.SetDataSource(mainDS);
           //crvSalaryrpt.ReportSource = rptMobileAllowanceUnitwise;

           rptdoc.Load(ReportPath("rptMobileAllowanceUnitwise.rpt"));
           rptdoc.SetDataSource(mainDS);

           crvSalaryrpt.ReportSource = rptdoc;
           crvSalaryrpt.DataBind();

        }
        if (reportFlag == "ASD")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aSalaryReportBll.AdvanceSalDeduc().Copy();
            allDataTable.TableName = "advanceSalDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptArrear rptArrear = new rptArrear();
            //rptArrear.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptArrear;

            rptdoc.Load(ReportPath("rptArrear.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "EO")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aSalaryReportBll.ExtraOverTime(monthId,monthName).Copy();
            allDataTable.TableName = "extraOtDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptExtraOvertime rptExtraOvertime = new rptExtraOvertime();
            //rptExtraOvertime.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptExtraOvertime;

            rptdoc.Load(ReportPath("rptExtraOvertime.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }
        if (reportFlag == "LR")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aSalaryReportBll.LoanAmountInfo(from,to,year).Copy();
            allDataTable.TableName = "advanceSalDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptAdvanceSalDeduc rptAdvanceSalDeduc = new rptAdvanceSalDeduc();
            //rptAdvanceSalDeduc.SetDataSource(mainDS);
            //crvSalaryrpt.ReportSource = rptAdvanceSalDeduc;

            rptdoc.Load(ReportPath("rptAdvanceSalDeduc.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvSalaryrpt.ReportSource = rptdoc;
            crvSalaryrpt.DataBind();

        }

    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }

    protected void crvSalaryrpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvSalaryrpt.Dispose();
        }
    }
    protected void crvSalaryrpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvSalaryrpt.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}