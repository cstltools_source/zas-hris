﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using DAL.HRM_DAL;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_LeaveReportViewer : System.Web.UI.Page
{
    LeaveInventoryBLL aInventoryBll = new LeaveInventoryBLL();
    ExpenseEntryDal aEntryDal = new ExpenseEntryDal();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {

        string ReportParameter = Session["PRAM"].ToString();
        DataSet mainDS = new DataSet();

        //////////Report Header for All Report//////
        /// 
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = aInventoryBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);

        //////////////////////////////
       

       DataTable leaveDataTable = new DataTable();
       leaveDataTable = aEntryDal.GetExpenseReport(ReportParameter).Copy();
       leaveDataTable.TableName = "ExpenseDataTable";
       mainDS.Tables.Add(leaveDataTable);

       //rptAllLeaveStatement rptAllLeaveStatement = new rptAllLeaveStatement();
       //rptAllLeaveStatement.SetDataSource(mainDS);
       //crvLeaveRpt.ReportSource = rptAllLeaveStatement;

       rptdoc.Load(ReportPath("crpMonthlyExpense.rpt"));
       rptdoc.SetDataSource(mainDS);

       crvLeaveRpt.ReportSource = rptdoc;
       crvLeaveRpt.DataBind();
        
            
        
        
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    
    protected void crvLeaveRpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvLeaveRpt.Dispose();
        }
    }
    protected void crvLeaveRpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvLeaveRpt.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }

}