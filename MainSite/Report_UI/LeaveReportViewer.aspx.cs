﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_LeaveReportViewer : System.Web.UI.Page
{
    LeaveInventoryBLL aInventoryBll = new LeaveInventoryBLL();

    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        string empcode = Request.QueryString["empcode"];
        string year = Request.QueryString["year"];
        string reportFlag = Request.QueryString["ReportFlag"];
        string ReportParameter = Session["ReportParameter"].ToString();
        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = aInventoryBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);

        //////////////////////////////
        if (reportFlag=="S")
        {
            DataTable leaveDataTable = new DataTable();
            DataTable leaveavailDataTable = new DataTable();
            leaveDataTable = aInventoryBll.LeaveReport(ReportParameter, year).Copy();
            leaveavailDataTable = aInventoryBll.LeaveAvailReport(empcode, year).Copy();
            leaveDataTable.TableName = "leaveDataTable";
            leaveavailDataTable.TableName = "leaveavailDataTable";
            mainDS.Tables.Add(leaveDataTable);
            mainDS.Tables.Add(leaveavailDataTable);

            //rptLeave rptLeave = new rptLeave();
            //rptLeave.SetDataSource(mainDS);
            //crvLeaveRpt.ReportSource = rptLeave;

            rptdoc.Load(ReportPath("rptLeaveNew.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvLeaveRpt.ReportSource = rptdoc;
            crvLeaveRpt.DataBind();
        }
        if (reportFlag == "A")
        {
            DataTable leaveDataTable = new DataTable();
            leaveDataTable = aInventoryBll.LeaveReport(ReportParameter, year).Copy();
            leaveDataTable.TableName = "leaveDataTable";
            mainDS.Tables.Add(leaveDataTable);

            //rptAllLeaveStatement rptAllLeaveStatement = new rptAllLeaveStatement();
            //rptAllLeaveStatement.SetDataSource(mainDS);
            //crvLeaveRpt.ReportSource = rptAllLeaveStatement;

            rptdoc.Load(ReportPath("rptAllLeaveStatement.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvLeaveRpt.ReportSource = rptdoc;
            crvLeaveRpt.DataBind();
        }
        if (reportFlag == "SN")
        {
            
            DataTable leaveDataTable = new DataTable();
            DataTable leaveavailDataTable = new DataTable();
            leaveDataTable = aInventoryBll.LeaveReport(ReportParameter, year).Copy();
            leaveavailDataTable = aInventoryBll.LeaveAvailReport(Session["ID"].ToString()).Copy();
            leaveDataTable.TableName = "leaveDataTable";
            leaveavailDataTable.TableName = "leaveavailDataTable";
            mainDS.Tables.Add(leaveDataTable);
            mainDS.Tables.Add(leaveavailDataTable);

            //rptLeaveAplication rptLeaveAplication = new rptLeaveAplication();
            //rptLeaveAplication.SetDataSource(mainDS);
            //crvLeaveRpt.ReportSource = rptLeaveAplication;

            rptdoc.Load(ReportPath("rptLeaveAplication.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvLeaveRpt.ReportSource = rptdoc;
            crvLeaveRpt.DataBind();
        }
            
        
        
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    
    protected void crvLeaveRpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvLeaveRpt.Dispose();
        }
    }
    protected void crvLeaveRpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvLeaveRpt.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }

}