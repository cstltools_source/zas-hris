﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_MultipleReportViewer : System.Web.UI.Page
{
    MisReportBLL aMisReportBll=new MisReportBLL();
    RptHeadingBLL aRptHeadingBll=new RptHeadingBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        string rptType = Request.QueryString["rptType"];
        string year = Request.QueryString["year"];
        string fromdt = Request.QueryString["fromdate"];
        string todt = Request.QueryString["todate"];
        string from = Request.QueryString["from"];
        string to = Request.QueryString["to"];
     
        DataSet mainDS = new DataSet();
       
        DataTable reportHederDataTable = new DataTable();
        
        reportHederDataTable = aRptHeadingBll.LoadRptHeaderReport().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////
        if (rptType == "YJL")
        {
            ////nameLabel.Text = "Yearly Join and Left";
            DataTable aDataTable = new DataTable();
            aDataTable = aMisReportBll.YearWiseJoinLeftReport(year).Copy();
            aDataTable.TableName = "empJoinLeftDataTable";

            mainDS.Tables.Add(aDataTable);

            //rptYearlyEmpJointandLeft rptYearlyEmpJointandLeft = new rptYearlyEmpJointandLeft();
            //rptYearlyEmpJointandLeft.SetDataSource(mainDS);
            //crvMisrpt.ReportSource = rptYearlyEmpJointandLeft;

            rptdoc.Load(ReportPath("rptYearlyEmpJointandLeft.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMisrpt.ReportSource = rptdoc;
            crvMisrpt.DataBind();

        }
        if (rptType == "YAS")
        {
            DataTable allDataTable = new DataTable();
            allDataTable = aMisReportBll.YearlyAttendanceStatement(fromdt, todt).Copy();
            allDataTable.TableName = "yearlyLeaveDataTable";

            mainDS.Tables.Add(allDataTable);

            rptdoc.Load(ReportPath("rptYearlyAttendanceStatement.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMisrpt.ReportSource = rptdoc;
            crvMisrpt.DataBind();
        }
        if (rptType == "DWTSC")
        {
            ////nameLabel.Text = "Department Wise Total Salary ";
            DataTable aDataTable = new DataTable();
            aDataTable = aMisReportBll.DeptSalReport().Copy();
            aDataTable.TableName = "deptEmpSalaryDataTable";

            mainDS.Tables.Add(aDataTable);

            //rptDeptWiseSalChart rptDeptWiseSalChart = new rptDeptWiseSalChart();
            //rptDeptWiseSalChart.SetDataSource(mainDS);
            //crvMisrpt.ReportSource = rptDeptWiseSalChart;

            rptdoc.Load(ReportPath("rptDeptWiseSalChart.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMisrpt.ReportSource = rptdoc;
            crvMisrpt.DataBind();

        }
        if (rptType == "DWTS")
        {
            ////nameLabel.Text = "Department Wise Total Salary ";
            DataTable aDataTable = new DataTable();
            aDataTable = aMisReportBll.DeptSalReport().Copy();
            aDataTable.TableName = "deptEmpSalaryDataTable";

            mainDS.Tables.Add(aDataTable);

            //rptDeptEmpSalary rptDeptEmpSalary = new rptDeptEmpSalary();
            //rptDeptEmpSalary.SetDataSource(mainDS);
            //crvMisrpt.ReportSource = rptDeptEmpSalary;

            rptdoc.Load(ReportPath("rptDeptEmpSalary.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMisrpt.ReportSource = rptdoc;
            crvMisrpt.DataBind();
            
        }
        if (rptType == "YSG")
        {
            ////nameLabel.Text = "Yearly Salary Chart";
            DataTable aDataTable = new DataTable();
            
            aDataTable = aMisReportBll.Temp().Copy();
            aDataTable.TableName = "totalSalDataTable";

            mainDS.Tables.Add(aDataTable);

            //rptYearlySalGraph rptYearlySalGraph = new rptYearlySalGraph();
            //rptYearlySalGraph.SetDataSource(mainDS);
            //crvMisrpt.ReportSource = rptYearlySalGraph;

            rptdoc.Load(ReportPath("rptYearlySalGraph.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMisrpt.ReportSource = rptdoc;
            crvMisrpt.DataBind();
        }

    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    
    protected void crvMisrpt_Disposed(object sender, EventArgs e)
    {

        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvMisrpt.Dispose();
        }
    }
    protected void crvMisrpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvMisrpt.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}