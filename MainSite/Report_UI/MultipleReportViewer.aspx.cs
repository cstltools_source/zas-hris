﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;


public partial class Report_UI_MultipleReportViewer : System.Web.UI.Page
{
    MultipleRportBLL aMultipleRportBll=new MultipleRportBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        string rptType = Request.QueryString["rptType"];
     
        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();
        
        reportHederDataTable = aMultipleRportBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////
        if (rptType == "SG")
        {
            //nameLabel.Text = "Salary Grade";
            DataTable salGradeDataTable = new DataTable();
            salGradeDataTable = aMultipleRportBll.SalaryGrade().Copy();
            salGradeDataTable.TableName = "salGradeDataTable";

            mainDS.Tables.Add(salGradeDataTable);

            //rptSalaryGrade rptSalaryGrade = new rptSalaryGrade();
            //rptSalaryGrade.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptSalaryGrade;

            rptdoc.Load(ReportPath("rptSalaryGrade.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();
        }
        if (rptType == "EU")
        {

            //nameLabel.Text = "User Information";
            DataTable allDataTable = new DataTable();
            allDataTable = aMultipleRportBll.UserRpt().Copy();
            allDataTable.TableName = "UserInformationDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptUserInformation rptrptEmpPromotione = new rptUserInformation();
            //rptrptEmpPromotione.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptrptEmpPromotione;

            rptdoc.Load(ReportPath("rptUserInformation.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "C")
        {
            //nameLabel.Text = "Company Info";
            DataTable companyDataTable = new DataTable();
            companyDataTable = aMultipleRportBll.CompanyRpt().Copy();
            companyDataTable.TableName = "companyDataTable";

            mainDS.Tables.Add(companyDataTable);

            //rptCompanyInfo rptCompanyInfo = new rptCompanyInfo();
            //rptCompanyInfo.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptCompanyInfo;

            rptdoc.Load(ReportPath("rptCompanyInfo.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "U")
        {
            //nameLabel.Text = "Unit ";
             DataTable unitDataTable = new DataTable();
             unitDataTable = aMultipleRportBll.UnitRpt().Copy();
             unitDataTable.TableName = "unitDataTable";

             mainDS.Tables.Add(unitDataTable);

             //rptUnit rptUnit = new rptUnit();
             //rptUnit.SetDataSource(mainDS);
             //crvMultiplerpt.ReportSource = rptUnit;

             rptdoc.Load(ReportPath("rptUnit.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvMultiplerpt.ReportSource = rptdoc;
             crvMultiplerpt.DataBind();

        }
        if (rptType == "D")
        {
            //nameLabel.Text = "Division ";
            DataTable divisionDataTable = new DataTable();
            divisionDataTable = aMultipleRportBll.DivisionRpt().Copy();
            divisionDataTable.TableName = "divisionDataTable";

            mainDS.Tables.Add(divisionDataTable);

            //rptDivision rptDivision = new rptDivision();
            //rptDivision.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptDivision;

            rptdoc.Load(ReportPath("rptDivision.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "DP")
        {
            //nameLabel.Text = "Department ";
            DataTable deptDataTable = new DataTable();
            deptDataTable = aMultipleRportBll.DepartmentRpt().Copy();
            deptDataTable.TableName = "deptDataTable";

            mainDS.Tables.Add(deptDataTable);

            //rptDepartment rptDepartment = new rptDepartment();
            //rptDepartment.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptDepartment;

            rptdoc.Load(ReportPath("rptDepartment.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "S")
        {
            //nameLabel.Text = "Section ";
            DataTable sectionDataTable = new DataTable();
            sectionDataTable = aMultipleRportBll.SectionRpt().Copy();
            sectionDataTable.TableName = "sectionDataTable";

            mainDS.Tables.Add(sectionDataTable);

            //rptSection rptSection = new rptSection();
            //rptSection.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptSection;

            rptdoc.Load(ReportPath("rptSection.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "ET")
        {
            //nameLabel.Text = "Employee Type ";
            DataTable empTypeDataTable = new DataTable();
            empTypeDataTable = aMultipleRportBll.EmpTypeRpt().Copy();
            empTypeDataTable.TableName = "empTypeDataTable";

            mainDS.Tables.Add(empTypeDataTable);

            //rptEmpType rptEmpType = new rptEmpType();
            //rptEmpType.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptEmpType;

            rptdoc.Load(ReportPath("rptEmpType.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "L")
        {
            //nameLabel.Text = "Line ";
            DataTable lineDataTable = new DataTable();
            lineDataTable = aMultipleRportBll.LineRpt().Copy();
            lineDataTable.TableName = "lineDataTable";

            mainDS.Tables.Add(lineDataTable);

            //rptLine rptLine = new rptLine();
            //rptLine.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptLine;

            rptdoc.Load(ReportPath("rptLine.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "DG")
        {
            //nameLabel.Text = "Designation ";
            DataTable desigDataTable = new DataTable();
            desigDataTable = aMultipleRportBll.DesignationRpt().Copy();
            desigDataTable.TableName = "aDataTable";

            mainDS.Tables.Add(desigDataTable);

            //rptDesignation rptDesignation = new rptDesignation();
            //rptDesignation.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptDesignation;

            rptdoc.Load(ReportPath("rptDesignation.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "G")
        {
            //nameLabel.Text = "Employee Grade ";
            DataTable gradeDataTable = new DataTable();
            gradeDataTable = aMultipleRportBll.EmpGradeRpt().Copy();
            gradeDataTable.TableName = "empgradeDataTable";

            mainDS.Tables.Add(gradeDataTable);

            //rptGrade rptGrade = new rptGrade();
            //rptGrade.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptGrade;

            rptdoc.Load(ReportPath("rptGrade.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "SH")
        {
            //nameLabel.Text = "Shift ";
            DataTable shiftDataTable = new DataTable();
            shiftDataTable = aMultipleRportBll.ShiftRpt().Copy();
            shiftDataTable.TableName = "shiftDataTable";

            mainDS.Tables.Add(shiftDataTable);

            //rptShift rptShift = new rptShift();
            //rptShift.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptShift;

            rptdoc.Load(ReportPath("rptShift.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        }
        if (rptType == "EGI")
        {
            //nameLabel.Text = "Shift ";
            DataTable shiftDataTable = new DataTable();
            shiftDataTable = aMultipleRportBll.EmpAttGroupRpt().Copy();
            shiftDataTable.TableName = "EmployeeGroupDataTable";

            mainDS.Tables.Add(shiftDataTable);

            //rptEmployeeManualAttGroup rptShift = new rptEmployeeManualAttGroup();
            //rptShift.SetDataSource(mainDS);
            //crvMultiplerpt.ReportSource = rptShift;

            rptdoc.Load(ReportPath("rptEmployeeManualAttGroup.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvMultiplerpt.ReportSource = rptdoc;
            crvMultiplerpt.DataBind();

        } 
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void crvMultiplerpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvMultiplerpt.Dispose();
        }
    }
    protected void crvMultiplerpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvMultiplerpt.Dispose();
        }
    }

    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}