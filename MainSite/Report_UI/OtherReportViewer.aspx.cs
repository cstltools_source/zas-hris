﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_OtherReportViewer : System.Web.UI.Page
{
    OtherReportBLL OtherReportBLL=new OtherReportBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        string rptType = Request.QueryString["rptType"];
        string empcode = Request.QueryString["empcode"];
        string year = Request.QueryString["year"];
        string fromdt = Request.QueryString["fromdt"];
        string actionstatus = Request.QueryString["actionstatus"];
        string todt = Request.QueryString["todt"];
        string status = Request.QueryString["status"];
        string ReportParameter = Session["ReportParameter"].ToString();

        //string loanId = Session["LoanId"].ToString();

        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = OtherReportBLL.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////
         if (rptType == "ME")
         {
             
            ////nameLabel.Text = "Manual Attandance";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.ManualAttRpt(ReportParameter).Copy();
            allDataTable.TableName = "ManualAttDataTable";

            mainDS.Tables.Add(allDataTable);
             

            //rptManualAttandance rptrptEmpPromotione = new rptManualAttandance();
            //rptrptEmpPromotione.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptrptEmpPromotione;

            rptdoc.Load(ReportPath("rptManualAttandance.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
         if (rptType == "U")
         {

             //nameLabel.Text = "User Information";
             DataTable allDataTable = new DataTable();
             allDataTable = OtherReportBLL.UserRpt(ReportParameter).Copy();
             allDataTable.TableName = "UserInformationDataTable";

             mainDS.Tables.Add(allDataTable);


             //rptUserInformation rptrptEmpPromotione = new rptUserInformation();
             //rptrptEmpPromotione.SetDataSource(mainDS);
             //crvotherrpt.ReportSource = rptrptEmpPromotione;

             rptdoc.Load(ReportPath("rptUserInformation.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvotherrpt.ReportSource = rptdoc;
             crvotherrpt.DataBind();
         }
        if (rptType == "EJ")
        {
            //nameLabel.Text = "Job Left";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.JobleftRpt(ReportParameter).Copy();
            allDataTable.TableName = "JobleftDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptJobLeft rptrptEmpPromotione = new rptJobLeft();
            //rptrptEmpPromotione.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptrptEmpPromotione;

            rptdoc.Load(ReportPath("rptJobLeft.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "HD")
        {
            //nameLabel.Text = "Holiday";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.HolidayRpt(ReportParameter).Copy();
            allDataTable.TableName = "HolidayDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptHoliday rptrptEmpPromotione = new rptHoliday();
            //rptrptEmpPromotione.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptrptEmpPromotione;

            rptdoc.Load(ReportPath("rptHoliday.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "RA")
        {
            //nameLabel.Text = "Reappointment";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.ReappointmentRpt(ReportParameter).Copy();
            allDataTable.TableName = "ReappointmentDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptReApointment rptrptEmpPromotione = new rptReApointment();
            //rptrptEmpPromotione.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptrptEmpPromotione;

            rptdoc.Load(ReportPath("rptReApointment.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "PRO")
        {
            //////nameLabel.Text = "Promotion";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.PromotionRpt(ReportParameter).Copy();
            allDataTable.TableName = "promotionDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptEmpPromotions rptrptEmpPromotione = new rptEmpPromotions();
            //rptrptEmpPromotione.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptrptEmpPromotione;

            rptdoc.Load(ReportPath("rptEmpPromotions.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "PIL")
        {
            //nameLabel.Text = "Promotion";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.PromotionRpt(ReportParameter).Copy();
            allDataTable.TableName = "promotionDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptEmpPromotions rptrptEmpPromotione = new rptEmpPromotions();
            //rptrptEmpPromotione.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptrptEmpPromotione;

            rptdoc.Load(ReportPath("rptEmpPromotions.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "LA")
        {
            //nameLabel.Text = "Late Approve";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.LateApproveRpt(ReportParameter).Copy();
            allDataTable.TableName = "lateAppDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptLateApprove rptLateApprove = new rptLateApprove();
            //rptLateApprove.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptLateApprove;

            rptdoc.Load(ReportPath("rptLateApprove.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "MA")
        {
            //nameLabel.Text = "Mobile Allowance";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.MobileAllowance(ReportParameter).Copy();
            allDataTable.TableName = "mAllowanceDataTableDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptMobileAllowance arptMobileAllowance = new rptMobileAllowance();
            //arptMobileAllowance.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = arptMobileAllowance;

            rptdoc.Load(ReportPath("rptMobileAllowance.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "MAUW")
        {
            //nameLabel.Text = "Mobile Allowance";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.MobileAllowance(ReportParameter).Copy();
            allDataTable.TableName = "mAllowanceDataTableDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptMobileAllowanceUnitwise rptMobileAllowanceUnitwise = new rptMobileAllowanceUnitwise();
            //rptMobileAllowanceUnitwise.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptMobileAllowanceUnitwise;

            rptdoc.Load(ReportPath("rptMobileAllowanceUnitwise.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "SD")
        {
            //nameLabel.Text = "Salary Deduction";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.SalaryDeduction(ReportParameter).Copy();
            allDataTable.TableName = "SalaryDeductDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptSalaryDeduction rptSalaryDeduction = new rptSalaryDeduction();
            //rptSalaryDeduction.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptSalaryDeduction;

            rptdoc.Load(ReportPath("rptSalaryDeduction.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "LN")
        {
            //nameLabel.Text = "Employee Loan";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.LoanRptDAL(ReportParameter).Copy();
            allDataTable.TableName = "EmployeeLoanDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptEmployeeLoan rptEmployeeLoan = new rptEmployeeLoan();
            //rptEmployeeLoan.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptEmployeeLoan;

            rptdoc.Load(ReportPath("rptEmployeeLoan.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "LND")
        {
            //nameLabel.Text = "Employee Loan";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.LoanRptDALNew(ReportParameter).Copy();
            allDataTable.TableName = "EmployeeLoanDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptEmployeeLoanDetail rptEmployeeLoan = new rptEmployeeLoanDetail();
            //rptEmployeeLoan.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptEmployeeLoan;

            rptdoc.Load(ReportPath("rptEmployeeLoanDetail.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "LMW")
        {
            //nameLabel.Text = "Employee Loan";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.LoanDetailYearly(year).Copy();
            allDataTable.TableName = "EmpLoanDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptEmployeeLoanMW rptEmployeeLoan = new rptEmployeeLoanMW();
            //rptEmployeeLoan.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptEmployeeLoan;

            rptdoc.Load(ReportPath("rptEmployeeLoanMW.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "LNS")
        {
            //nameLabel.Text = "Employee Loan";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.LoanRptDALSingle(ReportParameter,empcode,fromdt,todt).Copy();
            allDataTable.TableName = "EmployeeLoanDataTable";

            mainDS.Tables.Add(allDataTable);

            //EmployeeWiseLoan rptEmployeeLoan = new EmployeeWiseLoan();
            //rptEmployeeLoan.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptEmployeeLoan;

            rptdoc.Load(ReportPath("EmployeeWiseLoan.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "TY")
        {
            //nameLabel.Text = "Employee Income Tax";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.TaxRpt(fromdt, todt).Copy();
            allDataTable.TableName = "taxDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptIncomeTax rptIncomeTax = new rptIncomeTax();
            //rptIncomeTax.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptIncomeTax;

            rptdoc.Load(ReportPath("rptIncomeTax.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "AR")
        {
            //nameLabel.Text = "Arrear";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.ArrearRpt(ReportParameter).Copy();
            allDataTable.TableName = "arrearDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptArrear rptArrear = new rptArrear();
            //rptArrear.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptArrear;

            rptdoc.Load(ReportPath("rptArrear.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "SI")
        {
            //nameLabel.Text = "Salary Increment";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.IncrementRpt(ReportParameter).Copy();
            allDataTable.TableName = "IncrementDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptIncrement rptArrear = new rptIncrement();
            //rptArrear.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptArrear;

            rptdoc.Load(ReportPath("rptIncrement.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "SIA" )
        {

            //nameLabel.Text = "Salary Increment";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.AllIncrement( fromdt, todt, actionstatus).Copy();
            allDataTable.TableName = "incremtDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptAllEmpWIncrement rptAllEmpWIncrement = new rptAllEmpWIncrement();
            //rptAllEmpWIncrement.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptAllEmpWIncrement;

            rptdoc.Load(ReportPath("rptAllEmpWIncrement.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "SI" && status == "True")
        {

            //nameLabel.Text = "Salary Increment";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.SingleIncrement(empcode, fromdt, todt, actionstatus).Copy();
            allDataTable.TableName = "incremtDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptEmpWIncrement rptEmpWIncrement = new rptEmpWIncrement();
            //rptEmpWIncrement.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptEmpWIncrement;

            rptdoc.Load(ReportPath("rptEmpWIncrement.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }

        if (rptType == "PI" && status == "True")
        {

            //nameLabel.Text = "Promotional Increment";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.SingleIncrement(empcode, fromdt, todt, actionstatus).Copy();
            allDataTable.TableName = "incremtDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptPromotionIncrement rptPromotionIncrement = new rptPromotionIncrement();
            //rptPromotionIncrement.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptPromotionIncrement;

            rptdoc.Load(ReportPath("rptPromotionIncrement.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "ES")
        {
            //nameLabel.Text = "Suspend";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.SuspendRpt(ReportParameter).Copy();
            allDataTable.TableName = "SuspendDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptSuspend rptArrear = new rptSuspend();
            //rptArrear.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptArrear;

            rptdoc.Load(ReportPath("rptSuspend.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "R")
        {
            //nameLabel.Text = "Reappointment";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.ReappontmentRpt(ReportParameter).Copy();
            allDataTable.TableName = "reappoinmentDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptReAppointment rptReAppointment = new rptReAppointment();
            //rptReAppointment.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptReAppointment;

            rptdoc.Load(ReportPath("rptReAppointment.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "TF")
        {
            //nameLabel.Text = "Transfer";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.TransferRpt(ReportParameter).Copy();
            allDataTable.TableName = "transferDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptTransfer rptTransfer = new rptTransfer();
            //rptTransfer.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptTransfer;

            rptdoc.Load(ReportPath("rptTransfer.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "TL")
        {
            //nameLabel.Text = "Transfer";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.TransferRpt(ReportParameter).Copy();
            allDataTable.TableName = "transferDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptTransferLetter rptTransferLetter = new rptTransferLetter();
            //rptTransferLetter.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptTransferLetter;

            rptdoc.Load(ReportPath("rptTransferLetter.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();

        }
        if (rptType == "TX")
        {
            //nameLabel.Text = "Tax";
            DataTable allDataTable = new DataTable();
            allDataTable = OtherReportBLL.TaxRpt(ReportParameter).Copy();
            allDataTable.TableName = "taxDataTable";

            mainDS.Tables.Add(allDataTable);

            //rptTax rptTax = new rptTax();
            //rptTax.SetDataSource(mainDS);
            //crvotherrpt.ReportSource = rptTax;


            rptdoc.Load(ReportPath("rptTax.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvotherrpt.ReportSource = rptdoc;
            crvotherrpt.DataBind();
        }
     
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }

    protected void crvotherrpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvotherrpt.Dispose();
        }
    }
    protected void crvotherrpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvotherrpt.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }
}