﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;
using Library.CrystalReports.HRM_DataSets;
//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_SectionReport : System.Web.UI.Page
{
    SectionBLL aSectionBll = new SectionBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {
        DataSet mainDS = new DataSet();
        DataTable aDataTableReport = new DataTable();

        aDataTableReport = aSectionBll.LoadSectionView();
        dsSection aDsSection = new dsSection();
        foreach (DataRow row in aDataTableReport.Rows)
        {
            aDsSection.aDataTable.ImportRow(row);
        }
        //rptSection aRptSection = new rptSection();
        //aRptSection.SetDataSource(aDsSection);
        //crvSection.ReportSource = aRptSection;

        rptdoc.Load(ReportPath("rptSection.rpt"));
        rptdoc.SetDataSource(mainDS);

        crvSection.ReportSource = rptdoc;
        crvSection.DataBind();
    }
    protected void crvSection_Init(object sender, EventArgs e)
    {

    }

    protected void crvSection_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvSection.Dispose();
        }
    }
    protected void crvSection_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvSection.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }

    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
}