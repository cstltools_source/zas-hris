﻿using System;
using System.Data;
using System.Web.UI;
using CrystalDecisions.CrystalReports.Engine;
using Library.BLL.HRM_BLL;

//using Library.CrystalReports.HRM_RPT;

public partial class Report_UI_SuspendJobleftReportViewer : System.Web.UI.Page
{
    SuspendJobLeftReportBLL aSuspendJobLeftReportBll=new SuspendJobLeftReportBLL();
    ReportDocument rptdoc = new ReportDocument();
    protected void Page_Init(object sender, EventArgs e)
    {

        string rptType = Request.QueryString["rptType"];
        string empId = Request.QueryString["empId"];
        string parameter = Session["ReportParameter"].ToString();
        DataSet mainDS = new DataSet();
        //////////Report Header for All Report//////
        DataTable reportHederDataTable = new DataTable();

        reportHederDataTable = aSuspendJobLeftReportBll.RptHeader().Copy();
        reportHederDataTable.TableName = "reportHederDataTable";

        mainDS.Tables.Add(reportHederDataTable);
         
         //////////////////////////////

        if (rptType == "S" || rptType=="SA")
        {
            DataTable suspendDataTable = new DataTable();
            suspendDataTable = aSuspendJobLeftReportBll.SuspendRpt(parameter).Copy();
            suspendDataTable.TableName = "suspendjobleftDataTable";

            mainDS.Tables.Add(suspendDataTable);

            //rptSuspendJobleft rptSuspendJobleft = new rptSuspendJobleft();
            //rptSuspendJobleft.SetDataSource(mainDS);
            //crvsuspendjobleftrpt.ReportSource = rptSuspendJobleft;

            rptdoc.Load(ReportPath("rptSuspendJobleft.rpt"));
            rptdoc.SetDataSource(mainDS);

            crvsuspendjobleftrpt.ReportSource = rptdoc;
            crvsuspendjobleftrpt.DataBind();

        }

        if (rptType == "J" || rptType == "JLA")
         {
             DataTable jobleftDataTable = new DataTable();
             jobleftDataTable = aSuspendJobLeftReportBll.JobLeftRpt(parameter).Copy();
             jobleftDataTable.TableName = "suspendjobleftDataTable";

             mainDS.Tables.Add(jobleftDataTable);

             //rptSuspendJobleft rptSuspendJobleft = new rptSuspendJobleft();
             //rptSuspendJobleft.SetDataSource(mainDS);
             //crvsuspendjobleftrpt.ReportSource = rptSuspendJobleft;

             rptdoc.Load(ReportPath("rptSuspendJobleft.rpt"));
             rptdoc.SetDataSource(mainDS);

             crvsuspendjobleftrpt.ReportSource = rptdoc;
             crvsuspendjobleftrpt.DataBind();

         }
    }
    protected void closeButton_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, typeof(string), "Close", "window.close()", true);
    }
    protected void crvsuspendjobleftrpt_Disposed(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvsuspendjobleftrpt.Dispose();
        }
    }
    protected void crvsuspendjobleftrpt_Unload(object sender, EventArgs e)
    {
        if (this.rptdoc != null)
        {
            rptdoc.Close();
            rptdoc.Dispose();
            crvsuspendjobleftrpt.Dispose();
        }
    }
    private string ReportPath(string rptName)
    {
        return Convert.ToString(Server.MapPath("~\\HR_Report\\" + rptName));
    }

}