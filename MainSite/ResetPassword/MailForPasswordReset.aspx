﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MailForPasswordReset.aspx.cs" Inherits="ResetPassword_MailForPasswordReset" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Mail Send</title>
    
    <style type="text/css"> 
		
		body, html, h1, h2, h3, h4 ,h6, p,
		ul, li, th, td{
			margin:0;
			padding:0;
		}
		
		body{
			background-color:#E0E0E0;
		}
	
		.forgot-password{
			background-color:#49B6D6;
			padding: 40px;
			width: 50%;
			height: 400px;
			position:absolute;
			top:0;
			right:0;
			bottom:0;
			left:0;
			margin: auto;
			border-radius: 5px;
		}
		
		h1{
			text-align:center;
			color:#FFF; 
			margin: 20px auto;
			display:block;
		}
		
		.email-box{
			width:60%;
			border: 1px solid #FFF;
			margin: 20px auto;
			background-color:#FFF;
			padding: 40px;
			border-radius: 10px;
			text-align:center;
		}
		
		.email-box p{
			padding: 7px 0 12px 0;
		}
		
		.email-box .reset-email{
			width: 80%;
			box-sizing: border-box;
			font-size: 14px;
			padding: 5px;
			border-radius: 5px;
			border: 1px solid #B0BEC5;
		}
		
		.email-box .submit-button{
			margin: 20px;
			width: 50%;
			padding: 8px;
			border: none;
			background-color:#6B966E;
			color:#FFF;
			border-radius: 5px;
			cursor: pointer;
		}

        .ta {
            text-align: left;
        }
		
	</style>
</head>
<body>
    <form id="form1" runat="server">
        <!-- Forgot Password box -->
        <div class="forgot-password">
            <h1>Reset Password Form </h1>
            <div class="email-box">
                <h2>Forgot password </h2>
                <p>Please provide your email address </p>
                
                <div runat="server"  id="mail" Visible="true">
                    <asp:TextBox ID="resetEmailTextBox" class="reset-email" placeholder="exapmle@gmail.com"  runat="server"></asp:TextBox>
                </div>

                <div runat="server"  id="reset" Visible="false">
                    <asp:HiddenField ID="employeeId" runat="server" />
                    <asp:TextBox ID="passwordTextBox" class="reset-email ta" placeholder="New password"  runat="server"></asp:TextBox>
                    <br />
                    <asp:TextBox ID="conPasswordTextBox" class="reset-email ta" placeholder="Confirm password"  AutoPostBack="True" OnTextChanged="conPasswordTextBox_OnTextChanged" runat="server"></asp:TextBox>
                </div>
                
                <asp:Button ID="seubmitButton" class="submit-button" runat="server" Text="Reset my password" OnClick="seubmitButton_OnClick" />

            </div>
        </div>
    </form>
</body>
</html>
