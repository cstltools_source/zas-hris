﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.HRM_BLL;

public partial class ResetPassword_MailForPasswordReset : System.Web.UI.Page
{
    MailForPasswordResetBll aResetMailBll = new MailForPasswordResetBll();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    private void ShowMessageBox(string message)
    {
        string sScript;
        message = message.Replace("'", "\'");
        sScript = String.Format("alert('{0}');", message);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", sScript, true);
    }
    protected void seubmitButton_OnClick(object sender, EventArgs e)
    {
        if (Validation())
        {
            DataTable aDataTable = new DataTable();
            aDataTable = aResetMailBll.LoadUserIsValidOrNotInfo(resetEmailTextBox.Text.Trim());

            if (employeeId.Value == "")
            {
                if (aDataTable.Rows.Count > 0)
                {

                    //SendMail(aDataTable);
                    employeeId.Value = aDataTable.Rows[0].Field<Int32>("UserId").ToString(CultureInfo.InvariantCulture);
                    ProvidePasswordResetOption();
                }
                else
                {
                    ShowMessageBox("Please select valid email address!!!");
                    Clear();
                }
            }

            else
            {
                if (aResetMailBll.UpdateUserPassword(passwordTextBox.Text.Trim(), employeeId.Value))
                {
                    ShowMessageBox("Password Reset Successfully!!!");
                    Clear();
                }
                else
                {
                    ShowMessageBox("Password does not reset!!!");
                }
            }
        }
    }

    private void Clear()
    {
        employeeId.Value = " ";
        passwordTextBox.Text = "";
        conPasswordTextBox.Text = "";
        resetEmailTextBox.Text = "";

        //reset.Visible = false;
        //mail.Visible = true;
    }

    private void ProvidePasswordResetOption()
    {

        if (employeeId.Value != "")
        {
            reset.Visible = true;
            mail.Visible = false;
        }
        else
        {
            reset.Visible = false;
            mail.Visible = true;
        }

    }

    private void SendMail(DataTable aDataTable)
    {
        var fromAddress = "aptechdesignsltd@gmail.com";
        var toAddress = resetEmailTextBox.Text;
        const string fromPassword = "adomain@#";
        string subject = "User Password Reset";

        string body = "Reset my password:" + "\n";
        body += "Name: " + aDataTable.Rows[0].Field<String>("UserName") + "\n";
        body += "Employee Code: " + aDataTable.Rows[0].Field<String>("EmpMasterCode") + "\n";
        body += "LoginName: " + aDataTable.Rows[0].Field<String>("LoginName") + "\n";

        // smtp settings
        var smtp = new System.Net.Mail.SmtpClient();
        {
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
            smtp.Timeout = 20000;
        }

        // Passing values to smtp object
        smtp.Send(fromAddress, toAddress, subject, body);
        
    }

    private bool Validation()
    {
        if (employeeId.Value != "")
        {
            if (passwordTextBox.Text == "")
            {
                ShowMessageBox("Sorry! You have to provide new password!!!!");
                return false;
            }

            if (conPasswordTextBox.Text == "")
            {
                ShowMessageBox("Sorry! You have to provide confirm password!!!!");
                return false;
            }
        }

        else
        {
            if (resetEmailTextBox.Text == "")
            {
                ShowMessageBox("Sorry! You have to provide an email address!!!!");
                return false;
            }
        }
        
        return true;
    }

    protected void conPasswordTextBox_OnTextChanged(object sender, EventArgs e)
    {
        if (conPasswordTextBox.Text != "" && passwordTextBox.Text != "")
        {
            if (conPasswordTextBox.Text.Trim() != passwordTextBox.Text.Trim())
            {
                conPasswordTextBox.Text = "";
                ShowMessageBox("Password not matched!!!");
            }
        }
    }
}