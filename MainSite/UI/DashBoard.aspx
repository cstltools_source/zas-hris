﻿<%@ Page Title="Full Alert" Language="C#" MasterPageFile="~/MasterPages/MainMasterPage.master" AutoEventWireup="true" CodeFile="DashBoard.aspx.cs" Inherits="UI_DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="content" id="content">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>

                <!-- PAGE HEADING -->
                <div class="page-heading">
                    <div class="page-heading__container">
                        <div class="icon"><span class="li-register"></span></div>
                        <span></span>
                        <h1 class="title" style="font-size: 18px; padding-top: 9px;">Dashboard </h1>
                    </div>
                    <div class="page-heading__container float-right d-none d-sm-block">
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../UI/DashBoard.aspx">Dashboard</a></li>
                        </ol>
                    </nav>
                </div>
                <!-- //END PAGE HEADING -->

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-lg-3 margin-bottom-20">
                            <div class="widget invert ">
                                <div class="widget__icon_layer widget__icon_layer--middle"><span class="li-cash-dollar"></span></div>
                                <div class="widget__container">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="widget__informer text-left">
                                                Number of Company Unit
                                            </div>
                                        </div>

                                        <div class="col-4">
                                            <div class="widget__line">
                                                <asp:Label ID="totalGrossValueLabel" CssClass="float-right text-right" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="total_unit" CssClass="label-font-size" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="totalorderQtyLabel" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3 margin-bottom-20">
                            <div class="widget invert bg-gradient-11">
                                <div class="widget__icon_layer widget__icon_layer--middle"><span class="li-cash-dollar"></span></div>
                                <div class="widget__container">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="widget__informer text-left">
                                                Employee 
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="widget__informer text-left">
                                                On Duty
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row bottom">
                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="total_employee" CssClass="label-font-size" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="odLabel" CssClass="label-font-size" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 col-lg-3 margin-bottom-20">
                            <div class="widget invert bg-gradient-15">
                                <div class="widget__icon_layer widget__icon_layer--middle"><span class="li-cash-dollar"></span></div>
                                <div class="widget__container">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="widget__line text-center">
                                                Late 
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="widget__line text-left">
                                                Leave
                                            </div>
                                        </div>
                                    </div>

                                    <p></p>

                                    <div class="row">
                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="lateLabel" CssClass="label-font-size" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="leaveLabel" CssClass="label-font-size" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-lg-3 margin-bottom-20">
                            <div class="widget invert bg-gradient-14">
                                <div class="widget__icon_layer widget__icon_layer--middle"><span class="li-cash-dollar"></span></div>
                                <div class="widget__container">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="widget__line text-center">
                                                Present  
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="widget__line text-center">
                                                Absent
                                            </div>
                                        </div>
                                    </div>
                                    <p></p>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="presentLabel" CssClass="label-font-size" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <div class="widget__informer text-center">
                                                <asp:Label ID="absentLabel" CssClass="label-font-size" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="panel minimal minimal-gray">
                                        <div class="panel-body">
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="profile-1">
                                                    <div>
                                                        <div class="wrapper" style="height: 400px; text-align: center; padding: 120px">
                                                            <%-- <asp:Image ID="imgWait" runat="server" ImageAlign="Middle" ImageUrl="~/Assets/img/progress-bar-opt 3.gif"
                                                                        Height="155" Width="200" />--%>
                                                            <span style="font-weight: bold; font-size: 18px;"> Welcome to  </span>
                                                            <h1 style="font-weight: bold; color: #4098E0; margin-bottom: 5px;">" Full Alert HRM " </h1>
                                                            <h4 style="font-weight: bold">Copyright &copy; <%: DateTime.Now.Year %> Creatrix Soft Tech Ltd. </h4>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>



