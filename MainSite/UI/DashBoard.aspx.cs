﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL.HRM_BLL;

public partial class UI_DashBoard : System.Web.UI.Page
{
    DashBoardBll aDashBoardBll = new DashBoardBll();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           LoadCartInformation();
            LoadAttData();
        }
    }

    private void LoadCartInformation()
    {
        var numberofUnit = aDashBoardBll.LoadCompanyUnitInfo();

        if (numberofUnit.Rows.Count > 0)
        {
            total_unit.Text = Convert.ToString(numberofUnit.Rows.Count);
        }

        var numberofEmployee = aDashBoardBll.LoadEmployeeInfo();

        if (numberofEmployee.Rows.Count > 0)
        {
            total_employee.Text = Convert.ToString(numberofEmployee.Rows.Count);
        }
    }

    public void LoadAttData()
    {
        DataTable dt = aDashBoardBll.GetEmpAtt(DateTime.Now.ToString("dd-MMM-yyyy"));
        if (dt.Rows.Count>0)
        {
            leaveLabel.Text = dt.Rows[0]["Leave"].ToString();
            odLabel.Text = dt.Rows[0]["OnDuty"].ToString();
            lateLabel.Text = dt.Rows[0]["Late"].ToString();
            presentLabel.Text = dt.Rows[0]["Present"].ToString();
            absentLabel.Text = dt.Rows[0]["Absent"].ToString();
        }
    }

    //private void LoadCartInformation()
    //{
    //    aDashBoardBll.CreateConnection_SR_DB();

    //    OrderCartInformation();
    //    InvoiceCartInformation();
    //    GoodsIssueCartInformation();
    //    PaymentCartInformation();

    //    aDashBoardBll.CloseAllConnection();
    //}
    //private DateTime GetTodayDateTime()
    //{
    //    //return DateTime.Today;
    //    return Convert.ToDateTime("5/28/2018");
    //}
    //private void PaymentCartInformation()
    //{
    //    var paymentInformation = aDashBoardBll.LoadPaymentCartInfo(GetTodayDateTime());

    //    if (paymentInformation.Rows.Count > 0)
    //    {
    //        paymentLabel.Text = "TK. " + Convert.ToString(paymentInformation.Rows[0].Field<Decimal>("TotalPayment"), CultureInfo.InvariantCulture);
    //    }
    //    else
    //    {
    //        paymentLabel.Text = "TK. " +  Convert.ToString("0.00");
    //    }
    //}

    //private void GoodsIssueCartInformation()
    //{
    //    var goodsInformation = aDashBoardBll.LoadGoodsCartInfo(GetTodayDateTime());

    //    if (goodsInformation.Rows.Count > 0)
    //    {
    //        totalDeliveryLabel.Text = "TotalDelivary(" + Convert.ToString(goodsInformation.Rows[0].Field<Int32>("totalDelivary"), CultureInfo.InvariantCulture) + ")";
    //        totalValueLabel.Text = "TotalValue " + Convert.ToString(goodsInformation.Rows[0].Field<Decimal>("TotalValue"), CultureInfo.InvariantCulture);
    //        deliveryQtyLabel.Text = "Quantity (" + Convert.ToString(goodsInformation.Rows[0].Field<Decimal>("DeliveryQty"), CultureInfo.InvariantCulture) + ")";
    //    }
    //    else
    //    {
    //        totalDeliveryLabel.Text = Convert.ToString("0");
    //        totalValueLabel.Text = Convert.ToString("0.00");
    //        deliveryQtyLabel.Text = Convert.ToString("0");
    //    }
    //}

    //private void InvoiceCartInformation()
    //{
    //    var invoiceInformation = aDashBoardBll.LoadInvoiceCartInfo(GetTodayDateTime());

    //    if (invoiceInformation.Rows.Count > 0)
    //    {
    //        totalProformaLabel.Text = "TotalProforma(" + Convert.ToString(invoiceInformation.Rows[0].Field<Int32>("totalProforma"), CultureInfo.InvariantCulture) + ")";
    //        proformaTotalValueLabel.Text = "TotalValue " + Convert.ToString(invoiceInformation.Rows[0].Field<Decimal>("TotalValue"), CultureInfo.InvariantCulture);
    //        proformaQtyLabel.Text = "Quantity (" + Convert.ToString(invoiceInformation.Rows[0].Field<Decimal>("proformaQty"), CultureInfo.InvariantCulture) + ")";
    //    }
    //    else
    //    {
    //        totalProformaLabel.Text = Convert.ToString("0");
    //        proformaTotalValueLabel.Text = Convert.ToString("0.00");
    //        proformaQtyLabel.Text = Convert.ToString("0");
    //    }
    //}

    //private void OrderCartInformation()
    //{
    //    var orderInformation = aDashBoardBll.LoadOrderCartInfo(GetTodayDateTime());

    //    if (orderInformation.Rows.Count > 0)
    //    {
    //        totalOrderLabel.Text = "Total Order ( " + Convert.ToString(orderInformation.Rows[0].Field<Int32>("totalOrder"), CultureInfo.InvariantCulture) + ")";
    //        totalGrossValueLabel.Text = "GrossValue " + Convert.ToString(orderInformation.Rows[0].Field<Decimal>("TotalGrossValue"), CultureInfo.InvariantCulture);
    //        totalorderQtyLabel.Text = "Quantity ( " + Convert.ToString(orderInformation.Rows[0].Field<Decimal>("totalorderQty"), CultureInfo.InvariantCulture) + ")";
    //    }
    //    else
    //    {
    //        totalOrderLabel.Text = Convert.ToString("0");
    //        totalGrossValueLabel.Text = Convert.ToString("0.00");
    //        totalorderQtyLabel.Text = Convert.ToString("0");
    //    }
    //}
}